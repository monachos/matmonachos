use std::fmt;

#[derive(Debug, Clone)]
pub struct CalcError {
    pub message: String,
}

impl CalcError {
    pub fn from_string(msg: String) -> Self {
        Self {
            message: msg,
        }
    }

    pub fn from_str(msg: &str) -> Self {
        Self::from_string(String::from(msg))
    }

    pub fn not_supported() -> Self {
        Self::from_str("Not supported.")
    }
}


impl fmt::Display for CalcError {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "{}", self.message)
    }
}

impl std::error::Error for CalcError {
}
