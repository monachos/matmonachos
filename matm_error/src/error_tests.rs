#[cfg(test)]
mod tests {
    use crate::error::CalcError;

    #[test]
    fn test_init() {
        let err = CalcError {
            message: String::from("Simple message"),
        };
        assert_eq!("Simple message", err.message);
    }
    
    #[test]
    fn test_from_string() {
        let err = CalcError::from_string(String::from("Simple message"));
        assert_eq!("Simple message", err.message);
    }

    #[test]
    fn test_from_str() {
        let err = CalcError::from_str("Simple message");
        assert_eq!("Simple message", err.message);
    }

    #[test]
    fn test_not_supported() {
        let err = CalcError::not_supported();
        assert_eq!("Not supported.", err.message);
    }

    #[test]
    fn test_format() {
        let err = CalcError::from_str("Simple message");
        let result = format!("{}", err);
        assert_eq!("Simple message", result);
    }

    #[test]
    fn test_debug() {
        let err = CalcError::from_str("Simple message");
        let result = format!("{:?}", err);
        assert_eq!("CalcError { message: \"Simple message\" }", result);
    }

    #[test]
    fn test_clone() {
        let err = CalcError::from_str("Simple message");
        let result = err.clone();
        assert_eq!("Simple message", result.message);
    }
}