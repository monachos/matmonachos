#[cfg(test)]
mod tests {
    use ::rstest::rstest;
    use crate::colors::convert_colored_to_ratatui_color;

    #[rstest]
    #[case(colored::Color::Black, ratatui::style::Color::Black)]
    #[case(colored::Color::Red, ratatui::style::Color::Red)]
    #[case(colored::Color::Green, ratatui::style::Color::Green)]
    #[case(colored::Color::Yellow, ratatui::style::Color::Yellow)]
    #[case(colored::Color::Blue, ratatui::style::Color::Blue)]
    #[case(colored::Color::Magenta, ratatui::style::Color::Magenta)]
    #[case(colored::Color::Cyan, ratatui::style::Color::Cyan)]
    #[case(colored::Color::White, ratatui::style::Color::White)]
    #[case(colored::Color::BrightBlack, ratatui::style::Color::DarkGray)]
    #[case(colored::Color::BrightRed, ratatui::style::Color::LightRed)]
    #[case(colored::Color::BrightGreen, ratatui::style::Color::LightGreen)]
    #[case(colored::Color::BrightYellow, ratatui::style::Color::LightYellow)]
    #[case(colored::Color::BrightBlue, ratatui::style::Color::LightBlue)]
    #[case(colored::Color::BrightMagenta, ratatui::style::Color::LightMagenta)]
    #[case(colored::Color::BrightCyan, ratatui::style::Color::LightCyan)]
    #[case(colored::Color::BrightWhite, ratatui::style::Color::White)]
    #[case(
        colored::Color::TrueColor { r: 10, g: 20, b: 30 },
        ratatui::style::Color::Rgb(10, 20, 30)
    )]
    fn test_convert_colored_to_ratatui_color(
        #[case] input: colored::Color,
        #[case] expected: ratatui::style::Color,
    ) {
        let result = convert_colored_to_ratatui_color(input);
        assert_eq!(expected, result);
    }
}
