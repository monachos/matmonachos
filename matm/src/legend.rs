use crossterm::event::KeyModifiers;
use ratatui::prelude::Line;
use ratatui::style::Stylize;
use ratatui::text::Span;
use ratatui::widgets::block::Title;
use ratatui::widgets::Block;

use crate::application::ActiveWidget;

pub struct LegendItem {
    key: String,
    modifiers: KeyModifiers,
    description: String,
}

pub fn get_default_legend_items(active: &ActiveWidget) -> Vec<LegendItem> {
    return match active {
        ActiveWidget::Input => get_input_legend_items(),
        ActiveWidget::Output => get_output_legend_items(),
    };
}

fn get_input_legend_items() -> Vec<LegendItem> {
    return vec![
        LegendItem {
            key: String::from("esc"),
            modifiers: KeyModifiers::NONE,
            description: String::from("Activate Output"),
        },
        LegendItem {
            key: String::from("enter"),
            modifiers: KeyModifiers::ALT,
            description: String::from("Run"),
        },
        LegendItem {
            key: String::from("q"),
            modifiers: KeyModifiers::CONTROL,
            description: String::from("Quit"),
        },
    ];
}

fn get_output_legend_items() -> Vec<LegendItem> {
    return vec![
        LegendItem {
            key: String::from("enter"),
            modifiers: KeyModifiers::NONE,
            description: String::from("Activate Input"),
        },
        LegendItem {
            key: String::from("\u{2190}\u{2191}\u{2193}\u{2192} PgUp PgDown"),
            modifiers: KeyModifiers::NONE,
            description: String::from("Scroll"),
        },
        LegendItem {
            key: String::from("q"),
            modifiers: KeyModifiers::CONTROL,
            description: String::from("Quit"),
        },
    ];
}

pub fn legend_items_to_line(legend_items: &Vec<LegendItem>) -> Line {
    let mut items: Vec<Span> = Vec::new();
    items.push(" ".into());
    let mut add_sep = false;
    for li in legend_items {
        if add_sep {
            items.push("    ".into());
        }
        if li.modifiers.contains(KeyModifiers::SHIFT) {
            items.push("shift".light_red().bold());
            items.push("+".black().bold());
        }
        if li.modifiers.contains(KeyModifiers::CONTROL) {
            items.push("ctrl".light_red().bold());
            items.push("+".black().bold());
        }
        if li.modifiers.contains(KeyModifiers::ALT) {
            items.push("alt".light_red().bold());
            items.push("+".black().bold());
        }
        items.push(li.key.as_str().light_red().bold());
        items.push(" ".into());
        items.push(li.description.as_str().blue());
        add_sep = true;
    }
    return Line::from(items);
}

pub fn create_legend_block(legend_items: &Vec<LegendItem>) -> Block {
    let legend: Title = Title::from(legend_items_to_line(legend_items));
    return Block::new().title(legend).on_white();
}
