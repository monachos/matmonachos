use std::io;
use std::io::stdout;
use log::info;
use structured_logger::Builder;

use ratatui::prelude::CrosstermBackend;
use ratatui::prelude::Terminal;

mod application;
mod legend;
mod output;
mod colors;
mod colors_tests;

#[tokio::main]
async fn main() -> io::Result<()> {
    // Initialize the logger.
    Builder::with_level("error")
    // Builder::with_level("debug")
        .init();
    info!("Mat Monachos!");

    initialize_panic_handler();
    startup()?;
    run().await?;
    shutdown()?;
    Ok(())
}

pub fn initialize_panic_handler() {
    let original_hook = std::panic::take_hook();
    std::panic::set_hook(Box::new(move |panic_info| {
        shutdown().unwrap();
        original_hook(panic_info);
    }));
}

fn startup() -> io::Result<()> {
    crossterm::terminal::enable_raw_mode()?;
    crossterm::execute!(std::io::stderr(), crossterm::terminal::EnterAlternateScreen)?;
    Ok(())
}

fn shutdown() -> io::Result<()> {
    crossterm::execute!(std::io::stderr(), crossterm::terminal::LeaveAlternateScreen)?;
    crossterm::terminal::disable_raw_mode()?;
    Ok(())
}

async fn run() -> io::Result<()> {
    let mut terminal = Terminal::new(CrosstermBackend::new(stdout()))?;
    terminal.clear()?;

    let result = application::Appplication::new().run(&mut terminal);
    result
}
