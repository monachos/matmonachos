/// converts color colored::Color to ratatui::style::Color
pub(crate) fn convert_colored_to_ratatui_color(color: colored::Color) -> ratatui::style::Color {
    return match color {
        colored::Color::Black => ratatui::style::Color::Black,
        colored::Color::Red => ratatui::style::Color::Red,
        colored::Color::Green => ratatui::style::Color::Green,
        colored::Color::Yellow => ratatui::style::Color::Yellow,
        colored::Color::Blue => ratatui::style::Color::Blue,
        colored::Color::Magenta => ratatui::style::Color::Magenta,
        colored::Color::Cyan => ratatui::style::Color::Cyan,
        colored::Color::White => ratatui::style::Color::White,
        colored::Color::BrightBlack => ratatui::style::Color::DarkGray,
        colored::Color::BrightRed => ratatui::style::Color::LightRed,
        colored::Color::BrightGreen => ratatui::style::Color::LightGreen,
        colored::Color::BrightYellow => ratatui::style::Color::LightYellow,
        colored::Color::BrightBlue => ratatui::style::Color::LightBlue,
        colored::Color::BrightMagenta => ratatui::style::Color::LightMagenta,
        colored::Color::BrightCyan => ratatui::style::Color::LightCyan,
        colored::Color::BrightWhite => ratatui::style::Color::White,
        colored::Color::TrueColor { r, g, b } => ratatui::style::Color::Rgb(r, g, b),
    };
}
