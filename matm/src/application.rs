use std::rc::Rc;
use std::sync::Mutex;

use crossterm::event;
use crossterm::event::Event;
use crossterm::event::KeyCode;
use crossterm::event::KeyEvent;
use crossterm::event::KeyEventKind;
use crossterm::event::KeyModifiers;
use futures::executor::block_on;
use once_cell::sync::Lazy;
use ratatui::buffer::Buffer;
use ratatui::layout::Alignment;
use ratatui::layout::Offset;
use ratatui::layout::Rect;
use ratatui::prelude::CrosstermBackend;
use ratatui::prelude::Terminal;
use ratatui::style::Color;
use ratatui::style::Modifier;
use ratatui::style::Style;
use ratatui::style::Stylize;
use ratatui::widgets::Block;
use ratatui::widgets::BorderType;
use ratatui::widgets::Borders;
use ratatui::widgets::Paragraph;
use ratatui::widgets::Widget;
use ratatui::Frame;
use tui_textarea::TextArea;

use matm_calc::calculator::calculate_expression;
use matm_calc::calculator::context::CalcContext;
use matm_calc::result::ResultContainer;
use matm_calc::rules::CalcRules;
use matm_error::error::CalcError;
use matm_model::expressions::symbol::SymbolData;
use matm_model::expressions::Expression;
use matm_parser::errors::ErrorItemsContainer;
use matm_parser::parse_and_run_script_visitor;

use crate::legend::create_legend_block;
use crate::legend::get_default_legend_items;
use crate::output::OutputWidget;
use crate::output::OutputWidgetItem;

static CURRENT_OUTPUT_CONTENT_HEIGHT: Lazy<Mutex<i32>> = Lazy::new(|| Mutex::new(10));


pub enum ActiveWidget {
    Input,
    Output,
}

pub struct Appplication<'a> {
    exit: bool,
    input: TextArea<'a>,
    output: Vec<OutputWidgetItem>,
    output_offset: Offset,
    formula_counter: usize,
    active: ActiveWidget,
    calc_rules: CalcRules,
}

impl <'a> Appplication<'a> {
    pub fn new() -> Self {
        let mut input_widget = TextArea::default();
        input_widget.set_line_number_style(Style::default()
            .fg(Color::LightBlue)
            .add_modifier(Modifier::ITALIC));

        Self {
            exit: false,
            input: input_widget,
            output: Vec::new(),
            output_offset: Offset {
                x: 0,
                y: 0,
            },
            formula_counter: 0,
            active: ActiveWidget::Input,
            calc_rules: CalcRules::default(),
        }
    }

    pub fn run(
        &mut self,
        terminal: &mut Terminal<CrosstermBackend<std::io::Stdout>>,
    ) -> std::io::Result<()> {
        while !self.exit {
            terminal.draw(|frame| self.render_frame(frame))?;
            self.handle_events()?;
        }
        Ok(())
    }

    fn render_frame(&self, frame: &mut Frame) {
        frame.render_widget(self, frame.size());
    }

    fn handle_events(&mut self) -> std::io::Result<()> {
        match event::read()? {
            Event::Key(key_event) if key_event.kind == KeyEventKind::Press => {
                self.handle_key_event(key_event)
            }
            _ => {}
        };
        Ok(())
    }

    fn handle_key_event(&mut self, key_event: KeyEvent) {
        match self.active {
            ActiveWidget::Input => self.handle_input_key_event(key_event),
            ActiveWidget::Output => self.handle_output_key_event(key_event),
        }
    }

    fn handle_input_key_event(&mut self, key_event: KeyEvent) {
        match key_event.code {
            KeyCode::Enter => {
                //TODO Control modifier does not work, why?
                if key_event.modifiers == KeyModifiers::ALT {
                    block_on(self.run_input());
                    return;
                }
            },
            KeyCode::Esc => {
                self.active = ActiveWidget::Output;
                return;
            }
            KeyCode::Char('q') => {
                if key_event.modifiers.contains(KeyModifiers::CONTROL) {
                    self.exit();
                    return;
                }
            }
            _ => {}
        }
        self.input.input(key_event);
    }

    fn handle_output_key_event(&mut self, key_event: KeyEvent) {
        match key_event.code {
            KeyCode::Enter => {
                if key_event.modifiers == KeyModifiers::NONE {
                    self.active = ActiveWidget::Input;
                    return;
                }
            },
            KeyCode::Up => {
                if key_event.modifiers == KeyModifiers::NONE {
                    if self.output_offset.y > 0 {
                        self.output_offset.y -= 1;
                        return;
                    }
                }
            }
            KeyCode::Down => {
                if key_event.modifiers == KeyModifiers::NONE {
                    self.output_offset.y += 1;
                    return;
                }
            }
            KeyCode::Left => {
                if key_event.modifiers == KeyModifiers::NONE {
                    if self.output_offset.x > 0 {
                        self.output_offset.x -= 1;
                        return;
                    }
                }
            }
            KeyCode::Right => {
                if key_event.modifiers == KeyModifiers::NONE {
                    self.output_offset.x += 1;
                    return;
                }
            }
            KeyCode::PageUp => {
                if key_event.modifiers == KeyModifiers::NONE {
                    let page_height: i32 = *CURRENT_OUTPUT_CONTENT_HEIGHT.lock().unwrap();
                    if self.output_offset.y >= page_height {
                        self.output_offset.y -= page_height;
                    } else {
                        self.output_offset.y = 0;
                    }
                    return;
                }
            }
            KeyCode::PageDown => {
                if key_event.modifiers == KeyModifiers::NONE {
                    let page_height: i32 = *CURRENT_OUTPUT_CONTENT_HEIGHT.lock().unwrap();
                    self.output_offset.y += page_height;
                    return;
                }
            }
            KeyCode::Char('q') => {
                if key_event.modifiers.contains(KeyModifiers::CONTROL) {
                    self.exit();
                }
            }
            _ => {}
        }
    }

    fn exit(&mut self) {
        self.exit = true;
    }

    async fn run_input(&mut self) {
        let content: String = self.input.lines().join("\n").trim().to_owned();
        if !content.is_empty() {
            let mut errors = ErrorItemsContainer::new();
            let script: Vec<Rc<Expression>> = parse_and_run_script_visitor(&content, &mut errors);
            for script_expr in &script {
                self.formula_counter += 1;

                // save original expression
                let in_symbol = SymbolData::new(format!("in{}", self.formula_counter).as_str());
                let item = OutputWidgetItem::with_expression(in_symbol, Rc::clone(script_expr));
                self.output.push(item);

                // calculate expression
                let mut future_rules = CalcRules::default();
                future_rules.radix = self.calc_rules.radix;
                //TODO copy all rules
                let mut explanation = ResultContainer::new();
                let mut ctx = CalcContext::new(
                    &self.calc_rules,
                    &mut future_rules,
                    &mut explanation);
                match calculate_expression(&mut ctx, &script_expr).await {
                    Ok(result) => {
                        let item = OutputWidgetItem::with_result(explanation);
                        self.output.push(item);

                        let ans_symbol = SymbolData::new(format!("ans{}", self.formula_counter).as_str());
                        let item = OutputWidgetItem::with_expression(ans_symbol, Rc::clone(&result));
                        self.output.push(item);

                        // update current rules
                        self.calc_rules.radix = future_rules.radix;
                        //TODO update all rules
                    }
                    Err(err) => {
                        let item = OutputWidgetItem::with_error(err);
                        self.output.push(item);
                    }
                }
            }
            for err in errors.all() {
                let err_msg = format!("{:?} error at ({},{}): {}",
                                      err.source,
                                      err.line,
                                      err.column,
                                      err.message);
                let item = OutputWidgetItem::with_error(CalcError::from_string(err_msg));
                self.output.push(item);
            }
            
            self.input.select_all();
            self.input.cut();
        }
    }
}

impl Widget for &Appplication<'_> {
    fn render(self, area: Rect, buf: &mut Buffer) {
        Paragraph::new("Welcome to Mat Monachos!")
            .light_yellow()
            .on_black()
            .alignment(Alignment::Center)
            .render(Rect::new(0, 0, area.width, 1), buf);

        let title_height: u16 = 1;
        let legend_height: u16 = 1;
        let input_height: u16 = 6;

        let non_output_height: u16 = title_height + input_height + legend_height;
        let output_height: u16 = if area.height >= non_output_height {
            area.height - non_output_height
        } else {
            0
        };
        let min_output_height: u16 = 3;
        if output_height >= min_output_height {
            let border_color = match self.active {
                ActiveWidget::Output => Color::White,
                _ => Color::DarkGray,
            };
            Block::new()
                .title("Output")
                .borders(Borders::ALL)
                .border_type(BorderType::Rounded)
                .style(Style::default().bg(Color::Black).fg(border_color))
                .render(Rect::new(0, title_height, area.width, output_height), buf);

            let output_content_area = Rect::new(1, title_height + 1, area.width - 2, output_height - 2);
            *CURRENT_OUTPUT_CONTENT_HEIGHT.lock().unwrap() = output_content_area.height as i32;
            OutputWidget::new(&self.output, self.output_offset)
                .render(output_content_area, buf);
        }

        let input_y = title_height + output_height;
        if input_y + input_height < area.height {
            let border_color = match self.active {
                ActiveWidget::Input => Color::White,
                _ => Color::DarkGray,
            };
            Block::new()
                .title("Input")
                .borders(Borders::ALL)
                .border_type(BorderType::Rounded)
                .style(Style::default().bg(Color::Black).fg(border_color))
                .render(Rect::new(0, input_y, area.width, input_height), buf);
            let input_widget = self.input.widget();
            input_widget.render(Rect::new(1, input_y + 1, area.width - 2, input_height - 2), buf);
        }
        let legend_items = get_default_legend_items(&self.active);
        let legend = create_legend_block(&legend_items);
        legend.render(Rect::new(0, area.y + area.height - 1, area.width, 1), buf);
    }
}
