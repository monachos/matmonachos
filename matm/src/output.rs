use std::rc::Rc;
use async_recursion::async_recursion;
use futures::executor::block_on;

use matm_error::error::CalcError;
use matm_calc::result::ResultContainer;
use matm_calc::result::ResultItem;
use matm_model::core::dimensions::Coordinates;
use matm_model::core::index_range::IndexRange;
use matm_model::expressions::assignment::AssignmentData;
use matm_model::expressions::symbol::SymbolData;
use matm_model::expressions::ToExpression;
use ratatui::buffer::Buffer;
use ratatui::layout::Offset;
use ratatui::style::Style;
use ratatui::text::Line;
use ratatui::text::Span;
use ratatui::widgets::Paragraph;
use ratatui::layout::Rect;
use ratatui::widgets::Widget;
use matm_model::core::index::Index2D;
use matm_model::expressions::Expression;
use output_textcanv::output::colors::ColorSchema;
use output_textcanv::output::colors::TokenType;
use output_textcanv::output::text::TextCanvas;
use output_textcanv::output::text::TextCanvasContext;
use output_textcanv::output::text::TextCanvasRender;
use crate::colors::convert_colored_to_ratatui_color;


pub struct OutputWidget<'a> {
    items: &'a Vec<OutputWidgetItem>,
    offset: Offset,
}

impl<'a> OutputWidget<'a> {
    pub fn new(items: &'a Vec<OutputWidgetItem>, offset: Offset) -> Self {
        Self {
            items: items,
            offset: offset,
        }
    }
}

impl<'a> Widget for OutputWidget<'a> {
    fn render(self, area: Rect, buf: &mut Buffer) {
        let ctx = TextCanvasContext::unicode_color();
        let schema = ColorSchema::new();
        let content_area = Rect::new(area.x, area.y, area.width, area.height);
        let mut line_pos: u16 = 0;
        for item in self.items {
            match block_on(item.render(&content_area, &mut line_pos, &self.offset, buf, &ctx, &schema)) {
                Ok(_) => {
                    line_pos += 1;
                }
                Err(_) => {
                    break;
                }
            }
            let y = line_pos as i32 - self.offset.y;
            if y > 0 && y as u16 > (area.y + area.height) {
                break;
            }
        }
    }
}

//------------------------------------------------------

enum RenderingResult {
    ContinueRendering,
    BreakRendering,
}

pub enum OutputWidgetItemContent {
    Expression(Rc<Expression>),
    Result(ResultContainer),
    Error(CalcError),
}

pub struct OutputWidgetItem {
    symbol: Option<SymbolData>,
    content: OutputWidgetItemContent,
}

impl OutputWidgetItem {
    pub fn with_expression(sym: SymbolData, expr: Rc<Expression>) -> Self {
        Self {
            symbol: Some(sym),
            content: OutputWidgetItemContent::Expression(expr),
        }
    }

    pub fn with_result(result: ResultContainer) -> Self {
        Self {
            symbol: None,
            content: OutputWidgetItemContent::Result(result),
        }
    }

    pub fn with_error(err: CalcError) -> Self {
        Self {
            symbol: None,
            content: OutputWidgetItemContent::Error(err),
        }
    }

    async fn render(
        &self,
        area: &Rect,
        line_pos: &mut u16,
        offset: &Offset,
        buf: &mut Buffer,
        ctx: &TextCanvasContext,
        schema: &ColorSchema,
    ) -> Result<(), CalcError> {
        match &self.content {
            OutputWidgetItemContent::Expression(expr) => {
                let sym = &self.symbol.clone().unwrap();
                Self::render_expression(sym, expr, area, line_pos, offset, buf, ctx, schema).await?;
            }
            OutputWidgetItemContent::Result(result) => {
                Self::render_result(result, area, line_pos, offset, buf, ctx, schema).await?;
            }
            OutputWidgetItemContent::Error(err) => {
                Self::render_error(err, area, line_pos, offset, buf, ctx, schema).await?;
            }
        }
        Ok(())
    }

    async fn render_expression(
        symbol: &SymbolData,
        expr: &Rc<Expression>,
        area: &Rect,
        line_pos: &mut u16,
        offset: &Offset,
        buf: &mut Buffer,
        ctx: &TextCanvasContext,
        schema: &ColorSchema,
    ) -> Result<(), CalcError> {
        let assignment = AssignmentData::new(symbol.clone().to_rc_expr(), Rc::clone(expr));
        let canvas = assignment.to_text_canvas(ctx)?;
        Self::render_canvas(&canvas, area, line_pos, offset, 0, buf, schema).await?;
        Ok(())
    }

    #[async_recursion(?Send)]
    async fn render_result(
        result: &ResultContainer,
        area: &Rect,
        line_pos: &mut u16,
        offset: &Offset,
        buf: &mut Buffer,
        ctx: &TextCanvasContext,
        schema: &ColorSchema,
    ) -> Result<RenderingResult, CalcError> {
        let mut add_sep = false;
        for item in &result.items {
            match item {
                ResultItem::Statement(stmt) => {
                    if add_sep {
                        *line_pos += 1;
                    }
                    let canvas = stmt.to_text_canvas(ctx)?;
                    if let RenderingResult::BreakRendering = Self::render_canvas(&canvas, area, line_pos, offset, result.indentation, buf, schema).await? {
                        return Ok(RenderingResult::BreakRendering);
                    }
                    add_sep = true;
                }
                ResultItem::Nested(sub_result) => {
                    if !sub_result.items.is_empty() {
                        if add_sep {
                            *line_pos += 1;
                        }
                        if let RenderingResult::BreakRendering = Self::render_result(sub_result, area, line_pos, offset, buf, ctx, schema).await? {
                            return Ok(RenderingResult::BreakRendering);
                        }
                        add_sep = true;
                    }
                }
            }
        }
        return Ok(RenderingResult::ContinueRendering);
    }

    async fn render_error(
        err: &CalcError,
        area: &Rect,
        line_pos: &mut u16,
        offset: &Offset,
        buf: &mut Buffer,
        _ctx: &TextCanvasContext,
        schema: &ColorSchema,
    ) -> Result<(), CalcError> {
        let canvas = TextCanvas::from_row_text_color(&err.message, TokenType::Error);
        Self::render_canvas(&canvas, area, line_pos, offset, 0, buf, schema).await?;
        Ok(())
    }

    /// area - area of the widget available for rendering
    async fn render_canvas(
        canvas: &TextCanvas,
        area: &Rect,
        line_pos: &mut u16,
        offset: &Offset,
        indentation: usize,
        buf: &mut Buffer,
        schema: &ColorSchema,
    ) -> Result<RenderingResult, CalcError> {
        if let Some(coords) = canvas.coords() {
            for r in coords.vertical.iter(1)? {
                let y = *line_pos as i32 - offset.y + area.y as i32;
                if y > 0 {
                    let y = y as u16;
                    if y >= area.y + area.height {
                        // skip all next rows
                        return Ok(RenderingResult::BreakRendering);
                    }
                    if y >= area.y && y < area.y + area.height {
                        Self::render_canvas_row(
                            canvas,
                            &coords,
                            r,
                            area,
                            offset,
                            y,
                            indentation,
                            buf,
                            schema).await?;
                    }
                }
                *line_pos += 1;
            }
        }
        return Ok(RenderingResult::ContinueRendering);
    }

    async fn render_canvas_row(
        canvas: &TextCanvas,
        coords: &Coordinates<IndexRange>,
        r: usize,
        area: &Rect,
        offset: &Offset,
        y: u16,
        indentation: usize,
        buf: &mut Buffer,
        schema: &ColorSchema,
    ) -> Result<(), CalcError> {
        let mut line_items: Vec<Span> = Vec::new();
        let mut items_to_skip: i32 = offset.x;

        // add indent
        for _ in 0 .. indentation * 4 {
            if items_to_skip > 0 {
                items_to_skip -= 1;
            } else {
                line_items.push(Span::raw(" "));
            }
        }

        // add pixels
        for c in coords.horizontal.iter(1)? {
            if items_to_skip > 0 {
                items_to_skip -= 1;
            } else {
                let key = Index2D::new(r, c)?;
                match canvas.get_pixel(&key) {
                    Some(pixel) => {
                        //TODO pixels with the same style can be rendered as one span
                        let color = convert_colored_to_ratatui_color(schema.color(&pixel.token));
                        line_items.push(Span::styled(pixel.text.to_string(), Style::default().fg(color)));
                    }
                    None => {
                        line_items.push(Span::raw(" "));
                    }
                }
            }
        }

        let line = Line::from(line_items);
        let par_pos = Rect::new(area.x, y, area.width, 1);
        Paragraph::new(line).render(par_pos, buf);
        Ok(())
    }
}
