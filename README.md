# Mat Monachos

Welcome to Mat Monachos!
This is a Computer Algebra System (CAS) that helps students solve various math problems, especially linear algebra.
The program performs calculations from a human point of view,
showing step-by-step solution.

Currently, the program is on a very basic stage, as it only allows basic
calculations that are available mainly from unit tests.
A small subset of the implemented expressions and calculations
are available from within the `matm` console application.

## Currently implemented features

| Feature                              | Model | Parsing | Rendering | Calculating         |
|--------------------------------------|-------|---------|-----------|---------------------|
| decimal integers (no length limit)   | yes   | yes     | yes       |                     |
| radix integers (single character)    | yes   | yes     | yes       | yes                 |
| fractions                            | yes   | yes     | yes       | yes                 |
| int/frac op. int/frac                | yes   | yes     | yes       | yes                 |
| iterated binary expression           | yes   | no      | yes       | yes                 |
| exponentiation                       | yes   | yes     | yes       | partial             |
| root                                 | yes   | yes     | yes       | partial, not stable |
| logarithm                            | yes   | yes     | yes       | no                  |
| negation operator                    | yes   | yes     | yes       | yes                 |
| factorial                            | yes   | yes     | yes       | yes                 |
| binomial coefficients                | yes   | yes     | yes       | yes                 |
| matrix                               | yes   | yes     | yes       |                     |
| matrix op. int/frac                  | yes   | yes     | yes       | yes                 |
| int/frac op. matrix                  | yes   | yes     | yes       | yes                 |
| matrix + - * matrix                  | yes   | yes     | yes       | yes                 |
| LU matrix decomp.                    | no    | no      | yes       | yes                 |
| Cholesky-Banachiewicz matrix decomp. | yes   | yes     | yes       | yes                 |
| matrix det. (Sarrus)                 | yes   | yes     | yes       | yes                 |
| matrix det. (Laplace expansion)      | yes   | yes     | yes       | yes                 |
| matrix det. (Bareiss-Montante)       | yes   | yes     | yes       | partial             |
| Gaussian elimination by rows         | yes   | yes     | yes       | yes                 |
| Gaussian elimination by columns      | no    | no      | yes       | no                  |
| equation                             | yes   | yes     | yes       | yes                 |
| system of eq. (Cramer)               | no    | no      | yes       | yes                 |
| system of eq. (Gaussian ellim.)      | no    | no      | yes       | yes                 |
| system of eq. (upper tri. matrix)    | no    | no      | yes       | yes                 |
| matrix inversion (determinant)       | yes   | yes     | yes       | yes                 |
| matrix inversion (Gaussian ellim.)   | yes   | yes     | yes       | yes                 |
| matrix rank                          | yes   | yes     | yes       | yes                 |
| matrix transposition                 | yes   | yes     | yes       | yes                 |
| cracovian                            | yes   | yes     | yes       |                     |


## Requirements

To build the program Rust compiler is required.
See https://www.rust-lang.org/ for detailed information.

This project has been tested on Linux platform only.

## Building the app

```bash
cargo build
```

If you want to change the grammar files (lexer or parser),
you need to configure them with antlr4rust,
which is described in [matm_parser](matm_parser/README.md).

## Running the console app

```bash
cargo run
```

![Multiplication of matrices](img/mx-mul.png)

![Multiplication of matrices](img/mx-det.png)

## Known issues

There are a lot of missing functionality.
Some serious bugs are reported as ignored unit tests.

# Examples

Run the `matm` application and type one of the following expressions in the Input field:

```
1+2/3+3/(2+4);
1+-2;
2^8;
4^3^2;
x^2 + 2*x^2;
sin(1-2/2);
0.is_zero();
1.is_zero();
0.is_one();
1.is_one();
(1+2).is_positive();
(1-2).is_positive();
(1+2).is_negative();
(1-2).is_negative();
radix()
radix(2)
2#1001# + 2#11#
radix(16)
16#A0# + 100
factorial(5);
log(100);
log(100, 5);
log10(100);
log8(16);
root(100);
root(125, 3);
sqrt(100);
binomial(10, 3);
binomial(n, k);
equations().system(x=1+2, y=3+4);
matrix().zeros(3);
matrix().zeros(2, 3);
matrix().ones(3);
matrix().ones(2, 3);
matrix().identity(3);
matrix().diagonal(3, 3, 1, 2, 3);
matrix().diagonal(3, 5, 1, 2, 3);
matrix().of(10/5, 1+1, 1/2, 2/16, 10, 4/(3+5));
matrix().of(2, 2, 1/2, 1/3, 10, 2/3) * 2/5;
matrix().hilbert(3);
matrix().pascal(6);
matrix().pascal(6, pascal_matrix_config().symmetric());
matrix().pascal(6, pascal_matrix_config().lower());
matrix().pascal(6, pascal_matrix_config().upper());
matrix().pascal(6, pascal_matrix_config().lower()) * matrix().pascal(6, pascal_matrix_config().upper());
matrix().vandermonde(4, 2, 3);
matrix().vandermonde(4, a, b, c);
matrix().of(2, 3, 1, 2, 3, 4, 5, 6) * matrix().of(3, 1, 1, 2, 3);
matrix().of(3, 2, 1, 2, 3, 4, 5, 6) * matrix().of(2, 3, 2, 2, 100, 3, 1, 10);
matrix().of(1, 1, 5).det();
matrix().of(2, 2, 1, 2, 3, 4).det();
matrix().of(3, 3, 1, 2, 3, 4, 1, 6, 1, 3, 3).det();
matrix().of(3, 3, 1, 2, 3, 4, 1, 6, 1, 3, 3).det(det_config().laplace());
matrix().of(3, 3, 1, 2, 3, 4, 1, 6, 1, 3, 3).det(det_config().bareiss());
matrix().of(4, 4, 1, 1, 3, 5, 2, 1/4, 5/6, 1, 1, 1, 1, 2, 2/4, 1, 4, 1/2).det();
matrix().ones(5, 5).det();
matrix().ones(6, 6).det();
matrix().of(3, 3, 1, 2, 3, 4, 1, 6, 1, 3, 3).inv();
matrix().of(3, 3, 1, 2, 3, 4, 1, 6, 1, 3, 3).inv(inv_config().adj());
matrix().of(3, 3, 1, 2, 3, 4, 1, 6, 1, 3, 3).inv(inv_config().gauss());
matrix().of(4, 5,
   1, 2, 3, 4, 1,
   2, 5, 5, 7, 2,
   4, 6, 8, 9, 3,
   3, 5, 4, 4, 2).rank();
matrix().of(2, 3, 1, 2, 3, 4, 5, 6).transpose();
matrix().of(3, 3,
   1, 2, 3,
   2, 8, 10,
   3, 10, 22).cholesky();
matrix().of(3, 3,
   1, 2, 3,
   2, 8, 10,
   3, 10, 22).gauss();
matrix().of(3, 3,
   1, 2, 3,
   2, 8, 10,
   3, 10, 22).gauss(gauss_config());
matrix().of(3, 3,
   1, 2, 3,
   2, 8, 10,
   3, 10, 22).gauss(gauss_config().reduce_lower());
matrix().of(3, 3,
   1, 2, 3,
   2, 8, 10,
   3, 10, 22).gauss(gauss_config().reduce_upper());
matrix().of(3, 3,
   1, 2, 3,
   2, 8, 10,
   3, 10, 22).gauss(gauss_config().reduce_lower().reduce_upper());
matrix().of(3, 3,
   1, 2, 3,
   2, 8, 10,
   3, 10, 22).gauss(gauss_config().reduce_lower().reduce_upper().identity());
matrix().of(3, 3,
   1, 2, 3,
   2, 8, 10,
   3, 10, 22).gauss(gauss_config().reduce_lower().pivot_less());
matrix().of(3, 3,
   0, 2, 3,
   2, 8, 10,
   3, 10, 22).gauss(gauss_config().reduce_lower().pivot_zero());
matrix().ones(2, 3).rows();
matrix().ones(2, 3).columns();
matrix().ones(2, 3).is_square();
matrix().ones(3, 3).is_square();
matrix().ones(2, 3).is_zero();
matrix().zeros(2, 3).is_zero();
matrix().ones(2, 3).is_ones();
matrix().zeros(2, 3).is_ones();
matrix().ones(3, 3).is_identity();
matrix().identity(3).is_identity();
matrix().of(3, 3,
   1, 1, 1,
   0, 1, 1,
   0, 0, 1).is_upper_triangular();
matrix().of(3, 3,
   0, 1, 1,
   0, 0, 1,
   0, 0, 0).is_upper_triangular();
matrix().ones(3, 3).is_strictly_upper_triangular();
matrix().of(3, 3,
   1, 1, 1,
   0, 1, 1,
   0, 0, 1).is_strictly_upper_triangular();
matrix().of(3, 3,
   0, 1, 1,
   0, 0, 1,
   0, 0, 0).is_strictly_upper_triangular();
matrix().ones(3, 3).is_lower_triangular();
matrix().of(3, 3,
   1, 0, 0,
   2, 4, 0,
   3, 4, 1).is_lower_triangular();
matrix().of(3, 3,
   1, 2, 3,
   2, 4, 4,
   3, 4, 1).is_symmetric();
cracovian().zeros(3);
cracovian().zeros(2, 3);
cracovian().of(3, 2, 1, 2, 3, 4, 5, 6);
```
