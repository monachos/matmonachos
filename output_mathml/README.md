This crate creates MathML code from Mat Monachos data model.

## Links:

[MathML 3 standard](https://www.w3.org/TR/MathML3/)
