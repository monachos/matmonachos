use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::mn::MNData;
use mathml_model::MTag;
use mathml_model::ToMTag;
use matm_error::error::CalcError;
use matm_model::core::natural::Natural;
use std::rc::Rc;


impl MathMLRenderer for Natural {
    fn to_mathml(&self, _ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        Ok(MNData::from_string(&format!("{}", self)).to_tag())
    }
}