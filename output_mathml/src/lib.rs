use std::rc::Rc;
use mathml_model::MTag;
use matm_error::error::CalcError;

mod expressions;
mod statements;
mod core;

pub struct MathMLContext {
}

pub trait MathMLRenderer {
    fn to_mathml(&self, _ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        Err(CalcError::from_str("Object does not support MathML rendering."))
    }
}
