use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::mo::MOData;
use mathml_model::mrow::MRowData;
use mathml_model::{MTag, MValue};
use mathml_model::ToMTag;
use matm_error::error::CalcError;
use matm_model::expressions::binary::BinaryData;
use matm_model::expressions::binary::BinaryOperator;
use std::rc::Rc;

impl MathMLRenderer for BinaryData {
    fn to_mathml(&self, ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let mut tag = MRowData::new();

        if self.should_wrap_left_with_parentheses() {
            tag.children.push(MOData::from_string("(").to_tag());
            tag.children.push(self.left.to_mathml(ctx)?);
            tag.children.push(MOData::from_string(")").to_tag());
        } else {
            tag.children.push(self.left.to_mathml(ctx)?);
        };

        let op: MValue = match self.operator {
            BinaryOperator::Add => MValue::Text(String::from("+")),
            BinaryOperator::Sub => MValue::Text(String::from("-")),
            BinaryOperator::Mul => MValue::Raw(String::from("&#x22C5;")),
            BinaryOperator::Div => MValue::Text(String::from("/")),
        };
        tag.children.push(MOData::new(op).to_tag());

        if self.should_wrap_right_with_parentheses() {
            tag.children.push(MOData::from_string("(").to_tag());
            tag.children.push(self.right.to_mathml(ctx)?);
            tag.children.push(MOData::from_string(")").to_tag());
        } else {
            tag.children.push(self.right.to_mathml(ctx)?);
        };

        Ok(tag.to_tag())
    }
}

#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::core::natural::RADIX_10;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::int::IntData;
    use matm_model::expressions::ToExpression;

    #[test]
    fn test_render_to_mathml_add_ints() {
        let data = BinaryData::add(
            IntData::from_int(RADIX_10, 1).unwrap().to_rc_expr(),
            IntData::from_int(RADIX_10, 2).unwrap().to_rc_expr(),
        ).to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mrow>",
            "<mn>1</mn>",
            "<mo>+</mo>",
            "<mn>2</mn>",
            "</mrow>",
        ].join("");
        assert_eq!(expected, output);
    }

    #[test]
    fn test_render_to_mathml_mul_bins() {
        let data = BinaryData::mul(
            BinaryData::add(
                IntData::from_int(RADIX_10, 1).unwrap().to_rc_expr(),
                IntData::from_int(RADIX_10, 2).unwrap().to_rc_expr(),
            ).to_rc_expr(),
            BinaryData::add(
                IntData::from_int(RADIX_10, 3).unwrap().to_rc_expr(),
                IntData::from_int(RADIX_10, 4).unwrap().to_rc_expr(),
            ).to_rc_expr(),
        ).to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mrow>",
            "<mo>(</mo>",
            "<mrow><mn>1</mn><mo>+</mo><mn>2</mn></mrow>",
            "<mo>)</mo>",
            "<mo>&#x22C5;</mo>",
            "<mo>(</mo>",
            "<mrow><mn>3</mn><mo>+</mo><mn>4</mn></mrow>",
            "<mo>)</mo>",
            "</mrow>",
        ].join("");
        assert_eq!(expected, output);
    }
}
