use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::mi::MIData;
use mathml_model::mo::MOData;
use mathml_model::mrow::MRowData;
use mathml_model::msub::MSubData;
use mathml_model::mtext::MTextData;
use mathml_model::MTag;
use mathml_model::ToMTag;
use matm_error::error::CalcError;
use matm_model::expressions::symbol::SymbolData;
use std::rc::Rc;


impl MathMLRenderer for SymbolData {
    fn to_mathml(&self, ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let mut symbol_tags: Vec<Rc<MTag>> = Vec::new();

        if !self.indices.is_empty() || !self.postfix.is_empty() {
            if !self.indices.is_empty() {
                let base_tag = MIData::from_string(&self.name).to_tag();

                let mut indices_tags: Vec<Rc<MTag>> = Vec::new();
                match self.indices.len() {
                    0 => (),
                    1 => {
                        indices_tags.push(self.indices[0].to_mathml(ctx)?);
                    }
                    _ => {
                        let mut need_sep = false;
                        for index in &self.indices {
                            if need_sep {
                                indices_tags.push(MTextData::from_string(",").to_tag());
                            }
                            indices_tags.push(index.to_mathml(ctx)?);
                            need_sep = true;
                        }
                    }
                }

                let index_tag = if indices_tags.len() == 1 {
                    Rc::clone(&indices_tags[0])
                } else {
                    MRowData::from_vector(indices_tags).to_tag()
                };

                symbol_tags.push(MSubData::new(base_tag, index_tag).to_tag());
            } else {
                symbol_tags.push(MIData::from_string(&self.name).to_tag());
            }
        } else {
            symbol_tags.push(MIData::from_string(&self.name).to_tag());
        }

        if !self.postfix.is_empty() {
            symbol_tags.push(MOData::from_string(&self.postfix).to_tag());
        }

        let tag = if symbol_tags.len() == 1 {
            Rc::clone(&symbol_tags[0])
        } else {
            MRowData::from_vector(symbol_tags).to_tag()
        };
        Ok(tag)
    }
}

#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::core::natural::RADIX_10;
    use matm_model::expressions::int::IntData;
    use matm_model::expressions::symbol::SymbolData;
    use matm_model::expressions::ToExpression;

    #[test]
    fn test_render_to_mathml_simple() {
        let data = SymbolData::new("x").to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        assert_eq!("<mi>x</mi>", output);
    }

    #[test]
    fn test_render_to_mathml_index() {
        let data = SymbolData::with_index_e(
            "x",
            IntData::from_int(RADIX_10, 1).unwrap().to_rc_expr()
        );
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        assert_eq!("<msub><mi>x</mi><mn>1</mn></msub>", output);
    }

    #[test]
    fn test_render_to_mathml_many_indices() {
        let data = SymbolData::with_index_e_e(
            "x",
            IntData::from_int(RADIX_10, 1).unwrap().to_rc_expr(),
            IntData::from_int(RADIX_10, 2).unwrap().to_rc_expr()
        ).to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<msub>",
            "<mi>x</mi>",
            "<mrow><mn>1</mn><mtext>,</mtext><mn>2</mn></mrow>",
            "</msub>",
        ].join("");
        assert_eq!(expected, output);
    }

    #[test]
    fn test_render_to_mathml_postfix() {
        let mut data = SymbolData::new("x");
        data.postfix = String::from("'");
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mrow>",
            "<mi>x</mi><mo>'</mo>",
            "</mrow>",
        ].join("");
        assert_eq!(expected, output);
    }

    #[test]
    fn test_render_to_mathml_index_postfix() {
        let data = SymbolData::with_index_e_and_postfix(
            "x",
            IntData::from_int(RADIX_10, 1).unwrap().to_rc_expr(),
            "'"
        ).to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mrow>",
            "<msub><mi>x</mi><mn>1</mn></msub>",
            "<mo>'</mo>",
            "</mrow>",
        ].join("");
        assert_eq!(expected, output);
    }
}
