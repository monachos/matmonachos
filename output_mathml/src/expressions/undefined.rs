use crate::MathMLContext;
use mathml_model::mtext::MTextData;
use mathml_model::MTag;
use mathml_model::ToMTag;
use matm_error::error::CalcError;
use std::rc::Rc;

pub(crate) fn render_undefined_to_mathml(_ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
    let tag = MTextData::from_string("?").to_tag();
    Ok(tag)
}

#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::expressions::Expression;

    #[test]
    fn test_render_to_mathml() {
        let data = Expression::Undefined;
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        assert_eq!("<mtext>?</mtext>", output);
    }
}
