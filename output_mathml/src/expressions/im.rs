use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::mi::MIData;
use mathml_model::MTag;
use mathml_model::ToMTag;
use matm_error::error::CalcError;
use matm_model::expressions::imaginary::ImData;
use std::rc::Rc;

impl MathMLRenderer for ImData {
    fn to_mathml(&self, _ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let tag = MIData::from_string(&self.name).to_tag();
        Ok(tag)
    }
}


#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::expressions::imaginary::ImData;
    use matm_model::expressions::ToExpression;

    #[test]
    fn test_render_to_mathml_i() {
        let data = ImData::new("i").to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        assert_eq!("<mi>i</mi>", output);
    }
}
