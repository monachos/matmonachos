mod int;
mod bool;
mod frac;
mod symbol;
mod variable;
mod transc;
mod im;
mod log;
mod binary;
mod exp;
mod func;
mod method;
mod object;
mod assignment;
mod root;
mod equation;
mod soe;
mod matrix;
mod cracovian;
mod binomial;
mod conditional;
mod iterated_binary;
mod boxsign;
mod undefined;
mod unary;

use std::rc::Rc;
use mathml_model::MTag;
use crate::MathMLContext;
use crate::MathMLRenderer;
use matm_error::error::CalcError;
use matm_model::expressions::Expression;
use crate::expressions::boxsign::render_boxsign_to_mathml;
use crate::expressions::undefined::render_undefined_to_mathml;

impl MathMLRenderer for Expression {
    fn to_mathml(&self, ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        match self {
            Expression::Assignment(data) => data.to_mathml(ctx),
            Expression::Binary(data) => data.to_mathml(ctx),
            Expression::Binomial(data) => data.to_mathml(ctx),
            Expression::Bool(data) => data.to_mathml(ctx),
            Expression::BoxSign => render_boxsign_to_mathml(ctx),
            Expression::Conditional(data) => data.to_mathml(ctx),
            Expression::Cracovian(data) => data.to_mathml(ctx),
            Expression::Exp(data) => data.to_mathml(ctx),
            Expression::Equation(data) => data.to_mathml(ctx),
            Expression::Frac(data) => data.to_mathml(ctx),
            Expression::Function(data) => data.to_mathml(ctx),
            Expression::Im(data) => data.to_mathml(ctx),
            Expression::Int(data) => data.to_mathml(ctx),
            Expression::IteratedBinary(data) => data.to_mathml(ctx),
            Expression::Log(data) => data.to_mathml(ctx),
            Expression::Matrix(data) => data.to_mathml(ctx),
            Expression::Method(data) => data.to_mathml(ctx),
            Expression::Object(data) => data.to_mathml(ctx),
            Expression::Root(data) => data.to_mathml(ctx),
            Expression::Symbol(data) => data.to_mathml(ctx),
            Expression::SysOfEq(data) => data.to_mathml(ctx),
            Expression::Transc(data) => data.to_mathml(ctx),
            Expression::Unary(data) => data.to_mathml(ctx),
            Expression::Undefined => render_undefined_to_mathml(ctx),
            Expression::Variable(data) => data.to_mathml(ctx),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::factories::ObjFactory;

    #[test]
    fn test_render_to_mathml() {
        let fa = ObjFactory::new10();
        let expr = fa.ezi_u(10);
        let ctx = MathMLContext {};

        let tag = expr.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        assert_eq!("<mn>10</mn>", output);
    }
}