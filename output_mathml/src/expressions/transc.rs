use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::mi::MIData;
use mathml_model::MValue;
use mathml_model::MTag;
use mathml_model::ToMTag;
use matm_error::error::CalcError;
use matm_model::expressions::transcendental::TranscData;
use std::rc::Rc;

impl MathMLRenderer for TranscData {
    fn to_mathml(&self, _ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let tag = match self {
            TranscData::Pi => MIData::new(MValue::Raw("&#x3C0;".to_owned())).to_tag(),
            TranscData::Euler => MIData::from_string("e").to_tag(),
        };
        Ok(tag)
    }
}


#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::expressions::transcendental::TranscData;
    use matm_model::expressions::ToExpression;

    #[test]
    fn test_render_to_mathml_pi() {
        let data = TranscData::Pi.to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        assert_eq!("<mi>&#x3C0;</mi>", output);
    }

    #[test]
    fn test_render_to_mathml_euler() {
        let data = TranscData::Euler.to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        assert_eq!("<mi>e</mi>", output);
    }
}
