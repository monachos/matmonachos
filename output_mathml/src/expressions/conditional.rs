use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::mo::MOData;
use mathml_model::mrow::MRowData;
use mathml_model::mtext::MTextData;
use mathml_model::MValue;
use mathml_model::MTag;
use mathml_model::ToMTag;
use matm_error::error::CalcError;
use matm_model::expressions::arithmetic_condition::ArithmeticCondition;
use matm_model::expressions::arithmetic_operator::ArithmeticOperator;
use matm_model::expressions::condition::Condition;
use matm_model::expressions::conditional::ConditionalData;
use matm_model::expressions::logic_condition::LogicCondition;
use matm_model::expressions::logic_operator::LogicOperator;
use std::rc::Rc;
use mathml_model::mtable::MTableData;
use mathml_model::mtd::MTdData;
use mathml_model::mtr::MTrData;

impl MathMLRenderer for ConditionalData {
    fn to_mathml(&self, ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let mut tag = MRowData::new();
        tag.children.push(MOData::from_string("{").to_tag());
        let mut table = MTableData::new();
        for item in &self.clauses {
            let mut row = MTrData::new();

            let td = MTdData::new(item.expression.to_mathml(ctx)?);
            row.children.push(td.to_rc());

            let td = MTdData::new(item.condition.to_mathml(ctx)?);
            row.children.push(td.to_rc());

            table.rows.push(row.to_rc());
        }
        tag.children.push(table.to_tag());
        Ok(tag.to_tag())
    }
}

impl MathMLRenderer for Condition {
    fn to_mathml(&self, ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        match self {
            Condition::Bool(data) => data.to_mathml(ctx),
            Condition::Arithmetic(data) => data.to_mathml(ctx),
            Condition::Logic(data) => data.to_mathml(ctx),
        }
    }
}

impl MathMLRenderer for ArithmeticCondition {
    fn to_mathml(&self, ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let mut tag = MRowData::new();
        if self.should_wrap_left_with_parentheses() {
            tag.children.push(MOData::from_string("(").to_tag());
            tag.children.push(self.left.to_mathml(ctx)?);
            tag.children.push(MOData::from_string(")").to_tag());
        } else {
            tag.children.push(self.left.to_mathml(ctx)?);
        };

        tag.children.push(self.operator.to_mathml(ctx)?);

        if self.should_wrap_right_with_parentheses() {
            tag.children.push(MOData::from_string("(").to_tag());
            tag.children.push(self.right.to_mathml(ctx)?);
            tag.children.push(MOData::from_string(")").to_tag());
        } else {
            tag.children.push(self.right.to_mathml(ctx)?);
        };
        Ok(tag.to_tag())
    }
}

impl MathMLRenderer for ArithmeticOperator {
    fn to_mathml(&self, _ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let code = match self {
            ArithmeticOperator::Equal => "=",
            ArithmeticOperator::NotEqual => "&#x2260;",
            ArithmeticOperator::Greater => "&gt;",
            ArithmeticOperator::GreaterOrEqual => "&#x2265;",
            ArithmeticOperator::Less => "&lt;",
            ArithmeticOperator::LessOrEqual => "&#x2264;",
        };
        let tag = MOData::new(MValue::Raw(code.to_owned())).to_tag();
        Ok(tag)
    }
}

impl MathMLRenderer for LogicCondition {
    fn to_mathml(&self, ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let mut tag = MRowData::new();
        if self.should_wrap_left_with_parentheses() {
            tag.children.push(MOData::from_string("(").to_tag());
            tag.children.push(self.left.to_mathml(ctx)?);
            tag.children.push(MOData::from_string(")").to_tag());
        } else {
            tag.children.push(self.left.to_mathml(ctx)?);
        };
        tag.children.push(self.operator.to_mathml(ctx)?);
        if self.should_wrap_right_with_parentheses() {
            tag.children.push(MOData::from_string("(").to_tag());
            tag.children.push(self.right.to_mathml(ctx)?);
            tag.children.push(MOData::from_string(")").to_tag());
        } else {
            tag.children.push(self.right.to_mathml(ctx)?);
        };
        Ok(tag.to_tag())
    }
}

impl MathMLRenderer for LogicOperator {
    fn to_mathml(&self, _ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let text = match self {
            LogicOperator::And => "and",
            LogicOperator::Or => "or",
        };
        let tag = MTextData::from_string(text).to_tag();
        Ok(tag)
    }
}

#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::expressions::arithmetic_condition::ArithmeticCondition;
    use matm_model::expressions::arithmetic_operator::ArithmeticOperator;
    use matm_model::expressions::conditional::ConditionalData;
    use matm_model::expressions::conditional::ConditionalClause;
    use matm_model::expressions::logic_condition::LogicCondition;
    use matm_model::expressions::logic_operator::LogicOperator;
    use matm_model::expressions::condition::ToCondition;
    use matm_model::expressions::ToExpression;
    use matm_model::factories::ObjFactory;
    use rstest::rstest;

    #[rstest]
    #[case(ArithmeticOperator::Equal, "<mo>=</mo>")]
    #[case(ArithmeticOperator::NotEqual, "<mo>&#x2260;</mo>")]
    #[case(ArithmeticOperator::Greater, "<mo>&gt;</mo>")]
    #[case(ArithmeticOperator::GreaterOrEqual, "<mo>&#x2265;</mo>")]
    #[case(ArithmeticOperator::Less, "<mo>&lt;</mo>")]
    #[case(ArithmeticOperator::LessOrEqual, "<mo>&#x2264;</mo>")]
    fn test_render_arithmetic_operator_to_mathml(
        #[case] op: ArithmeticOperator,
        #[case] expected: &str,
    ) {
        let ctx = MathMLContext {};
        let tag = op.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        assert_eq!(expected, output);
    }

    #[test]
    fn test_render_arithmetic_condition_to_mathml() {
        let fa = ObjFactory::new10();
        let data = ArithmeticCondition::gt(fa.rsn("x"), fa.rz0());

        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mrow>",
            "<mi>x</mi>",
            "<mo>&gt;</mo>",
            "<mn>0</mn>",
            "</mrow>",
        ].join("");
        assert_eq!(expected, output);
    }

    #[rstest]
    #[case(LogicOperator::And, "<mtext>and</mtext>")]
    #[case(LogicOperator::Or, "<mtext>or</mtext>")]
    fn test_render_logic_operator_to_mathml(
        #[case] op: LogicOperator,
        #[case] expected: &str,
    ) {
        let ctx = MathMLContext {};
        let tag = op.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        assert_eq!(expected, output);
    }

    #[test]
    fn test_render_logic_condition_to_mathml() {
        let fa = ObjFactory::new10();
        let data = LogicCondition::and(
            ArithmeticCondition::gt(fa.rsn("x"), fa.rz0()).to_rc_cond(),
            ArithmeticCondition::lt(fa.rsn("x"), fa.rzi_u(10)).to_rc_cond()
        );

        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mrow>",
            "<mrow>",
            "<mi>x</mi>",
            "<mo>&gt;</mo>",
            "<mn>0</mn>",
            "</mrow>",
            "<mtext>and</mtext>",
            "<mrow>",
            "<mi>x</mi>",
            "<mo>&lt;</mo>",
            "<mn>10</mn>",
            "</mrow>",
            "</mrow>",
        ].join("");
        assert_eq!(expected, output);
    }

    #[test]
    fn test_render_logic_condition_to_mathml_groups() {
        let fa = ObjFactory::new10();
        let data = LogicCondition::and(
            LogicCondition::or(
                ArithmeticCondition::gt(fa.rsn("x"), fa.rz0()).to_rc_cond(),
                ArithmeticCondition::lt(fa.rsn("x"), fa.rzi_u(10)).to_rc_cond()
            ).to_rc_cond(),
            LogicCondition::or(
                ArithmeticCondition::gt(fa.rsn("y"), fa.rz0()).to_rc_cond(),
                ArithmeticCondition::lt(fa.rsn("y"), fa.rzi_u(10)).to_rc_cond()
            ).to_rc_cond()
        );

        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mrow>",
            "<mo>(</mo>",
            "<mrow>",
            "<mrow>",
            "<mi>x</mi>",
            "<mo>&gt;</mo>",
            "<mn>0</mn>",
            "</mrow>",
            "<mtext>or</mtext>",
            "<mrow>",
            "<mi>x</mi>",
            "<mo>&lt;</mo>",
            "<mn>10</mn>",
            "</mrow>",
            "</mrow>",
            "<mo>)</mo>",
            "<mtext>and</mtext>",
            "<mo>(</mo>",
            "<mrow>",
            "<mrow>",
            "<mi>y</mi>",
            "<mo>&gt;</mo>",
            "<mn>0</mn>",
            "</mrow>",
            "<mtext>or</mtext>",
            "<mrow>",
            "<mi>y</mi>",
            "<mo>&lt;</mo>",
            "<mn>10</mn>",
            "</mrow>",
            "</mrow>",
            "<mo>)</mo>",
            "</mrow>",
        ].join("");
        assert_eq!(expected, output);
    }

    #[test]
    fn test_render_conditional_to_mathml() {
        let fa = ObjFactory::new10();
        let data = ConditionalData::from_vector(
            vec![
                ConditionalClause {
                    expression: fa.rzi_u(1),
                    condition: ArithmeticCondition::gt(fa.rsn("x"), fa.rz0()).to_rc_cond(),
                },
                ConditionalClause {
                    expression: fa.rzi_u(-1),
                    condition: ArithmeticCondition::lt(fa.rsn("x"), fa.rz0()).to_rc_cond(),
                },
            ]).to_rc_expr();

        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mrow>",
            "<mo>{</mo>",
            "<mtable>",
            "<mtr>",
            "<mtd><mn>1</mn></mtd>",
            "<mtd><mrow><mi>x</mi><mo>&gt;</mo><mn>0</mn></mrow></mtd>",
            "</mtr>",
            "<mtr>",
            "<mtd><mrow><mo form=\"prefix\">&minus;</mo><mn>1</mn></mrow></mtd>",
            "<mtd><mrow><mi>x</mi><mo>&lt;</mo><mn>0</mn></mrow></mtd>",
            "</mtr>",
            "</mtable>",
            "</mrow>",
        ].join("");
        assert_eq!(expected, output);
    }
}
