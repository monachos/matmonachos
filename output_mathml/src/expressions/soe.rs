use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::mo::MOData;
use mathml_model::mrow::MRowData;
use mathml_model::mtable::MTableData;
use mathml_model::mtd::MTdData;
use mathml_model::mtr::MTrData;
use mathml_model::MTag;
use mathml_model::ToMTag;
use matm_error::error::CalcError;
use matm_model::expressions::soe::SysOfEqData;
use std::rc::Rc;

impl MathMLRenderer for SysOfEqData {
    fn to_mathml(&self, ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let mut tag = MRowData::new();
        tag.children.push(MOData::from_string("{").to_tag());
        let mut table = MTableData::new();
        for eq in &self.equations {
            let content = eq.as_ref().to_mathml(ctx)?;
            let row = MTrData::from_vector(vec![
                MTdData::new(content).to_rc()
            ]);
            table.rows.push(row.to_rc());
        }
        tag.children.push(table.to_tag());
        Ok(tag.to_tag())
    }
}

#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::core::natural::RADIX_10;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::equation::EquationData;
    use matm_model::expressions::int::IntData;
    use matm_model::expressions::soe::SysOfEqData;
    use matm_model::expressions::symbol::SymbolData;
    use matm_model::expressions::ToExpression;

    #[test]
    fn test_render_to_mathml_x_eq_bin() {
        let data = SysOfEqData::from_vector(vec![
            EquationData::new(
                SymbolData::new("x").to_rc_expr(),
                BinaryData::add(
                    IntData::from_int(RADIX_10, 1).unwrap().to_rc_expr(),
                    IntData::from_int(RADIX_10, 2).unwrap().to_rc_expr()
                ).to_rc_expr()
            ).to_rc_expr(),
            EquationData::new(
                SymbolData::new("y").to_rc_expr(),
                BinaryData::add(
                    IntData::from_int(RADIX_10, 3).unwrap().to_rc_expr(),
                    IntData::from_int(RADIX_10, 4).unwrap().to_rc_expr()
                ).to_rc_expr()
            ).to_rc_expr(),
        ]).to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mrow>",
            "<mo>{</mo>",
            "<mtable>",
            "<mtr>",
            "<mtd>",
            "<mrow>",
            "<mi>x</mi>",
            "<mo>=</mo>",
            "<mrow>",
            "<mn>1</mn>",
            "<mo>+</mo>",
            "<mn>2</mn>",
            "</mrow>",
            "</mrow>",
            "</mtd>",
            "</mtr>",
            "<mtr>",
            "<mtd>",
            "<mrow>",
            "<mi>y</mi>",
            "<mo>=</mo>",
            "<mrow>",
            "<mn>3</mn>",
            "<mo>+</mo>",
            "<mn>4</mn>",
            "</mrow>",
            "</mrow>",
            "</mtd>",
            "</mtr>",
            "</mtable>",
            "</mrow>",
        ].join("");
        assert_eq!(expected, output);
    }
}
