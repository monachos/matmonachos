use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::mo::MOData;
use mathml_model::mrow::MRowData;
use mathml_model::munderover::MUnderOverData;
use mathml_model::MTag;
use mathml_model::MValue;
use mathml_model::ToMTag;
use matm_error::error::CalcError;
use matm_model::expressions::iterated_binary::IteratedBinaryData;
use matm_model::expressions::iterated_binary::IteratedBinaryOperator;
use std::rc::Rc;

impl MathMLRenderer for IteratedBinaryData {
    fn to_mathml(&self, ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let mut tag = MRowData::new();

        let lower_cond = MRowData::from_vector(vec![
            self.iterator.to_mathml(ctx)?,
            MOData::from_string("=").to_tag(),
            self.lower_bound.to_mathml(ctx)?,
        ]).to_tag();
        let upper_cond = self.upper_bound.to_mathml(ctx)?;

        tag.children.push(MUnderOverData::new(
            self.operator.to_mathml(ctx)?,
            lower_cond,
            upper_cond
        ).to_tag());
        tag.children.push(self.value.to_mathml(ctx)?);

        Ok(tag.to_tag())
    }
}

impl MathMLRenderer for IteratedBinaryOperator {
    fn to_mathml(&self, _ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let code = match self {
            IteratedBinaryOperator::Sum => "&#x2211;",
            IteratedBinaryOperator::Product => "&#x220f;",
        };
        let tag = MOData::new(MValue::Raw(code.to_owned())).to_tag();
        Ok(tag)
    }
}

#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::expressions::iterated_binary::IteratedBinaryData;
    use matm_model::expressions::ToExpression;
    use matm_model::factories::ObjFactory;

    #[test]
    fn test_render_sum_to_mathml() {
        let fa = ObjFactory::new10();
        let data = IteratedBinaryData::sum(
            fa.sn("k"),
            fa.rzi_u(1),
            fa.rzi_u(5),
            fa.rsn("x")
        ).to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mrow>",
            "<munderover>",
            "<mo>&#x2211;</mo>",
            "<mrow><mi>k</mi><mo>=</mo><mn>1</mn></mrow>",
            "<mn>5</mn>",
            "</munderover>",
            "<mi>x</mi>",
            "</mrow>",
        ].join("");
        assert_eq!(expected, output);
    }

    #[test]
    fn test_render_product_to_mathml() {
        let fa = ObjFactory::new10();
        let data = IteratedBinaryData::product(
            fa.sn("k"),
            fa.rzi_u(1),
            fa.rzi_u(5),
            fa.rsn("x")
        ).to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mrow>",
            "<munderover>",
            "<mo>&#x220f;</mo>",
            "<mrow><mi>k</mi><mo>=</mo><mn>1</mn></mrow>",
            "<mn>5</mn>",
            "</munderover>",
            "<mi>x</mi>",
            "</mrow>",
        ].join("");
        assert_eq!(expected, output);
    }
}
