use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::mfrac::MFracData;
use mathml_model::MTag;
use mathml_model::ToMTag;
use matm_error::error::CalcError;
use matm_model::expressions::fractions::FracData;
use std::rc::Rc;

impl MathMLRenderer for FracData {
    fn to_mathml(&self, ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let tag = MFracData::new(
            self.numerator.to_mathml(ctx)?,
            self.denominator.to_mathml(ctx)?,
        ).to_tag();
        Ok(tag)
    }
}

#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::core::natural::RADIX_10;
    use matm_model::expressions::fractions::FracData;
    use matm_model::expressions::int::IntData;
    use matm_model::expressions::ToExpression;

    #[test]
    fn test_render_to_mathml_positive() {
        let data = FracData::new(
            IntData::from_int(RADIX_10, 2).unwrap().to_rc_expr(),
            IntData::from_int(RADIX_10, 3).unwrap().to_rc_expr()
        ).to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mfrac>",
            "<mn>2</mn>",
            "<mn>3</mn>",
            "</mfrac>",
        ].join("");
        assert_eq!(expected, output);
    }

    #[test]
    fn test_render_to_mathml_negative() {
        let data = FracData::new(
            IntData::from_int(RADIX_10, -2).unwrap().to_rc_expr(),
            IntData::from_int(RADIX_10, 3).unwrap().to_rc_expr()
        ).to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mfrac>",
            "<mrow><mo form=\"prefix\">&minus;</mo><mn>2</mn></mrow>",
            "<mn>3</mn>",
            "</mfrac>",
        ].join("");
        assert_eq!(expected, output);
    }
}
