use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::MTag;
use matm_error::error::CalcError;
use matm_model::expressions::variable::VariableData;
use std::rc::Rc;

impl MathMLRenderer for VariableData {
    fn to_mathml(&self, ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        self.name.to_mathml(ctx)
    }
}

#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::core::natural::RADIX_10;
    use matm_model::expressions::int::IntData;
    use matm_model::expressions::symbol::SymbolData;
    use matm_model::expressions::variable::VariableData;
    use matm_model::expressions::ToExpression;

    #[test]
    fn test_render_to_mathml_positive() {
        let data = VariableData::new(
            SymbolData::new("x"),
            IntData::from_int(RADIX_10, 0).unwrap().to_rc_expr()
        ).to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        assert_eq!("<mi>x</mi>", output);
    }
}
