use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::mi::MIData;
use mathml_model::mo::MOData;
use mathml_model::mrow::MRowData;
use mathml_model::MTag;
use mathml_model::ToMTag;
use matm_error::error::CalcError;
use matm_model::expressions::object::ObjectData;
use std::rc::Rc;

impl MathMLRenderer for ObjectData {
    fn to_mathml(&self, _ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let tag = MRowData::from_vector(vec![
            MOData::from_string("{").to_tag(),
            MIData::from_string(&self.object_type.to_string()).to_tag(),
            MOData::from_string("}").to_tag(),
        ]).to_tag();
        Ok(tag)
    }
}

#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::expressions::object::ObjectData;
    use matm_model::expressions::object::ObjectType;
    use matm_model::expressions::ToExpression;

    #[test]
    fn test_render_to_mathml_matrix() {
        let data = ObjectData::new(ObjectType::Matrix).to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mrow>",
            "<mo>{</mo>",
            "<mi>matrix</mi>",
            "<mo>}</mo>",
            "</mrow>",
        ].join("");
        assert_eq!(expected, output);
    }
}