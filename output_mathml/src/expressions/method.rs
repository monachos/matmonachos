use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::mo::MOData;
use mathml_model::mrow::MRowData;
use mathml_model::MTag;
use mathml_model::ToMTag;
use matm_error::error::CalcError;
use matm_model::expressions::method::MethodData;
use std::rc::Rc;

impl MathMLRenderer for MethodData {
    fn to_mathml(&self, ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let mut tag = MRowData::new();

        if self.should_wrap_object_with_parentheses() {
            tag.children.push(MOData::from_string("(").to_tag());
            tag.children.push(self.object.to_mathml(ctx)?);
            tag.children.push(MOData::from_string(")").to_tag());
        } else {
            tag.children.push(self.object.to_mathml(ctx)?);
        };

        tag.children.push(MOData::from_string(".").to_tag());
        tag.children.push(self.func.to_mathml(ctx)?);

        Ok(tag.to_tag())
    }
}

#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::core::natural::RADIX_10;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::int::IntData;
    use matm_model::expressions::method::MethodData;
    use matm_model::expressions::symbol::SymbolData;
    use matm_model::expressions::ToExpression;

    #[test]
    fn test_render_to_mathml_x_met_0() {
        let data = MethodData::new(
            SymbolData::new("x").to_rc_expr(),
            "met".to_owned(),
            vec![
                IntData::from_int(RADIX_10, 0).unwrap().to_rc_expr()
            ]
        ).to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mrow>",
            "<mi>x</mi>",
            "<mo>.</mo>",
            "<mrow>",
            "<mi>met</mi>",
            "<mo>(</mo>",
            "<mn>0</mn>",
            "<mo>)</mo>",
            "</mrow>",
            "</mrow>",
        ].join("");
        assert_eq!(expected, output);
    }

    #[test]
    fn test_render_to_mathml_bin_met_0() {
        let data = MethodData::new(
            BinaryData::add(
                SymbolData::new("x").to_rc_expr(),
                IntData::from_int(RADIX_10, 1).unwrap().to_rc_expr(),
            ).to_rc_expr(),
            "met".to_owned(),
            vec![
                IntData::from_int(RADIX_10, 0).unwrap().to_rc_expr()
            ]
        ).to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mrow>",
            "<mo>(</mo>",
            "<mrow>",
            "<mi>x</mi>",
            "<mo>+</mo>",
            "<mn>1</mn>",
            "</mrow>",
            "<mo>)</mo>",
            "<mo>.</mo>",
            "<mrow>",
            "<mi>met</mi>",
            "<mo>(</mo>",
            "<mn>0</mn>",
            "<mo>)</mo>",
            "</mrow>",
            "</mrow>",
        ].join("");
        assert_eq!(expected, output);
    }
}