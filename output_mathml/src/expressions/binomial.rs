use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::mfrac::MFracData;
use mathml_model::mo::MOData;
use mathml_model::mrow::MRowData;
use mathml_model::MTag;
use mathml_model::ToMTag;
use matm_error::error::CalcError;
use matm_model::expressions::binomial::BinomialData;
use std::rc::Rc;

impl MathMLRenderer for BinomialData {
    fn to_mathml(&self, ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let mut tag = MRowData::new();
        tag.children.push(MOData::from_string("(").to_tag());
        let mut frac = MFracData::new(
            self.set.to_mathml(ctx)?,
            self.subset.to_mathml(ctx)?
        );
        frac.line_thickness = Some(0);
        tag.children.push(frac.to_tag());
        tag.children.push(MOData::from_string(")").to_tag());
        Ok(tag.to_tag())
    }
}

#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::core::natural::RADIX_10;
    use matm_model::expressions::binomial::BinomialData;
    use matm_model::expressions::int::IntData;
    use matm_model::expressions::ToExpression;

    #[test]
    fn test_render_to_mathml() {
        let data = BinomialData::new(
            IntData::from_int(RADIX_10, 2).unwrap().to_rc_expr(),
            IntData::from_int(RADIX_10, 3).unwrap().to_rc_expr()
        ).to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mrow>",
            "<mo>(</mo>",
            "<mfrac linethickness=\"0\">",
            "<mn>2</mn>",
            "<mn>3</mn>",
            "</mfrac>",
            "<mo>)</mo>",
            "</mrow>",
        ].join("");
        assert_eq!(expected, output);
    }
}
