use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::mroot::MRootData;
use mathml_model::msqrt::MSqrtData;
use mathml_model::MTag;
use mathml_model::ToMTag;
use matm_error::error::CalcError;
use matm_model::expressions::root::RootData;
use std::rc::Rc;

impl MathMLRenderer for RootData {
    fn to_mathml(&self, ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let tag = if self.degree.is_int_2() {
            MSqrtData::new(self.radicand.to_mathml(ctx)?).to_tag()
        } else {
            MRootData::new(
                self.radicand.to_mathml(ctx)?,
                self.degree.to_mathml(ctx)?
            ).to_tag()
        };
        Ok(tag)
    }
}


#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::core::natural::RADIX_10;
    use matm_model::expressions::int::IntData;
    use matm_model::expressions::root::RootData;
    use matm_model::expressions::symbol::SymbolData;
    use matm_model::expressions::ToExpression;

    #[test]
    fn test_render_to_mathml_sqrt_x() {
        let data = RootData::new(
            SymbolData::new("x").to_rc_expr(),
            IntData::from_int(RADIX_10, 2).unwrap().to_rc_expr()
        ).to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<msqrt>",
            "<mi>x</mi>",
            "</msqrt>",
        ].join("");
        assert_eq!(expected, output);
    }

    #[test]
    fn test_render_to_mathml_root_x_of_3() {
        let data = RootData::new(
            SymbolData::new("x").to_rc_expr(),
            IntData::from_int(RADIX_10, 3).unwrap().to_rc_expr()
        ).to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mroot>",
            "<mi>x</mi>",
            "<mn>3</mn>",
            "</mroot>",
        ].join("");
        assert_eq!(expected, output);
    }
}
