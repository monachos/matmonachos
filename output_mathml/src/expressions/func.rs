use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::mi::MIData;
use mathml_model::mo::MOData;
use mathml_model::mrow::MRowData;
use mathml_model::mtext::MTextData;
use mathml_model::{MTag, MValue};
use mathml_model::ToMTag;
use matm_error::error::CalcError;
use matm_model::expressions::func::FunctionData;
use std::rc::Rc;
use mathml_model::mspace::MSpaceData;

impl MathMLRenderer for FunctionData {
    fn to_mathml(&self, ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let mut tag = MRowData::new();

        let no_parenthesis = self.can_have_no_parenthesis();

        tag.children.push(MIData::from_string(&self.name).to_tag());

        if no_parenthesis {
            tag.children.push(MOData::new(MValue::Raw("&#2061;".to_owned())).to_tag());
        } else {
            tag.children.push(MOData::from_string("(").to_tag());
        }

        let mut need_sep = false;
        for arg in &self.arguments {
            if need_sep {
                tag.children.push(MTextData::from_string(",").to_tag());
                tag.children.push(MSpaceData::with_width(".3em").to_tag());
            }
            tag.children.push(arg.to_mathml(ctx)?);
            need_sep = true;
        }

        if !no_parenthesis {
            tag.children.push(MOData::from_string(")").to_tag());
        }

        Ok(tag.to_tag())
    }
}


#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::core::natural::RADIX_10;
    use matm_model::expressions::func::FunctionData;
    use matm_model::expressions::int::IntData;
    use matm_model::expressions::ToExpression;

    #[test]
    fn test_render_to_mathml_sin_0() {
        let data = FunctionData::new(
            FunctionData::SIN.to_owned(),
            vec![
                IntData::from_int(RADIX_10, 0).unwrap().to_rc_expr()
            ]
        ).to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mrow>",
            "<mi>sin</mi>",
            "<mo>&#2061;</mo>",
            "<mn>0</mn>",
            "</mrow>",
        ].join("");
        assert_eq!(expected, output);
    }

    #[test]
    fn test_render_to_mathml_custom_0() {
        let data = FunctionData::new(
            "custom".to_owned(),
            vec![
                IntData::from_int(RADIX_10, 0).unwrap().to_rc_expr()
            ]
        ).to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mrow>",
            "<mi>custom</mi>",
            "<mo>(</mo>",
            "<mn>0</mn>",
            "<mo>)</mo>",
            "</mrow>",
        ].join("");
        assert_eq!(expected, output);
    }

    #[test]
    fn test_render_to_mathml_custom_many_params() {
        let data = FunctionData::new(
            "custom".to_owned(),
            vec![
                IntData::from_int(RADIX_10, 1).unwrap().to_rc_expr(),
                IntData::from_int(RADIX_10, 2).unwrap().to_rc_expr(),
                IntData::from_int(RADIX_10, 3).unwrap().to_rc_expr(),
            ]
        ).to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        println!("{}", output);
        let expected = vec![
            "<mrow>",
            "<mi>custom</mi>",
            "<mo>(</mo>",
            "<mn>1</mn>",
            "<mtext>,</mtext>",
            "<mspace width=\".3em\"/>",
            "<mn>2</mn>",
            "<mtext>,</mtext>",
            "<mspace width=\".3em\"/>",
            "<mn>3</mn>",
            "<mo>)</mo>",
            "</mrow>",
        ].join("");
        assert_eq!(expected, output);
    }
}