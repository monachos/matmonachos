use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::mo::MOData;
use mathml_model::mrow::MRowData;
use mathml_model::mtable::MTableData;
use mathml_model::mtd::MTdData;
use mathml_model::mtr::MTrData;
use mathml_model::MTag;
use mathml_model::ToMTag;
use matm_error::error::CalcError;
use matm_model::expressions::cracovians::CracovianData;
use std::rc::Rc;

impl MathMLRenderer for CracovianData {
    fn to_mathml(&self, ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let mut tag = MRowData::new();
        if self.grid.size.dimensions.len() == 2 {
            tag.children.push(MOData::from_string("{").to_tag());
            let mut table = MTableData::new();
            for c in self.iter_columns() {
                let mut row = MTrData::new();
                for r in self.iter_rows() {
                    let item = self.get_i_i(r, c)?;
                    let content = item.to_mathml(ctx)?;
                    row.children.push(MTdData::new(content).to_rc());
                }
                table.rows.push(row.to_rc());
            }
            tag.children.push(table.to_tag());
            tag.children.push(MOData::from_string("}").to_tag());
        }
        Ok(tag.to_tag())
    }
}

#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::core::natural::RADIX_10;
    use matm_model::expressions::cracovians::CracovianData;
    use matm_model::expressions::ToExpression;

    #[test]
    fn test_render_to_mathml_2x2() {
        let data = CracovianData::from_ivector(
            2, 2, RADIX_10,
            vec![
                1, 2,
                3, 4,
            ]).unwrap().to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mrow>",
            "<mo>{</mo>",
            "<mtable>",
            "<mtr>",
            "<mtd><mn>1</mn></mtd>",
            "<mtd><mn>2</mn></mtd>",
            "</mtr>",
            "<mtr>",
            "<mtd><mn>3</mn></mtd>",
            "<mtd><mn>4</mn></mtd>",
            "</mtr>",
            "</mtable>",
            "<mo>}</mo>",
            "</mrow>",
        ].join("");
        assert_eq!(expected, output);
    }
}
