use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::MTag;
use mathml_model::ToMTag;
use matm_error::error::CalcError;
use matm_model::expressions::exponentiation::ExpData;
use std::rc::Rc;
use mathml_model::mo::MOData;
use mathml_model::mrow::MRowData;
use mathml_model::msup::MSupData;

impl MathMLRenderer for ExpData {
    fn to_mathml(&self, ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let base = if self.should_wrap_base_with_parentheses() {
            MRowData::from_vector(vec![
                MOData::from_string("(").to_tag(),
                self.base.to_mathml(ctx)?,
                MOData::from_string(")").to_tag(),
            ]).to_tag()
        } else {
            self.base.to_mathml(ctx)?
        };

        let superscript = if self.should_wrap_exponent_with_parentheses() {
            MRowData::from_vector(vec![
                MOData::from_string("(").to_tag(),
                self.exponent.to_mathml(ctx)?,
                MOData::from_string(")").to_tag(),
            ]).to_tag()
        } else {
            self.exponent.to_mathml(ctx)?
        };

        let tag = MSupData::new(base, superscript).to_tag();
        Ok(tag)
    }
}


#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::core::natural::RADIX_10;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::exponentiation::ExpData;
    use matm_model::expressions::int::IntData;
    use matm_model::expressions::symbol::SymbolData;
    use matm_model::expressions::ToExpression;

    #[test]
    fn test_render_to_mathml_x_2() {
        let data = ExpData::new(
            SymbolData::new("x").to_rc_expr(),
            IntData::from_int(RADIX_10, 2).unwrap().to_rc_expr()
        ).to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<msup>",
            "<mi>x</mi>",
            "<mn>2</mn>",
            "</msup>",
        ].join("");
        assert_eq!(expected, output);
    }

    #[test]
    fn test_render_to_mathml_bin_bin() {
        let data = ExpData::new(
            BinaryData::add(
                SymbolData::new("x").to_rc_expr(),
                IntData::from_int(RADIX_10, 1).unwrap().to_rc_expr(),
            ).to_rc_expr(),
            BinaryData::add(
                SymbolData::new("y").to_rc_expr(),
                IntData::from_int(RADIX_10, 5).unwrap().to_rc_expr(),
            ).to_rc_expr(),
        ).to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<msup>",
            "<mrow>",
            "<mo>(</mo>",
            "<mrow>",
            "<mi>x</mi>",
            "<mo>+</mo>",
            "<mn>1</mn>",
            "</mrow>",
            "<mo>)</mo>",
            "</mrow>",

            "<mrow>",
            "<mo>(</mo>",
            "<mrow>",
            "<mi>y</mi>",
            "<mo>+</mo>",
            "<mn>5</mn>",
            "</mrow>",
            "<mo>)</mo>",
            "</mrow>",

            "</msup>",
        ].join("");
        assert_eq!(expected, output);
    }
}
