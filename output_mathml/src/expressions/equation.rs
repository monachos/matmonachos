use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::mo::MOData;
use mathml_model::mrow::MRowData;
use mathml_model::MTag;
use mathml_model::ToMTag;
use matm_error::error::CalcError;
use matm_model::expressions::equation::EquationData;
use std::rc::Rc;

impl MathMLRenderer for EquationData {
    fn to_mathml(&self, ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let tag = MRowData::from_vector(vec![
            self.left.to_mathml(ctx)?,
            MOData::from_string("=").to_tag(),
            self.right.to_mathml(ctx)?,
        ]).to_tag();
        Ok(tag)
    }
}

#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::core::natural::RADIX_10;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::equation::EquationData;
    use matm_model::expressions::int::IntData;
    use matm_model::expressions::symbol::SymbolData;
    use matm_model::expressions::ToExpression;

    #[test]
    fn test_render_to_mathml_x_eq_bin() {
        let data = EquationData::new(
            SymbolData::new("x").to_rc_expr(),
            BinaryData::add(
                IntData::from_int(RADIX_10, 1).unwrap().to_rc_expr(),
                IntData::from_int(RADIX_10, 2).unwrap().to_rc_expr()
            ).to_rc_expr()
        ).to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mrow>",
            "<mi>x</mi>",
            "<mo>=</mo>",
            "<mrow>",
            "<mn>1</mn>",
            "<mo>+</mo>",
            "<mn>2</mn>",
            "</mrow>",
            "</mrow>",
        ].join("");
        assert_eq!(expected, output);
    }
}
