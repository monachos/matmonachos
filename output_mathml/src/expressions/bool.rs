use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::mtext::MTextData;
use mathml_model::MTag;
use mathml_model::ToMTag;
use matm_error::error::CalcError;
use matm_model::expressions::bool::BoolData;
use std::rc::Rc;


impl MathMLRenderer for BoolData {
    fn to_mathml(&self, _ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let tag = MTextData::from_string(&self.value().to_string()).to_tag();
        Ok(tag)
    }
}

#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::expressions::bool::BoolData;
    use matm_model::expressions::ToExpression;

    #[test]
    fn test_render_to_mathml() {
        let data = BoolData::new(true).to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        assert_eq!("<mtext>true</mtext>", output);
    }
}
