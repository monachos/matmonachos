use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::form::MForm;
use mathml_model::mo::MOData;
use mathml_model::mrow::MRowData;
use mathml_model::MValue;
use mathml_model::MTag;
use mathml_model::ToMTag;
use matm_error::error::CalcError;
use matm_model::expressions::int::IntData;
use std::rc::Rc;

impl MathMLRenderer for IntData {
    fn to_mathml(&self, ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let num = self.natural().to_mathml(ctx)?;
        if self.is_negative() {
            let mut mo = MOData::new(MValue::Raw("&minus;".to_owned()));
            mo.form = Some(MForm::Prefix);
            Ok(MRowData::from_vector(vec![
                mo.to_tag(),
                num
            ]).to_tag())
        } else {
            Ok(num)
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::core::natural::RADIX_10;
    use matm_model::expressions::int::IntData;
    use matm_model::expressions::ToExpression;

    #[test]
    fn test_render_to_mathml_positive() {
        let data = IntData::from_int(RADIX_10, 15).unwrap().to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        assert_eq!("<mn>15</mn>", output);
    }

    #[test]
    fn test_render_to_mathml_negative() {
        let data = IntData::from_int(RADIX_10, -15).unwrap().to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mrow>",
            "<mo form=\"prefix\">&minus;</mo>",
            "<mn>15</mn>",
            "</mrow>",
        ].join("");
        assert_eq!(expected, output);
    }
}
