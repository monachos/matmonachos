use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::mo::MOData;
use mathml_model::mrow::MRowData;
use mathml_model::MTag;
use mathml_model::ToMTag;
use matm_error::error::CalcError;
use matm_model::expressions::assignment::AssignmentData;
use std::rc::Rc;

impl MathMLRenderer for AssignmentData {
    fn to_mathml(&self, ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let tag = MRowData::from_vector(vec![
            self.target.to_mathml(ctx)?,
            MOData::from_string(":").to_tag(),
            self.source.to_mathml(ctx)?,
        ]).to_tag();
        Ok(tag)
    }
}

#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::core::natural::RADIX_10;
    use matm_model::expressions::assignment::AssignmentData;
    use matm_model::expressions::int::IntData;
    use matm_model::expressions::symbol::SymbolData;
    use matm_model::expressions::ToExpression;

    #[test]
    fn test_render_to_mathml_x_10() {
        let data = AssignmentData::new(
            SymbolData::new("x").to_rc_expr(),
            IntData::from_int(RADIX_10, 10).unwrap().to_rc_expr(),
        ).to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mrow>",
            "<mi>x</mi>",
            "<mo>:</mo>",
            "<mn>10</mn>",
            "</mrow>",
        ].join("");
        assert_eq!(expected, output);
    }
}
