use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::form::MForm;
use mathml_model::mi::MIData;
use mathml_model::mo::MOData;
use mathml_model::mrow::MRowData;
use mathml_model::msup::MSupData;
use mathml_model::MValue;
use mathml_model::MTag;
use mathml_model::ToMTag;
use matm_error::error::CalcError;
use matm_model::expressions::precedence::OperatorPrecAccess;
use matm_model::expressions::unary::UnaryData;
use matm_model::expressions::unary::UnaryOperator;
use std::rc::Rc;

impl MathMLRenderer for UnaryData {
    fn to_mathml(&self, ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        match self.operator {
            UnaryOperator::Group => render_unary_group_to_mathml(self, ctx),
            UnaryOperator::Neg => render_unary_neg_to_mathml(self, ctx),
            UnaryOperator::Transpose => render_unary_transpose_to_mathml(self, ctx),
            UnaryOperator::Determinant => render_unary_det_to_mathml(self, ctx),
            UnaryOperator::Rank => render_unary_rank_to_mathml(self, ctx),
            UnaryOperator::Inversion => render_unary_inv_to_mathml(self, ctx),
            UnaryOperator::Factorial => render_unary_factorial_to_mathml(self, ctx),
        }
    }
}

fn render_unary_group_to_mathml(
    data: &UnaryData,
    ctx: &MathMLContext,
) -> Result<Rc<MTag>, CalcError> {
    let tag = MRowData::from_vector(vec![
        MOData::from_string("(").to_tag(),
        data.value.to_mathml(ctx)?,
        MOData::from_string(")").to_tag(),
    ]).to_tag();
    Ok(tag)
}

fn render_unary_neg_to_mathml(
    data: &UnaryData,
    ctx: &MathMLContext,
) -> Result<Rc<MTag>, CalcError> {
    let mut minus = MOData::new(MValue::Raw("&minus;".to_owned()));
    minus.form = Some(MForm::Prefix);
    let tag = MRowData::from_vector(vec![
        minus.to_tag(),
        data.value.to_mathml(ctx)?,
    ]).to_tag();
    Ok(tag)
}

fn render_unary_transpose_to_mathml(
    data: &UnaryData,
    ctx: &MathMLContext,
) -> Result<Rc<MTag>, CalcError> {
    let tag = MSupData::new(
        data.value.to_mathml(ctx)?,
        MIData::from_string("T").to_tag()
    ).to_tag();
    Ok(tag)
}

fn render_unary_det_to_mathml(
    data: &UnaryData,
    ctx: &MathMLContext,
) -> Result<Rc<MTag>, CalcError> {
    let tag = MRowData::from_vector(vec![
        MIData::from_string("det").to_tag(),
        MOData::from_string("(").to_tag(),
        data.value.to_mathml(ctx)?,
        MOData::from_string(")").to_tag(),
    ]).to_tag();
    Ok(tag)
}

fn render_unary_rank_to_mathml(
    data: &UnaryData,
    ctx: &MathMLContext,
) -> Result<Rc<MTag>, CalcError> {
    let tag = MRowData::from_vector(vec![
        MIData::from_string("rank").to_tag(),
        MOData::from_string("(").to_tag(),
        data.value.to_mathml(ctx)?,
        MOData::from_string(")").to_tag(),
    ]).to_tag();
    Ok(tag)
}

fn render_unary_inv_to_mathml(
    data: &UnaryData,
    ctx: &MathMLContext,
) -> Result<Rc<MTag>, CalcError> {
    let tag = MRowData::from_vector(vec![
        MIData::from_string("inv").to_tag(),
        MOData::from_string("(").to_tag(),
        data.value.to_mathml(ctx)?,
        MOData::from_string(")").to_tag(),
    ]).to_tag();
    Ok(tag)
}

fn render_unary_factorial_to_mathml(
    data: &UnaryData,
    ctx: &MathMLContext,
) -> Result<Rc<MTag>, CalcError> {
    let mut tag = MRowData::new();
    if data.value.operator_prec() < data.operator_prec() {
        tag.children.push(MOData::from_string("(").to_tag());
        tag.children.push(data.value.to_mathml(ctx)?);
        tag.children.push(MOData::from_string(")").to_tag());
    } else {
        tag.children.push(data.value.to_mathml(ctx)?);
    };
    tag.children.push(MOData::from_string("!").to_tag());
    Ok(tag.to_tag())
}


#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::core::natural::RADIX_10;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::matrices::MatrixData;
    use matm_model::expressions::symbol::SymbolData;
    use matm_model::expressions::unary::UnaryData;
    use matm_model::expressions::ToExpression;
    use matm_model::factories::ObjFactory;

    #[test]
    fn test_render_unary_group_to_mathml() {
        let fa = ObjFactory::new10();
        let data = UnaryData::group(fa.rzi_u(2)).to_rc_expr();

        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mrow>",
            "<mo>(</mo>",
            "<mn>2</mn>",
            "<mo>)</mo>",
            "</mrow>",
        ].join("");
        assert_eq!(expected, output);
    }

    #[test]
    fn test_render_unary_neg_to_mathml() {
        let fa = ObjFactory::new10();
        let data = UnaryData::neg(fa.rzi_u(2)).to_rc_expr();

        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mrow>",
            "<mo form=\"prefix\">&minus;</mo>",
            "<mn>2</mn>",
            "</mrow>",
        ].join("");
        assert_eq!(expected, output);
    }

    #[test]
    fn test_render_unary_transpose_to_mathml() {
        let data = UnaryData::transpose(
            MatrixData::identity(RADIX_10, 2).unwrap().to_rc_expr()
        ).to_rc_expr();

        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<msup>",
            "<mrow>",
            "<mo>(</mo>",
            "<mtable>",
            "<mtr>",
            "<mtd><mn>1</mn></mtd>",
            "<mtd><mn>0</mn></mtd>",
            "</mtr>",
            "<mtr>",
            "<mtd><mn>0</mn></mtd>",
            "<mtd><mn>1</mn></mtd>",
            "</mtr>",
            "</mtable>",
            "<mo>)</mo>",
            "</mrow>",
            "<mi>T</mi>",
            "</msup>",
        ].join("");
        assert_eq!(expected, output);
    }

    #[test]
    fn test_render_unary_det_to_mathml() {
        let data = UnaryData::det(
            SymbolData::new("A").to_rc_expr()
        ).to_rc_expr();

        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mrow>",
            "<mi>det</mi>",
            "<mo>(</mo>",
            "<mi>A</mi>",
            "<mo>)</mo>",
            "</mrow>",
        ].join("");
        assert_eq!(expected, output);
    }

    #[test]
    fn test_render_unary_rank_to_mathml() {
        let data = UnaryData::rank(
            SymbolData::new("A").to_rc_expr()
        ).to_rc_expr();

        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mrow>",
            "<mi>rank</mi>",
            "<mo>(</mo>",
            "<mi>A</mi>",
            "<mo>)</mo>",
            "</mrow>",
        ].join("");
        assert_eq!(expected, output);
    }

    #[test]
    fn test_render_unary_factorial_of_int_to_mathml() {
        let fa = ObjFactory::new10();
        let data = UnaryData::factorial(fa.rzi_u(2)).to_rc_expr();

        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mrow>",
            "<mn>2</mn>",
            "<mo>!</mo>",
            "</mrow>",
        ].join("");
        assert_eq!(expected, output);
    }

    #[test]
    fn test_render_unary_factorial_of_binary_to_mathml() {
        let fa = ObjFactory::new10();
        let data = UnaryData::factorial(
            BinaryData::add(fa.rzi_u(2), fa.rzi_u(3)).to_rc_expr()
        ).to_rc_expr();

        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mrow>",
            "<mo>(</mo>",
            "<mrow><mn>2</mn><mo>+</mo><mn>3</mn></mrow>",
            "<mo>)</mo>",
            "<mo>!</mo>",
            "</mrow>",
        ].join("");
        assert_eq!(expected, output);
    }
}
