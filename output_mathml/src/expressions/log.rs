use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::mi::MIData;
use mathml_model::mo::MOData;
use mathml_model::mrow::MRowData;
use mathml_model::msub::MSubData;
use mathml_model::MTag;
use mathml_model::ToMTag;
use matm_error::error::CalcError;
use matm_model::expressions::logarithm::LogData;
use std::rc::Rc;

impl MathMLRenderer for LogData {
    fn to_mathml(&self, ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let mut tag = MRowData::new();

        let base = MIData::from_string("log").to_tag();
        let subscript = self.base.to_mathml(ctx)?;
        tag.children.push(MSubData::new(base, subscript).to_tag());

        if self.should_wrap_value_with_parentheses() {
            tag.children.push(MOData::from_string("(").to_tag());
            tag.children.push(self.value.to_mathml(ctx)?);
            tag.children.push(MOData::from_string(")").to_tag());
        } else {
            tag.children.push(self.value.to_mathml(ctx)?);
        };
        Ok(tag.to_tag())
    }
}


#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::core::natural::RADIX_10;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::int::IntData;
    use matm_model::expressions::logarithm::LogData;
    use matm_model::expressions::transcendental::TranscData;
    use matm_model::expressions::ToExpression;

    #[test]
    fn test_render_to_mathml_log_e_50() {
        let data = LogData::with_value_and_base(
            IntData::from_int(RADIX_10, 50).unwrap().to_rc_expr(),
            TranscData::Euler.to_rc_expr()
        ).to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mrow>",
            "<msub><mi>log</mi><mi>e</mi></msub>",
            "<mn>50</mn>",
            "</mrow>",
        ].join("");
        assert_eq!(expected, output);
    }

    #[test]
    fn test_render_to_mathml_log_10_bin() {
        let data = LogData::with_value_and_base(
            BinaryData::add(
                IntData::from_int(RADIX_10, 1).unwrap().to_rc_expr(),
                IntData::from_int(RADIX_10, 2).unwrap().to_rc_expr(),
            ).to_rc_expr(),
            IntData::from_int(RADIX_10, 10).unwrap().to_rc_expr(),
        ).to_rc_expr();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mrow>",
            "<msub><mi>log</mi><mn>10</mn></msub>",
            "<mo>(</mo>",
            "<mrow>",
            "<mn>1</mn>",
            "<mo>+</mo>",
            "<mn>2</mn>",
            "</mrow>",
            "<mo>)</mo>",
            "</mrow>",
        ].join("");
        assert_eq!(expected, output);
    }
}
