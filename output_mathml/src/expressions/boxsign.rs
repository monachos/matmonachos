use crate::MathMLContext;
use mathml_model::mo::MOData;
use mathml_model::MTag;
use mathml_model::MValue;
use mathml_model::ToMTag;
use matm_error::error::CalcError;
use std::rc::Rc;

pub(crate) fn render_boxsign_to_mathml(_ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
    let tag = MOData::new(MValue::Raw("&#x25A1;".to_owned())).to_tag();
    Ok(tag)
}

#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::expressions::Expression;

    #[test]
    fn test_render_to_mathml() {
        let data = Expression::BoxSign;
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        assert_eq!("<mo>&#x25A1;</mo>", output);
    }
}
