use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::mtext::MTextData;
use mathml_model::MTag;
use mathml_model::MValue;
use mathml_model::ToMTag;
use matm_error::error::CalcError;
use matm_model::statements::text::TextCategory;
use matm_model::statements::text::TextStatement;
use std::rc::Rc;

impl MathMLRenderer for TextStatement {
    fn to_mathml(&self, _ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let tag = match self.category {
            TextCategory::Section => {
                MTextData::with_style(
                    MValue::Text(self.text.clone()),
                    MValue::Text("font-weight:bold; font-size: x-large".to_owned())
                )
            }
            TextCategory::Paragraph => {
                MTextData::from_string(&self.text)
            }
        };
        Ok(tag.to_tag())
    }
}

#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::statements::text::TextStatement;
    use matm_model::statements::ToStatement;

    #[test]
    fn test_render_section_to_mathml() {
        let data = TextStatement::section("This is a section").to_statement();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        assert_eq!("<mtext style=\"font-weight:bold; font-size: x-large\">This is a section</mtext>", output);
    }

    #[test]
    fn test_render_paragraph_to_mathml() {
        let data = TextStatement::paragraph("This is a paragraph.").to_statement();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        assert_eq!("<mtext>This is a paragraph.</mtext>", output);
    }

}