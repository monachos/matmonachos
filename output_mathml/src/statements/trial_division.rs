use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::mtable::MTableData;
use mathml_model::mtd::MTdData;
use mathml_model::mtr::MTrData;
use mathml_model::MValue;
use mathml_model::MTag;
use mathml_model::ToMTag;
use matm_error::error::CalcError;
use matm_model::statements::factorization::TrialDivisionStep;
use std::rc::Rc;

impl MathMLRenderer for TrialDivisionStep {
    fn to_mathml(&self, ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let mut table = MTableData::new();
        for item in &self.items {
            let mut row = MTrData::new();

            let td = MTdData::new(item.dividend.to_mathml(ctx)?);
            row.children.push(td.to_rc());

            let mut td = MTdData::empty();
            td.class = Some(MValue::Text("vert-left-sep".to_owned()));
            if let Some(the_divisor) = &item.divisor {
                td.content = Some(the_divisor.to_mathml(ctx)?)
            }
            row.children.push(td.to_rc());

            table.rows.push(row.to_rc())
        }
        Ok(table.to_tag())
    }
}

#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::factories::ObjFactory;
    use matm_model::statements::factorization::TrialDivisionFactor;
    use matm_model::statements::factorization::TrialDivisionStep;
    use matm_model::statements::ToStatement;

    #[test]
    fn test_render_to_mathml() {
        let fa = ObjFactory::new10();
        let data = TrialDivisionStep::from_vector(vec![
            TrialDivisionFactor {
                dividend: fa.zi_u(12),
                divisor: Some(fa.zi_u(2)),
            },
            TrialDivisionFactor {
                dividend: fa.zi_u(6),
                divisor: Some(fa.zi_u(2)),
            },
            TrialDivisionFactor {
                dividend: fa.zi_u(3),
                divisor: Some(fa.zi_u(3)),
            },
            TrialDivisionFactor {
                dividend: fa.zi_u(1),
                divisor: None,
            },
        ]).to_statement();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mtable>",
            "<mtr>",
            "<mtd><mn>12</mn></mtd>",
            "<mtd class=\"vert-left-sep\"><mn>2</mn></mtd>",
            "</mtr>",
            "<mtr>",
            "<mtd><mn>6</mn></mtd>",
            "<mtd class=\"vert-left-sep\"><mn>2</mn></mtd>",
            "</mtr>",
            "<mtr>",
            "<mtd><mn>3</mn></mtd>",
            "<mtd class=\"vert-left-sep\"><mn>3</mn></mtd>",
            "</mtr>",
            "<mtr>",
            "<mtd><mn>1</mn></mtd>",
            "<mtd class=\"vert-left-sep\"></mtd>",
            "</mtr>",
            "</mtable>",
        ].join("");
        assert_eq!(expected, output);
    }

}