use crate::statements::tabular::render_positional_number_to_mathml;
use crate::statements::tabular::render_positional_number_to_mathml_table_data;
use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::mo::MOData;
use mathml_model::mtable::MTableData;
use mathml_model::mtd::MTdData;
use mathml_model::mtr::MTrData;
use mathml_model::MTag;
use mathml_model::ToMTag;
use matm_error::error::CalcError;
use matm_model::statements::tabular_division::TabularDivisionData;
use matm_model::statements::tabular_division::TabularDivisionLine;
use std::rc::Rc;

impl MathMLRenderer for TabularDivisionData {
    fn to_mathml(&self, _ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let mut table = MTableData::new();
        let max_length = self.dividend.digits_len() + self.divisor.digits_len() + 1;
        let left_length = self.dividend.digits_len();

        // write quotient
        render_positional_number_to_mathml(
            &self.quotient,
            left_length - self.quotient.digits_len(),
            None,
            false,
            false,
            max_length,
            &mut table)?;

        // write dividend and divisor
        let mut row = MTrData::new();
        render_positional_number_to_mathml_table_data(
            &self.dividend,
            0,
            None,
            true,
            false,
            left_length,
            &mut row)?;
        row.children.push(
            MTdData::new(MOData::from_string(":").to_tag()).to_rc()
        );
        render_positional_number_to_mathml_table_data(
            &self.divisor,
            0,
            None,
            false,
            false,
            self.divisor.digits_len(),
            &mut row)?;
        table.rows.push(row.to_rc());

        // write partial calculations and line
        let mut add_separator = false;
        for line in &self.lines {
            match line {
                TabularDivisionLine::Separator => {
                    add_separator = true;
                }
                TabularDivisionLine::Number(num) => {
                    let mut row = MTrData::new();
                    render_positional_number_to_mathml_table_data(
                        num,
                        left_length - num.digits_len(),
                        None,
                        add_separator,
                        false,
                        left_length,
                        &mut row)?;
                    for _ in left_length..max_length {
                        row.children.push(
                            MTdData::empty().to_rc()
                        );
                    }
                    table.rows.push(row.to_rc());
                    add_separator = false;
                }
            }
        }
        Ok(table.to_tag())
    }
}

#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::core::natural::Natural;
    use matm_model::core::natural::RADIX_10;
    use matm_model::core::positional::PositionalNumber;
    use matm_model::statements::tabular_division::TabularDivisionData;
    use matm_model::statements::tabular_division::TabularDivisionLine;
    use matm_model::statements::ToStatement;

    #[test]
    fn test_render_to_mathml() {
        let mut quotient = PositionalNumber::new(RADIX_10);
        quotient.set_digit(1, 1).unwrap();
        quotient.set_digit(0, 2).unwrap();
        let mut line1 = PositionalNumber::new(RADIX_10);
        line1.set_digit(2, 1).unwrap();
        line1.set_digit(1, 2).unwrap();
        let mut line2 = PositionalNumber::new(RADIX_10);
        line2.set_digit(1, 0).unwrap();
        line2.set_digit(0, 0).unwrap();
        let mut line3 = PositionalNumber::new(RADIX_10);
        line3.set_digit(0, 0).unwrap();
        let mut line4 = PositionalNumber::new(RADIX_10);
        line4.set_digit(0, 0).unwrap();
        let data = TabularDivisionData {
            dividend: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 120).unwrap()),
            divisor: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 10).unwrap()),
            lines: vec![
                TabularDivisionLine::Number(line1),
                TabularDivisionLine::Separator,
                TabularDivisionLine::Number(line2),
                TabularDivisionLine::Number(line3),
                TabularDivisionLine::Separator,
                TabularDivisionLine::Number(line4),
            ],
            quotient,
        }.to_statement();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mtable>",
            "<mtr>",
            "<mtd></mtd>",
            "<mtd><mn>1</mn></mtd>",
            "<mtd><mn>2</mn></mtd>",
            "<mtd></mtd>",
            "<mtd></mtd>",
            "<mtd></mtd>",
            "</mtr>",
            "<mtr>",
            "<mtd class=\"hor-top-sep\"><mn>1</mn></mtd>",
            "<mtd class=\"hor-top-sep\"><mn>2</mn></mtd>",
            "<mtd class=\"hor-top-sep\"><mn>0</mn></mtd>",
            "<mtd><mo>:</mo></mtd>",
            "<mtd><mn>1</mn></mtd>",
            "<mtd><mn>0</mn></mtd>",
            "</mtr>",
            "<mtr>",
            "<mtd><mn>1</mn></mtd>",
            "<mtd><mn>2</mn></mtd>",
            "<mtd></mtd>",
            "<mtd></mtd>",
            "<mtd></mtd>",
            "<mtd></mtd>",
            "</mtr>",
            "<mtr>",
            "<mtd class=\"hor-top-sep\"></mtd>",
            "<mtd class=\"hor-top-sep\"><mn>0</mn></mtd>",
            "<mtd class=\"hor-top-sep\"><mn>0</mn></mtd>",
            "<mtd></mtd>",
            "<mtd></mtd>",
            "<mtd></mtd>",
            "</mtr>",
            "<mtr>",
            "<mtd></mtd>",
            "<mtd></mtd>",
            "<mtd><mn>0</mn></mtd>",
            "<mtd></mtd>",
            "<mtd></mtd>",
            "<mtd></mtd>",
            "</mtr>",
            "<mtr>",
            "<mtd class=\"hor-top-sep\"></mtd>",
            "<mtd class=\"hor-top-sep\"></mtd>",
            "<mtd class=\"hor-top-sep\"><mn>0</mn></mtd>",
            "<mtd></mtd>",
            "<mtd></mtd>",
            "<mtd></mtd>",
            "</mtr>",
            "</mtable>",
        ].join("");
        assert_eq!(expected, output);
    }

}