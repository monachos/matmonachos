mod expr_step;
mod text;
mod trial_division;
mod tabular_addition;
mod tabular_subtraction;
mod tabular_multiplication;
mod tabular_division;
mod tabular;

use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::MTag;
use matm_error::error::CalcError;
use matm_model::statements::Statement;
use std::rc::Rc;

impl MathMLRenderer for Statement {
    fn to_mathml(&self, ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        match self {
            Statement::ExprStep(data) => data.to_mathml(ctx),
            Statement::Text(data) => data.to_mathml(ctx),
            Statement::TabularAdd(data) => data.to_mathml(ctx),
            Statement::TabularSub(data) => data.to_mathml(ctx),
            Statement::TabularMul(data) => data.to_mathml(ctx),
            Statement::TabularDiv(data) => data.to_mathml(ctx),
            Statement::TrialDivision(data) => data.to_mathml(ctx),
        }
    }
}

