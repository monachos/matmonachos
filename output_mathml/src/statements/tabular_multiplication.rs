use crate::statements::tabular::render_positional_number_to_mathml;
use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::mtable::MTableData;
use mathml_model::ToMTag;
use mathml_model::MTag;
use mathml_model::MValue;
use matm_error::error::CalcError;
use matm_model::statements::tabular_multiplication::TabularMultiplicationData;
use std::cmp::max;
use std::rc::Rc;

impl MathMLRenderer for TabularMultiplicationData {
    fn to_mathml(&self, _ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let mut table = MTableData::new();

        let sign_length: usize = 1;
        let mut max_length = 0;
        max_length = max(max_length, self.multiplier.to_text().len());
        max_length = max(max_length, self.multiplicand.to_text().len());
        for prod in &self.partial_products {
            max_length = max(max_length, prod.to_string().len());
        }
        for carry in &self.multiplication_carries {
            max_length = max(max_length, carry.to_string().len());
        }
        max_length = max(max_length, self.addition_carry.to_string().len());
        max_length = max(max_length, self.product.to_string().len());
        max_length += sign_length;

        // multiplication carries
        for carry in self.multiplication_carries.iter().rev() {
            if carry.has_any_digit() {
                render_positional_number_to_mathml(
                    &carry,
                    max_length - carry.digits_len(),
                    None,
                    false,
                    true,
                    max_length,
                    &mut table)?;
            }
        }

        // write multiplier
        render_positional_number_to_mathml(
            &self.multiplier,
            max_length - self.multiplier.digits_len(),
            None,
            false,
            false,
            max_length,
            &mut table)?;

        // write multiplicand
        render_positional_number_to_mathml(
            &self.multiplicand,
            max_length - self.multiplicand.digits_len(),
            Some(MValue::Raw("&#xD7;".to_owned())),
            false,
            false,
            max_length,
            &mut table)?;

        // addition carry
        if !self.addition_carry.is_empty() {
            render_positional_number_to_mathml(
                &self.addition_carry,
                max_length - self.addition_carry.digits_len(),
                None,
                true,
                false,
                max_length,
                &mut table)?;
        }

        // write partial products
        for (idx, prod) in self.partial_products.iter().enumerate() {
            let symbol = if idx == self.partial_products.len() - 1 {
                Some(MValue::Text("+".to_owned()))
            } else {
                None
            };
            render_positional_number_to_mathml(
                prod,
                max_length - prod.digits_len(),
                symbol,
                false,
                false,
                max_length,
                &mut table)?;
        }

        // write product
        render_positional_number_to_mathml(
            &self.product,
            max_length - self.product.digits_len(),
            None,
            true,
            false,
            max_length,
            &mut table)?;

        Ok(table.to_tag())
    }
}

#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::core::natural::Natural;
    use matm_model::core::natural::RADIX_10;
    use matm_model::core::positional::PositionalNumber;
    use matm_model::statements::tabular_multiplication::TabularMultiplicationData;
    use matm_model::statements::ToStatement;

    #[test]
    fn test_render_to_mathml() {
        let mut prod1 = PositionalNumber::new(RADIX_10);
        prod1.set_digit(2, 3).unwrap();
        prod1.set_digit(1, 6).unwrap();
        prod1.set_digit(0, 4).unwrap();
        let mut prod2 = PositionalNumber::new(RADIX_10);
        prod2.set_digit(3, 1).unwrap();
        prod2.set_digit(2, 0).unwrap();
        prod2.set_digit(1, 4).unwrap();
        let mut mul_carry1 = PositionalNumber::new(RADIX_10);
        mul_carry1.set_digit(2, 3).unwrap();
        mul_carry1.set_digit(1, 1).unwrap();
        let mut mul_carry2 = PositionalNumber::new(RADIX_10);
        mul_carry2.set_digit(2, 1).unwrap();
        let mut add_carry = PositionalNumber::new(RADIX_10);
        add_carry.set_digit(2, 1).unwrap();
        let data = TabularMultiplicationData {
            multiplier: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 52).unwrap()),
            multiplicand: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 27).unwrap()),
            partial_products: vec![prod1, prod2],
            multiplication_carries: vec![mul_carry1, mul_carry2],
            addition_carry: add_carry,
            product: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 1404).unwrap()),
        }.to_statement();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mtable>",
            "<mtr>",
            "<mtd></mtd>",
            "<mtd></mtd>",
            "<mtd><mstyle mathsize=\"75%\"><mn>1</mn></mstyle></mtd>",
            "<mtd></mtd>",
            "<mtd></mtd>",
            "</mtr>",
            "<mtr>",
            "<mtd></mtd>",
            "<mtd></mtd>",
            "<mtd><mstyle mathsize=\"75%\"><mn>3</mn></mstyle></mtd>",
            "<mtd><mstyle mathsize=\"75%\"><mn>1</mn></mstyle></mtd>",
            "<mtd></mtd>",
            "</mtr>",
            "<mtr>",
            "<mtd></mtd>",
            "<mtd></mtd>",
            "<mtd></mtd>",
            "<mtd><mn>5</mn></mtd>",
            "<mtd><mn>2</mn></mtd>",
            "</mtr>",
            "<mtr>",
            "<mtd><mo>&#xD7;</mo></mtd>",
            "<mtd></mtd>",
            "<mtd></mtd>",
            "<mtd><mn>2</mn></mtd>",
            "<mtd><mn>7</mn></mtd>",
            "</mtr>",
            "<mtr>",
            "<mtd class=\"hor-top-sep\"></mtd>",
            "<mtd class=\"hor-top-sep\"></mtd>",
            "<mtd class=\"hor-top-sep\"><mn>1</mn></mtd>",
            "<mtd class=\"hor-top-sep\"></mtd>",
            "<mtd class=\"hor-top-sep\"></mtd>",
            "</mtr>",
            "<mtr>",
            "<mtd></mtd>",
            "<mtd></mtd>",
            "<mtd><mn>3</mn></mtd>",
            "<mtd><mn>6</mn></mtd>",
            "<mtd><mn>4</mn></mtd>",
            "</mtr>",
            "<mtr>",
            "<mtd><mo>+</mo></mtd>",
            "<mtd><mn>1</mn></mtd>",
            "<mtd><mn>0</mn></mtd>",
            "<mtd><mn>4</mn></mtd>",
            "<mtd></mtd>",
            "</mtr>",
            "<mtr>",
            "<mtd class=\"hor-top-sep\"></mtd>",
            "<mtd class=\"hor-top-sep\"><mn>1</mn></mtd>",
            "<mtd class=\"hor-top-sep\"><mn>4</mn></mtd>",
            "<mtd class=\"hor-top-sep\"><mn>0</mn></mtd>",
            "<mtd class=\"hor-top-sep\"><mn>4</mn></mtd>",
            "</mtr>",
            "</mtable>",
        ].join("");
        assert_eq!(expected, output);
    }

}