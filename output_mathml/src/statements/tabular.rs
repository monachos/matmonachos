use mathml_model::mn::MNData;
use mathml_model::mo::MOData;
use mathml_model::mstyle::MStyleData;
use mathml_model::mtable::MTableData;
use mathml_model::mtd::MTdData;
use mathml_model::mtr::MTrData;
use mathml_model::{MValue, ToMTag};
use matm_error::error::CalcError;
use matm_model::core::positional::PositionalNumber;

pub(crate) fn render_positional_number_to_mathml_table_data(
    number: &PositionalNumber,
    start_at: usize,
    start_symbol: Option<MValue>,
    add_horizontal_bar: bool,
    use_small_size: bool,
    max_length: usize,
    row: &mut MTrData,
) -> Result<(), CalcError> {
    let mut col = 0;
    if let Some(sym) = start_symbol {
        let mut td = MTdData::new(MOData::new(sym).to_tag());
        if add_horizontal_bar {
            td.class = Some(MValue::Text("hor-top-sep".to_owned()));
        }
        row.children.push(td.to_rc());
        col += 1;
    }
    for _ in col..start_at {
        let mut td = MTdData::empty();
        if add_horizontal_bar {
            td.class = Some(MValue::Text("hor-top-sep".to_owned()));
        }
        row.children.push(td.to_rc());
    }
    col = start_at;
    for digit in number.digits.iter().rev() {
        let mut td = MTdData::empty();
        if add_horizontal_bar {
            td.class = Some(MValue::Text("hor-top-sep".to_owned()));
        }

        let text = digit.to_text(number.radix);
        let text = text.trim();
        if !text.is_empty() {
            let digit_tag = MNData::from_string(text).to_tag();
            let content = if use_small_size {
                let mut style = MStyleData::new(digit_tag);
                style.math_size = Some(MValue::Text("75%".to_owned()));
                style.to_tag()
            } else {
                digit_tag
            };
            td.content = Some(content);
        }
        row.children.push(td.to_rc());
        col += 1;
    }

    for _ in col .. max_length {
        let mut td = MTdData::empty();
        if add_horizontal_bar {
            td.class = Some(MValue::Text("hor-top-sep".to_owned()));
        }
        row.children.push(td.to_rc());
    }

    Ok(())
}

pub(crate) fn render_positional_number_to_mathml(
    number: &PositionalNumber,
    start_at: usize,
    start_symbol: Option<MValue>,
    add_horizontal_bar: bool,
    use_small_size: bool,
    max_length: usize,
    table: &mut MTableData,
) -> Result<(), CalcError> {
    let mut row = MTrData::new();
    render_positional_number_to_mathml_table_data(
        number,
        start_at,
        start_symbol,
        add_horizontal_bar,
        use_small_size,
        max_length,
        &mut row,
    )?;
    table.rows.push(row.to_rc());
    Ok(())
}
