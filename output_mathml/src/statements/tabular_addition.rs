use crate::statements::tabular::render_positional_number_to_mathml;
use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::mtable::MTableData;
use mathml_model::MTag;
use mathml_model::MValue;
use mathml_model::ToMTag;
use matm_error::error::CalcError;
use matm_model::statements::tabular_addition::TabularAdditionData;
use std::cmp::max;
use std::rc::Rc;

impl MathMLRenderer for TabularAdditionData {
    fn to_mathml(&self, _ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let sign_length: usize = 1;
        let mut max_length = 0;
        max_length = max(max_length, self.sum.to_string().len());
        max_length = max(max_length, self.carry.to_string().len());
        for v in &self.addends {
            max_length = max(max_length, v.to_string().len());
        }
        max_length += sign_length;

        let mut table = MTableData::new();

        // write carry
        if self.carry.has_any_digit() {
            render_positional_number_to_mathml(
                &self.carry,
                max_length - self.carry.digits_len(),
                None,
                false,
                true,
                max_length,
                &mut table)?;
        }

        // write input addends
        for (idx, addend) in self.addends.iter().enumerate() {
            let symbol = if idx == self.addends.len() - 1 {
                Some(MValue::Text("+".to_owned()))
            } else {
                None
            };

            render_positional_number_to_mathml(
                addend,
                max_length - addend.digits_len(),
                symbol,
                false,
                false,
                max_length,
                &mut table)?;
        }

        // write sum
        render_positional_number_to_mathml(
            &self.sum,
            max_length - self.sum.digits_len(),
            None,
            true,
            false,
            max_length,
            &mut table)?;

        Ok(table.to_tag())
    }
}

#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::core::natural::Natural;
    use matm_model::core::natural::RADIX_10;
    use matm_model::core::positional::PositionalNumber;
    use matm_model::statements::tabular_addition::TabularAdditionData;
    use matm_model::statements::ToStatement;

    #[test]
    fn test_render_to_mathml() {
        let mut carry = PositionalNumber::new(RADIX_10);
        carry.set_digit(1, 1).unwrap();
        let data = TabularAdditionData {
            addends: vec![
                PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 123).unwrap()),
                PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 28).unwrap()),
            ],
            sum: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 151).unwrap()),
            carry,
        }.to_statement();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mtable>",
            "<mtr>",
            "<mtd></mtd>",
            "<mtd></mtd>",
            "<mtd><mstyle mathsize=\"75%\"><mn>1</mn></mstyle></mtd>",
            "<mtd></mtd>",
            "</mtr>",
            "<mtr>",
            "<mtd></mtd>",
            "<mtd><mn>1</mn></mtd>",
            "<mtd><mn>2</mn></mtd>",
            "<mtd><mn>3</mn></mtd>",
            "</mtr>",
            "<mtr>",
            "<mtd><mo>+</mo></mtd>",
            "<mtd></mtd>",
            "<mtd><mn>2</mn></mtd>",
            "<mtd><mn>8</mn></mtd>",
            "</mtr>",
            "<mtr>",
            "<mtd class=\"hor-top-sep\"></mtd>",
            "<mtd class=\"hor-top-sep\"><mn>1</mn></mtd>",
            "<mtd class=\"hor-top-sep\"><mn>5</mn></mtd>",
            "<mtd class=\"hor-top-sep\"><mn>1</mn></mtd>",
            "</mtr>",
            "</mtable>",
        ].join("");
        assert_eq!(expected, output);
    }

}