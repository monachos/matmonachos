use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::MTag;
use matm_error::error::CalcError;
use matm_model::statements::expression::ExpressionStep;
use std::rc::Rc;

impl MathMLRenderer for ExpressionStep {
    fn to_mathml(&self, ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        self.expression.to_mathml(ctx)
    }
}

#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::core::natural::RADIX_10;
    use matm_model::expressions::int::IntData;
    use matm_model::expressions::ToExpression;
    use matm_model::statements::expression::ExpressionStep;
    use matm_model::statements::ToStatement;

    #[test]
    fn test_render_to_mathml() {
        let data = ExpressionStep::from_expression(
            IntData::from_int(RADIX_10, 17)
                .unwrap()
                .to_rc_expr()
        ).to_statement();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        assert_eq!("<mn>17</mn>", output);
    }

}