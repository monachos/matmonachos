use crate::statements::tabular::render_positional_number_to_mathml;
use crate::MathMLContext;
use crate::MathMLRenderer;
use mathml_model::mtable::MTableData;
use mathml_model::ToMTag;
use mathml_model::MTag;
use mathml_model::MValue;
use matm_error::error::CalcError;
use matm_model::statements::tabular_subtraction::TabularSubtractionData;
use std::rc::Rc;

impl MathMLRenderer for TabularSubtractionData {
    fn to_mathml(&self, _ctx: &MathMLContext) -> Result<Rc<MTag>, CalcError> {
        let mut table = MTableData::new();

        let max_length = self.minuend.digits_len() + 1;

        // write borrowed
        if self.borrowed.has_any_digit() {
            render_positional_number_to_mathml(
                &self.borrowed,
                max_length - self.borrowed.digits_len(),
                None,
                false,
                true,
                max_length,
                &mut table)?;
        }

        // write minuend
        render_positional_number_to_mathml(
            &self.minuend,
            max_length - self.minuend.digits_len(),
            None,
            false,
            false,
            max_length,
            &mut table)?;

        // write subtrahend
        render_positional_number_to_mathml(
            &self.subtrahend,
            max_length - self.subtrahend.digits_len(),
            Some(MValue::Text("-".to_owned())),
            false,
            false,
            max_length,
            &mut table)?;

        // write difference
        render_positional_number_to_mathml(
            &self.difference,
            max_length - self.difference.digits_len(),
            None,
            true,
            false,
            max_length,
            &mut table)?;

        Ok(table.to_tag())
    }
}

#[cfg(test)]
mod tests {
    use crate::MathMLContext;
    use crate::MathMLRenderer;
    use matm_model::core::natural::Natural;
    use matm_model::core::natural::RADIX_10;
    use matm_model::core::positional::PositionalNumber;
    use matm_model::statements::tabular_subtraction::TabularSubtractionData;
    use matm_model::statements::ToStatement;

    #[test]
    fn test_render_to_mathml() {
        let mut borrowed = PositionalNumber::new(RADIX_10);
        borrowed.set_digit(2, 3).unwrap();
        let data = TabularSubtractionData {
            minuend: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 456).unwrap()),
            subtrahend: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 66).unwrap()),
            difference: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 390).unwrap()),
            borrowed,
        }.to_statement();
        let ctx = MathMLContext {};
        let tag = data.to_mathml(&ctx).unwrap();
        let output = format!("{}", tag);
        let expected = vec![
            "<mtable>",
            "<mtr>",
            "<mtd></mtd>",
            "<mtd><mstyle mathsize=\"75%\"><mn>3</mn></mstyle></mtd>",
            "<mtd></mtd>",
            "<mtd></mtd>",
            "</mtr>",
            "<mtr>",
            "<mtd></mtd>",
            "<mtd><mn>4</mn></mtd>",
            "<mtd><mn>5</mn></mtd>",
            "<mtd><mn>6</mn></mtd>",
            "</mtr>",
            "<mtr>",
            "<mtd><mo>-</mo></mtd>",
            "<mtd></mtd>",
            "<mtd><mn>6</mn></mtd>",
            "<mtd><mn>6</mn></mtd>",
            "</mtr>",
            "<mtr>",
            "<mtd class=\"hor-top-sep\"></mtd>",
            "<mtd class=\"hor-top-sep\"><mn>3</mn></mtd>",
            "<mtd class=\"hor-top-sep\"><mn>9</mn></mtd>",
            "<mtd class=\"hor-top-sep\"><mn>0</mn></mtd>",
            "</mtr>",
            "</mtable>",
        ].join("");
        assert_eq!(expected, output);
    }

}