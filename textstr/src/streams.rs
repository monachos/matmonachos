use std::fmt;
use std::io::stdout;
use std::io::Write;


pub trait OutputStream {
    fn write(&mut self, text: &str);
}

//----------------------

pub struct NullStream;

impl NullStream {
    pub fn new() -> Self {
        Self {
        }
    }
}

impl OutputStream for NullStream {
    fn write(&mut self, _text: &str) {
    }
}

impl fmt::Display for NullStream {
    fn fmt(&self, _f: &mut fmt::Formatter<'_>) -> fmt::Result {
        Ok(())
    }
}

//----------------------

pub struct StringStream {
    buffer: String
}

impl StringStream {
    pub fn new() -> Self {
        Self {
            buffer: String::new()
        }
    }
}

impl OutputStream for StringStream {
    fn write(&mut self, text: &str) {
        self.buffer.push_str(text);
    }
}

impl fmt::Display for StringStream {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.buffer)
    }
}

//----------------------

pub struct StdoutStream;

impl StdoutStream {
    pub fn new() -> Self {
        Self {
        }
    }
}

impl OutputStream for StdoutStream {
    fn write(&mut self, text: &str) {
        print!("{}", text);
        stdout().flush().unwrap_or_default();
    }
}
