#[cfg(test)]
mod tests {
    use crate::streams::NullStream;
    use crate::streams::OutputStream;
    use crate::streams::StdoutStream;
    use crate::streams::StringStream;

    #[test]
    fn test_null_stream_write() {
        let mut stream = NullStream::new();
        stream.write("abc");
    }

    #[test]
    fn test_null_stream_format() {
        let mut stream = NullStream::new();
        stream.write("abc");

        let result = format!("{}", stream);

        assert!(result.is_empty());
    }

    //--------------------------------------------

    #[test]
    fn test_string_stream_write() {
        let mut stream = StringStream::new();
        stream.write("abc");
    }

    #[test]
    fn test_string_stream_format() {
        let mut stream = StringStream::new();
        stream.write("abc");

        let result = format!("{}", stream);

        assert_eq!("abc", result);
    }

    //--------------------------------------------

    #[test]
    fn test_stdout_stream_write() {
        let mut stream = StdoutStream::new();
        stream.write("abc");
    }
}

