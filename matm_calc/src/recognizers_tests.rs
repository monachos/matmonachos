#[cfg(test)]
mod tests {
    use crate::calculator::context::CalcContext;
    use crate::recognizers::is_int;
    use crate::recognizers::is_int_or_product_of_ints;
    use crate::recognizers::is_int_pi;
    use crate::recognizers::is_pi;
    use crate::result::ResultContainer;
    use crate::rules::CalcRules;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::ToExpression;
    use matm_model::factories::ObjFactory;

    #[test]
    fn test_5_is_int_5() {
        let fa = ObjFactory::new10();
        let result = is_int(&fa.ezi_u(5), 5);
        assert_eq!(result, true);
    }

    #[test]
    fn test_6_is_int_5() {
        let fa = ObjFactory::new10();
        let result = is_int(&fa.ezi_u(6), 5);
        assert_eq!(result, false);
    }

    #[test]
    fn test_pi_is_int_5() {
        let fa = ObjFactory::new10();
        let result = is_int(&fa.epi(), 5);
        assert_eq!(result, false);
    }

    #[test]
    fn test_pi_is_pi() {
        let fa = ObjFactory::new10();
        let result = is_pi(&fa.epi());
        assert_eq!(result, true);
    }

    #[test]
    fn test_e_is_pi() {
        let fa = ObjFactory::new10();
        let result = is_pi(&fa.ee());
        assert_eq!(result, false);
    }

    #[test]
    fn test_2_is_pi() {
        let fa = ObjFactory::new10();
        let result = is_pi(&fa.ezi_u(2));
        assert_eq!(result, false);
    }

    #[test]
    fn test_5pi_is_int_pi_5pi() {
        let fa = ObjFactory::new10();
        let result = is_int_pi(
            &BinaryData::mul(fa.rzi_u(5), fa.rpi()).to_expr(),
            5);
        assert_eq!(result, true);
    }

    #[test]
    fn test_6pi_is_int_pi_5pi() {
        let fa = ObjFactory::new10();
        let result = is_int_pi(
            &BinaryData::mul(fa.rzi_u(6), fa.rpi()).to_expr(),
            5);
        assert_eq!(result, false);
    }

    #[test]
    fn test_pi_is_int_pi_5pi() {
        let fa = ObjFactory::new10();
        let result = is_int_pi(&fa.epi(), 5);
        assert_eq!(result, false);
    }

    #[test]
    fn test_is_int_or_product_of_ints_for_int() {
        let rules = CalcRules::default();
        let mut future_rules = CalcRules::default();
        let mut explanation = ResultContainer::new();
        let ctx = CalcContext::new(
            &rules,
            &mut future_rules,
            &mut explanation);

        let expr = ctx.fa().ezi_u(2);

        assert!(is_int_or_product_of_ints(&expr));
    }

    #[test]
    fn test_is_int_or_product_of_ints_for_prod_of_int() {
        let rules = CalcRules::default();
        let mut future_rules = CalcRules::default();
        let mut explanation = ResultContainer::new();
        let ctx = CalcContext::new(
            &rules,
            &mut future_rules,
            &mut explanation);

        let expr = BinaryData::mul(
            ctx.fa().rzi_u(2),
            ctx.fa().rzi_u(3)
        ).to_expr();

        assert!(is_int_or_product_of_ints(&expr));
    }

    #[test]
    fn test_is_int_or_product_of_ints_for_prod_of_int_nested() {
        let rules = CalcRules::default();
        let mut future_rules = CalcRules::default();
        let mut explanation = ResultContainer::new();
        let ctx = CalcContext::new(
            &rules,
            &mut future_rules,
            &mut explanation);

        let expr = BinaryData::mul(
            ctx.fa().rzi_u(2),
            BinaryData::mul(
                BinaryData::mul(
                    ctx.fa().rzi_u(3),
                    ctx.fa().rzi_u(5)
                ).to_rc_expr(),
                BinaryData::mul(
                    ctx.fa().rzi_u(7),
                    ctx.fa().rzi_u(11)
                ).to_rc_expr()
            ).to_rc_expr()
        ).to_expr();

        assert!(is_int_or_product_of_ints(&expr));
    }

    #[test]
    fn test_is_int_or_product_of_ints_for_sum_of_int() {
        let rules = CalcRules::default();
        let mut future_rules = CalcRules::default();
        let mut explanation = ResultContainer::new();
        let ctx = CalcContext::new(
            &rules,
            &mut future_rules,
            &mut explanation);

        let expr = BinaryData::add(
            ctx.fa().rzi_u(2),
            ctx.fa().rzi_u(3)
        ).to_expr();

        assert_eq!(false, is_int_or_product_of_ints(&expr));
    }

    #[test]
    fn test_is_int_or_product_of_ints_for_prod_of_int_with_sum() {
        let rules = CalcRules::default();
        let mut future_rules = CalcRules::default();
        let mut explanation = ResultContainer::new();
        let ctx = CalcContext::new(
            &rules,
            &mut future_rules,
            &mut explanation);

        let expr = BinaryData::mul(
            ctx.fa().rzi_u(2),
            BinaryData::mul(
                BinaryData::mul(
                    ctx.fa().rzi_u(3),
                    ctx.fa().rzi_u(5)
                ).to_rc_expr(),
                BinaryData::add(
                    ctx.fa().rzi_u(7),
                    ctx.fa().rzi_u(11)
                ).to_rc_expr()
            ).to_rc_expr()
        ).to_expr();

        assert_eq!(false, is_int_or_product_of_ints(&expr));
    }

}
