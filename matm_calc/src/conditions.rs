use async_recursion::async_recursion;
use matm_error::error::CalcError;
use matm_model::expressions::arithmetic_condition::ArithmeticCondition;
use matm_model::expressions::arithmetic_operator::ArithmeticOperator;
use matm_model::expressions::condition::Condition;
use matm_model::expressions::logic_condition::LogicCondition;
use matm_model::expressions::logic_operator::LogicOperator;

use crate::calculator::context::CalcContext;
use crate::comparator::compare_expressions;


#[async_recursion(?Send)]
pub async fn evaluate_condition(
    ctx: &mut CalcContext<'_>,
    cond: &Condition,
) -> Result<Option<bool>, CalcError> {
    return match cond {
        Condition::Bool(b) => Ok(Some(b.value())),
        Condition::Arithmetic(arithmetic) => evaluate_arithmetic_condition(ctx, arithmetic).await,
        Condition::Logic(logic) => evaluate_logic_condition(ctx, logic).await,
    };
}

async fn evaluate_arithmetic_condition(
    ctx: &mut CalcContext<'_>,
    cond: &ArithmeticCondition,
) -> Result<Option<bool>, CalcError> {
    match cond.operator {
        ArithmeticOperator::Equal => Ok(Some(cond.left.eq(&cond.right))),
        ArithmeticOperator::NotEqual => Ok(Some(cond.left.ne(&cond.right))),
        ArithmeticOperator::Greater => match compare_expressions(ctx, &cond.left, &cond.right).await? {
            Some(cmp) => Ok(Some(cmp.is_gt())),
            None => Ok(None),
        },
        ArithmeticOperator::GreaterOrEqual => match compare_expressions(ctx, &cond.left, &cond.right).await? {
            Some(cmp) => Ok(Some(cmp.is_ge())),
            None => Ok(None),
        },
        ArithmeticOperator::Less => match compare_expressions(ctx, &cond.left, &cond.right).await? {
            Some(cmp) => Ok(Some(cmp.is_lt())),
            None => Ok(None),
        },
        ArithmeticOperator::LessOrEqual => match compare_expressions(ctx, &cond.left, &cond.right).await? {
            Some(cmp) => Ok(Some(cmp.is_le())),
            None => Ok(None),
        },
    }
}

async fn evaluate_logic_condition(
    ctx: &mut CalcContext<'_>,
    cond: &LogicCondition,
) -> Result<Option<bool>, CalcError> {
    match (evaluate_condition(ctx, &cond.left).await?, evaluate_condition(ctx, &cond.right).await?) {
        (Some(eval_left), Some(eval_right)) => {
            return match cond.operator {
                LogicOperator::And => Ok(Some(eval_left && eval_right)),
                LogicOperator::Or => Ok(Some(eval_left || eval_right)),
            };
        }
        _ => {
            return Ok(None);
        }
    }
}

