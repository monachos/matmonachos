pub struct DivisionResult<T> {
    pub quotient: T,
    pub remainder: T,
}
