
pub struct MultiInstanceItem<T> {
    value: T,
    count: usize,
}

impl<T> MultiInstanceItem<T> {
    pub fn value(&self) -> &T {
        return &self.value;
    }

    pub fn count(&self) -> usize {
        return self.count;
    }
}

//---------------------------------------

pub struct MultiInstanceCollector<T>
    where T: Clone + PartialEq {
    pub items: Vec<MultiInstanceItem<T>>,
}

impl<T: Clone + PartialEq> MultiInstanceCollector<T> {
    pub fn new() -> Self {
        Self {
            items: Vec::new(),
        }
    }

    fn get_item_by_key_mut(&mut self, key: &T) -> Option<&mut MultiInstanceItem<T>> {
        for item in &mut self.items {
            if item.value.eq(key) {
                return Some(item);
            }
        }
        None
    }

    pub fn add_item(&mut self, value: &T) {
        match self.get_item_by_key_mut(value) {
            Some(item) => {
                item.count += 1;
            }
            None => {
                self.items.push(MultiInstanceItem {
                    value: value.clone(),
                    count: 1,
                });
            }
        }
    }

    /// gets count of elements including each instance.
    /// If the container contains two elements with 1 and 2 instances
    /// then total length of the container is 3.
    pub fn len(&self) -> usize {
        return self.items.iter().map(|i| i.count()).sum();
    }
}
