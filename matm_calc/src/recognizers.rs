use matm_model::expressions::binary::BinaryOperator;
use matm_model::expressions::transcendental::TranscData;
use matm_model::expressions::Expression;

pub fn is_int(expr: &Expression, value: i32) -> bool {
    if let Expression::Int(e_int) = expr {
        if let Some(e_int_val) = e_int.to_i32() {
            if e_int_val == value {
                return true;
            }
        }
    }
    return false;
}

pub fn is_pi(expr: &Expression) -> bool {
    if let Expression::Transc(tr) = expr {
        if let TranscData::Pi = tr.as_ref() {
            return true;
        }
    }
    return false;
}

pub fn is_int_pi(expr: &Expression, term: i32) -> bool {
    if let Expression::Binary(bin) = expr {
        if BinaryOperator::Mul == bin.operator
            && is_int(&bin.left, term)
            && is_pi(&bin.right) {
            return true;
        }
    }
    return false;
}

pub fn is_int_or_product_of_ints(expr: &Expression) -> bool {
    match expr {
        Expression::Binary(bin_expr) => {
            if let BinaryOperator::Mul = bin_expr.operator {
                if !is_int_or_product_of_ints(&bin_expr.left) {
                    return false;
                }
                if !is_int_or_product_of_ints(&bin_expr.right) {
                    return false;
                }
                return true;
            }
        }
        Expression::Int(_) => {
            return true;
        }
        _ => {}
    }

    return false;
}
