pub mod step_that;

use futures::executor::block_on;
use std::rc::Rc;

use crate::calculator::calculate_expression;
use crate::calculator::context::CalcContext;
use crate::result::dump_unicode_color;
use crate::result::ResultContainerFlatExpressionIterator;
use matm_error::error::CalcError;
use matm_model::expressions::Expression;
use matm_model::statements::expression::ExpressionStep;
use matm_model::testing::asserts::assert_result_ok;

use self::step_that::ExpressionStepAssertThat;


pub fn assert_next_step<'a>(step_iter: &'a mut ResultContainerFlatExpressionIterator
) -> &'a ExpressionStep {
    if let Some(step) = step_iter.next() {
        return step;
    } else {
        assert!(false, "Expected next step not found.");
        panic!("Step not found");
    }
}

pub fn assert_none_step(step_iter: &mut ResultContainerFlatExpressionIterator) {
    if let Some(_) = step_iter.next() {
        assert!(false, "Unexpected next step.");
    }
}

pub fn assert_next_step_that<'a>(step_iter: &'a mut ResultContainerFlatExpressionIterator
) -> ExpressionStepAssertThat<'a> {
    if let Some(step) = step_iter.next() {
        return ExpressionStepAssertThat {
            step: step,
        };
    } else {
        assert!(false, "Expected next step not found.");
        panic!("Step not found");
    }
}

pub fn test_calculation_with_explanation(
    ctx: &mut CalcContext<'_>,
    expr: &Rc<Expression>,
) -> Result<Rc<Expression>, CalcError> {
    println!("Calculating {}", expr);
    let result = block_on(calculate_expression(ctx, &expr));
    dump_unicode_color(&ctx.explanation);
    println!();
    result
}

pub fn test_calculation(
    ctx: &mut CalcContext<'_>,
    expr: &Rc<Expression>,
) -> Result<Rc<Expression>, CalcError> {
    test_calculation_with_explanation(ctx, expr)
}

pub fn test_calculation_ok(
    ctx: &mut CalcContext<'_>,
    expr: &Rc<Expression>,
) -> Rc<Expression> {
    let result = test_calculation(ctx, &expr);
    assert_result_ok(result)
}

pub fn test_calculation_with_explanation_ok(
    ctx: &mut CalcContext<'_>,
    expr: &Rc<Expression>,
) -> Rc<Expression> {
    let result = test_calculation_with_explanation(ctx, &expr);
    assert_result_ok(result)
}
