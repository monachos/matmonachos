use matm_model::testing::asserts::assert_expression_that;
use matm_model::testing::asserts::int_that::IntegerAssertThat;
use matm_model::testing::asserts::frac_that::FractionAssertThat;
use matm_model::testing::asserts::symbol_that::SymbolAssertThat;
use matm_model::testing::asserts::matrix_that::MatrixAssertThat;
use matm_model::testing::asserts::unary_that::UnaryAssertThat;
use matm_model::testing::asserts::binary_that::BinaryAssertThat;
use matm_model::statements::expression::ExpressionStep;
use matm_model::testing::asserts::binomial_that::BinomialAssertThat;
use matm_model::testing::asserts::conditional_that::ConditionalAssertThat;
use matm_model::testing::asserts::cracovian_that::CracovianAssertThat;
use matm_model::testing::asserts::func_that::FunctionAssertThat;
use matm_model::testing::asserts::log_that::LogAssertThat;
use matm_model::testing::asserts::root_that::RootAssertThat;


pub struct ExpressionStepAssertThat<'a> {
    pub step: &'a ExpressionStep,
}

impl <'a> ExpressionStepAssertThat<'a> {
    pub fn has_calculation_count(self, expected: usize) -> ExpressionStepAssertThat<'a> {
        assert_eq!(Some(expected), self.step.calculated_count);
        return self;
    }

    pub fn has_format(self, expected: &str) -> ExpressionStepAssertThat<'a> {
        assert_eq!(expected, self.step.expression.to_string());
        return self;
    }

    pub fn as_integer(self) -> IntegerAssertThat<'a> {
        return assert_expression_that(&self.step.expression).as_integer();
    }

    pub fn as_fraction(self) -> FractionAssertThat<'a> {
        return assert_expression_that(&self.step.expression).as_fraction();
    }

    pub fn as_symbol(self) -> SymbolAssertThat<'a> {
        return assert_expression_that(&self.step.expression).as_symbol();
    }

    pub fn as_matrix(self) -> MatrixAssertThat<'a> {
        return assert_expression_that(&self.step.expression).as_matrix();
    }

    pub fn as_cracovian(self) -> CracovianAssertThat<'a> {
        return assert_expression_that(&self.step.expression).as_cracovian();
    }

    pub fn as_unary(self) -> UnaryAssertThat<'a> {
        return assert_expression_that(&self.step.expression).as_unary();
    }

    pub fn as_binary(self) -> BinaryAssertThat<'a> {
        return assert_expression_that(&self.step.expression).as_binary();
    }

    pub fn as_binomial(self) -> BinomialAssertThat<'a> {
        return assert_expression_that(&self.step.expression).as_binomial();
    }

    pub fn as_root(self) -> RootAssertThat<'a> {
        return assert_expression_that(&self.step.expression).as_root();
    }

    pub fn as_log(self) -> LogAssertThat<'a> {
        return assert_expression_that(&self.step.expression).as_log();
    }

    pub fn as_function(self) -> FunctionAssertThat<'a> {
        return assert_expression_that(&self.step.expression).as_function();
    }

    pub fn as_conditional(self) -> ConditionalAssertThat<'a> {
        return assert_expression_that(&self.step.expression).as_conditional();
    }
}

