#[cfg(test)]
mod tests {
    use futures::executor::block_on;
    use rstest::rstest;
    use std::cmp::Ordering;

    use crate::calculator::context::CalcContext;
    use crate::comparator::compare_expressions;
    use crate::result::ResultContainer;
    use crate::rules::CalcRules;

    #[rstest]
    // positive/positive
    #[case((2, 5), Ordering::Less)]
    #[case((2, 2), Ordering::Equal)]
    #[case((2, 1), Ordering::Greater)]
    // positive/zero
    #[case((2, 0), Ordering::Greater)]
    // positive/negative
    #[case((2, -2), Ordering::Greater)]
    // zero/positive
    #[case((0, 2), Ordering::Less)]
    // zero/zero
    #[case((0, 0), Ordering::Equal)]
    // zero/negative
    #[case((0, -2), Ordering::Greater)]
    // negative/positive
    #[case((-2, 2), Ordering::Less)]
    // negative/zero
    #[case((-2, 0), Ordering::Less)]
    // negative/negative
    #[case((-2, 2), Ordering::Less)]
    #[case((-5, -4), Ordering::Less)]
    #[case((-2, -2), Ordering::Equal)]
    #[case((-2, -3), Ordering::Greater)]
    fn test_compare_expressions_int_int(
        #[case] input: (i32, i32),
        #[case] expected: Ordering,
    ) {
        let rules = CalcRules::default();
        let mut future_rules = CalcRules::default();
        let mut explanation = ResultContainer::new();
        let ctx = CalcContext::new(
            &rules,
            &mut future_rules,
            &mut explanation);

        let o1 = ctx.fa().ezi_u(input.0);
        let o2 = ctx.fa().ezi_u(input.1);

        println!("Comparing: {}, {}", o1, o2);
        let result = block_on(compare_expressions(&ctx, &o1, &o2)).unwrap();

        assert_eq!(result, Some(expected));
    }

    #[rstest]
    // positive/positive
    #[case(2, (3, 1), Ordering::Less)]
    #[case(2, (5, 2), Ordering::Less)]
    #[case(2, (7, 3), Ordering::Less)]
    #[case(2, (2, 1), Ordering::Equal)]
    #[case(2, (4, 2), Ordering::Equal)]
    #[case(2, (1, 1), Ordering::Greater)]
    #[case(2, (1, 2), Ordering::Greater)]
    #[case(2, (3, 2), Ordering::Greater)]
    // positive/zero
    #[case(2, (0, 2), Ordering::Greater)]
    // positive/negative
    #[case(2, (-1, 2), Ordering::Greater)]
    // zero/positive
    #[case(0, (1, 2), Ordering::Less)]
    // zero/zero
    #[case(0, (0, 2), Ordering::Equal)]
    // zero/negative
    #[case(0, (-1, 2), Ordering::Greater)]
    // negative/positive
    #[case(-2, (1, 2), Ordering::Less)]
    // negative/zero
    #[case(-2, (0, 2), Ordering::Less)]
    // negative/negative
    #[case(-2, (-3, 2), Ordering::Less)]
    #[case(-2, (3, -2), Ordering::Less)]
    #[case(-2, (-4, 2), Ordering::Equal)]
    #[case(-2, (4, -2), Ordering::Equal)]
    #[case(-1, (-3, 2), Ordering::Greater)]
    #[case(-1, (3, -2), Ordering::Greater)]
    fn test_compare_expressions_int_frac(
        #[case] left_input: i32,
        #[case] right_input: (i32, i32),
        #[case] expected: Ordering,
    ) {
        let rules = CalcRules::default();
        let mut future_rules = CalcRules::default();
        let mut explanation = ResultContainer::new();
        let ctx = CalcContext::new(
            &rules,
            &mut future_rules,
            &mut explanation);

        let o1 = ctx.fa().ezi_u(left_input);
        let o2 = ctx.fa().efi_u(right_input.0, right_input.1);

        println!("Comparing: {}, {}", o1, o2);
        let result = block_on(compare_expressions(&ctx, &o1, &o2)).unwrap();

        assert_eq!(result, Some(expected));
    }

    #[test]
    fn test_compare_expressions_int_frac_zero() {
        let rules = CalcRules::default();
        let mut future_rules = CalcRules::default();
        let mut explanation = ResultContainer::new();
        let ctx = CalcContext::new(
            &rules,
            &mut future_rules,
            &mut explanation);

        let o1 = ctx.fa().ezi_u(2);
        let o2 = ctx.fa().efi_u(2, 0);

        println!("Comparing: {}, {}", o1, o2);
        let result = block_on(compare_expressions(&ctx, &o1, &o2)).unwrap();

        assert_eq!(result, None);
    }

    #[rstest]
    // positive/positive
    #[case((1, 1), 2, Ordering::Less)]
    #[case((1, 2), 2, Ordering::Less)]
    #[case((3, 2), 2, Ordering::Less)]
    #[case((2, 1), 2, Ordering::Equal)]
    #[case((4, 2), 2, Ordering::Equal)]
    #[case((3, 1), 2, Ordering::Greater)]
    #[case((5, 2), 2, Ordering::Greater)]
    #[case((7, 3), 2, Ordering::Greater)]
    // positive/zero
    #[case((1, 2), 0, Ordering::Greater)]
    #[case((-1, -2), 0, Ordering::Greater)]
    // positive/negative
    #[case((1, 2), -1, Ordering::Greater)]
    #[case((-1, -2), -1, Ordering::Greater)]
    // zero/positive
    #[case((0, 1), 2, Ordering::Less)]
    // zero/zero
    #[case((0, 1), 0, Ordering::Equal)]
    // zero/negative
    #[case((0, 1), -2, Ordering::Greater)]
    // negative/positive
    #[case((-2, 1), 2, Ordering::Less)]
    // negative/zero
    #[case((-2, 1), 0, Ordering::Less)]
    // negative/negative
    #[case((3, -2), -1, Ordering::Less)]
    #[case((-3, 2), -1, Ordering::Less)]
    #[case((-4, 2), -2, Ordering::Equal)]
    #[case((-3, 2), -2, Ordering::Greater)]
    #[case((3, -2), -2, Ordering::Greater)]
    fn test_compare_expressions_frac_int(
        #[case] left_input: (i32, i32),
        #[case] right_input: i32,
        #[case] expected: Ordering,
    ) {
        let rules = CalcRules::default();
        let mut future_rules = CalcRules::default();
        let mut explanation = ResultContainer::new();
        let ctx = CalcContext::new(
            &rules,
            &mut future_rules,
            &mut explanation);

        let o1 = ctx.fa().efi_u(left_input.0, left_input.1);
        let o2 = ctx.fa().ezi_u(right_input);

        println!("Comparing: {}, {}", o1, o2);
        let result = block_on(compare_expressions(&ctx, &o1, &o2)).unwrap();

        assert_eq!(result, Some(expected));
    }

    #[test]
    fn test_compare_expressions_frac_zero_int() {
        let rules = CalcRules::default();
        let mut future_rules = CalcRules::default();
        let mut explanation = ResultContainer::new();
        let ctx = CalcContext::new(
            &rules,
            &mut future_rules,
            &mut explanation);

        let o1 = ctx.fa().efi_u(2, 0);
        let o2 = ctx.fa().ezi_u(2);

        println!("Comparing: {}, {}", o1, o2);
        let result = block_on(compare_expressions(&ctx, &o1, &o2)).unwrap();

        assert_eq!(result, None);
    }

    #[rstest]
    // positive/positive
    #[case((1, 1), (2, 1), Ordering::Less)]
    #[case((1, 1), (3, 2), Ordering::Less)]
    #[case((1, 2), (2, 3), Ordering::Less)]
    #[case((4, 3), (3, 2), Ordering::Less)]
    #[case((2, 1), (2, 1), Ordering::Equal)]
    #[case((4, 2), (2, 1), Ordering::Equal)]
    #[case((2, 3), (4, 6), Ordering::Equal)]
    #[case((3, 1), (2, 1), Ordering::Greater)]
    #[case((5, 2), (2, 1), Ordering::Greater)]
    #[case((7, 3), (2, 3), Ordering::Greater)]
    // positive/zero
    #[case((2, 3), (0, 6), Ordering::Greater)]
    // positive/negative
    #[case((2, 3), (-5, 6), Ordering::Greater)]
    #[case((2, 3), (5, -6), Ordering::Greater)]
    // zero/positive
    #[case((0, 3), (5, 6), Ordering::Less)]
    // zero/zero
    #[case((0, 3), (0, 6), Ordering::Equal)]
    // zero/negative
    #[case((0, 3), (-5, 6), Ordering::Greater)]
    #[case((0, 3), (5, -6), Ordering::Greater)]
    // negative/positive
    #[case((-2, 3), (5, 6), Ordering::Less)]
    #[case((2, -3), (5, 6), Ordering::Less)]
    // negative/zero
    #[case((-2, 3), (0, 6), Ordering::Less)]
    #[case((2, -3), (0, 6), Ordering::Less)]
    // negative/negative
    #[case((-3, 2), (-4, 3), Ordering::Less)]
    #[case((-3, 2), (4, -3), Ordering::Less)]
    #[case((3, -2), (-4, 3), Ordering::Less)]
    #[case((3, -2), (4, -3), Ordering::Less)]
    #[case((-2, 3), (-4, 6), Ordering::Equal)]
    #[case((-2, 3), (4, -6), Ordering::Equal)]
    #[case((2, -3), (-4, 6), Ordering::Equal)]
    #[case((2, -3), (4, -6), Ordering::Equal)]
    #[case((-2, 3), (-7, 3), Ordering::Greater)]
    #[case((-2, 3), (7, -3), Ordering::Greater)]
    #[case((2, -3), (-7, 3), Ordering::Greater)]
    #[case((2, -3), (7, -3), Ordering::Greater)]
    fn test_compare_expressions_frac_frac(
        #[case] left_input: (i32, i32),
        #[case] right_input: (i32, i32),
        #[case] expected: Ordering,
    ) {
        let rules = CalcRules::default();
        let mut future_rules = CalcRules::default();
        let mut explanation = ResultContainer::new();
        let ctx = CalcContext::new(
            &rules,
            &mut future_rules,
            &mut explanation);

        let o1 = ctx.fa().efi_u(left_input.0, left_input.1);
        let o2 = ctx.fa().efi_u(right_input.0, right_input.1);

        println!("Comparing: {}, {}", o1, o2);
        let result = block_on(compare_expressions(&ctx, &o1, &o2)).unwrap();

        assert_eq!(result, Some(expected));
    }

    #[rstest]
    #[case((1, 0), (2, 1))]
    #[case((1, 2), (2, 0))]
    #[case((1, 0), (2, 0))]
    fn test_compare_expressions_frac_zero_frac_zero(
        #[case] left_input: (i32, i32),
        #[case] right_input: (i32, i32),
    ) {
        let rules = CalcRules::default();
        let mut future_rules = CalcRules::default();
        let mut explanation = ResultContainer::new();
        let ctx = CalcContext::new(
            &rules,
            &mut future_rules,
            &mut explanation);

        let o1 = ctx.fa().efi_u(left_input.0, left_input.1);
        let o2 = ctx.fa().efi_u(right_input.0, right_input.1);

        println!("Comparing: {}, {}", o1, o2);
        let result = block_on(compare_expressions(&ctx, &o1, &o2)).unwrap();

        assert_eq!(result, None);
    }

}
