#[cfg(test)]
mod tests {
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::conditions::evaluate_condition;
    use futures::executor::block_on;
    use matm_model::expressions::arithmetic_condition::ArithmeticCondition;
    use matm_model::expressions::bool::BoolData;
    use matm_model::expressions::condition::ToCondition;
    use matm_model::expressions::logic_condition::LogicCondition;
    use ::rstest::rstest;


    #[rstest]
    #[case(false, false, false)]
    #[case(false, true, false)]
    #[case(true, false, false)]
    #[case(true, true, true)]
    fn test_logic_condition_and(
        #[case] left: bool,
        #[case] right: bool,
        #[case] expected: bool,
    ) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let cond = LogicCondition::and(
            BoolData::new(left).to_rc_cond(),
            BoolData::new(right).to_rc_cond()
        ).to_cond();

        let result = block_on(evaluate_condition(&mut ctx, &cond)).unwrap().unwrap();
        assert_eq!(expected, result);
    }

    #[rstest]
    #[case(false, false, false)]
    #[case(false, true, true)]
    #[case(true, false, true)]
    #[case(true, true, true)]
    fn test_logic_condition_or(
        #[case] left: bool,
        #[case] right: bool,
        #[case] expected: bool,
    ) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let cond = LogicCondition::or(
            BoolData::new(left).to_rc_cond(),
            BoolData::new(right).to_rc_cond()
        ).to_cond();

        let result = block_on(evaluate_condition(&mut ctx, &cond)).unwrap().unwrap();
        assert_eq!(expected, result);
    }

    #[rstest]
    #[case(4, 5, false)]
    #[case(5, 5, true)]
    #[case(6, 5, false)]
    fn test_arithmetic_condition_int_eq(
        #[case] left: i32,
        #[case] right: i32,
        #[case] expected: bool,
    ) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let cond = ArithmeticCondition::eq(
            ctx.fa().rzi_u(left),
            ctx.fa().rzi_u(right),
        ).to_cond();

        let result = block_on(evaluate_condition(&mut ctx, &cond)).unwrap().unwrap();
        assert_eq!(expected, result);
    }

    #[rstest]
    #[case(4, 5, true)]
    #[case(5, 5, false)]
    #[case(6, 5, true)]
    fn test_arithmetic_condition_int_neq(
        #[case] left: i32,
        #[case] right: i32,
        #[case] expected: bool,
    ) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let cond = ArithmeticCondition::neq(
            ctx.fa().rzi_u(left),
            ctx.fa().rzi_u(right),
        ).to_cond();

        let result = block_on(evaluate_condition(&mut ctx, &cond)).unwrap().unwrap();
        assert_eq!(expected, result);
    }

    #[rstest]
    #[case(4, 5, false)]
    #[case(5, 5, false)]
    #[case(6, 5, true)]
    fn test_arithmetic_condition_int_gt(
        #[case] left: i32,
        #[case] right: i32,
        #[case] expected: bool,
    ) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let cond = ArithmeticCondition::gt(
            ctx.fa().rzi_u(left),
            ctx.fa().rzi_u(right),
        ).to_cond();

        let result = block_on(evaluate_condition(&mut ctx, &cond)).unwrap().unwrap();

        assert_eq!(expected, result);
    }

    #[rstest]
    #[case(4, 5, false)]
    #[case(5, 5, true)]
    #[case(6, 5, true)]
    fn test_arithmetic_condition_int_gte(
        #[case] left: i32,
        #[case] right: i32,
        #[case] expected: bool,
    ) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let cond = ArithmeticCondition::gte(
            ctx.fa().rzi_u(left),
            ctx.fa().rzi_u(right),
        ).to_cond();

        let result = block_on(evaluate_condition(&mut ctx, &cond)).unwrap().unwrap();

        assert_eq!(expected, result);
    }

    #[rstest]
    #[case(4, 5, true)]
    #[case(5, 5, false)]
    #[case(6, 5, false)]
    fn test_arithmetic_condition_int_lt(
        #[case] left: i32,
        #[case] right: i32,
        #[case] expected: bool,
    ) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let cond = ArithmeticCondition::lt(
            ctx.fa().rzi_u(left),
            ctx.fa().rzi_u(right),
        ).to_cond();

        let result = block_on(evaluate_condition(&mut ctx, &cond)).unwrap().unwrap();

        assert_eq!(expected, result);
    }

    #[rstest]
    #[case(4, 5, true)]
    #[case(5, 5, true)]
    #[case(6, 5, false)]
    fn test_arithmetic_condition_int_lte(
        #[case] left: i32,
        #[case] right: i32,
        #[case] expected: bool,
    ) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let cond = ArithmeticCondition::lte(
            ctx.fa().rzi_u(left),
            ctx.fa().rzi_u(right),
        ).to_cond();

        let result = block_on(evaluate_condition(&mut ctx, &cond)).unwrap().unwrap();

        assert_eq!(expected, result);
    }
}
