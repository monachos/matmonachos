pub mod addition;
pub mod subtraction;
pub mod division;
pub mod multiplication;

use crate::calculator::context::CalcContext;
use crate::calculator::CalcResult;
use crate::calculator::ModelData;
use crate::calculator::CalcResultOrErr;
use matm_model::expressions::int::IntData;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;
use std::rc::Rc;

pub(crate) async fn calculate_int(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, IntData>,
) -> CalcResultOrErr {
    if model.data.radix() != ctx.rules().radix {
        // convert radix
        //TODO temporary solution
        if let Some(int_v) = model.data.natural().to_u32() {
            let mut digits: Vec<u8> = vec![];
            let mut v: u32 = int_v;
            loop {
                let digit = (v % ctx.rules().radix) as u8;
                v = v / ctx.rules().radix;
                digits.push(digit);
                if v == 0 {
                    break;
                }
            }
            let digits = digits.into_iter().rev().collect();
            let converted = IntData::from_vector(
                ctx.rules().radix,
                digits,
                model.data.sign())?;
            return Ok(CalcResult::one(converted.to_rc_expr()));
        }
    }
    Ok(CalcResult::original(Rc::clone(&model.orig)))
}


#[cfg(test)]
mod tests {
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::testing::asserts::test_calculation_ok;
    use matm_model::core::natural::RADIX_10;
    use matm_model::core::natural::RADIX_16;
    use matm_model::factories::ObjFactory;
    use matm_model::testing::asserts::assert_expression_that;
    use rstest::rstest;

    #[rstest]
    #[case(20)]
    #[case(0)]
    #[case(-20)]
    fn test_int_10_to_10(#[case] value: i32) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let expr = ctx.fa().rzi_u(value);

        let result = test_calculation_ok(&mut ctx, &expr);

        assert_expression_that(&result)
            .as_integer()
            .has_radix(10)
            .is_int(value);
    }

    #[rstest]
    #[case(20)]
    #[case(0)]
    #[case(-20)]
    fn test_int_16_to_10(#[case] value: i32) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let radix_fa = ObjFactory::new(RADIX_16);
        let expr = radix_fa.rzi_u(value);

        let result = test_calculation_ok(&mut ctx, &expr);

        assert_expression_that(&result)
            .as_integer()
            .has_radix(10)
            .is_int(value);
    }

    #[rstest]
    #[case(20)]
    #[case(0)]
    #[case(-20)]
    fn test_int_10_to_16(#[case] value: i32) {
        let mut ctx_data = CalcContextData::with_rules(|rules| {
            rules.radix = RADIX_16;
        });
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let radix_fa = ObjFactory::new(RADIX_10);
        let expr = radix_fa.rzi_u(value);

        let result = test_calculation_ok(&mut ctx, &expr);

        assert_expression_that(&result)
            .as_integer()
            .has_radix(16)
            .is_int(value);
    }
}
