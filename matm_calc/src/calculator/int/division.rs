use crate::calculator::tabular::division::calculate_tabular_division;
use crate::calculator::tabular::division::TabularDivisionResult;
use crate::calculator::context::CalcContext;
use matm_error::error::CalcError;
use matm_model::expressions::int::IntData;
use matm_model::expressions::sign::Sign;

/// Calculates division of two numbers
///
/// Returns (quotient, remainder)
pub async fn calculate_int_div(
    ctx: &mut CalcContext<'_>,
    v1: &IntData,
    v2: &IntData,
) -> Result<(IntData, IntData), CalcError> {
    if v2.is_zero() {
        return Err(CalcError::from_string(format!("Division of {} by zero.", v1)));
    }

    if v1.is_zero() {
        return Ok((IntData::zero(ctx.rules().radix)?, IntData::zero(ctx.rules().radix)?));
    }

    // non-zero values
    let negate = v1.sign() != v2.sign();
    match v1.natural().partial_cmp(v2.natural()) {
        Some(ord2) => {
            match ord2 {
                std::cmp::Ordering::Equal => {
                    return Ok((
                        ctx.fa().z1().calculate_neg_if(negate),
                        IntData::zero(ctx.rules().radix)?
                    ));
                }
                std::cmp::Ordering::Greater => {
                    let mut nested_ctx = CalcContext::new(
                        ctx.rules,
                        ctx.future_rules,
                        ctx.explanation.nested()?);
                    let div_res: TabularDivisionResult = calculate_tabular_division(
                        &mut nested_ctx,
                        v1.natural(),
                        v2.natural()).await?;
                    return Ok((
                        //quotient.neg_if(negate)
                        IntData::from_natural(
                            div_res.quotient,
                            if negate {
                                Sign::Negative
                            } else {
                                Sign::Positive
                            }),
                        //remainder.neg_if(v1.is_negative())
                        IntData::from_natural(
                            div_res.remainder,
                            if v1.is_negative() {
                                Sign::Negative
                            } else {
                                Sign::Positive
                            })
                    ));
                }
                std::cmp::Ordering::Less => {
                    return Ok((
                        IntData::zero(ctx.rules().radix)?,
                        v1.clone()
                    ));
                }
            }
        }
        None => {}
    }

    return Err(CalcError::from_string(format!("Cannot divide numbers {} / {}", v1, v2)));
}


#[cfg(test)]
mod tests {
    use crate::calculator::int::division::calculate_int_div;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use futures::executor::block_on;
    use matm_model::testing::asserts::assert_result_error_that;
    use matm_model::testing::asserts::assert_result_ok;
    use matm_model::testing::asserts::error_that::ErrorAssertThat;

    fn run_div_of_ints(n1: i32, n2: i32) -> (i32, i32) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let v1 = ctx.fa().zi_u(n1);
        let v2 = ctx.fa().zi_u(n2);

        let (quotient, remainder) = assert_result_ok(block_on(calculate_int_div(&mut ctx, &v1, &v2)));
        return (quotient.to_i32().unwrap(), remainder.to_i32().unwrap());
    }

    fn run_div_of_ints_error(n1: i32, n2: i32) -> ErrorAssertThat {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let v1 = ctx.fa().zi_u(n1);
        let v2 = ctx.fa().zi_u(n2);

        return assert_result_error_that(block_on(calculate_int_div(&mut ctx, &v1, &v2)));
    }

    #[test]
    fn test_div_10_2() {
        let (quotient, remainder) = run_div_of_ints(10, 2);
        assert_eq!(5, quotient);
        assert_eq!(0, remainder);
    }

    #[test]
    fn test_div_10_3() {
        let (quotient, remainder) = run_div_of_ints(10, 3);
        assert_eq!(3, quotient);
        assert_eq!(1, remainder);
    }

    #[test]
    fn test_div_10_10() {
        let (quotient, remainder) = run_div_of_ints(10, 10);
        assert_eq!(1, quotient);
        assert_eq!(0, remainder);
    }

    #[test]
    fn test_div_10_15() {
        assert_eq!(0, 10 / 15);
        assert_eq!(10, 10 % 15);

        let (quotient, remainder) = run_div_of_ints(10, 15);
        assert_eq!(0, quotient);
        assert_eq!(10, remainder);
    }

    #[test]
    fn test_div_10_0() {
        run_div_of_ints_error(10, 0)
            .has_message("Division of 10 by zero.");
    }

    #[test]
    fn test_div_10_m2() {
        let (quotient, remainder) = run_div_of_ints(10, -2);
        assert_eq!(-5, quotient);
        assert_eq!(0, remainder);
    }

    #[test]
    fn test_div_10_m3() {
        assert_eq!(-3, 10 / -3);
        assert_eq!(1, 10 % -3);

        let (quotient, remainder) = run_div_of_ints(10, -3);
        assert_eq!(-3, quotient);
        assert_eq!(1, remainder);
    }

    #[test]
    fn test_div_10_m10() {
        let (quotient, remainder) = run_div_of_ints(10, -10);
        assert_eq!(-1, quotient);
        assert_eq!(0, remainder);
    }

    #[test]
    fn test_div_10_m15() {
        assert_eq!(0, 10 / -15);
        assert_eq!(10, 10 % -15);

        let (quotient, remainder) = run_div_of_ints(10, -15);
        assert_eq!(0, quotient);
        assert_eq!(10, remainder);
    }

    #[test]
    fn test_div_0_2() {
        let (quotient, remainder) = run_div_of_ints(0, 2);
        assert_eq!(0, quotient);
        assert_eq!(0, remainder);
    }

    #[test]
    fn test_div_0_0() {
        run_div_of_ints_error(0, 0)
            .has_message("Division of 0 by zero.");
    }

    #[test]
    fn test_div_0_m2() {
        let (quotient, remainder) = run_div_of_ints(0, -2);
        assert_eq!(0, quotient);
        assert_eq!(0, remainder);
    }

    #[test]
    fn test_div_m10_2() {
        assert_eq!(-5, -10 / 2);
        assert_eq!(0, -10 % 2);

        let (quotient, remainder) = run_div_of_ints(-10, 2);
        assert_eq!(-5, quotient);
        assert_eq!(0, remainder);
    }

    #[test]
    fn test_div_m10_3() {
        assert_eq!(-3, -10 / 3);
        assert_eq!(-1, -10 % 3);

        let (quotient, remainder) = run_div_of_ints(-10, 3);
        assert_eq!(-3, quotient);
        assert_eq!(-1, remainder);
    }

    #[test]
    fn test_div_m10_10() {
        let (quotient, remainder) = run_div_of_ints(-10, 10);
        assert_eq!(-1, quotient);
        assert_eq!(0, remainder);
    }

    #[test]
    fn test_div_m10_15() {
        assert_eq!(0, -10 / 15);
        assert_eq!(-10, -10 % 15);

        let (quotient, remainder) = run_div_of_ints(-10, 15);
        assert_eq!(0, quotient);
        assert_eq!(-10, remainder);
    }

    #[test]
    fn test_div_m10_0() {
        run_div_of_ints_error(-10, 0)
            .has_message("Division of -10 by zero.");
    }

    #[test]
    fn test_div_m10_m2() {
        let (quotient, remainder) = run_div_of_ints(-10, -2);
        assert_eq!(5, quotient);
        assert_eq!(0, remainder);
    }

    #[test]
    fn test_div_m10_m3() {
        assert_eq!(3, -10 / -3);
        assert_eq!(-1, -10 % -3);

        let (quotient, remainder) = run_div_of_ints(-10, -3);
        assert_eq!(3, quotient);
        assert_eq!(-1, remainder);
    }

    #[test]
    fn test_div_m10_m10() {
        let (quotient, remainder) = run_div_of_ints(-10, -10);
        assert_eq!(1, quotient);
        assert_eq!(0, remainder);
    }

    #[test]
    fn test_div_m10_m15() {
        assert_eq!(0, -10 / -15);
        assert_eq!(-10, -10 % -15);

        let (quotient, remainder) = run_div_of_ints(-10, -15);
        assert_eq!(0, quotient);
        assert_eq!(-10, remainder);
    }
}
