use crate::calculator::int::subtraction::calculate_int_sub;
use crate::calculator::tabular::addition::calculate_tabular_addition;
use crate::calculator::context::CalcContext;
use async_recursion::async_recursion;
use matm_error::error::CalcError;
use matm_model::core::natural::Natural;
use matm_model::expressions::int::IntData;
use matm_model::expressions::sign::Sign;

#[async_recursion(?Send)]
pub async fn calculate_int_add(
    ctx: &mut CalcContext<'_>,
    v1: &IntData,
    v2: &IntData
) -> Result<IntData, CalcError> {
    if v1.is_zero() {
        return Ok(v2.clone());
    } else if v2.is_zero() {
        return Ok(v1.clone());
    }

    // non-zero values
    if v1.is_positive() && v2.is_positive() {
        // v1 + v2
        let mut nested_ctx = CalcContext::new(
            ctx.rules,
            ctx.future_rules,
            ctx.explanation.nested()?);
        let addends: Vec<Natural> = vec![v1.natural().clone(), v2.natural().clone()];
        let result = calculate_tabular_addition(&mut nested_ctx, &addends).await?;
        Ok(IntData::from_natural(result, Sign::Positive))
    } else if v1.is_positive() && v2.is_negative() {
        // v1 - n2
        let mut nested_ctx = CalcContext::new(
            ctx.rules,
            ctx.future_rules,
            ctx.explanation.nested()?);
        let n2 = v2.calculate_neg();
        let result = calculate_int_sub(&mut nested_ctx, v1, &n2).await?;
        Ok(result)
    } else if v1.is_negative() && v2.is_positive() {
        // v2 - n1
        let mut nested_ctx = CalcContext::new(
            ctx.rules,
            ctx.future_rules,
            ctx.explanation.nested()?);
        let n1 = v1.calculate_neg();
        let result = calculate_int_sub(&mut nested_ctx, v2, &n1).await?;
        Ok(result)
    } else {
        // -(n1 + n2)
        let mut nested_ctx = CalcContext::new(
            ctx.rules,
            ctx.future_rules,
            ctx.explanation.nested()?);
        let addends: Vec<Natural> = vec![v1.natural().clone(), v2.natural().clone()];
        let result = calculate_tabular_addition(&mut nested_ctx, &addends).await?;
        Ok(IntData::from_natural(result, Sign::Negative))
    }
}


#[cfg(test)]
mod tests {
    use crate::calculator::int::addition::calculate_int_add;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::result::dump_unicode_color;
    use futures::executor::block_on;
    use matm_model::testing::asserts::assert_result_ok;

    fn run_add_of_ints(n1: i32, n2: i32) -> i32 {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let v1 = ctx.fa().zi_u(n1);
        let v2 = ctx.fa().zi_u(n2);

        let result = assert_result_ok(block_on(calculate_int_add(&mut ctx, &v1, &v2)));

        dump_unicode_color(&ctx.explanation);
        result.to_i32().unwrap()
    }

    #[test]
    fn test_add_2_3() {
        let result = run_add_of_ints(2, 3);
        assert_eq!(5, result);
    }

    #[test]
    fn test_add_2_0() {
        let result = run_add_of_ints(2, 0);
        assert_eq!(2, result);
    }

    #[test]
    fn test_add_5_m2() {
        let result = run_add_of_ints(5, -2);
        assert_eq!(3, result);
    }

    #[test]
    fn test_add_5_m8() {
        let result = run_add_of_ints(5, -8);
        assert_eq!(-3, result);
    }

    #[test]
    fn test_add_0_2() {
        let result = run_add_of_ints(0, 2);
        assert_eq!(2, result);
    }

    #[test]
    fn test_add_0_0() {
        let result = run_add_of_ints(0, 0);
        assert_eq!(0, result);
    }

    #[test]
    fn test_add_0_m2() {
        let result = run_add_of_ints(0, -2);
        assert_eq!(-2, result);
    }

    #[test]
    fn test_add_m3_2() {
        let result = run_add_of_ints(-3, 2);
        assert_eq!(-1, result);
    }

    #[test]
    fn test_add_m3_5() {
        let result = run_add_of_ints(-3, 5);
        assert_eq!(2, result);
    }

    #[test]
    fn test_add_m3_0() {
        let result = run_add_of_ints(-3, 0);
        assert_eq!(-3, result);
    }

    #[test]
    fn test_add_m3_m2() {
        let result = run_add_of_ints(-3, -2);
        assert_eq!(-5, result);
    }

}
