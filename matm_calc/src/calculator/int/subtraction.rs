use crate::calculator::int::addition::calculate_int_add;
use crate::calculator::tabular::subtraction::calculate_tabular_subtraction;
use crate::calculator::context::CalcContext;
use matm_error::error::CalcError;
use matm_model::expressions::int::IntData;
use matm_model::expressions::sign::Sign;

pub async fn calculate_int_sub(
    ctx: &mut CalcContext<'_>,
    v1: &IntData,
    v2: &IntData
) -> Result<IntData, CalcError> {
    if v1.is_zero() {
        return Ok(v2.calculate_neg());
    } else if v2.is_zero() {
        return Ok(v1.clone());
    }

    // non-zero values
    if v1.is_positive() && v2.is_positive() {
        calculate_int_sub_positive_positive(ctx, v1, v2).await
    } else if v1.is_positive() && v2.is_negative() {
        calculate_int_sub_positive_negative(ctx, v1, v2).await
    } else if v1.is_negative() && v2.is_positive() {
        calculate_int_sub_negative_positive(ctx, v1, v2).await
    } else {
        calculate_int_sub_negative_negative(ctx, v1, v2).await
    }
}

async fn calculate_int_sub_positive_positive(
    ctx: &mut CalcContext<'_>,
    v1: &IntData,
    v2: &IntData
) -> Result<IntData, CalcError> {
    match v1.partial_cmp(v2) {
        Some(ord) => {
            match ord {
                std::cmp::Ordering::Equal => {
                    return Ok(IntData::zero(ctx.rules().radix)?);
                }
                std::cmp::Ordering::Greater => {
                    // v1 - v2
                    let mut nested_ctx = CalcContext::new(
                        ctx.rules,
                        ctx.future_rules,
                        ctx.explanation.nested()?);
                    let result = calculate_tabular_subtraction(
                        &mut nested_ctx,
                        v1.natural(),
                        v2.natural()).await?;
                    return Ok(IntData::from_natural(result, Sign::Positive));
                }
                std::cmp::Ordering::Less => {
                    // -(v2 - v1)
                    let mut nested_ctx = CalcContext::new(
                        ctx.rules,
                        ctx.future_rules,
                        ctx.explanation.nested()?);
                    let result = calculate_tabular_subtraction(
                        &mut nested_ctx,
                        v2.natural(),
                        v1.natural()).await?;
                    return Ok(IntData::from_natural(result, Sign::Negative));
                }
            }
        }
        None => {}
    }

    return Err(CalcError::from_string(format!("Cannot subtrace numbers {} - {}", v1, v2)));
}

async fn calculate_int_sub_positive_negative(
    ctx: &mut CalcContext<'_>,
    v1: &IntData,
    v2: &IntData
) -> Result<IntData, CalcError> {
    // v1 + n2
    let n2 = v2.calculate_neg();
    let result = calculate_int_add(ctx, v1, &n2).await?;
    return Ok(result);
}

async fn calculate_int_sub_negative_positive(
    ctx: &mut CalcContext<'_>,
    v1: &IntData,
    v2: &IntData
) -> Result<IntData, CalcError> {
    // -n1 - n2 = -(n1 + v2)
    let n1 = v1.calculate_neg();
    let result = calculate_int_add(ctx, &n1, v2).await?;
    return Ok(result.calculate_neg());
}

async fn calculate_int_sub_negative_negative(
    ctx: &mut CalcContext<'_>,
    v1: &IntData,
    v2: &IntData
) -> Result<IntData, CalcError> {
    match v1.natural().partial_cmp(v2.natural()) {
        Some(ord) => {
            match ord {
                std::cmp::Ordering::Equal => {
                    return Ok(IntData::zero(ctx.rules().radix)?);
                }
                std::cmp::Ordering::Greater => {
                    // -(n1 - n2)
                    let mut nested_ctx = CalcContext::new(
                        ctx.rules,
                        ctx.future_rules,
                        ctx.explanation.nested()?);
                    let result = calculate_tabular_subtraction(
                        &mut nested_ctx,
                        v1.natural(),
                        v2.natural()).await?;
                    return Ok(IntData::from_natural(result, Sign::Negative));
                }
                std::cmp::Ordering::Less => {
                    // n2 - n1
                    let mut nested_ctx = CalcContext::new(
                        ctx.rules,
                        ctx.future_rules,
                        ctx.explanation.nested()?);
                    let result = calculate_tabular_subtraction(
                        &mut nested_ctx,
                        v2.natural(),
                        v1.natural()).await?;
                    return Ok(IntData::from_natural(result, Sign::Positive));
                }
            }
        }
        None => {}
    }

    return Err(CalcError::from_string(format!("Cannot subtrace numbers {} - {}", v1, v2)));
}


#[cfg(test)]
mod tests {
    use crate::calculator::int::subtraction::calculate_int_sub;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use futures::executor::block_on;
    use matm_model::testing::asserts::assert_result_ok;

    fn run_sub_of_ints(n1: i32, n2: i32) -> i32 {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let v1 = ctx.fa().zi_u(n1);
        let v2 = ctx.fa().zi_u(n2);

        let result = assert_result_ok(block_on(calculate_int_sub(&mut ctx, &v1, &v2)));
        return result.to_i32().unwrap();
    }

    #[test]
    fn test_sub_2_3() {
        let result = run_sub_of_ints(2, 3);
        assert_eq!(-1, result);
    }

    #[test]
    fn test_sub_2_0() {
        let result = run_sub_of_ints(2, 0);
        assert_eq!(2, result);
    }

    #[test]
    fn test_sub_5_m2() {
        let result = run_sub_of_ints(5, -2);
        assert_eq!(7, result);
    }

    #[test]
    fn test_sub_0_2() {
        let result = run_sub_of_ints(0, 2);
        assert_eq!(-2, result);
    }

    #[test]
    fn test_sub_0_0() {
        let result = run_sub_of_ints(0, 0);
        assert_eq!(0, result);
    }

    #[test]
    fn test_sub_0_m2() {
        let result = run_sub_of_ints(0, -2);
        assert_eq!(2, result);
    }

    #[test]
    fn test_sub_m3_2() {
        let result = run_sub_of_ints(-3, 2);
        assert_eq!(-5, result);
    }

    #[test]
    fn test_sub_m3_0() {
        let result = run_sub_of_ints(-3, 0);
        assert_eq!(-3, result);
    }

    #[test]
    fn test_sub_m3_m2() {
        let result = run_sub_of_ints(-3, -2);
        assert_eq!(-1, result);
    }

    #[test]
    fn test_sub_m3_m3() {
        let result = run_sub_of_ints(-3, -3);
        assert_eq!(0, result);
    }

    #[test]
    fn test_sub_m3_m5() {
        let result = run_sub_of_ints(-3, -5);
        assert_eq!(2, result);
    }
}
