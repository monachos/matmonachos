use crate::calculator::tabular::multiplication::calculate_tabular_multiplication;
use crate::calculator::context::CalcContext;
use matm_error::error::CalcError;
use matm_model::expressions::int::IntData;
use matm_model::expressions::sign::Sign;

pub async fn calculate_int_mul(
    ctx: &mut CalcContext<'_>,
    v1: &IntData,
    v2: &IntData,
) -> Result<IntData, CalcError> {
    if v1.is_zero() || v2.is_zero() {
        return Ok(IntData::zero(ctx.rules().radix)?);
    }

    // non-zero values
    let negate = v1.sign() != v2.sign();
    let mut nested_ctx = CalcContext::new(
        ctx.rules,
        ctx.future_rules,
        ctx.explanation.nested()?);
    let result = calculate_tabular_multiplication(
        &mut nested_ctx,
        v1.natural(),
        v2.natural()).await?;
    return Ok(IntData::from_natural(
        result,
        if negate {
            Sign::Negative
        } else {
            Sign::Positive
        })
    );
}


#[cfg(test)]
mod tests {
    use crate::calculator::int::multiplication::calculate_int_mul;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use futures::executor::block_on;
    use matm_model::testing::asserts::assert_result_ok;

    fn run_mul_of_ints(n1: i32, n2: i32) -> i32 {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let v1 = ctx.fa().zi_u(n1);
        let v2 = ctx.fa().zi_u(n2);

        let result = assert_result_ok(block_on(calculate_int_mul(&mut ctx, &v1, &v2)));
        return result.to_i32().unwrap();
    }

    #[test]
    fn test_mul_2_3() {
        let result = run_mul_of_ints(2, 3);
        assert_eq!(6, result);
    }

    #[test]
    fn test_mul_2_0() {
        let result = run_mul_of_ints(2, 0);
        assert_eq!(0, result);
    }

    #[test]
    fn test_mul_2_m3() {
        let result = run_mul_of_ints(2, -3);
        assert_eq!(-6, result);
    }

    #[test]
    fn test_mul_0_2() {
        let result = run_mul_of_ints(0, 2);
        assert_eq!(0, result);
    }

    #[test]
    fn test_mul_0_0() {
        let result = run_mul_of_ints(0, 0);
        assert_eq!(0, result);
    }

    #[test]
    fn test_mul_0_m2() {
        let result = run_mul_of_ints(0, -2);
        assert_eq!(0, result);
    }

    #[test]
    fn test_mul_m3_2() {
        let result = run_mul_of_ints(-3, 2);
        assert_eq!(-6, result);
    }

    #[test]
    fn test_mul_m3_0() {
        let result = run_mul_of_ints(-3, 0);
        assert_eq!(0, result);
    }

    #[test]
    fn test_mul_m3_m2() {
        let result = run_mul_of_ints(-3, -2);
        assert_eq!(6, result);
    }
}
