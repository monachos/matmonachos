use crate::calculator::context::CalcContext;
use matm_error::error::CalcError;
use matm_model::expressions::int::IntData;
use matm_model::expressions::ToExpression;
use matm_model::statements::factorization::TrialDivisionFactor;
use matm_model::statements::factorization::TrialDivisionStep;
use matm_model::statements::text::TextStatement;
use matm_model::statements::ToStatement;

use super::int::addition::calculate_int_add;
use super::int::division::calculate_int_div;


pub async fn calculate_trial_division(
    ctx: &mut CalcContext<'_>,
    value: &IntData,
) -> Result<Vec<IntData>, CalcError> {
    let mut result: Vec<IntData> = Vec::new();

    if value.is_zero() {
        result.push(ctx.fa().z0());
        return Ok(result);
    }

    let mut data = TrialDivisionStep::new();
    if ctx.rules().explain_int_factorization {
        data.items.push(TrialDivisionFactor {
            dividend: value.clone(),
            divisor: None,
        });
    }

    let mut current_value = value.clone();
    let mut divisor = ctx.fa().zi(2)?;
    let one = ctx.fa().zi(1)?;
    while current_value.calculate_abs() > one {
        if ctx.rules().explain_int_factorization {
            ctx.explanation.add_statement(
                TextStatement::paragraph("Division {dividend} by {divisor}")
                .and_expression("dividend", current_value.clone().to_rc_expr())
                .and_expression("divisor", divisor.clone().to_rc_expr())
                .to_statement());
        }
        let (quotient, remainder) = calculate_int_div(ctx, &current_value, &divisor).await?;
        if remainder.is_zero() {
            // divisor divides current value
            result.push(divisor.clone());
            current_value = quotient;

            if ctx.rules().explain_int_factorization {
                if let Some(last) = data.items.last_mut() {
                    last.divisor = Some(divisor.clone());
                }
                data.items.push(TrialDivisionFactor {
                    dividend: current_value.clone(),
                    divisor: None,
                });
                ctx.explanation.add_statement(data.clone().to_statement());
            }
        } else {
            divisor = calculate_int_add(ctx, &divisor, &one).await?;
        }
    }

    if value.is_negative() {
        // additional step with -1
        let divisor = ctx.fa().zi(-1)?;
        result.push(divisor.clone());

        if ctx.rules().explain_int_factorization {
            ctx.explanation.add_statement(
                TextStatement::paragraph("Division {dividend} by {divisor}")
                .and_expression("dividend", current_value.clone().to_rc_expr())
                .and_expression("divisor", divisor.clone().to_rc_expr())
                .to_statement());
            if let Some(last) = data.items.last_mut() {
                last.divisor = Some(divisor.clone());
            }
            data.items.push(TrialDivisionFactor {
                dividend: one,
                divisor: None,
            });
            ctx.explanation.add_statement(data.clone().to_statement());
        }
    }

    return Ok(result);
}