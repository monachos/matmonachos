use std::rc::Rc;

use crate::calculator::linear::rank::calculate_rank;
use crate::calculator::linear::rank::CalculateRankConfig;
use crate::calculator::context::CalcContext;
use crate::calculator::CalcResult;
use crate::calculator::CalcResultOrErr;
use matm_model::expressions::Expression;

pub async fn calculate_unary_rank(
    ctx: &mut CalcContext<'_>,
    value: CalcResult<Rc<Expression>>,
) -> CalcResultOrErr {
    let rank_cfg = CalculateRankConfig {
        matrix_name: String::from("A"),//TODO why A?
    };

    match &value.object.as_ref() {
        Expression::Matrix(mx) => {
            let mut nested_ctx = CalcContext::new(
                ctx.rules,
                ctx.future_rules,
                ctx.explanation.nested()?);
            let rank_value = calculate_rank(&mut nested_ctx, &rank_cfg, mx).await?;
            return Ok(CalcResult::one(ctx.fa().rzu(rank_value)?));
        }
        _ => {
            //TODO error
        }
    }

    Ok(value)
}


#[cfg(test)]
mod tests {
    use crate::calculator::calculate_expression;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::matrix;
    use crate::result::dump_unicode_color;
    use crate::testing::asserts::assert_next_step;
    use crate::testing::asserts::assert_next_step_that;
    use crate::testing::asserts::assert_none_step;
    use futures::executor::block_on;
    use matm_model::expressions::unary::UnaryData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;

    // Example from "Algebra liniowa w zadaniach", Jerzy Rutkowski, PWN 2008, page 97
    #[test]
    fn test_rank() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = UnaryData::rank(
            matrix!(ctx.fa(), 4, 5;
                    1, 2, 3, 4, 1,
                    2, 5, 5, 7, 2,
                    4, 6, 8, 9, 3,
                    3, 5, 4, 4, 2
                ).unwrap().to_rc_expr(),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(3);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_integer()
            .is_int(3);
        assert_none_step(&mut steps);
    }
}
