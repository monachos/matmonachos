use crate::calculator::CalcResult;
use crate::calculator::CalcResultOrErr;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;
use std::rc::Rc;

pub async fn calculate_unary_transposition(
    value: CalcResult<Rc<Expression>>,
) -> CalcResultOrErr {
    //TODO test
    match &value.object.as_ref() {
        Expression::Matrix(matrix) => {
            let calc_matrix = matrix.transpose();
            let calc_matrix_expr = calc_matrix.to_rc_expr();
            Ok(CalcResult::new(calc_matrix_expr, 1))
        }
        _ => {
            // not supported transposition of other type than matrix
            Ok(value)
        }
    }
}


#[cfg(test)]
mod tests {
    use crate::calculator::calculate_expression;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::matrix;
    use crate::result::dump_unicode_color;
    use crate::testing::asserts::assert_next_step;
    use crate::testing::asserts::assert_next_step_that;
    use crate::testing::asserts::assert_none_step;
    use futures::executor::block_on;
    use matm_model::expressions::unary::UnaryData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;

    #[test]
    fn test_transpose() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = UnaryData::transpose(
            matrix!(ctx.fa(), 2, 1;
                (1/2),
                (5/15)
            ).unwrap().to_rc_expr(),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .is_i_i_fraction_that(1, 1, |f| {f.is_ints(1, 2);})
            .is_i_i_fraction_that(1, 2, |f| {f.is_ints(1, 3);});

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_unary()
            .is_transpose()
            .as_matrix()
            .is_i_i_fraction_that(1, 1, |f| {f.is_ints(1, 2);})
            .is_i_i_fraction_that(2, 1, |f| {f.is_ints(1, 3);});
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_matrix()
            .is_i_i_fraction_that(1, 1, |f| {f.is_ints(1, 2);})
            .is_i_i_fraction_that(1, 2, |f| {f.is_ints(1, 3);});
        assert_none_step(&mut steps);
    }
}
