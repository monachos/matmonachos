use std::rc::Rc;

use crate::calculator::linear::det::calculate_determinant;
use crate::calculator::linear::det::CalculateDeterminantConfig;
use crate::calculator::context::CalcContext;
use crate::calculator::CalcResult;
use crate::calculator::ModelData;
use crate::calculator::CalcResultOrErr;
use matm_model::expressions::symbol::SymbolData;
use matm_model::expressions::unary::UnaryData;
use matm_model::expressions::Expression;

pub async fn calculate_unary_determinant(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, UnaryData>,
) -> CalcResultOrErr {
    let matrix_symbol = match model.data.value.as_ref() {
        Expression::Symbol(sym) => sym.as_ref().clone(),
        _ => SymbolData::new("A"),
    };
    let det_cfg = CalculateDeterminantConfig::new(matrix_symbol);

    match model.data.value.as_ref() {
        Expression::Matrix(mx) => {
            let det_value = calculate_determinant(ctx, &det_cfg, mx).await?;
            return Ok(CalcResult::one(det_value));
        }
        //TODO what about determinant of scalar
        _ => {
        }
    }
    Ok(CalcResult::original(Rc::clone(model.orig)))
}


#[cfg(test)]
mod tests {
    use crate::calculator::calculate_expression;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::matrix;
    use crate::result::dump_unicode_color;
    use crate::testing::asserts::assert_next_step;
    use crate::testing::asserts::assert_next_step_that;
    use crate::testing::asserts::assert_none_step;
    use futures::executor::block_on;
    use matm_model::expressions::unary::UnaryData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;

    #[test]
    fn test_determinant() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = UnaryData::det(
            matrix!(ctx.fa(), 3, 3;
                    1, 2, 3,
                    4, 1, 6,
                    1, 3, 3
                    ).unwrap().to_rc_expr(),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(6);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_integer()
            .is_int(6);
        assert_none_step(&mut steps);
    }
}
