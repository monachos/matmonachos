use crate::calculator::context::CalcContext;
use crate::calculator::CalcResult;
use crate::calculator::ModelData;
use crate::calculator::CalcResultOrErr;
use matm_model::expressions::fractions::FracData;
use matm_model::expressions::matrices::MatrixData;
use matm_model::expressions::unary::UnaryData;
use matm_model::expressions::unary::UnaryOperator;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;
use std::rc::Rc;

pub async fn calculate_unary_neg(
    _ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, Expression>,
) -> CalcResultOrErr {
    match model.data {
        Expression::Int(o) => {
            let res_expr = o.calculate_neg().to_rc_expr();
            return Ok(CalcResult::one(res_expr));
        }
        Expression::Frac(o) => {
            let res_expr = FracData::new(
                UnaryData::neg(Rc::clone(&o.numerator)).to_rc_expr(),
                Rc::clone(&o.denominator)
            ).to_rc_expr();
            return Ok(CalcResult::one(res_expr));
        }
        Expression::Matrix(o) => {
            let neg_elements = o.iter_right_down()
                .map(|idx| UnaryData::neg(Rc::clone(o.get(&idx).unwrap())).to_rc_expr())
                .collect();
            let res_expr = MatrixData::from_vector(o.rows(), o.columns(), neg_elements)?.to_rc_expr();
            return Ok(CalcResult::one(res_expr));
        }
        Expression::Unary(o) => {
            match o.operator {
                UnaryOperator::Neg => {
                    let res_expr = Rc::clone(&o.value);
                    return Ok(CalcResult::one(res_expr));
                }
                _ => {}
            }
        }
        _ => {}
    }

    Ok(CalcResult::original(Rc::clone(model.orig)))
}


#[cfg(test)]
mod tests {
    use crate::calculator::calculate_expression;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::matrix;
    use crate::result::dump_unicode_color;
    use futures::executor::block_on;
    use matm_model::expressions::unary::UnaryData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;
    use rstest::rstest;

    #[test]
    fn test_neg_int() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = UnaryData::neg(ctx.fa().rzi_u(3)).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(-3);
    }

    #[rstest]
    #[case((2, 3), (-2, 3))]
    #[case((-2, 3), (2, 3))]
    #[case((2, -3), (2, 3))]
    #[case((-2, -3), (-2, 3))]
    fn test_neg_frac(
        #[case] input: (i32, i32),
        #[case] expected: (i32, i32),
    ) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = UnaryData::neg(ctx.fa().rfi_u(input.0, input.1)).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_fraction()
            .is_ints(expected.0, expected.1);
    }

    #[test]
    fn test_neg_mx() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = UnaryData::neg(matrix!(ctx.fa(), 2, 2;
            1, 2, 3, 4).unwrap().to_rc_expr()).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .is_i_i_integer_that(1, 1, |e| {e.is_int(-1);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(-2);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(-3);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(-4);});
    }

    #[test]
    fn test_neg_symbol() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = UnaryData::neg(ctx.fa().rsn("x")).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .is_equal_to(&UnaryData::neg(ctx.fa().rsn("x")).to_expr());
    }

    #[test]
    fn test_neg_neg_int() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = UnaryData::neg(UnaryData::neg(ctx.fa().rzi_u(2)).to_rc_expr()).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(2);
    }

    #[test]
    fn test_neg_neg_symbol() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = UnaryData::neg(UnaryData::neg(ctx.fa().rsn("x")).to_rc_expr()).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_symbol()
            .has_name("x");
    }
}
