use crate::calculator::CalcResult;
use crate::calculator::CalcResultOrErr;
use matm_model::expressions::unary::UnaryData;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;
use std::rc::Rc;

pub async fn calculate_unary_group(
    value: CalcResult<Rc<Expression>>,
) -> CalcResultOrErr {
    match &value.object.as_ref() {
        Expression::Undefined |
        Expression::Assignment(_) |
        Expression::Bool(_) |
        Expression::BoxSign |
        Expression::Equation(_) |
        Expression::SysOfEq(_) |
        Expression::Int(_) |
        Expression::Frac(_) |
        Expression::Cracovian(_) |
        Expression::Matrix(_) |
        Expression::Symbol(_) |
        Expression::Unary(_) |
        Expression::Function(_) |
        Expression::Method(_) |
        Expression::Object(_) |
        Expression::Exp(_) |
        Expression::Root(_) |
        Expression::Log(_) |
        Expression::IteratedBinary(_) |
        Expression::Im(_) |
        Expression::Transc(_) |
        Expression::Variable(_) |
        Expression::Conditional(_) |
        Expression::Binomial(_)
        => Ok(CalcResult::one(value.object)),
        Expression::Binary(_) => {
            let res_expr = UnaryData::group(value.object).to_rc_expr();
            Ok(CalcResult::new(res_expr, value.calculated_count))
        }
    }
}


#[cfg(test)]
mod tests {
    use crate::calculator::calculate_expression;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::result::dump_unicode_color;
    use crate::testing::asserts::assert_next_step;
    use crate::testing::asserts::assert_next_step_that;
    use crate::testing::asserts::assert_none_step;
    use futures::executor::block_on;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::unary::UnaryData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;

    #[test]
    fn test_group() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = UnaryData::group(
            BinaryData::add(
                ctx.fa().rfi_u(2, 5),
                ctx.fa().rfi_u(3, 5),
            ).to_rc_expr()
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(1);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(2)
            .has_format("(2 + 3)/5");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .is_ints(5, 5);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_integer()
            .is_int(1);
        assert_none_step(&mut steps);
    }

    /// testing (3) => 3
    #[test]
    fn test_group_remove_from_int() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = UnaryData::group(ctx.fa().rzi_u(3)).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(3);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_integer()
            .is_int(3);
        assert_none_step(&mut steps);
    }

    /// testing (1/2) => 1/2
    #[test]
    fn test_group_remove_from_frac() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = UnaryData::group(ctx.fa().rfi_u(1, 2)).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_fraction()
            .is_ints(1, 2);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .is_ints(1, 2);
        assert_none_step(&mut steps);
    }

    /// testing (1+2) => 3
    #[test]
    fn test_group_remove_from_binary_1_2() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = UnaryData::group(
            BinaryData::add(ctx.fa().rzi_u(1), ctx.fa().rzi_u(2)).to_rc_expr()
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(3);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(2)
            .as_integer()
            .is_int(3);
        assert_none_step(&mut steps);
    }

    /// testing (1+2/2) => (1+1)
    #[test]
    fn test_group_remove_from_binary_1_p_22() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = UnaryData::group(
            BinaryData::add(
                ctx.fa().rzi_u(1),
                ctx.fa().rfi_u(2, 2)
            ).to_rc_expr()
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(2);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_unary()
            .is_group()
            .has_format("(1 + 1)");
        assert_next_step_that(&mut steps)
            .has_calculation_count(2)
            .as_integer()
            .is_int(2);
        assert_none_step(&mut steps);
    }
}
