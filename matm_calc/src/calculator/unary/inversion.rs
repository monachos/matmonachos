use std::rc::Rc;

use crate::calculator::linear::inversion::invert_matrix_using_adjugate;
use crate::calculator::linear::inversion::InvertMatrixUsingAdjugateConfig;
use crate::calculator::CalcResult;
use crate::calculator::ModelData;
use crate::calculator::CalcResultOrErr;
use crate::calculator::context::CalcContext;
use matm_model::expressions::unary::UnaryData;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;

//TODO test
pub async fn calculate_unary_inversion(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, UnaryData>,
) -> CalcResultOrErr {
    match model.data.value.as_ref() {
        Expression::Matrix(mx) => {
            //TODO select used method
            let mut nested_ctx = CalcContext::new(
                ctx.rules,
                ctx.future_rules,
                ctx.explanation.nested()?);
            let cfg = InvertMatrixUsingAdjugateConfig::default();
            let inv_mx = invert_matrix_using_adjugate(&mut nested_ctx, &cfg, &mx).await?;
            return Ok(CalcResult::one(inv_mx.to_rc_expr()));
        }
        _ => {
            //TODO error
        }
    }
    Ok(CalcResult::original(Rc::clone(model.orig)))
}


#[cfg(test)]
mod tests {
    //TODO test

}
