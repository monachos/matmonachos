mod group;
mod neg;
mod transposition;
mod determinant;
mod rank;
mod inversion;
mod factorial;

use crate::calculator::calculate_expression_step;
use crate::calculator::context::CalcContext;
use crate::calculator::unary::determinant::calculate_unary_determinant;
use crate::calculator::unary::factorial::calculate_unary_factorial;
use crate::calculator::unary::group::calculate_unary_group;
use crate::calculator::unary::inversion::calculate_unary_inversion;
use crate::calculator::unary::neg::calculate_unary_neg;
use crate::calculator::unary::rank::calculate_unary_rank;
use crate::calculator::unary::transposition::calculate_unary_transposition;
use crate::calculator::CalcResult;
use crate::calculator::CalcResultOrErr;
use crate::calculator::ModelData;
use matm_model::expressions::unary::UnaryData;
use matm_model::expressions::unary::UnaryOperator;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;


pub async fn calculate_unary_expression(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, UnaryData>,
) -> CalcResultOrErr {
    let value = calculate_expression_step(ctx, &model.data.value).await?;
    if value.calculated_count > 0 {
        //TODO test all conditions
        return if is_group_unary_expression_redundant(model.data, &value.object).await {
            Ok(CalcResult::new(value.object, value.calculated_count + 1))
        } else {
            let expr = UnaryData::from_operator(model.data.operator, value.object).to_rc_expr();
            Ok(CalcResult::new(expr, value.calculated_count))
        }
    }

    match model.data.operator {
        UnaryOperator::Group => calculate_unary_group(value).await,
        UnaryOperator::Neg => calculate_unary_neg(ctx, &ModelData::new(model.orig, &model.data.value)).await,
        UnaryOperator::Transpose => calculate_unary_transposition(value).await,
        UnaryOperator::Determinant => calculate_unary_determinant(ctx, model).await,
        UnaryOperator::Rank => calculate_unary_rank(ctx, value).await,
        UnaryOperator::Inversion => calculate_unary_inversion(ctx, model).await,
        UnaryOperator::Factorial => calculate_unary_factorial(ctx, &ModelData::new(model.orig, &model.data.value)).await,
    }
}

//TODO test
/// checks if used expression is not composed and parentheses can be removed
async fn is_group_unary_expression_redundant(
    expr: &UnaryData,
    inner_expr: &Expression,
) -> bool {
    match expr.operator {
        UnaryOperator::Group => {
            match inner_expr {
                Expression::Undefined => true,
                Expression::Assignment(_) => true,
                Expression::Bool(_) => true,
                Expression::BoxSign => true,
                Expression::Equation(_) => true,
                Expression::SysOfEq(_) => true,
                Expression::Int(_) => true,
                Expression::Frac(_) => true,
                Expression::Cracovian(_) => false,
                Expression::Matrix(_) => false,
                Expression::Symbol(_) => false,
                Expression::Unary(unary_expr) =>
                    match unary_expr.operator {
                        UnaryOperator::Group => true,
                        _ => false,
                    },
                Expression::Function(_) => false,
                Expression::Method(_) => false,
                Expression::Object(_) => true,
                Expression::Exp(_) => true,
                Expression::Root(_) => true,
                Expression::Log(_) => false,
                Expression::Binary(_) => false,
                Expression::IteratedBinary(_) => false,
                Expression::Im(_) => true,
                Expression::Transc(_) => true,
                Expression::Variable(_) => true,
                Expression::Conditional(_) => true,
                Expression::Binomial(_) => true,
            }
        }
        _ => {
            false
        }
    }
}
