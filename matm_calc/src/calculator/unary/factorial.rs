use std::rc::Rc;

use crate::calculator::context::CalcContext;
use crate::calculator::CalcResult;
use crate::calculator::ModelData;
use crate::calculator::CalcResultOrErr;
use matm_error::error::CalcError;
use matm_model::expressions::iterated_binary::IteratedBinaryData;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;

pub async fn calculate_unary_factorial(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, Expression>,
) -> CalcResultOrErr {
    match model.data {
        Expression::Int(value) => {
            if value.is_negative() {
                return Err(CalcError::from_string(format!(
                    "Cannot calculate factorial of negative value: {}.",
                    model.data)));
            }
            match value.to_i32() {
                Some(int_value) => {
                    if int_value == 0 {
                        let res_expr = ctx.fa().rz1();
                        Ok(CalcResult::one(res_expr))
                    } else if int_value == 1 {
                        let res_expr = ctx.fa().rz1();
                        Ok(CalcResult::one(res_expr))
                    } else if int_value > 1000 {
                        Err(CalcError::from_string(format!(
                            "Cannot calculate factorial of too big value: {}.",
                            model.data)))
                    } else {
                        let result = IteratedBinaryData::product(
                            ctx.fa().sn("k"),
                            ctx.fa().rzi(2)?,
                            ctx.fa().rzi(int_value)?,
                            ctx.fa().rsn("k")).to_rc_expr();
                        Ok(CalcResult::one(result))
                    }
                }
                None => {
                    Err(CalcError::from_string(format!(
                        "Cannot calculate factorial of too big value: {}.",
                        model.data)))
                }
            }
        }
        _ => {
            Ok(CalcResult::original(Rc::clone(model.orig)))
        }
    }
}


#[cfg(test)]
mod tests {
    use crate::calculator::calculate_expression;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::result::dump_unicode_color;
    use futures::executor::block_on;
    use matm_model::expressions::int::IntData;
    use matm_model::expressions::unary::UnaryData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;
    use matm_model::testing::asserts::assert_result_error_that;

    #[test]
    fn test_factorial_0() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = UnaryData::factorial(ctx.fa().rzi_u(0)).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(1);
    }

    #[test]
    fn test_factorial_1() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = UnaryData::factorial(ctx.fa().rzi_u(1)).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(1);
    }

    #[test]
    fn test_factorial_6() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let expr = UnaryData::factorial(ctx.fa().rzi_u(6)).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(720);
    }

    #[test]
    fn test_factorial_50() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = UnaryData::factorial(ctx.fa().rzi_u(50)).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_positive()
            .is_equal_to(ctx.fa().zs_u("30414093201713378043612608166064768844377641568960512000000000000"));
    }

    #[test]
    fn test_factorial_negative() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = UnaryData::factorial(ctx.fa().rzi_u(-1)).to_rc_expr();
        assert_result_error_that(block_on(calculate_expression(&mut ctx, &expr)))
            .has_message("Cannot calculate factorial of negative value: -1.");
    }

    #[test]
    fn test_factorial_too_big_i32() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = UnaryData::factorial(ctx.fa().rzi_u(1001)).to_rc_expr();
        assert_result_error_that(block_on(calculate_expression(&mut ctx, &expr)))
            .has_message("Cannot calculate factorial of too big value: 1001.");
    }

    #[test]
    fn test_factorial_too_big_int() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = UnaryData::factorial(IntData::from_string(ctx.rules().radix, "100000000000000000000000000000000000000000000000000").unwrap().to_rc_expr())
            .to_rc_expr();
        assert_result_error_that(block_on(calculate_expression(&mut ctx, &expr)))
            .has_message("Cannot calculate factorial of too big value: 100000000000000000000000000000000000000000000000000.");
    }
}
