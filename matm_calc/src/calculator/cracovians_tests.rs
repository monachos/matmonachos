#[cfg(test)]
mod tests {
    use crate::calculator::context::CalcContext;
    use crate::calculator::calculate_expression;
    use crate::calculator::context::CalcContextData;
    use crate::result::dump_unicode_color;
    use crate::testing::asserts::assert_next_step_that;
    use crate::testing::asserts::assert_none_step;
    use futures::executor::block_on;
    use matm_model::expressions::cracovians::CracovianData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;


    #[test]
    fn test_cracovian_calculate() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = CracovianData::from_vector(2, 2, vec![
            ctx.fa().rfi_u(1, 5), ctx.fa().rfi_u(2, 5),
            ctx.fa().rfi_u(3, 6), ctx.fa().rfi_u(4, 2)
            ]).unwrap().to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_cracovian()
            .has_rows(2)
            .has_columns(2)
            .is_i_i_fraction_that(1, 1, |e| {e.is_ints(1, 5);})
            .is_i_i_fraction_that(2, 1, |e| {e.is_ints(2, 5);})
            .is_i_i_fraction_that(1, 2, |e| {e.is_ints(1, 2);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(2);});

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .as_cracovian()
            .has_rows(2)
            .has_columns(2)
            .is_i_i_fraction_that(1, 1, |e| {e.is_ints(1, 5);})
            .is_i_i_fraction_that(2, 1, |e| {e.is_ints(2, 5);})
            .is_i_i_fraction_that(1, 2, |e| {e.is_ints(3, 6);})
            .is_i_i_fraction_that(2, 2, |e| {e.is_ints(4, 2);});
        assert_next_step_that(&mut steps)
            .has_calculation_count(2)
            .as_cracovian()
            .has_rows(2)
            .has_columns(2)
            .is_i_i_fraction_that(1, 1, |e| {e.is_ints(1, 5);})
            .is_i_i_fraction_that(2, 1, |e| {e.is_ints(2, 5);})
            .is_i_i_fraction_that(1, 2, |e| {e.is_ints(1, 2);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(2);});
        assert_none_step(&mut steps);
    }

}