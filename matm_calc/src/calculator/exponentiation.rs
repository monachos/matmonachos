use std::rc::Rc;

use crate::calculator::calculate_expression_step;
use crate::calculator::ModelData;
use crate::calculator::CalcResultOrErr;
use crate::calculator::CalcResult;
use crate::calculator::context::CalcContext;
use matm_error::error::CalcError;
use matm_model::expressions::binary::BinaryData;
use matm_model::expressions::exponentiation::ExpData;
use matm_model::expressions::int::IntData;
use matm_model::expressions::matrices::MatrixData;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;


pub async fn calculate_exp_expression(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, ExpData>,
) -> CalcResultOrErr {
    let base_value = calculate_expression_step(ctx, &model.data.base).await?;
    let exponent_value = calculate_expression_step(ctx, &model.data.exponent).await?;
    let total_calculated = base_value.calculated_count + exponent_value.calculated_count;
    if total_calculated > 0 {
        let expr = ExpData::new(base_value.object, exponent_value.object).to_rc_expr();
        return Ok(CalcResult::new(expr, total_calculated));
    }

    // calculate mx ^ int
    match (model.data.base.as_ref(), model.data.exponent.as_ref()) {
        (Expression::Matrix(base_value), Expression::Int(exponent_value)) => {
           return calculate_exp_mx_to_int(ctx, base_value, exponent_value).await;
        }
        _ => {}
    }

    // calculate exp ^ int
    match model.data.exponent.as_ref() {
        Expression::Int(exponent_value) => {
           return calculate_exp_expr_int(ctx, &model.data.base, exponent_value).await;
        }
        _ => {}
    }

    Ok(CalcResult::original(Rc::clone(&model.orig)))
}

async fn calculate_exp_expr_int(
    ctx: &mut CalcContext<'_>,
    base: &Rc<Expression>,
    exponent_value: &IntData,
) -> CalcResultOrErr {
    if exponent_value.is_positive() {
        calculate_exp_expr_to_positive_int(ctx, base, exponent_value).await
    } else if exponent_value.is_negative() {
        calculate_exp_expr_to_negative_int(ctx, base, exponent_value).await
    } else {
        // exponent is zero
        let res_expr = ctx.fa().rz1();
        return Ok(CalcResult::one(res_expr));
    }
}

async fn calculate_exp_expr_to_positive_int(
    _ctx: &mut CalcContext<'_>,
    base: &Rc<Expression>,
    exponent_value: &IntData,
) -> CalcResultOrErr {
    if exponent_value.is_one() {
        return Ok(CalcResult::one(Rc::clone(base)));
    }
    if base.is_zero() || base.is_one() {
        return Ok(CalcResult::one(Rc::clone(base)));
    }
    //TODO add case base.is_minus_one()
    // if base.is_minus_one() {
    //     let res_expr = if exponent_value % 2 == 0 {
    //         ctx.fa().ezi(1)
    //     } else {
    //         ctx.fa().ezi(-1);
    //     };
    // }

    match exponent_value.to_i32() {
        Some(exp32) => {
            if exp32 > 1000 {//TODO define limit
                return Err(CalcError::from_string(format!(
                    "Invalid exponent value: {}",
                    exponent_value)));
            }
            let mut factors = base.clone();
            for _ in 2..=exp32 {
                factors = BinaryData::mul(factors, Rc::clone(base)).to_rc_expr();
            }
            return Ok(CalcResult::one(factors));
        }
        None => {
            return Err(CalcError::from_string(format!(
                "Invalid exponent value: {}",
                exponent_value)));
        }
    }
}

async fn calculate_exp_expr_to_negative_int(
    ctx: &mut CalcContext<'_>,
    base: &Rc<Expression>,
    exponent_value: &IntData,
) -> CalcResultOrErr {
    if exponent_value.is_minus_one() {
        let res_expr = ctx.fa().rfr(
            ctx.fa().rz1(),
            Rc::clone(base)
        );
        return Ok(CalcResult::one(res_expr));
    }
    if base.is_one() {
        return Ok(CalcResult::one(Rc::clone(base)));
    }
    //TODO add case base.is_minus_one()
    let res_expr = ctx.fa().rfr(
        ctx.fa().rz1(),
        ExpData::new(
            Rc::clone(base),
            ctx.fa().rz(exponent_value.calculate_neg())).to_rc_expr()
    );
    return Ok(CalcResult::one(res_expr));
}

async fn calculate_exp_mx_to_int(
    ctx: &mut CalcContext<'_>,
    base: &MatrixData,
    exponent_value: &IntData,
) -> CalcResultOrErr {
    if !base.is_square() {
        return Err(CalcError::from_str("Matrix is not square."));
    }

    if exponent_value.is_positive() {
        if let Some(exp32) = exponent_value.to_i32() {//TODO extract usize
            if exp32 > 1000 {//TODO define limit
                return Err(CalcError::from_string(format!(
                    "Invalid exponent value: {}",
                    exponent_value)));
            }
            let mut res_mx = base.clone().to_rc_expr();
            for _ in 2..=exp32 {
                res_mx = BinaryData::mul(
                    res_mx,
                    base.clone().to_rc_expr()
                ).to_rc_expr();
            }
            return Ok(CalcResult::one(res_mx));
        }
        return Err(CalcError::not_supported());
    } else if exponent_value.is_negative() {
        return Err(CalcError::not_supported());
    } else {
        let res_expr = MatrixData::identity(ctx.rules().radix, base.rows())?.to_rc_expr();
        return Ok(CalcResult::one(res_expr));
    }
}
