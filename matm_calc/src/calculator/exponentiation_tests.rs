#[cfg(test)]
mod tests {
    use futures::executor::block_on;
    use std::rc::Rc;

    use crate::calculator::calculate_expression;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::matrix;
    use crate::result::dump_unicode_color;
    use crate::rules::CalcRules;
    use matm_model::expressions::exponentiation::ExpData;
    use matm_model::expressions::Expression;
    use matm_model::expressions::ToExpression;
    use matm_model::factories::ObjFactory;
    use matm_model::testing::asserts::assert_expression_that;
    use matm_model::testing::asserts::assert_result_error_that;
    use matm_model::testing::asserts::assert_result_ok;
    use matm_model::testing::asserts::error_that::ErrorAssertThat;
    use once_cell::sync::Lazy;
    use rstest::rstest;


    const DEFAULT_RULES: Lazy<CalcRules> = Lazy::new(|| {CalcRules::default()});
    const DEFAULT_FA: Lazy<ObjFactory> = Lazy::new(|| {ObjFactory::new(DEFAULT_RULES.radix)});

    fn run_exp(
        ctx: &mut CalcContext<'_>,
        base: Rc<Expression>,
        exponent: Rc<Expression>
    ) -> Rc<Expression> {
        let expr = ExpData::new(base, exponent).to_rc_expr();
        let result = block_on(calculate_expression(ctx, &expr));

        println!("Testing: {}", expr);
        dump_unicode_color(&ctx.explanation);
        println!();
        println!("-------------");

        // check result
        assert_result_ok(result)
    }

    fn run_exp_err(
        ctx: &mut CalcContext<'_>,
        base: Rc<Expression>,
        exponent: Rc<Expression>,
    ) -> ErrorAssertThat {
        let expr = ExpData::new(base, exponent).to_rc_expr();
        let result = block_on(calculate_expression(ctx, &expr));

        println!("Testing: {}", expr);
        dump_unicode_color(&ctx.explanation);
        println!();
        println!("-------------");

        // check result
        assert_result_error_that(result)
    }

    #[rstest]
    #[case(-2, 0, 1)]
    #[case(-1, 0, 1)]
    #[case(0, 0, 1)]
    #[case(1, 0, 1)]
    #[case(2, 0, 1)]
    fn test_exp_int_0(
        #[case] base_input: i32,
        #[case] exponent_input: i32,
        #[case] expected: i32,
    ) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let base = ctx.fa().rzi_u(base_input);
        let exponent = ctx.fa().rzi_u(exponent_input);

        let result = run_exp(&mut ctx, base, exponent);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(expected);
    }

    #[rstest]
    #[case(-2, 1, -2)]
    #[case(-1, 1, -1)]
    #[case(0, 1, 0)]
    #[case(1, 1, 1)]
    #[case(2, 1, 2)]
    #[case(10, 1, 10)]
    #[case(-2, 2, 4)]
    #[case(-1, 2, 1)]
    #[case(0, 2, 0)]
    #[case(1, 2, 1)]
    #[case(2, 2, 4)]
    #[case(10, 2, 100)]
    #[case(-2, 5, -32)]
    #[case(-1, 5, -1)]
    #[case(0, 5, 0)]
    #[case(1, 5, 1)]
    #[case(2, 5, 32)]
    #[case(10, 5, 100_000)]
    fn test_exp_int_posint(
        #[case] base_input: i32,
        #[case] exponent_input: i32,
        #[case] expected: i32,
    ) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let base = ctx.fa().rzi_u(base_input);
        let exponent = ctx.fa().rzi_u(exponent_input);

        let result = run_exp(&mut ctx, base, exponent);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(expected);
    }

    #[rstest]
    #[case(-2, -1, DEFAULT_FA.efi_u(-1, 2))]
    #[case(1, -1, DEFAULT_FA.ezi_u(1))]
    #[case(2, -1, DEFAULT_FA.efi_u(1, 2))]
    #[case(-2, -2, DEFAULT_FA.efi_u(1, 4))]
    #[case(1, -2, DEFAULT_FA.ezi_u(1))]
    #[case(2, -2, DEFAULT_FA.efi_u(1, 4))]
    #[case(-2, -5, DEFAULT_FA.efi_u(-1, 32))]
    #[case(1, -5, DEFAULT_FA.ezi_u(1))]
    #[case(2, -5, DEFAULT_FA.efi_u(1, 32))]
    fn test_exp_int_negint_par(
        #[case] base_input: i32,
        #[case] exponent_input: i32,
        #[case] expected: Expression,
    ) {
        let mut ctx_data = CalcContextData::from_rules(&DEFAULT_RULES);
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let result = run_exp(
            &mut ctx,
            DEFAULT_FA.rzi_u(base_input),
            DEFAULT_FA.rzi_u(exponent_input));

        // check result
        assert_expression_that(&result)
            .is_equal_to(&expected);
    }

    #[rstest]
    #[case(-2, -1, DEFAULT_FA.efi_u(-1, 2))]
    #[case(1, -1, DEFAULT_FA.ezi_u(1))]
    #[case(2, -1, DEFAULT_FA.efi_u(1, 2))]
    #[case(-2, -2, DEFAULT_FA.efi_u(1, 4))]
    #[case(1, -2, DEFAULT_FA.ezi_u(1))]
    #[case(2, -2, DEFAULT_FA.efi_u(1, 4))]
    #[case(-2, -5, DEFAULT_FA.efi_u(-1, 32))]
    #[case(1, -5, DEFAULT_FA.ezi_u(1))]
    #[case(2, -5, DEFAULT_FA.efi_u(1, 32))]
    fn test_exp_int_negint(
        #[case] base_input: i32,
        #[case] exponent_input: i32,
        #[case] expected: Expression,
    ) {
        let mut ctx_data = CalcContextData::from_rules(&DEFAULT_RULES);
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let result = run_exp(
            &mut ctx,
            DEFAULT_FA.rzi_u(base_input),
            DEFAULT_FA.rzi_u(exponent_input));

        // check result
        assert_expression_that(&result)
            .is_equal_to(&expected);
    }

    #[rstest]
    #[case(-1)]
    #[case(-2)]
    #[case(-5)]
    fn test_exp_0_negint(#[case] exponent_input: i32) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let base = ctx.fa().rz0();
        let exponent = ctx.fa().rzi_u(exponent_input);

        run_exp_err(&mut ctx, base, exponent)
            .has_message("Division by zero in fraction 1/0.");
    }

    #[rstest]
    #[case("2000")]
    #[case("1000000000000000000000000000000000000")]
    fn test_exp_int_too_big(#[case] exp_val: &str) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let base = ctx.fa().rzi_u(2);
        let exponent = ctx.fa().rzs_u(exp_val);

        run_exp_err(&mut ctx, base, Rc::clone(&exponent))
            .has_message(format!("Invalid exponent value: {}", exponent).as_str());
    }

    #[test]
    fn test_exp_frac_0() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let base = ctx.fa().rfi_u(2, 3);
        let exponent = ctx.fa().rz0();

        let result = run_exp(&mut ctx, base, exponent);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(1);
    }

    #[test]
    fn test_exp_frac_posint() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let base = ctx.fa().rfi_u(2, 3);
        let exponent = ctx.fa().rzi_u(3);

        let result = run_exp(&mut ctx, base, exponent);

        // check result
        assert_expression_that(&result)
            .as_fraction()
            .is_ints(8, 27);
    }

    #[test]
    fn test_exp_mx_0() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let mx = matrix!(ctx.fa(), 2, 2;
                1, 2,
                1, 3
        ).unwrap().to_rc_expr();
        let exponent = ctx.fa().rz0();

        let result = run_exp(&mut ctx, mx, exponent);

        // check result
        let expected = matrix!(ctx.fa(), 2, 2;
                1, 0,
                0, 1
        ).unwrap();
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(2)
            .has_columns(2)
            .is_equal_to(expected);
    }

    #[test]
    fn test_exp_mx_1() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let mx = matrix!(ctx.fa(), 2, 2;
                1, 2,
                1, 3
        ).unwrap().to_rc_expr();
        let exponent = ctx.fa().rzi_u(1);
        let result = run_exp(&mut ctx, mx, exponent);

        // check result
        let expected = matrix!(ctx.fa(), 2, 2;
                1, 2,
                1, 3
        ).unwrap();
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(2)
            .has_columns(2)
            .is_equal_to(expected);
    }

    #[test]
    fn test_exp_mx_3() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let mx = matrix!(ctx.fa(), 2, 2;
                1, 2,
                1, 3
        ).unwrap().to_rc_expr();
        let exponent = ctx.fa().rzi_u(3);

        let result = run_exp(&mut ctx, mx, exponent);

        // check result
        let expected = matrix!(ctx.fa(), 2, 2;
                11, 30,
                15, 41
        ).unwrap();
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(2)
            .has_columns(2)
            .is_equal_to(expected);
    }

    #[test]
    fn test_exp_mx_5() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let mx = matrix!(ctx.fa(), 2, 2;
                1, 2,
                1, 3
        ).unwrap().to_rc_expr();
        let exponent = ctx.fa().rzi_u(5);

        let result = run_exp(&mut ctx, mx, exponent);

        // check result
        let expected = matrix!(ctx.fa(), 2, 2;
                153, 418,
                209, 571
        ).unwrap();
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(2)
            .has_columns(2)
            .is_equal_to(expected);
    }

    #[test]
    fn test_exp_mx_not_square() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let mx = matrix!(ctx.fa(), 1, 2; 1, 3).unwrap().to_rc_expr();
        let exponent = ctx.fa().rzi_u(3);

        run_exp_err(&mut ctx, mx, exponent)
            .has_message("Matrix is not square.");
    }

    #[test]
    fn test_exp_mx_invalid() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let mx = matrix!(ctx.fa(), 2, 2;
                1, 2,
                1, 3
            ).unwrap().to_rc_expr();
        let exponent = ctx.fa().rzi_u(1001);

        run_exp_err(&mut ctx, mx, exponent)
            .has_message("Invalid exponent value: 1001");
    }
}