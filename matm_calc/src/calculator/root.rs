use log::debug;
use std::collections::VecDeque;
use std::rc::Rc;
use async_recursion::async_recursion;

use crate::utils::multi_instance_collector::MultiInstanceCollector;
use crate::calculator::calculate_expression_step;
use crate::calculator::ModelData;
use crate::calculator::CalcResultOrErr;
use crate::calculator::context::CalcContext;
use crate::calculator::CalcResult;
use crate::calculator::factorization::calculate_trial_division;
use crate::calculator::int::subtraction::calculate_int_sub;
use crate::calculator::transformers::collect_product_factors;
use crate::calculator::transformers::group_ints_by_count;
use crate::recognizers::is_int_or_product_of_ints;
use matm_error::error::CalcError;
use matm_model::expressions::binary::BinaryData;
use matm_model::expressions::binary::BinaryOperator;
use matm_model::expressions::exponentiation::ExpData;
use matm_model::expressions::root::RootData;
use matm_model::expressions::Expression;
use matm_model::expressions::int::IntData;
use matm_model::expressions::ToExpression;


pub async fn calculate_root_expression(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, RootData>,
) -> CalcResultOrErr {
    debug!("calculate root: {}", model.data);

    if let Some(res) = calculate_degree(ctx, model.data).await? {
        return Ok(res);
    }

    if let Some(res) = calculate_root_of_exp(ctx, model.data).await? {
        return Ok(res);
    }

    if let Some(res) = perform_radicand_factorization(ctx, model.data).await? {
        return Ok(res);
    }

    if let Some(res) = calculate_root_int_int(ctx, model.data).await? {
        return Ok(res);
    }

    if let Some(res) = transform_product_of_ints_to_product_of_exp_and_int(ctx, model.data).await? {
        return Ok(res);
    }

    if let Some(res) = transform_root_of_product_to_product_of_roots(ctx, model.data).await? {
        return Ok(res);
    }

    Ok(CalcResult::original(Rc::clone(model.orig)))
}


async fn calculate_degree(
    ctx: &mut CalcContext<'_>,
    root_expr: &RootData,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    let degree_value = calculate_expression_step(ctx, &root_expr.degree).await?;
    if degree_value.calculated_count > 0 {
        let res = RootData::new(
            Rc::clone(&root_expr.radicand),
            degree_value.object
        ).to_rc_expr();
        return Ok(Some(CalcResult::new(res, degree_value.calculated_count)));
    }
    return Ok(None);
}

async fn perform_radicand_factorization(
    ctx: &mut CalcContext<'_>,
    root_expr: &RootData,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    if !root_expr.radicand.is_product() {
        let calc_radicand = calculate_expression_step(ctx, &root_expr.radicand).await?;
        if calc_radicand.calculated_count > 0 {
            let expr = RootData::new(
                calc_radicand.object,
                Rc::clone(&root_expr.degree)
            ).to_rc_expr();
            return Ok(Some(CalcResult::new(expr, calc_radicand.calculated_count)));
        }
    }
    return Ok(None);
}

/// calculates root of exponent:
/// - root(i^2, 2) => i
async fn calculate_root_of_exp(
    _ctx: &mut CalcContext<'_>,
    root_expr: &RootData,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    match (root_expr.radicand.as_ref(), root_expr.degree.as_ref()) {
        (Expression::Exp(radicand_value), Expression::Int(_)) => {
            if radicand_value.exponent.eq(&root_expr.degree) {
                let res_expr = Rc::clone(&radicand_value.base);
                return Ok(Some(CalcResult::one(res_expr)));
            }
        }
        _ => {}
    }
    return Ok(None);
}

/// calculates root of int with int degree:
/// - root(8, 3)
async fn calculate_root_int_int(
    ctx: &mut CalcContext<'_>,
    root_expr: &RootData,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    match (root_expr.radicand.as_ref(), root_expr.degree.as_ref()) {
        (Expression::Int(radicand), Expression::Int(degree)) => {
            if radicand.is_zero() || radicand.is_one() {
                let res_expr = radicand.clone().to_rc_expr();
                return Ok(Some(CalcResult::one(res_expr)));
            }
            if degree.is_one() {
                let res_expr = radicand.clone().to_rc_expr();
                return Ok(Some(CalcResult::one(res_expr)));
            }
            let one = ctx.fa().z1();
            if degree.as_ref().gt(&one) {
                return calculate_root_int_gt1(ctx, radicand, degree).await;
            }
        }
        _ => {}
    }
    return Ok(None);
}

async fn calculate_root_int_gt1(
    ctx: &mut CalcContext<'_>,
    radicand: &IntData,
    degree: &IntData,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    //TODO exception for 6
    //TODO should be solved for any set of factors
    if let Some(6) = radicand.to_i32() {
        if let Some(2) = degree.to_i32() {
            return Ok(None);
        }
    }

    let factors = calculate_trial_division(ctx, radicand).await?;
    if factors.len() > 1 {
        let mut factors_expr = factors.first().unwrap().clone().to_rc_expr();
        for f in factors.iter().skip(1) {
            factors_expr = BinaryData::mul(
                factors_expr,
                f.clone().to_rc_expr()
            ).to_rc_expr();
        }
        let res_expr = RootData::new(factors_expr, degree.clone().to_rc_expr()).to_rc_expr();
        return Ok(Some(CalcResult::one(res_expr)));
    }

    return Ok(None);
}

/// detects factorization that can be changed to exponentiation:
/// - root(5*5, 2) => root(5^2, 2)
/// - root(5*5*5, 3) => root(5^3, 3)
/// - root(5*5*7, 2) => root(5^2*7, 2)
/// - root(5*5*7*7, 2) => root(5^2*7^2, 2)
async fn transform_product_of_ints_to_product_of_exp_and_int(
    ctx: &mut CalcContext<'_>,
    root_expr: &RootData,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    if let Expression::Int(degree_value) = root_expr.degree.as_ref() {
        if is_int_or_product_of_ints(&root_expr.radicand) {
            let mut ints_by_count = MultiInstanceCollector::new();
            group_ints_by_count(&mut ints_by_count, &root_expr.radicand).await;
            if ints_by_count.len() > 1 {
                let mut factors: VecDeque<Rc<Expression>> = VecDeque::new();
                let one = ctx.fa().z1();
                for item in &ints_by_count.items {
                    debug!("root transformation: {}: {}", item.value(), item.count());
                    let mut count = ctx.fa().zu(item.count())?;
                    while count.is_positive() {
                        let new_exponent: IntData = if count >= *degree_value.as_ref() {
                            degree_value.as_ref().clone()
                        } else {
                            count.clone()
                        };
                        if new_exponent > one {
                            factors.push_back(ExpData::new(
                                item.value().clone().to_rc_expr(),
                                new_exponent.clone().to_rc_expr()
                            ).to_rc_expr());
                        } else {
                            factors.push_back(item.value().clone().to_rc_expr());
                        }
                        count = calculate_int_sub(ctx, &count, &new_exponent).await?;
                    }
                }

                let mut radicand_expr: Rc<Expression> = factors.pop_front().unwrap();
                while let Some(f) = factors.pop_front() {
                    radicand_expr = BinaryData::mul(
                        radicand_expr,
                        f
                    ).to_rc_expr();
                }
                let res_expr = RootData::new(
                    radicand_expr,
                    Rc::clone(&root_expr.degree)
                ).to_rc_expr();
                return Ok(Some(CalcResult::one(res_expr)));
            }
        }
    }
    return Ok(None);
}

#[async_recursion(?Send)]
async fn is_product_of_ints_and_exponents(expr: &Expression, degree: &Expression) -> bool {
    match expr {
        Expression::Binary(bin_expr) => {
            if let BinaryOperator::Mul = bin_expr.operator {
                if !is_product_of_ints_and_exponents(&bin_expr.left, degree).await {
                    return false;
                }
                if !is_product_of_ints_and_exponents(&bin_expr.right, degree).await {
                    return false;
                }
                return true;
            }
        }
        Expression::Int(_) => {
            return true;
        }
        Expression::Exp(exp) => {
            return exp.exponent.as_ref().eq(degree);
        }
        _ => {}
    }

    return false;
}

/// Transforms root of product to product of roots:
/// - root(i^2 * j, 2) => root(i^2, 2) * root(j, 2)
async fn transform_root_of_product_to_product_of_roots(
    _ctx: &mut CalcContext<'_>,
    root_expr: &RootData,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    if let Expression::Int(_) = root_expr.degree.as_ref() {
        if is_product_of_ints_and_exponents(&root_expr.radicand, &root_expr.degree).await {
            let mut factors: VecDeque<&Expression> = VecDeque::new();
            collect_product_factors(&mut factors, &root_expr.radicand).await;
            if factors.len() > 1 {
                let mut result: Rc<Expression> = RootData::new(
                    Rc::new(factors.pop_front().unwrap().clone()),
                    Rc::clone(&root_expr.degree)
                ).to_rc_expr();
                while let Some(f) = factors.pop_front() {
                    result = BinaryData::mul(
                        result,
                        RootData::new(
                            Rc::new(f.clone()),
                            Rc::clone(&root_expr.degree)
                        ).to_rc_expr()
                    ).to_rc_expr();
                }
                return Ok(Some(CalcResult::one(result)));
            }
        }
    }
    return Ok(None);
}

