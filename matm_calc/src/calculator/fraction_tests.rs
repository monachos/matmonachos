#[cfg(test)]
mod tests {
    use crate::calculator::calculate_expression;
    use crate::calculator::fraction::reduce_fraction_int;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::result::dump_unicode_color;
    use crate::testing::asserts::assert_next_step;
    use crate::testing::asserts::assert_next_step_that;
    use crate::testing::asserts::assert_none_step;
    use futures::executor::block_on;
    use matm_model::testing::asserts::assert_expression_that;
    use matm_model::testing::asserts::assert_result_error_that;

    #[test]
    fn test_reduce_fraction() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let i1 = ctx.fa().zi_u(2);
        let i2 = ctx.fa().zi_u(4);

        match block_on(reduce_fraction_int(&mut ctx, &i1, &i2)).unwrap() {
            Some((o1, o2)) => {
                assert_eq!(1, o1.to_i32().unwrap());
                assert_eq!(2, o2.to_i32().unwrap());
            }
            None => {
                assert!(false);
            }
        }
    }

    //-------------------------------------------------------

    #[test]
    fn test_0_by_2() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let expr = ctx.fa().rfe(ctx.fa().ez0(), ctx.fa().ezi_u(2));

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(0);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_integer()
            .is_int(0);
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_5_by_1() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let expr = ctx.fa().rfe(ctx.fa().ezi_u(5), ctx.fa().ezi_u(1));

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(5);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_integer()
            .is_int(5);
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_2_by_3() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let expr = ctx.fa().rfi_u(2, 3);

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_fraction()
            .is_ints(2, 3);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_4_by_2() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let expr = ctx.fa().rfi_u(4, 2);

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(2);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_integer()
            .is_int(2);
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_4_by_6() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let expr = ctx.fa().rfi_u(4, 6);

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_fraction()
            .is_ints(2, 3);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .is_ints(2, 3);
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_10_by_4() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let expr = ctx.fa().rfi_u(10, 4);

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_fraction()
            .is_ints(5, 2);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .is_ints(5, 2);
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_m2_by_m4() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let expr = ctx.fa().rfi_u(-2, -4);

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_fraction()
            .is_ints(1, 2);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .is_ints(1, 2);
        assert_none_step(&mut steps);
    }

    //----------------------------------------

    #[test]
    fn test_1_2_by_3() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = ctx.fa().rfe(
            ctx.fa().efi_u(1, 2),
            ctx.fa().ezi_u(3)
        );

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_fraction()
            .is_ints(1, 6);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .has_numerator_int(1)
            .has_denominator_format("2 * 3");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .is_ints(1, 6);
        assert_none_step(&mut steps);
    }   

    //----------------------------------------

    #[test]
    fn test_1_by_2_3() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = ctx.fa().rfe(
            ctx.fa().ezi_u(1),
            ctx.fa().efi_u(2, 3)
        );

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_fraction()
            .is_ints(3, 2);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .has_numerator_format("1 * 3")
            .has_denominator_int(2);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .is_ints(3, 2);
        assert_none_step(&mut steps);
    }

    //----------------------------------------

    #[test]
    fn test_1_2_by_3_4() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = ctx.fa().rfe(
            ctx.fa().efi_u(1, 2),
            ctx.fa().efi_u(3, 4)
        );

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_fraction()
            .is_ints(2, 3);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .has_numerator_format("1 * 4")
            .has_denominator_format("2 * 3");
        assert_next_step_that(&mut steps)
            .has_calculation_count(2)
            .as_fraction()
            .is_ints(4, 6);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .is_ints(2, 3);
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_5_by_0() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = ctx.fa().rfe(ctx.fa().ezi_u(5), ctx.fa().ez0());

        let result = block_on(calculate_expression(&mut ctx, &expr));

        // check result
        assert_result_error_that(result)
            .has_message("Division by zero in fraction 5/0.");
    }

}