use std::rc::Rc;

use crate::calculator::CalcResultOrErr;
use crate::calculator::ModelData;
use crate::calculator::CalcResult;
use crate::calculator::calculate_expression_step;
use crate::calculator::context::CalcContext;
use matm_model::expressions::cracovians::CracovianData;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;


pub async fn calculate_cracovian(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, CracovianData>,
) -> CalcResultOrErr {
    // calculate all matrix elements
    let mut elements: Vec<Rc<Expression>> = Vec::new();
    let mut total_calculated = 0;
    for idx in model.data.iter_right_down() {
        let el = model.data.get(&idx)?;
        let el_value = calculate_expression_step(ctx, el).await?;
        total_calculated += el_value.calculated_count;
        elements.push(el_value.object);
    }
 
    // at least element is calculated
    if total_calculated > 0 {
        let new_crac = CracovianData::from_vector(model.data.rows(), model.data.columns(), elements)?;
        let expr = new_crac.to_rc_expr();
        return Ok(CalcResult::new(expr, total_calculated));
    }
 
    Ok(CalcResult::original(Rc::clone(&model.orig)))
}
