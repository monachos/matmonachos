use std::rc::Rc;

use matm_model::expressions::binary::BinaryData;
use matm_model::expressions::binomial::BinomialData;
use matm_model::expressions::unary::UnaryData;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;

use crate::calculator::CalcResult;
use crate::calculator::ModelData;
use crate::calculator::CalcResultOrErr;
use crate::calculator::calculate_expression_step;
use crate::calculator::context::CalcContext;

pub async fn calculate_binomial_expression(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, BinomialData>,
) -> CalcResultOrErr {
    let set_value = calculate_expression_step(ctx, &model.data.set).await?;
    let subset_value = calculate_expression_step(ctx, &model.data.subset).await?;
    let total_calculated = set_value.calculated_count + subset_value.calculated_count;
    if total_calculated > 0 {
        let expr = ctx.fa().rfr(set_value.object, subset_value.object);
        return Ok(CalcResult::new(expr, total_calculated));
    }

    // / k \       n!
    // |   | = ----------
    // \ k /    k!(n-k)!
    let result = ctx.fa().rfr(
        UnaryData::factorial(Rc::clone(&model.data.set)).to_rc_expr(),
        BinaryData::mul(
            UnaryData::factorial(Rc::clone(&model.data.subset)).to_rc_expr(),
            UnaryData::factorial(BinaryData::sub(Rc::clone(&model.data.set), Rc::clone(&model.data.subset)).to_rc_expr()).to_rc_expr()
        ).to_rc_expr()
    );
    Ok(CalcResult::one(result))
}