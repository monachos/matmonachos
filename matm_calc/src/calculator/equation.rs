use crate::calculator::CalcResult;
use crate::calculator::ModelData;
use crate::calculator::calculate_expression_step;
use crate::calculator::CalcResultOrErr;
use crate::calculator::context::CalcContext;
use matm_model::expressions::equation::EquationData;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;
use std::rc::Rc;

pub async fn calculate_equation(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, EquationData>,
) -> CalcResultOrErr {
    let left = calculate_expression_step(ctx, &model.data.left).await?;
    let right = calculate_expression_step(ctx, &model.data.right).await?;
    if left.calculated_count > 0 || right.calculated_count > 0 {
        let res = EquationData::new(left.object, right.object).to_rc_expr();
        let total_calc = left.calculated_count + right.calculated_count;
        return Ok(CalcResult::new(res, total_calc));
    }

    Ok(CalcResult::original(Rc::clone(&model.orig)))
}

