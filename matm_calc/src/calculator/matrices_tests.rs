#[cfg(test)]
mod tests {
    use crate::calculator::calculate_expression;
    use crate::calculator::matrices::is_positive_definite_matrix;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::matrix;
    use crate::result::dump_unicode_color;
    use crate::testing::asserts::assert_next_step_that;
    use crate::testing::asserts::assert_none_step;
    use futures::executor::block_on;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;


    #[test]
    fn test_matrix_calculate() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = matrix!(ctx.fa(), 2, 2;
            (1/5), (2/5),
            (3/6), (4/2)
            ).unwrap().to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(2)
            .has_columns(2)
            .is_i_i_fraction_that(1, 1, |e| {e.is_ints(1, 5);})
            .is_i_i_fraction_that(1, 2, |e| {e.is_ints(2, 5);})
            .is_i_i_fraction_that(2, 1, |e| {e.is_ints(1, 2);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(2);});

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .as_matrix()
            .has_rows(2)
            .has_columns(2)
            .is_i_i_fraction_that(1, 1, |e| {e.is_ints(1, 5);})
            .is_i_i_fraction_that(1, 2, |e| {e.is_ints(2, 5);})
            .is_i_i_fraction_that(2, 1, |e| {e.is_ints(3, 6);})
            .is_i_i_fraction_that(2, 2, |e| {e.is_ints(4, 2);});
        assert_next_step_that(&mut steps)
            .has_calculation_count(2)
            .as_matrix()
            .has_rows(2)
            .has_columns(2)
            .is_i_i_fraction_that(1, 1, |e| {e.is_ints(1, 5);})
            .is_i_i_fraction_that(1, 2, |e| {e.is_ints(2, 5);})
            .is_i_i_fraction_that(2, 1, |e| {e.is_ints(1, 2);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(2);});
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_is_positive_definite_002() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let m = matrix!(ctx.fa(), 3, 3;
                0, 1, 5,
                1, 0, 3,
                4, 3, 2
            ).unwrap();
        
        let result = block_on(is_positive_definite_matrix(&mut ctx, &m)).unwrap();

        assert_eq!(result, Some(false));
    }

    #[test]
    fn test_is_positive_definite_112() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let m = matrix!(ctx.fa(), 3, 3;
                1, 1, 5,
                1, 1, 3,
                4, 3, 2
            ).unwrap();
        
        let result = block_on(is_positive_definite_matrix(&mut ctx, &m)).unwrap();

        assert_eq!(result, Some(true));
    }

    #[test]
    fn test_is_positive_definite_1x() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let m = matrix!(ctx.fa(), 2, 2;
                1, 1,
                1, "x"
            ).unwrap();
        
        let result = block_on(is_positive_definite_matrix(&mut ctx, &m)).unwrap();

        assert_eq!(result, None);
    }

    #[test]
    fn test_is_positive_definite_not_square() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let m = matrix!(ctx.fa(), 2, 3;
                1, 1, 2,
                2, 3, 3
            ).unwrap();
        
        let result = block_on(is_positive_definite_matrix(&mut ctx, &m)).unwrap();

        assert_eq!(result, None);
    }
}