use std::rc::Rc;

use crate::calculator::int::addition::calculate_int_add;
use crate::calculator::int::division::calculate_int_div;
use crate::calculator::{CalcResultOrErr, ModelData};
use crate::calculator::CalcResult;
use crate::calculator::calculate_expression_step;
use crate::calculator::context::CalcContext;
use matm_error::error::CalcError;
use matm_model::expressions::binary::BinaryData;
use matm_model::expressions::fractions::FracData;
use matm_model::expressions::int::IntData;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;


pub(crate) async fn calculate_fraction(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, FracData>,
) -> CalcResultOrErr {
    let num_value = calculate_expression_step(ctx, &model.data.numerator).await?;
    let den_value = calculate_expression_step(ctx, &model.data.denominator).await?;
    let total_calculated = num_value.calculated_count + den_value.calculated_count;
    if total_calculated > 0 {
        let expr = ctx.fa().rfr(num_value.object, den_value.object);
        return Ok(CalcResult::new(expr, total_calculated));
    }

    // calculate int [op] int
    if let Some(result) = calculate_fraction_int_int(ctx, model.data).await? {
        return Ok(result);
    }

    // calculate int [op] frac
    if let Some(result) = calculate_fraction_int_frac(ctx, model.data).await? {
        return Ok(result);
    }

    // calculate frac [op] int
    if let Some(result) = calculate_fraction_frac_int(ctx, model.data).await? {
        return Ok(result);
    }

    // calculate frac [op] frac
    if let Some(result) = calculate_fraction_frac_frac(ctx, model.data).await? {
        return Ok(result);
    }

    Ok(CalcResult::original(Rc::clone(&model.orig)))
}

/// Reduces the fraction
/// 
/// Returns (numerator, denominator) tuple if the fraction has
/// been reduced, None otherwise.
pub async fn reduce_fraction_int(
    ctx: &mut CalcContext<'_>,
    numerator: &IntData,
    denominator: &IntData,
) -> Result<Option<(IntData, IntData)>, CalcError> {
    let mut divider: IntData = IntData::from_int(numerator.radix(), 2)?;
    let mut num = numerator.calculate_abs();
    let mut den = denominator.calculate_abs();
    let one = ctx.fa().z1();
    while num >= divider {
        loop {
            let (num_div_q, num_div_rem) = calculate_int_div(ctx, &num, &divider).await?;
            let (den_div_q, den_div_rem) = calculate_int_div(ctx, &den, &divider).await?;
            if num_div_rem.is_zero() && den_div_rem.is_zero() {
                num = num_div_q.clone();
                den = den_div_q.clone();
            } else {
                break;
            }
        }
        divider = calculate_int_add(ctx, &divider, &one).await?;
    }
    if is_fraction_negative_int(numerator, denominator).await {
        num = num.calculate_neg();
    }
    if num != *numerator || den != *denominator {
        return Ok(Some((num, den)));
    } else {
        return Ok(None);
    }
}

async fn is_fraction_negative_int(numerator: &IntData, denominator: &IntData) -> bool {
   (numerator.is_negative() && denominator.is_positive())
        || (numerator.is_positive() && denominator.is_negative())
}

/// Calculates fraction int / int
async fn calculate_fraction_int_int(
    ctx: &mut CalcContext<'_>,
    frac: &FracData,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    if frac.denominator.is_zero() {
        return Err(CalcError::from_string(format!("Division by zero in fraction {}.", frac)));
    }
 
    match (frac.numerator.as_ref(), frac.denominator.as_ref()) {
        (Expression::Int(num), Expression::Int(den)) => {
            // reduce, this can be done by few algorithms:
            // 1. replace numerator and denominator with values
            //    divided by greatest common divisor:
            //    12 / 16 = 3 / 4
            // 2. replace numerator and denominator with product
            //    of reduced values and greatest common divisor:
            //    12 / 16 = (3*4) / (4*4)
            // 3. replace numerator and denominator with product
            //    of theirs factors
            //    12 / 16 = (2*2*3) / (2*2*2*2)
            if num.is_zero() || den.is_one() {
                let result = num.clone().to_rc_expr();
                return Ok(Some(CalcResult::one(result)));
            } else {
                match reduce_fraction_int(ctx, num, den).await? {
                    Some((red_num, red_den)) => {
                        if red_den.is_one() {
                            let result = red_num.to_rc_expr();
                            return Ok(Some(CalcResult::one(result)));
                        } else {
                            let result = ctx.fa().rfb(red_num, red_den);
                            return Ok(Some(CalcResult::one(result)));
                        }
                    }
                    None => {}
                }
            }
        }
        _ => {}
    }
    return Ok(None);
}


/// Calculates fraction [scalar] + frac
pub async fn build_fraction_add_scalar_frac<T: ToExpression + Clone>(
    ctx: &mut CalcContext<'_>,
    a: &Box<T>,
    b: &FracData,
) -> Result<Rc<Expression>, CalcError> {
    // a + bn/bd = (a*bd+bn)/bd
    let result = ctx.fa().rfr(
        BinaryData::add(
            BinaryData::mul(
                a.clone().to_rc_expr(),
                Rc::clone(&b.denominator)).to_rc_expr(),
            Rc::clone(&b.numerator)
        ).to_rc_expr(),
        Rc::clone(&b.denominator)
    );
    return Ok(result);
}

/// Calculates fraction [scalar] - frac
pub async fn build_fraction_sub_scalar_frac<T: ToExpression + Clone>(
    ctx: &mut CalcContext<'_>,
    a: &Box<T>,
    b: &FracData,
) -> Result<Rc<Expression>, CalcError> {
    // a - bn/bd = (a*bd-bn)/bd
    let result = ctx.fa().rfr(
        BinaryData::sub(
            BinaryData::mul(
                a.clone().to_rc_expr(),
                Rc::clone(&b.denominator)
            ).to_rc_expr(),
            Rc::clone(&b.numerator)
        ).to_rc_expr(),
        Rc::clone(&b.denominator)
    );
    return Ok(result);
}

/// Calculates fraction [scalar] * frac
pub async fn build_fraction_mul_scalar_frac<T: ToExpression + Clone>(
    ctx: &mut CalcContext<'_>,
    a: &Box<T>,
    b: &FracData,
) -> Result<Rc<Expression>, CalcError> {
    let result = ctx.fa().rfr(
        Expression::binary_mul_if_needed(
            a.clone().to_rc_expr(),
            b.numerator.clone()
        ),
        b.denominator.clone()
    );
    return Ok(result);
}

/// Calculates fraction [scalar] / frac
pub async fn build_fraction_div_scalar_frac<T: ToExpression + Clone>(
    ctx: &mut CalcContext<'_>,
    a: &Box<T>,
    b: &FracData,
    reduce_identity: bool,
) -> Result<Rc<Expression>, CalcError> {
    let numerator = if reduce_identity {
        Expression::binary_mul_if_needed(
            a.clone().to_rc_expr(),
            Rc::clone(&b.denominator)
        )
    } else {
        BinaryData::mul(
            a.clone().to_rc_expr(),
            Rc::clone(&b.denominator)
        ).to_rc_expr()
    };
    let result = ctx.fa().rfr(
        numerator,
        b.numerator.clone()
    );
    return Ok(result);
}

/// Builds fraction frac + [scalar]
pub async fn build_fraction_add_frac_scalar<T: ToExpression + Clone>(
    ctx: &mut CalcContext<'_>,
    a: &FracData,
    b: &Box<T>,
) -> Result<Rc<Expression>, CalcError> {
    // an/ad + b = (an+b*ad)/ad
    let result = ctx.fa().rfr(
        BinaryData::add(
            Rc::clone(&a.numerator),
            Expression::binary_mul_if_needed(
                b.clone().to_rc_expr(),
                Rc::clone(&a.denominator)
            )
        ).to_rc_expr(),
        Rc::clone(&a.denominator),
    );
    return Ok(result);
}

/// Builds fraction frac - [scalar]
pub async fn build_fraction_sub_frac_scalar<T: ToExpression + Clone>(
    ctx: &mut CalcContext<'_>,
    a: &FracData,
    b: &Box<T>,
) -> Result<Rc<Expression>, CalcError> {
    // an/ad + b = (an-b*ad)/ad
    let result = ctx.fa().rfr(
        BinaryData::sub(
            Rc::clone(&a.numerator),
            Expression::binary_mul_if_needed(
                b.clone().to_rc_expr(),
                Rc::clone(&a.denominator)
            )
        ).to_rc_expr(),
        Rc::clone(&a.denominator),
    );
    return Ok(result);
}

/// Builds fraction frac * [scalar]
pub async fn build_fraction_mul_frac_scalar<T: ToExpression + Clone>(
    ctx: &mut CalcContext<'_>,
    a: &FracData,
    b: &Box<T>,
) -> Result<Rc<Expression>, CalcError> {
    let result = ctx.fa().rfr(
        Expression::binary_mul_if_needed(
            Rc::clone(&a.numerator),
            b.clone().to_rc_expr()
        ),
        Rc::clone(&a.denominator)
    );
    return Ok(result);
}

/// Builds fraction frac / [scalar]
pub async fn build_fraction_div_frac_scalar<T: ToExpression + Clone>(
    ctx: &mut CalcContext<'_>,
    a: &FracData,
    b: &Box<T>,
    reduce_identity: bool,
) -> Result<Rc<Expression>, CalcError> {
    let denominator = if reduce_identity {
        Expression::binary_mul_if_needed(
            Rc::clone(&a.denominator),
            b.clone().to_rc_expr()
        )
    } else {
        BinaryData::mul(
            Rc::clone(&a.denominator),
            b.clone().to_rc_expr()
        ).to_rc_expr()
    };
    let result = ctx.fa().rfr(
        Rc::clone(&a.numerator),
        denominator,
    );
    return Ok(result);
}

/// Builds fraction frac + frac
pub async fn build_fraction_add_frac_frac(
    ctx: &mut CalcContext<'_>,
    a: &FracData,
    b: &FracData,
) -> Result<Rc<Expression>, CalcError> {
    if a.denominator.eq(&b.denominator) {
        // an/ad + bn/bd = (an+bn)/ad
        let result = ctx.fa().rfr(
            BinaryData::add(
                Rc::clone(&a.numerator),
                Rc::clone(&b.numerator)
            ).to_rc_expr(),
            Rc::clone(&a.denominator)
        );
        return Ok(result);
    } else {
        // an/ad + bn/bd = (an*bd+bn*ad)/(ad*bd)
        let result = ctx.fa().rfr(
            BinaryData::add(
                Expression::binary_mul_if_needed(
                    Rc::clone(&a.numerator),
                    Rc::clone(&b.denominator)
                ),
                Expression::binary_mul_if_needed(
                    Rc::clone(&b.numerator),
                    Rc::clone(&a.denominator)
                )
            ).to_rc_expr(),
            Expression::binary_mul_if_needed(
                Rc::clone(&a.denominator),
                Rc::clone(&b.denominator)
            ),
        );
        return Ok(result);
    }
}

/// Builds fraction frac - frac
pub async fn build_fraction_sub_frac_frac(
    ctx: &mut CalcContext<'_>,
    a: &FracData,
    b: &FracData,
) -> Result<Rc<Expression>, CalcError> {
    if a.denominator.eq(&b.denominator) {
        if a.numerator.eq(&b.numerator) {
           // the same 
           let result = ctx.fa().rz0();
           return Ok(result);
        } else {
            // an/ad - bn/bd = (an-bn)/ad
            let result = ctx.fa().rfr(
                BinaryData::sub(Rc::clone(&a.numerator), Rc::clone(&b.numerator)).to_rc_expr(),
                Rc::clone(&a.denominator)
            );
            return Ok(result);
        }
    } else {
        // an/ad - bn/bd = (an*bd-bn*ad)/(ad*bd)
        let result = ctx.fa().rfr(
            BinaryData::sub(
                Expression::binary_mul_if_needed(
                    a.numerator.clone(),
                    b.denominator.clone()
                ),
                Expression::binary_mul_if_needed(
                    b.numerator.clone(),
                    a.denominator.clone()
                )
            ).to_rc_expr(),
            Expression::binary_mul_if_needed(
                Rc::clone(&a.denominator),
                Rc::clone(&b.denominator))
        );
        return Ok(result);
    }
}

/// Builds fraction frac * frac
pub async fn build_fraction_mul_frac_frac(
    ctx: &mut CalcContext<'_>,
    a: &FracData,
    b: &FracData,
) -> Result<Rc<Expression>, CalcError> {
    let result = ctx.fa().rfr(
        Expression::binary_mul_if_needed(
            a.numerator.clone(),
            b.numerator.clone()
        ),
        Expression::binary_mul_if_needed(
            a.denominator.clone(),
            b.denominator.clone()
        ),
    );
    return Ok(result);
}

/// Builds fraction frac / frac
pub async fn build_fraction_div_frac_frac(
    ctx: &mut CalcContext<'_>,
    a: &FracData,
    b: &FracData,
) -> Result<Rc<Expression>, CalcError> {
    let result = ctx.fa().rfr(
        BinaryData::mul(
            Rc::clone(&a.numerator),
            Rc::clone(&b.denominator)
        ).to_rc_expr(),
        BinaryData::mul(
            Rc::clone(&a.denominator),
            Rc::clone(&b.numerator)
        ).to_rc_expr(),
    );
    return Ok(result);
}

//----------------------------------------------------

/// Calculates fraction int/frac
async fn calculate_fraction_int_frac(
    ctx: &mut CalcContext<'_>,
    frac: &FracData,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    match (frac.numerator.as_ref(), frac.denominator.as_ref()) {
        (Expression::Int(num), Expression::Frac(den)) => {
            let result = build_fraction_div_scalar_frac(ctx, num, den, false).await?;
            return Ok(Some(CalcResult::one(result)));
        }
        _ => {}
    }
    return Ok(None);
}

/// Calculates fraction frac/int
async fn calculate_fraction_frac_int(
    ctx: &mut CalcContext<'_>,
    frac: &FracData,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    match (frac.numerator.as_ref(), frac.denominator.as_ref()) {
        (Expression::Frac(num), Expression::Int(den)) => {
            let result = build_fraction_div_frac_scalar(ctx, num, den, false).await?;
            return Ok(Some(CalcResult::one(result)));
        }
        _ => {}
    }
    return Ok(None);
}

/// Calculates fraction frac/frac
async fn calculate_fraction_frac_frac(
    ctx: &mut CalcContext<'_>,
    frac: &FracData,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    match (frac.numerator.as_ref(), frac.denominator.as_ref()) {
        (Expression::Frac(num), Expression::Frac(den)) => {
            let result = build_fraction_div_frac_frac(ctx, num, den).await?;
            return Ok(Some(CalcResult::one(result)));
        }
        _ => {}
    }
    return Ok(None);
}
