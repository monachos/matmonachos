#[cfg(test)]
mod tests {
    use futures::executor::block_on;

    use matm_model::expressions::binomial::BinomialData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;

    use crate::calculator::context::CalcContext;
    use crate::calculator::calculate_expression;
    use crate::calculator::context::CalcContextData;
    use crate::result::dump_unicode_color;

    #[test]
    fn test_binomial_n_k() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinomialData::new(ctx.fa().rsn("n"), ctx.fa().rsn("k")).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_fraction()
            .has_numerator_that(|e| {
                e.as_unary()
                    .is_factorial()
                    .as_symbol()
                    .has_name("n");
            })
            .has_denominator_that(|e| {
                e.as_binary()
                    .is_mul()
                    .has_left_that(|l| {
                        l.as_unary()
                            .is_factorial()
                            .as_symbol()
                            .has_name("k");
                    })
                    .has_right_that(|r| {
                        r.as_unary()
                            .is_factorial()
                            .as_binary()
                            .is_left_symbol_that(|s| { s.has_name("n"); })
                            .is_right_symbol_that(|s| { s.has_name("k"); });
                    });
            })
        ;
    }
}
