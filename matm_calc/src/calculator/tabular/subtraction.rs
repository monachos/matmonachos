use crate::calculator::context::CalcContext;
use matm_error::error::CalcError;
use matm_model::core::natural::Natural;
use matm_model::core::positional::PositionalMark;
use matm_model::core::positional::PositionalNumber;
use matm_model::expressions::binary::BinaryData;
use matm_model::expressions::ToExpression;
use matm_model::statements::tabular_subtraction::TabularSubtractionData;
use matm_model::statements::text::TextStatement;
use matm_model::statements::ToStatement;

pub async fn calculate_tabular_subtraction(
    ctx: &mut CalcContext<'_>,
    minuend: &Natural,
    subtrahend: &Natural,
) -> Result<Natural, CalcError> {
    // all numbers should have the same radix
    if minuend.radix() != ctx.rules().radix {
        return Err(CalcError::from_string(format!(
            "Invalid radix {} of the minuend {}, expected {}.",
            minuend.radix(),
            minuend,
            ctx.rules().radix)));
    }
    if subtrahend.radix() != ctx.rules().radix {
        return Err(CalcError::from_string(format!(
            "Invalid radix {} of the subtrahend {}, expected {}.",
            subtrahend.radix(),
            subtrahend,
            ctx.rules().radix)));
    }
    if minuend.lt(&subtrahend) {
        return Err(CalcError::from_string(format!(
            "The minuend {} must be greater than or equal to the subtrahend {}.",
            minuend,
            subtrahend)));
    }

    let mut data = TabularSubtractionData {
        minuend: PositionalNumber::from_natural(minuend),
        subtrahend: PositionalNumber::from_natural(subtrahend),
        difference: PositionalNumber::new(ctx.rules().radix),
        borrowed: PositionalNumber::new(ctx.rules().radix),
    };

    for pos in 0..data.minuend.digits_len() {
        let dim = data.minuend.get_digit_else_zero(pos);
        let sub = data.subtrahend.get_digit_else_zero(pos);

        if ctx.rules().explain_elementary_operations {
            data.minuend.mark_digit_as_source(pos)?;
            data.subtrahend.mark_digit_as_source(pos)?;
            data.difference.set_unknown(pos)?;

            let par = TextStatement::paragraph("Subtraction at position {index}")
                .and_expression("index", ctx.fa().rzu(pos+1)?)
                .to_statement();
            ctx.explanation.add_statement(par);
            ctx.explanation.add_statement(data.clone().to_statement());
        }

        if dim >= sub {
            // calculate difference
            let diff = dim - sub;
            if ctx.rules().explain_elementary_operations {
                let left_formula = BinaryData::sub(
                    ctx.fa().rzi(dim as i32)?,
                    ctx.fa().rzi(sub as i32)?).to_rc_expr();
                let right_formula = ctx.fa().rzi(diff as i32)?;
                let par = TextStatement::paragraph("Position {index}: {formula}")
                    .and_expression("index", ctx.fa().rzu(pos+1)?)
                    .and_equation("formula", left_formula, right_formula)
                    .to_statement();
                ctx.explanation.add_statement(par);
            }
            data.difference.set_digit_mark(pos, diff, PositionalMark::Target)?;
        } else {
            // borrow 1
            data.minuend.remove_marks();
            data.subtrahend.remove_marks();

            if ctx.rules().explain_elementary_operations {
                let par = TextStatement::paragraph(
                    "Position {index}: borrowing {borrowed} from position {source_position} to position {index}")
                    .and_expression("index", ctx.fa().rzu(pos+1)?)
                    .and_expression("borrowed", ctx.fa().rz1())
                    .and_expression("source_position", ctx.fa().rzu(pos+2)?)
                    .to_statement();
                ctx.explanation.add_statement(par);
            }
            data.borrowed.set_digit_mark(pos, 1, PositionalMark::Target)?;
            borrow_from_minuend(ctx, pos+1, &mut data.minuend).await?;

            if ctx.rules().explain_elementary_operations {
                ctx.explanation.add_statement(data.clone().to_statement());
            }
            data.minuend.remove_marks();

            // calculate difference
            let dim = data.minuend.get_digit_else_zero(pos);
            let borr = data.borrowed.get_digit_else_zero(pos);
            let new_dim = dim + (ctx.rules().radix as u8) * borr;
            let diff = new_dim - sub;
            if ctx.rules().explain_elementary_operations {
                let left_formula = BinaryData::sub(
                    ctx.fa().rzi(new_dim as i32)?,
                    ctx.fa().rzi(sub as i32)?).to_rc_expr();
                let right_formula = ctx.fa().rzi(diff as i32)?;
                let par = TextStatement::paragraph("Position {index}: {formula}")
                    .and_expression("index", ctx.fa().rzu(pos+1)?)
                    .and_equation("formula", left_formula, right_formula)
                    .to_statement();
                ctx.explanation.add_statement(par);
            }
            data.minuend.mark_digit_as_source(pos)?;
            data.subtrahend.mark_digit_as_source(pos)?;
            data.borrowed.mark_digit_as_source(pos)?;
            data.difference.set_digit_mark(pos, diff, PositionalMark::Target)?;
        }

        if ctx.rules().explain_elementary_operations {
            ctx.explanation.add_statement(data.clone().to_statement());
        }

        data.minuend.remove_marks();
        data.subtrahend.remove_marks();
        data.borrowed.remove_marks();
        data.difference.remove_marks();
    }

    return Natural::from_string(
        data.difference.radix,
        data.difference.to_digits_as_string().as_str());
}

async fn borrow_from_minuend(
    ctx: &mut CalcContext<'_>,
    start_pos: usize,
    minuend: &mut PositionalNumber,
) -> Result<(), CalcError> {
    for pos in start_pos..minuend.digits_len() {
        let digit = minuend.get_digit_else_zero(pos);
        if digit > 0 {
            let new_digit = digit - 1;
            if ctx.rules().explain_elementary_operations {
                let par = TextStatement::paragraph("Position {index}: {old} -> {new}")
                    .and_expression("index", ctx.fa().rzu(pos+1)?)
                    .and_expression("old", ctx.fa().rzi(digit as i32)?)
                    .and_expression("new", ctx.fa().rzi(new_digit as i32)?)
                    .to_statement();
                ctx.explanation.add_statement(par);
            }
            minuend.set_digit_mark(pos, new_digit, PositionalMark::Source)?;
            return Ok(());
        } else {
            // borrow
            if ctx.rules().explain_elementary_operations {
                let par = TextStatement::paragraph(
                    "Position {index}: borrowing {borrowed} from position {source_position} to position {index}")
                    .and_expression("index", ctx.fa().rzu(pos+1)?)
                    .and_expression("borrowed", ctx.fa().rz1())
                    .and_expression("source_position", ctx.fa().rzu(pos+2)?)
                    .to_statement();
                ctx.explanation.add_statement(par);
            }

            let new_minuend_digit = ctx.rules().radix as u8 - 1;
            if ctx.rules().explain_elementary_operations {
                let par = TextStatement::paragraph("Position {index}: {old} -> {new}")
                    .and_expression("index", ctx.fa().rzu(pos+1)?)
                    .and_expression("old", ctx.fa().rz0())
                    .and_expression("new", ctx.fa().rzi(new_minuend_digit as i32)?)
                    .to_statement();
                ctx.explanation.add_statement(par);
            }
            minuend.set_digit_mark(pos, new_minuend_digit, PositionalMark::Source)?;
        }
    }
    return Err(CalcError::from_string(format!("Cannot borrow number from minuend {}.", minuend)));
}


#[cfg(test)]
mod tests {
    use crate::calculator::tabular::subtraction::calculate_tabular_subtraction;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::result::dump_unicode_color;
    use crate::rules::CalcRules;
    use futures::executor::block_on;
    use matm_error::error::CalcError;
    use matm_model::core::natural::Natural;
    use matm_model::core::natural::RADIX_10;
    use matm_model::core::natural::RADIX_16;
    use matm_model::core::natural::RADIX_2;
    use matm_model::testing::asserts::assert_result_error_that;
    use matm_model::testing::asserts::assert_result_ok;
    use matm_model::testing::asserts::error_that::ErrorAssertThat;

    /// Calculates subtraction of numbers using columnar algorithm
    fn calculate_and_render_natural_subtraction(
        rules: CalcRules,
        minuend: &Natural,
        subtrahend: &Natural,
    ) -> Result<Natural, CalcError> {
        let mut ctx_data = CalcContextData::from_rules(&rules);
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let result = block_on(calculate_tabular_subtraction(&mut ctx, minuend, subtrahend))?;
        dump_unicode_color(&ctx.explanation);
        println!();
        return Ok(result);
    }

    fn run_subtraction_error(
        rules: CalcRules,
        minuend: &Natural,
        subtrahend: &Natural,
    ) -> ErrorAssertThat {
        let mut ctx_data = CalcContextData::from_rules(&rules);
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let result = block_on(calculate_tabular_subtraction(&mut ctx, minuend, subtrahend));
        assert_result_error_that(result)
    }

    fn get_subtraction_of_ints(radix: u32, minuend: u32, subtrahend: u32) -> u32 {
        let minuend_obj = Natural::from_int(radix, minuend).unwrap();
        let subtrahend_obj = Natural::from_int(radix, subtrahend).unwrap();
        let mut rules = CalcRules::default();
        rules.radix = radix;
        rules.explain_elementary_operations = true;
        let result = assert_result_ok(calculate_and_render_natural_subtraction(rules, &minuend_obj, &subtrahend_obj));
        return result.to_u32().unwrap();
    }

    #[test]
    fn test_subtraction_3_1() {
        assert_eq!(2, get_subtraction_of_ints(RADIX_10, 3, 1));
    }

    #[test]
    fn test_subtraction_9_2() {
        assert_eq!(7, get_subtraction_of_ints(RADIX_10, 9, 2));
    }

    #[test]
    fn test_subtraction_24_2() {
        assert_eq!(22, get_subtraction_of_ints(RADIX_10, 24, 2));
    }

    #[test]
    fn test_subtraction_100024_2() {
        assert_eq!(100022, get_subtraction_of_ints(RADIX_10, 100024, 2));
    }

    #[test]
    fn test_subtraction_24_5() {
        assert_eq!(19, get_subtraction_of_ints(RADIX_10, 24, 5));
    }

    #[test]
    fn test_subtraction_1000_1() {
        assert_eq!(999, get_subtraction_of_ints(RADIX_10, 1000, 1));
    }

    #[test]
    fn test_subtraction_2000_1() {
        assert_eq!(1999, get_subtraction_of_ints(RADIX_10, 2000, 1));
    }

    #[test]
    fn test_subtraction_3332000_40001() {
        assert_eq!(3291999, get_subtraction_of_ints(RADIX_10, 3332000, 40001));
    }

    #[test]
    fn test_subtraction_b1111_b0010() {
        assert_eq!(0b1101, get_subtraction_of_ints(RADIX_2, 0b1111, 0b0010));
    }

    #[test]
    fn test_subtraction_b1100_b0001() {
        assert_eq!(0b1011, get_subtraction_of_ints(RADIX_2, 0b1100, 0b0001));
    }

    #[test]
    fn test_subtraction_x2f_x1d() {
        assert_eq!(0x12, get_subtraction_of_ints(RADIX_16, 0x2f, 0x1d));
    }

    #[test]
    fn test_subtraction_invalid_minuend_radix() {
        let minuend = Natural::from_int(RADIX_16, 1).unwrap();
        let subtrahend = Natural::from_int(RADIX_10, 1).unwrap();
        let rules = CalcRules::default();

        run_subtraction_error(rules, &minuend, &subtrahend)
            .has_message("Invalid radix 16 of the minuend 16#1#, expected 10.");
    }

    #[test]
    fn test_subtraction_invalid_subtrahend_radix() {
        let minuend = Natural::from_int(RADIX_10, 1).unwrap();
        let subtrahend = Natural::from_int(RADIX_16, 1).unwrap();
        let rules = CalcRules::default();

        run_subtraction_error(rules, &minuend, &subtrahend)
            .has_message("Invalid radix 16 of the subtrahend 16#1#, expected 10.");
    }

    #[test]
    fn test_subtraction_minuend_subtrahend_negative() {
        let minuend = Natural::from_int(RADIX_10, 10).unwrap();
        let subtrahend = Natural::from_int(RADIX_10, 11).unwrap();
        let rules = CalcRules::default();

        run_subtraction_error(rules, &minuend, &subtrahend)
            .has_message("The minuend 10 must be greater than or equal to the subtrahend 11.");
    }
}
