use crate::calculator::context::CalcContext;
use matm_error::error::CalcError;
use matm_model::core::natural::Natural;
use matm_model::core::positional::PositionalMark;
use matm_model::core::positional::PositionalNumber;
use matm_model::expressions::binary::BinaryData;
use matm_model::expressions::ToExpression;
use matm_model::statements::tabular_multiplication::TabularMultiplicationData;
use matm_model::statements::text::TextStatement;
use matm_model::statements::ToStatement;

pub async fn calculate_tabular_multiplication(
    ctx: &mut CalcContext<'_>,
    multiplier: &Natural,
    multiplicand: &Natural,
) -> Result<Natural, CalcError> {
    // all numbers should have the same radix
    if multiplier.radix() != ctx.rules().radix {
        return Err(CalcError::from_string(format!(
            "Invalid radix {} of the multiplier {}, expected {}.",
            multiplier.radix(),
            multiplier,
            ctx.rules().radix)));
    }
    if multiplicand.radix() != ctx.rules().radix {
        return Err(CalcError::from_string(format!(
            "Invalid radix {} of the multiplicand {}, expected {}.",
            multiplicand.radix(),
            multiplicand,
            ctx.rules().radix)));
    }

    let mut data = TabularMultiplicationData {
        multiplier: PositionalNumber::from_natural(multiplier),
        multiplicand: PositionalNumber::from_natural(multiplicand),
        partial_products: Vec::new(),
        multiplication_carries: Vec::new(),
        addition_carry: PositionalNumber::new(ctx.rules().radix),
        product: PositionalNumber::new(ctx.rules().radix),
    };

    for outer_pos in 0..multiplicand.digits_len() {
        let d2 = multiplicand.get_digit(outer_pos).unwrap();
        data.multiplicand.mark_digit_as_source(outer_pos)?;
        data.multiplication_carries.push(PositionalNumber::new(ctx.rules().radix));
        data.partial_products.push(PositionalNumber::new(ctx.rules().radix));

        let mut last_partial_prod_pos = 0;
        for inner_pos in 0..multiplier.digits_len() {
            let d1 = multiplier.get_digit(inner_pos).unwrap();
            data.multiplier.mark_digit_as_source(inner_pos)?;

            let mul_carry_value = match data.multiplication_carries.last() {
                Some(carry) => carry.get_digit_else_zero(outer_pos+inner_pos),
                _ => 0,
            };

            let mul = u32::from(d1) * u32::from(d2)
                + u32::from(mul_carry_value);
            let d_mul = (mul % ctx.rules().radix) as u8;
            let c = (mul / ctx.rules().radix) as u8;
            if c > 0 {
                match data.multiplication_carries.last_mut() {
                    Some(carry) => {
                        carry.set_digit_mark(outer_pos+inner_pos+1, c, PositionalMark::Target)?;
                    }
                    _ => {}
                }
            }

            match data.partial_products.last_mut() {
                Some(partial_prod) => {
                    partial_prod.set_digit_mark(inner_pos+outer_pos, d_mul, PositionalMark::Target)?;
                }
                None => {}
            };

            if ctx.rules().explain_elementary_operations {
                let mut left_formula = BinaryData::mul(
                    ctx.fa().rzi(d1 as i32)?,
                    ctx.fa().rzi(d2 as i32)?
                ).to_rc_expr();
                if mul_carry_value > 0 {
                    left_formula = BinaryData::add(
                        left_formula,
                        ctx.fa().rzi(mul_carry_value as i32)?
                    ).to_rc_expr();
                }
                let right_formula = ctx.fa().rzi(mul as i32)?;
                let par = TextStatement::paragraph(
                    "Multiplication position {index1} by position {index2}: {formula}")
                    .and_expression("index1", ctx.fa().rzu(inner_pos+1)?)
                    .and_expression("index2", ctx.fa().rzu(outer_pos+1)?)
                    .and_equation("formula", left_formula, right_formula)
                    .to_statement();
                ctx.explanation.add_statement(par);
                ctx.explanation.add_statement(data.clone().to_statement());
            }

            // remove marks
            data.multiplier.remove_marks();
            match data.multiplication_carries.last_mut() {
                Some(carry) => {
                    carry.remove_marks();
                }
                _ => {}
            }
            match data.partial_products.last_mut() {
                Some(partial_prod) => {
                    partial_prod.remove_marks();
                }
                None => {}
            }

            last_partial_prod_pos = inner_pos+outer_pos;
        }

        // remove marks
        data.multiplicand.remove_marks();

        // add not used carry digits
        let carry_len = match data.multiplication_carries.last() {
            Some(carry) => carry.digits.len(),
            None => 0,
        };
        //for pos in multiplier.digits_len()..carry_len {
        for pos in last_partial_prod_pos+1..carry_len {
            let d = match data.multiplication_carries.last() {
                Some(carry) => carry.get_digit_else_zero(pos),
                None => 0,
            };
            match data.partial_products.last_mut() {
                Some(partial_prod) => {
                    partial_prod.set_digit_mark(pos, d, PositionalMark::Target)?;
                }
                None => {}
            }

            if ctx.rules().explain_elementary_operations {
                let par = TextStatement::paragraph("Copying carry from position {index}")
                    .and_expression("index", ctx.fa().rzu(pos+outer_pos+1)?)
                    .to_statement();
                ctx.explanation.add_statement(par);
                ctx.explanation.add_statement(data.clone().to_statement());
            }

            // remove marks
            match data.partial_products.last_mut() {
                Some(partial_prod) => {
                    partial_prod.remove_marks();
                }
                None => {}
            }
        }
    }

    // sum partial products
    let max_partial_products = data.partial_products.iter()
        .map(|p| p.digits.len())
        .max()
        .unwrap_or(0);
    for pos in 0..max_partial_products {
        // sum at index pos
        let mut sum: u32 = 0;
        for part in &mut data.partial_products {
            match part.get_digit(pos) {
                Some(d) => {
                    sum += u32::from(d);
                    part.mark_digit_as_source(pos)?;
                }
                _ => {}
            }
        }
        sum += u32::from(data.addition_carry.get_digit_else_zero(pos));
        let d = (sum % ctx.rules().radix) as u8;
        let c = (sum / ctx.rules().radix) as u8;
        if c > 0 {
            data.addition_carry.set_digit_mark(pos+1, c, PositionalMark::Target)?;
        }
        data.product.set_digit_mark(pos, d, PositionalMark::Target)?;

        if ctx.rules().explain_elementary_operations {
            let par = TextStatement::paragraph("Adding partial products at position {index}")
                .and_expression("index", ctx.fa().rzu(pos+1)?)
                .to_statement();
            ctx.explanation.add_statement(par);
            ctx.explanation.add_statement(data.clone().to_statement());
        }

        // remove marks
        for part in &mut data.partial_products {
            part.remove_marks();
        }
        data.product.remove_marks();
    }

    // add not used carry digits
    for pos in max_partial_products..data.addition_carry.digits.len() {
        let d = data.addition_carry.get_digit_else_zero(pos);
        data.product.set_digit_mark(pos, d, PositionalMark::Target)?;

        if ctx.rules().explain_elementary_operations {
            let par = TextStatement::paragraph("Copying carry from position {index}")
                .and_expression("index", ctx.fa().rzu(pos+1)?)
                .to_statement();
            ctx.explanation.add_statement(par);
            ctx.explanation.add_statement(data.clone().to_statement());
        }
        data.product.remove_marks();
    }

    if ctx.rules().explain_elementary_operations {
        ctx.explanation.add_statement(
            TextStatement::paragraph("Result:")
                .to_statement());
        ctx.explanation.add_statement(data.clone().to_statement());
    }

    return Natural::from_string(
        data.product.radix,
        data.product.to_digits_as_string().as_str());
}


#[cfg(test)]
mod tests {
    use crate::calculator::tabular::multiplication::calculate_tabular_multiplication;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::result::dump_unicode_color;
    use crate::rules::CalcRules;
    use futures::executor::block_on;
    use matm_error::error::CalcError;
    use matm_model::core::natural::Natural;
    use matm_model::core::natural::RADIX_10;
    use matm_model::core::natural::RADIX_16;
    use matm_model::core::natural::RADIX_2;
    use matm_model::testing::asserts::assert_result_error_that;
    use matm_model::testing::asserts::assert_result_ok;
    use matm_model::testing::asserts::error_that::ErrorAssertThat;

    /// Calculates multiplication of numbers using columnar algorithm
    fn calculate_and_render_natural_multiplication(
        rules: CalcRules,
        minuend: &Natural,
        subtrahend: &Natural,
    ) -> Result<Natural, CalcError> {
        let mut ctx_data = CalcContextData::from_rules(&rules);
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let result = block_on(calculate_tabular_multiplication(&mut ctx, minuend, subtrahend));
        dump_unicode_color(&ctx.explanation);
        println!();
        result
    }

    fn run_natural_multiplication_error(
        rules: CalcRules,
        minuend: &Natural,
        subtrahend: &Natural,
    ) -> ErrorAssertThat {
        let mut ctx_data = CalcContextData::from_rules(&rules);
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        assert_result_error_that(block_on(calculate_tabular_multiplication(&mut ctx, minuend, subtrahend)))
    }

    fn get_multiplication_of_ints(radix: u32, minuend: u32, subtrahend: u32) -> u32 {
        let minuend_obj = Natural::from_int(radix, minuend).unwrap();
        let subtrahend_obj = Natural::from_int(radix, subtrahend).unwrap();
        let mut rules = CalcRules::default();
        rules.radix = radix;
        rules.explain_elementary_operations = true;
        let result = assert_result_ok(calculate_and_render_natural_multiplication(rules, &minuend_obj, &subtrahend_obj));
        return result.to_u32().unwrap();
    }

    #[test]
    fn test_multiplication_0_0() {
        assert_eq!(0, get_multiplication_of_ints(RADIX_10, 0, 0));
    }

    #[test]
    fn test_multiplication_0_3() {
        assert_eq!(0, get_multiplication_of_ints(RADIX_10, 0, 3));
    }

    #[test]
    fn test_multiplication_2_3() {
        assert_eq!(6, get_multiplication_of_ints(RADIX_10, 2, 3));
    }

    // Example from "Arithmetic and Algebra" by Rosanne Proga 1986, page 23
    #[test]
    fn test_multiplication_2_5() {
        assert_eq!(10, get_multiplication_of_ints(RADIX_10, 2, 5));
    }

    // Example from "Arithmetic and Algebra" by Rosanne Proga 1986, page 26
    #[test]
    fn test_multiplication_36_4() {
        assert_eq!(144, get_multiplication_of_ints(RADIX_10, 36, 4));
    }

    // Example from "Arithmetic and Algebra" by Rosanne Proga 1986, page 26
    #[test]
    fn test_multiplication_87_32() {
        assert_eq!(2784, get_multiplication_of_ints(RADIX_10, 87, 32));
    }

    #[test]
    fn test_multiplication_111_3() {
        assert_eq!(333, get_multiplication_of_ints(RADIX_10, 111, 3));
    }

    #[test]
    fn test_multiplication_3_111() {
        assert_eq!(333, get_multiplication_of_ints(RADIX_10, 3, 111));
    }

    // Example from "Arithmetic and Algebra" by Rosanne Proga 1986, page 27
    #[test]
    fn test_multiplication_328_65() {
        assert_eq!(21320, get_multiplication_of_ints(RADIX_10, 328, 65));
    }

    #[test]
    fn test_multiplication_10_111() {
        assert_eq!(1110, get_multiplication_of_ints(RADIX_10, 10, 111));
    }

    // Example from "Arithmetic and Algebra" by Rosanne Proga 1986, page 29
    #[test]
    fn test_multiplication_132_308() {
        assert_eq!(40656, get_multiplication_of_ints(RADIX_10, 132, 308));
    }

    // Example from "Arithmetic and Algebra" by Rosanne Proga 1986, page 29
    #[test]
    fn test_multiplication_6127_4001() {
        assert_eq!(24514127, get_multiplication_of_ints(RADIX_10, 6127, 4001));
    }

    // Example from "Arithmetic and Algebra" by Rosanne Proga 1986, page 29
    #[test]
    fn test_multiplication_2187_50030() {
        assert_eq!(109415610, get_multiplication_of_ints(RADIX_10, 2187, 50030));
    }

    // Example from "Arithmetic and Algebra" by Rosanne Proga 1986, page 30
    #[test]
    fn test_multiplication_52_27() {
        assert_eq!(1404, get_multiplication_of_ints(RADIX_10, 52, 27));
    }

    // Example from "Arithmetic and Algebra" by Rosanne Proga 1986, page 30
    #[test]
    fn test_multiplication_285_893() {
        assert_eq!(254505, get_multiplication_of_ints(RADIX_10, 285, 893));
    }

    #[test]
    fn test_multiplication_b10_b111() {
        assert_eq!(0b1110, get_multiplication_of_ints(RADIX_2, 0b10, 0b111));
    }

    #[test]
    fn test_multiplication_0xa12_0x10() {
        assert_eq!(0xa120, get_multiplication_of_ints(RADIX_2, 0xa12, 0x10));
    }

    #[test]
    fn test_multiplication_invalid_multiplier_radix() {
        let multiplier = Natural::from_int(RADIX_16, 1).unwrap();
        let multiplicand = Natural::from_int(RADIX_10, 1).unwrap();
        let rules = CalcRules::default();

        run_natural_multiplication_error(rules, &multiplier, &multiplicand)
            .has_message("Invalid radix 16 of the multiplier 16#1#, expected 10.");
    }

    #[test]
    fn test_multiplication_invalid_multiplicand_radix() {
        let multiplier = Natural::from_int(RADIX_10, 1).unwrap();
        let multiplicand = Natural::from_int(RADIX_16, 1).unwrap();
        let rules = CalcRules::default();

        run_natural_multiplication_error(rules, &multiplier, &multiplicand)
            .has_message("Invalid radix 16 of the multiplicand 16#1#, expected 10.");
    }
}
