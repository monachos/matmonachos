use crate::calculator::tabular::addition::calculate_tabular_addition;
use crate::calculator::tabular::subtraction::calculate_tabular_subtraction;
use crate::calculator::context::CalcContext;
use crate::calculator::context::CalcContextData;
use crate::result::ResultContainer;
use crate::rules::CalcRules;
use crate::utils::division::DivisionResult;
use matm_error::error::CalcError;
use matm_model::core::natural::Natural;
use matm_model::core::positional::PositionalMark;
use matm_model::core::positional::PositionalNumber;
use matm_model::expressions::binary::BinaryData;
use matm_model::expressions::int::IntData;
use matm_model::expressions::sign::Sign;
use matm_model::expressions::ToExpression;
use matm_model::statements::tabular_division::TabularDivisionData;
use matm_model::statements::tabular_division::TabularDivisionLine;
use matm_model::statements::text::TextStatement;
use matm_model::statements::ToStatement;

pub(crate) type TabularDivisionResult = DivisionResult<Natural>;

/// Calculates division of two numbers
///
/// Returns TabularDivisionResult
pub(crate) async fn calculate_tabular_division(
    ctx: &mut CalcContext<'_>,
    dividend: &Natural,
    divisor: &Natural,
) -> Result<TabularDivisionResult, CalcError> {
    // all numbers should have the same radix
    if dividend.radix() != ctx.rules().radix {
        return Err(CalcError::from_string(format!(
            "Invalid radix {} of the dividend {}, expected {}.",
            dividend.radix(),
            dividend,
            ctx.rules().radix)));
    }
    if divisor.radix() != ctx.rules().radix {
        return Err(CalcError::from_string(format!(
            "Invalid radix {} of the divisor {}, expected {}.",
            divisor.radix(),
            divisor,
            ctx.rules().radix)));
    }
    if dividend < divisor {
        return Err(CalcError::from_string(format!("The dividend {} must be greater than or equal to the divisor {}.", dividend, divisor)));
    }
    if dividend == divisor {
        return Ok(TabularDivisionResult {
            quotient: Natural::one(ctx.rules().radix)?,
            remainder: Natural::zero(ctx.rules().radix)?,
        });
    }

    let mut data = TabularDivisionData {
        dividend: PositionalNumber::from_natural(dividend),
        divisor: PositionalNumber::from_natural(divisor),
        lines: Vec::new(),
        quotient: PositionalNumber::new(ctx.rules().radix),
    };
    if ctx.rules().explain_elementary_operations {
        ctx.explanation.add_statement(data.clone().to_statement());
    }

    // we start out of range
    let mut pos = dividend.digits_len();

    // find least sub-dividend that is greater than or equal to divisor
    let mut sub_dividend = Natural::zero(ctx.rules().radix)?;
    while sub_dividend < *divisor {
        pos -= 1;
        sub_dividend.append_digit_right(dividend.get_digit_else_error(pos)?)?;
    }

    // iterate through index 0
    let mut first_iteration = true;
    loop {
        // calculate partial quotient
        let div_res = calculate_division_by_addition(&sub_dividend, divisor).await?;
        let quotiend_digit = div_res.quotient as u8;
        data.divisor.mark_number_as_source();
        data.quotient.set_digit_mark(pos, quotiend_digit, PositionalMark::Target)?;
        if ctx.rules().explain_elementary_operations {
            if first_iteration {
                for sub_pos in pos..data.dividend.digits_len() {
                    data.dividend.mark_digit_as_source(sub_pos)?;
                }
            } else {
                if let Some(rem) = data.lines.last_mut() {
                    match rem {
                        TabularDivisionLine::Number(num) => {
                            num.mark_number_as_source();
                        }
                        TabularDivisionLine::Separator => {}
                    }
                }
            }
            let left_formula = BinaryData::div(
                IntData::from_natural(sub_dividend.clone(), Sign::Positive).to_rc_expr(),
                IntData::from_natural(divisor.clone(), Sign::Positive).to_rc_expr()
            ).to_rc_expr();
            let right_formula = ctx.fa().rzi(quotiend_digit as i32)?;
            let par = TextStatement::paragraph(
                "Calculating quotient: {formula}")
                .and_equation("formula", left_formula, right_formula)
                .to_statement();
            ctx.explanation.add_statement(par);
            ctx.explanation.add_statement(data.clone().to_statement());

            data.dividend.remove_marks();
            data.divisor.remove_marks();
            data.quotient.remove_marks();
            if let Some(rem) = data.lines.last_mut() {
                match rem {
                    TabularDivisionLine::Number(num) => {
                        num.remove_marks();
                    }
                    TabularDivisionLine::Separator => {}
                }
            }
        }

        // multiply calculated partial quotient by divisor
        let mut product = PositionalNumber::new(ctx.rules().radix);
        product.set_value_at(pos, &div_res.greatest_sum)?;
        product.mark_number_as_target();
        data.quotient.mark_digit_as_source(pos)?;
        data.divisor.mark_number_as_source();
        data.lines.push(TabularDivisionLine::Number(product.clone()));
        if ctx.rules().explain_elementary_operations {
            let left_formula = BinaryData::mul(
                ctx.fa().rzi(div_res.quotient as i32)?,
                IntData::from_natural(divisor.clone(), Sign::Positive).to_rc_expr()
            ).to_rc_expr();
            let right_formula = IntData::from_natural(div_res.greatest_sum.clone(), Sign::Positive)
                .to_rc_expr();
            let par = TextStatement::paragraph(
                "Calculating product: {formula}")
                .and_equation("formula", left_formula, right_formula)
                .to_statement();
            ctx.explanation.add_statement(par);
            ctx.explanation.add_statement(data.clone().to_statement());
        }
        data.divisor.remove_marks();
        data.quotient.remove_marks();
        match data.lines.last_mut() {
            Some(product) => {
                match product {
                    TabularDivisionLine::Number(num) => {
                        num.remove_marks();
                    }
                    TabularDivisionLine::Separator => {}
                }
            }
            None => {}
        }

        // calculate remainder: sub_dividend - product
        data.lines.push(TabularDivisionLine::Separator);
        product.shift_right(pos)?;
        let product_value = Natural::from_positional_number(&product)?;

        let mut sub_rules = CalcRules::default();
        sub_rules.radix = ctx.rules().radix;
        let mut sub_explanation = ResultContainer::new();
        let mut sub_ctx = CalcContext::new(
            ctx.rules(),
            &mut sub_rules,
            &mut sub_explanation);

        let remainder = calculate_tabular_subtraction(
            &mut sub_ctx,
            &sub_dividend,
            &product_value).await?;

        let mut pos_remainder = PositionalNumber::new(ctx.rules().radix);
        pos_remainder.set_value_at(pos, &remainder)?;
        data.lines.push(TabularDivisionLine::Number(pos_remainder.clone()));
        if ctx.rules().explain_elementary_operations {
            if first_iteration {
                for sub_pos in pos..data.dividend.digits_len() {
                    data.dividend.mark_digit_as_source(sub_pos)?;
                }
            } else {
                let prev_rem_index = data.lines.len() - 4;
                if let Some(prev_rem) = data.lines.get_mut(prev_rem_index) {
                    match prev_rem {
                        TabularDivisionLine::Number(num) => {
                            num.mark_number_as_source();
                        }
                        TabularDivisionLine::Separator => {}
                    }
                }
            }
            let prod_index = data.lines.len() - 3;
            if let Some(prod) = data.lines.get_mut(prod_index) {
                match prod {
                    TabularDivisionLine::Number(num) => {
                        num.mark_number_as_source();
                    }
                    TabularDivisionLine::Separator => {}
                }
            }
            match data.lines.last_mut() {
                Some(rem) => {
                    match rem {
                        TabularDivisionLine::Number(num) => {
                            num.mark_number_as_target();
                        }
                        TabularDivisionLine::Separator => {}
                    }
                }
                None => {}
            }
            let left_formula = BinaryData::sub(
                IntData::from_natural(sub_dividend.clone(), Sign::Positive).to_rc_expr(),
                IntData::from_natural(product_value.clone(), Sign::Positive).to_rc_expr()
            ).to_rc_expr();
            let right_formula = IntData::from_natural(remainder.clone(), Sign::Positive)
                .to_rc_expr();
            ctx.explanation.add_statement(
                TextStatement::paragraph(
                    "Calculating remainder: {formula}")
                    .and_equation("formula", left_formula, right_formula)
                    .to_statement());
            ctx.explanation.add_statement(data.clone().to_statement());
            if first_iteration {
                data.dividend.remove_marks();
            } else {
                let prev_rem_index = data.lines.len() - 4;
                if let Some(prev_rem) = data.lines.get_mut(prev_rem_index) {
                    match prev_rem {
                        TabularDivisionLine::Number(num) => {
                            num.remove_marks();
                        }
                        TabularDivisionLine::Separator => {}
                    }
                }
            }
            let prod_index = data.lines.len() - 3;
            if let Some(prod) = data.lines.get_mut(prod_index) {
                match prod {
                    TabularDivisionLine::Number(num) => {
                        num.remove_marks();
                    }
                    TabularDivisionLine::Separator => {}
                }
            }
            match data.lines.last_mut() {
                Some(product) => {
                    match product {
                        TabularDivisionLine::Number(num) => {
                            num.remove_marks();
                        }
                        TabularDivisionLine::Separator => {}
                    }
                }
                None => {}
            }
        }

        if pos > 0 {
            pos -= 1;
            // copy digit from dividend
            let digit = dividend.get_digit_else_error(pos)?;
            pos_remainder.set_digit(pos, digit)?;
            // update line
            data.lines.remove(data.lines.len() - 1);
            data.lines.push(TabularDivisionLine::Number(pos_remainder.clone()));
            if ctx.rules().explain_elementary_operations {
                data.dividend.mark_digit_as_source(pos)?;
                match data.lines.last_mut() {
                    Some(product) => {
                        match product {
                            TabularDivisionLine::Number(num) => {
                                num.mark_digit_as_target(pos)?;
                            }
                            TabularDivisionLine::Separator => {}
                        }
                    }
                    None => {}
                }
                let par = TextStatement::paragraph("Copying next digit from position {index}")
                    .and_expression("index", ctx.fa().rzu(pos+1)?)
                    .to_statement();
                ctx.explanation.add_statement(par);
                ctx.explanation.add_statement(data.clone().to_statement());
                data.dividend.remove_marks();
                match data.lines.last_mut() {
                    Some(product) => {
                        match product {
                            TabularDivisionLine::Number(num) => {
                                num.remove_marks();
                            }
                            TabularDivisionLine::Separator => {}
                        }
                    }
                    None => {}
                }
            }

            pos_remainder.shift_right(pos)?;
            sub_dividend = Natural::from_positional_number(&pos_remainder)?;
        } else {
            return Ok(TabularDivisionResult {
                quotient: Natural::from_positional_number(&data.quotient)?,
                remainder: remainder,
            });
        }

        first_iteration = false;
    }
}

struct DivisionByAdditionResult {
    pub quotient: usize,
    pub greatest_sum: Natural,
}

async fn calculate_division_by_addition(
    dividend: &Natural,
    divisor: &Natural,
) -> Result<DivisionByAdditionResult, CalcError> {
    // all numbers should have the same radix
    if dividend.radix() != divisor.radix() {
        return Err(CalcError::from_string(format!(
            "Cannot divide numbers of differ radix: {} and {}.",
            dividend,
            divisor)));
    }
    if divisor.is_zero() {
        return Err(CalcError::from_string(format!(
            "Cannot divide number {} by zero.",
            dividend)));
    }

    let mut ctx_data = CalcContextData::with_rules(|rules| {
        rules.radix = dividend.radix();
    });
    let mut ctx = CalcContext::from_data(&mut ctx_data);

    let mut result = DivisionByAdditionResult {
        quotient: 0,
        greatest_sum: Natural::zero(dividend.radix())?,
    };

    loop {
        let addends: Vec<Natural> = vec![result.greatest_sum.clone(), divisor.clone()];
        let sum = calculate_tabular_addition(&mut ctx, &addends).await?;
        match sum.partial_cmp(dividend) {
            Some(ord) => {
                match ord {
                    std::cmp::Ordering::Less => {
                        result.quotient += 1;
                        result.greatest_sum = sum.clone();
                    }
                    std::cmp::Ordering::Equal => {
                        result.quotient += 1;
                        result.greatest_sum = sum.clone();
                        break;
                    }
                    std::cmp::Ordering::Greater => {
                        break;
                    }
                }
            }
            None => {
                break;
            }
        }
    }

    return Ok(result);
}


#[cfg(test)]
mod tests {
    use crate::calculator::tabular::division::calculate_division_by_addition;
    use crate::calculator::tabular::division::calculate_tabular_division;
    use crate::calculator::tabular::division::TabularDivisionResult;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::result::dump_unicode_color;
    use crate::rules::CalcRules;
    use crate::utils::division::DivisionResult;
    use futures::executor::block_on;
    use matm_error::error::CalcError;
    use matm_model::core::natural::Natural;
    use matm_model::core::natural::RADIX_10;
    use matm_model::core::natural::RADIX_16;
    use matm_model::testing::asserts::assert_result_error_that;
    use matm_model::testing::asserts::assert_result_ok;
    use matm_model::testing::asserts::error_that::ErrorAssertThat;

    /// Calculates division of numbers using columnar algorithm
    fn calculate_and_render_natural_division(
        rules: CalcRules,
        dividend: &Natural,
        divisor: &Natural,
    ) -> Result<TabularDivisionResult, CalcError> {
        let mut ctx_data = CalcContextData::from_rules(&rules);
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let result = block_on(calculate_tabular_division(&mut ctx, dividend, divisor));
        dump_unicode_color(&ctx.explanation);
        println!();
        result
    }

    fn run_division_error(
        rules: CalcRules,
        dividend: &Natural,
        divisor: &Natural,
    ) -> ErrorAssertThat {
        let mut ctx_data = CalcContextData::from_rules(&rules);
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        assert_result_error_that(block_on(calculate_tabular_division(&mut ctx, dividend, divisor)))
    }

    fn get_division_of_ints(radix: u32, dividend: u32, divisor: u32) -> DivisionResult<u32> {
        let dividend_obj = Natural::from_int(radix, dividend).unwrap();
        let divisor_obj = Natural::from_int(radix, divisor).unwrap();
        let mut rules = CalcRules::default();
        rules.radix = radix;
        rules.explain_elementary_operations = true;
        let res = assert_result_ok(
            calculate_and_render_natural_division(rules, &dividend_obj, &divisor_obj));
        return DivisionResult {
            quotient: res.quotient.to_u32().unwrap(),
            remainder: res.remainder.to_u32().unwrap(),
        };
    }

    #[test]
    fn test_division_123_123() {
        let res: DivisionResult<u32> = get_division_of_ints(RADIX_10, 123, 123);
        assert_eq!(1, res.quotient);
        assert_eq!(0, res.remainder);
    }

    // Example from "Arithmetic and Algebra" by Rosanne Proga 1986, page 36
    #[test]
    fn test_division_29_4() {
        let res: DivisionResult<u32> = get_division_of_ints(RADIX_10, 29, 4);
        assert_eq!(7, res.quotient);
        assert_eq!(1, res.remainder);
    }

    // Example from "Arithmetic and Algebra" by Rosanne Proga 1986, page 37
    #[test]
    fn test_division_53_7() {
        let res: DivisionResult<u32> = get_division_of_ints(RADIX_10, 53, 7);
        assert_eq!(7, res.quotient);
        assert_eq!(4, res.remainder);
    }

    // Example from "Arithmetic and Algebra" by Rosanne Proga 1986, page 37
    #[test]
    fn test_division_153_9() {
        let res: DivisionResult<u32> = get_division_of_ints(RADIX_10, 153, 9);
        assert_eq!(17, res.quotient);
        assert_eq!(0, res.remainder);
    }

    // Example from "Arithmetic and Algebra" by Rosanne Proga 1986, page 37
    #[test]
    fn test_division_486_7() {
        let res: DivisionResult<u32> = get_division_of_ints(RADIX_10, 486, 7);
        assert_eq!(69, res.quotient);
        assert_eq!(3, res.remainder);
    }

    // Example from "Arithmetic and Algebra" by Rosanne Proga 1986, page 38
    #[test]
    fn test_division_2695_4() {
        let res: DivisionResult<u32> = get_division_of_ints(RADIX_10, 2695, 4);
        assert_eq!(673, res.quotient);
        assert_eq!(3, res.remainder);
    }

    // Example from "Arithmetic and Algebra" by Rosanne Proga 1986, page 38
    #[test]
    fn test_division_284_32() {
        let res: DivisionResult<u32> = get_division_of_ints(RADIX_10, 284, 32);
        assert_eq!(8, res.quotient);
        assert_eq!(28, res.remainder);
    }

    // Example from "Arithmetic and Algebra" by Rosanne Proga 1986, page 39
    #[test]
    fn test_division_8467_53() {
        let res: DivisionResult<u32> = get_division_of_ints(RADIX_10, 8467, 53);
        assert_eq!(159, res.quotient);
        assert_eq!(40, res.remainder);
    }

    // Example from "Arithmetic and Algebra" by Rosanne Proga 1986, page 39
    #[test]
    fn test_division_68921_213() {
        let res: DivisionResult<u32> = get_division_of_ints(RADIX_10, 68921, 213);
        assert_eq!(323, res.quotient);
        assert_eq!(122, res.remainder);
    }

    // Example from "Arithmetic and Algebra" by Rosanne Proga 1986, page 40
    #[test]
    fn test_division_21350_14() {
        let res: DivisionResult<u32> = get_division_of_ints(RADIX_10, 21350, 14);
        assert_eq!(1525, res.quotient);
        assert_eq!(0, res.remainder);
    }

    #[test]
    fn test_division_invalid_dividend_radix() {
        let dividend = Natural::from_int(RADIX_16, 1).unwrap();
        let divisor = Natural::from_int(RADIX_10, 1).unwrap();
        let rules = CalcRules::default();

        run_division_error(rules, &dividend, &divisor)
            .has_message("Invalid radix 16 of the dividend 16#1#, expected 10.");
    }

    #[test]
    fn test_division_invalid_divisor_radix() {
        let dividend = Natural::from_int(RADIX_10, 1).unwrap();
        let divisor = Natural::from_int(RADIX_16, 1).unwrap();
        let rules = CalcRules::default();

        run_division_error(rules, &dividend, &divisor)
            .has_message("Invalid radix 16 of the divisor 16#1#, expected 10.");
    }

    #[test]
    fn test_division_dividend_divisor_negative() {
        let dividend = Natural::from_int(RADIX_10, 10).unwrap();
        let divisor = Natural::from_int(RADIX_10, 11).unwrap();
        let rules = CalcRules::default();

        run_division_error(rules, &dividend, &divisor)
            .has_message("The dividend 10 must be greater than or equal to the divisor 11.");
    }

    //-----------------------------------------------

    fn get_division_by_addition_of_ints(dividend: u32, divisor: u32) -> (usize, u32) {
        let dividend_obj = Natural::from_int(RADIX_10, dividend).unwrap();
        let divisor_obj = Natural::from_int(RADIX_10, divisor).unwrap();
        let result = assert_result_ok(
            block_on(calculate_division_by_addition(&dividend_obj, &divisor_obj)));
        return (result.quotient, result.greatest_sum.to_u32().unwrap());
    }

    #[test]
    fn test_division_by_addition_2_3() {
        let (quotient, sum) = get_division_by_addition_of_ints(2, 3);
        assert_eq!(0, quotient);
        assert_eq!(0, sum);
    }

    #[test]
    fn test_division_by_addition_3_3() {
        let (quotient, sum) = get_division_by_addition_of_ints(3, 3);
        assert_eq!(1, quotient);
        assert_eq!(3, sum);
    }

    #[test]
    fn test_division_by_addition_9_3() {
        let (quotient, sum) = get_division_by_addition_of_ints(9, 3);
        assert_eq!(3, quotient);
        assert_eq!(9, sum);
    }

    #[test]
    fn test_division_by_addition_10_3() {
        let (quotient, sum) = get_division_by_addition_of_ints(10, 3);
        assert_eq!(3, quotient);
        assert_eq!(9, sum);
    }

    #[test]
    fn test_division_by_addition_different_radix() {
        let dividend = Natural::from_int(RADIX_10, 10).unwrap();
        let divisor = Natural::from_int(RADIX_16, 3).unwrap();
        assert_result_error_that(
            block_on(calculate_division_by_addition(&dividend, &divisor)))
            .has_message("Cannot divide numbers of differ radix: 10 and 16#3#.");
    }

    #[test]
    fn test_division_by_addition_divide_by_zero() {
        let dividend = Natural::from_int(RADIX_10, 10).unwrap();
        let divisor = Natural::zero(RADIX_10).unwrap();
        assert_result_error_that(
            block_on(calculate_division_by_addition(&dividend, &divisor)))
            .has_message("Cannot divide number 10 by zero.");
    }
}
