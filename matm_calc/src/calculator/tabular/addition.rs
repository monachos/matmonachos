use crate::calculator::context::CalcContext;
use matm_error::error::CalcError;
use matm_model::core::natural::Natural;
use matm_model::core::positional::PositionalMark;
use matm_model::core::positional::PositionalNumber;
use matm_model::expressions::binary::BinaryData;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;
use matm_model::statements::tabular_addition::TabularAdditionData;
use matm_model::statements::text::TextStatement;
use matm_model::statements::ToStatement;
use std::collections::VecDeque;
use std::rc::Rc;

pub async fn calculate_tabular_addition(
    ctx: &mut CalcContext<'_>,
    addends: &Vec<Natural>,
) -> Result<Natural, CalcError> {
    for v in addends {
        // all numbers should have the same radix
        if v.radix() != ctx.rules().radix {
            return Err(CalcError::from_string(format!(
                "Invalid radix {} of the number {}, expected {}.",
                v.radix(),
                v,
                ctx.rules().radix)));
        }
    }

    let max_len = Natural::get_max_digits_length(addends);

    let mut data = TabularAdditionData {
        addends: addends.iter()
            .map(|a| PositionalNumber::from_natural(a))
            .collect(),
        sum: PositionalNumber::new(ctx.rules().radix),
        carry: PositionalNumber::new(ctx.rules().radix),
    };

    // calculate sum
    for pos in 0..max_len {
        let mut digit_sum: u32 = 0;
        let mut addends_queue: VecDeque<Expression> = VecDeque::with_capacity(addends.len());

        // add carry
        let carry_d = data.carry.get_digit_else_zero(pos);
        if carry_d > 0 {
            digit_sum += u32::from(carry_d);
            addends_queue.push_back(ctx.fa().ezu(carry_d as usize)?);
        }
        data.carry.mark_digit_as_source(pos)?;

        // sum input arguments
        for addend in addends {
            match addend.get_digit(pos) {
                Some(d) => {
                    digit_sum += u32::from(d);
                    addends_queue.push_back(ctx.fa().ezu(d as usize)?);
                }
                None => {
                    addends_queue.push_back(ctx.fa().ezu(0)?);
                }
            }
        }
        // mark input digits
        for addend in &mut data.addends {
            addend.mark_digit_as_source(pos)?;
        }

        let d = (digit_sum % ctx.rules().radix) as u8;
        data.sum.set_digit_mark(pos, d, PositionalMark::Target)?;

        // add rest of sum to carry
        let carry_value = digit_sum / ctx.rules().radix;
        if carry_value > 0 {
            data.carry.add_value_to_digit(pos + 1, carry_value, PositionalMark::Target)?;
        }

        if ctx.rules().explain_elementary_operations {
            let mut left_formula: Rc<Expression> = Rc::new(addends_queue.pop_front().unwrap());
            while let Some(e) = addends_queue.pop_front() {
                left_formula = BinaryData::add(left_formula, Rc::new(e)).to_rc_expr();
            }
            let right_formula = ctx.fa().rzu(digit_sum as usize)?;
            let par = TextStatement::paragraph("Addition at position {index}: {formula}")
                .and_expression("index", ctx.fa().rzu(pos+1)?)
                .and_equation("formula", left_formula, right_formula)
                .to_statement();
            ctx.explanation.add_statement(par);
            ctx.explanation.add_statement(data.clone().to_statement());
        }

        // remove marks
        for addend in &mut data.addends {
            addend.remove_marks();
        }
        data.carry.remove_marks();
        data.sum.remove_marks();
    }
    // add not used carry digits
    for pos in max_len..data.carry.digits_len() {
        let d = data.carry.get_digit_else_zero(pos);
        data.carry.mark_digit_as_source(pos)?;
        data.sum.set_digit_mark(pos, d, PositionalMark::Target)?;

        if ctx.rules().explain_elementary_operations {
            let par = TextStatement::paragraph("Addition at position {index}")
                .and_expression("index", ctx.fa().rzu(pos+1)?)
                .to_statement();
            ctx.explanation.add_statement(par);
            ctx.explanation.add_statement(data.clone().to_statement());
        }

        data.carry.remove_marks();
        data.sum.remove_marks();
    }

    return Natural::from_string(
        data.sum.radix,
        data.sum.to_digits_as_string().as_str());
}

#[cfg(test)]
mod tests {
    use crate::calculator::tabular::addition::calculate_tabular_addition;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::result::dump_unicode_color;
    use futures::executor::block_on;
    use matm_error::error::CalcError;
    use matm_model::core::natural::Natural;
    use matm_model::core::natural::RADIX_10;
    use matm_model::core::natural::RADIX_16;
    use matm_model::core::natural::RADIX_2;
    use matm_model::statements::text::TextStatement;
    use matm_model::statements::ToStatement;
    use matm_model::testing::asserts::assert_result_error_that;
    use matm_model::testing::asserts::assert_result_ok;

    /// Calculates sum of numbers using columnar addition
    fn calculate_and_render_natural_sum(
        ctx: &mut CalcContext<'_>,
        values: &Vec<Natural>,
    ) -> Result<Natural, CalcError> {
        let result = block_on(calculate_tabular_addition(ctx, values))?;
        ctx.explanation.add_statement(
            TextStatement::paragraph(
                "")
                .to_statement());
        return Ok(result);
    }

    fn get_sum_of_ints(radix: u32, int_values: Vec<u32>) -> u32 {
        let mut ctx_data = CalcContextData::with_rules(|rules| {
            rules.radix = radix;
            rules.explain_elementary_operations = true;
        });
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let values: Vec<Natural> = int_values.iter()
            .map(|v| Natural::from_int(radix, *v).unwrap())
            .collect();

        let result = assert_result_ok(calculate_and_render_natural_sum(&mut ctx, &values));

        dump_unicode_color(&ctx.explanation);

        return result.to_u32().unwrap();
    }

    #[test]
    fn test_sum_1_2() {
        assert_eq!(3, get_sum_of_ints(RADIX_10, vec![1, 2]));
    }

    #[test]
    fn test_sum_123456_123() {
        assert_eq!(123579, get_sum_of_ints(RADIX_10, vec![123456, 123]));
    }

    #[test]
    fn test_sum_9_2() {
        assert_eq!(11, get_sum_of_ints(RADIX_10, vec![9, 2]));
    }

    #[test]
    fn test_sum_999_2() {
        assert_eq!(1001, get_sum_of_ints(RADIX_10, vec![999, 2]));
    }

    #[test]
    fn test_sum_of_three() {
        assert_eq!(1152, get_sum_of_ints(RADIX_10, vec![123, 1001, 28]));
    }

    #[test]
    fn test_sum_1b_11b() {
        assert_eq!(0b100, get_sum_of_ints(RADIX_2, vec![0b1, 0b11]));
    }

    #[test]
    fn test_sum_of_many_1b() {
        assert_eq!(0b1010, get_sum_of_ints(RADIX_2, vec![0b1; 10]));
    }

    #[test]
    fn test_sum_of_many_11b() {
        assert_eq!(0b11110, get_sum_of_ints(RADIX_2, vec![0b11; 10]));
    }

    #[test]
    fn test_sum_invalid_radix() {
        let mut ctx_data = CalcContextData::with_rules(|rules| {
            rules.radix = RADIX_10;
            rules.explain_elementary_operations = true;
        });
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let values = vec![
            Natural::from_int(RADIX_16, 0x31).unwrap()
        ];

        assert_result_error_that(calculate_and_render_natural_sum(&mut ctx, &values))
            .has_message("Invalid radix 16 of the number 16#31#, expected 10.");

        dump_unicode_color(&ctx.explanation);
    }
}
