#[cfg(test)]
mod tests {
    use crate::calculator::linear::solvers::solve_eq_system_using_gaussian_elimination;
    use crate::calculator::linear::solvers::SolveGaussianConfig;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::matrix;
    use crate::result::dump_unicode_color;
    use futures::executor::block_on;
    use matm_model::expressions::object::GaussPivotType;
    use matm_model::testing::asserts::assert_matrix_that;
    use matm_model::testing::asserts::assert_result_error_that;
    use matm_model::testing::asserts::assert_result_ok;

    #[test]
    fn test_gaussian_elimination_and_solve_sample_1() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            1, 2, (-2),
            2, 1, 1,
            3, 2, 1
        ).unwrap();
        ctx.explanation.add_name_eq_matrix("A", a.clone()).unwrap();

        let b = matrix!(ctx.fa(), 3, 1;
            4,
            0,
            1
        ).unwrap();
        ctx.explanation.add_name_eq_matrix("b", b.clone()).unwrap();

        let cfg = SolveGaussianConfig::default();

        let result = block_on(solve_eq_system_using_gaussian_elimination(&mut ctx, &cfg, &a, &b));
        dump_unicode_color(&ctx.explanation);

        let x = assert_result_ok(result);
        ctx.explanation.add_name_eq_matrix("x", x.clone()).unwrap();

        let expected = matrix!(ctx.fa(), 3, 1;
            0, 1, (-1)).unwrap();
        assert_matrix_that(&x)
            .is_equal_to(expected);
    }

    #[test]
    fn test_gaussian_elimination_and_solve_sample_2() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 4, 4;
            1, 1, 1, (-1),
            2, 1, 1, 0,
            (-1), 0, 1, (-1),
            3, 2, (-1), 2
        ).unwrap();
        ctx.explanation.add_name_eq_matrix("A", a.clone()).unwrap();

        let b = matrix!(ctx.fa(), 4, 1;
            2,
            3,
            0,
            (-1)
        ).unwrap();
        ctx.explanation.add_name_eq_matrix("b", b.clone()).unwrap();

        let cfg = SolveGaussianConfig::default();

        let result = block_on(solve_eq_system_using_gaussian_elimination(&mut ctx, &cfg, &a, &b));
        dump_unicode_color(&ctx.explanation);

        let x = assert_result_ok(result);
        ctx.explanation.add_name_eq_matrix("x", x.clone()).unwrap();

        let expected = matrix!(ctx.fa(), 4, 1;
            2, (-2), 1, (-1)).unwrap();
        assert_matrix_that(&x)
            .is_equal_to(expected);
    }

    #[test]
    fn test_gaussian_elimination_and_solve_sample_3() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 4, 4;
            1, 1, (-1), (-1),
            2, 3, (-2), 1,
            3, 0, 5, 0,
            (-1), 1, (-3), 2
        ).unwrap();
        ctx.explanation.add_name_eq_matrix("A", a.clone()).unwrap();

        let b = matrix!(ctx.fa(), 4, 1;
            0,
            4,
            0,
            3
        ).unwrap();
        ctx.explanation.add_name_eq_matrix("b", b.clone()).unwrap();

        let cfg = SolveGaussianConfig::default();

        let result = block_on(solve_eq_system_using_gaussian_elimination(&mut ctx, &cfg, &a, &b));
        dump_unicode_color(&ctx.explanation);

        let x = assert_result_ok(result);
        ctx.explanation.add_name_eq_matrix("x", x.clone()).unwrap();

        let expected = matrix!(ctx.fa(), 4, 1;
            0, 1, 0, 1).unwrap();
        assert_matrix_that(&x)
            .is_equal_to(expected);
    }

    #[test]
    fn test_gaussian_elimination_and_solve_sample_4() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 4, 4;
            2, 0, 1, 0,
            0, (-3), (-1), (-1),
            2, 0, 5, 3,
            3, (-3/2), 5, (9/2)
        ).unwrap();
        ctx.explanation.add_name_eq_matrix("A", a.clone()).unwrap();

        let b = matrix!(ctx.fa(), 4, 1;
            5,
            1,
            12,
            17
        ).unwrap();
        ctx.explanation.add_name_eq_matrix("b", b.clone()).unwrap();

        let mut cfg = SolveGaussianConfig::default();
        cfg.pivot = GaussPivotType::IfLessThanMaxAbs;

        let result = block_on(solve_eq_system_using_gaussian_elimination(&mut ctx, &cfg, &a, &b));
        dump_unicode_color(&ctx.explanation);

        let x = assert_result_ok(result);

        let expected = matrix!(ctx.fa(), 4, 1;
            2,
            (-1),
            1,
            1
        ).unwrap();
        assert_matrix_that(&x).is_equal_to(expected);
    }

    #[test]
    fn test_gaussian_elimination_and_solve_sample_5() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            (-1), 1, (-4),
            2, 2, 0,
            3, 3, 2
        ).unwrap();
        ctx.explanation.add_name_eq_matrix("A", a.clone()).unwrap();

        let b = matrix!(ctx.fa(), 3, 1;
            0,
            1,
            (1/2)
        ).unwrap();
        ctx.explanation.add_name_eq_matrix("b", b.clone()).unwrap();

        let mut cfg = SolveGaussianConfig::default();
        cfg.pivot = GaussPivotType::IfLessThanMaxAbs;

        let result = block_on(solve_eq_system_using_gaussian_elimination(&mut ctx, &cfg, &a, &b));
        dump_unicode_color(&ctx.explanation);

        let x = assert_result_ok(result);

        let expected = matrix!(ctx.fa(), 3, 1;
            (5/4),
            (-3/4),
            (-1/2)
        ).unwrap();
        assert_matrix_that(&x).is_equal_to(expected);
    }

    #[test]
    fn test_gaussian_elimination_and_solve_sample_6() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 5, 5;
            0, 0, 2, 1, 2,
            0, 1, 0, 2, (-1),
            1, 2, 0, (-2), 0,
            0, 0, 0, (-1), 1,
            0, 1, (-1), 1, (-1)
        ).unwrap();
        ctx.explanation.add_name_eq_matrix("A", a.clone()).unwrap();

        let b = matrix!(ctx.fa(), 5, 1;
            1,
            1,
            (-4),
            (-2),
            (-1)
        ).unwrap();
        ctx.explanation.add_name_eq_matrix("b", b.clone()).unwrap();

        let mut cfg = SolveGaussianConfig::default();
        cfg.pivot = GaussPivotType::IfLessThanMaxAbs;

        let result = block_on(solve_eq_system_using_gaussian_elimination(&mut ctx, &cfg, &a, &b));
        dump_unicode_color(&ctx.explanation);

        let x = assert_result_ok(result);

        let expected = matrix!(ctx.fa(), 5, 1;
            2,
            (-2),
            1,
            1,
            (-1)
        ).unwrap();
        assert_matrix_that(&x).is_equal_to(expected);
    }

    #[test]
    fn test_solve_eq_system_using_gaussian_elimination_not_square() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 2;
            2, 0,
            0, (-3),
            2, 0
        ).unwrap();

        let b = matrix!(ctx.fa(), 3, 1;
            5,
            1,
            12
        ).unwrap();

        let mut cfg = SolveGaussianConfig::default();
        cfg.pivot = GaussPivotType::IfLessThanMaxAbs;

        let result = block_on(solve_eq_system_using_gaussian_elimination(&mut ctx, &cfg, &a, &b));
        dump_unicode_color(&ctx.explanation);

        assert_result_error_that(result)
            .has_message("Matrix A is not square.");
    }

    #[test]
    fn test_solve_eq_system_using_gaussian_elimination_diff_rows() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 2, 2;
            2, 0,
            0, (-3)
        ).unwrap();

        let b = matrix!(ctx.fa(), 3, 1;
            5,
            1,
            12
        ).unwrap();

        let mut cfg = SolveGaussianConfig::default();
        cfg.pivot = GaussPivotType::IfLessThanMaxAbs;

        let result = block_on(solve_eq_system_using_gaussian_elimination(&mut ctx, &cfg, &a, &b));
        dump_unicode_color(&ctx.explanation);

        assert_result_error_that(result)
            .has_message("Matrices A and b have different number of rows.");
    }
}