use std::rc::Rc;

use matm_error::error::CalcError;
use matm_model::core::index::Index;
use matm_model::core::index_range::IndexRange;
use matm_model::expressions::binary::BinaryData;
use matm_model::expressions::equation::EquationData;
use matm_model::expressions::fractions::FracData;
use matm_model::expressions::matrices::MatrixData;
use matm_model::expressions::object::GaussPivotType;
use matm_model::expressions::symbol::SymbolData;
use matm_model::expressions::unary::UnaryData;
use matm_model::expressions::ToExpression;
use matm_model::statements::text::TextStatement;
use matm_model::statements::ToStatement;

use crate::calculator::linear::det::calculate_determinant;
use crate::calculator::linear::det::CalculateDeterminantConfig;
use crate::calculator::linear::gaussian::perform_gaussian_elimination;
use crate::calculator::linear::gaussian::GaussianEliminationCalculatorConfig;
use crate::calculator::calculate_equation_statement;
use crate::calculator::context::CalcContext;
use crate::substitution::substitute_expression;
use crate::substitution::SymbolTable;

pub struct SolveGaussianConfig {
    /// name of input matrix
    pub input_matrix_name: String,

    /// name of constant column (one-column matrix)
    pub constant_column_name: String,

    /// name of output matrix
    pub output_column_name: String,

    pub pivot: GaussPivotType,
    pub make_identity: bool,
}

impl SolveGaussianConfig {
    pub fn default() -> Self {
        Self {
            input_matrix_name: String::from("A"),
            constant_column_name: String::from("b"),
            output_column_name: String::from("x"),
            pivot: GaussPivotType::No,
            make_identity: false,
        }
    }
}

/// Solves equation system using Gaussian elimination method
///
/// Input:
///
///   a - square matrix
///
///   b - 1-column matrix
///
/// Returns:
///
///   x - solved variables
pub async fn solve_eq_system_using_gaussian_elimination(
    ctx: &mut CalcContext<'_>,
    cfg: &SolveGaussianConfig,
    a: &MatrixData,
    b: &MatrixData,
) -> Result<MatrixData, CalcError> {
    if !a.is_square() {
        return Err(CalcError::from_string(format!(
            "Matrix {} is not square.",
            cfg.input_matrix_name
        )));
    }
    if a.rows() != b.rows() {
        return Err(CalcError::from_string(format!(
            "Matrices {} and {} have different number of rows.",
            cfg.input_matrix_name,
            cfg.constant_column_name
        )));
    }
    let mut ab = MatrixData::augmented(&a, &b).unwrap();
    
    let gaussian = GaussianEliminationCalculatorConfig {
        matrix_name: format!("[{}|{}]", cfg.input_matrix_name, cfg.constant_column_name),
        row_name: String::from("r"),
        column_name: String::from("c"),
        pivot: cfg.pivot,
        reduce_upper: false,
        reduce_lower: true,
        make_identity: cfg.make_identity,
    };
    perform_gaussian_elimination(ctx, &gaussian, &mut ab).await?;

    let r = ab.get_submatrix(
        &IndexRange::All,
        &IndexRange::range(
            Index::abs(1)?,
            Index::abs(a.columns())?)?)?;
    let c = ab.get_submatrix(
        &IndexRange::All,
        &IndexRange::range(
            Index::abs(a.columns() + 1)?,
            Index::abs(a.columns() + 1)?)?)?;

    ctx.explanation.add_name_eq_matrix(&cfg.input_matrix_name, r.clone())?;
    ctx.explanation.add_name_eq_matrix(&cfg.constant_column_name, c.clone())?;
    
    let mut up_tri = SolveUpperTriangularConfig::default();
    up_tri.input_matrix_name = cfg.input_matrix_name.clone();
    up_tri.constant_column_name = cfg.constant_column_name.clone();
    up_tri.output_column_name = cfg.output_column_name.clone();
    let x = solve_upper_triangular(ctx, &up_tri, &r, &c).await?;

    ctx.explanation.add_name_eq_matrix(&cfg.output_column_name, x.clone())?;

    Ok(x)
}


pub struct SolveUpperTriangularConfig {
    /// name of upper triangular matrix
    pub input_matrix_name: String,

    /// name of constant column (one-column matrix)
    pub constant_column_name: String,

    /// name of output matrix
    pub output_column_name: String,
}

impl SolveUpperTriangularConfig {
    pub fn default() -> Self {
        Self {
            input_matrix_name: String::from("U"),
            constant_column_name: String::from("c"),
            output_column_name: String::from("x"),
        }
    }
}

/// r - upper triangular matrix
/// 
/// c - transformed additional column
pub async fn solve_upper_triangular(
    ctx: &mut CalcContext<'_>,
    cfg: &SolveUpperTriangularConfig,
    r: &MatrixData,
    c: &MatrixData,
) -> Result<MatrixData, CalcError> {
    if !r.is_square() {
        return Err(CalcError::from_string(format!(
            "Matrix {} is not square.",
            cfg.input_matrix_name
        )));
    }
    if !r.is_upper_triangular() {
        return Err(CalcError::from_string(format!(
            "Matrix {} is not upper triangular.",
            cfg.input_matrix_name
        )));
    }
    if r.rows() != c.rows() {
        return Err(CalcError::from_string(format!(
            "Matrices {} and {} have different number of rows.",
            cfg.input_matrix_name,
            cfg.constant_column_name
        )));
    }
    if c.columns() != 1 {
        return Err(CalcError::from_string(format!(
            "Matrix {} should have one column.",
            &cfg.constant_column_name
        )));
    }

    let n = r.rows();
    let mut x = MatrixData::zeros(ctx.rules().radix, n, 1)?;

    let mut symbols = SymbolTable::new();
    symbols.add_matrix_elements(ctx.rules().radix, &cfg.input_matrix_name, &r);
    symbols.add_matrix_column(ctx.rules().radix, &cfg.constant_column_name, c, 1)?;

    for i in c.iter_rows().rev() {
        // numerator: c(i) - R(i,k) * x(k)
        let mut num_expr = SymbolData::with_index_e(
            &cfg.constant_column_name,
            ctx.fa().rzu(i)?).to_rc_expr();
        for k in i+1..=n {
            let rik_xk_expr = BinaryData::mul(
                SymbolData::with_index_e_e(&cfg.input_matrix_name, ctx.fa().rzu(i)?, ctx.fa().rzu(k)?).to_rc_expr(),
                SymbolData::with_index_e(
                    &cfg.output_column_name,
                    ctx.fa().rzu(k)?).to_rc_expr()
            ).to_rc_expr();
            num_expr = BinaryData::sub(num_expr, rik_xk_expr).to_rc_expr();
        }
        // denominator
        let rii_expr = SymbolData::with_index_e_e(
            &cfg.input_matrix_name,
            ctx.fa().rzu(i)?,
            ctx.fa().rzu(i)?).to_rc_expr();
        
        // full expression
        let right_expr = FracData::new(num_expr, rii_expr).to_rc_expr();
        let xi_eq = EquationData::new(
            SymbolData::with_index_e(
                &cfg.output_column_name,
                ctx.fa().rzu(i)?).to_rc_expr(),
            right_expr.clone()
        ).to_rc_expr();
        ctx.explanation.add_expr(Rc::clone(&xi_eq))?;

        // calculate equation in many steps
        let subst_xi_eq = substitute_expression(xi_eq, &symbols).await;
        let eq = calculate_equation_statement(ctx, &subst_xi_eq).await?;
        x.set_i_i(i, 1, Rc::clone(&eq.right))?;
        symbols.add(
            SymbolData::with_index_e(&cfg.output_column_name, ctx.fa().rzu(i)?),
            eq.right);
    }
    Ok(x)
}

pub struct SolveCramerConfig {
    /// name of input matrix
    pub input_matrix_name: String,

    /// name of constant column (one-column matrix)
    pub constant_column_name: String,

    /// name of output matrix
    pub output_column_name: String,
}

impl SolveCramerConfig {
    pub fn default() -> Self {
        Self {
            input_matrix_name: String::from("A"),
            constant_column_name: String::from("b"),
            output_column_name: String::from("x"),
        }
    }
}


/// Solves equation system using Cramer's rule
///
/// Input:
///
///   a - square matrix
///
///   b - 1-column matrix
///
/// Returns:
///
///   x - solved variables
pub async fn solve_eq_system_using_cramer(
    ctx: &mut CalcContext<'_>,
    cfg: &SolveCramerConfig,
    a: &MatrixData,
    b: &MatrixData,
) -> Result<MatrixData, CalcError> {
    // Cramer's rule https://en.wikipedia.org/wiki/Cramer%27s_rule
    if !a.is_square() {
        return Err(CalcError::from_string(format!(
            "Matrix {} is not square.",
            cfg.input_matrix_name
        )));
    }
    if a.rows() != b.rows() {
        return Err(CalcError::from_string(format!(
            "Matrices {} and {} have different number of rows.",
            cfg.input_matrix_name,
            cfg.constant_column_name
        )));
    }

    let a_symbol = SymbolData::new(&cfg.input_matrix_name);
    for k in a.iter_columns() {
        let xk_symbol = SymbolData::with_index_e(&cfg.output_column_name, ctx.fa().rzu(k)?);
        let ak_symbol = SymbolData::with_index_e(&cfg.input_matrix_name, ctx.fa().rzu(k)?);
        let xk_expr = FracData::new(
            UnaryData::det(ak_symbol.to_rc_expr()).to_rc_expr(),
            UnaryData::det(a_symbol.clone().to_rc_expr()).to_rc_expr()
        ).to_rc_expr();
        ctx.explanation.add_symbol_eq_expr(xk_symbol, xk_expr)?;
    }

    ctx.explanation.add_statement(
        TextStatement::section("Calculating determinant of {matrix}")
        .and_expression("matrix", SymbolData::new(&cfg.input_matrix_name).to_rc_expr())
        .to_statement());

    let det_cfg = CalculateDeterminantConfig::new(SymbolData::new(&cfg.input_matrix_name));
    let det_a = calculate_determinant(ctx, &det_cfg, a).await?;
    ctx.explanation.add_expr_eq_expr(
        UnaryData::det(a_symbol.clone().to_rc_expr()).to_rc_expr(),
        Rc::clone(&det_a))?;

    let mut x = MatrixData::zeros(ctx.rules().radix, a.rows(), 1)?;
    for k in a.iter_columns() {
        let ak_symbol = SymbolData::with_index_e(&cfg.input_matrix_name, ctx.fa().rzu(k)?).to_rc_expr();
        ctx.explanation.add_statement(
            TextStatement::section("Calculating determinant of {matrix}")
            .and_expression("matrix", Rc::clone(&ak_symbol))
            .to_statement());

        let mut ak_mx = a.clone();
        //TODO add vector operations
        for row in a.iter_rows() {
            ak_mx.set_i_i(row, k, Rc::clone(b.get_i_i(row, 1)?))?;
        }
        ctx.explanation.add_expr_eq_expr(
            Rc::clone(&ak_symbol),
            ak_mx.clone().to_rc_expr())?;
        let det_cfg = CalculateDeterminantConfig::new(SymbolData::with_index_e(&cfg.input_matrix_name, ctx.fa().rzu(k)?));
        let det_ak = calculate_determinant(ctx, &det_cfg, &ak_mx).await?;
        ctx.explanation.add_expr_eq_expr(
            UnaryData::det(Rc::clone(&ak_symbol)).to_rc_expr(),
            Rc::clone(&det_ak))?;
        
        let xk_symbol = SymbolData::with_index_e(&cfg.output_column_name, ctx.fa().rzu(k)?);
        ctx.explanation.add_statement(
            TextStatement::section("calculating {xk}")
            .and_expression("xk", xk_symbol.clone().to_rc_expr())
            .to_statement());
        let ak_symbol = SymbolData::with_index_e(&cfg.input_matrix_name, ctx.fa().rzu(k)?);
        let xk_expr = FracData::new(
            UnaryData::det(ak_symbol.to_rc_expr()).to_rc_expr(),
            UnaryData::det(a_symbol.clone().to_rc_expr()).to_rc_expr()
        ).to_rc_expr();

        let xk_eq = EquationData::new(
            xk_symbol.clone().to_rc_expr(),
            xk_expr
        ).to_rc_expr();
        ctx.explanation.add_expr(Rc::clone(&xk_eq))?;

        //TODO use substitution
        let xk_expr = FracData::new(
            det_ak,
            Rc::clone(&det_a)
        ).to_rc_expr();
        let xk_eq = EquationData::new(
            xk_symbol.clone().to_rc_expr(),
            xk_expr
        ).to_rc_expr();
        ctx.explanation.add_expr(Rc::clone(&xk_eq))?;

        let eq = calculate_equation_statement(ctx, &xk_eq).await?;
        // save result
        x.set_i_i(k, 1, eq.right)?;
    }

    return Ok(x);
}