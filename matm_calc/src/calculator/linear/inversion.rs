use std::rc::Rc;

use matm_error::error::CalcError;
use matm_model::core::index::Index;
use matm_model::core::index_range::IndexRange;
use matm_model::expressions::binary::BinaryData;
use matm_model::expressions::equation::EquationData;
use matm_model::expressions::matrices::MatrixData;
use matm_model::expressions::object::GaussPivotType;
use matm_model::expressions::symbol::SymbolData;
use matm_model::expressions::unary::UnaryData;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;

use crate::calculator::calculate_equation_statement;
use crate::calculator::linear::det::calculate_adjugate_item;
use crate::calculator::linear::det::calculate_determinant;
use crate::calculator::linear::det::CalculateDeterminantConfig;
use crate::calculator::linear::gaussian::perform_gaussian_elimination;
use crate::calculator::linear::gaussian::GaussianEliminationCalculatorConfig;
use crate::calculator::context::CalcContext;
use crate::substitution::substitute_expression;
use crate::substitution::SymbolTable;

pub struct InvertMatrixUsingAdjugateConfig {
    pub matrix_name: String,
    pub minor_name: String,
}

impl InvertMatrixUsingAdjugateConfig {
    pub fn default() -> Self {
        Self {
            matrix_name: String::from("A"),
            minor_name: String::from("M"),
        }
    }
}


/// Inverts a matrix using det(A)^-1 * [D], where D is a matrix of minors
/// 
/// a - square matrix
/// 
/// returns inverted matrix a
pub async fn invert_matrix_using_adjugate(
    ctx: &mut CalcContext<'_>,
    cfg: &InvertMatrixUsingAdjugateConfig,
    a: &MatrixData,
) -> Result<MatrixData, CalcError> {
    if !a.is_square() {
        return Err(CalcError::from_string(format!(
            "Matrix {} is not square.",
            &cfg.matrix_name)));
    }

    // create matrix of minors (cofactor items)
    let mut m = MatrixData::zeros(ctx.rules().radix, a.rows(), a.columns())?;
    for idx in a.iter_right_down() {
        m.set(&idx,
              SymbolData::with_index_e_e(
                &cfg.minor_name,
                ctx.fa().rzu(idx.row)?,
                ctx.fa().rzu(idx.column)?
            ).to_rc_expr()
        )?;
    }

    // Main equation
    let main_right_expr = BinaryData::mul(
        ctx.fa().rfe(
            ctx.fa().ez1(),
            SymbolData::new(format!("det({})", &cfg.matrix_name).as_str()).to_expr() //TODO det as symbolic expression
        ),
        UnaryData::transpose(m.clone().to_rc_expr()).to_rc_expr()
    ).to_rc_expr();
    let main_eq = EquationData::new(
        UnaryData::inv(SymbolData::new(&cfg.matrix_name).to_rc_expr()).to_rc_expr(),
        main_right_expr.clone()
    ).to_rc_expr();
    ctx.explanation.add_expr(Rc::clone(&main_eq))?;

    let mut symbols = SymbolTable::new();

    //TODO use Unary::Determinant
    // det(A)
    let det_cfg = CalculateDeterminantConfig::new(SymbolData::new(&cfg.matrix_name));
    let det_a = calculate_determinant(ctx, &det_cfg, a).await?;
    //TODO substitute expression det(A) instead of "det(A)" symbol
    symbols.add(SymbolData::new(format!("det({})", &cfg.matrix_name).as_str()), Rc::clone(&det_a));

    // calculate matrix of minors (cofactor items)
    let cofactor_cfg = CalculateDeterminantConfig::new(SymbolData::new(&cfg.minor_name));
    let m_values = calculate_adjugate_matrix(ctx, &cofactor_cfg, a).await?;
    symbols.add_matrix_elements(ctx.rules().radix, &cfg.minor_name, &m_values);

    // repeat main equation
    ctx.explanation.add_expr(Rc::clone(&main_eq))?;

    // calculate
    let subst_main_eq = substitute_expression(Rc::clone(&main_eq), &symbols).await;
    let eq = calculate_equation_statement(ctx, &subst_main_eq).await?;
    if let Expression::Matrix(mx) = eq.right.as_ref() {
        return Ok(mx.as_ref().clone());
    }
    Err(CalcError::from_str("Cannot extract matrix from the result"))
}

async fn calculate_adjugate_matrix(
    ctx: &mut CalcContext<'_>,
    cofactor_cfg: &CalculateDeterminantConfig,
    a: &MatrixData)
    -> Result<MatrixData, CalcError> {
    let mut elements: Vec<Rc<Expression>> = Vec::new();
    for idx in a.iter_right_down() {
        let item = calculate_adjugate_item(ctx, &cofactor_cfg, &a, &idx).await?;
        elements.push(item);
    }
    return MatrixData::from_vector(
        a.rows(),
        a.columns(),
        elements);
}


pub struct InvertMatrixUsingGaussConfig {
    /// name of input matrix to be inverted
    pub matrix_name: String,
    /// name of constant column (one-column matrix)
    pub constant_column_name: String,
}

impl InvertMatrixUsingGaussConfig {
    pub fn default() -> Self {
        Self {
            matrix_name: String::from("A"),
            constant_column_name: String::from("b"),
        }
    }
}

/// Inverts a matrix using Gaussian elimination method
/// 
/// a - square matrix
/// 
/// returns inverted matrix a
pub async fn invert_matrix_using_gaussian_elimination(
    ctx: &mut CalcContext<'_>,
    cfg: &InvertMatrixUsingGaussConfig,
    a: &MatrixData,
) -> Result<MatrixData, CalcError> {
    if !a.is_square() {
        return Err(CalcError::from_string(format!(
            "Matrix {} is not square.",
            &cfg.matrix_name)));
    }
    let b = MatrixData::identity(ctx.rules().radix, a.rows())?;
    let mut ab = MatrixData::augmented(&a, &b).unwrap();

    let gauss_cfg = GaussianEliminationCalculatorConfig {
        matrix_name: format!("[{}|{}]", &cfg.matrix_name, &cfg.constant_column_name),
        row_name: String::from("r"),
        column_name: String::from("c"),
        pivot: GaussPivotType::IfZero,
        reduce_upper: true,
        reduce_lower: true,
        make_identity: true,
    };
    
    perform_gaussian_elimination(ctx, &gauss_cfg, &mut ab).await?;

    let inv_a = ab.get_submatrix(
        &IndexRange::All,
        &IndexRange::range(
            Index::abs(a.columns() + 1)?,
            Index::end())?)?;
    return Ok(inv_a);
}

