// Determinant algorithms

use async_recursion::async_recursion;
use std::rc::Rc;

use crate::calculator::calculate_equation_statement;
use crate::calculator::context::CalcContext;
use crate::substitution::substitute_expression;
use crate::substitution::SymbolTable;
use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use matm_model::expressions::binary::BinaryData;
use matm_model::expressions::equation::EquationData;
use matm_model::expressions::exponentiation::ExpData;
use matm_model::expressions::fractions::FracData;
use matm_model::expressions::iterated_binary::IteratedBinaryData;
use matm_model::expressions::matrices::MatrixData;
use matm_model::expressions::object::DetMethod;
use matm_model::expressions::symbol::SymbolData;
use matm_model::expressions::unary::UnaryData;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;
use matm_model::statements::text::TextStatement;
use matm_model::statements::ToStatement;


pub struct CalculateDeterminantConfig {
    pub matrix_name: SymbolData,
    /// name used by Laplace expansion
    pub minor_name: SymbolData,
    /// used method
    pub method: DetMethod,
}

impl CalculateDeterminantConfig {
    pub fn default() -> Self {
        Self {
            matrix_name: SymbolData::new("A"),
            minor_name: SymbolData::new("M"),
            method: DetMethod::Default,
        }
    }

    pub fn new(name: SymbolData) -> Self {
        Self {
            matrix_name: name,
            minor_name: SymbolData::new("M"),
            method: DetMethod::Default,
        }
    }

    pub fn with_minor(name: SymbolData, minor: SymbolData) -> Self {
        Self {
            matrix_name: name,
            minor_name: minor,
            method: DetMethod::Default,
        }
    }
}

#[async_recursion(?Send)]
pub async fn calculate_determinant(
    ctx: &mut CalcContext<'_>,
    cfg: &CalculateDeterminantConfig,
    m: &MatrixData,
) -> Result<Rc<Expression>, CalcError> {
    if !m.is_square() {
        return Err(CalcError::from_string(format!("Matrix {} is not square.", cfg.matrix_name)));
    }
    if m.is_zero() {
        return Ok(ctx.fa().rz0());
    }
    if m.is_identity() {
        return calculate_det_for_identity(ctx, cfg, m).await;
    }
    if m.is_upper_triangular() {
        let mut nested_ctx = CalcContext::new(
            ctx.rules,
            ctx.future_rules,
            ctx.explanation.nested()?);
        return calculate_det_for_upper_triangular(&mut nested_ctx, cfg, m).await;
    }
    if m.is_lower_triangular() {
        let mut nested_ctx = CalcContext::new(
            ctx.rules,
            ctx.future_rules,
            ctx.explanation.nested()?);
        return calculate_det_for_lower_triangular(&mut nested_ctx, cfg, m).await;
    }

    match m.rows() {
        0 => {
            panic!("Invalid matrix size: {}.", m.rows());
        }
        1 => {
            return Ok(Rc::clone(m.get_i_i(1, 1)?));
        }
        2 => {
            let mut nested_ctx = CalcContext::new(
                ctx.rules,
                ctx.future_rules,
                ctx.explanation.nested()?);
            return calculate_det_2(&mut nested_ctx, cfg, m).await;
        }
        rows => {
            let mut nested_ctx = CalcContext::new(
                ctx.rules,
                ctx.future_rules,
                ctx.explanation.nested()?);
            match cfg.method {
                DetMethod::Default => {
                    if rows > 3 {
                        return calculate_det_by_cofactor_expansion(&mut nested_ctx, cfg, m).await;
                    } else {
                        return calculate_det_by_sarrus(&mut nested_ctx, cfg, m).await;
                    }
                }
                DetMethod::CofactorExpansion => {
                    return calculate_det_by_cofactor_expansion(&mut nested_ctx, cfg, m).await;
                }
                DetMethod::Bareiss => {
                    return calculate_det_by_bareiss(&mut nested_ctx, cfg, m).await;
                }
            }
        }
    }
}

async fn calculate_det_for_identity(
    ctx: &mut CalcContext<'_>,
    _cfg: &CalculateDeterminantConfig,
    _a: &MatrixData,
) -> Result<Rc<Expression>, CalcError> {
    return Ok(ctx.fa().rz1());
}

async fn calculate_det_for_upper_triangular(
    ctx: &mut CalcContext<'_>,
    cfg: &CalculateDeterminantConfig,
    a: &MatrixData,
) -> Result<Rc<Expression>, CalcError> {
    // det = product of diagonal elements
    ctx.explanation.add_statement(
        TextStatement::section("calculating determinant of upper triangular matrix {matrix}")
        .and_expression("matrix", cfg.matrix_name.clone().to_rc_expr())
        .to_statement());
    calculate_det_for_diagonal_product(ctx, cfg, a).await
}

async fn calculate_det_for_lower_triangular(
    ctx: &mut CalcContext<'_>,
    cfg: &CalculateDeterminantConfig,
    a: &MatrixData,
) -> Result<Rc<Expression>, CalcError> {
    // det = product of diagonal elements
    ctx.explanation.add_statement(
        TextStatement::section("calculating determinant of lower triangular matrix {matrix}")
        .and_expression("matrix", cfg.matrix_name.clone().to_rc_expr())
        .to_statement());
    calculate_det_for_diagonal_product(ctx, cfg, a).await
}

async fn calculate_det_for_diagonal_product(
    ctx: &mut CalcContext<'_>,
    cfg: &CalculateDeterminantConfig,
    a: &MatrixData,
) -> Result<Rc<Expression>, CalcError> {
    //TODO change to symbol with nested indices: cfg.matrix_name.to_string()
    let det_expr = IteratedBinaryData::product(
        ctx.fa().sn("i"),
        ctx.fa().rz1(),
        ctx.fa().rzu(a.rows())?,
        SymbolData::with_index_s_s(&cfg.matrix_name.to_string(), "i", "i").to_rc_expr()
    ).to_rc_expr();

    let eq = EquationData::new(
        UnaryData::det(cfg.matrix_name.clone().to_rc_expr()).to_rc_expr(),
        det_expr
    ).to_rc_expr();

    // symbolic calculation
    let eq = calculate_equation_statement(ctx, &eq).await?;

    // arithmetic calculation
    let mut symbols = SymbolTable::new();
    symbols.add_matrix_elements(ctx.rules().radix, cfg.matrix_name.to_string().as_str(), &a);
    // symbols.dump_variables(explanation)?;
    let eq = substitute_expression(eq.to_rc_expr(), &symbols).await;
    let eq = calculate_equation_statement(ctx, &eq).await?;
    return Ok(eq.right);
}

async fn calculate_det_2(
    ctx: &mut CalcContext<'_>,
    cfg: &CalculateDeterminantConfig,
    a: &MatrixData,
) -> Result<Rc<Expression>, CalcError> {
    // use equation a11*a22 - a12*a21
    ctx.explanation.add_statement(
        TextStatement::section("calculating determinant of {matrix}")
        .and_expression("matrix", cfg.matrix_name.clone().to_rc_expr())
        .to_statement());

    // in plus
    let plus_expr = BinaryData::mul(
        SymbolData::with_index_e_e(cfg.matrix_name.to_string().as_str(), ctx.fa().rz1(), ctx.fa().rz1()).to_rc_expr(),
        SymbolData::with_index_e_e(cfg.matrix_name.to_string().as_str(), ctx.fa().rzi(2)?, ctx.fa().rzi(2)?).to_rc_expr(),
    ).to_rc_expr();

    // in minus
    let minus_expr = BinaryData::mul(
        SymbolData::with_index_e_e(cfg.matrix_name.to_string().as_str(), ctx.fa().rz1(), ctx.fa().rzi(2)?).to_rc_expr(),
        SymbolData::with_index_e_e(cfg.matrix_name.to_string().as_str(), ctx.fa().rzi(2)?, ctx.fa().rz1()).to_rc_expr(),
    ).to_rc_expr();

    let det_expr = BinaryData::sub(plus_expr, minus_expr).to_rc_expr();

    let det_eq = EquationData::new(
        UnaryData::det(cfg.matrix_name.clone().to_rc_expr()).to_rc_expr(),
        det_expr
    ).to_rc_expr();
    
    let mut symbols = SymbolTable::new();
    symbols.add_matrix_elements(ctx.rules().radix, cfg.matrix_name.to_string().as_str(), &a);

    let subst_det_eq = substitute_expression(Rc::clone(&det_eq), &symbols).await;
    ctx.explanation.add_expr(det_eq)?;

    let eq = calculate_equation_statement(ctx, &subst_det_eq).await?;
    return Ok(eq.right);
}

async fn calculate_det_by_sarrus(
    ctx: &mut CalcContext<'_>,
    cfg: &CalculateDeterminantConfig,
    m: &MatrixData,
) -> Result<Rc<Expression>, CalcError> {
    // use Sarrus rule
    ctx.explanation.add_statement(
        TextStatement::section("calculating determinant of {matrix} using Sarrus rule")
        .and_expression("matrix", cfg.matrix_name.clone().to_rc_expr())
        .to_statement());

    let mut det_expr: Option<Rc<Expression>> = None;

    // in plus - diagonals
    for base_col in m.iter_columns() {
        let mut diag_expr: Option<Rc<Expression>> = None;
        for offset in 0..m.columns() {
            let r: usize = 1 + offset;
            let c = m.align_column(base_col + offset)?;
            let factor = SymbolData::with_index_e_e(
                cfg.matrix_name.to_string().as_str(),
                ctx.fa().rzu(r)?,
                ctx.fa().rzu(c)?
            ).to_rc_expr();
            match diag_expr {
                Some(diag_val) => {
                    diag_expr = Some(BinaryData::mul(diag_val, factor).to_rc_expr());
                }
                None => {
                    diag_expr = Some(factor);
                }
            }
        }
        let diag_expr: Rc<Expression> = diag_expr.unwrap();
        match det_expr {
            Some(res_val) => {
                det_expr = Some(BinaryData::add(res_val, diag_expr).to_rc_expr());
            }
            None => {
                det_expr = Some(diag_expr);
            }
        }
    }

    let mut det_expr = det_expr.unwrap();

    // in minus - reverse diagonals
    for base_col in m.iter_columns().rev() {
        let mut diag_expr: Option<Rc<Expression>> = None;
        for offset in 0..m.columns() {
            let r: usize = 1 + offset;
            let c = m.align_column(base_col + m.columns() - offset)?;
            let factor = SymbolData::with_index_e_e(
                cfg.matrix_name.to_string().as_str(),
                ctx.fa().rzu(r)?,
                ctx.fa().rzu(c)?
            ).to_rc_expr();
            match diag_expr {
                Some(diag_val) => {
                    diag_expr = Some(BinaryData::mul(diag_val, factor).to_rc_expr());
                }
                None => {
                    diag_expr = Some(factor);
                }
            }
        }
        let diag_expr = diag_expr.unwrap();
        det_expr = BinaryData::sub(det_expr, diag_expr).to_rc_expr();
    }

    let det_eq = EquationData::new(
        UnaryData::det(cfg.matrix_name.clone().to_rc_expr()).to_rc_expr(),
        Rc::clone(&det_expr)
    ).to_rc_expr();

    let mut symbols = SymbolTable::new();
    symbols.add_matrix_elements(ctx.rules().radix, cfg.matrix_name.to_string().as_str(), &m);

    let subst_det_eq = substitute_expression(Rc::clone(&det_eq), &symbols).await;
    ctx.explanation.add_expr(det_eq)?;

    let eq = calculate_equation_statement(ctx, &subst_det_eq).await?;
    return Ok(eq.right);
}


#[derive(Debug)]
pub enum LaplaceExpansionDirection {
    ByRow(usize),
    ByColumn(usize),
}

/// determines the best expansion row or column
pub fn determine_laplace_expansion_direction(m: &MatrixData) -> LaplaceExpansionDirection {
    let max_row = m.find_row_with_max_zeros();
    let max_col = m.find_column_with_max_zeros();
    match (max_row, max_col) {
        (Some(r), Some(c)) => {
            if r.count >= c.count {
                LaplaceExpansionDirection::ByRow(r.index)
            } else {
                LaplaceExpansionDirection::ByColumn(c.index)
            }
        }
        (Some(r), None) => {
            LaplaceExpansionDirection::ByRow(r.index)
        }
        (None, Some(c)) => {
            LaplaceExpansionDirection::ByColumn(c.index)
        }
        _ => {
            // use first row
            LaplaceExpansionDirection::ByRow(1)
        }
    }
}

/// Laplace expansion
async fn calculate_det_by_cofactor_expansion(
    ctx: &mut CalcContext<'_>,
    cfg: &CalculateDeterminantConfig,
    m: &MatrixData,
) -> Result<Rc<Expression>, CalcError> {
    let mut nested_ctx = CalcContext::new(
        ctx.rules,
        ctx.future_rules,
        ctx.explanation.nested()?);
    let dir = determine_laplace_expansion_direction(m);
    calculate_det_by_cofactor_expansion_by_dir(&mut nested_ctx, cfg, m, &dir).await
}

/// Laplace's expansion by row/column
//TODO validate
pub async fn calculate_det_by_cofactor_expansion_by_dir(
    ctx: &mut CalcContext<'_>,
    cfg: &CalculateDeterminantConfig,
    m: &MatrixData,
    expansion_index: &LaplaceExpansionDirection,
) -> Result<Rc<Expression>, CalcError> {
    let section = match expansion_index {
        LaplaceExpansionDirection::ByRow(row) =>
            TextStatement::section("cofactor expansion by row {row}")
                .and_expression("row", ctx.fa().rzu(*row)?),
        LaplaceExpansionDirection::ByColumn(col) =>
            TextStatement::section("cofactor expansion by column {column}")
                .and_expression("column", ctx.fa().rzu(*col)?),
    };
    ctx.explanation.add_statement(section.to_statement());

    let exp_idx_value = match expansion_index {
        LaplaceExpansionDirection::ByRow(row) => *row,
        LaplaceExpansionDirection::ByColumn(col) => *col,
    };
    let mut it = match expansion_index {
        LaplaceExpansionDirection::ByRow(_) => m.iter_columns(),
        LaplaceExpansionDirection::ByColumn(_) => m.iter_rows(),
    };
    if let Some(col) = it.next() {
        let index = Index2D::new(exp_idx_value, col)?;
        //TODO change to symbolic formula
        let mut result: Rc<Expression> = calculate_cofactor_item(ctx, cfg, &m, &index).await?;
        while let Some(col) = it.next() {
            let index = Index2D::new(exp_idx_value, col)?;
            let item = calculate_cofactor_item(ctx, cfg, &m, &index).await?;
            result = BinaryData::add(result, item).to_rc_expr();
        }
        let eq = EquationData::new(
            UnaryData::det(cfg.matrix_name.clone().to_rc_expr()).to_rc_expr(),
            result,
        ).to_rc_expr();
        let calc_result = calculate_equation_statement(ctx, &eq).await?;
        return Ok(calc_result.right);
    } else {
        return Err(CalcError::from_str("Matrix does not have any element."));
    }
}

//TODO DRY similar to calculate_adjugate_item
pub async fn calculate_cofactor_item(
    ctx: &mut CalcContext<'_>,
    cfg: &CalculateDeterminantConfig,
    m: &MatrixData,
    index: &Index2D,
) -> Result<Rc<Expression>, CalcError> {
    let mrc = m.get(&index)?;
    if mrc.is_zero() {
        return Ok(ctx.fa().rz0());
    } else {
        //TODO change to symbolic expression
        let mut result = ExpData::new(
            ctx.fa().rzi(-1)?,
            BinaryData::add(
                ctx.fa().rzu(index.row)?,
                ctx.fa().rzu(index.column)?
            ).to_rc_expr()
        ).to_rc_expr();
        result = BinaryData::mul(result, Rc::clone(mrc)).to_rc_expr();

        let mii_symbol = SymbolData::with_index_e_e(
            &cfg.minor_name.to_string(),
            ctx.fa().rzu(index.row)?,
            ctx.fa().rzu(index.column)?
        );

        let par = TextStatement::paragraph("creating sub-matrix {submatrix} of {matrix} by removing row {row} and column {column}:")
            .and_expression("submatrix", mii_symbol.clone().to_rc_expr())
            .and_expression("matrix", cfg.matrix_name.clone().to_rc_expr())
            .and_expression("row", ctx.fa().rzu(index.row)?)
            .and_expression("column", ctx.fa().rzu(index.column)?)
            .to_statement();
        ctx.explanation.add_statement(par);

        // show how to create new matrix by removing row/column
        let submatrix_template = m.get_submatrix_with_crossed_lines(&index)?.to_rc_expr();
        ctx.explanation.add_symbol_eq_expr(
            mii_symbol.clone(),
            submatrix_template)?;

        let submatrix = m.get_submatrix_by_removing(&index)?;
        ctx.explanation.add_symbol_eq_expr(
            mii_symbol.clone(),
            submatrix.clone().to_rc_expr())?;

        let minor_calc = CalculateDeterminantConfig::with_minor(mii_symbol, cfg.minor_name.clone());
        let minor = calculate_determinant(ctx, &minor_calc, &submatrix).await?;
        return Ok(BinaryData::mul(result, minor).to_rc_expr());
    }
}

pub async fn calculate_adjugate_item(
    ctx: &mut CalcContext<'_>,
    cfg: &CalculateDeterminantConfig,
    m: &MatrixData,
    index: &Index2D,
) -> Result<Rc<Expression>, CalcError> {
    let mrc = m.get(&index)?;
    if mrc.is_zero() {
        return Ok(ctx.fa().rz0());
    } else {
        let mii_symbol = SymbolData::with_index_e_e(
            &cfg.minor_name.to_string(),
            ctx.fa().rzu(index.row)?,
            ctx.fa().rzu(index.column)?
        );

        let par = TextStatement::paragraph("creating sub-matrix {submatrix} of {matrix} by removing row {row} and column {column}:")
            .and_expression("submatrix", mii_symbol.clone().to_rc_expr())
            .and_expression("matrix", cfg.matrix_name.clone().to_rc_expr())
            .and_expression("row", ctx.fa().rzu(index.row)?)
            .and_expression("column", ctx.fa().rzu(index.column)?)
            .to_statement();
        ctx.explanation.add_statement(par);

        // show how to create new matrix by removing row/column
        let submatrix_template = m.get_submatrix_with_crossed_lines(&index)?.to_rc_expr();
        ctx.explanation.add_symbol_eq_expr(
            mii_symbol.clone(),
            submatrix_template)?;

        let submatrix = m.get_submatrix_by_removing(&index)?;
        ctx.explanation.add_symbol_eq_expr(
            mii_symbol.clone(),
            submatrix.clone().to_rc_expr())?;

        let minor_calc = CalculateDeterminantConfig::with_minor(mii_symbol, cfg.minor_name.clone());
        let minor = calculate_determinant(ctx, &minor_calc, &submatrix).await?;

        let result = Rc::new(ExpData::new(
            ctx.fa().rzi(-1)?,
            Rc::new(BinaryData::add(ctx.fa().rzu(index.row)?, ctx.fa().rzu(index.column)?).to_expr())
        ).to_expr());

        let result = BinaryData::mul(result, minor).to_rc_expr();
        return Ok(result);
    }
}

/// Bareiss-Montante algorithm
/// https://en.wikipedia.org/wiki/Bareiss_algorithm
/// Preconditions:
/// - matrix is square
/// - matrix size is 3 or more
async fn calculate_det_by_bareiss(
    ctx: &mut CalcContext<'_>,
    cfg: &CalculateDeterminantConfig,
    m: &MatrixData,
) -> Result<Rc<Expression>, CalcError> {
    ctx.explanation.add_statement(
        TextStatement::section("calculating determinant of {matrix} using Bareiss-Montante algorithm")
        .and_expression("matrix", cfg.matrix_name.clone().to_rc_expr())
        .to_statement());
    
    //TODO pivoting is not supported
    
    let n = m.rows();
    let mut mk = m.clone();

    // for k from 1 to n-1
    for k in 1..=n {
        let par = TextStatement::section("calculating for {k}")
            .and_equation("k", SymbolData::new("k").to_rc_expr(), ctx.fa().rzu(k)?)
            .to_statement();
        ctx.explanation.add_statement(par);

        let p_symbol = SymbolData::new("p");
        let aij = Rc::new(SymbolData::with_index_s_s(&cfg.matrix_name.to_string(), "i", "j").to_expr());
        let akk = Rc::new(SymbolData::with_index_s_s(&cfg.matrix_name.to_string(), "k", "k").to_expr());
        let aik = Rc::new(SymbolData::with_index_s_s(&cfg.matrix_name.to_string(), "i", "k").to_expr());
        let akj = Rc::new(SymbolData::with_index_s_s(&cfg.matrix_name.to_string(), "k", "j").to_expr());
        let aij_expr = Rc::new(FracData::new(
            Rc::new(BinaryData::sub(
                Rc::new(BinaryData::mul(aij, akk).to_expr()),
                Rc::new(BinaryData::mul(aik, akj).to_expr()),
            ).to_expr()),
            Rc::new(p_symbol.clone().to_expr()),
        ).to_expr());
        let aijp = Rc::new(SymbolData::with_index_s_s_and_postfix(&cfg.matrix_name.to_string(), "i", "j", "'").to_expr());

        let eq = EquationData::new(Rc::clone(&aijp), Rc::clone(&aij_expr)).to_rc_expr();
        ctx.explanation.add_expr(Rc::clone(&eq))?;

        let p: Rc<Expression> = if k > 1 {
            mk.get_i_i(k-1, k-1)?.clone()
        } else {
            Rc::new(ctx.fa().ez1())
        };

        let mut curr = mk.clone();
        for i in 1..=n {
            if i == k {
                continue;
            }
            for j in 1..=n {
                let par = TextStatement::paragraph("for {i}, {j}")
                    .and_equation("i", SymbolData::new("i").to_rc_expr(), ctx.fa().rzu(i)?)
                    .and_equation("j", SymbolData::new("j").to_rc_expr(), ctx.fa().rzu(j)?)
                    .to_statement();
                ctx.explanation.add_statement(par);

                let eq_ij = Rc::clone(&eq);
            
                // substitute i, j, k, p
                let mut symbols = SymbolTable::new();
                symbols.add(SymbolData::new("i"), ctx.fa().rzu(i)?);
                symbols.add(SymbolData::new("j"), ctx.fa().rzu(j)?);
                symbols.add(SymbolData::new("k"), ctx.fa().rzu(k)?);
                symbols.add(p_symbol.clone(), Rc::clone(&p));
                let eq_ij = substitute_expression(Rc::clone(&eq_ij), &symbols).await;
                ctx.explanation.add_expr(Rc::clone(&eq_ij))?;

                // substitute matrix elements
                symbols.add_matrix_elements(ctx.rules().radix, &cfg.matrix_name.to_string(), &mk);
                let eq_ij = substitute_expression(Rc::clone(&eq_ij), &symbols).await;
                ctx.explanation.add_expr(Rc::clone(&eq_ij))?;

                // calculate
                let eq_ij = calculate_equation_statement(ctx, &eq_ij).await?;
                ctx.explanation.add_expr(eq_ij.clone().to_rc_expr())?;

                curr.set_i_i(i, j, eq_ij.right)?;
            }
        }
        mk = curr;

        ctx.explanation.add_symbol_eq_expr(cfg.matrix_name.clone(), mk.clone().to_rc_expr())?;
    }

    let det = mk.get_i_i(n, n)?;
    return Ok(Rc::clone(&det));
}