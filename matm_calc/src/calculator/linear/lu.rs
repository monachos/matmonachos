use std::rc::Rc;

use matm_error::error::CalcError;
use matm_model::expressions::arithmetic_condition::ArithmeticCondition;
use matm_model::expressions::binary::BinaryData;
use matm_model::expressions::condition::ToCondition;
use matm_model::expressions::conditional::ConditionalClause;
use matm_model::expressions::conditional::ConditionalData;
use matm_model::expressions::equation::EquationData;
use matm_model::expressions::iterated_binary::IteratedBinaryData;
use matm_model::expressions::matrices::MatrixData;
use matm_model::expressions::symbol::SymbolData;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;
use matm_model::statements::text::TextStatement;
use matm_model::statements::ToStatement;

use crate::calculator::calculate_equation_statement;
use crate::calculator::calculate_expression;
use crate::calculator::context::CalcContext;
use crate::substitution::substitute_expression;
use crate::substitution::SymbolTable;

pub struct DecomposeLUConfig {
    pub input_matrix_name: String,
    pub l_matrix_name: String,
    pub u_matrix_name: String,
}

impl DecomposeLUConfig {
    pub fn default() -> Self {
        Self {
            input_matrix_name: String::from("A"),
            l_matrix_name: String::from("L"),
            u_matrix_name: String::from("U"),
        }
    }
}

pub async fn decompose_lu(
    ctx: &mut CalcContext<'_>,
    cfg: &DecomposeLUConfig,
    a: &MatrixData,
) -> Result<(MatrixData, MatrixData), CalcError> {
    if !a.is_square() {
        return Err(CalcError::from_string(format!("Matrix {} is not square.", cfg.input_matrix_name)));
    }

    let mut l = MatrixData::zeros(ctx.rules().radix, a.rows(), a.columns())?;
    let mut u = MatrixData::zeros(ctx.rules().radix, a.rows(), a.columns())?;

    let mut symbols = SymbolTable::new();
    symbols.add_matrix_elements(ctx.rules().radix, &cfg.input_matrix_name, &a);

    ctx.explanation.add_statement(
        TextStatement::section("LU decomposition of matrix {matrix}")
        .and_expression("matrix", SymbolData::new(&cfg.input_matrix_name).to_rc_expr())
        .to_statement());

    let uij_eq_symbol = build_uij_equation(ctx, cfg).await?;
    ctx.explanation.add_expr(Rc::clone(&uij_eq_symbol))?;

    let lij_eq_symbol = build_lij_equation(ctx, cfg).await?;
    ctx.explanation.add_expr(Rc::clone(&lij_eq_symbol))?;

    for iteration in a.iter_rows() {
        let par = TextStatement::paragraph("Calculate row {row} for {matrix}")
            .and_expression("matrix", SymbolData::new(&cfg.u_matrix_name).to_rc_expr())
            .and_expression("row", ctx.fa().rzu(iteration)?)
            .to_statement();
        ctx.explanation.add_statement(par);
        for j in a.iter_columns() {
            ctx.explanation.add_expr(Rc::clone(&uij_eq_symbol))?;

            symbols.add(ctx.fa().sn("i"), ctx.fa().rzu(iteration)?);
            symbols.add(ctx.fa().sn("j"), ctx.fa().rzu(j)?);

            let subst_eq = substitute_expression(Rc::clone(&uij_eq_symbol), &symbols).await;
            ctx.explanation.add_expr(Rc::clone(&subst_eq))?;

            // calculate equation in many steps
            let eq = calculate_expression(ctx, &subst_eq).await?;
            let subst_eq = substitute_expression(eq, &symbols).await;
            let eq = calculate_equation_statement(ctx, &subst_eq).await?;

            u.set_i_i(iteration, j, Rc::clone(&eq.right))?;
            let uij = SymbolData::with_index_e_e(
                &cfg.u_matrix_name,
                ctx.fa().rzu(iteration)?,
                ctx.fa().rzu(j)?);
            symbols.add(uij, eq.right);

            symbols.remove(ctx.fa().sn("i"));
            symbols.remove(ctx.fa().sn("j"));
        }

        let par = TextStatement::paragraph("Calculate column {column} for {matrix}")
            .and_expression("matrix", SymbolData::new(&cfg.l_matrix_name).to_rc_expr())
            .and_expression("column", ctx.fa().rzu(iteration)?)
            .to_statement();
        ctx.explanation.add_statement(par);
        for i in a.iter_rows() {
            ctx.explanation.add_expr(Rc::clone(&lij_eq_symbol))?;

            symbols.add(ctx.fa().sn("i"), ctx.fa().rzu(i)?);
            symbols.add(ctx.fa().sn("j"), ctx.fa().rzu(iteration)?);

            let subst_eq = substitute_expression(Rc::clone(&lij_eq_symbol), &symbols).await;
            ctx.explanation.add_expr(Rc::clone(&subst_eq))?;

            // calculate equation in many steps
            let eq = calculate_expression(ctx, &subst_eq).await?;
            let subst_eq = substitute_expression(eq, &symbols).await;
            let eq = calculate_equation_statement(ctx, &subst_eq).await?;

            l.set_i_i(i, iteration, Rc::clone(&eq.right))?;
            let lij = SymbolData::with_index_e_e(&cfg.l_matrix_name, ctx.fa().rzu(i)?, ctx.fa().rzu(iteration)?);
            symbols.add(lij, eq.right);

            symbols.remove(ctx.fa().sn("i"));
            symbols.remove(ctx.fa().sn("j"));
        }
    }

    return Ok((l, u));
}

async fn build_uij_equation(
    ctx: &CalcContext<'_>,
    cfg: &DecomposeLUConfig,
) -> Result<Rc<Expression>, CalcError> {
    let aij_symbol = SymbolData::with_index_e_e(&cfg.input_matrix_name, ctx.fa().rsn("i"), ctx.fa().rsn("j"));
    let lik_symbol = SymbolData::with_index_e_e(&cfg.l_matrix_name, ctx.fa().rsn("i"), ctx.fa().rsn("k"));
    let ukj_symbol = SymbolData::with_index_e_e(&cfg.u_matrix_name, ctx.fa().rsn("k"), ctx.fa().rsn("j"));
    let uij_symbol = SymbolData::with_index_e_e(&cfg.u_matrix_name, ctx.fa().rsn("i"), ctx.fa().rsn("j"));
    let uij_formula = ConditionalData::from_vector(vec![
        ConditionalClause {
            expression: BinaryData::sub(
                aij_symbol.clone().to_rc_expr(),
                IteratedBinaryData::sum(
                    ctx.fa().sn("k"),
                    ctx.fa().rzi(1)?,
                    BinaryData::sub(ctx.fa().rsn("i"), ctx.fa().rz1()).to_rc_expr(),
                    BinaryData::mul(lik_symbol.to_rc_expr(), ukj_symbol.to_rc_expr()).to_rc_expr()
                ).to_rc_expr()
            ).to_rc_expr(),
            condition: ArithmeticCondition::lte(
                ctx.fa().rsn("i"),
                ctx.fa().rsn("j")
            ).to_rc_cond()
        },
        ConditionalClause {
            expression: ctx.fa().rz0(),
            condition: ArithmeticCondition::gt(
                ctx.fa().rsn("i"),
                ctx.fa().rsn("j")
            ).to_rc_cond()
        }
    ]);
    return Ok(EquationData::new(
        uij_symbol.to_rc_expr(),
        uij_formula.to_rc_expr()
    ).to_rc_expr());
}


async fn build_lij_equation(
    ctx: &CalcContext<'_>,
    cfg: &DecomposeLUConfig,
) -> Result<Rc<Expression>, CalcError> {
    let aij_symbol = SymbolData::with_index_e_e(&cfg.input_matrix_name, ctx.fa().rsn("i"), ctx.fa().rsn("j"));
    let lik_symbol = SymbolData::with_index_e_e(&cfg.l_matrix_name, ctx.fa().rsn("i"), ctx.fa().rsn("k"));
    let ukj_symbol = SymbolData::with_index_e_e(&cfg.u_matrix_name, ctx.fa().rsn("k"), ctx.fa().rsn("j"));
    let ujj_symbol = SymbolData::with_index_e_e(&cfg.u_matrix_name, ctx.fa().rsn("j"), ctx.fa().rsn("j"));
    let lij_symbol = SymbolData::with_index_e_e(&cfg.l_matrix_name, ctx.fa().rsn("i"), ctx.fa().rsn("j"));
    let lij_formula = ConditionalData::from_vector(vec![
        ConditionalClause {
            expression: Rc::new(ctx.fa().efr(
                BinaryData::sub(
                    aij_symbol.to_rc_expr(),
                    IteratedBinaryData::sum(
                        ctx.fa().sn("k"),
                        ctx.fa().rzi(1)?,
                        BinaryData::sub(ctx.fa().rsn("j"), ctx.fa().rz1()).to_rc_expr(),
                        BinaryData::mul(lik_symbol.to_rc_expr(), ukj_symbol.to_rc_expr()).to_rc_expr()
                    ).to_rc_expr()
                ).to_rc_expr(),
                ujj_symbol.to_rc_expr()
            )),
            condition: ArithmeticCondition::gt(
                ctx.fa().rsn("i"),
                ctx.fa().rsn("j")
            ).to_rc_cond()
        },
        ConditionalClause {
            expression: ctx.fa().rz1(),
            condition: ArithmeticCondition::eq(
                ctx.fa().rsn("i"),
                ctx.fa().rsn("j")
            ).to_rc_cond()
        },
        ConditionalClause {
            expression: ctx.fa().rz0(),
            condition: ArithmeticCondition::lt(
                ctx.fa().rsn("i"),
                ctx.fa().rsn("j")
            ).to_rc_cond()
        }
    ]);
    return Ok(EquationData::new(
        lij_symbol.to_rc_expr(),
        lij_formula.to_rc_expr()
    ).to_rc_expr());
}