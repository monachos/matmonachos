#[cfg(test)]
mod tests {
    use crate::calculator::linear::lu::decompose_lu;
    use crate::calculator::linear::lu::DecomposeLUConfig;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::matrix;
    use crate::result::dump_unicode_color;
    use crate::result::ResultContainer;
    use futures::executor::block_on;
    use matm_model::expressions::matrices::MatrixData;
    use matm_model::testing::asserts::assert_result_error_that;
    use matm_model::testing::asserts::assert_result_ok;

    #[test]
    fn test_decompose_lu_22() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 2, 2;
            3, (-4),
            (3/2), (-1)
        ).unwrap();

        let cfg = DecomposeLUConfig {
            input_matrix_name: String::from("A"),
            l_matrix_name: String::from("L"),
            u_matrix_name: String::from("U"),
        };

        ctx.explanation.add_name_eq_matrix(&cfg.input_matrix_name, a.clone()).unwrap();

        let result = block_on(decompose_lu(&mut ctx, &cfg, &a));

        dump_unicode_color(&ctx.explanation);

        let (l, u) = assert_result_ok(result);

        let mut explanation = ResultContainer::new();
        explanation.add_name_eq_matrix(&cfg.l_matrix_name, l.clone()).unwrap();
        explanation.add_name_eq_matrix(&cfg.u_matrix_name, u.clone()).unwrap();
        println!();
        dump_unicode_color(&explanation);

        let exp_l = matrix!(ctx.fa(), 2, 2;
            1, 0,
            (1/2), 1
        ).unwrap();
        let exp_u = matrix!(ctx.fa(), 2, 2;
            3, (-4),
            0, 1
        ).unwrap();
        assert!(l == exp_l);
        assert!(u == exp_u);
    }

    #[test]
    fn test_decompose_lu_33() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            3, (-4), 4,
            (3/2), (-1), 2,
            (3/2), (-1/2), 0
        ).unwrap();

        let cfg = DecomposeLUConfig {
            input_matrix_name: String::from("A"),
            l_matrix_name: String::from("L"),
            u_matrix_name: String::from("U"),
        };

        ctx.explanation.add_name_eq_matrix(&cfg.input_matrix_name, a.clone()).unwrap();

        let result = block_on(decompose_lu(&mut ctx, &cfg, &a));

        dump_unicode_color(&ctx.explanation);

        let (l, u) = assert_result_ok(result);

        let mut explanation = ResultContainer::new();
        explanation.add_name_eq_matrix(&cfg.l_matrix_name, l.clone()).unwrap();
        explanation.add_name_eq_matrix(&cfg.u_matrix_name, u.clone()).unwrap();
        println!();
        dump_unicode_color(&explanation);

        let exp_l = matrix!(ctx.fa(), 3, 3;
            1, 0, 0,
            (1/2), 1, 0,
            (1/2), (3/2), 1
        ).unwrap();
        let exp_u = matrix!(ctx.fa(), 3, 3;
            3, (-4), 4,
            0, 1, 0,
            0, 0, (-2)
        ).unwrap();
        assert!(l == exp_l);
        assert!(u == exp_u);
    }

    #[test]
    fn test_decompose_lu_44() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 4, 4;
            3, (-4), 4, (-4),
            (3/2), (-1), 2, (-2),
            (3/2), (-1/2), 0, (-3),
            (9/2), (-11/2), 4, (-9)
        ).unwrap();

        let cfg = DecomposeLUConfig {
            input_matrix_name: String::from("A"),
            l_matrix_name: String::from("L"),
            u_matrix_name: String::from("U"),
        };

        ctx.explanation.add_name_eq_matrix(&cfg.input_matrix_name, a.clone()).unwrap();

        let result = block_on(decompose_lu(&mut ctx, &cfg, &a));

        dump_unicode_color(&ctx.explanation);

        let (l, u) = assert_result_ok(result);

        let mut explanation = ResultContainer::new();
        explanation.add_name_eq_matrix(&cfg.l_matrix_name, l.clone()).unwrap();
        explanation.add_name_eq_matrix(&cfg.u_matrix_name, u.clone()).unwrap();
        println!();
        dump_unicode_color(&explanation);

        let exp_l = matrix!(ctx.fa(), 4, 4;
            1, 0, 0, 0,
            (1/2), 1, 0, 0,
            (1/2), (3/2), 1, 0,
            (3/2), (1/2), 1, 1
        ).unwrap();
        let exp_u = matrix!(ctx.fa(), 4, 4;
            3, (-4), 4, (-4),
            0, 1, 0, 0,
            0, 0, (-2), (-1),
            0, 0, 0, (-2)
        ).unwrap();
        assert!(l == exp_l);
        assert!(u == exp_u);
    }

    #[test]
    fn test_decompose_lu_not_square() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = MatrixData::zeros(ctx.rules().radix, 4, 3).unwrap();
        let cfg = DecomposeLUConfig::default();

        let result = block_on(decompose_lu(&mut ctx, &cfg, &a));
        dump_unicode_color(&ctx.explanation);

        assert_result_error_that(result)
            .has_message(format!("Matrix {} is not square.", cfg.input_matrix_name).as_str());
    }
}