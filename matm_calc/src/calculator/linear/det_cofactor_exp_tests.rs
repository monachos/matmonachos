#[cfg(test)]
mod tests {
    use crate::calculator::linear::det::calculate_determinant;
    use crate::calculator::linear::det::determine_laplace_expansion_direction;
    use crate::calculator::linear::det::CalculateDeterminantConfig;
    use crate::calculator::linear::det::LaplaceExpansionDirection;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::matrix;
    use crate::result::dump_unicode_color;
    use assert_matches::assert_matches;
    use futures::executor::block_on;
    use matm_model::expressions::object::DetMethod;
    use matm_model::testing::asserts::assert_expression_that;
    use matm_model::testing::asserts::assert_result_ok;


    /// Laplace expansion for matrix 3x3
    #[test]
    fn test_calculate_determinant_3_by_cofactor_exp() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let a = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            4, 1, 6,
            1, 3, 3
        ).unwrap();
        let mut cfg = CalculateDeterminantConfig::default();
        cfg.method = DetMethod::CofactorExpansion;
        ctx.explanation.add_symbol_eq_matrix(cfg.matrix_name.clone(), a.clone()).unwrap();

        let result = block_on(calculate_determinant(&mut ctx, &cfg, &a));

        dump_unicode_color(&ctx.explanation);

        let d = assert_result_ok(result);
        assert_expression_that(&d)
            .as_integer()
            .is_int(6);
    }

    /// Laplace expansion by first row is used
    #[test]
    fn test_calculate_determinant_4_no_zeros() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let a = matrix!(ctx.fa(), 4, 4;
            1, 1, 3, 5,
            2, (-1), (-2), 1,
            1, 1, 1, 2,
            (-2), 1, 4, (-1)
        ).unwrap();
        let cfg = CalculateDeterminantConfig::default();
        ctx.explanation.add_symbol_eq_matrix(cfg.matrix_name.clone(), a.clone()).unwrap();

        assert_matches!(determine_laplace_expansion_direction(&a), LaplaceExpansionDirection::ByRow(1));

        let result = block_on(calculate_determinant(&mut ctx, &cfg, &a));

        dump_unicode_color(&ctx.explanation);

        let d = assert_result_ok(result);
        assert_expression_that(&d)
            .as_integer()
            .is_int(-18);
    }

    /// Laplace expansion by 2nd row with 3 zeros
    #[test]
    fn test_calculate_determinant_4_by_r2() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let a = matrix!(ctx.fa(), 4, 4;
            1, 1, 3, 5,
            2, 0, 0, 0,
            1, 1, 1, 2,
            (-2), 1, 4, (-1)
        ).unwrap();
        let cfg = CalculateDeterminantConfig::default();
        ctx.explanation.add_symbol_eq_matrix(cfg.matrix_name.clone(), a.clone()).unwrap();

        assert_matches!(determine_laplace_expansion_direction(&a), LaplaceExpansionDirection::ByRow(2));

        let result = block_on(calculate_determinant(&mut ctx, &cfg, &a));

        dump_unicode_color(&ctx.explanation);

        let d = assert_result_ok(result);
        assert_expression_that(&d)
            .as_integer()
            .is_int(-30);
    }

    /// Laplace expansion by 3rd row with 3 zeros
    #[test]
    fn test_calculate_determinant_4_by_r3() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let a = matrix!(ctx.fa(), 4, 4;
            1, 1, 3, 5,
            1, 1, 1, 2,
            2, 0, 0, 0,
            (-2), 1, 4, (-1)
        ).unwrap();
        let cfg = CalculateDeterminantConfig::default();
        ctx.explanation.add_symbol_eq_matrix(cfg.matrix_name.clone(), a.clone()).unwrap();

        assert_matches!(determine_laplace_expansion_direction(&a), LaplaceExpansionDirection::ByRow(3));

        let result = block_on(calculate_determinant(&mut ctx, &cfg, &a));

        dump_unicode_color(&ctx.explanation);

        let d = assert_result_ok(result);
        assert_expression_that(&d)
            .as_integer()
            .is_int(30);
    }

    /// Laplace expansion by 1st column with 2 zeros
    #[test]
    fn test_calculate_determinant_4_by_c1() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let a = matrix!(ctx.fa(), 4, 4;
            1, 1, 3, 5,
            0, 1, 2, 1,
            0, 1, 1, 2,
            (-2), 1, 4, (-1)
        ).unwrap();
        let cfg = CalculateDeterminantConfig::default();
        ctx.explanation.add_symbol_eq_matrix(cfg.matrix_name.clone(), a.clone()).unwrap();

        assert_matches!(determine_laplace_expansion_direction(&a), LaplaceExpansionDirection::ByColumn(1));

        let result = block_on(calculate_determinant(&mut ctx, &cfg, &a));

        dump_unicode_color(&ctx.explanation);

        let d = assert_result_ok(result);
        assert_expression_that(&d)
            .as_integer()
            .is_int(-10);
    }

    /// Laplace expansion by 4th column with 2 zeros
    #[test]
    fn test_calculate_determinant_4_by_c4() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let a = matrix!(ctx.fa(), 4, 4;
            1, 3, 5, 1,
            1, 2, 1, 0,
            1, 1, 2, 0,
            1, 4, (-1), (-2)
        ).unwrap();
        let cfg = CalculateDeterminantConfig::default();
        ctx.explanation.add_symbol_eq_matrix(cfg.matrix_name.clone(), a.clone()).unwrap();

        assert_matches!(determine_laplace_expansion_direction(&a), LaplaceExpansionDirection::ByColumn(4));

        let result = block_on(calculate_determinant(&mut ctx, &cfg, &a));

        dump_unicode_color(&ctx.explanation);

        let d = assert_result_ok(result);
        assert_expression_that(&d)
            .as_integer()
            .is_int(10);
    }

    #[test]
    fn test_calculate_determinant_4_many_zeros_r3() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let a = matrix!(ctx.fa(), 4, 4;
            1, 0, 3, 5,
            0, (-1), (-2), 1,
            0, 0, 0, 2,
            0, 1, 4, (-1)
        ).unwrap();
        let cfg = CalculateDeterminantConfig::default();
        ctx.explanation.add_symbol_eq_matrix(cfg.matrix_name.clone(), a.clone()).unwrap();

        assert_matches!(determine_laplace_expansion_direction(&a), LaplaceExpansionDirection::ByRow(3));

        let result = block_on(calculate_determinant(&mut ctx, &cfg, &a));

        dump_unicode_color(&ctx.explanation);

        let d = assert_result_ok(result);
        assert_expression_that(&d)
            .as_integer()
            .is_int(4);
    }
}
