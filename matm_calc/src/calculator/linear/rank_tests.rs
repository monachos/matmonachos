#[cfg(test)]
mod tests {
    use crate::calculator::linear::rank::calculate_rank;
    use crate::calculator::linear::rank::CalculateRankConfig;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::matrix;
    use crate::result::dump_unicode_color;
    use futures::executor::block_on;
    use matm_model::testing::asserts::assert_result_ok;

    // Example from "Algebra liniowa w zadaniach", Jerzy Rutkowski, PWN 2008, page 97
    #[test]
    fn test_rank() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 4, 5;
            1, 2, 3, 4, 1,
            2, 5, 5, 7, 2,
            4, 6, 8, 9, 3,
            3, 5, 4, 4, 2
        ).unwrap();
        ctx.explanation.add_name_eq_matrix("A", a.clone()).unwrap();

        let cfg = CalculateRankConfig {
            matrix_name: String::from("A"),
        };

        let result = block_on(calculate_rank(&mut ctx, &cfg, &a));
        dump_unicode_color(&ctx.explanation);

        let rank = assert_result_ok(result);
        assert_eq!(3, rank);
    }
}
