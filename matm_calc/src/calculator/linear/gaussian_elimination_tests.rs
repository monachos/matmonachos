#[cfg(test)]
mod tests {
    use crate::calculator::linear::gaussian::perform_gaussian_elimination;
    use crate::calculator::linear::gaussian::GaussianEliminationCalculatorConfig;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::matrix;
    use crate::result::dump_unicode_color;
    use futures::executor::block_on;
    use matm_model::expressions::matrices::MatrixData;
    use matm_model::expressions::object::GaussPivotType;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;
    use matm_model::testing::asserts::assert_result_ok;

    fn run_gaussian_elimination_ok(
        ctx: &mut CalcContext<'_>,
        config: &GaussianEliminationCalculatorConfig,
        mx: &MatrixData,
    ) -> MatrixData {
        let mut mut_mx = mx.clone();
        ctx.explanation.add_name_eq_matrix(&config.matrix_name, mx.clone()).unwrap();

        let result = block_on(perform_gaussian_elimination(ctx, config, &mut mut_mx));
        dump_unicode_color(&ctx.explanation);
        println!();
        assert_result_ok(result);

        mut_mx
    }

    #[test]
    fn test_gaussian_by_rows_45() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let mut ab = matrix!(ctx.fa(), 4, 5;
            2, 0, 1, 0, 5,
            0, (-3), (-1), (-1), 1,
            2, 0, 5, 3, 12,
            3, (-3/2), 5, (9/2), 17
        ).unwrap();

        let mut config = GaussianEliminationCalculatorConfig::default();
        config.matrix_name = String::from("[A|b]");
        config.reduce_lower = true;
        config.pivot = GaussPivotType::IfLessThanMaxAbs;
        let result = run_gaussian_elimination_ok(&mut ctx, &config, &mut ab);

        let expected = matrix!(ctx.fa(), 4, 5;
            3, (-3/2), 5, (9/2), 17,
            0, 1, (5/3), 0, (2/3),
            0, 0, 4, (-1), 3,
            0, 0, 0, (-4), (-4)
            ).unwrap();
        assert_expression_that(&result.to_expr())
            .as_matrix()
            .is_equal_to(expected);
    }

    #[test]
    fn test_gaussian_by_rows_reduce_lower() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let mut a = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            2, 8, 2,
            3, 4, 1
        ).unwrap();

        let mut config = GaussianEliminationCalculatorConfig::default();
        config.reduce_lower = true;
        let result = run_gaussian_elimination_ok(&mut ctx, &config, &mut a);

        let expected = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            0, 4, (-4),
            0, 0, (-10)
            ).unwrap();
        assert_expression_that(&result.to_expr())
            .as_matrix()
            .is_equal_to(expected);
    }

    #[test]
    fn test_gaussian_by_rows_reduce_upper() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let mut a = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            2, 4, 2,
            3, 4, 1
        ).unwrap();

        let mut config = GaussianEliminationCalculatorConfig::default();
        config.reduce_upper = true;
        let result = run_gaussian_elimination_ok(&mut ctx, &config, &mut a);

        let expected = matrix!(ctx.fa(), 3, 3;
            2, 0, 0,
            (-4), (-4), 0,
            3, 4, 1
            ).unwrap();
        assert_expression_that(&result.to_expr())
            .as_matrix()
            .is_equal_to(expected);
    }

    #[test]
    fn test_gaussian_by_rows_reduce_lower_upper() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let mut a = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            2, 8, 2,
            3, 4, 1
        ).unwrap();

        let mut config = GaussianEliminationCalculatorConfig::default();
        config.reduce_lower = true;
        config.reduce_upper = true;
        let result = run_gaussian_elimination_ok(&mut ctx, &config, &mut a);

        let expected = matrix!(ctx.fa(), 3, 3;
            1, 0, 0,
            0, 4, 0,
            0, 0, (-10)
            ).unwrap();
        assert_expression_that(&result.to_expr())
            .as_matrix()
            .is_equal_to(expected);
    }

    #[test]
    fn test_gaussian_by_rows_reduce_lower_identity() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let mut a = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            2, 8, 2,
            3, 4, 1
        ).unwrap();

        let mut config = GaussianEliminationCalculatorConfig::default();
        config.reduce_lower = true;
        config.make_identity = true;
        let result = run_gaussian_elimination_ok(&mut ctx, &config, &mut a);

        let expected = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            0, 1, (-1),
            0, 0, 1
            ).unwrap();
        assert_expression_that(&result.to_expr())
            .as_matrix()
            .is_equal_to(expected);
    }

    #[test]
    fn test_gaussian_by_rows_reduce_lower_pivot_less() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let mut a = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            2, 8, 2,
            3, 4, 1
        ).unwrap();

        let mut config = GaussianEliminationCalculatorConfig::default();
        config.reduce_lower = true;
        config.pivot = GaussPivotType::IfLessThanMaxAbs;
        let result = run_gaussian_elimination_ok(&mut ctx, &config, &mut a);

        let expected = matrix!(ctx.fa(), 3, 3;
            3, 4, 1,
            0, (16/3), (4/3),
            0, 0, (5/2)
            ).unwrap();
        assert_expression_that(&result.to_expr())
            .as_matrix()
            .is_equal_to(expected);
    }

    #[test]
    fn test_gaussian_by_rows_reduce_lower_pivot_zero() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let mut a = matrix!(ctx.fa(), 3, 3;
            0, 2, 3,
            2, 8, 2,
            3, 4, 1
        ).unwrap();

        let mut config = GaussianEliminationCalculatorConfig::default();
        config.reduce_lower = true;
        config.pivot = GaussPivotType::IfZero;
        let result = run_gaussian_elimination_ok(&mut ctx, &config, &mut a);

        let expected = matrix!(ctx.fa(), 3, 3;
            3, 4, 1,
            0, (16/3), (4/3),
            0, 0, (5/2)
            ).unwrap();
        assert_expression_that(&result.to_expr())
            .as_matrix()
            .is_equal_to(expected);
    }

    /// only pivoting without any elimination
    #[test]
    fn test_gaussian_by_rows_pivot_less() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let mut a = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            2, 8, 2,
            3, 4, 1
        ).unwrap();

        let mut config = GaussianEliminationCalculatorConfig::default();
        config.pivot = GaussPivotType::IfLessThanMaxAbs;
        let result = run_gaussian_elimination_ok(&mut ctx, &config, &mut a);

        let expected = matrix!(ctx.fa(), 3, 3;
            3, 4, 1,
            2, 8, 2,
            1, 2, 3
            ).unwrap();
        assert_expression_that(&result.to_expr())
            .as_matrix()
            .is_equal_to(expected);
    }

    /// only pivoting without any elimination
    #[test]
    fn test_gaussian_by_rows_pivot_zero() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let mut a = matrix!(ctx.fa(), 3, 3;
            0, 2, 3,
            2, 8, 2,
            3, 4, 1
        ).unwrap();

        let mut config = GaussianEliminationCalculatorConfig::default();
        config.pivot = GaussPivotType::IfZero;
        let result = run_gaussian_elimination_ok(&mut ctx, &config, &mut a);

        let expected = matrix!(ctx.fa(), 3, 3;
            3, 4, 1,
            2, 8, 2,
            0, 2, 3
            ).unwrap();
        assert_expression_that(&result.to_expr())
            .as_matrix()
            .is_equal_to(expected);
    }

    /// only making identity without any elimination
    #[test]
    fn test_gaussian_by_rows_identity() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let mut a = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            2, 8, 2,
            3, 4, 1
        ).unwrap();

        let mut config = GaussianEliminationCalculatorConfig::default();
        config.make_identity = true;
        let result = run_gaussian_elimination_ok(&mut ctx, &config, &mut a);

        let expected = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            (1/4), 1, (1/4),
            3, 4, 1
            ).unwrap();
        assert_expression_that(&result.to_expr())
            .as_matrix()
            .is_equal_to(expected);
    }
}