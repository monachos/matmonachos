#[cfg(test)]
mod tests {
    use crate::calculator::linear::det::calculate_determinant;
    use crate::calculator::linear::det::CalculateDeterminantConfig;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::matrix;
    use crate::result::dump_unicode_color;
    use futures::executor::block_on;
    use matm_model::expressions::object::DetMethod;
    use matm_model::testing::asserts::assert_expression_that;
    use matm_model::testing::asserts::assert_result_ok;

    /// Bareiss-Montante algorithm for matrix 3x3
    #[test]
    fn test_calculate_determinant_3_by_bareiss() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let a = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            4, 1, 6,
            1, 3, 3
        ).unwrap();
        let mut cfg = CalculateDeterminantConfig::default();
        cfg.method = DetMethod::Bareiss;
        ctx.explanation.add_symbol_eq_matrix(cfg.matrix_name.clone(), a.clone()).unwrap();

        let result = block_on(calculate_determinant(&mut ctx, &cfg, &a));

        dump_unicode_color(&ctx.explanation);

        let d = assert_result_ok(result);
        assert_expression_that(&d)
            .as_integer()
            .is_int(6);
    }

}
