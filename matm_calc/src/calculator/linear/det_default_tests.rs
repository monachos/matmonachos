#[cfg(test)]
mod tests {
    use futures::executor::block_on;
    use std::rc::Rc;

    use crate::calculator::linear::det::calculate_determinant;
    use crate::calculator::linear::det::CalculateDeterminantConfig;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::matrix;
    use crate::result::dump_unicode_color;
    use matm_model::expressions::matrices::MatrixData;
    use matm_model::expressions::Expression;
    use matm_model::testing::asserts::assert_expression_that;
    use matm_model::testing::asserts::assert_result_error_that;
    use matm_model::testing::asserts::assert_result_ok;

    #[test]
    fn test_calculate_determinant_not_square() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let a = MatrixData::zeros(ctx.rules().radix, 3, 2).unwrap();
        let cfg = CalculateDeterminantConfig::default();
        
        let result = block_on(calculate_determinant(&mut ctx, &cfg, &a));

        dump_unicode_color(&ctx.explanation);

        assert_result_error_that(result)
            .has_message(format!("Matrix {} is not square.", cfg.matrix_name).as_str());
    }

    #[test]
    fn test_calculate_determinant_zeros() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = MatrixData::zeros(ctx.rules().radix, 6, 6).unwrap();
        
        let cfg = CalculateDeterminantConfig::default();
        ctx.explanation.add_name_eq_matrix(&cfg.matrix_name.to_string(), a.clone()).unwrap();
        
        let result = block_on(calculate_determinant(&mut ctx, &cfg, &a));

        dump_unicode_color(&ctx.explanation);
        
        let d: Rc<Expression> = assert_result_ok(result);
        assert_eq!(true, d.is_zero());
    }

    #[test]
    fn test_calculate_determinant_identity() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = MatrixData::identity(ctx.rules().radix, 6).unwrap();
        let cfg = CalculateDeterminantConfig::default();
        ctx.explanation.add_symbol_eq_matrix(cfg.matrix_name.clone(), a.clone()).unwrap();
        
        let result = block_on(calculate_determinant(&mut ctx, &cfg, &a));

        dump_unicode_color(&ctx.explanation);

        let d = assert_result_ok(result);
        assert_eq!(true, d.is_one());
    }

    #[test]
    fn test_calculate_determinant_scalar() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = MatrixData::scalar(ctx.fa().rzi_u(5));
        let cfg = CalculateDeterminantConfig::default();
        ctx.explanation.add_symbol_eq_matrix(cfg.matrix_name.clone(), a.clone()).unwrap();

        let result = block_on(calculate_determinant(&mut ctx, &cfg, &a));

        dump_unicode_color(&ctx.explanation);

        let d = assert_result_ok(result);
        assert_expression_that(&d)
            .as_integer()
            .is_int(5);
    }

    #[test]
    fn test_calculate_determinant_2() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 2, 2;
            1, 2,
            4, 1
        ).unwrap();
        let cfg = CalculateDeterminantConfig::default();
        ctx.explanation.add_symbol_eq_matrix(cfg.matrix_name.clone(), a.clone()).unwrap();

        let result = block_on(calculate_determinant(&mut ctx, &cfg, &a));

        dump_unicode_color(&ctx.explanation);

        let d = assert_result_ok(result);
        assert_expression_that(&d)
            .as_integer()
            .is_int(-7);
    }

    #[test]
    fn test_calculate_determinant_upper_triangular() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            0, 1, 6,
            0, 0, 3
        ).unwrap();
        let cfg = CalculateDeterminantConfig::default();
        ctx.explanation.add_symbol_eq_matrix(cfg.matrix_name.clone(), a.clone()).unwrap();

        let result = block_on(calculate_determinant(&mut ctx, &cfg, &a));

        dump_unicode_color(&ctx.explanation);

        let d = assert_result_ok(result);
        assert_expression_that(&d)
            .as_integer()
            .is_int(3);
    }

    #[test]
    fn test_calculate_determinant_lower_triangular() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            1, 0, 0,
            2, 1, 0,
            3, 6, 3
        ).unwrap();
        let cfg = CalculateDeterminantConfig::default();
        ctx.explanation.add_symbol_eq_matrix(cfg.matrix_name.clone(), a.clone()).unwrap();

        let result = block_on(calculate_determinant(&mut ctx, &cfg, &a));

        dump_unicode_color(&ctx.explanation);

        let d = assert_result_ok(result);
        assert_expression_that(&d)
            .as_integer()
            .is_int(3);
    }

    /// By default Sarrus rule is used
    #[test]
    fn test_calculate_determinant_3() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            4, 1, 6,
            1, 3, 3
        ).unwrap();
        let cfg = CalculateDeterminantConfig::default();
        ctx.explanation.add_symbol_eq_matrix(cfg.matrix_name.clone(), a.clone()).unwrap();

        let result = block_on(calculate_determinant(&mut ctx, &cfg, &a));

        dump_unicode_color(&ctx.explanation);

        let d = assert_result_ok(result);
        assert_expression_that(&d)
            .as_integer()
            .is_int(6);
    }

    /// By default Sarrus rule is used
    #[test]
    fn test_calculate_determinant_3_fraq() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            (1/2), (2/3), (3/7),
            4, (1/3), 6,
            1, 3, 3
        ).unwrap();
        let cfg = CalculateDeterminantConfig::default();
        ctx.explanation.add_symbol_eq_matrix(cfg.matrix_name.clone(), a.clone()).unwrap();

        let result = block_on(calculate_determinant(&mut ctx, &cfg, &a));

        dump_unicode_color(&ctx.explanation);

        let det = assert_result_ok(result);
        assert_expression_that(&det)
            .as_fraction()
            .is_ints(-15, 2);
    }

}