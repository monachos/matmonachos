use std::rc::Rc;

use matm_error::error::CalcError;
use matm_model::expressions::binary::BinaryData;
use matm_model::expressions::equation::EquationData;
use matm_model::expressions::exponentiation::ExpData;
use matm_model::expressions::fractions::FracData;
use matm_model::expressions::matrices::MatrixData;
use matm_model::expressions::root::RootData;
use matm_model::expressions::symbol::SymbolData;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;
use matm_model::statements::text::TextStatement;
use matm_model::statements::ToStatement;

use crate::calculator::matrices::is_positive_definite_matrix;
use crate::calculator::calculate_equation_statement;
use crate::calculator::context::CalcContext;
use crate::substitution::substitute_expression;
use crate::substitution::SymbolTable;

pub struct DecomposeCholeskyConfig {
    pub input_matrix_name: String,
    pub l_matrix_name: String,
}

impl DecomposeCholeskyConfig {
    pub fn default() -> Self {
        Self {
            input_matrix_name: String::from("A"),
            l_matrix_name: String::from("L"),
        }
    }
}

/// Cholesky-Banachiewicz decomposition
/// https://pl.wikipedia.org/wiki/Rozk%C5%82ad_Choleskiego
/// https://en.wikipedia.org/wiki/Cholesky_decomposition
pub async fn decompose_cholesky(
    ctx: &mut CalcContext<'_>,
    cfg: &DecomposeCholeskyConfig,
    a: &MatrixData,
) -> Result<MatrixData, CalcError> {
    if !a.is_square() {
        return Err(CalcError::from_string(format!("Matrix {} is not square.", cfg.input_matrix_name)));
    }
    if !a.is_symmetric() {
        return Err(CalcError::from_string(format!("Matrix {} is not symmetric.", cfg.input_matrix_name)));
    }
    match is_positive_definite_matrix(ctx, a).await? {
        Some(result) => {
            if !result {
                return Err(CalcError::from_string(format!("Matrix {} is not positive-definite.", cfg.input_matrix_name)));
            }
        }
        None => {}
    }

    let mut l = MatrixData::zeros(ctx.rules().radix, a.rows(), a.columns())?;

    let mut symbols = SymbolTable::new();
    symbols.add_matrix_elements(ctx.rules().radix, &cfg.input_matrix_name, &a);

    ctx.explanation.add_statement(
        TextStatement::section("Cholesky decomposition of matrix {matrix}")
        .and_expression("matrix", SymbolData::new(&cfg.input_matrix_name).to_rc_expr())
        .to_statement());
    
    for idx in a.iter_down_right_lower_tri() {
        // calculate equation
        let element = if idx.row == idx.column {
            let lkk = SymbolData::with_index_e_e(
                &cfg.l_matrix_name,
                ctx.fa().rzu(idx.row)?,
                ctx.fa().rzu(idx.column)?);
            let expr = create_expression_for_lkk(ctx, cfg, idx.row).await?;
            Some((lkk, expr))
        } else if idx.row > idx.column {
            let lik = SymbolData::with_index_e_e(
                &cfg.l_matrix_name,
                ctx.fa().rzu(idx.row)?,
                ctx.fa().rzu(idx.column)?);
            let expr = create_expression_for_lik(ctx, cfg, idx.row, idx.column).await?;
            Some((lik, expr))
        } else {
            None
        };

        if let Some((el_sym, el_expr)) = element {
            // write equation
            let el_eq = EquationData::new(
                el_sym.clone().to_rc_expr(),
                el_expr
            ).to_rc_expr();
            ctx.explanation.add_expr(Rc::clone(&el_eq))?;

            let subst_el_eq = substitute_expression(el_eq, &symbols).await;
            let eq = calculate_equation_statement(ctx, &subst_el_eq).await?;
            l.set(&idx, Rc::clone(&eq.right))?;
            symbols.add(el_sym, eq.right);
        }
    }

    return Ok(l);
}

async fn create_expression_for_lkk(
    ctx: &mut CalcContext<'_>,
    cfg: &DecomposeCholeskyConfig,
    k: usize,
) -> Result<Rc<Expression>, CalcError> {
    let akk = SymbolData::with_index_e_e(&cfg.input_matrix_name, ctx.fa().rzu(k)?, ctx.fa().rzu(k)?).to_rc_expr();
    let mut radicand = akk;
    for s in 1..k {
        let lks = SymbolData::with_index_e_e(&cfg.l_matrix_name, ctx.fa().rzu(k)?, ctx.fa().rzu(s)?).to_rc_expr();
        let l2ks = ExpData::new(lks, ctx.fa().rzi(2)?).to_rc_expr();
        radicand = BinaryData::sub(radicand, l2ks).to_rc_expr();
    }
    return Ok(RootData::new(radicand, ctx.fa().rzi(2)?).to_rc_expr());
}

async fn create_expression_for_lik(
    ctx: &mut CalcContext<'_>,
    cfg: &DecomposeCholeskyConfig,
    i: usize,
    k: usize,
) -> Result<Rc<Expression>, CalcError> {
    let aik = SymbolData::with_index_e_e(&cfg.input_matrix_name, ctx.fa().rzu(i)?, ctx.fa().rzu(k)?).to_rc_expr();
    let mut numerator = aik;
    for s in 1..k {
        let lis = SymbolData::with_index_e_e(&cfg.l_matrix_name, ctx.fa().rzu(i)?, ctx.fa().rzu(s)?).to_rc_expr();
        let lks = SymbolData::with_index_e_e(&cfg.l_matrix_name, ctx.fa().rzu(k)?, ctx.fa().rzu(s)?).to_rc_expr();
        numerator = BinaryData::sub(
            numerator,
            BinaryData::mul(lis, lks).to_rc_expr()
        ).to_rc_expr();
    }
    let lkk = SymbolData::with_index_e_e(&cfg.l_matrix_name, ctx.fa().rzu(k)?, ctx.fa().rzu(k)?).to_rc_expr();
    return Ok(FracData::new(numerator, lkk).to_rc_expr());
}
