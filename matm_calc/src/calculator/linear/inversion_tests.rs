#[cfg(test)]
mod tests {
    use crate::calculator::linear::inversion::invert_matrix_using_adjugate;
    use crate::calculator::linear::inversion::invert_matrix_using_gaussian_elimination;
    use crate::calculator::linear::inversion::InvertMatrixUsingAdjugateConfig;
    use crate::calculator::linear::inversion::InvertMatrixUsingGaussConfig;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::matrix;
    use crate::result::dump_unicode_color;
    use futures::executor::block_on;
    use matm_model::testing::asserts::assert_result_ok;

    // Example from "Algebra liniowa w zadaniach", Jerzy Rutkowski, PWN 2008, page 80
    #[test]
    fn test_invert_by_adjugate_3_3() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            4, 5, 7,
            1, 2, 4,
            2, 3, 6).unwrap();

        let cfg = InvertMatrixUsingAdjugateConfig::default();
        ctx.explanation.add_name_eq_matrix(&cfg.matrix_name, a.clone()).unwrap();

        let result = block_on(invert_matrix_using_adjugate(&mut ctx, &cfg, &a));
        dump_unicode_color(&ctx.explanation);

        let inv_a = assert_result_ok(result);

        let exp_inv_a = matrix!(ctx.fa(), 3, 3;
            0, (-3), 2,
            (2/3), (10/3), (-3),
            (-1/3), (-2/3), 1).unwrap();
        assert_eq!(true, exp_inv_a == inv_a);
    }

    // Example from "Algebra liniowa w zadaniach", Jerzy Rutkowski, PWN 2008, page 88
    #[test]
    fn test_invert_by_gaussian_elimination_3_3() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let cfg = InvertMatrixUsingGaussConfig::default();

        let a = matrix!(ctx.fa(), 3, 3;
            1, 1, 3,
            2, 3, 5,
            4, 6, 11).unwrap();

        ctx.explanation.add_name_eq_matrix(&cfg.matrix_name, a.clone()).unwrap();

        let result = block_on(invert_matrix_using_gaussian_elimination(&mut ctx, &cfg, &a));
        dump_unicode_color(&ctx.explanation);

        let inv_a = assert_result_ok(result);
        ctx.explanation.add_name_eq_matrix("inv(A)", inv_a.clone()).unwrap();

        let exp_inv_a = matrix!(ctx.fa(), 3, 3;
            3, 7, (-4),
            (-2), (-1), 1,
            0, (-2), 1).unwrap();
        assert_eq!(true, exp_inv_a == inv_a);
    }

    // Example from "Algebra liniowa w zadaniach", Jerzy Rutkowski, PWN 2008, page 88
    #[test]
    fn test_invert_by_gaussian_elimination_4_4() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let cfg = InvertMatrixUsingGaussConfig::default();

        let a = matrix!(ctx.fa(), 4, 4;
            1, 1, 2, 1,
            4, 3, 7, 4,
            5, 4, 9, 6,
            4, 2, 5, 4).unwrap();
        ctx.explanation.add_name_eq_matrix(&cfg.matrix_name, a.clone()).unwrap();

        let result = block_on(invert_matrix_using_gaussian_elimination(&mut ctx, &cfg, &a));
        dump_unicode_color(&ctx.explanation);

        let inv_a = assert_result_ok(result);
        ctx.explanation.add_name_eq_matrix("inv(A)", inv_a.clone()).unwrap();
        
        let exp_inv_a = matrix!(ctx.fa(), 4, 4;
            2, 0, (-1), 1,
            8, (-3), 0, 1,
            (-4), 2, 0, (-1),
            (-1), (-1), 1, 0).unwrap();
        assert_eq!(true, exp_inv_a == inv_a);
    }

}
