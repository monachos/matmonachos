// Linear algebra algorithms

use std::rc::Rc;

use crate::calculator::calculate_equation_statement;
use crate::calculator::calculate_expression_quiet;
use crate::calculator::calculate_expression;
use crate::calculator::context::CalcContext;
use crate::comparator::compare_expressions;
use matm_error::error::CalcError;
use matm_model::core::index::Index;
use matm_model::core::index::Index2D;
use matm_model::core::index_range::IndexRange;
use matm_model::expressions::binary::BinaryData;
use matm_model::expressions::equation::EquationData;
use matm_model::expressions::matrices::MatrixData;
use matm_model::expressions::method::MethodData;
use matm_model::expressions::object::GaussPivotType;
use matm_model::expressions::symbol::SymbolData;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;
use matm_model::statements::text::TextStatement;
use matm_model::statements::ToStatement;


//TODO add columns operation (on left and right side of diagonal)
//TODO do pivot on columns
pub struct GaussianEliminationCalculatorConfig {
    pub matrix_name: String,
    pub row_name: String,
    pub column_name: String,
    pub pivot: GaussPivotType,
    pub make_identity: bool,
    pub reduce_upper: bool,
    pub reduce_lower: bool,
}

impl GaussianEliminationCalculatorConfig {
    pub fn default() -> Self {
        Self {
            matrix_name: String::from("A"),
            row_name: String::from("r"),
            column_name: String::from("c"),
            pivot: GaussPivotType::No,
            reduce_upper: false,
            reduce_lower: false,
            make_identity: false,
        }
    }
}

/// matrix - any matrix e.g. augmented matrix (square matrix and (1-column matrix or identity matrix))
pub async fn perform_gaussian_elimination(
    ctx: &mut CalcContext<'_>,
    cfg: &GaussianEliminationCalculatorConfig,
    matrix: &mut MatrixData,
) -> Result<(), CalcError> {
    ctx.explanation.add_statement(
        TextStatement::section("Gaussian elimination")
        .to_statement());
    ctx.explanation.add_name_eq_matrix(&cfg.matrix_name, matrix.clone())?;

    eliminate_by_rows(ctx, cfg, matrix).await?;
    return Ok(());
}

/// reduces lower elements or lower and upper elements
async fn eliminate_by_rows(
    ctx: &mut CalcContext<'_>,
    cfg: &GaussianEliminationCalculatorConfig,
    matrix: &mut MatrixData,
) -> Result<(), CalcError> {
    let columns_to_iterate: Vec<usize> = if cfg.reduce_lower {
        matrix.iter_rows().collect()
    } else if cfg.reduce_upper {
        matrix.iter_rows().rev().collect()
    } else {
        // iterate all rows (as columns) in order
        // to make_identity and pivot elements
        matrix.iter_rows().collect()
    };
    for iteration in columns_to_iterate {
        let base_index = Index2D::new(iteration, iteration)?;
        let column_symbol = SymbolData::with_index_e(&cfg.column_name, ctx.fa().rzu(base_index.column)?);
        ctx.explanation.add_statement(
            TextStatement::section("row reduction of column {column}")
            .and_expression("column", column_symbol.to_rc_expr())
            .to_statement());

        if base_index.row < matrix.rows() {
            run_pivoting_down(ctx, cfg, &cfg.pivot, matrix, &base_index).await?;
        }
        
        if cfg.reduce_lower && base_index.row < matrix.rows() {
            // performing lower rows operations
            let rows_range = IndexRange::range(
                Index::abs(base_index.row + 1)?,
                Index::end())?;
            perform_rows_operations(ctx, cfg, matrix, &base_index, rows_range).await?;
        }

        if cfg.reduce_upper && base_index.row > 1 {
            if !matrix.get(&base_index)?.is_zero() {
                // performing upper rows operations
                let rows_range = IndexRange::range(
                    Index::abs(1)?,
                    Index::abs(base_index.row - 1)?)?;
                perform_rows_operations(ctx, cfg, matrix, &base_index, rows_range).await?;
            }
        }

        if cfg.make_identity {
            perform_identity_element(ctx, cfg, matrix, &base_index).await?;
        }

        let main_eq = EquationData::new(
            SymbolData::new(&cfg.matrix_name).to_rc_expr(),
            matrix.clone().to_rc_expr()
        ).to_rc_expr();
        ctx.explanation.add_expr(Rc::clone(&main_eq))?;

        let eq = calculate_equation_statement(ctx, &main_eq).await?;
        match eq.right.as_ref() {
            Expression::Matrix(mx) => {
                matrix.set_matrix(Index2D::one(), &mx)?;
            }
            _ => {
                return Err(CalcError::from_str("Expected matrix"));
            }
        }
    }

    return Ok(());
}

async fn run_pivoting_down(
    ctx: &mut CalcContext<'_>,
    cfg: &GaussianEliminationCalculatorConfig,
    pivot: &GaussPivotType,
    matrix: &mut MatrixData,
    base_index: &Index2D) -> Result<(), CalcError> {
    match pivot {
        GaussPivotType::IfLessThanMaxAbs => {
            // partial pivoting
            let rows_range = IndexRange::range(
                Index::abs(base_index.row + 1)?,
                Index::end())?;
            run_pivot_for_rows(
                ctx,
                cfg,
                matrix,
                &base_index,
                rows_range).await?;
        }
        GaussPivotType::IfZero => {
            // partial pivoting only of base element is zero
            if matrix.get(&base_index)?.is_zero() {
                let rows_range = IndexRange::range(
                    Index::abs(base_index.row + 1)?,
                    Index::end())?;
                run_pivot_for_rows(
                    ctx,
                    cfg,
                    matrix,
                    &base_index,
                    rows_range).await?;
            }
        }
        _ => {}
    }
    Ok(())
}


async fn run_pivot_for_rows(
    ctx: &mut CalcContext<'_>,
    cfg: &GaussianEliminationCalculatorConfig,
    matrix: &mut MatrixData,
    base_index: &Index2D,
    rows_range: IndexRange) -> Result<(), CalcError> {
    // look for max(abs(a(base_index.row .. a.get_rows(), base_index.column)))
    let mut max_value_row: usize = base_index.row;
    let mut max_value: Rc<Expression> = matrix.get(&base_index)?.clone();
    for r in rows_range.iter(matrix.rows())? {
        let aii: Rc<Expression> = Rc::clone(matrix.get_i_i(r, base_index.column)?);
        let abs_aii = MethodData::abs(aii).to_rc_expr();
        let abs_value = calculate_expression_quiet(ctx, &abs_aii).await?;
        if let Some(cmp) = compare_expressions(ctx, &abs_value, &max_value).await? {
            if cmp.is_gt() {
                max_value_row = r;
                max_value = abs_value;
            } else {
                //TODO cannot compare
            }
        } else {
            //TODO cannot compare
        }
    }
    if base_index.row != max_value_row {
        // pivoting
        let pivot_el_sym = SymbolData::with_index_e_e(
            &cfg.matrix_name,
            ctx.fa().rzu(max_value_row)?,
            ctx.fa().rzu(base_index.row)?
        ).to_rc_expr();
        let par = TextStatement::paragraph(
            "found pivot element at {pivot_element}, \
            interchanging rows {current_row} with {pivot_row}...")
            .and_expression("pivot_element", pivot_el_sym)
            .and_expression("pivot_row", SymbolData::with_index_e(&cfg.row_name, ctx.fa().rzu(max_value_row)?).to_rc_expr())
            .and_expression("current_row", SymbolData::with_index_e(&cfg.row_name, ctx.fa().rzu(base_index.row)?).to_rc_expr())
            .to_statement();
        ctx.explanation.add_statement(par);
        matrix.swap_rows(base_index.row, max_value_row)?;

        ctx.explanation.add_name_eq_matrix(&cfg.matrix_name, matrix.clone())?;
    }
    Ok(())
}

async fn perform_rows_operations(
    ctx: &mut CalcContext<'_>,
    cfg: &GaussianEliminationCalculatorConfig,
    matrix: &mut MatrixData,
    base_index: &Index2D,
    rows_range: IndexRange) -> Result<(), CalcError> {
    for row in rows_range.iter(matrix.rows())? {
        if !matrix.get_i_i(row, base_index.column)?.is_zero() {
            let m = BinaryData::div(
                Rc::clone(matrix.get_i_i(row, base_index.column)?),
                Rc::clone(matrix.get_i_i(base_index.row, base_index.column)?)
            ).to_rc_expr();

            // r{i}' = r{i} - {m} * r{base_index.row}
            let row_i_prim_expr = SymbolData::with_index_e_and_postfix(
                &cfg.row_name,
                ctx.fa().rzu(row)?,
                "'").to_rc_expr();

            let row_i_expr = SymbolData::with_index_e(
                &cfg.row_name,
                ctx.fa().rzu(row)?
            ).to_rc_expr();

            let row_iter_expr = BinaryData::mul(
                Rc::clone(&m),
                SymbolData::with_index_e(
                    &cfg.row_name,
                    ctx.fa().rzu(base_index.row)?
                    ).to_rc_expr()
            ).to_rc_expr();

            ctx.explanation.add_expr_eq_expr(
                Rc::clone(&row_i_prim_expr),
                BinaryData::sub(row_i_expr, row_iter_expr).to_rc_expr()
            )?;

            let base_row = matrix.get_row(Index::abs(base_index.row)?)?;
            let base_row_m = BinaryData::mul(base_row.to_rc_expr(), m).to_rc_expr();
            let cur_row = matrix.get_row(Index::abs(row)?)?;
            let reduced_row_expr = BinaryData::sub(cur_row.to_rc_expr(), base_row_m).to_rc_expr();
            let row_eq = EquationData::new(
                row_i_prim_expr,
                reduced_row_expr
            ).to_rc_expr();
            ctx.explanation.add_expr(Rc::clone(&row_eq))?;

            // calculate row
            let eq = calculate_equation_statement(ctx, &row_eq).await?;
            if let Expression::Matrix(reduced_row) = eq.right.as_ref() {
                matrix.set_matrix(Index2D::new(row, 1)?, &reduced_row)?;
            } else {
                return Err(CalcError::from_str("Calculated row is not a matrix."));
            }
        }
    }
    Ok(())
}

async fn perform_identity_element(
    ctx: &mut CalcContext<'_>,
    cfg: &GaussianEliminationCalculatorConfig,
    matrix: &mut MatrixData,
    base_index: &Index2D) -> Result<(), CalcError> {
    let m1 = matrix.get(&base_index)?;
    if !m1.is_zero() && !m1.is_one() {
        let m = BinaryData::div(
            ctx.fa().rz1(),
            Rc::clone(m1)).to_rc_expr();
        // r{base_index.row}' = {m} * r{base_index.row}
        let row_index_expr = ctx.fa().rzu(base_index.row)?;
        let row_i_prim_expr = SymbolData::with_index_e_and_postfix(
            &cfg.row_name,
            Rc::clone(&row_index_expr),
            "'"
        ).to_rc_expr();

        ctx.explanation.add_expr_eq_expr(
            Rc::clone(&row_i_prim_expr),
            Expression::binary_mul_if_needed(
                Rc::clone(&m),
                SymbolData::with_index_e(&cfg.row_name, row_index_expr).to_rc_expr()
            )
        )?;

        let base_row = matrix.get_row(Index::abs(base_index.row)?)?;
        let new_row = BinaryData::mul(
            base_row.to_rc_expr(),
            m).to_rc_expr();

        // calculate row
        let reduced_row_res = calculate_expression(ctx, &new_row).await?;
        ctx.explanation.add_expr_eq_expr(Rc::clone(&row_i_prim_expr), Rc::clone(&reduced_row_res))?;

        match reduced_row_res.as_ref() {
            Expression::Matrix(reduced_row) => {
                matrix.set_matrix(Index2D::new(base_index.row, 1)?, &reduced_row)?;
            }
            _ => {
                return Err(CalcError::from_str("Calculated row is not a matrix."));
            }
        }
    }
    Ok(())
}

