use crate::calculator::linear::gaussian::perform_gaussian_elimination;
use crate::calculator::linear::gaussian::GaussianEliminationCalculatorConfig;
use crate::calculator::context::CalcContext;
use matm_error::error::CalcError;
use matm_model::expressions::matrices::MatrixData;
use matm_model::expressions::object::GaussPivotType;


pub struct CalculateRankConfig {
    pub matrix_name: String,
}

impl CalculateRankConfig {
    pub fn default() -> Self {
        Self {
            matrix_name: "A".to_owned(),
        }
    }
}

pub async fn calculate_rank(
    ctx: &mut CalcContext<'_>,
    cfg: &CalculateRankConfig,
    mx: &MatrixData
) -> Result<usize, CalcError> {
    return calculate_rank_using_row_echelon_form(ctx, cfg, mx).await;
}

/// calculates matrix rank using row echelon form
pub async fn calculate_rank_using_row_echelon_form(
    ctx: &mut CalcContext<'_>,
    cfg: &CalculateRankConfig,
    mx: &MatrixData
) -> Result<usize, CalcError> {
    
    let gauss_cfg = GaussianEliminationCalculatorConfig {
        matrix_name: cfg.matrix_name.clone(),
        row_name: String::from("r"),
        column_name: String::from("c"),
        pivot: GaussPivotType::IfZero,
        reduce_upper: false,
        reduce_lower: true,
        make_identity: false,
    };
    
    let mut elim_mx = mx.clone();
    perform_gaussian_elimination(ctx, &gauss_cfg, &mut elim_mx).await?;

    ctx.explanation.add_name_eq_matrix(&cfg.matrix_name, elim_mx.clone())?;

    let rank = elim_mx.count_non_zero_rows();
    Ok(rank)
}