#[cfg(test)]
mod tests {
    use crate::calculator::linear::cholesky::decompose_cholesky;
    use crate::calculator::linear::cholesky::DecomposeCholeskyConfig;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::matrix;
    use crate::result::dump_unicode_color;
    use futures::executor::block_on;
    use matm_model::expressions::matrices::MatrixData;
    use matm_model::testing::asserts::assert_matrix_that;
    use matm_model::testing::asserts::assert_result_error_that;
    use matm_model::testing::asserts::assert_result_ok;

    #[test]
    fn test_decompose_cholesky() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
                1, 2, 3,
                2, 8, 10,
                3, 10, 22
                ).unwrap();

        let cfg = DecomposeCholeskyConfig {
            input_matrix_name: String::from("A"),
            l_matrix_name: String::from("L"),
        };

        ctx.explanation.add_name_eq_matrix(&cfg.input_matrix_name, a.clone()).unwrap();

        let result = block_on(decompose_cholesky(&mut ctx, &cfg, &a));

        dump_unicode_color(&ctx.explanation);

        let l = assert_result_ok(result);
        ctx.explanation.add_name_eq_matrix(&cfg.input_matrix_name, l.clone()).unwrap();

        let exp_l = matrix!(ctx.fa(), 3, 3;
                1, 0, 0,
                2, 2, 0,
                3, 2, 3
                ).unwrap();
        assert_matrix_that(&l)
            .is_equal_to(exp_l);
    }

    #[test]
    fn test_decompose_cholesky_not_square() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let a = MatrixData::zeros(ctx.rules().radix, 4, 3).unwrap();
        let cfg = DecomposeCholeskyConfig::default();

        let result = block_on(decompose_cholesky(&mut ctx, &cfg, &a));
        dump_unicode_color(&ctx.explanation);

        assert_result_error_that(result)
            .has_message(format!("Matrix {} is not square.", &cfg.input_matrix_name).as_str());
    }

    #[test]
    fn test_decompose_cholesky_not_symmetric() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let a = matrix!(ctx.fa(), 3, 3;
                1, 50, 3,
                2, 8, 10,
                3, 10, 22
                ).unwrap();
        let cfg = DecomposeCholeskyConfig::default();

        let result = block_on(decompose_cholesky(&mut ctx, &cfg, &a));

        dump_unicode_color(&ctx.explanation);

        assert_result_error_that(result)
            .has_message(format!("Matrix {} is not symmetric.", &cfg.input_matrix_name).as_str());
    }
}