#[cfg(test)]
mod tests {
    use crate::calculator::linear::solvers::solve_eq_system_using_cramer;
    use crate::calculator::linear::solvers::SolveCramerConfig;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::matrix;
    use crate::result::dump_unicode_color;
    use futures::executor::block_on;
    use matm_model::testing::asserts::assert_matrix_that;
    use matm_model::testing::asserts::assert_result_error_that;
    use matm_model::testing::asserts::assert_result_ok;


    #[test]
    fn test_solve_eq_system_using_cramer_2() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 2, 2;
            1, 2,
            2, 5
        ).unwrap();
        
        let b = matrix!(ctx.fa(), 2, 1;
            4,
            0
        ).unwrap();

        let cfg = SolveCramerConfig::default();

        ctx.explanation.add_name_eq_matrix(&cfg.input_matrix_name, a.clone()).unwrap();
        ctx.explanation.add_name_eq_matrix(&cfg.constant_column_name, b.clone()).unwrap();
        let result = block_on(solve_eq_system_using_cramer(&mut ctx, &cfg, &a, &b));
        
        if let Ok(x) = &result {
            ctx.explanation.add_name_eq_matrix("x", x.clone()).unwrap();
        }

        dump_unicode_color(&ctx.explanation);

        let x = assert_result_ok(result);
        
        let expected = matrix!(ctx.fa(), 2, 1; 20, (-8)).unwrap();
        assert_matrix_that(&x).is_equal_to(expected);
    }

    #[test]
    fn test_solve_eq_system_using_cramer_3() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            1, 2, (-2),
            2, 1, 1,
            3, 2, 1
        ).unwrap();
        
        let b = matrix!(ctx.fa(), 3, 1;
            4,
            0,
            1
        ).unwrap();

        let cfg = SolveCramerConfig::default();

        ctx.explanation.add_name_eq_matrix(&cfg.input_matrix_name, a.clone()).unwrap();
        ctx.explanation.add_name_eq_matrix(&cfg.constant_column_name, b.clone()).unwrap();
        let result = block_on(solve_eq_system_using_cramer(&mut ctx, &cfg, &a, &b));
        
        if let Ok(x) = &result {
            ctx.explanation.add_name_eq_matrix("x", x.clone()).unwrap();
        }

        dump_unicode_color(&ctx.explanation);

        let x = assert_result_ok(result);
        
        let expected = matrix!(ctx.fa(), 3, 1;
            0, 1, (-1)).unwrap();
        assert_matrix_that(&x).is_equal_to(expected);
    }

    #[test]
    fn test_solve_eq_system_using_cramer_4() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 4, 4;
            2, 0, 1, 0,
            0, (-3), (-1), (-1),
            2, 0, 5, 3,
            3, (-3/2), 5, (9/2)
        ).unwrap();
        
        let b = matrix!(ctx.fa(), 4, 1;
                5,
                1,
                12,
                17
                ).unwrap();

        let cfg = SolveCramerConfig::default();

        ctx.explanation.add_name_eq_matrix(&cfg.input_matrix_name, a.clone()).unwrap();
        ctx.explanation.add_name_eq_matrix(&cfg.constant_column_name, b.clone()).unwrap();
        let result = block_on(solve_eq_system_using_cramer(&mut ctx, &cfg, &a, &b));
        
        if let Ok(x) = &result {
            ctx.explanation.add_name_eq_matrix("x", x.clone()).unwrap();
        }

        dump_unicode_color(&ctx.explanation);

        let x = assert_result_ok(result);
        
        let expected = matrix!(ctx.fa(), 4, 1;
            2,
            (-1),
            1,
            1
        ).unwrap();
        assert_matrix_that(&x).is_equal_to(expected);
    }

    #[test]
    fn test_solve_eq_system_using_cramer_not_square() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 2;
            2, 0,
            0, (-3),
            2, 0
        ).unwrap();

        let b = matrix!(ctx.fa(), 3, 1;
            5,
            1,
            12
        ).unwrap();

        let cfg = SolveCramerConfig::default();

        let result = block_on(solve_eq_system_using_cramer(&mut ctx, &cfg, &a, &b));
        dump_unicode_color(&ctx.explanation);

        assert_result_error_that(result)
            .has_message(format!("Matrix {} is not square.", cfg.input_matrix_name).as_str());
    }

    #[test]
    fn test_solve_eq_system_using_cramer_diff_rows() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 2, 2;
            2, 0,
            0, (-3)
        ).unwrap();

        let b = matrix!(ctx.fa(), 3, 1;
            5,
            1,
            12
        ).unwrap();

        let cfg = SolveCramerConfig::default();

        let result = block_on(solve_eq_system_using_cramer(&mut ctx, &cfg, &a, &b));
        dump_unicode_color(&ctx.explanation);

        assert_result_error_that(result)
            .has_message(format!(
            "Matrices {} and {} have different number of rows.",
            cfg.input_matrix_name,
            cfg.constant_column_name
        ).as_str());
    }
}