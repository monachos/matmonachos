#[cfg(test)]
mod tests {
    use crate::calculator::linear::solvers::solve_upper_triangular;
    use crate::calculator::linear::solvers::SolveUpperTriangularConfig;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::matrix;
    use crate::result::dump_unicode_color;
    use futures::executor::block_on;
    use matm_model::testing::asserts::assert_matrix_that;
    use matm_model::testing::asserts::assert_result_error_that;
    use matm_model::testing::asserts::assert_result_ok;

    #[test]
    fn test_solve() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let r = matrix!(ctx.fa(), 4, 4;
            3, (-3/2), 5, (9/2),
            0, (-3), (-1), (-1),
            0, 0, (-8/3), (-10/3),
            0, 0, 0, (-2)
        ).unwrap();

        let c = matrix!(ctx.fa(), 4, 1;
            17,
            1,
            (-6),
            (-2)
        ).unwrap();
        
        let cfg = SolveUpperTriangularConfig::default();

        ctx.explanation.add_name_eq_matrix(&cfg.input_matrix_name, r.clone()).unwrap();
        ctx.explanation.add_name_eq_matrix(&cfg.constant_column_name, c.clone()).unwrap();

        let x = assert_result_ok(
            block_on(solve_upper_triangular(
                &mut ctx,
                &cfg,
                &r,
                &c)));
        ctx.explanation.add_name_eq_matrix("x", x.clone()).unwrap();

        dump_unicode_color(&ctx.explanation);

        let expected = matrix!(ctx.fa(), 4, 1;
            2, (-1), 1, 1).unwrap();
        assert_matrix_that(&x)
            .is_equal_to(expected);
    }

    #[test]
    fn test_solve_not_square() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let r = matrix!(ctx.fa(), 3, 2;
            3, (-3/2),
            0, (-3),
            0, 0
        ).unwrap();

        let c = matrix!(ctx.fa(), 3, 1;
            17,
            1,
            (-6)
        ).unwrap();

        let cfg = SolveUpperTriangularConfig::default();

        let result = block_on(solve_upper_triangular(
            &mut ctx,
            &cfg,
            &r,
            &c));
        dump_unicode_color(&ctx.explanation);

        assert_result_error_that(result)
            .has_message(format!("Matrix {} is not square.", cfg.input_matrix_name).as_str());
    }

    #[test]
    fn test_solve_not_upper_triangular() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let r = matrix!(ctx.fa(), 4, 4;
            3, (-3/2), 5, (9/2),
            0, (-3), (-1), (-1),
            10, 0, (-8/3), (-10/3),
            0, 0, 0, (-2)
        ).unwrap();

        let c = matrix!(ctx.fa(), 4, 1;
            17,
            1,
            (-6),
            (-2)
        ).unwrap();

        let cfg = SolveUpperTriangularConfig::default();

        let result = block_on(solve_upper_triangular(&mut ctx, &cfg, &r, &c));

        dump_unicode_color(&ctx.explanation);

        assert_result_error_that(result)
            .has_message(format!("Matrix {} is not upper triangular.", cfg.input_matrix_name).as_str());
    }

    #[test]
    fn test_solve_different_rows() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let r = matrix!(ctx.fa(), 2, 2;
            3, (-3/2),
            0, (-3)
        ).unwrap();

        let c = matrix!(ctx.fa(), 3, 1;
            17,
            1,
            (-6)
        ).unwrap();

        let cfg = SolveUpperTriangularConfig::default();

        let result = block_on(solve_upper_triangular(&mut ctx, &cfg, &r, &c));
        dump_unicode_color(&ctx.explanation);

        assert_result_error_that(result)
            .has_message(format!("Matrices {} and {} have different number of rows.", cfg.input_matrix_name, cfg.constant_column_name).as_str());
    }

    #[test]
    fn test_solve_one_column() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let r = matrix!(ctx.fa(), 4, 4;
            3, (-3/2), 5, (9/2),
            0, (-3), (-1), (-1),
            0, 0, (-8/3), (-10/3),
            0, 0, 0, (-2)
        ).unwrap();

        let c = matrix!(ctx.fa(), 4, 2;
            17, 1,
            1, 1,
            (-6), 1,
            (-2), 1
        ).unwrap();

        let cfg = SolveUpperTriangularConfig::default();

        let result = block_on(solve_upper_triangular(&mut ctx, &cfg, &r, &c));
        dump_unicode_color(&ctx.explanation);

        assert_result_error_that(result)
            .has_message(format!("Matrix {} should have one column.", cfg.constant_column_name).as_str());
    }

}
