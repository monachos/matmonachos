use std::collections::VecDeque;
use std::rc::Rc;
use async_recursion::async_recursion;

use matm_model::expressions::ToExpression;
use matm_model::expressions::binary::BinaryOperator;
use matm_model::expressions::Expression;
use matm_model::expressions::int::IntData;
use matm_model::expressions::sign2::Sign2;
use crate::utils::multi_instance_collector::MultiInstanceCollector;


/// iterate through expr expression and collects product of all multiplication factors
#[async_recursion(?Send)]
pub async fn collect_product_factors<'a>(
    container: &mut VecDeque<&'a Expression>,
    expr: &'a Expression
) {
    match expr {
        Expression::Binary(bin_expr) => {
            if let BinaryOperator::Mul = bin_expr.operator {
                collect_product_factors(container, &bin_expr.left).await;
                collect_product_factors(container, &bin_expr.right).await;
            } else {
                container.push_back(expr);
            }
        }
        _ => {
            container.push_back(expr);
        }
    }
}

//---------------------------------------

pub struct ValueAndSign2<T> {
    pub value: T,
    pub sign: Sign2,
}

impl<T> ValueAndSign2<T> {
    pub fn new(value: T, sign: Sign2) -> Self {
        Self {
            value,
            sign,
        }
    }
}

/// Iterate through expr expression and collects sum/difference
/// of all addition/subtraction terms.
pub async fn collect_terms_of_sum_or_difference<'a>(
    expr: &'a Expression,
) -> VecDeque<ValueAndSign2<&'a Expression>> {
    let mut container = VecDeque::new();
    collect_terms_of_sum_or_difference_with_sign(&mut container, expr, Sign2::Positive).await;
    container
}

#[async_recursion(?Send)]
async fn collect_terms_of_sum_or_difference_with_sign<'a>(
    container: &mut VecDeque<ValueAndSign2<&'a Expression>>,
    expr: &'a Expression,
    sign: Sign2,
) {
    match expr {
        Expression::Binary(bin_expr) => {
            if let BinaryOperator::Add = bin_expr.operator {
                collect_terms_of_sum_or_difference_with_sign(container, &bin_expr.left, sign).await;
                collect_terms_of_sum_or_difference_with_sign(container, &bin_expr.right, sign).await;
            } else if let BinaryOperator::Sub = bin_expr.operator {
                collect_terms_of_sum_or_difference_with_sign(container, &bin_expr.left, sign).await;
                collect_terms_of_sum_or_difference_with_sign(container, &bin_expr.right, -sign).await;
            } else {
                container.push_back(ValueAndSign2::new(expr, sign));
            }
        }
        _ => {
            container.push_back(ValueAndSign2::new(expr, sign));
        }
    }
}

pub struct OptionalConstantAndVariable<C, V> {
    pub constant: C,
    pub variable: V,
}

//TODO test
pub async fn extract_product_of_constant_and_expr(
    expr: &Expression
) -> OptionalConstantAndVariable<Option<Rc<Expression>>, Option<Rc<Expression>>> {
    match expr {
        Expression::Binary(bin) => {
            if let BinaryOperator::Mul = bin.operator {
                if let Expression::Int(coef) = bin.left.as_ref() {
                    return OptionalConstantAndVariable {
                        constant: Some(coef.clone().to_expr().into()),
                        variable: Some(Rc::clone(&bin.right)),
                    };
                } else if let Expression::Frac(coef) = bin.left.as_ref() {
                    if !coef.has_symbol() {
                        return OptionalConstantAndVariable {
                            constant: Some(coef.clone().to_expr().into()),
                            variable: Some(bin.right.clone()),
                        };
                    }
                }
                if let Expression::Int(coef) = bin.right.as_ref() {
                    return OptionalConstantAndVariable {
                        constant: Some(coef.clone().to_expr().into()),
                        variable: Some(bin.left.clone()),
                    };
                } else if let Expression::Frac(coef) = bin.right.as_ref() {
                    if !coef.has_symbol() {
                        return OptionalConstantAndVariable {
                            constant: Some(coef.clone().to_expr().into()),
                            variable: Some(bin.left.clone()),
                        };
                    }
                }
            }
        }
        Expression::Int(i) => {
            return OptionalConstantAndVariable {
                constant: Some(i.clone().to_rc_expr()),
                variable: None,
            };
        }
        Expression::Frac(frac) => {
            if !frac.has_symbol() {
                return OptionalConstantAndVariable {
                    constant: Some(frac.clone().to_rc_expr()),
                    variable: None,
                };
            }
        }
        _ => {
        }
    }

    return OptionalConstantAndVariable {
        constant: None,
        variable: Some(Rc::new(expr.clone())),
    };
}

//---------------------------------------

//TODO test
#[async_recursion(?Send)]
pub async fn group_ints_by_count(collector: &mut MultiInstanceCollector<IntData>, expr: &Expression) {
    match expr {
        Expression::Binary(bin_expr) => {
            if let BinaryOperator::Mul = bin_expr.operator {
                group_ints_by_count(collector, &bin_expr.left).await;
                group_ints_by_count(collector, &bin_expr.right).await;
            }
        }
        Expression::Int(v) => {
            collector.add_item(v);
        }
        _ => {}
    }
}
