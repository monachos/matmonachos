
#[cfg(test)]
mod tests {
    use crate::calculator::calculate_expression;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::testing::asserts::test_calculation;
    use crate::testing::asserts::test_calculation_ok;
    use futures::executor::block_on;
    use matm_model::core::natural::RADIX_16;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::func::FunctionData;
    use matm_model::expressions::object::DetConfigData;
    use matm_model::expressions::object::GaussConfigData;
    use matm_model::expressions::object::InvConfigData;
    use matm_model::expressions::object::ObjectType;
    use matm_model::expressions::object::PascalMatrixConfigData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;
    use matm_model::testing::asserts::assert_result_error_that;
    use matm_model::testing::asserts::assert_result_ok;
    use rstest::rstest;

    #[test]
    fn test_calculate_unknown_function() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = FunctionData::new(
            "foo".to_owned(),
            vec![
                BinaryData::add(ctx.fa().rzi_u(4), ctx.fa().rzi_u(5)).to_rc_expr()
            ]).to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        assert_result_error_that(result)
            .has_message("Unknown function foo().");
    }

    #[rstest]
    #[case("cracovian", ObjectType::Cracovian)]
    #[case("det_config", ObjectType::DetConfig(DetConfigData::new()))]
    #[case("gauss_config", ObjectType::GaussConfig(GaussConfigData::new()))]
    #[case("inv_config", ObjectType::InvConfig(InvConfigData::new()))]
    #[case("matrix", ObjectType::Matrix)]
    #[case("pascal_matrix_config", ObjectType::PascalMatrixConfig(PascalMatrixConfigData::new()))]
    fn test_calculate_function(
        #[case] func_name: &str,
        #[case] expected_type: ObjectType,
    ) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = FunctionData::new(func_name.to_owned(), Vec::new()).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        assert_expression_that(&result)
            .as_object()
            .is_type(expected_type);
    }

    #[rstest]
    #[case("cracovian")]
    #[case("det_config")]
    #[case("gauss_config")]
    #[case("inv_config")]
    #[case("matrix")]
    #[case("pascal_matrix_config")]
    fn test_calculate_function_no_arg_expected_error(
        #[case] func_name: &str,
    ) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = FunctionData::new(func_name.to_owned(), vec![ctx.fa().rzi_u(1)]).to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        assert_result_error_that(result)
            .has_message(format!(
                "Function {}() cannot have any arguments.",
                func_name
            ).as_str());
    }

    #[test]
    fn test_radix_get_default() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let expr = FunctionData::new("radix".to_owned(), Vec::new()).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        assert_expression_that(&result)
            .as_integer()
            .is_int(10);
    }

    #[test]
    fn test_radix_get_16() {
        let mut ctx_data = CalcContextData::with_rules(|rules| {
            rules.radix = RADIX_16;
        });
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = FunctionData::new("radix".to_owned(), Vec::new()).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        assert_expression_that(&result)
            .as_integer()
            .is_int(16);
    }

    #[test]
    fn test_radix_set_16() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let expr = FunctionData::new(
            "radix".to_owned(),
            vec![ctx.fa().rzi_u(16)]
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr));
        let result = assert_result_ok(result);

        assert_expression_that(&result)
            .as_integer()
            .is_int(10);
        assert_eq!(16, ctx.future_rules.radix);
    }
}

