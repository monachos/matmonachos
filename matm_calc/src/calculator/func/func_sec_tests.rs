#[cfg(test)]
mod tests {
    use std::rc::Rc;

    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::func::FunctionData;
    use matm_model::expressions::Expression;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;

    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::testing::asserts::test_calculation_ok;

    fn run_function(factory: fn(calc_ctx: &CalcContext) -> Rc<Expression>) -> Rc<Expression> {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = FunctionData::new(
            FunctionData::SEC.to_owned(),
            vec![factory(&ctx)]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);
        return result;
    }

    #[test]
    fn test_sec_0() {
        let result = run_function(|ctx| ctx.fa().rzi_u(0));
        assert_expression_that(&result)
            .as_integer()
            .is_int(1);
    }

    #[test]
    fn test_sec_pi_12() {
        let result = run_function(|ctx| ctx.fa().rfe(
            ctx.fa().epi(),
            ctx.fa().ezi_u(12)));
        // sqrt(6) - sqrt(2)
        assert_expression_that(&result)
            .as_binary()
            .has_left_that(|e| {
                e.as_root().is_sqrt_i32(6);
            })
            .is_sub()
            .has_right_that(|e| {
                e.as_root().is_sqrt_i32(2);
            });
    }

    #[test]
    fn test_sec_pi_6() {
        let result = run_function(|ctx| ctx.fa().rfe(
            ctx.fa().epi(),
            ctx.fa().ezi_u(6)));
        // 2 * sqrt(3) / 3
        assert_expression_that(&result)
            .as_fraction()
            .has_numerator_that(|e| {
                e.as_binary()
                    .is_mul()
                    .has_left_i32(2)
                    .has_right_that(|e| {
                        e.as_root().is_sqrt_i32(3);
                    });
            })
            .has_denominator_int(3);
    }

    #[test]
    fn test_sec_pi_4() {
        let result = run_function(|ctx| ctx.fa().rfe(
            ctx.fa().epi(),
            ctx.fa().ezi_u(4)));
        // sqrt(2)
        assert_expression_that(&result)
            .as_root()
            .is_sqrt_i32(2);
    }

    #[test]
    fn test_sec_pi_3() {
        let result = run_function(|ctx| ctx.fa().rfe(
            ctx.fa().epi(),
            ctx.fa().ezi_u(3)));
        assert_expression_that(&result)
            .as_integer()
            .is_int(2);
    }

    #[test]
    fn test_sec_5pi_12() {
        let result = run_function(|ctx| ctx.fa().rfe(
            BinaryData::mul(ctx.fa().rzi_u(5), ctx.fa().rpi()).to_expr(),
            ctx.fa().ezi_u(12)));
        // sqrt(6) + sqrt(2)
        assert_expression_that(&result)
            .as_binary()
            .has_left_that(|e| {
                e.as_root().is_sqrt_i32(6);
            })
            .is_add()
            .has_right_that(|e| {
                e.as_root().is_sqrt_i32(2);
            });
    }

    #[test]
    fn test_sec_pi_2() {
        let result = run_function(|ctx| ctx.fa().rfe(
            ctx.fa().epi(),
            ctx.fa().ezi_u(2)));
        assert_expression_that(&result)
            .is_undefined();
    }

    #[test]
    fn test_sec_pi() {
        let result = run_function(|ctx| ctx.fa().rpi());
        assert_expression_that(&result)
            .as_integer()
            .is_int(-1);
    }
}
