#[cfg(test)]
mod tests {
    use rstest::rstest;

    use matm_model::expressions::func::FunctionData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_result_error_that;

    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::testing::asserts::test_calculation;

    #[rstest]
    #[case(FunctionData::SIN)]
    #[case(FunctionData::COS)]
    #[case(FunctionData::TAN)]
    #[case(FunctionData::COT)]
    #[case(FunctionData::SEC)]
    #[case(FunctionData::CSC)]
    fn test_trigonometric_func_no_argument(
        #[case] func_name: &str,
    ) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = FunctionData::new(
            func_name.to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        assert_result_error_that(result)
            .has_message(format!(
                "Function {}() should have 1 argument.",
                func_name
            ).as_str());
    }

    #[rstest]
    #[case(FunctionData::SIN)]
    #[case(FunctionData::COS)]
    #[case(FunctionData::TAN)]
    #[case(FunctionData::COT)]
    #[case(FunctionData::SEC)]
    #[case(FunctionData::CSC)]
    fn test_trigonometric_func_2_arguments(
        #[case] func_name: &str,
    ) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = FunctionData::new(
            func_name.to_owned(),
            vec![ctx.fa().rz0(), ctx.fa().rz0()]).to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        assert_result_error_that(result)
            .has_message(format!(
                "Function {}() should have 1 argument.",
                func_name
            ).as_str());
    }
}
