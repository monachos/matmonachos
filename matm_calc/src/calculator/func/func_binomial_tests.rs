
#[cfg(test)]
mod tests {
    use matm_model::expressions::func::FunctionData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;
    use matm_model::testing::asserts::assert_result_error_that;

    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::testing::asserts::assert_next_step_that;
    use crate::testing::asserts::test_calculation;
    use crate::testing::asserts::test_calculation_with_explanation_ok;

    #[test]
    fn test_binomial() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let expr = FunctionData::new("binomial".to_owned(), vec![
            ctx.fa().rzi_u(10),
            ctx.fa().rzi_u(3)
        ]).to_rc_expr();

        let result = test_calculation_with_explanation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(120);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .as_function()
            .has_name("binomial");
        assert_next_step_that(&mut steps)
            .as_binomial()
            .is_set_integer_that(|e| {e.is_int(10);})
            .is_subset_integer_that(|e| {e.is_int(3);});
    }

    #[test]
    fn test_binomial_no_argument() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = FunctionData::new(
            "binomial".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        assert_result_error_that(result)
            .has_message("Function binomial() should have 2 arguments.");
    }

    #[test]
    fn test_binomial_3_arguments() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = FunctionData::new(
            "binomial".to_owned(),
            vec![ctx.fa().rz0(), ctx.fa().rz0(), ctx.fa().rz0()]).to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        assert_result_error_that(result)
            .has_message("Function binomial() should have 2 arguments.");
    }
}

