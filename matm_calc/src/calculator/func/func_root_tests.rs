#[cfg(test)]
mod tests {
    use matm_model::expressions::func::FunctionData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;
    use matm_model::testing::asserts::assert_result_error_that;
    use rstest::rstest;

    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::testing::asserts::assert_next_step_that;
    use crate::testing::asserts::test_calculation;
    use crate::testing::asserts::test_calculation_with_explanation_ok;

    #[rstest]
    #[case(FunctionData::ROOT)]
    #[case(FunctionData::SQRT)]
    fn test_root(#[case] func_name: &str) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let expr = FunctionData::new(func_name.to_owned(), vec![
            ctx.fa().rzi_u(100)
        ]).to_rc_expr();

        let result = test_calculation_with_explanation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(10);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .as_function()
            .has_name(func_name);
        assert_next_step_that(&mut steps)
            .as_root()
            .is_radicand_integer_that(|e| {e.is_int(100);})
            .is_degree_integer_that(|e| {e.is_int(2);});
    }

    #[test]
    fn test_root_with_degree() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let expr = FunctionData::new("root".to_owned(), vec![
            ctx.fa().rzi_u(125),
            ctx.fa().rzi_u(3)
        ]).to_rc_expr();

        let result = test_calculation_with_explanation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(5);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .as_function()
            .has_name("root");
        assert_next_step_that(&mut steps)
            .as_root()
            .is_radicand_integer_that(|e| {e.is_int(125);})
            .is_degree_integer_that(|e| {e.is_int(3);});
    }

    #[test]
    fn test_root_no_argument() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = FunctionData::new(
            "root".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        assert_result_error_that(result)
            .has_message("Function root() should have 1 or 2 arguments.");
    }

    #[test]
    fn test_root_3_arguments() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = FunctionData::new(
            "root".to_owned(),
            vec![ctx.fa().rz0(), ctx.fa().rz0(), ctx.fa().rz0()]).to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        assert_result_error_that(result)
            .has_message("Function root() should have 1 or 2 arguments.");
    }

    #[test]
    fn test_sqrt_no_argument() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = FunctionData::new(
            "sqrt".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        assert_result_error_that(result)
            .has_message("Function sqrt() should have 1 argument.");
    }

    #[test]
    fn test_sqrt_2_arguments() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = FunctionData::new(
            "sqrt".to_owned(),
            vec![ctx.fa().rz0(), ctx.fa().rz0()]).to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        assert_result_error_that(result)
            .has_message("Function sqrt() should have 1 argument.");
    }
}

