mod func_binomial_tests;
mod func_cos_tests;
mod func_cot_tests;
mod func_csc_tests;
mod func_factorial_tests;
mod func_log_tests;
mod func_root_tests;
mod func_sec_tests;
mod func_sin_tests;
mod func_tan_tests;
mod func_tests;
mod func_trigonometric_tests;

use std::rc::Rc;

use matm_error::error::CalcError;
use matm_model::core::natural::RADIX_MAX;
use matm_model::core::natural::RADIX_MIN;
use matm_model::expressions::binary::BinaryData;
use matm_model::expressions::binomial::BinomialData;
use matm_model::expressions::func::FunctionData;
use matm_model::expressions::logarithm::LogData;
use matm_model::expressions::object::DetConfigData;
use matm_model::expressions::object::GaussConfigData;
use matm_model::expressions::object::InvConfigData;
use matm_model::expressions::object::ObjectData;
use matm_model::expressions::object::ObjectType;
use matm_model::expressions::object::PascalMatrixConfigData;
use matm_model::expressions::root::RootData;
use matm_model::expressions::unary::UnaryData;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;

use crate::angles::get_predefined_angle_from_expression;
use crate::angles::PredefinedAngle;
use crate::calculator::calculate_expression_step;
use crate::calculator::ModelData;
use crate::calculator::method::extract_limited_i32_argument;
use crate::calculator::CalcResultOrErr;
use crate::calculator::CalcResult;
use crate::calculator::context::CalcContext;

pub(crate) async fn calculate_function_expression(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, FunctionData>,
) -> CalcResultOrErr {
    let mut total_calculated = 0;
    // arguments
    let mut calc_args: Vec<Rc<Expression>> = Vec::new();
    for arg in &model.data.arguments {
        let calc_arg = calculate_expression_step(ctx, arg).await?;
        total_calculated += calc_arg.calculated_count;
        calc_args.push(calc_arg.object);
    }
    if total_calculated > 0 {
        let expr = FunctionData::new(
            model.data.name.clone(),
            calc_args).to_rc_expr();
        return Ok(CalcResult::new(expr, total_calculated));
    }

    if FunctionData::MATRIX.eq(&model.data.name) {
        calculate_matrix(ctx, &model).await
    } else if FunctionData::DET_CONFIG.eq(&model.data.name) {
        calculate_det_config(ctx, &model).await
    } else if FunctionData::GAUSS_CONFIG.eq(&model.data.name) {
        calculate_gauss_config(ctx, &model).await
    } else if FunctionData::INV_CONFIG.eq(&model.data.name) {
        calculate_inv_config(ctx, &model).await
    } else if FunctionData::CRACOVIAN.eq(&model.data.name) {
        calculate_cracovian(ctx, &model).await
    } else if FunctionData::PASCAL_MATRIX_CONFIG.eq(&model.data.name) {
        calculate_pascal_matrix_config(ctx, &model).await
    } else if FunctionData::SIN.eq(&model.data.name) {
        calculate_sin(ctx, &model).await
    } else if FunctionData::COS.eq(&model.data.name) {
        calculate_cos(ctx, &model).await
    } else if FunctionData::TAN.eq(&model.data.name) {
        calculate_tan(ctx, &model).await
    } else if FunctionData::COT.eq(&model.data.name) {
        calculate_cot(ctx, &model).await
    } else if FunctionData::SEC.eq(&model.data.name) {
        calculate_sec(ctx, &model).await
    } else if FunctionData::CSC.eq(&model.data.name) {
        calculate_csc(ctx, &model).await
    } else if FunctionData::RADIX.eq(&model.data.name) {
        calculate_radix(ctx, &model).await
    } else if FunctionData::BINOMIAL.eq(&model.data.name) {
        create_binomial(ctx, &model).await
    } else if FunctionData::FACTORIAL.eq(&model.data.name) {
        create_factorial(ctx, &model).await
    } else if FunctionData::LOG.eq(&model.data.name) {
        create_log(ctx, &model).await
    } else if FunctionData::LOG2.eq(&model.data.name) {
        create_log2(ctx, &model).await
    } else if FunctionData::LOG10.eq(&model.data.name) {
        create_log10(ctx, &model).await
    } else if FunctionData::ROOT.eq(&model.data.name) {
        create_root(ctx, &model).await
    } else if FunctionData::SQRT.eq(&model.data.name) {
        create_sqrt(ctx, &model).await
    } else if FunctionData::EQUATIONS.eq(&model.data.name) {
        create_equations(ctx, &model).await
    } else {
        Err(CalcError::from_string(format!(
            "Unknown function {}().",
            model.data.name)))
    }
}

fn validate_no_argument_expected(
    model: &ModelData<'_, Expression, FunctionData>,
) -> Result<(), CalcError> {
    if !model.data.arguments.is_empty() {
        return Err(CalcError::from_string(format!(
            "Function {}() cannot have any arguments.",
            model.data.name
        )));
    }
    Ok(())
}

async fn calculate_matrix(
    _ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, FunctionData>,
) -> CalcResultOrErr {
    validate_no_argument_expected(model)?;
    //TODO it should be a singleton
    let obj = ObjectData::new(ObjectType::Matrix).to_rc_expr();
    Ok(CalcResult::one(obj))
}

async fn calculate_det_config(
    _ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, FunctionData>,
) -> CalcResultOrErr {
    validate_no_argument_expected(model)?;
    let obj = ObjectData::new(ObjectType::DetConfig(DetConfigData::new())).to_rc_expr();
    Ok(CalcResult::one(obj))
}

async fn calculate_gauss_config(
    _ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, FunctionData>,
) -> CalcResultOrErr {
    validate_no_argument_expected(model)?;
    let obj = ObjectData::new(ObjectType::GaussConfig(GaussConfigData::new())).to_rc_expr();
    return Ok(CalcResult::one(obj));
}

async fn calculate_inv_config(
    _ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, FunctionData>,
) -> CalcResultOrErr {
    validate_no_argument_expected(model)?;
    let obj = ObjectData::new(ObjectType::InvConfig(InvConfigData::new())).to_rc_expr();
    return Ok(CalcResult::one(obj));
}

async fn calculate_cracovian(
    _ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, FunctionData>,
) -> CalcResultOrErr {
    validate_no_argument_expected(model)?;
    //TODO it should be a singleton
    let obj = ObjectData::new(ObjectType::Cracovian).to_rc_expr();
    return Ok(CalcResult::one(obj));
}

async fn calculate_pascal_matrix_config(
    _ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, FunctionData>,
) -> CalcResultOrErr {
    validate_no_argument_expected(model)?;
    let obj = ObjectData::new(ObjectType::PascalMatrixConfig(PascalMatrixConfigData::new())).to_rc_expr();
    return Ok(CalcResult::one(obj));
}

async fn calculate_sin(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, FunctionData>,
) -> CalcResultOrErr {
    if model.data.arguments.len() != 1 {
        return Err(CalcError::from_string(format!(
            "Function {}() should have 1 argument.",
            model.data.name
        )));
    }

    let value = model.data.arguments.get(0).unwrap();
    if let Some(ang) = get_predefined_angle_from_expression(value).await {
        match ang {
            PredefinedAngle::ZERO => {
                return Ok(CalcResult::one(ctx.fa().rz0()));
            }
            PredefinedAngle::PI12 => {
                // (sqrt(6)-sqrt(2)) / 4
                return Ok(CalcResult::one(ctx.fa().rfr(
                    BinaryData::sub(ctx.fa().rrootz(6)?, ctx.fa().rrootz(2)?).to_rc_expr(),
                    ctx.fa().rzi(4)?
                )));
            }
            PredefinedAngle::PI6 => {
                // 1/2
                return Ok(CalcResult::one(ctx.fa().rfi(1, 2)?));
            }
            PredefinedAngle::PI4 => {
                // sqrt(2) / 2
                return Ok(CalcResult::one(ctx.fa().rfr(
                    ctx.fa().rrootz(2)?,
                    ctx.fa().rzi(2)?
                )));
            }
            PredefinedAngle::PI3 => {
                // sqrt(3)/2
                return Ok(CalcResult::one(ctx.fa().rfr(
                    ctx.fa().rrootz(3)?,
                    ctx.fa().rzi(2)?
                )));
            }
            PredefinedAngle::FIVEPI12 => {
                // (sqrt(6)+sqrt(2)) / 4
                return Ok(CalcResult::one(ctx.fa().rfr(
                    BinaryData::add(ctx.fa().rrootz(6)?, ctx.fa().rrootz(2)?).to_rc_expr(),
                    ctx.fa().rzi(4)?
                )));
            }
            PredefinedAngle::PI2 => {
                return Ok(CalcResult::one(ctx.fa().rz1()));
            }
            PredefinedAngle::PI => {
                return Ok(CalcResult::one(ctx.fa().rz0()));
            }
        }
    }

    Ok(CalcResult::original(Rc::clone(model.orig)))
}

async fn calculate_cos(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, FunctionData>,
) -> CalcResultOrErr {
    if model.data.arguments.len() != 1 {
        return Err(CalcError::from_string(format!(
            "Function {}() should have 1 argument.",
            model.data.name
        )));
    }

    let value = model.data.arguments.get(0).unwrap();
    if let Some(ang) = get_predefined_angle_from_expression(value).await {
        match ang {
            PredefinedAngle::ZERO => {
                return Ok(CalcResult::one(ctx.fa().rz1()));
            }
            PredefinedAngle::PI12 => {
                // (sqrt(6)+sqrt(2)) / 4
                return Ok(CalcResult::one(ctx.fa().rfr(
                    BinaryData::add(ctx.fa().rrootz(6)?, ctx.fa().rrootz(2)?).to_rc_expr(),
                    ctx.fa().rzi(4)?
                )));
            }
            PredefinedAngle::PI6 => {
                // sqrt(3)/2
                return Ok(CalcResult::one(ctx.fa().rfr(
                    ctx.fa().rrootz(3)?,
                    ctx.fa().rzi(2)?
                )));
            }
            PredefinedAngle::PI4 => {
                // sqrt(2) / 2
                return Ok(CalcResult::one(ctx.fa().rfr(
                    ctx.fa().rrootz(2)?,
                    ctx.fa().rzi(2)?
                )));
            }
            PredefinedAngle::PI3 => {
                // 1/2
                return Ok(CalcResult::one(ctx.fa().rfi(1, 2)?));
            }
            PredefinedAngle::FIVEPI12 => {
                // (sqrt(6)-sqrt(2)) / 4
                return Ok(CalcResult::one(ctx.fa().rfr(
                    BinaryData::sub(ctx.fa().rrootz(6)?, ctx.fa().rrootz(2)?).to_rc_expr(),
                    ctx.fa().rzi(4)?
                )));
            }
            PredefinedAngle::PI2 => {
                return Ok(CalcResult::one(ctx.fa().rz0()));
            }
            PredefinedAngle::PI => {
                return Ok(CalcResult::one(ctx.fa().rzi_u(-1)));
            }
        }
    }
    Ok(CalcResult::original(Rc::clone(model.orig)))
}

async fn calculate_tan(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, FunctionData>,
) -> CalcResultOrErr {
    if model.data.arguments.len() != 1 {
        return Err(CalcError::from_string(format!(
            "Function {}() should have 1 argument.",
            model.data.name
        )));
    }

    let value = model.data.arguments.get(0).unwrap();
    if let Some(ang) = get_predefined_angle_from_expression(value).await {
        match ang {
            PredefinedAngle::ZERO => {
                return Ok(CalcResult::one(ctx.fa().rz0()));
            }
            PredefinedAngle::PI12 => {
                // 2 - sqrt(3)
                return Ok(CalcResult::one(BinaryData::sub(
                    ctx.fa().rzi(2)?,
                    ctx.fa().rrootz(3)?).to_rc_expr()));
            }
            PredefinedAngle::PI6 => {
                // sqrt(3) / 3
                return Ok(CalcResult::one(ctx.fa().rfr(
                    ctx.fa().rrootz(3)?,
                    ctx.fa().rzi(3)?
                )));
            }
            PredefinedAngle::PI4 => {
                return Ok(CalcResult::one(ctx.fa().rz1()));
            }
            PredefinedAngle::PI3 => {
                return Ok(CalcResult::one(ctx.fa().rrootz(3)?));
            }
            PredefinedAngle::FIVEPI12 => {
                // 2 + sqrt(3)
                return Ok(CalcResult::one(BinaryData::add(
                    ctx.fa().rzi(2)?,
                    ctx.fa().rrootz(3)?).to_rc_expr()));
            }
            PredefinedAngle::PI2 => {
                return Ok(CalcResult::one(Rc::new(Expression::Undefined)));
            }
            PredefinedAngle::PI => {
                return Ok(CalcResult::one(ctx.fa().rz0()));
            }
        }
    }
    Ok(CalcResult::original(Rc::clone(model.orig)))
}

async fn calculate_cot(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, FunctionData>,
) -> CalcResultOrErr {
    if model.data.arguments.len() != 1 {
        return Err(CalcError::from_string(format!(
            "Function {}() should have 1 argument.",
            model.data.name
        )));
    }

    let value = model.data.arguments.get(0).unwrap();
    if let Some(ang) = get_predefined_angle_from_expression(value).await {
        match ang {
            PredefinedAngle::ZERO => {
                return Ok(CalcResult::one(Rc::new(Expression::Undefined)));
            }
            PredefinedAngle::PI12 => {
                // 2 + sqrt(3)
                return Ok(CalcResult::one(BinaryData::add(
                    ctx.fa().rzi(2)?,
                    ctx.fa().rrootz(3)?).to_rc_expr()));
            }
            PredefinedAngle::PI6 => {
                return Ok(CalcResult::one(ctx.fa().rrootz(3)?));
            }
            PredefinedAngle::PI4 => {
                return Ok(CalcResult::one(ctx.fa().rz1()));
            }
            PredefinedAngle::PI3 => {
                // sqrt(3) / 3
                return Ok(CalcResult::one(ctx.fa().rfr(
                    ctx.fa().rrootz(3)?,
                    ctx.fa().rzi(3)?
                )));
            }
            PredefinedAngle::FIVEPI12 => {
                // 2 - sqrt(3)
                return Ok(CalcResult::one(BinaryData::sub(
                    ctx.fa().rzi(2)?,
                    ctx.fa().rrootz(3)?).to_rc_expr()));
            }
            PredefinedAngle::PI2 => {
                return Ok(CalcResult::one(ctx.fa().rz0()));
            }
            PredefinedAngle::PI => {
                return Ok(CalcResult::one(Rc::new(Expression::Undefined)));
            }
        }
    }
    Ok(CalcResult::original(Rc::clone(model.orig)))
}

async fn calculate_sec(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, FunctionData>,
) -> CalcResultOrErr {
    if model.data.arguments.len() != 1 {
        return Err(CalcError::from_string(format!(
            "Function {}() should have 1 argument.",
            model.data.name
        )));
    }

    let value = model.data.arguments.get(0).unwrap();
    if let Some(ang) = get_predefined_angle_from_expression(value).await {
        match ang {
            PredefinedAngle::ZERO => {
                return Ok(CalcResult::one(ctx.fa().rz1()));
            }
            PredefinedAngle::PI12 => {
                // sqrt(6) - sqrt(2)
                return Ok(CalcResult::one(BinaryData::sub(
                    ctx.fa().rrootz(6)?,
                    ctx.fa().rrootz(2)?).to_rc_expr()));
            }
            PredefinedAngle::PI6 => {
                // (2 * sqrt(3)) / 3
                return Ok(CalcResult::one(ctx.fa().rfr(
                    BinaryData::mul(
                        ctx.fa().rzi(2)?,
                        ctx.fa().rrootz(3)?
                    ).to_rc_expr(),
                    ctx.fa().rzi(3)?
                )));
            }
            PredefinedAngle::PI4 => {
                return Ok(CalcResult::one(ctx.fa().rrootz(2)?));
            }
            PredefinedAngle::PI3 => {
                return Ok(CalcResult::one(ctx.fa().rzi(2)?));
            }
            PredefinedAngle::FIVEPI12 => {
                // sqrt(6) + sqrt(2)
                return Ok(CalcResult::one(BinaryData::add(
                    ctx.fa().rrootz(6)?,
                    ctx.fa().rrootz(2)?).to_rc_expr()));
            }
            PredefinedAngle::PI2 => {
                return Ok(CalcResult::one(Rc::new(Expression::Undefined)));
            }
            PredefinedAngle::PI => {
                return Ok(CalcResult::one(ctx.fa().rzi(-1)?));
            }
        }
    }
    Ok(CalcResult::original(Rc::clone(model.orig)))
}

async fn calculate_csc(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, FunctionData>,
) -> CalcResultOrErr {
    if model.data.arguments.len() != 1 {
        return Err(CalcError::from_string(format!(
            "Function {}() should have 1 argument.",
            model.data.name
        )));
    }

    let value = model.data.arguments.get(0).unwrap();
    if let Some(ang) = get_predefined_angle_from_expression(value).await {
        match ang {
            PredefinedAngle::ZERO => {
                return Ok(CalcResult::one(Rc::new(Expression::Undefined)));
            }
            PredefinedAngle::PI12 => {
                // sqrt(6) + sqrt(2)
                return Ok(CalcResult::one(BinaryData::add(
                    ctx.fa().rrootz(6)?,
                    ctx.fa().rrootz(2)?).to_rc_expr()));
            }
            PredefinedAngle::PI6 => {
                return Ok(CalcResult::one(ctx.fa().rzi(2)?));
            }
            PredefinedAngle::PI4 => {
                return Ok(CalcResult::one(ctx.fa().rrootz(2)?));
            }
            PredefinedAngle::PI3 => {
                // (2 * sqrt(3)) / 3
                return Ok(CalcResult::one(ctx.fa().rfr(
                    BinaryData::mul(ctx.fa().rzi(2)?, ctx.fa().rrootz(3)?).to_rc_expr(),
                    ctx.fa().rzi(3)?
                )));
            }
            PredefinedAngle::FIVEPI12 => {
                // sqrt(6) - sqrt(2)
                return Ok(CalcResult::one(BinaryData::sub(
                    ctx.fa().rrootz(6)?,
                    ctx.fa().rrootz(2)?).to_rc_expr()));
            }
            PredefinedAngle::PI2 => {
                return Ok(CalcResult::one(ctx.fa().rz1()));
            }
            PredefinedAngle::PI => {
                return Ok(CalcResult::one(Rc::new(Expression::Undefined)));
            }
        }
    }
    Ok(CalcResult::original(Rc::clone(model.orig)))
}

async fn calculate_radix(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, FunctionData>,
) -> CalcResultOrErr {
    match model.data.arguments.len() {
        0 => {
            // return current radix
        }
        1 => {
            ctx.future_rules.radix = extract_limited_i32_argument(
                &model.data.arguments,
                0,
                RADIX_MIN as i32,
                RADIX_MAX as i32).await? as u32;
        }
        _ => {
            return Err(CalcError::from_string(format!(
                "Function {}() should have 0 or 1 argument.",
                model.data.name
            )));
        }
    }

    let res = ctx.fa().rzu(ctx.rules().radix as usize)?;
    return Ok(CalcResult::one(res));
}

async fn create_binomial(
    _ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, FunctionData>,
) -> CalcResultOrErr {
    if model.data.arguments.len() != 2 {
        return Err(CalcError::from_string(format!(
            "Function {}() should have 2 arguments.",
            model.data.name
        )));
    }

    let res = BinomialData::new(
        Rc::clone(&model.data.arguments[0]),
        Rc::clone(&model.data.arguments[1]),
    ).to_rc_expr();
    return Ok(CalcResult::one(res));
}

async fn create_factorial(
    _ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, FunctionData>,
) -> CalcResultOrErr {
    if model.data.arguments.len() != 1 {
        return Err(CalcError::from_string(format!(
            "Function {}() should have 1 argument.",
            model.data.name
        )));
    }

    let res = UnaryData::factorial(Rc::clone(&model.data.arguments[0])).to_rc_expr();
    return Ok(CalcResult::one(res));
}

async fn create_log(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, FunctionData>,
) -> CalcResultOrErr {
    return match model.data.arguments.len() {
        1 => {
            let value = &model.data.arguments[0];
            let res = LogData::with_value_and_base(Rc::clone(value), ctx.fa().re()).to_rc_expr();
            Ok(CalcResult::one(res))
        }
        2 => {
            let value = &model.data.arguments[0];
            let base = &model.data.arguments[1];
            let res = LogData::with_value_and_base(Rc::clone(value), Rc::clone(base)).to_rc_expr();
            Ok(CalcResult::one(res))
        }
        _ => {
            Err(CalcError::from_string(format!(
                "Function {}() should have 1 or 2 arguments.",
                model.data.name
            )))
        }
    }
}

async fn create_log2(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, FunctionData>,
) -> CalcResultOrErr {
    return match model.data.arguments.len() {
        1 => {
            let value = &model.data.arguments[0];
            let res = LogData::with_value_and_base(Rc::clone(value), ctx.fa().rzi_u(2)).to_rc_expr();
            Ok(CalcResult::one(res))
        }
        _ => {
            Err(CalcError::from_string(format!(
                "Function {}() should have 1 argument.",
                model.data.name
            )))
        }
    }
}

async fn create_log10(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, FunctionData>,
) -> CalcResultOrErr {
    return match model.data.arguments.len() {
        1 => {
            let value = &model.data.arguments[0];
            let res = LogData::with_value_and_base(Rc::clone(value), ctx.fa().rzi_u(10)).to_rc_expr();
            Ok(CalcResult::one(res))
        }
        _ => {
            Err(CalcError::from_string(format!(
                "Function {}() should have 1 argument.",
                model.data.name
            )))
        }
    }
}

async fn create_root(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, FunctionData>,
) -> CalcResultOrErr {
    return match model.data.arguments.len() {
        1 => {
            let radicand = &model.data.arguments[0];
            let res = RootData::new(Rc::clone(radicand), ctx.fa().rzi_u(2)).to_rc_expr();
            Ok(CalcResult::one(res))
        }
        2 => {
            let radicand = &model.data.arguments[0];
            let degree = &model.data.arguments[1];
            let res = RootData::new(Rc::clone(radicand), Rc::clone(degree)).to_rc_expr();
            Ok(CalcResult::one(res))
        }
        _ => {
            Err(CalcError::from_string(format!(
                "Function {}() should have 1 or 2 arguments.",
                model.data.name
            )))
        }
    }
}

async fn create_sqrt(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, FunctionData>,
) -> CalcResultOrErr {
    return match model.data.arguments.len() {
        1 => {
            let radicand = &model.data.arguments[0];
            let res = RootData::new(Rc::clone(radicand), ctx.fa().rzi_u(2)).to_rc_expr();
            Ok(CalcResult::one(res))
        }
        _ => {
            Err(CalcError::from_string(format!(
                "Function {}() should have 1 argument.",
                model.data.name
            )))
        }
    }
}

async fn create_equations(
    _ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, FunctionData>,
) -> CalcResultOrErr {
    validate_no_argument_expected(model)?;
    //TODO it should be a singleton
    let obj = ObjectData::new(ObjectType::Equations).to_rc_expr();
    return Ok(CalcResult::one(obj));
}
