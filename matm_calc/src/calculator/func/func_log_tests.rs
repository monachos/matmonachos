
#[cfg(test)]
mod tests {
    use matm_model::expressions::func::FunctionData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;
    use matm_model::testing::asserts::assert_result_error_that;
    use rstest::rstest;

    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::testing::asserts::assert_next_step_that;
    use crate::testing::asserts::test_calculation;
    use crate::testing::asserts::test_calculation_with_explanation_ok;

    #[test]
    fn test_log() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let expr = FunctionData::new("log".to_owned(), vec![
            ctx.fa().rzi_u(100)
        ]).to_rc_expr();

        let result = test_calculation_with_explanation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_log()
            .is_value_integer_that(|e| {e.is_int(100);})
            .has_base_that(|e| {e.is_e();});

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .as_function()
            .has_name("log");
        assert_next_step_that(&mut steps)
            .as_log()
            .is_value_integer_that(|e| {e.is_int(100);})
            .has_base_that(|e| {e.is_e();});
    }

    #[test]
    fn test_log_with_base() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let expr = FunctionData::new("log".to_owned(), vec![
            ctx.fa().rzi_u(100),
            ctx.fa().rzi_u(5)
        ]).to_rc_expr();

        let result = test_calculation_with_explanation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_log()
            .is_value_integer_that(|e| {e.is_int(100);})
            .is_base_integer_that(|e| {e.is_int(5);});

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .as_function()
            .has_name("log");
        assert_next_step_that(&mut steps)
            .as_log()
            .is_value_integer_that(|e| {e.is_int(100);})
            .is_base_integer_that(|e| {e.is_int(5);});
    }

    #[test]
    fn test_log2() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let expr = FunctionData::new("log2".to_owned(), vec![
            ctx.fa().rzi_u(100)
        ]).to_rc_expr();

        let result = test_calculation_with_explanation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_log()
            .is_value_integer_that(|e| {e.is_int(100);})
            .is_base_integer_that(|e| {e.is_int(2);});

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .as_function()
            .has_name("log2");
        assert_next_step_that(&mut steps)
            .as_log()
            .is_value_integer_that(|e| {e.is_int(100);})
            .is_base_integer_that(|e| {e.is_int(2);});
    }

    #[test]
    fn test_log10() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let expr = FunctionData::new("log10".to_owned(), vec![
            ctx.fa().rzi_u(100)
        ]).to_rc_expr();

        let result = test_calculation_with_explanation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_log()
            .is_value_integer_that(|e| {e.is_int(100);})
            .is_base_integer_that(|e| {e.is_int(10);});

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .as_function()
            .has_name("log10");
        assert_next_step_that(&mut steps)
            .as_log()
            .is_value_integer_that(|e| {e.is_int(100);})
            .is_base_integer_that(|e| {e.is_int(10);});
    }

    #[test]
    fn test_log_no_argument() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = FunctionData::new(
            "log".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        assert_result_error_that(result)
            .has_message("Function log() should have 1 or 2 arguments.");
    }

    #[rstest]
    #[case(FunctionData::LOG2)]
    #[case(FunctionData::LOG10)]
    fn test_log2_log10_no_argument(#[case] func_name: &str) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = FunctionData::new(
            func_name.to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        assert_result_error_that(result)
            .has_message(format!(
                "Function {}() should have 1 argument.",
                func_name
            ).as_str());
    }

    #[test]
    fn test_log_3_arguments() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = FunctionData::new(
            "log".to_owned(),
            vec![ctx.fa().rz0(), ctx.fa().rz0(), ctx.fa().rz0()]).to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        assert_result_error_that(result)
            .has_message("Function log() should have 1 or 2 arguments.");
    }

    #[rstest]
    #[case(FunctionData::LOG2)]
    #[case(FunctionData::LOG10)]
    fn test_2_arguments(#[case] func_name: &str) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = FunctionData::new(
            func_name.to_owned(),
            vec![ctx.fa().rz0(), ctx.fa().rz0()]).to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        assert_result_error_that(result)
            .has_message(format!(
                "Function {}() should have 1 argument.",
                func_name
            ).as_str());
    }
}

