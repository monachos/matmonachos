
#[cfg(test)]
mod tests {
    use matm_model::expressions::func::FunctionData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;
    use matm_model::testing::asserts::assert_result_error_that;

    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::testing::asserts::assert_next_step_that;
    use crate::testing::asserts::test_calculation;
    use crate::testing::asserts::test_calculation_with_explanation_ok;

    #[test]
    fn test_factorial() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let expr = FunctionData::new("factorial".to_owned(), vec![
            ctx.fa().rzi_u(4)
        ]).to_rc_expr();

        let result = test_calculation_with_explanation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(24);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .as_function()
            .has_name("factorial");
        assert_next_step_that(&mut steps)
            .as_unary()
            .is_factorial()
            .as_integer()
            .is_int(4);
    }

    #[test]
    fn test_factorial_no_argument() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = FunctionData::new(
            "factorial".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        assert_result_error_that(result)
            .has_message("Function factorial() should have 1 argument.");
    }

    #[test]
    fn test_factorial_2_arguments() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = FunctionData::new(
            "factorial".to_owned(),
            vec![ctx.fa().rz0(), ctx.fa().rz0()]).to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        assert_result_error_that(result)
            .has_message("Function factorial() should have 1 argument.");
    }
}

