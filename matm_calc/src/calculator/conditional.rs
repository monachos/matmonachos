use std::rc::Rc;

use crate::calculator::calculate_condition_step;
use crate::calculator::ModelData;
use crate::calculator::CalcResultOrErr;
use crate::calculator::CalcResult;
use crate::calculator::calculate_expression_step;
use crate::calculator::context::CalcContext;
use crate::conditions::evaluate_condition;
use matm_error::error::CalcError;
use matm_model::expressions::arithmetic_condition::ArithmeticCondition;
use matm_model::expressions::condition::Condition;
use matm_model::expressions::condition::ToCondition;
use matm_model::expressions::conditional::ConditionalClause;
use matm_model::expressions::conditional::ConditionalData;
use matm_model::expressions::logic_condition::LogicCondition;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;


pub async fn calculate_arithmetic_condition(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Condition, ArithmeticCondition>,
) -> Result<CalcResult<Rc<Condition>>, CalcError> {
    let left_calc = calculate_expression_step(ctx, &model.data.left).await?;
    let right_calc = calculate_expression_step(ctx, &model.data.right).await?;
    let total_calculated = left_calc.calculated_count + right_calc.calculated_count;
    if total_calculated > 0 {
        let res = ArithmeticCondition::from_operator(
            model.data.operator.clone(),
            left_calc.object,
            right_calc.object).to_rc_cond();
        return Ok(CalcResult::new(res, total_calculated));
    }

    Ok(CalcResult::original(Rc::clone(model.orig)))
}

pub async fn calculate_logic_condition(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Condition, LogicCondition>,
) -> Result<CalcResult<Rc<Condition>>, CalcError> {
    let left_calc = calculate_condition_step(ctx, &model.data.left).await?;
    let right_calc = calculate_condition_step(ctx, &model.data.right).await?;
    let total_calculated = left_calc.calculated_count + right_calc.calculated_count;
    if total_calculated > 0 {
        let res = LogicCondition::from_operator(
            model.data.operator.clone(),
            left_calc.object,
            right_calc.object).to_rc_cond();
        return Ok(CalcResult::new(res, total_calculated));
    }

    Ok(CalcResult::original(Rc::clone(model.orig)))
}


pub async fn calculate_conditional_expression(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, ConditionalData>,
) -> CalcResultOrErr {
    let mut total_calculated = 0;
    let mut clauses: Vec<ConditionalClause> = Vec::new();
    for clause in &model.data.clauses {
        let calc_cond = calculate_condition_step(ctx, &clause.condition).await?;

        let new_clause = ConditionalClause {
            expression: Rc::clone(&clause.expression),
            condition: calc_cond.object,
        };
        clauses.push(new_clause);

        total_calculated += calc_cond.calculated_count;
    }

    if total_calculated > 0 {
        let res = ConditionalData::from_vector(clauses).to_rc_expr();
        return Ok(CalcResult::new(res, total_calculated));
    }

    // evaluate conditions
    let mut eval_result: Vec<(usize, &ConditionalClause)> = Vec::new();
    for (i, clause) in model.data.clauses.iter().enumerate() {
        if let Some(eval_res) = evaluate_condition(ctx, &clause.condition).await? {
            if eval_res {
                eval_result.push((i, clause));
            }
        }
    }

    match eval_result.len() {
        0 => {
            // none is true
            Ok(CalcResult::original(Rc::clone(model.orig)))
        }
        1 => {
            // one condition is true => replace with single expresion
            let clause = eval_result.first().unwrap();
            Ok(CalcResult::one(Rc::clone(&clause.1.expression)))
        }
        _ => {
            // 2 or more are true => invalid conditions
            let conditions_met = eval_result.iter()
                .map(|tu| format!("{} at {}", tu.1, (tu.0)+1))
                .collect::<Vec<_>>()
                .join(", ");
            Err(CalcError::from_string(format!(
                "Only one condition can be met, conditions that are true: {}.",
                conditions_met)))
        }
    }
}
