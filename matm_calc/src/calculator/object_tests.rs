#[cfg(test)]
mod tests {
    use matm_model::expressions::object::DetConfigData;
    use matm_model::expressions::object::DetMethod;
    use matm_model::expressions::object::GaussConfigData;
    use matm_model::expressions::object::GaussPivotType;
    use matm_model::expressions::object::InvConfigData;
    use matm_model::expressions::object::InvMethod;
    use matm_model::expressions::object::ObjectData;
    use matm_model::expressions::object::ObjectType;
    use matm_model::expressions::object::PascalMatrixConfigData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;

    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::testing::asserts::test_calculation_ok;

    #[test]
    fn test_cracovian() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = ObjectData::new(ObjectType::Cracovian).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_object()
            .is_type(ObjectType::Cracovian);
    }

    #[test]
    fn test_det_config() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = ObjectData::new(ObjectType::DetConfig(DetConfigData::new())).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_object()
            .is_type(ObjectType::DetConfig(DetConfigData {
                method: DetMethod::Default,
            }));
    }

    #[test]
    fn test_gauss_config() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = ObjectData::new(ObjectType::GaussConfig(GaussConfigData::new())).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_object()
            .is_type(ObjectType::GaussConfig(GaussConfigData {
                pivot: GaussPivotType::No,
                make_identity: false,
                reduce_upper: false,
                reduce_lower: false,
            }));
    }

    #[test]
    fn test_inv_config() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = ObjectData::new(ObjectType::InvConfig(InvConfigData::new())).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_object()
            .is_type(ObjectType::InvConfig(InvConfigData {
                method: InvMethod::Adjugate,
            }));
    }

    #[test]
    fn test_matrix() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = ObjectData::new(ObjectType::Matrix).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_object()
            .is_type(ObjectType::Matrix);
    }

    #[test]
    fn test_pascal_matrix_config() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = ObjectData::new(ObjectType::PascalMatrixConfig(PascalMatrixConfigData::new())).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_object()
            .is_type(ObjectType::PascalMatrixConfig(PascalMatrixConfigData::new()));
    }
}
