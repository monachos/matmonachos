#[cfg(test)]
mod tests {
    use crate::calculator::calculate_expression;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::result::dump_unicode_color;
    use crate::testing::asserts::assert_next_step;
    use crate::testing::asserts::assert_next_step_that;
    use crate::testing::asserts::assert_none_step;
    use futures::executor::block_on;
    use matm_model::expressions::iterated_binary::IteratedBinaryData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;

    #[test]
    fn test_sum_int() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = IteratedBinaryData::sum(
            ctx.fa().sn("k"),
            ctx.fa().rzi_u(1),
            ctx.fa().rzi_u(4),
            ctx.fa().rsn("k")
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(10);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("1 + 2 + 3 + 4");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("3 + 3 + 4");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("6 + 4");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_integer()
            .is_int(10);
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_product_int() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = IteratedBinaryData::product(
            ctx.fa().sn("k"),
            ctx.fa().rzi_u(1),
            ctx.fa().rzi_u(4),
            ctx.fa().rsn("k")
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(24);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("1 * 2 * 3 * 4");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("2 * 3 * 4");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("6 * 4");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_integer()
            .is_int(24);
        assert_none_step(&mut steps);
    }

}