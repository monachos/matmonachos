use std::collections::VecDeque;
use std::rc::Rc;

use crate::calculator::int::addition::calculate_int_add;
use crate::calculator::CalcResultOrErr;
use crate::calculator::ModelData;
use crate::calculator::CalcResult;
use crate::calculator::calculate_expression_step;
use crate::calculator::context::CalcContext;
use crate::substitution::substitute_expression;
use crate::substitution::SymbolTable;
use matm_model::expressions::binary::BinaryData;
use matm_model::expressions::iterated_binary::IteratedBinaryData;
use matm_model::expressions::iterated_binary::IteratedBinaryOperator;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;


pub async fn calculate_iterated_binary_expression(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, IteratedBinaryData>,
) -> CalcResultOrErr {
    let calc_lower = calculate_expression_step(ctx, &model.data.lower_bound).await?;
    let calc_upper = calculate_expression_step(ctx, &model.data.upper_bound).await?;
    let calc_value = calculate_expression_step(ctx, &model.data.value).await?;
    let total_calc = calc_lower.calculated_count + calc_upper.calculated_count + calc_value.calculated_count;
    if total_calc > 0 {
        let res = IteratedBinaryData::from_operator(
            model.data.operator,
            model.data.iterator.clone(),
            calc_lower.object,
            calc_upper.object,
            calc_value.object).to_rc_expr();
        return Ok(CalcResult::new(res, total_calc));
    }

    match (model.data.lower_bound.as_ref(), model.data.upper_bound.as_ref()) {
        (Expression::Int(lower), Expression::Int(upper)) => {
            let mut k = lower.clone();
            let one = ctx.fa().z1();

            let mut items: VecDeque<Rc<Expression>> = VecDeque::new();
            while k <= *upper {
                let mut symbols = SymbolTable::new();
                symbols.add(model.data.iterator.clone(), k.clone().to_rc_expr());
                let subst_value = substitute_expression(Rc::clone(&model.data.value), &symbols).await;
                items.push_back(subst_value);
                k = Box::new(calculate_int_add(ctx, &k, &one).await?);
            }

            if !items.is_empty() {
                let mut result: Rc<Expression> = items.pop_front().unwrap();
                while let Some(e) = items.pop_front() {
                    result = match model.data.operator {
                        IteratedBinaryOperator::Sum => BinaryData::add(result, e).to_rc_expr(),
                        IteratedBinaryOperator::Product => BinaryData::mul(result, e).to_rc_expr(),
                    };
                }
                return Ok(CalcResult::one(result));
            } else {
                // return neutral element value
                let result = match model.data.operator {
                    IteratedBinaryOperator::Sum => ctx.fa().rz0(),
                    IteratedBinaryOperator::Product => ctx.fa().rz1(),
                };
                return Ok(CalcResult::one(result));
            }
        }
        _ => {}
    }

    Ok(CalcResult::original(Rc::clone(model.orig)))
}

