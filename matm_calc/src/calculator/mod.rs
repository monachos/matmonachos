mod assignment;
mod assignment_tests;
mod binary;
mod binomial;
mod binomial_tests;
mod conditional;
mod conditional_tests;
mod cracovian;
mod cracovians_tests;
mod equation;
mod equation_tests;
mod exponentiation;
mod exponentiation_tests;
mod factorization_tests;
mod fraction_tests;
mod iterated_binary;
mod iterated_binary_tests;
mod matrices;
mod matrices_tests;
mod object_tests;
mod root;
mod root_tests;
mod soe;
mod soe_tests;
mod tabular;
mod transformers_tests;
mod unary;
pub mod context;
pub mod factorization;
pub mod fraction;
pub mod func;
pub mod int;
pub mod linear;
pub mod method;
pub mod object;
pub mod transformers;

use async_recursion::async_recursion;
use std::rc::Rc;

use crate::calculator::binary::calculate_binary_expression;
use crate::calculator::binomial::calculate_binomial_expression;
use crate::calculator::context::CalcContext;
use crate::calculator::equation::calculate_equation;
use crate::calculator::exponentiation::calculate_exp_expression;
use crate::calculator::fraction::calculate_fraction;
use crate::calculator::int::calculate_int;
use crate::calculator::matrices::calculate_matrix;
use crate::calculator::root::calculate_root_expression;
use crate::calculator::soe::calculate_system_of_equations;
use crate::calculator::unary::calculate_unary_expression;
use crate::result::ResultContainer;
use matm_error::error::CalcError;
use matm_model::expressions::condition::Condition;
use matm_model::expressions::equation::EquationData;
use matm_model::expressions::Expression;
use matm_model::statements::Statement;

use self::assignment::calculate_assignment;
use self::conditional::calculate_arithmetic_condition;
use self::conditional::calculate_conditional_expression;
use self::conditional::calculate_logic_condition;
use self::cracovian::calculate_cracovian;
use self::func::calculate_function_expression;
use self::iterated_binary::calculate_iterated_binary_expression;
use self::method::calculate_method_expression;
use self::object::calculate_object_expression;


pub struct CalcResult<T> {
    pub object: T,
    pub calculated_count: usize,
}
 
impl<T> CalcResult<T> {
    pub fn new(obj: T, calc_count: usize) -> Self {
        Self {
            object: obj,
            calculated_count: calc_count,
        }
    }
 
    pub fn original(obj: T) -> Self {
        Self {
            object: obj,
            calculated_count: 0,
        }
    }
 
    /// Result with one calculated object
    pub fn one(obj: T) -> Self {
        Self {
            object: obj,
            calculated_count: 1,
        }
    }
}

type CalcResultOrErr = Result<CalcResult<Rc<Expression>>, CalcError>;

//-------------------------------------------------------------

pub(crate) struct ModelData<'a, M, D> {
    orig: &'a Rc<M>,
    data: &'a D,
}

impl<'a, M, D> ModelData<'a, M, D> {
    pub fn new(orig: &'a Rc<M>, data: &'a D) -> Self {
        Self {
            orig,
            data,
        }
    }
}


/// calculates expression in one step
#[async_recursion(?Send)]
pub async fn calculate_expression_step(
    ctx: &mut CalcContext<'_>,
    expr: &Rc<Expression>,
) -> CalcResultOrErr {
    match expr.as_ref() {
        Expression::Undefined => {
            Ok(CalcResult::original(Rc::clone(expr)))
        }
        Expression::Assignment(data) => {
            calculate_assignment(ctx, &ModelData::new(expr, data)).await
        }
        Expression::Bool(_) => {
            Ok(CalcResult::original(Rc::clone(expr)))
        }
        Expression::BoxSign => {
            Err(CalcError::from_str("Operation not allowed."))//TODO test
        }
        Expression::Equation(value) => {
            calculate_equation(ctx, &ModelData::new(expr, value)).await
        }
        Expression::SysOfEq(value) => {
            calculate_system_of_equations(ctx, &ModelData::new(expr, value)).await
        }
        Expression::Int(value) => {
            calculate_int(ctx, &ModelData::new(expr, value)).await
        }
        Expression::Frac(value) => {
            calculate_fraction(ctx, &ModelData::new(expr, value)).await
        }
        Expression::Cracovian(value) => {
            calculate_cracovian(ctx, &ModelData::new(expr, value)).await
        },
        Expression::Matrix(value) => {
            calculate_matrix(ctx, &ModelData::new(expr, value)).await
        }
        Expression::Symbol(_) => {
            Ok(CalcResult::original(Rc::clone(expr)))
        }
        Expression::Unary(data) => {
            calculate_unary_expression(ctx, &ModelData::new(expr, data)).await
        }
        Expression::Binary(data) => {
            calculate_binary_expression(ctx, &ModelData::new(expr, data)).await
        }
        Expression::Function(data) => {
            calculate_function_expression(ctx, &ModelData::new(expr, data)).await
        }
        Expression::Method(data) => {
            calculate_method_expression(ctx, &ModelData::new(expr, data)).await
        }
        Expression::Object(data) => {
            calculate_object_expression(ctx, &ModelData::new(expr, data)).await
        }
        Expression::IteratedBinary(data) => {
            calculate_iterated_binary_expression(ctx, &ModelData::new(expr, data)).await
        }
        Expression::Exp(obj) => {
            calculate_exp_expression(ctx, &ModelData::new(expr, obj)).await
        },
        Expression::Root(obj) => {
            calculate_root_expression(ctx, &ModelData::new(expr, obj)).await
        },
        Expression::Log(_) => {
            //TODO
            Ok(CalcResult::original(Rc::clone(expr)))
        },
        Expression::Im(_) => {
            //TODO
            Ok(CalcResult::original(Rc::clone(expr)))
        },
        Expression::Transc(_) => {
            //TODO
            Ok(CalcResult::original(Rc::clone(expr)))
        },
        Expression::Variable(_) => {
            Ok(CalcResult::original(Rc::clone(expr)))
        }
        Expression::Conditional(obj) => {
            calculate_conditional_expression(ctx, &ModelData::new(expr, obj)).await
        }
        Expression::Binomial(obj) => {
            calculate_binomial_expression(ctx, &ModelData::new(expr, obj)).await
        }
    }
}

/// calculates expression in many steps in order to achieve final result
pub async fn calculate_expression(
    ctx: &mut CalcContext<'_>,
    expr: &Rc<Expression>,
) -> Result<Rc<Expression>, CalcError> {
    // calculate expression in many steps
    let mut last_expr: Rc<Expression> = Rc::clone(expr);
    ctx.explanation.add_expr_and_calc_count(Rc::clone(&last_expr), 0)?;
    loop {
        let result = calculate_expression_step(ctx, &last_expr).await?;
        if result.calculated_count == 0 {
            break;
        }
        ctx.explanation.add_expr_and_calc_count(Rc::clone(&result.object), result.calculated_count)?;
        last_expr = result.object;
    }
    Ok(last_expr)
}

pub async fn calculate_expression_quiet(
    ctx: &mut CalcContext<'_>,
    expr: &Rc<Expression>,
) -> Result<Rc<Expression>, CalcError> {
    let mut explanation = ResultContainer::new();
    let mut nested_ctx = CalcContext::new(
        ctx.rules,
        ctx.future_rules,
        &mut explanation);
    calculate_expression(&mut nested_ctx, expr).await
}

/// calculates condition in one step
#[async_recursion(?Send)]
pub async fn calculate_condition_step(
    ctx: &mut CalcContext<'_>,
    cond: &Rc<Condition>,
) -> Result<CalcResult<Rc<Condition>>, CalcError> {
    match cond.as_ref() {
        Condition::Bool(_) => Ok(CalcResult::original(Rc::clone(cond))),
        Condition::Arithmetic(co) => calculate_arithmetic_condition(ctx, &ModelData::new(cond, co)).await,
        Condition::Logic(co) => calculate_logic_condition(ctx, &ModelData::new(cond, co)).await,
    }
}

//TODO test
pub async fn calculate_statement_step(
    _ctx: &mut CalcContext<'_>,
    stmt: &Statement,
) -> Result<CalcResult<Statement>, CalcError> {
    match stmt {
        _ => {
            let res_stmt = stmt.clone();//TODO do not copy
            Ok(CalcResult::original(res_stmt))
        }
    }
}

pub async fn calculate_statement(
    ctx: &mut CalcContext<'_>,
    stmt: &Statement,
) -> Result<Statement, CalcError> {
    // calculate statement in many steps
    let mut mut_stmt = stmt.clone();
    let mut last_calculated_count = 0;
    loop {
        let result = calculate_statement_step(ctx, &mut_stmt).await?;
        if result.calculated_count == 0 {
            ctx.explanation.add_statement_and_calc_count(mut_stmt.clone(), last_calculated_count)?;
            break;
        }
        // borrow used object without cloning
        ctx.explanation.add_statement_and_calc_count(mut_stmt, last_calculated_count)?;
        mut_stmt = result.object;
        last_calculated_count = result.calculated_count;
    }
    Ok(mut_stmt)
}

pub async fn calculate_equation_statement(
    ctx: &mut CalcContext<'_>,
    equation: &Rc<Expression>,
) -> Result<EquationData, CalcError> {
    let result = calculate_expression(ctx, equation).await?;
    if let Expression::Equation(eq) = result.as_ref() {
        Ok(eq.as_ref().clone())//TODO not optimal cloning
    } else {
        Err(CalcError::from_string(format!(
            "Calculated statement is not an equation: {}",
            result)))
    }
}
