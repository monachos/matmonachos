#[cfg(test)]
mod tests {
    use futures::executor::block_on;

    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::equation::EquationData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;

    use crate::calculator::context::CalcContext;
    use crate::calculator::calculate_expression;
    use crate::calculator::context::CalcContextData;
    use crate::result::dump_unicode_color;

    #[test]
    fn test_symbol_eq_bin() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = EquationData::new(
            ctx.fa().rsn("a"),
            BinaryData::add(
                ctx.fa().rzi_u(2),
                ctx.fa().rzi_u(3)
            ).to_rc_expr()
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_equation()
            .is_left_symbol_that(|s| { s.has_name("a"); })
            .has_right_that(|e| {
                e.as_integer().is_int(5);
            });

        // check steps
        assert_eq!(1, ctx.explanation.calculated_count());
    }
}