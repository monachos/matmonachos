use std::rc::Rc;

use matm_model::expressions::object::ObjectData;
use matm_model::expressions::Expression;

use crate::calculator::CalcResult;
use crate::calculator::ModelData;
use crate::calculator::CalcResultOrErr;
use crate::calculator::context::CalcContext;

pub(crate) async fn calculate_object_expression(
    _ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, ObjectData>,
) -> CalcResultOrErr {
    Ok(CalcResult::original(Rc::clone(model.orig)))
}
