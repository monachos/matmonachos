#[cfg(test)]
mod tests {
    use crate::calculator::calculate_expression;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::result::dump_unicode_color;
    use crate::testing::asserts::assert_next_step_that;
    use crate::testing::asserts::assert_none_step;
    use futures::executor::block_on;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;

    #[test]
    fn test_5_add_m1_mul_sym() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::add(
            ctx.fa().rzi_u(5),
            BinaryData::mul(
                ctx.fa().rzi_u(-1),
                ctx.fa().rsn("a"),
            ).to_rc_expr(),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("5 + (-1) * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("5 + (-a)");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("5 - a");
        assert_none_step(&mut steps);

        // check result
        assert_expression_that(&result)
            .as_binary()
            .is_sub();
    }

    #[test]
    fn test_5_add_sym_mul_m1() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::add(
            ctx.fa().rzi_u(5),
            BinaryData::mul(
                ctx.fa().rsn("a"),
                ctx.fa().rzi_u(-1),
            ).to_rc_expr(),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("5 + a * (-1)");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("5 + (-a)");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("5 - a");
        assert_none_step(&mut steps);

        // check result
        assert_expression_that(&result)
            .as_binary()
            .is_sub();
    }

    #[test]
    fn test_5_add_m10_mul_sym() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::add(
            ctx.fa().rzi_u(5),
            BinaryData::mul(
                ctx.fa().rzi_u(-10),
                ctx.fa().rsn("a"),
            ).to_rc_expr(),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("5 + (-10) * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("5 - 10 * a");
        assert_none_step(&mut steps);

        // check result
        assert_expression_that(&result)
            .as_binary()
            .is_sub();
    }

    #[test]
    fn test_5_add_sym_mul_m10() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::add(
            ctx.fa().rzi_u(5),
            BinaryData::mul(
                ctx.fa().rsn("a"),
                ctx.fa().rzi_u(-10),
            ).to_rc_expr(),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("5 + a * (-10)");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("5 - 10 * a");
        assert_none_step(&mut steps);

        // check result
        assert_expression_that(&result)
            .as_binary()
            .is_sub();
    }

    #[test]
    fn test_5_sub_m1_mul_sym() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::sub(
            ctx.fa().rzi_u(5),
            BinaryData::mul(
                ctx.fa().rzi_u(-1),
                ctx.fa().rsn("a"),
            ).to_rc_expr(),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("5 - (-1) * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("5 - (-a)");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("5 + a");
        assert_none_step(&mut steps);

        // check result
        assert_expression_that(&result)
            .as_binary()
            .is_add();
    }

    #[test]
    fn test_5_sub_sym_mul_m1() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::sub(
            ctx.fa().rzi_u(5),
            BinaryData::mul(
                ctx.fa().rsn("a"),
                ctx.fa().rzi_u(-1),
            ).to_rc_expr(),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("5 - a * (-1)");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("5 - (-a)");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("5 + a");
        assert_none_step(&mut steps);

        // check result
        assert_expression_that(&result)
            .as_binary()
            .is_add();
    }

    #[test]
    fn test_5_sub_m10_mul_sym() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::sub(
            ctx.fa().rzi_u(5),
            BinaryData::mul(
                ctx.fa().rzi_u(-10),
                ctx.fa().rsn("a"),
            ).to_rc_expr(),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("5 - (-10) * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("5 + 10 * a");
        assert_none_step(&mut steps);

        // check result
        assert_expression_that(&result)
            .as_binary()
            .is_add();
    }

    #[test]
    fn test_5_sub_sym_mul_m10() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::sub(
            ctx.fa().rzi_u(5),
            BinaryData::mul(
                ctx.fa().rsn("a"),
                ctx.fa().rzi_u(-10),
            ).to_rc_expr(),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("5 - a * (-10)");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("5 + 10 * a");
        assert_none_step(&mut steps);

        // check result
        assert_expression_that(&result)
            .as_binary()
            .is_add();
    }
}