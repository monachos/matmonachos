#[cfg(test)]
mod tests {
    use crate::calculator::context::CalcContext;
    use crate::calculator::calculate_expression;
    use crate::calculator::context::CalcContextData;
    use crate::result::dump_unicode_color;
    use crate::testing::asserts::assert_next_step;
    use crate::testing::asserts::assert_next_step_that;
    use crate::testing::asserts::assert_none_step;
    use futures::executor::block_on;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;


    #[test]
    fn test_add_int_frac() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::add(
            ctx.fa().rzi_u(2),
            ctx.fa().rfi_u(3, 4)
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_fraction()
            .is_ints(11, 4);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .has_numerator_format("2 * 4 + 3")
            .has_denominator_int(4);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .has_numerator_format("8 + 3")
            .has_denominator_int(4);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .is_ints(11, 4);
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_sub_int_frac() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::sub(
            ctx.fa().rzi_u(2),
            ctx.fa().rfi_u(3, 4),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_fraction()
            .is_ints(5, 4);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .has_numerator_format("2 * 4 - 3")
            .has_denominator_int(4);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .has_numerator_format("8 - 3")
            .has_denominator_int(4);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .is_ints(5, 4);
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_mul_int_frac() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::mul(
            ctx.fa().rzi_u(2),
            ctx.fa().rfi_u(3, 4),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_fraction()
            .is_ints(3, 2);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .has_numerator_format("2 * 3")
            .has_denominator_int(4);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .is_ints(6, 4);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .is_ints(3, 2);
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_div_int_frac() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::div(
            ctx.fa().rzi_u(2),
            ctx.fa().rfi_u(3, 4)
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_fraction()
            .is_ints(8, 3);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .has_numerator_format("2 * 4")
            .has_denominator_int(3);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .is_ints(8, 3);
        assert_none_step(&mut steps);
    }

}