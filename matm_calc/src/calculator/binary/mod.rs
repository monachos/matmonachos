mod binary_frac_frac_tests;
mod binary_frac_int_tests;
mod binary_frac_mx_tests;
mod binary_int_binary_tests;
mod binary_int_frac_tests;
mod binary_int_int_tests;
mod binary_int_mx_tests;
mod binary_int_sym_tests;
mod binary_int_unary_tests;
mod binary_mx_frac_tests;
mod binary_mx_int_tests;
mod binary_mx_mx_tests;
mod binary_sym_distributive_tests;
mod binary_sym_int_tests;
mod binary_sym_sym_tests;
mod binary_tests;

use std::collections::VecDeque;
use std::rc::Rc;

use crate::calculator::fraction::build_fraction_add_frac_frac;
use crate::calculator::fraction::build_fraction_add_frac_scalar;
use crate::calculator::fraction::build_fraction_add_scalar_frac;
use crate::calculator::fraction::build_fraction_div_frac_frac;
use crate::calculator::fraction::build_fraction_div_frac_scalar;
use crate::calculator::fraction::build_fraction_div_scalar_frac;
use crate::calculator::fraction::build_fraction_mul_frac_frac;
use crate::calculator::fraction::build_fraction_mul_frac_scalar;
use crate::calculator::fraction::build_fraction_mul_scalar_frac;
use crate::calculator::fraction::build_fraction_sub_frac_frac;
use crate::calculator::fraction::build_fraction_sub_frac_scalar;
use crate::calculator::fraction::build_fraction_sub_scalar_frac;
use crate::calculator::int::addition::calculate_int_add;
use crate::calculator::int::multiplication::calculate_int_mul;
use crate::calculator::int::subtraction::calculate_int_sub;
use crate::calculator::transformers::collect_terms_of_sum_or_difference;
use crate::calculator::transformers::extract_product_of_constant_and_expr;
use crate::calculator::transformers::ValueAndSign2;
use crate::calculator::CalcResultOrErr;
use crate::calculator::ModelData;
use crate::calculator::CalcResult;
use crate::calculator::calculate_expression_step;
use crate::calculator::context::CalcContext;
use crate::queue_multimap::QueueMultiMap;
use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use matm_model::expressions::binary::BinaryData;
use matm_model::expressions::binary::BinaryOperator;
use matm_model::expressions::int::IntData;
use matm_model::expressions::matrices::MatrixData;
use matm_model::expressions::sign2::Sign2;
use matm_model::expressions::symbol::SymbolData;
use matm_model::expressions::unary::UnaryData;
use matm_model::expressions::unary::UnaryOperator;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;


pub async fn calculate_binary_expression(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, BinaryData>,
) -> CalcResultOrErr {
    // calculate sum/diff of many terms
    if let Some(res) = calculate_binary_sym_terms(ctx, model.orig).await? {
        return Ok(res);
    }

    let left_value = calculate_expression_step(ctx, &model.data.left).await?;
    let right_value = calculate_expression_step(ctx, &model.data.right).await?;
    let total_calculated = left_value.calculated_count + right_value.calculated_count;
    if total_calculated > 0 {
        let expr = BinaryData::from_operator(
            model.data.operator,
            left_value.object,
            right_value.object).to_rc_expr();
        return Ok(CalcResult::new(expr, total_calculated));
    }

    if let Some(res) = calculate_binary_int_int(ctx, model).await? {
        return Ok(res);
    }
    if let Some(res) = calculate_binary_scalar_frac(ctx, model).await? {
        return Ok(res);
    }
    if let Some(res) = calculate_binary_int_matrix(ctx, model).await? {
        return Ok(res);
    }
    if let Some(res) = calculate_binary_int_symbol(ctx, model).await? {
        return Ok(res);
    }
    if let Some(res) = calculate_binary_int_unary(ctx, model).await? {
        return Ok(res);
    }
    if let Some(res) = calculate_binary_int_binary(ctx, model).await? {
        return Ok(res);
    }
    if let Some(res) = calculate_binary_frac_scalar(ctx, model).await? {
        return Ok(res);
    }
    if let Some(res) = calculate_binary_frac_frac(ctx, model).await? {
        return Ok(res);
    }
    if let Some(res) = calculate_binary_frac_matrix(ctx, model).await? {
        return Ok(res);
    }
    if let Some(res) = calculate_binary_matrix_int(ctx, model).await? {
        return Ok(res);
    }
    if let Some(res) = calculate_binary_matrix_frac(ctx, model).await? {
        return Ok(res);
    }
    if let Some(res) = calculate_binary_matrix_matrix(ctx, model).await? {
        return Ok(res);
    }
    if let Some(res) = calculate_binary_symbol_int(ctx, model).await? {
        return Ok(res);
    }
    if let Some(res) = calculate_binary_symbol_symbol(ctx, model).await? {
        return Ok(res);
    }
    if let Some(res) = calculate_binary_expr_binary(ctx, model).await? {
        return Ok(res);
    }
    Ok(CalcResult::original(Rc::clone(&model.orig)))
}

/// calculates int [op] int
async fn calculate_binary_int_int(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, BinaryData>,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    match (model.data.left.as_ref(), model.data.right.as_ref()) {
        (Expression::Int(a), Expression::Int(b)) => {
            let res_expr = match model.data.operator {
                BinaryOperator::Add => calculate_int_add(ctx, &a, &b).await?.to_rc_expr(),
                BinaryOperator::Sub => calculate_int_sub(ctx, &a, &b).await?.to_rc_expr(),
                BinaryOperator::Mul => calculate_int_mul(ctx, &a, &b).await?.to_rc_expr(),
                BinaryOperator::Div => ctx.fa().rfr(Rc::clone(&model.data.left), Rc::clone(&model.data.right)),
            };
            return Ok(Some(CalcResult::one(res_expr)));
        }
        _ => {}
    }

    Ok(None)
}

/// calculates int [op] frac
async fn calculate_binary_scalar_frac(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, BinaryData>,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    match (model.data.left.as_ref(), model.data.right.as_ref()) {
        (Expression::Int(a), Expression::Frac(b)) => {
            let res_expr = match model.data.operator {
                BinaryOperator::Add => build_fraction_add_scalar_frac(ctx, a, b).await?,
                BinaryOperator::Sub => build_fraction_sub_scalar_frac(ctx, a, b).await?,
                BinaryOperator::Mul => build_fraction_mul_scalar_frac(ctx, a, b).await?,
                BinaryOperator::Div => build_fraction_div_scalar_frac(ctx, a, b, true).await?,
            };
            return Ok(Some(CalcResult::one(res_expr)));
        }
        _ => {}
    }
    Ok(None)
}

/// calculates int [op] matrix
async fn calculate_binary_int_matrix(
    _ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, BinaryData>,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    match (model.data.left.as_ref(), model.data.right.as_ref()) {
        (Expression::Int(value), Expression::Matrix(mx)) => {
            let elements = mx.iter_right_down()
                .map(|idx| BinaryData::from_operator(
                    model.data.operator,
                        value.clone().to_rc_expr(),
                        Rc::clone(mx.get(&idx).unwrap())
                    ).to_rc_expr()
                )
                .collect();
            let new_matrix = MatrixData::from_vector(mx.rows(), mx.columns(), elements)?;
            let res_expr = new_matrix.to_rc_expr();
            return Ok(Some(CalcResult::one(res_expr)));
        }
        _ => {}
    }
    Ok(None)
}

/// calculates int [op] symbol
async fn calculate_binary_int_symbol(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, BinaryData>,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    match (model.data.left.as_ref(), model.data.right.as_ref()) {
        (Expression::Int(a), Expression::Symbol(b)) => {
            return match model.data.operator {
                BinaryOperator::Add => calculate_add_int_symbol(ctx, a, b).await,
                BinaryOperator::Sub => calculate_sub_int_symbol(ctx, a, b).await,
                BinaryOperator::Mul => calculate_mul_int_symbol(ctx, a, b).await,
                BinaryOperator::Div => calculate_div_int_symbol(ctx, a, b).await,
            };
        }
        _ => {}
    }
    Ok(None)
}

/// calculates int + symbol
async fn calculate_add_int_symbol(
    _ctx: &mut CalcContext<'_>,
    a: &IntData,
    b: &SymbolData,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    if a.is_zero() {
        // 0 + a => a
        let res = b.clone().to_rc_expr();
        return Ok(Some(CalcResult::one(res)));
    }
    Ok(None)
}

/// calculates int - symbol
async fn calculate_sub_int_symbol(
    _ctx: &mut CalcContext<'_>,
    a: &IntData,
    b: &SymbolData,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    if a.is_zero() {
        // 0 - a => -a
        let res = UnaryData::neg(b.clone().to_rc_expr()).to_rc_expr();
        return Ok(Some(CalcResult::one(res)));
    }
    Ok(None)
}

/// calculates int * symbol
async fn calculate_mul_int_symbol(
    ctx: &mut CalcContext<'_>,
    a: &IntData,
    b: &SymbolData,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    if a.is_zero() {
        // 0 * a => 0
        let res = ctx.fa().rz0();
        return Ok(Some(CalcResult::one(res)));
    }
    if a.is_one() {
        // 1 * a => a
        let res = b.clone().to_rc_expr();
        return Ok(Some(CalcResult::one(res)));
    }
    if a.is_minus_one() {
        // (-1) * a => -a
        let res = UnaryData::neg(b.clone().to_rc_expr()).to_rc_expr();
        return Ok(Some(CalcResult::one(res)));
    }
    Ok(None)
}

/// calculates int / symbol
async fn calculate_div_int_symbol(
    ctx: &mut CalcContext<'_>,
    a: &IntData,
    b: &SymbolData,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    if a.is_zero() {
        // 0 / a => 0
        let res = ctx.fa().rz0();
        return Ok(Some(CalcResult::one(res)));
    }
    let res = ctx.fa().rfr(a.clone().to_rc_expr(), b.clone().to_rc_expr());
    Ok(Some(CalcResult::one(res)))
}

/// calculates int [op] unary
async fn calculate_binary_int_unary(
    _ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, BinaryData>,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    match (model.data.left.as_ref(), model.data.right.as_ref()) {
        (Expression::Int(_), Expression::Unary(unary)) => {
            match unary.operator {
                UnaryOperator::Neg => {
                    match model.data.operator {
                        BinaryOperator::Add => {
                            // 1 + (-x) => 1 - x
                            let res = BinaryData::sub(Rc::clone(&model.data.left), Rc::clone(&unary.value)).to_rc_expr();
                            return Ok(Some(CalcResult::one(res)));
                        }
                        BinaryOperator::Sub => {
                            // 1 - (-x) => 1 + x
                            let res = BinaryData::add(Rc::clone(&model.data.left), Rc::clone(&unary.value)).to_rc_expr();
                            return Ok(Some(CalcResult::one(res)));
                        }
                        _ => {}
                    }
                }
                _ => {}
            }
        }
        _ => {}
    }
    Ok(None)
}

/// calculates int [op] binary
async fn calculate_binary_int_binary(
    _ctx: &mut CalcContext<'_>,
    _model: &ModelData<'_, Expression, BinaryData>,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    // match (&binary.left, &binary.right) {
    //     (Expression::Int(_), Expression::Binary(right)) => {
    //         if let BinaryOperator::Mul = right.operator {
    //             if right.left.is_minus_one() {
    //                 match binary.operator {
    //                     BinaryOperator::Add => {
    //                         // 1 + (-1) * x => 1 - x
    //                         let res = Binary::sub(binary.left.clone(), right.right.clone()).to_expr();
    //                         return Ok(Some(CalcResult::one(res)));
    //                     }
    //                     BinaryOperator::Sub => {
    //                         // 1 - (-1) * x => 1 + x
    //                         let res = Binary::add(binary.left.clone(), right.right.clone()).to_expr();
    //                         return Ok(Some(CalcResult::one(res)));
    //                     }
    //                     _ => {}
    //                 }
    //             }
    //         }
    //     }
    //     _ => {}
    // }
    Ok(None)
}

/// calculates frac [op] int
async fn calculate_binary_frac_scalar(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, BinaryData>,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    match (model.data.left.as_ref(), model.data.right.as_ref()) {
        (Expression::Frac(a), Expression::Int(b)) => {
            let res_expr = match model.data.operator {
                BinaryOperator::Add => build_fraction_add_frac_scalar(ctx, a, b).await?,
                BinaryOperator::Sub => build_fraction_sub_frac_scalar(ctx, a, b).await?,
                BinaryOperator::Mul => build_fraction_mul_frac_scalar(ctx, a, b).await?,
                BinaryOperator::Div => build_fraction_div_frac_scalar(ctx, a, b, true).await?,
            };
            return Ok(Some(CalcResult::one(res_expr)));
        }
        _ => {}
    }
    Ok(None)
}

/// calculates frac [op] frac
async fn calculate_binary_frac_frac(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, BinaryData>,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    match (model.data.left.as_ref(), model.data.right.as_ref()) {
        (Expression::Frac(a), Expression::Frac(b)) => {
            let res_expr = match model.data.operator {
                BinaryOperator::Add => build_fraction_add_frac_frac(ctx, a, b).await?,
                BinaryOperator::Sub => build_fraction_sub_frac_frac(ctx, a, b).await?,
                BinaryOperator::Mul => build_fraction_mul_frac_frac(ctx, a, b).await?,
                BinaryOperator::Div => build_fraction_div_frac_frac(ctx, a, b).await?,
            };
            return Ok(Some(CalcResult::one(res_expr)));
        }
        _ => {}
    }
    Ok(None)
}

/// calculates frac [op] matrix
async fn calculate_binary_frac_matrix(
    _ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, BinaryData>,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    match (model.data.left.as_ref(), model.data.right.as_ref()) {
        (Expression::Frac(frac), Expression::Matrix(mx)) => {
            let elements = mx.iter_right_down()
                .map(|idx| BinaryData::from_operator(
                    model.data.operator,
                        frac.clone().to_rc_expr(),
                        Rc::clone(mx.get(&idx).unwrap())
                    ).to_rc_expr()
                )
                .collect();
            let new_matrix = MatrixData::from_vector(mx.rows(), mx.columns(), elements)?;
            let res_expr = new_matrix.to_rc_expr();
            return Ok(Some(CalcResult::one(res_expr)));
        }
        _ => {}
    }
    Ok(None)
}

/// calculates matrix [op] int
async fn calculate_binary_matrix_int(
    _ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, BinaryData>,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    match (model.data.left.as_ref(), model.data.right.as_ref()) {
        (Expression::Matrix(mx), Expression::Int(value)) => {
            let elements = mx.iter_right_down()
                .map(|idx| BinaryData::from_operator(
                    model.data.operator,
                        Rc::clone(mx.get(&idx).unwrap()),
                        value.clone().to_rc_expr()
                    ).to_rc_expr()
                )
                .collect();
            let new_matrix = MatrixData::from_vector(mx.rows(), mx.columns(), elements)?;
            let res_expr = new_matrix.to_rc_expr();
            return Ok(Some(CalcResult::one(res_expr)));
        }
        _ => {}
    }
    Ok(None)
}

/// calculates matrix [op] frac
async fn calculate_binary_matrix_frac(
    _ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, BinaryData>,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    match (model.data.left.as_ref(), model.data.right.as_ref()) {
        (Expression::Matrix(mx), Expression::Frac(frac)) => {
            let elements = mx.iter_right_down()
                .map(|idx| BinaryData::from_operator(
                    model.data.operator,
                        Rc::clone(mx.get(&idx).unwrap()),
                        frac.clone().to_rc_expr()
                    ).to_rc_expr()
                )
                .collect();
            let new_matrix = MatrixData::from_vector(mx.rows(), mx.columns(), elements)?;
            let res_expr = new_matrix.to_rc_expr();
            return Ok(Some(CalcResult::one(res_expr)));
        }
        _ => {}
    }
    Ok(None)
}

/// calculates matrix [op] matrix
async fn calculate_binary_matrix_matrix(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, BinaryData>,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    match (model.data.left.as_ref(), model.data.right.as_ref()) {
        (Expression::Matrix(left_mx), Expression::Matrix(right_mx)) => {
            match model.data.operator {
                BinaryOperator::Add | BinaryOperator::Sub => {
                    return Ok(Some(apply_binary_add_sub_matrix_matrix(ctx, model.data.operator, left_mx, right_mx).await?));
                }
                BinaryOperator::Mul => {
                    return Ok(Some(apply_mul_matrix_matrix(ctx, left_mx, right_mx).await?));
                }
                _ => {
                    return Err(CalcError::from_string(format!(
                        "Not supported operation {} for two matrices",
                        model.data.operator)));
                }
            }
        }
        _ => {}
    }
    Ok(None)
}

async fn apply_binary_add_sub_matrix_matrix(
    _ctx: &mut CalcContext<'_>,
    operator: BinaryOperator,
    left_mx: &MatrixData,
    right_mx: &MatrixData
) -> CalcResultOrErr {
    if left_mx.rows() != right_mx.rows()
        || left_mx.columns() != right_mx.columns() {
        return Err(CalcError::from_string(format!("Cannot use operation {} for matrices of different size",
            operator)));
    }

    let elements = left_mx.iter_right_down()
        .map(|idx| BinaryData::from_operator(
                operator,
                Rc::clone(left_mx.get(&idx).unwrap()),
                Rc::clone(right_mx.get(&idx).unwrap())
            ).to_rc_expr()
        )
        .collect();
    let new_mx = MatrixData::from_vector(left_mx.rows(), left_mx.columns(), elements)?;
    let res_expr = new_mx.to_rc_expr();
    Ok(CalcResult::one(res_expr))
}

async fn apply_mul_matrix_matrix(
    ctx: &mut CalcContext<'_>,
    left_mx: &MatrixData,
    right_mx: &MatrixData
) -> CalcResultOrErr {
    if left_mx.columns() != right_mx.rows() {
        return Err(CalcError::from_string(format!("Cannot multiple matrices of size {} and {}",
            left_mx.size(), right_mx.size())));
    }

    let mut new_mx = MatrixData::zeros(
        ctx.rules().radix,
        left_mx.rows(),
        right_mx.columns())?;
    for idx in new_mx.iter_right_down() {
        let mut sum: Rc<Expression> = calculate_matrix_element_product(ctx, left_mx, right_mx, &idx, 1).await?;
        let iterations = left_mx.columns();
        for i in 2..=iterations {
            let term = calculate_matrix_element_product(ctx, left_mx, right_mx, &idx, i).await?;
            sum = BinaryData::add(sum, term).to_rc_expr();
        }
        new_mx.set(&idx, sum)?;
    }

    let res_expr = new_mx.to_rc_expr();
    Ok(CalcResult::one(res_expr))
}

async fn calculate_matrix_element_product(
    _ctx: &mut CalcContext<'_>,
    left_mx: &MatrixData,
    right_mx: &MatrixData,
    index: &Index2D,
    iteration: usize,
) -> Result<Rc<Expression>, CalcError> {
    let prod = BinaryData::mul(
        Rc::clone(left_mx.get_i_i(index.row, iteration)?),
        Rc::clone(right_mx.get_i_i(iteration, index.column)?),
    ).to_rc_expr();
    Ok(prod)
}

/// calculates symbol [op] int
async fn calculate_binary_symbol_int(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, BinaryData>,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    match (model.data.left.as_ref(), model.data.right.as_ref()) {
        (Expression::Symbol(a), Expression::Int(b)) => {
            return match model.data.operator {
                BinaryOperator::Add => calculate_add_symbol_int(ctx, a, b).await,
                BinaryOperator::Sub => calculate_sub_symbol_int(ctx, a, b).await,
                BinaryOperator::Mul => calculate_mul_symbol_int(ctx, a, b).await,
                BinaryOperator::Div => calculate_div_symbol_int(ctx, a, b).await,
            };
        }
        _ => {}
    }
    Ok(None)
}

/// calculates symbol + int
async fn calculate_add_symbol_int(
    _ctx: &mut CalcContext<'_>,
    a: &SymbolData,
    b: &IntData,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    if b.is_zero() {
        // a + 0 => a
        let res = a.clone().to_rc_expr();
        return Ok(Some(CalcResult::one(res)));
    }
    Ok(None)
}

/// calculates symbol - int
async fn calculate_sub_symbol_int(
    _ctx: &mut CalcContext<'_>,
    a: &SymbolData,
    b: &IntData,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    if b.is_zero() {
        // a - 0 => a
        let res = a.clone().to_rc_expr();
        return Ok(Some(CalcResult::one(res)));
    }
    Ok(None)
}

/// calculates symbol * int
async fn calculate_mul_symbol_int(
    ctx: &mut CalcContext<'_>,
    a: &SymbolData,
    b: &IntData,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    if b.is_zero() {
        // a * 0 => 0
        let res = ctx.fa().rz0();
        return Ok(Some(CalcResult::one(res)));
    }
    if b.is_one() {
        // a * 1 => a
        let res = a.clone().to_rc_expr();
        return Ok(Some(CalcResult::one(res)));
    }
    if b.is_minus_one() {
        // a * (-1) => -a
        let res = UnaryData::neg(a.clone().to_rc_expr()).to_rc_expr();
        return Ok(Some(CalcResult::one(res)));
    }
    Ok(None)
}

/// calculates symbol / int
async fn calculate_div_symbol_int(
    ctx: &mut CalcContext<'_>,
    a: &SymbolData,
    b: &IntData,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    if b.is_zero() {
        // a / 0 => error
        return Err(CalcError::from_string(format!(
            "Division of {} by zero.",
            a)));
    }
    let res = ctx.fa().rfe(a.clone().to_expr(), b.clone().to_expr());
    Ok(Some(CalcResult::one(res)))
}

/// calculates symbol [op] symbol
async fn calculate_binary_symbol_symbol(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, BinaryData>,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    match (model.data.left.as_ref(), model.data.right.as_ref()) {
        (Expression::Symbol(left), Expression::Symbol(right)) => {
            if left.eq(&right) {
                match model.data.operator {
                    BinaryOperator::Add |
                    BinaryOperator::Sub => {
                    }
                    BinaryOperator::Mul => {
                        //TODO not supported
                        // let res = Exp::new(
                        //     left.clone().to_expr(),
                        //     ctx.fa().ezi(2)?
                        // ).to_expr();
                        // return Ok(Some(CalcResult::one(res)));
                    }
                    BinaryOperator::Div => {
                        // a / a => 1
                        return Ok(Some(CalcResult::one(ctx.fa().rz1())));
                    }
                }
            }
            return Ok(None);
        }
        _ => {}
    }
    Ok(None)
}

async fn calculate_binary_sym_terms(
    ctx: &mut CalcContext<'_>,
    expr: &Expression,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    if !expr.has_symbol() {
        return Ok(None);
    }

    let flat_sum = collect_terms_of_sum_or_difference(expr).await;
    let flat_sum_len = flat_sum.len();
    if flat_sum_len > 1 {
        // for t in &flat_sum {
        //     let par = TextStatement::paragraph(
        //         "Flat term: {sign} {expr}")
        //         .and_param("sign", TextStatement::paragraph(&t.sign.to_string()).to_statement())
        //         .and_expression("expr", t.value.clone())
        //         .to_statement();
        //     explanation.add_statement(par);
        // }

        // aggregated variables: key is variable or other expression, value is constant (coefficient)
        let mut aggr_vars: QueueMultiMap<Rc<Expression>, ValueAndSign2<Rc<Expression>>> = QueueMultiMap::new();
        // aggregated constants
        let mut aggr_consts: VecDeque<ValueAndSign2<Rc<Expression>>> = VecDeque::new();
        for term in flat_sum {
            // extract term as multiplication of constant and any expression
            let prod = extract_product_of_constant_and_expr(&term.value).await;
            if let Some(v) = prod.variable {
                if let Some(coef) = prod.constant {
                    aggr_vars.push(v, ValueAndSign2::new(coef, term.sign));
                } else {
                    aggr_vars.push(v, ValueAndSign2::new(Rc::new(ctx.fa().ez1()), term.sign));
                }
            } else if let Some(coef) = prod.constant {
                aggr_consts.push_back(ValueAndSign2::new(coef, term.sign));
            } else {
                return Ok(None);
            }
        }

        // for i in &aggr_consts {
        //     let par = TextStatement::paragraph(
        //         "Grouped constant: {sign} {expr}")
        //         .and_param("sign", TextStatement::paragraph(&i.sign.to_string()).to_statement())
        //         .and_expression("expr", i.value.clone())
        //         .to_statement();
        //     explanation.add_statement(par);
        // }

        // for aggr in aggr_vars.iter() {
        //     let par = TextStatement::paragraph(
        //         "Grouped expression: {expr}")
        //         .and_expression("expr", aggr.key.clone())
        //         .to_statement();
        //     explanation.add_statement(par);

        //     for f in &aggr.values {
        //         let par = TextStatement::paragraph(
        //             "    value: {sign} {expr}")
        //             .and_param("sign", TextStatement::paragraph(&f.sign.to_string()).to_statement())
        //             .and_expression("expr", f.value.clone())
        //             .to_statement();
        //         explanation.add_statement(par);
        //     }
        // }

        let aggr_len = aggr_vars.keys_len() + aggr_consts.len();
        if !aggr_vars.is_empty() && aggr_len < flat_sum_len {
            let mut res_items: VecDeque<ValueAndSign2<Rc<Expression>>> = VecDeque::new();
            // first terms: collect constants c1 + c2 + ...
            while let Some(term) = aggr_consts.pop_front() {
                res_items.push_back(term);
            }
            // next terms: collect products (c1 + c2 + ...) * expr
            while let Some(mut term) = aggr_vars.pop() {
                // at least one coefficient exists
                let first_coef: ValueAndSign2<Rc<Expression>> = term.values.pop_front().unwrap();
                let mut coefficients: Rc<Expression> = match &first_coef.sign {
                    Sign2::Positive => Rc::clone(&first_coef.value),
                    Sign2::Negative => UnaryData::neg(first_coef.value).to_rc_expr(),
                };

                while let Some(e) = term.values.pop_front() {
                    coefficients = BinaryData::add_or_sub(coefficients, e.value, e.sign).to_rc_expr();
                }
                let prod = BinaryData::mul(coefficients, term.key).to_rc_expr();
                res_items.push_back(ValueAndSign2::new(prod, Sign2::Positive));
            }
            // create sum of all collected terms
            if let Some(term) = res_items.pop_front() {
                let mut res = term.value;
                while let Some(term) = res_items.pop_front() {
                    res = BinaryData::add_or_sub(res, term.value, term.sign).to_rc_expr();
                }
                return Ok(Some(CalcResult::one(res)));
            }
        }
    }

    Ok(None)
}

/// calculates expr [op] binary
async fn calculate_binary_expr_binary(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, BinaryData>,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    match model.data.right.as_ref() {
        Expression::Binary(right) => {
            match model.data.operator {
                BinaryOperator::Add => {
                    return calculate_add_expr_binary(ctx, &model.data.left, &right).await;
                }
                BinaryOperator::Sub => {
                    return calculate_sub_expr_binary(ctx, &model.data.left, &right).await;
                }
                _ => {}
            };
        }
        _ => {}
    }
    Ok(None)
}

/// calculates expr + binary
async fn calculate_add_expr_binary(
    _ctx: &mut CalcContext<'_>,
    a: &Rc<Expression>,
    b: &BinaryData,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    match b.operator {
        BinaryOperator::Mul => {
            match b.left.as_ref() {
                Expression::Int(b1) => {
                    if b1.is_negative() {
                        // a + (-20) * b2 => a - 20 * b2
                        let res = BinaryData::sub(
                            Rc::clone(a),
                            BinaryData::mul(
                                b1.calculate_neg().to_rc_expr(),
                                Rc::clone(&b.right)
                            ).to_rc_expr(),
                        ).to_rc_expr();
                        return Ok(Some(CalcResult::one(res)));
                    }
                }
                _ => {}
            }
            match b.right.as_ref() {
                Expression::Int(b2) => {
                    if b2.is_negative() {
                        // a + b1 * (-20) => a - 20 * b1 (swap operands)
                        let res = BinaryData::sub(
                            Rc::clone(a),
                            BinaryData::mul(
                                b2.calculate_neg().to_rc_expr(),
                                Rc::clone(&b.left.clone())
                            ).to_rc_expr(),
                        ).to_rc_expr();
                        return Ok(Some(CalcResult::one(res)));
                    }
                }
                _ => {}
            }
        }
        _ => {}
    }
    return Ok(None);
}

/// calculates expr - binary
async fn calculate_sub_expr_binary(
    _ctx: &mut CalcContext<'_>,
    a: &Rc<Expression>,
    b: &BinaryData,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    match b.operator {
        BinaryOperator::Mul => {
            match b.left.as_ref() {
                Expression::Int(b1) => {
                    if b1.is_negative() {
                        // a - (-20) * b2 => a + 20 * b2
                        let res = BinaryData::add(
                            Rc::clone(a),
                            BinaryData::mul(
                                b1.calculate_neg().to_rc_expr(),
                                Rc::clone(&b.right)
                            ).to_rc_expr(),
                        ).to_rc_expr();
                        return Ok(Some(CalcResult::one(res)));
                    }
                }
                _ => {}
            }
            match b.right.as_ref() {
                Expression::Int(b2) => {
                    if b2.is_negative() {
                        // a - b1 * (-20) => a + 20 * b1 (swap operands)
                        let res = BinaryData::add(
                            Rc::clone(a),
                            BinaryData::mul(
                                b2.calculate_neg().to_rc_expr(),
                                Rc::clone(&b.left)
                            ).to_rc_expr(),
                        ).to_rc_expr();
                        return Ok(Some(CalcResult::one(res)));
                    }
                }
                _ => {}
            }
        }
        _ => {}
    }
    return Ok(None);
}