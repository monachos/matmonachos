#[cfg(test)]
mod tests {
    use crate::calculator::calculate_expression;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::matrix;
    use crate::result::dump_unicode_color;
    use crate::testing::asserts::assert_next_step;
    use crate::testing::asserts::assert_next_step_that;
    use crate::testing::asserts::assert_none_step;
    use futures::executor::block_on;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;


    #[test]
    fn test_add_mx_int() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::add(
            matrix!(ctx.fa(), 2, 2;
                    1, 2,
                    3, 4
                ).unwrap().to_rc_expr(),
            ctx.fa().rzi_u(2)
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(2)
            .has_columns(2)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(3);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(4);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(5);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(6);});

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_matrix()
            .has_i_i_format(1, 1, "1 + 2")
            .has_i_i_format(1, 2, "2 + 2")
            .has_i_i_format(2, 1, "3 + 2")
            .has_i_i_format(2, 2, "4 + 2");
        assert_next_step_that(&mut steps)
            .has_calculation_count(4)
            .as_matrix()
            .is_i_i_integer_that(1, 1, |e| {e.is_int(3);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(4);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(5);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(6);});
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_sub_mx_int() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::sub(
            matrix!(ctx.fa(), 2, 2;
                    1, 2,
                    3, 4
                ).unwrap().to_rc_expr(),
            ctx.fa().rzi_u(2)
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(2)
            .has_columns(2)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(-1);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(0);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(1);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(2);});

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_matrix()
            .has_i_i_format(1, 1, "1 - 2")
            .has_i_i_format(1, 2, "2 - 2")
            .has_i_i_format(2, 1, "3 - 2")
            .has_i_i_format(2, 2, "4 - 2");
        assert_next_step_that(&mut steps)
            .has_calculation_count(4)
            .as_matrix()
            .is_i_i_integer_that(1, 1, |e| {e.is_int(-1);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(0);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(1);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(2);});
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_mul_mx_int() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::mul(
            matrix!(ctx.fa(), 2, 2;
                    1, 2,
                    3, 4
                ).unwrap().to_rc_expr(),
            ctx.fa().rzi_u(2)
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(2)
            .has_columns(2)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(2);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(4);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(6);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(8);});

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1);
        assert_next_step_that(&mut steps)
            .has_calculation_count(4);
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_div_mx_int() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::div(
            matrix!(ctx.fa(), 2, 2;
                    1, 2,
                    3, 4
                ).unwrap().to_rc_expr(),
            ctx.fa().rzi_u(2)
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(2)
            .has_columns(2)
            .is_i_i_fraction_that(1, 1, |e| {e.is_ints(1, 2);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(1);})
            .is_i_i_fraction_that(2, 1, |e| {e.is_ints(3, 2);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(2);});

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1);
        assert_next_step_that(&mut steps)
            .has_calculation_count(4);
        assert_next_step_that(&mut steps)
            .has_calculation_count(2);
        assert_none_step(&mut steps);
    }

}