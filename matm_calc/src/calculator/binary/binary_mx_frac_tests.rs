#[cfg(test)]
mod tests {
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::matrix;
    use crate::testing::asserts::test_calculation_ok;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;


    #[test]
    fn test_add_mx_frac() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::add(
            matrix!(ctx.fa(), 3, 2;
                (1/5), (2/5),
                (3/5), (4/5),
                1, (6/5)
            ).unwrap().to_rc_expr(),
            ctx.fa().rfi_u(2, 5),
        ).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(3)
            .has_columns(2)
            .is_i_i_fraction_that(1, 1, |e| {e.is_ints(3, 5);})
            .is_i_i_fraction_that(1, 2, |e| {e.is_ints(4, 5);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(1);})
            .is_i_i_fraction_that(2, 2, |e| {e.is_ints(6, 5);})
            .is_i_i_fraction_that(3, 1, |e| {e.is_ints(7, 5);})
            .is_i_i_fraction_that(3, 2, |e| {e.is_ints(8, 5);});
    }

    #[test]
    fn test_sub_mx_frac() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::sub(
            matrix!(ctx.fa(), 3, 2;
                (1/5), (2/5),
                (3/5), (4/5),
                1, (6/5)
            ).unwrap().to_rc_expr(),
            ctx.fa().rfi_u(2, 5),
        ).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(3)
            .has_columns(2)
            .is_i_i_fraction_that(1, 1, |e| {e.is_ints(-1, 5);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(0);})
            .is_i_i_fraction_that(2, 1, |e| {e.is_ints(1, 5);})
            .is_i_i_fraction_that(2, 2, |e| {e.is_ints(2, 5);})
            .is_i_i_fraction_that(3, 1, |e| {e.is_ints(3, 5);})
            .is_i_i_fraction_that(3, 2, |e| {e.is_ints(4, 5);});
    }

    #[test]
    fn test_mul_mx_frac() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::mul(
            matrix!(ctx.fa(), 3, 2;
                (1/5), (2/5),
                (3/5), (4/5),
                1, (6/5)
            ).unwrap().to_rc_expr(),
            ctx.fa().rfi_u(5, 2),
        ).to_rc_expr();
        
        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(3)
            .has_columns(2)
            .is_i_i_fraction_that(1, 1, |e| {e.is_ints(1, 2);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(1);})
            .is_i_i_fraction_that(2, 1, |e| {e.is_ints(3, 2);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(2);})
            .is_i_i_fraction_that(3, 1, |e| {e.is_ints(5, 2);})
            .is_i_i_integer_that(3, 2, |e| {e.is_int(3);});
    }

    #[test]
    fn test_div_mx_frac() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::div(
            matrix!(ctx.fa(), 3, 2;
                (1/5), (2/5),
                (3/5), (4/5),
                1, (6/5)
            ).unwrap().to_rc_expr(),
            ctx.fa().rfi_u(2, 3)
        ).to_rc_expr();
       
        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(3)
            .has_columns(2)
            .is_i_i_fraction_that(1, 1, |e| {e.is_ints(3, 10);})
            .is_i_i_fraction_that(1, 2, |e| {e.is_ints(3, 5);})
            .is_i_i_fraction_that(2, 1, |e| {e.is_ints(9, 10);})
            .is_i_i_fraction_that(2, 2, |e| {e.is_ints(6, 5);})
            .is_i_i_fraction_that(3, 1, |e| {e.is_ints(3, 2);})
            .is_i_i_fraction_that(3, 2, |e| {e.is_ints(9, 5);});
    }

}