#[cfg(test)]
mod tests {
    use crate::calculator::calculate_expression;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::result::dump_unicode_color;
    use futures::executor::block_on;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;

    #[test]
    fn test_add_a_a() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::add(
            ctx.fa().rsn("a"),
            ctx.fa().rsn("a")
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_binary()
            .is_mul()
            .has_left_i32(2)
            .is_right_symbol_that(|s| {s.has_name("a");});

        // check steps
        assert_eq!(2, ctx.explanation.calculated_count());
    }

    #[test]
    fn test_sub_a_a() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::sub(
            ctx.fa().rsn("a"),
            ctx.fa().rsn("a")
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(0);

        // check steps
        assert_eq!(3, ctx.explanation.calculated_count());
    }

    #[test]
    fn test_mul_a_a() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::mul(
            ctx.fa().rsn("a"),
            ctx.fa().rsn("a")
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_binary()
            .is_mul()
            .is_right_symbol_that(|s| {s.has_name("a");})
            .is_right_symbol_that(|s| {s.has_name("a");});

        // check steps
        assert_eq!(0, ctx.explanation.calculated_count());
    }

    #[test]
    fn test_div_a_a() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::div(
            ctx.fa().rsn("a"),
            ctx.fa().rsn("a")
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(1);

        // check steps
        assert_eq!(1, ctx.explanation.calculated_count());
    }

}