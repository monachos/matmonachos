#[cfg(test)]
mod tests {
    use crate::calculator::calculate_expression;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::result::dump_unicode_color;
    use crate::testing::asserts::assert_next_step_that;
    use crate::testing::asserts::assert_none_step;
    use futures::executor::block_on;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;
    use matm_model::testing::asserts::assert_result_error_that;

    #[test]
    fn test_sym_add_0() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::add(
            ctx.fa().rsn("a"),
            ctx.fa().rz0(),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("a + 0");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("a");
        assert_none_step(&mut steps);

        // check result
        assert_expression_that(&result)
            .as_symbol()
            .has_name("a");
    }

    #[test]
    fn test_sym_sub_0() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::sub(
            ctx.fa().rsn("a"),
            ctx.fa().rz0(),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("a - 0");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("a");
        assert_none_step(&mut steps);

        // check result
        assert_expression_that(&result)
            .as_symbol()
            .has_name("a");
    }

    #[test]
    fn test_sym_mul_1() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::mul(
            ctx.fa().rsn("a"),
            ctx.fa().rz1(),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("a * 1");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("a");
        assert_none_step(&mut steps);

        // check result
        assert_expression_that(&result)
            .as_symbol()
            .has_name("a");
    }

    #[test]
    fn test_sym_mul_0() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::mul(
            ctx.fa().rsn("a"),
            ctx.fa().rz0(),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("a * 0");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("0");
        assert_none_step(&mut steps);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(0);
    }

    #[test]
    fn test_sym_mul_m1() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::mul(
            ctx.fa().rsn("a"),
            ctx.fa().rzi_u(-1),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("a * (-1)");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("-a");
        assert_none_step(&mut steps);

        // check result
        assert_expression_that(&result)
            .as_unary()
            .is_neg();
    }

    #[test]
    fn test_sym_div_5() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::div(
            ctx.fa().rsn("a"),
            ctx.fa().rzi_u(5),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("a div 5");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .has_numerator_format("a")
            .has_denominator_int(5);
        assert_none_step(&mut steps);

        // check result
        assert_expression_that(&result)
            .as_fraction()
            .has_numerator_format("a")
            .has_denominator_int(5);
    }

    #[test]
    fn test_sym_div_0() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::div(
            ctx.fa().rsn("a"),
            ctx.fa().rz0(),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr));

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_result_error_that(result)
            .has_message("Division of a by zero.");

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("a div 0");
        assert_none_step(&mut steps);
   }
}