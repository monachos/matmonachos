#[cfg(test)]
mod tests {
    use crate::calculator::calculate_expression;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::result::dump_unicode_color;
    use crate::testing::asserts::assert_next_step;
    use crate::testing::asserts::assert_next_step_that;
    use crate::testing::asserts::assert_none_step;
    use futures::executor::block_on;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;


    #[test]
    fn test_add_frac_int() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::add(
            ctx.fa().rfi_u(2, 5),
            ctx.fa().rzi_u(2)
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();
        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_fraction()
            .is_ints(12, 5);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .has_numerator_format("2 + 2 * 5")
            .has_denominator_int(5);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .has_numerator_format("2 + 10")
            .has_denominator_int(5);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .is_ints(12, 5);
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_sub_frac_int() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::sub(
            ctx.fa().rfi_u(2, 5),
            ctx.fa().rzi_u(2)
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_fraction()
            .is_ints(-8, 5);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .has_numerator_format("2 - 2 * 5")
            .has_denominator_int(5);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .has_numerator_format("2 - 10")
            .has_denominator_int(5);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .is_ints(-8, 5);
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_mul_frac_int() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::mul(
            ctx.fa().rfi_u(2, 5),
            ctx.fa().rzi_u(2)
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_fraction()
            .is_ints(4, 5);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .has_numerator_format("2 * 2")
            .has_denominator_int(5);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .is_ints(4, 5);
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_div_frac_int() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::div(
            ctx.fa().rfi_u(2, 5),
            ctx.fa().rzi_u(2)
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_fraction()
            .is_ints(1, 5);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .has_numerator_int(2)
            .has_denominator_format("5 * 2");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .is_ints(2, 10);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .is_ints(1, 5);
        assert_none_step(&mut steps);
    }

}