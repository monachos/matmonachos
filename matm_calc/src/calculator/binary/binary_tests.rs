#[cfg(test)]
mod tests {
    use futures::executor::block_on;
    use std::rc::Rc;

    use crate::calculator::calculate_expression;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::matrix;
    use crate::result::dump_unicode_color;
    use crate::testing::asserts::assert_next_step;
    use crate::testing::asserts::assert_next_step_that;
    use crate::testing::asserts::assert_none_step;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::unary::UnaryData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;


    #[test]
    fn test_sum_of_determinants() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::add(
            UnaryData::det(
                matrix!(ctx.fa(), 3, 3;
                        1, 2, 3,
                        4, 1, 6,
                        1, 3, 3
                    ).unwrap().to_rc_expr(),
            ).to_rc_expr(),
            UnaryData::det(
                matrix!(ctx.fa(), 2, 2;
                        1, 2,
                        5, 2
                    ).unwrap().to_rc_expr(),
            ).to_rc_expr(),
        ).to_rc_expr();
        ctx.explanation.add_expr(Rc::clone(&expr)).unwrap();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(-2);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(2)
            .has_format("6 + (-8)");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_integer()
            .is_int(-2);
        assert_none_step(&mut steps);
    }

}