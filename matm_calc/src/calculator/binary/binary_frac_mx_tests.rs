#[cfg(test)]
mod tests {
    use crate::calculator::calculate_expression;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::matrix;
    use crate::result::dump_unicode_color;
    use crate::testing::asserts::assert_next_step;
    use crate::testing::asserts::assert_next_step_that;
    use crate::testing::asserts::assert_none_step;
    use crate::testing::asserts::test_calculation_ok;
    use futures::executor::block_on;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;


    #[test]
    fn test_add_frac_mx() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::add(
            ctx.fa().rfi_u(2, 5),
            matrix!(ctx.fa(), 3, 2;
                (1/5), (2/5),
                (3/5), (4/5),
                1, (6/5)
            ).unwrap().to_rc_expr(),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(3)
            .has_columns(2)
            .is_i_i_fraction_that(1, 1, |e| {e.is_ints(3, 5);})
            .is_i_i_fraction_that(1, 2, |e| {e.is_ints(4, 5);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(1);})
            .is_i_i_fraction_that(2, 2, |e| {e.is_ints(6, 5);})
            .is_i_i_fraction_that(3, 1, |e| {e.is_ints(7, 5);})
            .is_i_i_fraction_that(3, 2, |e| {e.is_ints(8, 5);});

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_matrix()
            .has_rows(3)
            .has_columns(2)
            .has_i_i_format(1, 1, "2/5 + 1/5")
            .has_i_i_format(1, 2, "2/5 + 2/5")
            .has_i_i_format(2, 1, "2/5 + 3/5")
            .has_i_i_format(2, 2, "2/5 + 4/5")
            .has_i_i_format(3, 1, "2/5 + 1")
            .has_i_i_format(3, 2, "2/5 + 6/5");
        assert_next_step_that(&mut steps)
            .has_calculation_count(6)
            .as_matrix()
            .has_rows(3)
            .has_columns(2)
            .has_i_i_format(1, 1, "(2 + 1)/5")
            .has_i_i_format(1, 2, "(2 + 2)/5")
            .has_i_i_format(2, 1, "(2 + 3)/5")
            .has_i_i_format(2, 2, "(2 + 4)/5")
            .has_i_i_format(3, 1, "(2 + 5)/5")
            .has_i_i_format(3, 2, "(2 + 6)/5");
        assert_next_step_that(&mut steps)
            .has_calculation_count(6)
            .as_matrix()
            .has_rows(3)
            .has_columns(2)
            .is_i_i_fraction_that(1, 1, |e| {e.is_ints(3, 5);})
            .is_i_i_fraction_that(1, 2, |e| {e.is_ints(4, 5);})
            .is_i_i_fraction_that(2, 1, |e| {e.is_ints(5, 5);})
            .is_i_i_fraction_that(2, 2, |e| {e.is_ints(6, 5);})
            .is_i_i_fraction_that(3, 1, |e| {e.is_ints(7, 5);})
            .is_i_i_fraction_that(3, 2, |e| {e.is_ints(8, 5);});
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_matrix()
            .has_rows(3)
            .has_columns(2)
            .is_i_i_fraction_that(1, 1, |e| {e.is_ints(3, 5);})
            .is_i_i_fraction_that(1, 2, |e| {e.is_ints(4, 5);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(1);})
            .is_i_i_fraction_that(2, 2, |e| {e.is_ints(6, 5);})
            .is_i_i_fraction_that(3, 1, |e| {e.is_ints(7, 5);})
            .is_i_i_fraction_that(3, 2, |e| {e.is_ints(8, 5);});
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_sub_frac_mx() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::sub(
            ctx.fa().rfi_u(2, 5),
            matrix!(ctx.fa(), 3, 2;
                (1/5), (2/5),
                (3/5), (4/5),
                1, (6/5)
            ).unwrap().to_rc_expr(),
        ).to_rc_expr();
 
        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(3)
            .has_columns(2)
            .is_i_i_fraction_that(1, 1, |e| {e.is_ints(1, 5);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(0);})
            .is_i_i_fraction_that(2, 1, |e| {e.is_ints(-1, 5);})
            .is_i_i_fraction_that(2, 2, |e| {e.is_ints(-2, 5);})
            .is_i_i_fraction_that(3, 1, |e| {e.is_ints(-3, 5);})
            .is_i_i_fraction_that(3, 2, |e| {e.is_ints(-4, 5);});
    }

    #[test]
    fn test_mul_frac_mx() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::mul(
            ctx.fa().rfi_u(5, 2),
            matrix!(ctx.fa(), 3, 2;
                (1/5), (2/5),
                (3/5), (4/5),
                1, (6/5)
            ).unwrap().to_rc_expr(),
        ).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(3)
            .has_columns(2)
            .is_i_i_fraction_that(1, 1, |e| {e.is_ints(1, 2);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(1);})
            .is_i_i_fraction_that(2, 1, |e| {e.is_ints(3, 2);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(2);})
            .is_i_i_fraction_that(3, 1, |e| {e.is_ints(5, 2);})
            .is_i_i_integer_that(3, 2, |e| {e.is_int(3);});
    }

    #[test]
    fn test_div_frac_mx() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::div(
            ctx.fa().rfi_u(2, 3),
            matrix!(ctx.fa(), 3, 2;
                (1/5), (2/5),
                (3/5), (4/5),
                1, (6/5)
            ).unwrap().to_rc_expr()
        ).to_rc_expr();
        
        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(3)
            .has_columns(2)
            .is_i_i_fraction_that(1, 1, |e| {e.is_ints(10, 3);})
            .is_i_i_fraction_that(1, 2, |e| {e.is_ints(5, 3);})
            .is_i_i_fraction_that(2, 1, |e| {e.is_ints(10, 9);})
            .is_i_i_fraction_that(2, 2, |e| {e.is_ints(5, 6);})
            .is_i_i_fraction_that(3, 1, |e| {e.is_ints(2, 3);})
            .is_i_i_fraction_that(3, 2, |e| {e.is_ints(5, 9);});
    }

}