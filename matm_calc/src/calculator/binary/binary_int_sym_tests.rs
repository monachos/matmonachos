#[cfg(test)]
mod tests {
    use crate::calculator::calculate_expression;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::result::dump_unicode_color;
    use crate::testing::asserts::assert_next_step_that;
    use crate::testing::asserts::assert_none_step;
    use futures::executor::block_on;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;

    #[test]
    fn test_0_add_sym() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::add(
            ctx.fa().rz0(),
            ctx.fa().rsn("a"),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("0 + a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("a");
        assert_none_step(&mut steps);

        // check result
        assert_expression_that(&result)
            .as_symbol()
            .has_name("a");
    }

    #[test]
    fn test_0_sub_sym() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::sub(
            ctx.fa().rz0(),
            ctx.fa().rsn("a"),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("0 - a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("-a");
        assert_none_step(&mut steps);

        // check result
        assert_expression_that(&result)
            .as_unary()
            .is_neg()
            .as_symbol()
            .has_name("a");
    }

    #[test]
    fn test_1_mul_sym() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::mul(
            ctx.fa().rz1(),
            ctx.fa().rsn("a"),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("1 * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("a");
        assert_none_step(&mut steps);

        // check result
        assert_expression_that(&result)
            .as_symbol()
            .has_name("a");
    }

    #[test]
    fn test_0_mul_sym() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::mul(
            ctx.fa().rz0(),
            ctx.fa().rsn("a"),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("0 * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("0");
        assert_none_step(&mut steps);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(0);
    }

    #[test]
    fn test_m1_mul_sym() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::mul(
            ctx.fa().rzi_u(-1),
            ctx.fa().rsn("a"),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("(-1) * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("-a");
        assert_none_step(&mut steps);

        // check result
        assert_expression_that(&result)
            .as_unary()
            .is_neg();
    }

    #[test]
    fn test_5_div_sym() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::div(
            ctx.fa().rzi_u(5),
            ctx.fa().rsn("a"),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("5 div a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("5/a")
            .as_fraction()
            .has_numerator_int(5);
        assert_none_step(&mut steps);

        // check result
        assert_expression_that(&result)
            .as_fraction()
            .has_numerator_int(5)
            .has_denominator_format("a");
    }

    #[test]
    fn test_0_div_sym() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::div(
            ctx.fa().rz0(),
            ctx.fa().rsn("a"),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("0 div a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("0");
        assert_none_step(&mut steps);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(0);
    }
}