#[cfg(test)]
mod tests {
    use crate::calculator::calculate_expression;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::result::dump_unicode_color;
    use crate::testing::asserts::assert_next_step_that;
    use crate::testing::asserts::assert_none_step;
    use futures::executor::block_on;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;

    #[test]
    fn test_2a_add_a() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::add(
            BinaryData::mul(
                ctx.fa().rzi_u(2),
                ctx.fa().rsn("a")
            ).to_rc_expr(),
            ctx.fa().rsn("a"),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_binary()
            .is_mul()
            .has_left_i32(3)
            .is_right_symbol_that(|s| {s.has_name("a");});

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("2 * a + a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("(2 + 1) * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("3 * a");
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_a_add_2a() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::add(
            ctx.fa().rsn("a"),
            BinaryData::mul(ctx.fa().rzi_u(2), ctx.fa().rsn("a")).to_rc_expr(),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_binary()
            .is_mul()
            .has_left_i32(3)
            .is_right_symbol_that(|s| {s.has_name("a");});

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("a + 2 * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("(1 + 2) * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("3 * a");
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_2a_add_3a() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::add(
            BinaryData::mul(ctx.fa().rzi_u(2), ctx.fa().rsn("a")).to_rc_expr(),
            BinaryData::mul(ctx.fa().rzi_u(3), ctx.fa().rsn("a")).to_rc_expr(),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_binary()
            .is_mul()
            .has_left_i32(5)
            .is_right_symbol_that(|s| {s.has_name("a");});

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("2 * a + 3 * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("(2 + 3) * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("5 * a");
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_2a_add_3a_4a() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::add(
            BinaryData::mul(ctx.fa().rzi_u(2), ctx.fa().rsn("a")).to_rc_expr(),
            BinaryData::add(
                BinaryData::mul(ctx.fa().rzi_u(3), ctx.fa().rsn("a")).to_rc_expr(),
                BinaryData::mul(ctx.fa().rzi_u(4), ctx.fa().rsn("a")).to_rc_expr(),
            ).to_rc_expr(),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_binary()
            .is_mul()
            .has_left_i32(9)
            .is_right_symbol_that(|s| {s.has_name("a");});

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("2 * a + 3 * a + 4 * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("(2 + 3 + 4) * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("(5 + 4) * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("9 * a");
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_2a_3a_add_4a() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::add(
            BinaryData::add(
                BinaryData::mul(ctx.fa().rzi_u(2), ctx.fa().rsn("a")).to_rc_expr(),
                BinaryData::mul(ctx.fa().rzi_u(3), ctx.fa().rsn("a")).to_rc_expr(),
            ).to_rc_expr(),
            BinaryData::mul(ctx.fa().rzi_u(4), ctx.fa().rsn("a")).to_rc_expr(),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_binary()
            .is_mul()
            .has_left_i32(9)
            .is_right_symbol_that(|s| {s.has_name("a");});

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("2 * a + 3 * a + 4 * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("(2 + 3 + 4) * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("(5 + 4) * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("9 * a");
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_a_3a_add_4a() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::add(
            BinaryData::add(
                ctx.fa().rsn("a"),
                BinaryData::mul(ctx.fa().rzi_u(3), ctx.fa().rsn("a")).to_rc_expr(),
            ).to_rc_expr(),
            BinaryData::mul(ctx.fa().rzi_u(4), ctx.fa().rsn("a")).to_rc_expr(),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_binary()
            .is_mul()
            .has_left_i32(8)
            .is_right_symbol_that(|s| {s.has_name("a");});

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("a + 3 * a + 4 * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("(1 + 3 + 4) * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("(4 + 4) * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("8 * a");
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_a_sub_2a() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::sub(
            ctx.fa().rsn("a"),
            BinaryData::mul(ctx.fa().rzi_u(2), ctx.fa().rsn("a")).to_rc_expr(),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("a - 2 * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("(1 - 2) * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("(-1) * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("-a");
        assert_none_step(&mut steps);

        // check result
        assert_expression_that(&result)
            .as_unary()
            .is_neg()
            .as_symbol()
            .has_name("a");
    }

    #[test]
    fn test_2a_add_3a_sub_4a() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::sub(
            BinaryData::add(
                BinaryData::mul(ctx.fa().rzi_u(2), ctx.fa().rsn("a")).to_rc_expr(),
                BinaryData::mul(ctx.fa().rzi_u(3), ctx.fa().rsn("a")).to_rc_expr(),
            ).to_rc_expr(),
            BinaryData::mul(ctx.fa().rzi_u(4), ctx.fa().rsn("a")).to_rc_expr(),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_symbol()
            .has_name("a");

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("2 * a + 3 * a - 4 * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("(2 + 3 - 4) * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("(5 - 4) * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("1 * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("a");
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_2a_add_10_sub_3a_sub_5() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::sub(
            BinaryData::sub(
                BinaryData::add(
                    BinaryData::mul(ctx.fa().rzi_u(2), ctx.fa().rsn("a")).to_rc_expr(),
                    ctx.fa().rzi_u(10)
                ).to_rc_expr(),
                BinaryData::mul(ctx.fa().rzi_u(3), ctx.fa().rsn("a")).to_rc_expr(),
            ).to_rc_expr(),
            ctx.fa().rzi_u(5),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("2 * a + 10 - 3 * a - 5");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("10 - 5 + (2 - 3) * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(2)
            .has_format("5 + (-1) * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("5 + (-a)");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("5 - a");
        assert_none_step(&mut steps);

        // check result
        assert_expression_that(&result)
            .as_binary()
            .is_sub()
            .has_left_i32(5)
            .is_right_symbol_that(|s| {s.has_name("a");});
    }

    #[test]
    fn test_2a_add_3b() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::add(
            BinaryData::mul(ctx.fa().rzi_u(2), ctx.fa().rsn("a")).to_rc_expr(),
            BinaryData::mul(ctx.fa().rzi_u(3), ctx.fa().rsn("b")).to_rc_expr(),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("2 * a + 3 * b");
        assert_none_step(&mut steps);

        // check result
        assert_expression_that(&result)
            .as_binary()
            .is_add();
    }

    #[test]
    fn test_2a_add_3a_add_6b_sub_4b() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::add(
            BinaryData::mul(ctx.fa().rzi_u(2), ctx.fa().rsn("a")).to_rc_expr(),
            BinaryData::add(
                BinaryData::mul(ctx.fa().rzi_u(3), ctx.fa().rsn("a")).to_rc_expr(),
                BinaryData::sub(
                    BinaryData::mul(ctx.fa().rzi_u(6), ctx.fa().rsn("b")).to_rc_expr(),
                    BinaryData::mul(ctx.fa().rzi_u(4), ctx.fa().rsn("b")).to_rc_expr(),
                ).to_rc_expr()
            ).to_rc_expr()
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("2 * a + 3 * a + 6 * b - 4 * b");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("(2 + 3) * a + (6 - 4) * b");
        assert_next_step_that(&mut steps)
            .has_calculation_count(2)
            .has_format("5 * a + 2 * b");
        assert_none_step(&mut steps);

        // check result
        assert_expression_that(&result)
            .as_binary()
            .is_add();
    }

    #[test]
    fn test_group_2a_add_3a_sub_group_6b_add_4b() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::sub(
            BinaryData::add(
                BinaryData::mul(ctx.fa().rzi_u(2), ctx.fa().rsn("a")).to_rc_expr(),
                BinaryData::mul(ctx.fa().rzi_u(3), ctx.fa().rsn("a")).to_rc_expr(),
            ).to_rc_expr(),
            BinaryData::add(
                BinaryData::mul(ctx.fa().rzi_u(6), ctx.fa().rsn("b")).to_rc_expr(),
                BinaryData::mul(ctx.fa().rzi_u(4), ctx.fa().rsn("b")).to_rc_expr(),
            ).to_rc_expr()
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("2 * a + 3 * a - (6 * b + 4 * b)");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("(2 + 3) * a + ((-6) - 4) * b");
        assert_next_step_that(&mut steps)
            .has_calculation_count(2)
            .has_format("5 * a + ((-6) - 4) * b");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("5 * a + (-10) * b");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("5 * a - 10 * b");
        assert_none_step(&mut steps);

        // check result
        assert_expression_that(&result)
            .as_binary()
            .is_sub();
    }

    #[test]
    fn test_group_2a_add_3a_sub_group_6b_sub_4b() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::sub(
            BinaryData::add(
                BinaryData::mul(ctx.fa().rzi_u(2), ctx.fa().rsn("a")).to_rc_expr(),
                BinaryData::mul(ctx.fa().rzi_u(3), ctx.fa().rsn("a")).to_rc_expr(),
            ).to_rc_expr(),
            BinaryData::sub(
                BinaryData::mul(ctx.fa().rzi_u(6), ctx.fa().rsn("b")).to_rc_expr(),
                BinaryData::mul(ctx.fa().rzi_u(4), ctx.fa().rsn("b")).to_rc_expr(),
            ).to_rc_expr()
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("2 * a + 3 * a - (6 * b - 4 * b)");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("(2 + 3) * a + ((-6) + 4) * b");
        assert_next_step_that(&mut steps)
            .has_calculation_count(2)
            .has_format("5 * a + ((-6) + 4) * b");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("5 * a + (-2) * b");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("5 * a - 2 * b");
        assert_none_step(&mut steps);

        // check result
        assert_expression_that(&result)
            .as_binary()
            .is_sub();
    }

    #[test]
    fn test_2of3a_add_a() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::add(
            BinaryData::mul(ctx.fa().rfi_u(2, 3), ctx.fa().rsn("a")).to_rc_expr(),
            ctx.fa().rsn("a"),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("2/3 * a + a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("(2/3 + 1) * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("(2 + 3)/3 * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("5/3 * a");
        assert_none_step(&mut steps);

        // check result
        assert_expression_that(&result)
            .as_binary()
            .is_mul()
            .is_left_fraction_that(|v| {v.is_ints(5, 3);})
            .is_right_symbol_that(|s| {s.has_name("a");});
    }

    #[test]
    fn test_1a_add_3of4a_sub_2a_add_3_add_1of2() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::add(
            BinaryData::add(
                BinaryData::sub(
                    BinaryData::add(
                        BinaryData::mul(ctx.fa().rzi_u(1), ctx.fa().rsn("a")).to_rc_expr(),
                        BinaryData::mul(ctx.fa().rfi_u(3, 4), ctx.fa().rsn("a")).to_rc_expr(),
                    ).to_rc_expr(),
                    BinaryData::mul(ctx.fa().rzi_u(2), ctx.fa().rsn("a")).to_rc_expr(),
                ).to_rc_expr(),
                ctx.fa().rzi_u(3),
            ).to_rc_expr(),
            ctx.fa().rfi_u(1, 2),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .has_format("1 * a + 3/4 * a - 2 * a + 3 + 1/2");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("3 + 1/2 + (1 + 3/4 - 2) * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(2)
            .has_format("(3 * 2 + 1)/2 + ((1 * 4 + 3)/4 - 2) * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(2)
            .has_format("(6 + 1)/2 + ((4 + 3)/4 - 2) * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(2)
            .has_format("7/2 + (7/4 - 2) * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("7/2 + (7 - 2 * 4)/4 * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("7/2 + (7 - 8)/4 * a");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("7/2 + -1/4 * a");//TODO invert sign of fraction
        assert_none_step(&mut steps);

        // check result
        assert_expression_that(&result)
            .as_binary()
            .is_add()
            .is_left_fraction_that(|v| {v.is_ints(7, 2);});
    }

}