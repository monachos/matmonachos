#[cfg(test)]
mod tests {
    use crate::calculator::context::CalcContext;
    use crate::calculator::calculate_expression;
    use crate::calculator::context::CalcContextData;
    use crate::matrix;
    use crate::testing::asserts::test_calculation_ok;
    use futures::executor::block_on;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;
    use matm_model::testing::asserts::assert_result_error_that;

    #[test]
    fn test_add_matrix_32_matrix_32() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::add(
            matrix!(ctx.fa(), 3, 2;
                1, 2,
                3, 4,
                5, 6
            ).unwrap().to_rc_expr(),
            matrix!(ctx.fa(), 3, 2;
                10, 10,
                100, 100,
                (-1), (-100)
            ).unwrap().to_rc_expr(),
        ).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(3)
            .has_columns(2)
            .is_equal_to(matrix!(ctx.fa(), 3, 2;
                11, 12,
                103, 104,
                4, (-94)
                ).unwrap());
    }

    #[test]
    fn test_add_matrix_32_matrix_23() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::add(
            matrix!(ctx.fa(), 3, 2;
                1, 2,
                3, 4,
                5, 6
            ).unwrap().to_rc_expr(),
            matrix!(ctx.fa(), 2, 3;
                10, 10, 0,
                100, 100, 4
            ).unwrap().to_rc_expr(),
        ).to_rc_expr();

        assert_result_error_that(block_on(calculate_expression(&mut ctx, &expr)))
            .has_message("Cannot use operation + for matrices of different size");
    }

    #[test]
    fn test_sub_matrix_32_matrix_32() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::sub(
            matrix!(ctx.fa(), 3, 2;
                10, 10,
                20, 30,
                100, 0
            ).unwrap().to_rc_expr(),
            matrix!(ctx.fa(), 3, 2;
                5, 1,
                4, (-1),
                0, 6
            ).unwrap().to_rc_expr(),
        ).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(3)
            .has_columns(2)
            .is_equal_to(matrix!(ctx.fa(), 3, 2;
                    5, 9,
                    16, 31,
                    100, (-6)
                ).unwrap());
    }
 
    #[test]
    fn test_sub_matrix_32_matrix_23() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::sub(
            matrix!(ctx.fa(), 3, 2;
                1, 2,
                3, 4,
                5, 6
            ).unwrap().to_rc_expr(),
            matrix!(ctx.fa(), 2, 3;
                10, 10, 0,
                100, 100, 4
            ).unwrap().to_rc_expr(),
        ).to_rc_expr();

        assert_result_error_that(block_on(calculate_expression(&mut ctx, &expr)))
            .has_message("Cannot use operation - for matrices of different size");
    }

    #[test]
    fn test_mul_matrix_32_matrix_23() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::mul(
            matrix!(ctx.fa(), 3, 2;
                1, 2,
                3, 4,
                5, 6
            ).unwrap().to_rc_expr(),
            matrix!(ctx.fa(), 2, 3;
                2, 2, 100,
                3, 1, 10
            ).unwrap().to_rc_expr(),
        ).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(3)
            .has_columns(3)
            .is_equal_to(matrix!(ctx.fa(), 3, 3;
                    8, 4, 120,
                    18, 10, 340,
                    28, 16, 560
                ).unwrap());
    }

    #[test]
    fn test_mul_matrix_23_matrix_31() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::mul(
            matrix!(ctx.fa(), 2, 3;
                1, 2, 3,
                4, 5, 6
            ).unwrap().to_rc_expr(),
            matrix!(ctx.fa(), 3, 1;
                1,
                2,
                3
            ).unwrap().to_rc_expr(),
        ).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(2)
            .has_columns(1)
            .is_equal_to(matrix!(ctx.fa(), 2, 1;
                    14,
                    32
                ).unwrap());
    }

    #[test]
    fn test_mul_matrix_32_matrix_31() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::mul(
            matrix!(ctx.fa(), 3, 2;
                1, 2,
                3, 4,
                5, 6
            ).unwrap().to_rc_expr(),
            matrix!(ctx.fa(), 3, 1;
                10,
                100,
                100
            ).unwrap().to_rc_expr(),
        ).to_rc_expr();

        assert_result_error_that(block_on(calculate_expression(&mut ctx, &expr)))
            .has_message("Cannot multiple matrices of size 3x2 and 3x1");
    }

    #[test]
    fn test_div_matrix_32_matrix_31() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::div(
            matrix!(ctx.fa(), 3, 2;
                1, 2,
                3, 4,
                5, 6
            ).unwrap().to_rc_expr(),
            matrix!(ctx.fa(), 3, 1;
                10,
                100,
                100
            ).unwrap().to_rc_expr(),
        ).to_rc_expr();

        assert_result_error_that(block_on(calculate_expression(&mut ctx, &expr)))
            .has_message("Not supported operation div for two matrices");
    }
}