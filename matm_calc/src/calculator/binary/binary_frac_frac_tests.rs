#[cfg(test)]
mod tests {
    use crate::calculator::context::CalcContext;
    use crate::calculator::calculate_expression;
    use crate::calculator::context::CalcContextData;
    use crate::result::dump_unicode_color;
    use crate::testing::asserts::assert_next_step;
    use crate::testing::asserts::assert_next_step_that;
    use crate::testing::asserts::assert_none_step;
    use futures::executor::block_on;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;
    use rstest::rstest;


    #[rstest]
    fn test_add_frac_frac_eq_denominators() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::add(
            ctx.fa().rfi_u(2, 5),
            ctx.fa().rfi_u(3, 5),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(1);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .has_numerator_format("2 + 3")
            .has_denominator_int(5);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .is_ints(5, 5);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_integer()
            .is_int(1);
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_add_frac_frac_neq_denominators() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::add(
            ctx.fa().rfi_u(2, 5),
            ctx.fa().rfi_u(3, 4),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_fraction()
            .is_ints(23, 20);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .has_numerator_format("2 * 4 + 3 * 5")
            .has_denominator_format("5 * 4");
        assert_next_step_that(&mut steps)
            .has_calculation_count(3)
            .as_fraction()
            .has_numerator_format("8 + 15")
            .has_denominator_int(20);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .is_ints(23, 20);
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_sub_frac_frac_eq_denominators() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::sub(
            ctx.fa().rfi_u(2, 5),
            ctx.fa().rfi_u(3, 5),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_fraction()
            .is_ints(-1, 5);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .has_numerator_format("2 - 3")
            .has_denominator_int(5);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .is_ints(-1, 5);
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_sub_frac_frac_neq_denominators() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::sub(
            ctx.fa().rfi_u(2, 5),
            ctx.fa().rfi_u(3, 4),
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_fraction()
            .is_ints(-7, 20);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .has_numerator_format("2 * 4 - 3 * 5")
            .has_denominator_format("5 * 4");
        assert_next_step_that(&mut steps)
            .has_calculation_count(3)
            .as_fraction()
            .has_numerator_format("8 - 15")
            .has_denominator_int(20);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .is_ints(-7, 20);
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_mul_frac_frac() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::mul(
            ctx.fa().rfi_u(5, 2),
            ctx.fa().rfi_u(6, 5)
        ).to_rc_expr();
        
        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(3);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .has_numerator_format("5 * 6")
            .has_denominator_format("2 * 5");
        assert_next_step_that(&mut steps)
            .has_calculation_count(2)
            .as_fraction()
            .is_ints(30, 10);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_integer()
            .is_int(3);
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_div_frac_frac() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::div(
            ctx.fa().rfi_u(5, 2),
            ctx.fa().rfi_u(5, 6)
        ).to_rc_expr();
        
        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(3);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_fraction()
            .has_numerator_format("5 * 6")
            .has_denominator_format("2 * 5");
        assert_next_step_that(&mut steps)
            .has_calculation_count(2)
            .as_fraction()
            .is_ints(30, 10);
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_integer()
            .is_int(3);
        assert_none_step(&mut steps);
    }

}