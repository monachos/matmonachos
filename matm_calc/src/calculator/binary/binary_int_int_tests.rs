#[cfg(test)]
mod tests {
    use crate::calculator::calculate_expression;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::result::dump_unicode_color;
    use futures::executor::block_on;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;

    #[test]
    fn test_add_int_int() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::add(
            ctx.fa().rzi_u(2),
            ctx.fa().rzi_u(3)
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(5);

        // check steps
        assert_eq!(1, ctx.explanation.calculated_count());
    }

    #[test]
    fn test_sub_int_int() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::sub(
            ctx.fa().rzi_u(2),
            ctx.fa().rzi_u(3)
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(-1);

        // check steps
        assert_eq!(1, ctx.explanation.calculated_count());
    }

    #[test]
    fn test_mul_int_int() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::mul(
            ctx.fa().rzi_u(2),
            ctx.fa().rzi_u(3)
        ).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(6);

        // check steps
        assert_eq!(1, ctx.explanation.calculated_count());
        
    }

    #[test]
    fn test_div_int_int() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = BinaryData::div(ctx.fa().rzi_u(2), ctx.fa().rzi_u(3)).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_fraction()
            .is_ints(2, 3);

        // check steps
        assert_eq!(1, ctx.explanation.calculated_count());
    }
}