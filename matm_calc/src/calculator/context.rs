use matm_model::factories::ObjFactory;
use crate::result::ResultContainer;
use crate::rules::CalcRules;

pub struct CalcContext<'a> {
    pub rules: &'a CalcRules,
    factory: ObjFactory,
    pub future_rules: &'a mut CalcRules,
    pub explanation: &'a mut ResultContainer,
}

impl<'a> CalcContext<'a> {
    pub fn new(
        rules: &'a CalcRules,
        future_rules: &'a mut CalcRules,
        explanation: &'a mut ResultContainer,
    ) -> Self {
        let factory = ObjFactory::new(rules.radix);
        Self {
            rules,
            factory,
            future_rules,
            explanation,
        }
    }

    pub fn from_data(data: &'a mut CalcContextData) -> Self {
        let factory = ObjFactory::new(data.rules.radix);
        Self {
            rules: &data.rules,
            factory,
            future_rules: &mut data.future_rules,
            explanation: &mut data.explanation,
        }
    }

    pub fn rules(&self) -> &CalcRules {
        &self.rules
    }

    pub fn fa(&self) -> &ObjFactory {
        &self.factory
    }
}

//-------------------------------------------------------------

pub struct CalcContextData {
    pub rules: CalcRules,
    pub future_rules: CalcRules,
    pub explanation: ResultContainer,
}

impl CalcContextData {
    pub fn default() -> Self {
        Self {
            rules: CalcRules::default(),
            future_rules: CalcRules::default(),
            explanation: ResultContainer::new(),
        }
    }

    pub fn from_rules(rules: &CalcRules) -> Self {
        Self {
            rules: rules.clone(),
            future_rules: rules.clone(),
            explanation: ResultContainer::new(),
        }
    }

    pub fn with_rules(rules_handler: impl Fn(&mut CalcRules)) -> Self {
        let mut rules = CalcRules::default();
        rules_handler(&mut rules);
        Self {
            rules: rules.clone(),
            future_rules: rules,
            explanation: ResultContainer::new(),
        }
    }
}
