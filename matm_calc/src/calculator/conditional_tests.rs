#[cfg(test)]
mod tests {
    use futures::executor::block_on;

    use matm_model::expressions::arithmetic_condition::ArithmeticCondition;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::condition::ToCondition;
    use matm_model::expressions::conditional::ConditionalClause;
    use matm_model::expressions::conditional::ConditionalData;
    use matm_model::expressions::logic_condition::LogicCondition;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;
    use matm_model::testing::asserts::assert_result_error_that;

    use crate::calculator::calculate_expression;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::result::dump_unicode_color;
    use crate::testing::asserts::assert_next_step;
    use crate::testing::asserts::assert_next_step_that;
    use crate::testing::asserts::assert_none_step;
    use crate::testing::asserts::test_calculation_with_explanation_ok;

    #[test]
    fn test_conditional_none_matched() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = ConditionalData::from_vector(
            vec![
                ConditionalClause {
                    expression: BinaryData::add(
                        ctx.fa().rzi_u(1),
                        ctx.fa().rzi_u(2)
                    ).to_rc_expr(),
                    condition: ArithmeticCondition::gt(
                        BinaryData::mul(
                            BinaryData::mul(ctx.fa().rzi_u(2), ctx.fa().rzi_u(3)).to_rc_expr(),
                            ctx.fa().rsn("x")
                        ).to_rc_expr(),
                        ctx.fa().rz0()
                    ).to_rc_cond()
                },
                ConditionalClause {
                    expression: BinaryData::mul(ctx.fa().rzi_u(4), ctx.fa().rzi_u(5)).to_rc_expr(),
                    condition: ArithmeticCondition::eq(
                        BinaryData::mul(
                            BinaryData::add(ctx.fa().rzi_u(6), ctx.fa().rzi_u(2)).to_rc_expr(),
                            ctx.fa().rsn("x")
                        ).to_rc_expr(),
                        ctx.fa().rz0()
                    ).to_rc_cond()
                },
            ]).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_conditional();

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step(&mut steps);
        assert_next_step_that(&mut steps)
            .has_calculation_count(2)
            .as_conditional()
            .has_i_expression_format(0, "1 + 2")
            .has_i_condition_format(0, "6 * x > 0")
            .has_i_expression_format(1, "4 * 5")
            .has_i_condition_format(1, "8 * x = 0");
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_conditional_one_matched() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = ConditionalData::from_vector(
            vec![
                ConditionalClause {
                    expression: BinaryData::add(ctx.fa().rzi_u(1), ctx.fa().rzi_u(2)).to_rc_expr(),
                    condition: ArithmeticCondition::gt(
                        ctx.fa().rzi_u(2),
                        ctx.fa().rz0()
                    ).to_rc_cond()
                },
                ConditionalClause {
                    expression: BinaryData::mul(ctx.fa().rzi_u(4), ctx.fa().rzi_u(5)).to_rc_expr(),
                    condition: ArithmeticCondition::eq(
                        ctx.fa().rzi_u(2),
                        ctx.fa().rz0()
                    ).to_rc_cond()
                },
            ]).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr)).unwrap();

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(3);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .as_conditional()
            .has_i_expression_format(0, "1 + 2")
            .has_i_condition_format(0, "2 > 0")
            .has_i_expression_format(1, "4 * 5")
            .has_i_condition_format(1, "2 = 0");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .has_format("1 + 2");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_integer()
            .is_int(3);
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_conditional_many_matched() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = ConditionalData::from_vector(
            vec![
                ConditionalClause {
                    expression: BinaryData::add(ctx.fa().rzi_u(1), ctx.fa().rzi_u(2)).to_rc_expr(),
                    condition: ArithmeticCondition::gt(
                        ctx.fa().rzi_u(2),
                        ctx.fa().rz0()
                    ).to_rc_cond()
                },
                ConditionalClause {
                    expression: BinaryData::mul(ctx.fa().rzi_u(4), ctx.fa().rzi_u(5)).to_rc_expr(),
                    condition: ArithmeticCondition::eq(
                        ctx.fa().rz0(),
                        ctx.fa().rz0()
                    ).to_rc_cond()
                },
            ]).to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr));

        dump_unicode_color(&ctx.explanation);

        // check result
        assert_result_error_that(result)
            .has_message("Only one condition can be met, conditions that are true: 1 + 2, 2 > 0 at 1, 4 * 5, 0 = 0 at 2.");

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .as_conditional()
            .has_i_expression_format(0, "1 + 2")
            .has_i_condition_format(0, "2 > 0")
            .has_i_expression_format(1, "4 * 5")
            .has_i_condition_format(1, "0 = 0");
        assert_none_step(&mut steps);
    }

    #[test]
    fn test_conditional_logic_condition() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = ConditionalData::from_vector(
            vec![
                ConditionalClause {
                    expression: ctx.fa().rzi_u(1),
                    condition: LogicCondition::or(
                        ArithmeticCondition::gt(
                            BinaryData::add(ctx.fa().rzi_u(1), ctx.fa().rzi_u(2)).to_rc_expr(),
                            ctx.fa().rz0()
                        ).to_rc_cond(),
                        ArithmeticCondition::lt(
                            BinaryData::add(ctx.fa().rzi_u(3), ctx.fa().rzi_u(4)).to_rc_expr(),
                            ctx.fa().rz0()
                        ).to_rc_cond()
                    ).to_rc_cond()
                },
                ConditionalClause {
                    expression: ctx.fa().rzi_u(0),
                    condition: LogicCondition::and(
                        ArithmeticCondition::gt(
                            BinaryData::add(ctx.fa().rzi_u(5), ctx.fa().rzi_u(6)).to_rc_expr(),
                            ctx.fa().rz0()
                        ).to_rc_cond(),
                        ArithmeticCondition::lt(
                            BinaryData::add(ctx.fa().rzi_u(7), ctx.fa().rzi_u(8)).to_rc_expr(),
                            ctx.fa().rz0()
                        ).to_rc_cond()
                    ).to_rc_cond()
                },
            ]).to_rc_expr();

        let result = test_calculation_with_explanation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(1);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .has_calculation_count(0)
            .as_conditional()
            .has_i_expression_format(0, "1")
            .has_i_condition_format(0, "1 + 2 > 0 or 3 + 4 < 0")
            .has_i_expression_format(1, "0")
            .has_i_condition_format(1, "5 + 6 > 0 and 7 + 8 < 0");
        assert_next_step_that(&mut steps)
            .has_calculation_count(4)
            .as_conditional()
            .has_i_expression_format(0, "1")
            .has_i_condition_format(0, "3 > 0 or 7 < 0")
            .has_i_expression_format(1, "0")
            .has_i_condition_format(1, "11 > 0 and 15 < 0");
        assert_next_step_that(&mut steps)
            .has_calculation_count(1)
            .as_integer()
            .is_int(1);
        assert_none_step(&mut steps);
    }
}
