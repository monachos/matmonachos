use crate::calculator::CalcResult;
use crate::calculator::ModelData;
use crate::calculator::calculate_expression_step;
use crate::calculator::CalcResultOrErr;
use crate::calculator::context::CalcContext;
use matm_model::expressions::soe::SysOfEqData;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;
use std::rc::Rc;

pub async fn calculate_system_of_equations(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, SysOfEqData>,
) -> CalcResultOrErr {
    let mut calc_equations: Vec<Rc<Expression>> = Vec::with_capacity(model.data.equations.len());
    let mut total_calculated: usize = 0;
    for eq in &model.data.equations {
        let res: CalcResult<Rc<Expression>> = calculate_expression_step(ctx, eq).await?;
        total_calculated += res.calculated_count;
        calc_equations.push(res.object);
    }
    if total_calculated > 0 {
        return match calc_equations.len() {
            1 => {
                // reduce equation system -> equation
                Ok(CalcResult::new(
                    Rc::clone(&calc_equations[0]),
                    total_calculated))
            }
            _ => {
                Ok(CalcResult::new(
                    SysOfEqData::from_vector(calc_equations).to_rc_expr(),
                    total_calculated))
            }
        }
    }

    match model.data.equations.len() {
        1 => {
            // reduce equation system -> equation
            return Ok(CalcResult::one(
                Rc::clone(&model.data.equations[0])));
        }
        _ => {
            //TODO solve
        }
    }

    Ok(CalcResult::original(Rc::clone(&model.orig)))
}

