#[cfg(test)]
mod tests {
    use crate::calculator::factorization::calculate_trial_division;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::result::dump_unicode_color;
    use futures::executor::block_on;
    use matm_model::expressions::int::IntData;
    use matm_model::factories::ObjFactory;
    use matm_model::testing::asserts::assert_result_ok;

    fn run_trial_division(value: i32) -> Vec<IntData> {
        let mut ctx_data = CalcContextData::with_rules(|rules| {
            rules.explain_int_factorization = true;
        });
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let input = ctx.fa().zi_u(value);
        let result = block_on(calculate_trial_division(&mut ctx, &input));

        dump_unicode_color(&ctx.explanation);

        return assert_result_ok(result);
    }

    #[test]
    fn test_trial_division_1() {
        let factors = run_trial_division(1);

        let mut it = factors.iter();
        // 1
        assert_eq!(it.next(), None);
    }

    #[test]
    fn test_trial_division_2() {
        let fa = ObjFactory::new10();
        
        let factors = run_trial_division(2);

        let mut it = factors.iter();
        // 2
        let int2 = fa.zi_u(2);
        assert_eq!(it.next(), Some(&int2));
        // 1
        assert_eq!(it.next(), None);
    }

    #[test]
    fn test_trial_division_23() {
        let fa = ObjFactory::new10();
        
        let factors = run_trial_division(23);

        let mut it = factors.iter();
        // 23
        let int23 = fa.zi_u(23);
        assert_eq!(it.next(), Some(&int23));
        // 1
        assert_eq!(it.next(), None);
    }

    #[test]
    fn test_trial_division_24() {
        let fa = ObjFactory::new10();
        
        let factors = run_trial_division(24);

        let mut it = factors.iter();
        // 24
        let int2 = fa.zi_u(2);
        assert_eq!(it.next(), Some(&int2));
        // 12
        assert_eq!(it.next(), Some(&int2));
        // 6
        assert_eq!(it.next(), Some(&int2));
        // 3
        let int3 = fa.zi_u(3);
        assert_eq!(it.next(), Some(&int3));
        // 1
        assert_eq!(it.next(), None);
    }

    #[test]
    fn test_trial_division_m1() {
        let fa = ObjFactory::new10();
        
        let factors = run_trial_division(-1);

        let mut it = factors.iter();
        // -1
        let intm1 = fa.zi_u(-1);
        assert_eq!(it.next(), Some(&intm1));
        // 1
        assert_eq!(it.next(), None);
    }

    #[test]
    fn test_trial_division_m2() {
        let fa = ObjFactory::new10();
        
        let factors = run_trial_division(-2);

        let mut it = factors.iter();
        // -2
        let int2 = fa.zi_u(2);
        assert_eq!(it.next(), Some(&int2));
        // -1
        let intm1 = fa.zi_u(-1);
        assert_eq!(it.next(), Some(&intm1));
        // 1
        assert_eq!(it.next(), None);
    }

    #[test]
    fn test_trial_division_m23() {
        let fa = ObjFactory::new10();
        
        let factors = run_trial_division(-23);

        let mut it = factors.iter();
        // -23
        let int23 = fa.zi_u(23);
        assert_eq!(it.next(), Some(&int23));
        // -1
        let intm1 = fa.zi_u(-1);
        assert_eq!(it.next(), Some(&intm1));
        // 1
        assert_eq!(it.next(), None);
    }

    #[test]
    fn test_trial_division_m24() {
        let fa = ObjFactory::new10();
        
        let factors = run_trial_division(-24);

        let mut it = factors.iter();
        // -24
        let int2 = fa.zi_u(2);
        assert_eq!(it.next(), Some(&int2));
        // -12
        assert_eq!(it.next(), Some(&int2));
        // -6
        assert_eq!(it.next(), Some(&int2));
        // -3
        let int3 = fa.zi_u(3);
        assert_eq!(it.next(), Some(&int3));
        // -1
        let intm1 = fa.zi_u(-1);
        assert_eq!(it.next(), Some(&intm1));
        // 1
        assert_eq!(it.next(), None);
    }

    #[test]
    fn test_trial_division_0() {
        let fa = ObjFactory::new10();
        
        let factors = run_trial_division(0);

        let mut it = factors.iter();
        // 0
        let zero = fa.zi_u(0);
        assert_eq!(it.next(), Some(&zero));
        assert_eq!(it.next(), None);
    }
}