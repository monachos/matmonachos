use std::cmp::Ordering;
use std::rc::Rc;

use crate::calculator::calculate_expression_step;
use crate::calculator::ModelData;
use crate::calculator::CalcResultOrErr;
use crate::calculator::CalcResult;
use crate::calculator::context::CalcContext;
use crate::comparator::compare_expressions;
use matm_error::error::CalcError;
use matm_model::expressions::matrices::MatrixData;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;


pub async fn calculate_matrix(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, MatrixData>,
) -> CalcResultOrErr {
    // calculate all matrix elements
    let mut elements: Vec<Rc<Expression>> = Vec::new();
    let mut total_calculated = 0;
    for idx in model.data.iter_right_down() {
        let el = model.data.get(&idx)?;
        let el_value = calculate_expression_step(ctx, el).await?;
        total_calculated += el_value.calculated_count;
        elements.push(el_value.object);
    }
 
    // at least element is calculated
    if total_calculated > 0 {
        let new_mx = MatrixData::from_vector(model.data.rows(), model.data.columns(), elements)?;
        let expr = new_mx.to_rc_expr();
        return Ok(CalcResult::new(expr, total_calculated));
    }
 
    Ok(CalcResult::original(Rc::clone(&model.orig)))
}

pub async fn is_positive_definite_matrix(
    ctx: &mut CalcContext<'_>,
    mx: &MatrixData,
) -> Result<Option<bool>, CalcError> {
    if !mx.is_square() {
        return Ok(None);
    }
    let zero = ctx.fa().ez0();
    for i in mx.iter_rows() {
        let element = mx.get_i_i(i, i).unwrap();
        match compare_expressions(ctx, element, &zero).await? {
            Some(cmp) => {
                match cmp {
                    Ordering::Equal | Ordering::Less => {
                        return Ok(Some(false));
                    }
                    _ => {
                    }
                }
            }
            None => {
                return Ok(None);
            }
        }
    }
    return Ok(Some(true));
}

