use std::rc::Rc;

use matm_model::expressions::assignment::AssignmentData;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;

use crate::calculator::CalcResult;
use crate::calculator::ModelData;
use crate::calculator::calculate_expression_step;
use crate::calculator::CalcResultOrErr;
use crate::calculator::context::CalcContext;

pub async fn calculate_assignment(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, AssignmentData>,
) -> CalcResultOrErr {
    // target
    let step = calculate_expression_step(ctx, &model.data.source).await?;
    if step.calculated_count > 0 {
        let expr = AssignmentData::new(
            Rc::clone(&model.data.target),
            step.object).to_rc_expr();
        return Ok(CalcResult::new(expr, step.calculated_count));
    }

    //TODO execute

    return Ok(CalcResult::original(Rc::clone(model.orig)));
}