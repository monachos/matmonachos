#[cfg(test)]
mod tests {
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::testing::asserts::test_calculation_ok;
    use matm_model::expressions::assignment::AssignmentData;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;

    #[test]
    fn test_calculate_assignment_of_binary() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = AssignmentData::new(
            ctx.fa().rsn("x"),
            BinaryData::add(ctx.fa().rzi_u(4), ctx.fa().rzi_u(5)).to_rc_expr(),
        )
        .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        assert_expression_that(&result)
            .as_assignment()
            .is_target_symbol_that(|e| {e.has_name("x");})
            .is_source_integer_that(|e| {e.is_int(9);});
    }
}
