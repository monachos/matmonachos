#[cfg(test)]
mod tests {
    use futures::executor::block_on;
    use std::rc::Rc;

    use crate::calculator::calculate_expression;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::result::dump_unicode_color;
    use crate::rules::CalcRules;
    use crate::testing::asserts::assert_next_step_that;
    use crate::testing::asserts::assert_none_step;
    use matm_model::expressions::root::RootData;
    use matm_model::expressions::Expression;
    use matm_model::expressions::ToExpression;
    use matm_model::factories::ObjFactory;
    // use structured_logger::Builder;
    use matm_model::testing::asserts::assert_expression_that;
    use matm_model::testing::asserts::assert_result_ok;
    use rstest::rstest;

    fn run_root(rules: CalcRules, radicand: Rc<Expression>, degree: Rc<Expression>) -> Rc<Expression> {
        let mut ctx_data = CalcContextData::from_rules(&rules);
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = RootData::new(radicand, degree).to_rc_expr();
        let result = block_on(calculate_expression(&mut ctx, &expr));

        println!("Testing: {}", expr);
        dump_unicode_color(&ctx.explanation);
        println!();
        println!("-------------");

        // check result
        return assert_result_ok(result);
    }

    #[rstest]
    #[case(0, 0)]
    #[case(1, 1)]
    #[case(4, 2)]
    #[case(9, 3)]
    fn test_square_root_int(
        #[case] input: i32,
        #[case] expected: i32,
    ) {
        let rules = CalcRules::default();
        let fa = ObjFactory::new(rules.radix);

        let result = run_root(
            rules,
            fa.rzi_u(input),
            fa.rzi_u(2));

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(expected);
    }

    #[test]
    fn test_square_root_bigint() {
        let rules = CalcRules::default();
        let fa = ObjFactory::new(rules.radix);

        let result = run_root(
            rules,
            fa.rzs_u("10000000000000000000000000000000000000"),
            fa.rzi_u(2));

        // check result
        println!("{}", result);
        //TODO this should be reduced
    }

    #[test]
    fn test_bigint_root_int() {
        // Builder::with_level("debug")
        //     .init();

        let rules = CalcRules::default();
        let fa = ObjFactory::new(rules.radix);

        let result = run_root(
            rules,
            fa.rzi_u(5),
            fa.rzs_u("10000000000000000000000000000000000000"));

        // check result
        assert_expression_that(&result)
            .as_root()
            .is_radicand_integer_that(|e| {e.is_int(5);})
            .has_degree_that(|r| {r.as_integer().is_positive();});
    }

    #[test]
    fn test_2_2_root_int() {
        let rules = CalcRules::default();
        let fa = ObjFactory::new(rules.radix);

        let result = run_root(
            rules,
            fa.rzi_u(2),
            fa.rzi_u(2));
        assert_expression_that(&result)
            .as_root()
            .is_sqrt_i32(2);
    }

    #[test]
    fn test_6_2_root_int() {
        let rules = CalcRules::default();
        let fa = ObjFactory::new(rules.radix);

        let result = run_root(
            rules,
            fa.rzi_u(6),
            fa.rzi_u(2));
        assert_expression_that(&result)
            .as_root()
            .is_sqrt_i32(6);
    }

    #[test]
    #[ignore = "infty loop"]
    fn test_10_2_root_int() {
        let rules = CalcRules::default();
        let fa = ObjFactory::new(rules.radix);

        let result = run_root(
            rules,
            fa.rzi_u(10),
            fa.rzi_u(2));
        assert_expression_that(&result)
            .as_root()
            .is_sqrt_i32(10);
    }

    #[test]
    fn test_3_root_int() {
        let rules = CalcRules::default();
        let fa = ObjFactory::new(rules.radix);

        let result = run_root(
            rules,
            fa.rzi_u(8),
            fa.rzi_u(3));
        assert_expression_that(&result)
            .as_integer()
            .is_int(2);
    }

    #[test]
    fn test_2_root_20() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = RootData::new(ctx.fa().rzi_u(20), ctx.fa().rzi_u(2)).to_rc_expr();
        let result = block_on(calculate_expression(&mut ctx, &expr));

        dump_unicode_color(&ctx.explanation);
        println!();
        println!("-------------");

        // check result
        assert_result_ok(result);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        // sqrt(20)
        assert_next_step_that(&mut steps)
            .as_root()
            .is_radicand_integer_that(|r| { r.is_int(20); })
            .is_degree_integer_that(|d| { d.is_int(2); });
        // sqrt(2 * 2 * 5)
        assert_next_step_that(&mut steps)
            .as_root()
            .has_radicand_that(|r| {
                r.as_binary()
                    .has_left_that(|e12| {
                        e12.as_binary()
                            .has_left_i32(2)
                            .has_right_i32(2);
                    })
                    .has_right_i32(5);
            })
            .is_degree_integer_that(|d| { d.is_int(2); });
        // sqrt(2^2 * 5)
        assert_next_step_that(&mut steps)
            .as_root()
            .has_radicand_that(|r| {
                r.as_binary()
                    .has_left_that(|left| {
                        left.as_exponent()
                            .is_base_integer_that(|e| { e.is_int(2); })
                            .is_exponent_integer_that(|e| { e.is_int(2); });
                    })
                    .has_right_that(|right| {
                        right.as_integer().is_int(5);
                    });
            })
            .is_degree_integer_that(|e| { e.is_int(2); });

        // sqrt(2^2) * sqrt(5)
        assert_next_step_that(&mut steps)
            .as_binary()
            .has_left_that(|left| {
                left.as_root()
                    .has_radicand_that(|r| {
                        r.as_exponent()
                            .is_base_integer_that(|e| { e.is_int(2); })
                            .is_exponent_integer_that(|e| { e.is_int(2); });
                    });
            })
            .has_right_that(|right| {
                right.as_root().is_sqrt_i32(5);
            });

        // 2 * sqrt(5)
        assert_next_step_that(&mut steps)
            .as_binary()
            .has_left_that(|left| {
                left.as_integer().is_int(2);
            })
            .has_right_that(|right| {
                right.as_root().is_sqrt_i32(5);
            });

        assert_none_step(&mut steps);
    }

    #[test]
    fn test_2_root_4() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = RootData::new(ctx.fa().rzi_u(4), ctx.fa().rzi_u(2)).to_rc_expr();
        let result = block_on(calculate_expression(&mut ctx, &expr));

        dump_unicode_color(&ctx.explanation);
        println!();
        println!("-------------");

        // check result
        assert_result_ok(result);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        // sqrt(4)
        assert_next_step_that(&mut steps)
            .as_root()
            .is_radicand_integer_that(|r| {r.is_int(4);})
            .is_degree_integer_that(|d| {d.is_int(2);});
        // sqrt(2 * 2)
        assert_next_step_that(&mut steps)
            .as_root()
            .has_radicand_that(|r| {
                r.as_binary()
                    .has_left_i32(2)
                    .has_right_i32(2);
                })
            .is_degree_integer_that(|d| {d.is_int(2);});
        // sqrt(2^2)
        assert_next_step_that(&mut steps)
            .as_root()
            .has_radicand_that(|r| {
                    r.as_exponent()
                    .is_base_integer_that(|e|{e.is_int(2);})
                    .is_exponent_integer_that(|e|{e.is_int(2);});
            })
            .is_degree_integer_that(|e|{e.is_int(2);});

        // 2
        assert_next_step_that(&mut steps)
            .as_integer()
            .is_int(2);

        assert_none_step(&mut steps);
    }

    #[test]
    fn test_2_root_8() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = RootData::new(ctx.fa().rzi_u(8), ctx.fa().rzi_u(2)).to_rc_expr();
        let result = block_on(calculate_expression(&mut ctx, &expr));

        dump_unicode_color(&ctx.explanation);
        println!();
        println!("-------------");

        // check result
        assert_result_ok(result);

        // check steps
        let mut steps = ctx.explanation.flat_expressions();
        assert_next_step_that(&mut steps)
            .as_root()
            .is_radicand_i32(8)
            .is_degree_i32(2);
        assert_next_step_that(&mut steps)
            .as_root()
            .has_radicand_format("2 * 2 * 2")
            .is_degree_i32(2);
        assert_next_step_that(&mut steps)
            .as_root()
            .has_radicand_format("2 ^ 2 * 2")
            .is_degree_i32(2);
        assert_next_step_that(&mut steps)
            .as_binary()
            .is_mul()
            .has_left_that(|e| {
                e.as_root()
                    .has_radicand_format("2 ^ 2")
                    .is_degree_i32(2);
            })
            .has_right_that(|e| {
                e.as_root().is_sqrt_i32(2);
            });
        assert_next_step_that(&mut steps)
            .as_binary()
            .is_mul()
            .has_left_i32(2)
            .has_right_that(|e| {
                e.as_root().is_sqrt_i32(2);
            });
        assert_none_step(&mut steps);
    }
}