#[cfg(test)]
mod tests {
    use matm_model::expressions::func::FunctionData;
    use matm_model::expressions::matrices::MatrixData;
    use matm_model::expressions::method::MethodData;
    use matm_model::expressions::object::DetConfigData;
    use matm_model::expressions::object::InvConfigData;
    use matm_model::expressions::object::ObjectData;
    use matm_model::expressions::object::ObjectType::DetConfig;
    use matm_model::expressions::object::ObjectType::InvConfig;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;
    use matm_model::testing::asserts::assert_result_error_that;

    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::matrix;
    use crate::testing::asserts::test_calculation;
    use crate::testing::asserts::test_calculation_ok;

    #[test]
    fn test_mx_zeros_det() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = MatrixData::zeros(ctx.rules().radix, 4, 4).unwrap().to_rc_expr();
        let expr = MethodData::new(
            a,
            "det".to_owned(),
            vec![]).to_rc_expr();
        
        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(0);
    }

    #[test]
    fn test_mx_33_det() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            4, 1, 6,
            1, 3, 3
        ).unwrap().to_rc_expr();
        let expr = MethodData::new(
            a,
            "det".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(6);
    }

    #[test]
    fn test_mx_33_det_laplace() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            4, 1, 6,
            1, 3, 3
        ).unwrap().to_rc_expr();
        let config = MethodData::new(
            FunctionData::new("det_config".to_owned(), vec![]).to_rc_expr(),
            "laplace".to_owned(),
            vec![]).to_rc_expr();
        let expr = MethodData::new(
            a,
            "det".to_owned(),
            vec![config]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(6);
    }

    #[test]
    fn test_mx_33_det_bareiss() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            4, 1, 6,
            1, 3, 3
        ).unwrap().to_rc_expr();
        let config = MethodData::new(
            FunctionData::new("det_config".to_owned(), vec![]).to_rc_expr(),
            "bareiss".to_owned(),
            vec![]).to_rc_expr();
        let expr = MethodData::new(
            a,
            "det".to_owned(),
            vec![config]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(6);
    }

    #[test]
    fn test_mx_det_invalid_arg() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = MatrixData::ones(ctx.rules().radix, 3, 3).unwrap().to_rc_expr();
        let config = ObjectData::new(InvConfig(InvConfigData::new())).to_rc_expr();
        let expr = MethodData::new(
            a,
            "det".to_owned(),
            vec![config]).to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        assert_result_error_that(result)
            .has_message("Invalid argument passed to the det() method. Expected object {det_config}, found object {inv_config}.");
    }

    // Example from "Algebra liniowa w zadaniach", Jerzy Rutkowski, PWN 2008, page 80
    #[test]
    fn test_mx_33_inv() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            4, 5, 7,
            1, 2, 4,
            2, 3, 6).unwrap().to_rc_expr();
        let expr = MethodData::new(
            a,
            "inv".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        let exp_inv = matrix!(ctx.fa(), 3, 3;
            0, (-3), 2,
            (2/3), (10/3), (-3),
            (-1/3), (-2/3), 1).unwrap();
        assert_expression_that(&result)
            .as_matrix()
            .is_equal_to(exp_inv);
    }

    // Example from "Algebra liniowa w zadaniach", Jerzy Rutkowski, PWN 2008, page 80
    #[test]
    fn test_mx_33_inv_adjugate() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            4, 5, 7,
            1, 2, 4,
            2, 3, 6).unwrap().to_rc_expr();
        let config = MethodData::new(
            FunctionData::new("inv_config".to_owned(), vec![]).to_rc_expr(),
            "adj".to_owned(),
            vec![]).to_rc_expr();
        let expr = MethodData::new(
            a,
            "inv".to_owned(),
            vec![config]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        let exp_inv = matrix!(ctx.fa(), 3, 3;
            0, (-3), 2,
            (2/3), (10/3), (-3),
            (-1/3), (-2/3), 1).unwrap();
        assert_expression_that(&result)
            .as_matrix()
            .is_equal_to(exp_inv);
    }

    // Example from "Algebra liniowa w zadaniach", Jerzy Rutkowski, PWN 2008, page 80
    #[test]
    fn test_mx_33_inv_gauss() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            4, 5, 7,
            1, 2, 4,
            2, 3, 6).unwrap().to_rc_expr();
        let config = MethodData::new(
            FunctionData::new("inv_config".to_owned(), vec![]).to_rc_expr(),
            "gauss".to_owned(),
            vec![]).to_rc_expr();
        let expr = MethodData::new(
            a,
            "inv".to_owned(),
            vec![config]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        let expected = matrix!(ctx.fa(), 3, 3;
            0, (-3), 2,
            (2/3), (10/3), (-3),
            (-1/3), (-2/3), 1).unwrap();
        assert_expression_that(&result)
            .as_matrix()
            .is_equal_to(expected);
    }

    #[test]
    fn test_mx_inv_invalid_arg() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = MatrixData::ones(ctx.rules().radix, 3, 3).unwrap().to_rc_expr();
        let config = ObjectData::new(DetConfig(DetConfigData::new())).to_rc_expr();
        let expr = MethodData::new(
            a,
            "inv".to_owned(),
            vec![config]).to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        assert_result_error_that(result)
            .has_message("Invalid argument passed to the inv() method. Expected object {inv_config}, found object {det_config}.");
    }

    // Example from "Algebra liniowa w zadaniach", Jerzy Rutkowski, PWN 2008, page 97
    #[test]
    fn test_mx_45_rank() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 4, 5;
            1, 2, 3, 4, 1,
            2, 5, 5, 7, 2,
            4, 6, 8, 9, 3,
            3, 5, 4, 4, 2
        ).unwrap().to_rc_expr();
        let expr = MethodData::new(
            a,
            "rank".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        assert_expression_that(&result)
            .as_integer()
            .is_int(3);
    }

    #[test]
    fn test_mx_23_transpose() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 2, 3;
            1, 2, 3,
            4, 5, 6
        ).unwrap().to_rc_expr();
        let expr = MethodData::new(
            a,
            "transpose".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        let expected = matrix!(ctx.fa(), 3, 2;
            1, 4,
            2, 5,
            3, 6).unwrap();
        assert_expression_that(&result)
            .as_matrix()
            .is_equal_to(expected);
    }

    #[test]
    fn test_mx_33_cholesky() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            2, 8, 10,
            3, 10, 22
            ).unwrap().to_rc_expr();
        let expr = MethodData::new(
            a,
            "cholesky".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        let expected = matrix!(ctx.fa(), 3, 3;
            1, 0, 0,
            2, 2, 0,
            3, 2, 3
            ).unwrap();
        assert_expression_that(&result)
            .as_matrix()
            .is_equal_to(expected);
    }

    #[test]
    fn test_mx_33_gauss() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            2, 8, 10,
            3, 10, 22
            ).unwrap().to_rc_expr();
        let expr = MethodData::new(
            a,
            "gauss".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        let expected = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            0, 4, 4,
            0, 0, 9
            ).unwrap();
        assert_expression_that(&result)
            .as_matrix()
            .is_equal_to(expected);
    }

    #[test]
    fn test_mx_33_gauss_config() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            2, 8, 10,
            3, 10, 22
            ).unwrap().to_rc_expr();
        let config = FunctionData::new("gauss_config".to_owned(), vec![]).to_rc_expr();
        let expr = MethodData::new(
            a,
            "gauss".to_owned(),
            vec![config]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        let expected = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            2, 8, 10,
            3, 10, 22
            ).unwrap();
        assert_expression_that(&result)
            .as_matrix()
            .is_equal_to(expected);
    }

    #[test]
    fn test_mx_gauss_invalid_config() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = MatrixData::ones(ctx.rules().radix, 3, 3).unwrap().to_rc_expr();
        let config = ObjectData::new(InvConfig(InvConfigData::new())).to_rc_expr();
        let expr = MethodData::new(
            a,
            "gauss".to_owned(),
            vec![config]).to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        assert_result_error_that(result)
            .has_message("Invalid argument passed to the gauss() method. Expected object {gauss_config}, found object {inv_config}.");
    }

    #[test]
    fn test_mx_33_gauss_pivot_less() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            2, 8, 10,
            3, 10, 22
            ).unwrap().to_rc_expr();
        let config = MethodData::new(
            MethodData::new(
                FunctionData::new("gauss_config".to_owned(), vec![]).to_rc_expr(),
                "reduce_lower".to_owned(),
                vec![]).to_rc_expr(),
            "pivot_less".to_owned(),
            vec![]).to_rc_expr();
        let expr = MethodData::new(
            a,
            "gauss".to_owned(),
            vec![config]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        let expected = matrix!(ctx.fa(), 3, 3;
            3, 10, 22,
            0, (4/3), (-14/3),
            0, 0, (-9)
            ).unwrap();
        assert_expression_that(&result)
            .as_matrix()
            .is_equal_to(expected);
    }

    #[test]
    fn test_mx_33_gauss_pivot_zero() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            2, 0, 10,
            3, 10, 22
            ).unwrap().to_rc_expr();
        let config = MethodData::new(
            MethodData::new(
                FunctionData::new("gauss_config".to_owned(), vec![]).to_rc_expr(),
                "reduce_lower".to_owned(),
                vec![]).to_rc_expr(),
            "pivot_zero".to_owned(),
            vec![]).to_rc_expr();
        let expr = MethodData::new(
            a,
            "gauss".to_owned(),
            vec![config]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        let expected = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            0, (-4), 4,
            0, 0, 17
            ).unwrap();
        assert_expression_that(&result)
            .as_matrix()
            .is_equal_to(expected);
    }

    #[test]
    fn test_mx_33_gauss_identity() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            2, 8, 10,
            3, 10, 22
            ).unwrap().to_rc_expr();
        let config = MethodData::new(
            MethodData::new(
                FunctionData::new("gauss_config".to_owned(), vec![]).to_rc_expr(),
                "reduce_lower".to_owned(),
                vec![]).to_rc_expr(),
            "identity".to_owned(),
            vec![]).to_rc_expr();
        let expr = MethodData::new(
            a,
            "gauss".to_owned(),
            vec![config]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        let expected = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            0, 1, 1,
            0, 0, 1
            ).unwrap();
        assert_expression_that(&result)
            .as_matrix()
            .is_equal_to(expected);
    }

    #[test]
    fn test_mx_33_gauss_reduce_lower_true() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            2, 8, 10,
            3, 10, 22
            ).unwrap().to_rc_expr();
        let config = MethodData::new(
            FunctionData::new("gauss_config".to_owned(), vec![]).to_rc_expr(),
            "reduce_lower".to_owned(),
            vec![]).to_rc_expr();
        let expr = MethodData::new(
            a,
            "gauss".to_owned(),
            vec![config]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        let expected = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            0, 4, 4,
            0, 0, 9
            ).unwrap();
        assert_expression_that(&result)
            .as_matrix()
            .is_equal_to(expected);
    }

    #[test]
    fn test_mx_33_gauss_reduce_upper_true() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            2, 4, 2,
            3, 4, 1
            ).unwrap().to_rc_expr();
        let config = MethodData::new(
            FunctionData::new("gauss_config".to_owned(), vec![]).to_rc_expr(),
            "reduce_upper".to_owned(),
            vec![]).to_rc_expr();
        let expr = MethodData::new(
            a,
            "gauss".to_owned(),
            vec![config]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        let expected = matrix!(ctx.fa(), 3, 3;
            2, 0, 0,
            (-4), (-4), 0,
            3, 4, 1
            ).unwrap();
        assert_expression_that(&result)
            .as_matrix()
            .is_equal_to(expected);
    }

    #[test]
    fn test_mx_34_rows() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 4;
            1, 2, 3, 4,
            2, 4, 2, 1,
            3, 4, 1, 1
            ).unwrap().to_rc_expr();
        let expr = MethodData::new(
            a,
            "rows".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        assert_expression_that(&result)
            .as_integer()
            .is_int(3);
    }

    #[test]
    fn test_mx_34_columns() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 4;
            1, 2, 3, 4,
            2, 4, 2, 1,
            3, 4, 1, 1
            ).unwrap().to_rc_expr();
        let expr = MethodData::new(
            a,
            "columns".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        assert_expression_that(&result)
            .as_integer()
            .is_int(4);
    }

    #[test]
    fn test_mx_33_is_square() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = MatrixData::ones(ctx.rules().radix, 3, 3).unwrap().to_rc_expr();
        let expr = MethodData::new(
            a,
            "is_square".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        assert_expression_that(&result)
            .as_bool()
            .is_true();
    }

    #[test]
    fn test_mx_34_is_square() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = MatrixData::ones(ctx.rules().radix, 3, 4).unwrap().to_rc_expr();
        let expr = MethodData::new(
            a,
            "is_square".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        assert_expression_that(&result)
            .as_bool()
            .is_false();
    }

    #[test]
    fn test_mx_non_zero_is_zero() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = MatrixData::ones(ctx.rules().radix, 3, 3).unwrap().to_rc_expr();
        let expr = MethodData::new(
            a,
            "is_zero".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        assert_expression_that(&result)
            .as_bool()
            .is_false();
    }

    #[test]
    fn test_mx_zero_is_zero() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = MatrixData::zeros(ctx.rules().radix, 3, 3).unwrap().to_rc_expr();
        let expr = MethodData::new(
            a,
            "is_zero".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        assert_expression_that(&result)
            .as_bool()
            .is_true();
    }

    #[test]
    fn test_mx_zero_is_ones() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = MatrixData::zeros(ctx.rules().radix, 3, 3).unwrap().to_rc_expr();
        let expr = MethodData::new(
            a,
            "is_ones".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        assert_expression_that(&result)
            .as_bool()
            .is_false();
    }

    #[test]
    fn test_mx_ones_is_ones() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = MatrixData::ones(ctx.rules().radix, 3, 3).unwrap().to_rc_expr();
        let expr = MethodData::new(
            a,
            "is_ones".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        assert_expression_that(&result)
            .as_bool()
            .is_true();
    }

    #[test]
    fn test_mx_zero_is_identity() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = MatrixData::zeros(ctx.rules().radix, 3, 3).unwrap().to_rc_expr();
        let expr = MethodData::new(
            a,
            "is_identity".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        assert_expression_that(&result)
            .as_bool()
            .is_false();
    }

    #[test]
    fn test_mx_identity_is_identity() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = MatrixData::identity(ctx.rules().radix, 3).unwrap().to_rc_expr();
        let expr = MethodData::new(
            a,
            "is_identity".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        assert_expression_that(&result)
            .as_bool()
            .is_true();
    }

    #[test]
    fn test_mx_non_u_is_upper_triangular() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            1, 1, 1,
            1, 1, 1,
            1, 1, 1
            ).unwrap().to_rc_expr();
        let expr = MethodData::new(
            a,
            "is_upper_triangular".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        assert_expression_that(&result)
            .as_bool()
            .is_false();
    }

    #[test]
    fn test_mx_u_is_upper_triangular() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            1, 1, 1,
            0, 1, 1,
            0, 0, 1
            ).unwrap().to_rc_expr();
        let expr = MethodData::new(
            a,
            "is_upper_triangular".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        assert_expression_that(&result)
            .as_bool()
            .is_true();
    }

    #[test]
    fn test_mx_non_su_is_strictly_upper_triangular() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            1, 1, 1,
            0, 1, 1,
            0, 0, 1
            ).unwrap().to_rc_expr();
        let expr = MethodData::new(
            a,
            "is_strictly_upper_triangular".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        assert_expression_that(&result)
            .as_bool()
            .is_false();
    }

    #[test]
    fn test_mx_su_is_strictly_upper_triangular() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            0, 1, 1,
            0, 0, 1,
            0, 0, 0
            ).unwrap().to_rc_expr();
        let expr = MethodData::new(
            a,
            "is_strictly_upper_triangular".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        assert_expression_that(&result)
            .as_bool()
            .is_true();
    }

    #[test]
    fn test_mx_non_l_is_lower_triangular() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            1, 0, 100,
            1, 1, 0,
            1, 1, 1
            ).unwrap().to_rc_expr();
        let expr = MethodData::new(
            a,
            "is_lower_triangular".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        assert_expression_that(&result)
            .as_bool()
            .is_false();
    }

    #[test]
    fn test_mx_l_is_lower_triangular() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            1, 0, 0,
            1, 1, 0,
            1, 1, 1
            ).unwrap().to_rc_expr();
        let expr = MethodData::new(
            a,
            "is_lower_triangular".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        assert_expression_that(&result)
            .as_bool()
            .is_true();
    }

    #[test]
    fn test_mx_non_symmetric_is_symmetric() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            1, 2, 100,
            2, 4, 4,
            3, 4, 1
            ).unwrap().to_rc_expr();
        let expr = MethodData::new(
            a,
            "is_symmetric".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        assert_expression_that(&result)
            .as_bool()
            .is_false();
    }

    #[test]
    fn test_mx_symmetric_is_symmetric() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            2, 4, 4,
            3, 4, 1
            ).unwrap().to_rc_expr();
        let expr = MethodData::new(
            a,
            "is_symmetric".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        assert_expression_that(&result)
            .as_bool()
            .is_true();
    }

    #[test]
    fn test_unknown_method() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = matrix!(ctx.fa(), 3, 3;
            1, 2, 3,
            2, 4, 4,
            3, 4, 1
            ).unwrap().to_rc_expr();
        let expr = MethodData::new(
            a,
            "abc".to_owned(),
            Vec::new())
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("matrix [1, 2, 3; 2, 4, 4; 3, 4, 1] does not have abc() method.");
    }
}

