use crate::calculator::method::raise_unknown_method;
use crate::calculator::context::CalcContext;
use crate::calculator::CalcResult;
use crate::calculator::CalcResultOrErr;
use matm_error::error::CalcError;
use matm_model::expressions::method::MethodData;
use matm_model::expressions::soe::SysOfEqData;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;
use std::rc::Rc;

pub async fn calculate_obj_equations_method(
    ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
) -> CalcResultOrErr {
    if MethodData::SYSTEM.eq(&method_obj.func.name) {
        calculate_obj_equations_method_system(ctx, method_obj, &method_obj.func.arguments).await
    } else {
        raise_unknown_method(method_obj)
    }
}

async fn calculate_obj_equations_method_system(
    _ctx: &mut CalcContext<'_>,
    _method_obj: &MethodData,
    args: &Vec<Rc<Expression>>,
) -> CalcResultOrErr {
    let mut equations: Vec<Rc<Expression>> = Vec::new();
    for expr in args {
        if let Expression::Equation(_) = expr.as_ref() {
            equations.push(Rc::clone(expr));
        } else {
            return Err(CalcError::from_str("All arguments should be equations."));
        }
    }
    let soe = SysOfEqData::from_vector(equations);
    Ok(CalcResult::one(soe.to_rc_expr()))
}

#[cfg(test)]
mod tests {
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::testing::asserts::test_calculation;
    use crate::testing::asserts::test_calculation_ok;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::equation::EquationData;
    use matm_model::expressions::method::MethodData;
    use matm_model::expressions::object::ObjectData;
    use matm_model::expressions::object::ObjectType;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;
    use matm_model::testing::asserts::assert_result_error_that;

    #[test]
    fn test_system_1eq() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Equations).to_rc_expr(),
            "system".to_owned(),
            vec![
                EquationData::new(
                    ctx.fa().rsn("x"),
                    BinaryData::add(
                        ctx.fa().rzi_u(2),
                        ctx.fa().rzi_u(3)
                    ).to_rc_expr()
                ).to_rc_expr(),
            ])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_equation()
            .is_left_symbol_that(|e| {
                e.has_name("x");
            }).has_right_that(|e| {
            e.as_integer().is_int(5);
        });
    }

    #[test]
    fn test_system_2eq() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Equations).to_rc_expr(),
            "system".to_owned(),
            vec![
                EquationData::new(
                    ctx.fa().rsn("x"),
                    BinaryData::add(
                        ctx.fa().rzi_u(2),
                        ctx.fa().rzi_u(3)
                    ).to_rc_expr()
                ).to_rc_expr(),
                EquationData::new(
                    ctx.fa().rsn("y"),
                    BinaryData::add(
                        ctx.fa().rzi_u(4),
                        ctx.fa().rzi_u(5)
                    ).to_rc_expr()
                ).to_rc_expr(),
            ])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_soe()
            .has_equations_size(2)
            .has_equation_i_that(0, |e| {
                e.as_equation().is_left_symbol_that(|e| {
                    e.has_name("x");
                }).has_right_that(|e| {
                    e.as_integer().is_int(5);
                });
            })
            .has_equation_i_that(1, |e| {
                e.as_equation().is_left_symbol_that(|e| {
                    e.has_name("y");
                }).has_right_that(|e| {
                    e.as_integer().is_int(9);
                });
            });
    }

    #[test]
    fn test_system_no_eq_as_argument() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Equations).to_rc_expr(),
            "system".to_owned(),
            vec![
                EquationData::new(
                    ctx.fa().rsn("x"),
                    BinaryData::add(
                        ctx.fa().rzi_u(2),
                        ctx.fa().rzi_u(3)
                    ).to_rc_expr()
                ).to_rc_expr(),
                ctx.fa().rzi_u(1),
            ])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("All arguments should be equations.");
    }
}
