use matm_model::expressions::method::MethodData;
use matm_model::expressions::object::ObjectData;
use matm_model::expressions::object::ObjectType;
use matm_model::expressions::object::PascalMatrixConfigData;
use matm_model::expressions::object::PascalMatrixVariant;
use matm_model::expressions::ToExpression;

use crate::calculator::method::raise_unknown_method;
use crate::calculator::method::validate_method_no_argument_expected;
use crate::calculator::CalcResult;
use crate::calculator::CalcResultOrErr;

pub async fn calculate_obj_pascal_matrix_config_method(
    method_obj: &MethodData,
    data: &PascalMatrixConfigData,
) -> CalcResultOrErr {
    return if MethodData::SYMMETRIC.eq(&method_obj.func.name) {
        calculate_symmetric(method_obj, data).await
    } else if MethodData::LOWER.eq(&method_obj.func.name) {
        calculate_lower(method_obj, data).await
    } else if MethodData::UPPER.eq(&method_obj.func.name) {
        calculate_upper(method_obj, data).await
    } else {
        raise_unknown_method(method_obj)
    }
}

async fn calculate_symmetric(
    method_obj: &MethodData,
    data: &PascalMatrixConfigData,
) -> CalcResultOrErr {
    validate_method_no_argument_expected(method_obj)?;
    let new_data = PascalMatrixConfigData {
        variant: PascalMatrixVariant::Symmetric,
        ..*data
    };
    return Ok(CalcResult::one(
        ObjectData::new(ObjectType::PascalMatrixConfig(new_data)).to_rc_expr(),
    ));
}

async fn calculate_lower(
    method_obj: &MethodData,
    data: &PascalMatrixConfigData,
) -> CalcResultOrErr {
    validate_method_no_argument_expected(method_obj)?;
    let new_data = PascalMatrixConfigData {
        variant: PascalMatrixVariant::LowerTriangular,
        ..*data
    };
    return Ok(CalcResult::one(
        ObjectData::new(ObjectType::PascalMatrixConfig(new_data)).to_rc_expr(),
    ));
}

async fn calculate_upper(
    method_obj: &MethodData,
    data: &PascalMatrixConfigData,
) -> CalcResultOrErr {
    validate_method_no_argument_expected(method_obj)?;
    let new_data = PascalMatrixConfigData {
        variant: PascalMatrixVariant::UpperTriangular,
        ..*data
    };
    return Ok(CalcResult::one(
        ObjectData::new(ObjectType::PascalMatrixConfig(new_data)).to_rc_expr(),
    ));
}


#[cfg(test)]
mod tests {
    use rstest::rstest;

    use matm_model::expressions::method::MethodData;
    use matm_model::expressions::object::ObjectData;
    use matm_model::expressions::object::ObjectType;
    use matm_model::expressions::object::PascalMatrixConfigData;
    use matm_model::expressions::object::PascalMatrixVariant;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;
    use matm_model::testing::asserts::assert_result_error_that;

    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::testing::asserts::test_calculation;
    use crate::testing::asserts::test_calculation_ok;

    #[test]
    fn test_symmetric() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::PascalMatrixConfig(PascalMatrixConfigData::new())).to_rc_expr(),
            "symmetric".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_object()
            .is_type(ObjectType::PascalMatrixConfig(PascalMatrixConfigData {
                variant: PascalMatrixVariant::Symmetric,
            }));
    }

    #[test]
    fn test_lower() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::PascalMatrixConfig(PascalMatrixConfigData::new())).to_rc_expr(),
            "lower".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_object()
            .is_type(ObjectType::PascalMatrixConfig(PascalMatrixConfigData {
                variant: PascalMatrixVariant::LowerTriangular,
            }));
    }

    #[test]
    fn test_upper() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::PascalMatrixConfig(PascalMatrixConfigData::new())).to_rc_expr(),
            "upper".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_object()
            .is_type(ObjectType::PascalMatrixConfig(PascalMatrixConfigData {
                variant: PascalMatrixVariant::UpperTriangular,
            }));
    }

    #[rstest]
    #[case("symmetric")]
    #[case("lower")]
    #[case("upper")]
    fn test_method_no_arg_expected_error(
        #[case] method_name: &str,
    ) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::PascalMatrixConfig(PascalMatrixConfigData::new())).to_rc_expr(),
            method_name.to_owned(),
            vec![ctx.fa().rzi_u(2)])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message(format!(
                "Method {}() cannot have any arguments.",
                method_name
            ).as_str());
    }

    #[test]
    fn test_unknown_method() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::PascalMatrixConfig(PascalMatrixConfigData::new())).to_rc_expr(),
            "abc".to_owned(),
            Vec::new())
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("object {pascal_matrix_config} does not have abc() method.");
    }
}

