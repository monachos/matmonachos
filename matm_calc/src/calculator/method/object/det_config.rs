use crate::calculator::method::raise_unknown_method;
use crate::calculator::method::validate_method_no_argument_expected;
use crate::calculator::CalcResult;
use crate::calculator::CalcResultOrErr;
use matm_model::expressions::method::MethodData;
use matm_model::expressions::object::DetConfigData;
use matm_model::expressions::object::DetMethod;
use matm_model::expressions::object::ObjectData;
use matm_model::expressions::object::ObjectType;
use matm_model::expressions::ToExpression;


pub async fn calculate_obj_det_config_method(
    method_obj: &MethodData,
    data: &DetConfigData,
) -> CalcResultOrErr {
    return if MethodData::LAPLACE.eq(&method_obj.func.name) {
        calculate_laplace(method_obj, data).await
    } else if MethodData::BAREISS.eq(&method_obj.func.name) {
        calculate_bareiss(method_obj, data).await
    } else {
        raise_unknown_method(method_obj)
    }
}

async fn calculate_laplace(
    method_obj: &MethodData,
    data: &DetConfigData,
) -> CalcResultOrErr {
    validate_method_no_argument_expected(method_obj)?;
    let new_data = DetConfigData {
        method: DetMethod::CofactorExpansion,
        ..*data
    };
    return Ok(CalcResult::one(
        ObjectData::new(ObjectType::DetConfig(new_data)).to_rc_expr(),
    ));
}

async fn calculate_bareiss(
    method_obj: &MethodData,
    data: &DetConfigData,
) -> CalcResultOrErr {
    validate_method_no_argument_expected(method_obj)?;
    let new_data = DetConfigData {
        method: DetMethod::Bareiss,
        ..*data
    };
    return Ok(CalcResult::one(
        ObjectData::new(ObjectType::DetConfig(new_data)).to_rc_expr(),
    ));
}


#[cfg(test)]
mod tests {
    use rstest::rstest;

    use matm_model::expressions::method::MethodData;
    use matm_model::expressions::object::DetConfigData;
    use matm_model::expressions::object::DetMethod;
    use matm_model::expressions::object::ObjectData;
    use matm_model::expressions::object::ObjectType;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;
    use matm_model::testing::asserts::assert_result_error_that;

    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::testing::asserts::test_calculation;
    use crate::testing::asserts::test_calculation_ok;

    #[test]
    fn test_laplace() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::DetConfig(DetConfigData::new())).to_rc_expr(),
            "laplace".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_object()
            .is_type(ObjectType::DetConfig(DetConfigData {
                method: DetMethod::CofactorExpansion,
            }));
    }

    #[test]
    fn test_bareiss() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::DetConfig(DetConfigData::new())).to_rc_expr(),
            "bareiss".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_object()
            .is_type(ObjectType::DetConfig(DetConfigData {
                method: DetMethod::Bareiss,
            }));
    }

    #[rstest]
    #[case("laplace")]
    #[case("bareiss")]
    fn test_method_no_arg_expected_error(
        #[case] method_name: &str,
    ) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::DetConfig(DetConfigData::new())).to_rc_expr(),
            method_name.to_owned(),
            vec![ctx.fa().rzi_u(2)])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message(format!(
                "Method {}() cannot have any arguments.",
                method_name
            ).as_str());
    }

    #[test]
    fn test_unknown_method() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::DetConfig(DetConfigData::new())).to_rc_expr(),
            "abc".to_owned(),
            Vec::new())
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("object {det_config} does not have abc() method.");
    }
}

