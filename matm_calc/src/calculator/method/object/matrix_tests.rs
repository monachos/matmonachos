#[cfg(test)]
mod tests {
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::matrix;
    use crate::rules::CalcRules;
    use crate::testing::asserts::test_calculation;
    use crate::testing::asserts::test_calculation_ok;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::exponentiation::ExpData;
    use matm_model::expressions::func::FunctionData;
    use matm_model::expressions::matrices::MatrixData;
    use matm_model::expressions::method::MethodData;
    use matm_model::expressions::object::InvConfigData;
    use matm_model::expressions::object::ObjectData;
    use matm_model::expressions::object::ObjectType;
    use matm_model::expressions::object::ObjectType::InvConfig;
    use matm_model::expressions::Expression;
    use matm_model::expressions::ToExpression;
    use matm_model::factories::ObjFactory;
    use matm_model::testing::asserts::assert_expression_that;
    use matm_model::testing::asserts::assert_result_error_that;
    use once_cell::sync::Lazy;
    use rstest::rstest;
    use std::rc::Rc;

    const DEFAULT_RULES: Lazy<CalcRules> = Lazy::new(|| {CalcRules::default()});
    const DEFAULT_FA: Lazy<ObjFactory> = Lazy::new(|| {ObjFactory::new(DEFAULT_RULES.radix)});

    #[test]
    fn test_zeros_2() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "zeros".to_owned(),
            vec![ctx.fa().rzi_u(2)])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(2)
            .has_columns(2)
            .has_i_i_zero(1, 1)
            .has_i_i_zero(1, 2)
            .has_i_i_zero(2, 1)
            .has_i_i_zero(2, 2);
    }

    #[test]
    fn test_zeros_bin() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "zeros".to_owned(),
            vec![BinaryData::add(ctx.fa().rzi_u(1), ctx.fa().rzi_u(1)).to_rc_expr()])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(2)
            .has_columns(2)
            .has_i_i_zero(1, 1)
            .has_i_i_zero(1, 2)
            .has_i_i_zero(2, 1)
            .has_i_i_zero(2, 2);
    }

    #[test]
    fn test_zeros_2_3() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "zeros".to_owned(),
            vec![ctx.fa().rzi_u(2), ctx.fa().rzi_u(3)])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(2)
            .has_columns(3)
            .has_i_i_zero(1, 1)
            .has_i_i_zero(1, 2)
            .has_i_i_zero(1, 3)
            .has_i_i_zero(2, 1)
            .has_i_i_zero(2, 2)
            .has_i_i_zero(2, 3);
    }

    #[test]
    fn test_zeros_bin_bin() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "zeros".to_owned(),
            vec![
                BinaryData::add(ctx.fa().rzi_u(1), ctx.fa().rzi_u(1)).to_rc_expr(),
                BinaryData::add(ctx.fa().rzi_u(1), ctx.fa().rzi_u(2)).to_rc_expr()
                ])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(2)
            .has_columns(3)
            .has_i_i_zero(1, 1)
            .has_i_i_zero(1, 2)
            .has_i_i_zero(1, 3)
            .has_i_i_zero(2, 1)
            .has_i_i_zero(2, 2)
            .has_i_i_zero(2, 3);
    }

    #[test]
    fn test_zeros_no_args() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "zeros".to_owned(),
            vec![])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected 1 or 2 arguments of positive integer type.");
    }

    #[test]
    fn test_zeros_3_args() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "zeros".to_owned(),
            vec![ctx.fa().rzi_u(2), ctx.fa().rzi_u(2), ctx.fa().rzi_u(2)])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected 1 or 2 arguments of positive integer type.");
    }

    #[test]
    fn test_zeros_a() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "zeros".to_owned(),
            vec![ctx.fa().rsn("a")])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected argument (1) of integer type.");
    }

    #[test]
    fn test_zeros_2_a() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "zeros".to_owned(),
            vec![ctx.fa().rzi_u(2), ctx.fa().rsn("a")])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected argument (2) of integer type.");
    }

    #[test]
    fn test_zeros_1000() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "zeros".to_owned(),
            vec![ctx.fa().rzi_u(1000)])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected argument (1) of integer in range 1..100.");
    }

    #[test]
    fn test_zeros_2_1000() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "zeros".to_owned(),
            vec![ctx.fa().rzi_u(2), ctx.fa().rzi_u(1000)])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected argument (2) of integer in range 1..100.");
    }

    #[test]
    fn test_ones_2() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "ones".to_owned(),
            vec![ctx.fa().rzi_u(2)])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(2)
            .has_columns(2)
            .has_i_i_one(1, 1)
            .has_i_i_one(1, 2)
            .has_i_i_one(2, 1)
            .has_i_i_one(2, 2);
    }

    #[test]
    fn test_ones_bin() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "ones".to_owned(),
            vec![BinaryData::add(ctx.fa().rzi_u(1), ctx.fa().rzi_u(1)).to_rc_expr()])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(2)
            .has_columns(2)
            .has_i_i_one(1, 1)
            .has_i_i_one(1, 2)
            .has_i_i_one(2, 1)
            .has_i_i_one(2, 2);
    }

    #[test]
    fn test_ones_2_3() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "ones".to_owned(),
            vec![ctx.fa().rzi_u(2), ctx.fa().rzi_u(3)])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(2)
            .has_columns(3)
            .has_i_i_one(1, 1)
            .has_i_i_one(1, 2)
            .has_i_i_one(1, 3)
            .has_i_i_one(2, 1)
            .has_i_i_one(2, 2)
            .has_i_i_one(2, 3);
    }

    #[test]
    fn test_ones_bin_bin() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "ones".to_owned(),
            vec![
                BinaryData::add(ctx.fa().rzi_u(1), ctx.fa().rzi_u(1)).to_rc_expr(),
                BinaryData::add(ctx.fa().rzi_u(1), ctx.fa().rzi_u(2)).to_rc_expr()
            ])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(2)
            .has_columns(3)
            .has_i_i_one(1, 1)
            .has_i_i_one(1, 2)
            .has_i_i_one(1, 3)
            .has_i_i_one(2, 1)
            .has_i_i_one(2, 2)
            .has_i_i_one(2, 3);
    }

    #[test]
    fn test_ones_no_args() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "ones".to_owned(),
            vec![])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected 1 or 2 arguments of positive integer type.");
    }

    #[test]
    fn test_ones_3_args() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "ones".to_owned(),
            vec![ctx.fa().rzi_u(2), ctx.fa().rzi_u(2), ctx.fa().rzi_u(2)])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected 1 or 2 arguments of positive integer type.");
    }

    #[test]
    fn test_ones_a() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "ones".to_owned(),
            vec![ctx.fa().rsn("a")])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected argument (1) of integer type.");
    }

    #[test]
    fn test_ones_2_a() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "ones".to_owned(),
            vec![ctx.fa().rzi_u(2), ctx.fa().rsn("a")])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected argument (2) of integer type.");
    }

    #[test]
    fn test_ones_1000() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "ones".to_owned(),
            vec![ctx.fa().rzi_u(1000)])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected argument (1) of integer in range 1..100.");
    }

    #[test]
    fn test_ones_2_1000() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "ones".to_owned(),
            vec![ctx.fa().rzi_u(2), ctx.fa().rzi_u(1000)])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected argument (2) of integer in range 1..100.");
    }

    #[test]
    fn test_diagonal_11() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "diagonal".to_owned(),
            vec![ctx.fa().rzi_u(1), ctx.fa().rzi_u(1),
                 ctx.fa().rzi_u(1)])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        let expected = matrix!(ctx.fa(), 1, 1;
            1
        ).unwrap();
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(1)
            .has_columns(1)
            .is_equal_to(expected);
    }

    #[test]
    fn test_diagonal_33() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "diagonal".to_owned(),
            vec![ctx.fa().rzi_u(3), ctx.fa().rzi_u(3),
                 ctx.fa().rzi_u(1),
                 ctx.fa().rzi_u(2),
                 ctx.fa().rzi_u(3)])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        let expected = matrix!(ctx.fa(), 3, 3;
            1, 0, 0,
            0, 2, 0,
            0, 0, 3
        ).unwrap();
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(3)
            .has_columns(3)
            .is_equal_to(expected);
    }

    #[test]
    fn test_diagonal_32() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "diagonal".to_owned(),
            vec![ctx.fa().rzi_u(3), ctx.fa().rzi_u(2),
                 ctx.fa().rzi_u(1),
                 ctx.fa().rzi_u(2)])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        let expected = matrix!(ctx.fa(), 3, 2;
            1, 0,
            0, 2,
            0, 0
        ).unwrap();
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(3)
            .has_columns(2)
            .is_equal_to(expected);
    }

    #[test]
    fn test_diagonal_23() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "diagonal".to_owned(),
            vec![ctx.fa().rzi_u(2), ctx.fa().rzi_u(3),
                 ctx.fa().rzi_u(1),
                 ctx.fa().rzi_u(2)])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        let expected = matrix!(ctx.fa(), 2, 3;
            1, 0, 0,
            0, 2, 0
        ).unwrap();
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(2)
            .has_columns(3)
            .is_equal_to(expected);
    }

    #[test]
    fn test_diagonal_calculate_size() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "diagonal".to_owned(),
            vec![
                BinaryData::add(ctx.fa().rzi_u(1), ctx.fa().rzi_u(2)).to_rc_expr(),
                BinaryData::add(ctx.fa().rzi_u(1), ctx.fa().rzi_u(3)).to_rc_expr(),
                ctx.fa().rzi_u(1),
                ctx.fa().rzi_u(2),
                ctx.fa().rzi_u(3),
            ])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(3)
            .has_columns(4);
    }

    #[rstest]
    #[case(vec![])]
    #[case(vec![DEFAULT_FA.rzi_u(3)])]
    #[case(vec![DEFAULT_FA.rzi_u(1), DEFAULT_FA.rzi_u(2)])]
    fn test_diagonal_too_few_args(
        #[case] args: Vec<Rc<Expression>>
    ) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "diagonal".to_owned(),
            args)
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected 2 arguments for size (rows, columns) and 1 or more arguments for diagonal values.");
    }

    #[test]
    fn test_diagonal_1000_2() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "diagonal".to_owned(),
            vec![ctx.fa().rzi_u(1000), ctx.fa().rzi_u(2),
                 ctx.fa().rzi_u(1)])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected argument (1) of integer in range 1..100.");
    }

    #[test]
    fn test_diagonal_2_1000() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "diagonal".to_owned(),
            vec![ctx.fa().rzi_u(2), ctx.fa().rzi_u(1000),
                 ctx.fa().rzi_u(1)])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected argument (2) of integer in range 1..100.");
    }

    #[test]
    fn test_identity_3() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "identity".to_owned(),
            vec![ctx.fa().rzi_u(3)])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(3)
            .has_columns(3)
            .has_i_i_one(1, 1)
            .has_i_i_zero(1, 2)
            .has_i_i_zero(1, 3)
            .has_i_i_zero(2, 1)
            .has_i_i_one(2, 2)
            .has_i_i_zero(2, 3)
            .has_i_i_zero(3, 1)
            .has_i_i_zero(3, 2)
            .has_i_i_one(3, 3);
    }

    #[test]
    fn test_identity_bin() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "identity".to_owned(),
            vec![BinaryData::add(ctx.fa().rzi_u(1), ctx.fa().rzi_u(2)).to_rc_expr()])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(3)
            .has_columns(3)
            .has_i_i_one(1, 1)
            .has_i_i_zero(1, 2)
            .has_i_i_zero(1, 3)
            .has_i_i_zero(2, 1)
            .has_i_i_one(2, 2)
            .has_i_i_zero(2, 3)
            .has_i_i_zero(3, 1)
            .has_i_i_zero(3, 2)
            .has_i_i_one(3, 3);
    }

    #[test]
    fn test_identity_no_args() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "identity".to_owned(),
            vec![])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected 1 argument of positive integer type.");
    }

    #[test]
    fn test_identity_2_args() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "identity".to_owned(),
            vec![ctx.fa().rzi_u(2), ctx.fa().rzi_u(2)])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected 1 argument of positive integer type.");
    }

    #[test]
    fn test_identity_a() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "identity".to_owned(),
            vec![ctx.fa().rsn("a")])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected argument (1) of integer type.");
    }

    #[test]
    fn test_identity_1000() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "identity".to_owned(),
            vec![ctx.fa().rzi_u(1000)])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected argument (1) of integer in range 1..100.");
    }

    #[test]
    fn test_of_2_3() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "of".to_owned(),
            vec![ctx.fa().rzi_u(2), ctx.fa().rzi_u(3),
                 ctx.fa().rzi_u(11), ctx.fa().rzi_u(12), ctx.fa().rzi_u(13),
                 ctx.fa().rzi_u(21), ctx.fa().rzi_u(22), ctx.fa().rzi_u(23)
            ])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(2)
            .has_columns(3)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(11);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(12);})
            .is_i_i_integer_that(1, 3, |e| {e.is_int(13);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(21);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(22);})
            .is_i_i_integer_that(2, 3, |e| {e.is_int(23);})
            ;
    }

    #[test]
    fn test_of_bin_bin() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "of".to_owned(),
            vec![
                BinaryData::add(ctx.fa().rzi_u(1), ctx.fa().rzi_u(1)).to_rc_expr(),
                BinaryData::add(ctx.fa().rzi_u(1), ctx.fa().rzi_u(2)).to_rc_expr(),
                ctx.fa().rzi_u(11), ctx.fa().rzi_u(12), ctx.fa().rzi_u(13),
                ctx.fa().rzi_u(21), ctx.fa().rzi_u(22), ctx.fa().rzi_u(23)
            ])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(2)
            .has_columns(3)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(11);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(12);})
            .is_i_i_integer_that(1, 3, |e| {e.is_int(13);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(21);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(22);})
            .is_i_i_integer_that(2, 3, |e| {e.is_int(23);})
            ;
    }

    #[test]
    fn test_of_no_size() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "of".to_owned(),
            vec![])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected 2 first arguments defining matrix size (row and column).");
    }

    #[test]
    fn test_of_2_args() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "of".to_owned(),
            vec![ctx.fa().rzi_u(2), ctx.fa().rzi_u(2)])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected 2 first arguments defining matrix size (row and column).");
    }

    #[test]
    fn test_of_a_3() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "of".to_owned(),
            vec![ctx.fa().rsn("a"), ctx.fa().rzi_u(3), ctx.fa().rz0()])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected argument (1) of integer type.");
    }

    #[test]
    fn test_of_2_a() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "of".to_owned(),
            vec![ctx.fa().rzi_u(2), ctx.fa().rsn("a"), ctx.fa().rz0()])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected argument (2) of integer type.");
    }

    #[test]
    fn test_of_1000_3() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "of".to_owned(),
            vec![ctx.fa().rzi_u(1000), ctx.fa().rzi_u(3), ctx.fa().rz0()])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected argument (1) of integer in range 1..100.");
    }

    #[test]
    fn test_of_2_1000() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "of".to_owned(),
            vec![ctx.fa().rzi_u(2), ctx.fa().rzi_u(1000), ctx.fa().rz0()])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected argument (2) of integer in range 1..100.");
    }

    #[test]
    fn test_of_2_3_missing_elements() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "of".to_owned(),
            vec![ctx.fa().rzi_u(2), ctx.fa().rzi_u(3),
                 ctx.fa().rz0(), ctx.fa().rz0(), ctx.fa().rz0()])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected 2+6 arguments defining matrix size and elements.");
    }

    #[test]
    fn test_hilbert_3() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "hilbert".to_owned(),
            vec![ctx.fa().rzi_u(3)])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        let expected = matrix!(ctx.fa(), 3, 3;
            1, (1/2), (1/3),
            (1/2), (1/3), (1/4),
            (1/3), (1/4), (1/5)
        ).unwrap();
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(3)
            .has_columns(3)
            .is_equal_to(expected)
        ;
    }

    #[test]
    fn test_hilbert_calculate_size() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "hilbert".to_owned(),
            vec![BinaryData::add(ctx.fa().rzi_u(1), ctx.fa().rzi_u(2)).to_rc_expr()])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(3)
            .has_columns(3)
        ;
    }

    #[test]
    fn test_hilbert_no_size() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "hilbert".to_owned(),
            vec![])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected 1 argument defining matrix size.");
    }

    #[test]
    fn test_hilbert_2_args() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "hilbert".to_owned(),
            vec![ctx.fa().rzi_u(2), ctx.fa().rzi_u(2)])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected 1 argument defining matrix size.");
    }

    #[test]
    fn test_pascal_5() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "pascal".to_owned(),
            vec![ctx.fa().rzi_u(5)])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        let expected = matrix!(ctx.fa(), 5, 5;
            1, 1, 1, 1, 1,
            1, 2, 3, 4, 5,
            1, 3, 6, 10, 15,
            1, 4, 10, 20, 35,
            1, 5, 15, 35, 70
        ).unwrap();
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(5)
            .has_columns(5)
            .is_equal_to(expected)
        ;
    }

    #[test]
    fn test_pascal_calculate_size() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "pascal".to_owned(),
            vec![BinaryData::add(ctx.fa().rzi_u(1), ctx.fa().rzi_u(2)).to_rc_expr()])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(3)
            .has_columns(3)
        ;
    }

    #[test]
    fn test_pascal_5_symmetric() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let config = MethodData::new(
            FunctionData::new("pascal_matrix_config".to_owned(), vec![]).to_rc_expr(),
            "symmetric".to_owned(),
            vec![]).to_rc_expr();
        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "pascal".to_owned(),
            vec![ctx.fa().rzi_u(5), config])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        let expected = matrix!(ctx.fa(), 5, 5;
            1, 1, 1, 1, 1,
            1, 2, 3, 4, 5,
            1, 3, 6, 10, 15,
            1, 4, 10, 20, 35,
            1, 5, 15, 35, 70
        ).unwrap();
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(5)
            .has_columns(5)
            .is_equal_to(expected)
        ;
    }

    #[test]
    fn test_pascal_5_lower() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let config = MethodData::new(
            FunctionData::new("pascal_matrix_config".to_owned(), vec![]).to_rc_expr(),
            "lower".to_owned(),
            vec![]).to_rc_expr();
        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "pascal".to_owned(),
            vec![ctx.fa().rzi_u(5), config])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        let expected = matrix!(ctx.fa(), 5, 5;
            1, 0, 0, 0, 0,
            1, 1, 0, 0, 0,
            1, 2, 1, 0, 0,
            1, 3, 3, 1, 0,
            1, 4, 6, 4, 1
        ).unwrap();
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(5)
            .has_columns(5)
            .is_equal_to(expected)
        ;
    }

    #[test]
    fn test_pascal_calculate_size_lower() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let config = MethodData::new(
            FunctionData::new("pascal_matrix_config".to_owned(), vec![]).to_rc_expr(),
            "lower".to_owned(),
            vec![]).to_rc_expr();
        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "pascal".to_owned(),
            vec![
                BinaryData::add(ctx.fa().rzi_u(1), ctx.fa().rzi_u(2)).to_rc_expr(),
                config
            ])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(3)
            .has_columns(3)
        ;
    }

    #[test]
    fn test_pascal_5_upper() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let config = MethodData::new(
            FunctionData::new("pascal_matrix_config".to_owned(), vec![]).to_rc_expr(),
            "upper".to_owned(),
            vec![]).to_rc_expr();
        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "pascal".to_owned(),
            vec![ctx.fa().rzi_u(5), config])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        let expected = matrix!(ctx.fa(), 5, 5;
            1, 1, 1, 1, 1,
            0, 1, 2, 3, 4,
            0, 0, 1, 3, 6,
            0, 0, 0, 1, 4,
            0, 0, 0, 0, 1
        ).unwrap();
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(5)
            .has_columns(5)
            .is_equal_to(expected)
        ;
    }

    #[test]
    fn test_pascal_invalid_config() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let config = ObjectData::new(InvConfig(InvConfigData::new())).to_rc_expr();
        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "pascal".to_owned(),
            vec![ctx.fa().rzi_u(5), config])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Invalid argument passed to the pascal() method. Expected object {pascal_matrix_config}, found object {inv_config}.");
    }

    #[test]
    fn test_pascal_no_size() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "pascal".to_owned(),
            vec![])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected 1st argument defining matrix size and 2nd optional argument containing configuration.");
    }

    #[test]
    fn test_pascal_3_args() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "pascal".to_owned(),
            vec![
                ctx.fa().rzi_u(1),
                ctx.fa().rzi_u(2),
                ctx.fa().rzi_u(3)
            ])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected 1st argument defining matrix size and 2nd optional argument containing configuration.");
    }

    #[test]
    fn test_vandermonde_ints() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "vandermonde".to_owned(),
            vec![
                ctx.fa().rzi_u(4),
                ctx.fa().rzi_u(2),
                ctx.fa().rzi_u(3),
            ])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        let expected = matrix!(ctx.fa(), 2, 4;
            1, 2, 4, 8,
            1, 3, 9, 27
        ).unwrap();
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(2)
            .has_columns(4)
            .is_equal_to(expected)
        ;
    }

    #[test]
    #[ignore = "Matrix elements cannot be verified due to unnecessary factorization"]
    fn test_vandermonde_symbols() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = ctx.fa().rsn("a");
        let b = ctx.fa().rsn("b");
        let c = ctx.fa().rsn("c");

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "vandermonde".to_owned(),
            vec![
                ctx.fa().rzi_u(4),
                Rc::clone(&a),
                Rc::clone(&b),
                Rc::clone(&c),
            ])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        let expected = MatrixData::from_vector(
            3, 4,
            vec![
                ctx.fa().rz1(), Rc::clone(&a), ExpData::new(Rc::clone(&a), ctx.fa().rzi_u(2)).to_rc_expr(),
                ctx.fa().rz1(), Rc::clone(&b), ExpData::new(Rc::clone(&b), ctx.fa().rzi_u(2)).to_rc_expr(),
                ctx.fa().rz1(), Rc::clone(&c), ExpData::new(Rc::clone(&c), ctx.fa().rzi_u(2)).to_rc_expr(),
            ]
        ).unwrap();
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(3)
            .has_columns(4)
            .is_equal_to(expected)
        ;
    }

    #[test]
    fn test_vandermonde_calculate_columns() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let a = ctx.fa().rsn("a");
        let b = ctx.fa().rsn("b");

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "vandermonde".to_owned(),
            vec![
                BinaryData::add(ctx.fa().rzi_u(1), ctx.fa().rzi_u(2)).to_rc_expr(),
                Rc::clone(&a),
                Rc::clone(&b),
            ])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_matrix()
            .has_rows(2)
            .has_columns(3)
        ;
    }

    #[test]
    fn test_vandermonde_no_columns() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "vandermonde".to_owned(),
            vec![])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("Expected first argument defining number of columns and next arguments as elements of 2nd column.");
    }

    #[test]
    fn test_unknown_method() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Matrix).to_rc_expr(),
            "abc".to_owned(),
            Vec::new())
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("object {matrix} does not have abc() method.");
    }
}

