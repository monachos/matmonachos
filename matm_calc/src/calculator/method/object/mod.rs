mod cracovian_tests;
mod matrix_tests;
pub mod cracovian;
pub mod det_config;
pub mod gauss_config;
pub mod inv_config;
pub mod matrix;
pub mod pascal_matrix_config;
mod equations;


use crate::calculator::method::object::cracovian::calculate_obj_cracovian_method;
use crate::calculator::method::object::det_config::calculate_obj_det_config_method;
use crate::calculator::method::object::equations::calculate_obj_equations_method;
use crate::calculator::method::object::gauss_config::calculate_obj_gauss_config_method;
use crate::calculator::method::object::inv_config::calculate_obj_inv_config_method;
use crate::calculator::method::object::matrix::calculate_obj_matrix_method;
use crate::calculator::method::object::pascal_matrix_config::calculate_obj_pascal_matrix_config_method;
use crate::calculator::CalcResultOrErr;
use crate::calculator::context::CalcContext;
use matm_model::expressions::method::MethodData;
use matm_model::expressions::object::ObjectData;
use matm_model::expressions::object::ObjectType;


pub(crate) async fn calculate_object_method(
    ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    obj: &ObjectData,
) -> CalcResultOrErr {
    match &obj.object_type {
        ObjectType::Cracovian => calculate_obj_cracovian_method(ctx, method_obj).await,
        ObjectType::DetConfig(data) => calculate_obj_det_config_method(method_obj, data).await,
        ObjectType::GaussConfig(data) => calculate_obj_gauss_config_method(method_obj, data).await,
        ObjectType::InvConfig(data) => calculate_obj_inv_config_method(method_obj, data).await,
        ObjectType::Matrix => calculate_obj_matrix_method(ctx, method_obj).await,
        ObjectType::PascalMatrixConfig(data) => calculate_obj_pascal_matrix_config_method(method_obj, data).await,
        ObjectType::Equations => calculate_obj_equations_method(ctx, method_obj).await,
    }
}
