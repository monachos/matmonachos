use std::cmp::min;
use std::rc::Rc;

use matm_error::error::CalcError;
use matm_model::expressions::binary::BinaryData;
use matm_model::expressions::exponentiation::ExpData;
use matm_model::expressions::matrices::MatrixData;
use matm_model::expressions::method::MethodData;
use matm_model::expressions::object::ObjectData;
use matm_model::expressions::object::ObjectType;
use matm_model::expressions::object::PascalMatrixConfigData;
use matm_model::expressions::object::PascalMatrixVariant;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;

use crate::calculator::calculate_expression_quiet;
use crate::calculator::method::calculate_arguments;
use crate::calculator::method::extract_limited_i32_argument;
use crate::calculator::method::raise_invalid_method_argument_type;
use crate::calculator::method::raise_unknown_method;
use crate::calculator::CalcResultOrErr;
use crate::calculator::CalcResult;
use crate::calculator::context::CalcContext;

pub async fn calculate_obj_matrix_method(
    ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
) -> CalcResultOrErr {
    if MethodData::ZEROS.eq(&method_obj.func.name) {
        calculate_obj_matrix_method_zeros(ctx, method_obj, &method_obj.func.arguments).await
    } else if MethodData::ONES.eq(&method_obj.func.name) {
        calculate_obj_matrix_method_ones(ctx, method_obj, &method_obj.func.arguments).await
    } else if MethodData::DIAGONAL.eq(&method_obj.func.name) {
        calculate_obj_matrix_method_diagonal(ctx, method_obj, &method_obj.func.arguments).await
    } else if MethodData::IDENTITY.eq(&method_obj.func.name) {
        calculate_obj_matrix_method_identity(ctx, method_obj, &method_obj.func.arguments).await
    } else if MethodData::OF.eq(&method_obj.func.name) {
        calculate_obj_matrix_method_of(ctx, method_obj, &method_obj.func.arguments).await
    } else if MethodData::HILBERT.eq(&method_obj.func.name) {
        calculate_obj_matrix_method_hilbert(ctx, method_obj, &method_obj.func.arguments).await
    } else if MethodData::PASCAL.eq(&method_obj.func.name) {
        calculate_pascal(ctx, method_obj, &method_obj.func.arguments).await
    } else if MethodData::VANDERMONDE.eq(&method_obj.func.name) {
        calculate_vandermonde(ctx, method_obj, &method_obj.func.arguments).await
    } else {
        raise_unknown_method(method_obj)
    }
}

async fn calculate_obj_matrix_method_zeros(
    ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    args: &Vec<Rc<Expression>>,
) -> CalcResultOrErr {
    match args.len() {
        1 => {
            calculate_obj_matrix_method_zeros_square(ctx, method_obj, args).await
        }
        2 => {
            calculate_obj_matrix_method_zeros_rectangular(ctx, method_obj, args).await
        }
        _ => {
            Err(CalcError::from_str("Expected 1 or 2 arguments of positive integer type."))
        }
    }
}

async fn calculate_obj_matrix_method_zeros_square(
    ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    args: &Vec<Rc<Expression>>,
) -> CalcResultOrErr {
    if let Some(calc) = calculate_arguments(ctx, method_obj, vec![0]).await? {
        return Ok(calc);
    }
    let size: i32 = extract_limited_i32_argument(args, 0, 1, 100).await?;
    let mx = MatrixData::zeros(ctx.rules().radix, size as usize, size as usize)?;
    return Ok(CalcResult::one(mx.to_rc_expr()));
}

async fn calculate_obj_matrix_method_zeros_rectangular(
    ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    args: &Vec<Rc<Expression>>,
) -> CalcResultOrErr {
    if let Some(calc) = calculate_arguments(ctx, method_obj, vec![0, 1]).await? {
        return Ok(calc);
    }
    let rows: i32 = extract_limited_i32_argument(args, 0, 1, 100).await?;
    let cols: i32 = extract_limited_i32_argument(args, 1, 1, 100).await?;
    let mx = MatrixData::zeros(ctx.rules().radix, rows as usize, cols as usize)?;
    Ok(CalcResult::one(mx.to_rc_expr()))
}

async fn calculate_obj_matrix_method_ones(
    ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    args: &Vec<Rc<Expression>>,
) -> CalcResultOrErr {
    match args.len() {
        1 => {
            calculate_obj_matrix_method_ones_square(ctx, method_obj, args).await
        }
        2 => {
            calculate_obj_matrix_method_ones_rectangular(ctx, method_obj, args).await
        }
        _ => {
            Err(CalcError::from_str("Expected 1 or 2 arguments of positive integer type."))
        }
    }
}

async fn calculate_obj_matrix_method_ones_square(
    ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    args: &Vec<Rc<Expression>>,
) -> CalcResultOrErr {
    if let Some(calc) = calculate_arguments(ctx, method_obj, vec![0]).await? {
        return Ok(calc);
    }
    let size: i32 = extract_limited_i32_argument(args, 0, 1, 100).await?;
    let mx = MatrixData::ones(ctx.rules().radix, size as usize, size as usize)?;
    return Ok(CalcResult::one(mx.to_rc_expr()));
}

async fn calculate_obj_matrix_method_ones_rectangular(
    ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    args: &Vec<Rc<Expression>>,
) -> CalcResultOrErr {
    if let Some(calc) = calculate_arguments(ctx, method_obj, vec![0, 1]).await? {
        return Ok(calc);
    }
    let rows: i32 = extract_limited_i32_argument(args, 0, 1, 100).await?;
    let cols: i32 = extract_limited_i32_argument(args, 1, 1, 100).await?;
    let mx = MatrixData::ones(ctx.rules().radix, rows as usize, cols as usize)?;
    return Ok(CalcResult::one(mx.to_rc_expr()));
}

async fn calculate_obj_matrix_method_diagonal(
    ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    args: &Vec<Rc<Expression>>,
) -> CalcResultOrErr {
    // calculate matrix size
    if args.len() < 3 {
        return Err(CalcError::from_str("Expected 2 arguments for size (rows, columns) and 1 or more arguments for diagonal values."));
    }
    if let Some(calc) = calculate_arguments(ctx, method_obj, vec![0, 1]).await? {
        return Ok(calc);
    }
    let rows: usize = extract_limited_i32_argument(args, 0, 1, 100).await? as usize;
    let cols: usize = extract_limited_i32_argument(args, 1, 1, 100).await? as usize;
    let diagonal_count = min(rows, cols);
    if args.len() != (2 + diagonal_count) {
        return Err(CalcError::from_string(format!(
            "Expected 2 arguments for size (rows, columns) and {diagonal_count} arguments for diagonal values.")));
    }
    // create elements
    let mut element_index = 2;
    let total_count = rows * cols;
    let elements: Vec<Rc<Expression>> = (1..=total_count).into_iter()
        .map(|i| {
            let row: usize = (i - 1) / cols + 1;
            let col: usize = (i - 1) % cols + 1;
            if row == col {
                let el = args.get(element_index).unwrap();
                element_index += 1;
                Rc::clone(el)
            } else {
                ctx.fa().rz0()
            }
        })
        .collect();
    let mx = MatrixData::from_vector(rows, cols, elements)?;
    return Ok(CalcResult::one(mx.to_rc_expr()));
}

async fn calculate_obj_matrix_method_identity(
    ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    args: &Vec<Rc<Expression>>,
) -> CalcResultOrErr {
   match args.len() {
       1 => {
           if let Some(calc) = calculate_arguments(ctx, method_obj, vec![0]).await? {
               return Ok(calc);
           }
           let size: i32 = extract_limited_i32_argument(args, 0, 1, 100).await?;
           let mx = MatrixData::identity(ctx.rules().radix, size as usize)?;
           return Ok(CalcResult::one(mx.to_rc_expr()));
       }
       _ => {
           return Err(CalcError::from_str("Expected 1 argument of positive integer type."));
       }
   }
}

async fn calculate_obj_matrix_method_of(
    ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    args: &Vec<Rc<Expression>>,
) -> CalcResultOrErr {
   if args.len() <= 2 {
       return Err(CalcError::from_str("Expected 2 first arguments defining matrix size (row and column)."));
   }
   if let Some(calc) = calculate_arguments(ctx, method_obj, vec![0, 1]).await? {
       return Ok(calc);
   }
   let rows: usize = extract_limited_i32_argument(args, 0, 1, 100).await? as usize;
   let cols: usize = extract_limited_i32_argument(args, 1, 1, 100).await? as usize;
   let exp_elements = rows * cols;
   if args.len() != (2 + exp_elements) {
       return Err(CalcError::from_string(format!(
           "Expected 2+{} arguments defining matrix size and elements.",
           exp_elements)));
   }
   let elements: Vec<Rc<Expression>> = args.iter()
       .skip(2)
       .map(|e| Rc::clone(e))
       .collect();
   let mx = MatrixData::from_vector(rows, cols, elements)?;
   return Ok(CalcResult::one(mx.to_rc_expr()));
}

async fn calculate_obj_matrix_method_hilbert(
    ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    args: &Vec<Rc<Expression>>,
) -> CalcResultOrErr {
    if args.len() != 1 {
        return Err(CalcError::from_str("Expected 1 argument defining matrix size."));
    }
    if let Some(calc) = calculate_arguments(ctx, method_obj, vec![0]).await? {
        return Ok(calc);
    }
    let size: usize = extract_limited_i32_argument(args, 0, 1, 100).await? as usize;
    let total_count = size * size;
    let elements: Vec<Rc<Expression>> = (1..=total_count).into_iter()
        .map(|i| {
            let row: usize = (i - 1) / size + 1;
            let col: usize = (i - 1) % size + 1;
            if i == 1 {
                ctx.fa().rz1()
            } else {
                let denominator = (row + col - 1) as i32;
                ctx.fa().rfi_u(1, denominator)
            }
        })
        .collect();
    let mx = MatrixData::from_vector(size, size, elements)?;
    return Ok(CalcResult::one(mx.to_rc_expr()));
}

async fn calculate_pascal(
    ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    args: &Vec<Rc<Expression>>,
) -> CalcResultOrErr {
    return match args.len() {
        1 => {
            if let Some(calc) = calculate_arguments(ctx, method_obj, vec![0]).await? {
                return Ok(calc);
            }
            let size: usize = extract_limited_i32_argument(args, 0, 1, 100).await? as usize;

            let config = PascalMatrixConfigData::new();
            return calculate_pascal_by_config(ctx, size, &config).await;
        }
        2 => {
            if let Some(calc) = calculate_arguments(ctx, method_obj, vec![0]).await? {
                return Ok(calc);
            }
            let size: usize = extract_limited_i32_argument(args, 0, 1, 100).await? as usize;

            let config_arg = method_obj.func.arguments.get(1).unwrap();
            //TODO add method extracting evaluated expression
            let config_arg_calc = calculate_expression_quiet(ctx, config_arg).await?;
            if let Expression::Object(builder_obj) = config_arg_calc.as_ref() {
                if let ObjectType::PascalMatrixConfig(config) = &builder_obj.object_type {
                    return calculate_pascal_by_config(ctx, size, config).await;
                }
            }

            return raise_invalid_method_argument_type(
                method_obj,
                ObjectData::new(ObjectType::PascalMatrixConfig(PascalMatrixConfigData::new())).to_rc_expr(),
                config_arg_calc
            );
        }
        _ => {
            Err(CalcError::from_str("Expected 1st argument defining matrix size and 2nd optional argument containing configuration."))
        }
    }
}

async fn calculate_pascal_by_config(
    ctx: &mut CalcContext<'_>,
    size: usize,
    config: &PascalMatrixConfigData,
) -> CalcResultOrErr {
    match config.variant {
        PascalMatrixVariant::Symmetric => calculate_pascal_symmetric(ctx, size, config).await,
        PascalMatrixVariant::LowerTriangular => calculate_pascal_lower(ctx, size, config).await,
        PascalMatrixVariant::UpperTriangular => calculate_pascal_upper(ctx, size, config).await,
    }
}

async fn calculate_pascal_symmetric(
    ctx: &mut CalcContext<'_>,
    size: usize,
    _config: &PascalMatrixConfigData,
) -> CalcResultOrErr {
    let mut mx = MatrixData::zeros(ctx.rules().radix, size, size)?;
    for idx in mx.iter_right_down() {
        if 1 == idx.row || 1 == idx.column {
            mx.set(&idx, ctx.fa().rz1())?;
        } else {
            let left: &Rc<Expression> = mx.get(&idx.left()?)?;
            let up: &Rc<Expression> = mx.get(&idx.up()?)?;
            let sum = BinaryData::add(Rc::clone(left), Rc::clone(up)).to_rc_expr();
            let calc_sum = calculate_expression_quiet(ctx, &sum).await?;
            mx.set(&idx, calc_sum)?;
        }
    }
    Ok(CalcResult::one(mx.to_rc_expr()))
}

async fn calculate_pascal_lower(
    ctx: &mut CalcContext<'_>,
    size: usize,
    _config: &PascalMatrixConfigData,
) -> CalcResultOrErr {
    let mut mx = MatrixData::zeros(ctx.rules().radix, size, size)?;
    for idx in mx.iter_right_down() {
        if idx.row == idx.column || 1 == idx.column {
            mx.set(&idx, ctx.fa().rz1())?;
        } else if idx.row < idx.column {
            mx.set(&idx, ctx.fa().rz0())?;
        } else {
            let left_up: &Rc<Expression> = mx.get(&idx.left_up()?)?;
            let up: &Rc<Expression> = mx.get(&idx.up()?)?;
            let sum = BinaryData::add(Rc::clone(left_up), Rc::clone(up)).to_rc_expr();
            let calc_sum = calculate_expression_quiet(ctx, &sum).await?;
            mx.set(&idx, calc_sum)?;
        }
    }
    Ok(CalcResult::one(mx.to_rc_expr()))
}

async fn calculate_pascal_upper(
    ctx: &mut CalcContext<'_>,
    size: usize,
    _config: &PascalMatrixConfigData,
) -> CalcResultOrErr {
    let mut mx = MatrixData::zeros(ctx.rules().radix, size, size)?;
    for idx in mx.iter_right_down() {
        if idx.row == idx.column || 1 == idx.row {
            mx.set(&idx, ctx.fa().rz1())?;
        } else if idx.column < idx.row {
            mx.set(&idx, ctx.fa().rz0())?;
        } else {
            let left: &Rc<Expression> = mx.get(&idx.left()?)?;
            let left_up: &Rc<Expression> = mx.get(&idx.left_up()?)?;
            let sum = BinaryData::add(Rc::clone(left), Rc::clone(left_up)).to_rc_expr();
            let calc_sum = calculate_expression_quiet(ctx, &sum).await?;
            mx.set(&idx, calc_sum)?;
        }
    }
    Ok(CalcResult::one(mx.to_rc_expr()))
}

/// Creates Vandermonde matrix in form:
///
/// ```markdown
/// 1  x_1 x_1^2 x_1^3
/// 1  x_2 x_2^2 x_2^3
/// 1  x_3 x_3^2 x_3^3
/// ```
///
/// # Arguments (args)
///
/// - number of columns
/// - x1 element
/// - x2 element
/// - x3 element
/// - ...
async fn calculate_vandermonde(
    ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    args: &Vec<Rc<Expression>>,
) -> CalcResultOrErr {
    if args.len() <= 2 {
        return Err(CalcError::from_str("Expected first argument defining number of columns and next arguments as elements of 2nd column."));
    }
    if let Some(calc) = calculate_arguments(ctx, method_obj, vec![0]).await? {
        return Ok(calc);
    }
    let cols: usize = extract_limited_i32_argument(args, 0, 1, 100).await? as usize;
    let rows = args.len() - 1;
    let total_count = rows * cols;
    let elements: Vec<Rc<Expression>> = (1..=total_count).into_iter()
        .map(|i| {
            let row: usize = (i - 1) / cols + 1;
            let col: usize = (i - 1) % cols + 1;
            if 1 == col {
                ctx.fa().rz1()
            } else if 2 == col {
                let x: &Rc<Expression> = args.get(row).unwrap();
                Rc::clone(x)
            } else {
                let x: &Rc<Expression> = args.get(row).unwrap();
                ExpData::new(Rc::clone(x), ctx.fa().rzu(col - 1).unwrap()).to_rc_expr()
            }
        })
        .collect();
    let mx = MatrixData::from_vector(rows, cols, elements)?;
    return Ok(CalcResult::one(mx.to_rc_expr()));
}

