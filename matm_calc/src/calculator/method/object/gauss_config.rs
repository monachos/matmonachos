use matm_model::expressions::method::MethodData;
use matm_model::expressions::object::GaussConfigData;
use matm_model::expressions::object::GaussPivotType;
use matm_model::expressions::object::ObjectData;
use matm_model::expressions::object::ObjectType;
use matm_model::expressions::ToExpression;

use crate::calculator::method::raise_unknown_method;
use crate::calculator::method::validate_method_no_argument_expected;
use crate::calculator::CalcResult;
use crate::calculator::CalcResultOrErr;

pub async fn calculate_obj_gauss_config_method(
    method_obj: &MethodData,
    data: &GaussConfigData,
) -> CalcResultOrErr {
    return if MethodData::PIVOT_LESS.eq(&method_obj.func.name) {
        calculate_pivot_less(method_obj, data).await
    } else if MethodData::PIVOT_ZERO.eq(&method_obj.func.name) {
        calculate_pivot_zero(method_obj, data).await
    } else if MethodData::IDENTITY.eq(&method_obj.func.name) {
        calculate_identity(method_obj, data).await
    } else if MethodData::REDUCE_LOWER.eq(&method_obj.func.name) {
        calculate_reduce_lower(method_obj, data).await
    } else if MethodData::REDUCE_UPPER.eq(&method_obj.func.name) {
        calculate_reduce_upper(method_obj, data).await
    } else {
        raise_unknown_method(method_obj)
    }
}

async fn calculate_pivot_less(
    method_obj: &MethodData,
    data: &GaussConfigData,
) -> CalcResultOrErr {
    validate_method_no_argument_expected(method_obj)?;
    let new_data = GaussConfigData {
        pivot: GaussPivotType::IfLessThanMaxAbs,
        ..*data
    };
    return Ok(CalcResult::one(
        ObjectData::new(ObjectType::GaussConfig(new_data)).to_rc_expr(),
    ));
}

async fn calculate_pivot_zero(
    method_obj: &MethodData,
    data: &GaussConfigData,
) -> CalcResultOrErr {
    validate_method_no_argument_expected(method_obj)?;
    let new_data = GaussConfigData {
        pivot: GaussPivotType::IfZero,
        ..*data
    };
    return Ok(CalcResult::one(
        ObjectData::new(ObjectType::GaussConfig(new_data)).to_rc_expr(),
    ));
}

async fn calculate_identity(
    method_obj: &MethodData,
    data: &GaussConfigData,
) -> CalcResultOrErr {
    validate_method_no_argument_expected(method_obj)?;
    let new_data = GaussConfigData {
        make_identity: true,
        ..*data
    };
    return Ok(CalcResult::one(
        ObjectData::new(ObjectType::GaussConfig(new_data)).to_rc_expr(),
    ));
}

async fn calculate_reduce_lower(
    method_obj: &MethodData,
    data: &GaussConfigData,
) -> CalcResultOrErr {
    validate_method_no_argument_expected(method_obj)?;
    let new_data = GaussConfigData {
        reduce_lower: true,
        ..*data
    };
    return Ok(CalcResult::one(
        ObjectData::new(ObjectType::GaussConfig(new_data)).to_rc_expr(),
    ));
}

async fn calculate_reduce_upper(
    method_obj: &MethodData,
    data: &GaussConfigData,
) -> CalcResultOrErr {
    validate_method_no_argument_expected(method_obj)?;
    let new_data = GaussConfigData {
        reduce_upper: true,
        ..*data
    };
    return Ok(CalcResult::one(
        ObjectData::new(ObjectType::GaussConfig(new_data)).to_rc_expr(),
    ));
}

#[cfg(test)]
mod tests {
    use rstest::rstest;

    use matm_model::expressions::method::MethodData;
    use matm_model::expressions::object::GaussConfigData;
    use matm_model::expressions::object::GaussPivotType;
    use matm_model::expressions::object::ObjectData;
    use matm_model::expressions::object::ObjectType;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;
    use matm_model::testing::asserts::assert_result_error_that;

    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::testing::asserts::test_calculation;
    use crate::testing::asserts::test_calculation_ok;

    #[test]
    fn test_pivot_less() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::GaussConfig(GaussConfigData::new())).to_rc_expr(),
            "pivot_less".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_object()
            .is_type(ObjectType::GaussConfig(GaussConfigData {
                pivot: GaussPivotType::IfLessThanMaxAbs,
                make_identity: false,
                reduce_upper: false,
                reduce_lower: false,
            }));
    }

    #[test]
    fn test_pivot_zero() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::GaussConfig(GaussConfigData::new())).to_rc_expr(),
            "pivot_zero".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_object()
            .is_type(ObjectType::GaussConfig(GaussConfigData {
                pivot: GaussPivotType::IfZero,
                make_identity: false,
                reduce_upper: false,
                reduce_lower: false,
            }));
    }

    #[test]
    fn test_identity() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::GaussConfig(GaussConfigData::new())).to_rc_expr(),
            "identity".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_object()
            .is_type(ObjectType::GaussConfig(GaussConfigData {
                pivot: GaussPivotType::No,
                make_identity: true,
                reduce_upper: false,
                reduce_lower: false,
            }));
    }

    #[test]
    fn test_reduce_lower() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::GaussConfig(GaussConfigData::new())).to_rc_expr(),
            "reduce_lower".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_object()
            .is_type(ObjectType::GaussConfig(GaussConfigData {
                pivot: GaussPivotType::No,
                make_identity: false,
                reduce_upper: false,
                reduce_lower: true,
            }));
    }

    #[test]
    fn test_reduce_upper() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::GaussConfig(GaussConfigData::new())).to_rc_expr(),
            "reduce_upper".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_object()
            .is_type(ObjectType::GaussConfig(GaussConfigData {
                pivot: GaussPivotType::No,
                make_identity: false,
                reduce_upper: true,
                reduce_lower: false,
            }));
    }

    #[rstest]
    #[case("pivot_less")]
    #[case("pivot_zero")]
    #[case("identity")]
    #[case("reduce_lower")]
    #[case("reduce_upper")]
    fn test_method_no_arg_expected_error(
        #[case] method_name: &str,
    ) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::GaussConfig(GaussConfigData::new())).to_rc_expr(),
            method_name.to_owned(),
            vec![ctx.fa().rzi_u(2)])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message(format!(
                "Method {}() cannot have any arguments.",
                method_name
            ).as_str());
    }

    #[test]
    fn test_unknown_method() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::GaussConfig(GaussConfigData::new())).to_rc_expr(),
            "abc".to_owned(),
            Vec::new())
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("object {gauss_config} does not have abc() method.");
    }
}

