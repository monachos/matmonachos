#[cfg(test)]
mod tests {
    use crate::calculator::calculate_expression;
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::testing::asserts::test_calculation;
    use crate::testing::asserts::test_calculation_ok;
    use futures::executor::block_on;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::method::MethodData;
    use matm_model::expressions::object::ObjectData;
    use matm_model::expressions::object::ObjectType;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;
    use matm_model::testing::asserts::assert_result_error_that;


    #[test]
    fn test_obj_cracovian_zeros_2() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Cracovian).to_rc_expr(),
            "zeros".to_owned(),
            vec![ctx.fa().rzi_u(2)])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_cracovian()
            .has_rows(2)
            .has_columns(2)
            .has_i_i_zero(1, 1)
            .has_i_i_zero(2, 1)
            .has_i_i_zero(1, 2)
            .has_i_i_zero(2, 2);
    }

    #[test]
    fn test_obj_cracovian_zeros_bin() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Cracovian).to_rc_expr(),
            "zeros".to_owned(),
            vec![BinaryData::add(ctx.fa().rzi_u(1), ctx.fa().rzi_u(1)).to_rc_expr()])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_cracovian()
            .has_rows(2)
            .has_columns(2)
            .has_i_i_zero(1, 1)
            .has_i_i_zero(2, 1)
            .has_i_i_zero(1, 2)
            .has_i_i_zero(2, 2);
    }

    #[test]
    fn test_obj_cracovian_zeros_3_2() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Cracovian).to_rc_expr(),
            "zeros".to_owned(),
            vec![ctx.fa().rzi_u(3), ctx.fa().rzi_u(2)])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_cracovian()
            .has_columns(3)
            .has_rows(2)
            .has_i_i_zero(1, 1)
            .has_i_i_zero(2, 1)
            .has_i_i_zero(3, 1)
            .has_i_i_zero(1, 2)
            .has_i_i_zero(2, 2)
            .has_i_i_zero(3, 2);
    }

    #[test]
    fn test_obj_cracovian_zeros_bin_bin() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Cracovian).to_rc_expr(),
            "zeros".to_owned(),
            vec![
                BinaryData::add(ctx.fa().rzi_u(1), ctx.fa().rzi_u(2)).to_rc_expr(),
                BinaryData::add(ctx.fa().rzi_u(1), ctx.fa().rzi_u(1)).to_rc_expr(),
            ])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_cracovian()
            .has_columns(3)
            .has_rows(2)
            .has_i_i_zero(1, 1)
            .has_i_i_zero(2, 1)
            .has_i_i_zero(3, 1)
            .has_i_i_zero(1, 2)
            .has_i_i_zero(2, 2)
            .has_i_i_zero(3, 2);
    }

    #[test]
    fn test_obj_cracovian_zeros_no_args() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Cracovian).to_rc_expr(),
            "zeros".to_owned(),
            vec![])
            .to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr));

        // check result
        assert_result_error_that(result)
            .has_message("Expected 1 or 2 arguments of positive integer type.");
    }

    #[test]
    fn test_obj_cracovian_zeros_3_args() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Cracovian).to_rc_expr(),
            "zeros".to_owned(),
            vec![ctx.fa().rzi_u(2), ctx.fa().rzi_u(2), ctx.fa().rzi_u(2)])
            .to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr));

        // check result
        assert_result_error_that(result)
            .has_message("Expected 1 or 2 arguments of positive integer type.");
    }

    #[test]
    fn test_obj_cracovian_zeros_a() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Cracovian).to_rc_expr(),
            "zeros".to_owned(),
            vec![ctx.fa().rsn("a")])
            .to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr));

        // check result
        assert_result_error_that(result)
            .has_message("Expected argument (1) of integer type.");
    }

    #[test]
    fn test_obj_cracovian_zeros_2_a() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Cracovian).to_rc_expr(),
            "zeros".to_owned(),
            vec![ctx.fa().rzi_u(2), ctx.fa().rsn("a")])
            .to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr));

        // check result
        assert_result_error_that(result)
            .has_message("Expected argument (2) of integer type.");
    }

    #[test]
    fn test_obj_cracovian_zeros_1000() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Cracovian).to_rc_expr(),
            "zeros".to_owned(),
            vec![ctx.fa().rzi_u(1000)])
            .to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr));

        // check result
        assert_result_error_that(result)
            .has_message("Expected argument (1) of integer in range 1..100.");
    }

    #[test]
    fn test_obj_cracovian_zeros_2_1000() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Cracovian).to_rc_expr(),
            "zeros".to_owned(),
            vec![ctx.fa().rzi_u(2), ctx.fa().rzi_u(1000)])
            .to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr));

        // check result
        assert_result_error_that(result)
            .has_message("Expected argument (2) of integer in range 1..100.");
    }

    #[test]
    fn test_obj_cracovian_of_3_2() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Cracovian).to_rc_expr(),
            "of".to_owned(),
            vec![
                ctx.fa().rzi_u(3),
                ctx.fa().rzi_u(2),
                ctx.fa().rzi_u(11), ctx.fa().rzi_u(21), ctx.fa().rzi_u(31),
                ctx.fa().rzi_u(12), ctx.fa().rzi_u(22), ctx.fa().rzi_u(32)
            ])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_cracovian()
            .has_columns(3)
            .has_rows(2)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(11);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(21);})
            .is_i_i_integer_that(3, 1, |e| {e.is_int(31);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(12);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(22);})
            .is_i_i_integer_that(3, 2, |e| {e.is_int(32);})
            ;
    }

    #[test]
    fn test_obj_cracovian_of_bin_bin() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Cracovian).to_rc_expr(),
            "of".to_owned(),
            vec![
                BinaryData::add(ctx.fa().rzi_u(1), ctx.fa().rzi_u(2)).to_rc_expr(),
                BinaryData::add(ctx.fa().rzi_u(1), ctx.fa().rzi_u(1)).to_rc_expr(),
                ctx.fa().rzi_u(11), ctx.fa().rzi_u(21), ctx.fa().rzi_u(31),
                ctx.fa().rzi_u(12), ctx.fa().rzi_u(22), ctx.fa().rzi_u(32)
            ])
            .to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_cracovian()
            .has_columns(3)
            .has_rows(2)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(11);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(21);})
            .is_i_i_integer_that(3, 1, |e| {e.is_int(31);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(12);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(22);})
            .is_i_i_integer_that(3, 2, |e| {e.is_int(32);})
            ;
    }

    #[test]
    fn test_obj_cracovian_of_no_size() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Cracovian).to_rc_expr(),
            "of".to_owned(),
            vec![])
            .to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr));

        // check result
        assert_result_error_that(result)
            .has_message("Expected 2 first arguments defining cracovian size (row and column).");
    }

    #[test]
    fn test_obj_cracovian_of_2_args() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Cracovian).to_rc_expr(),
            "of".to_owned(),
            vec![ctx.fa().rzi_u(2), ctx.fa().rzi_u(2)])
            .to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr));

        // check result
        assert_result_error_that(result)
            .has_message("Expected 2 first arguments defining cracovian size (row and column).");
    }

    #[test]
    fn test_obj_cracovian_of_a_3() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Cracovian).to_rc_expr(),
            "of".to_owned(),
            vec![ctx.fa().rsn("a"), ctx.fa().rzi_u(3), ctx.fa().rz0()])
            .to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr));

        // check result
        assert_result_error_that(result)
            .has_message("Expected argument (1) of integer type.");
    }

    #[test]
    fn test_obj_cracovian_of_2_a() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Cracovian).to_rc_expr(),
            "of".to_owned(),
            vec![ctx.fa().rzi_u(2), ctx.fa().rsn("a"), ctx.fa().rz0()])
            .to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr));

        // check result
        assert_result_error_that(result)
            .has_message("Expected argument (2) of integer type.");
    }

    #[test]
    fn test_obj_cracovian_of_1000_3() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Cracovian).to_rc_expr(),
            "of".to_owned(),
            vec![ctx.fa().rzi_u(1000), ctx.fa().rzi_u(3), ctx.fa().rz0()])
            .to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr));

        // check result
        assert_result_error_that(result)
            .has_message("Expected argument (1) of integer in range 1..100.");
    }

    #[test]
    fn test_obj_cracovian_of_2_1000() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Cracovian).to_rc_expr(),
            "of".to_owned(),
            vec![ctx.fa().rzi_u(2), ctx.fa().rzi_u(1000), ctx.fa().rz0()])
            .to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr));

        // check result
        assert_result_error_that(result)
            .has_message("Expected argument (2) of integer in range 1..100.");
    }

    #[test]
    fn test_obj_cracovian_of_3_2_missing_elements() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Cracovian).to_rc_expr(),
            "of".to_owned(),
            vec![ctx.fa().rzi_u(3), ctx.fa().rzi_u(2),
                 ctx.fa().rz0(), ctx.fa().rz0(), ctx.fa().rz0()])
            .to_rc_expr();

        let result = block_on(calculate_expression(&mut ctx, &expr));

        // check result
        assert_result_error_that(result)
            .has_message("Expected 2+6 arguments defining cracovian size and elements.");
    }

    #[test]
    fn test_unknown_method() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::Cracovian).to_rc_expr(),
            "abc".to_owned(),
            Vec::new())
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("object {cracovian} does not have abc() method.");
    }
}

