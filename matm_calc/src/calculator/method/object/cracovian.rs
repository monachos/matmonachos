use std::rc::Rc;

use crate::calculator::method::calculate_arguments;
use crate::calculator::method::extract_limited_i32_argument;
use crate::calculator::method::raise_unknown_method;
use crate::calculator::CalcResultOrErr;
use crate::calculator::CalcResult;
use crate::calculator::context::CalcContext;
use matm_error::error::CalcError;
use matm_model::expressions::cracovians::CracovianData;
use matm_model::expressions::method::MethodData;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;


pub async fn calculate_obj_cracovian_method(
    ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
) -> CalcResultOrErr {
    if MethodData::ZEROS.eq(&method_obj.func.name) {
        calculate_obj_cracovian_method_zeros(ctx, method_obj, &method_obj.func.arguments).await
    } else if MethodData::OF.eq(&method_obj.func.name) {
        calculate_obj_cracovian_method_of(ctx, method_obj, &method_obj.func.arguments).await
    } else {
        raise_unknown_method(method_obj)
    }
}

async fn calculate_obj_cracovian_method_zeros(
    ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    args: &Vec<Rc<Expression>>,
) -> CalcResultOrErr {
    match args.len() {
        1 => {
            calculate_obj_cracovian_method_zeros_square(ctx, method_obj, args).await
        }
        2 => {
            calculate_obj_cracovian_method_zeros_rectangular(ctx, method_obj, args).await
        }
        _ => {
            Err(CalcError::from_str("Expected 1 or 2 arguments of positive integer type."))
        }
    }
}

async fn calculate_obj_cracovian_method_zeros_square(
    ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    args: &Vec<Rc<Expression>>,
) -> CalcResultOrErr {
    if let Some(calc) = calculate_arguments(ctx, method_obj, vec![0]).await? {
        return Ok(calc);
    }
    let size: usize = extract_limited_i32_argument(args, 0, 1, 100).await? as usize;
    let mx = CracovianData::zeros(ctx.rules().radix, size, size)?;
    return Ok(CalcResult::one(mx.to_rc_expr()));
}

async fn calculate_obj_cracovian_method_zeros_rectangular(
    ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    args: &Vec<Rc<Expression>>,
) -> CalcResultOrErr {
    if let Some(calc) = calculate_arguments(ctx, method_obj, vec![0, 1]).await? {
        return Ok(calc);
    }
    let rows: usize = extract_limited_i32_argument(args, 0, 1, 100).await? as usize;
    let cols: usize = extract_limited_i32_argument(args, 1, 1, 100).await? as usize;
    let mx = CracovianData::zeros(ctx.rules().radix, rows, cols)?;
    return Ok(CalcResult::one(mx.to_rc_expr()));
}

async fn calculate_obj_cracovian_method_of(
    ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    args: &Vec<Rc<Expression>>,
) -> CalcResultOrErr {
    if args.len() <= 2 {
        return Err(CalcError::from_str("Expected 2 first arguments defining cracovian size (row and column)."));
    }
    if let Some(calc) = calculate_arguments(ctx, method_obj, vec![0, 1]).await? {
        return Ok(calc);
    }
    let rows: usize = extract_limited_i32_argument(args, 0, 1, 100).await? as usize;
    let cols: usize = extract_limited_i32_argument(args, 1, 1, 100).await? as usize;
    let exp_elements = rows * cols;
    if args.len() != (2 + exp_elements) {
        return Err(CalcError::from_string(format!(
            "Expected 2+{} arguments defining cracovian size and elements.",
            exp_elements)));
    }
    let elements: Vec<Rc<Expression>> = args.iter()
        .skip(2)
        .map(|e| Rc::clone(e))
        .collect();
    let mx = CracovianData::from_vector(rows, cols, elements)?;
    return Ok(CalcResult::one(mx.to_rc_expr()));
}

