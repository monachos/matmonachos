use matm_model::expressions::method::MethodData;
use matm_model::expressions::object::InvConfigData;
use matm_model::expressions::object::InvMethod;
use matm_model::expressions::object::ObjectData;
use matm_model::expressions::object::ObjectType;
use matm_model::expressions::ToExpression;

use crate::calculator::method::raise_unknown_method;
use crate::calculator::method::validate_method_no_argument_expected;
use crate::calculator::CalcResult;
use crate::calculator::CalcResultOrErr;

pub async fn calculate_obj_inv_config_method(
    method_obj: &MethodData,
    data: &InvConfigData,
) -> CalcResultOrErr {
    if MethodData::ADJ.eq(&method_obj.func.name) {
        calculate_adj(method_obj, data).await
    } else if MethodData::GAUSS.eq(&method_obj.func.name) {
        calculate_gauss(method_obj, data).await
    } else {
        raise_unknown_method(method_obj)
    }
}

async fn calculate_adj(
    method_obj: &MethodData,
    data: &InvConfigData,
) -> CalcResultOrErr {
    validate_method_no_argument_expected(&method_obj)?;
    let new_data = InvConfigData {
        method: InvMethod::Adjugate,
        ..*data
    };
    return Ok(CalcResult::one(
        ObjectData::new(ObjectType::InvConfig(new_data)).to_rc_expr(),
    ));
}

async fn calculate_gauss(
    method_obj: &MethodData,
    data: &InvConfigData,
) -> CalcResultOrErr {
    validate_method_no_argument_expected(&method_obj)?;
    let new_data = InvConfigData {
        method: InvMethod::Gaussian,
        ..*data
    };
    return Ok(CalcResult::one(
        ObjectData::new(ObjectType::InvConfig(new_data)).to_rc_expr(),
    ));
}

#[cfg(test)]
mod tests {
    use rstest::rstest;

    use matm_model::expressions::method::MethodData;
    use matm_model::expressions::object::InvConfigData;
    use matm_model::expressions::object::InvMethod;
    use matm_model::expressions::object::ObjectData;
    use matm_model::expressions::object::ObjectType;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;
    use matm_model::testing::asserts::assert_result_error_that;

    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::testing::asserts::test_calculation;
    use crate::testing::asserts::test_calculation_ok;

    #[test]
    fn test_adj() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::InvConfig(InvConfigData::new())).to_rc_expr(),
            "adj".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_object()
            .is_type(ObjectType::InvConfig(InvConfigData {
                method: InvMethod::Adjugate,
            }));
    }

    #[test]
    fn test_gauss() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::InvConfig(InvConfigData::new())).to_rc_expr(),
            "gauss".to_owned(),
            vec![]).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_object()
            .is_type(ObjectType::InvConfig(InvConfigData {
                method: InvMethod::Gaussian,
            }));
    }

    #[rstest]
    #[case("adj")]
    #[case("gauss")]
    fn test_method_no_arg_expected_error(
        #[case] method_name: &str,
    ) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::InvConfig(InvConfigData::new())).to_rc_expr(),
            method_name.to_owned(),
            vec![ctx.fa().rzi_u(2)])
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message(format!(
                "Method {}() cannot have any arguments.",
                method_name
            ).as_str());
    }

    #[test]
    fn test_unknown_method() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ObjectData::new(ObjectType::InvConfig(InvConfigData::new())).to_rc_expr(),
            "abc".to_owned(),
            Vec::new())
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("object {inv_config} does not have abc() method.");
    }
}

