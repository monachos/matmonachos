use matm_error::error::CalcError;
use matm_model::expressions::bool::BoolData;
use matm_model::expressions::matrices::MatrixData;
use matm_model::expressions::method::MethodData;
use matm_model::expressions::object::DetConfigData;
use matm_model::expressions::object::GaussConfigData;
use matm_model::expressions::object::InvConfigData;
use matm_model::expressions::object::InvMethod;
use matm_model::expressions::object::ObjectData;
use matm_model::expressions::object::ObjectType;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;

use crate::calculator::calculate_expression_quiet;
use crate::calculator::linear::cholesky::decompose_cholesky;
use crate::calculator::linear::cholesky::DecomposeCholeskyConfig;
use crate::calculator::linear::det::calculate_determinant;
use crate::calculator::linear::det::CalculateDeterminantConfig;
use crate::calculator::linear::gaussian::perform_gaussian_elimination;
use crate::calculator::linear::gaussian::GaussianEliminationCalculatorConfig;
use crate::calculator::linear::inversion::invert_matrix_using_adjugate;
use crate::calculator::linear::inversion::invert_matrix_using_gaussian_elimination;
use crate::calculator::linear::inversion::InvertMatrixUsingAdjugateConfig;
use crate::calculator::linear::inversion::InvertMatrixUsingGaussConfig;
use crate::calculator::linear::rank::calculate_rank;
use crate::calculator::linear::rank::CalculateRankConfig;
use crate::calculator::method::validate_method_no_argument_expected;
use crate::calculator::method::{raise_invalid_method_argument_type, raise_unknown_method};
use crate::calculator::CalcResultOrErr;
use crate::calculator::CalcResult;
use crate::calculator::context::CalcContext;

pub async fn calculate_matrix_method(
    ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    obj: &MatrixData,
) -> CalcResultOrErr {
    if MethodData::DET.eq(&method_obj.func.name) {
        calculate_matrix_method_det(ctx, method_obj, obj).await
    } else if MethodData::INV.eq(&method_obj.func.name) {
        calculate_matrix_method_inv(ctx, method_obj, obj).await
    } else if MethodData::RANK.eq(&method_obj.func.name) {
        calculate_matrix_method_rank(ctx, method_obj, obj).await
    } else if MethodData::TRANSPOSE.eq(&method_obj.func.name) {
        calculate_matrix_method_transpose(ctx, method_obj, obj).await
    } else if MethodData::CHOLESKY.eq(&method_obj.func.name) {
        calculate_matrix_method_cholesky(ctx, method_obj, obj).await
    } else if MethodData::GAUSS.eq(&method_obj.func.name) {
        calculate_matrix_method_gauss(ctx, method_obj, obj).await
    } else if MethodData::ROWS.eq(&method_obj.func.name) {
        calculate_matrix_method_rows(ctx, method_obj, obj)
    } else if MethodData::COLUMNS.eq(&method_obj.func.name) {
        calculate_matrix_method_columns(ctx, method_obj, obj)
    } else if MethodData::IS_SQUARE.eq(&method_obj.func.name) {
        calculate_matrix_method_is_square(ctx, method_obj, obj)
    } else if MethodData::IS_ZERO.eq(&method_obj.func.name) {
        calculate_matrix_method_is_zero(ctx, method_obj, obj)
    } else if MethodData::IS_ONES.eq(&method_obj.func.name) {
        calculate_matrix_method_is_ones(ctx, method_obj, obj)
    } else if MethodData::IS_IDENTITY.eq(&method_obj.func.name) {
        calculate_matrix_method_is_identity(ctx, method_obj, obj)
    } else if MethodData::IS_UPPER_TRIANGULAR.eq(&method_obj.func.name) {
        calculate_matrix_method_is_upper_triangular(ctx, method_obj, obj)
    } else if MethodData::IS_STRICTLY_UPPER_TRIANGULAR.eq(&method_obj.func.name) {
        calculate_matrix_method_is_strictly_upper_triangular(ctx, method_obj, obj)
    } else if MethodData::IS_LOWER_TRIANGULAR.eq(&method_obj.func.name) {
        calculate_matrix_method_is_lower_triangular(ctx, method_obj, obj)
    } else if MethodData::IS_SYMMETRIC.eq(&method_obj.func.name) {
        calculate_matrix_method_is_symmetric(ctx, method_obj, obj)
    } else {
        raise_unknown_method(method_obj)
    }
}

async fn calculate_matrix_method_det(
    ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    obj: &MatrixData,
) -> CalcResultOrErr {
    match method_obj.func.arguments.len() {
        0 => {
            let cfg = CalculateDeterminantConfig::default();
            let result = calculate_determinant(ctx, &cfg, obj).await?;
            return Ok(CalcResult::one(result));
        }
        1 => {
            let builder = calculate_expression_quiet(
                ctx,
                method_obj.func.arguments.get(0).unwrap()
            ).await?;
            if let Expression::Object(builder_obj) = builder.as_ref() {
                if let ObjectType::DetConfig(config) = &builder_obj.object_type {
                    let mut cfg = CalculateDeterminantConfig::default();
                    cfg.method = config.method;
                    let result = calculate_determinant(ctx, &cfg, obj).await?;
                    return Ok(CalcResult::one(result));
                }
            }
            return raise_invalid_method_argument_type(
                method_obj,
                ObjectData::new(ObjectType::DetConfig(DetConfigData::new())).to_rc_expr(),
                builder
            );
        }
        _ => {
            //TODO bad error message
            return Err(CalcError::from_string(format!(
                "Method {}() cannot have any argument.",
                method_obj.func.name
            )));
        }
    }
}

async fn calculate_matrix_method_inv(
    ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    obj: &MatrixData,
) -> CalcResultOrErr {
    match method_obj.func.arguments.len() {
        0 => {
            let cfg = InvertMatrixUsingAdjugateConfig::default();
            let result = invert_matrix_using_adjugate(ctx, &cfg, obj).await?;
            return Ok(CalcResult::one(result.to_rc_expr()));
        }
        1 => {
            //TODO extract argument
            //TODO add method extracting evaluated expression
            let builder = calculate_expression_quiet(ctx, method_obj.func.arguments.get(0).unwrap()).await?;
            if let Expression::Object(builder_obj) = builder.as_ref() {
                if let ObjectType::InvConfig(config) = &builder_obj.object_type {
                    match config.method {
                        InvMethod::Adjugate => {
                            let cfg = InvertMatrixUsingAdjugateConfig::default();
                            let result = invert_matrix_using_adjugate(ctx, &cfg, obj).await?;
                            return Ok(CalcResult::one(result.to_rc_expr()));
                        }
                        InvMethod::Gaussian => {
                            let cfg = InvertMatrixUsingGaussConfig::default();
                            let result = invert_matrix_using_gaussian_elimination(ctx, &cfg, obj).await?;
                            return Ok(CalcResult::one(result.to_rc_expr()));
                        }
                    }
                }
            }
            return raise_invalid_method_argument_type(
                method_obj,
                ObjectData::new(ObjectType::InvConfig(InvConfigData::new())).to_rc_expr(),
                builder
            );
        }
        _ => {
            //TODO bad error message
            return Err(CalcError::from_string(format!(
                "Method {}() cannot have any argument.",
                method_obj.func.name
            )));
        }
    }
}

async fn calculate_matrix_method_rank(
    ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    obj: &MatrixData,
) -> CalcResultOrErr {
    validate_method_no_argument_expected(method_obj)?;
    let cfg = CalculateRankConfig::default();
    let result = calculate_rank(ctx, &cfg, obj).await?;
    return Ok(CalcResult::one(ctx.fa().rzu(result)?));
}

async fn calculate_matrix_method_transpose(
    _ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    obj: &MatrixData,
) -> CalcResultOrErr {
    validate_method_no_argument_expected(method_obj)?;
    let res = obj.transpose().to_rc_expr();
    return Ok(CalcResult::one(res));
}

async fn calculate_matrix_method_cholesky(
    ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    obj: &MatrixData,
) -> CalcResultOrErr {
    validate_method_no_argument_expected(method_obj)?;
    let cfg = DecomposeCholeskyConfig::default();
    let res = decompose_cholesky(ctx, &cfg, obj).await?;
    return Ok(CalcResult::one(res.to_rc_expr()));
}

async fn calculate_matrix_method_gauss(
    ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    obj: &MatrixData,
) -> CalcResultOrErr {
    match method_obj.func.arguments.len() {
        0 => {
            let mut cfg = GaussianEliminationCalculatorConfig::default();
            cfg.reduce_lower = true;
            let mut mx = obj.clone();
            perform_gaussian_elimination(ctx, &cfg, &mut mx).await?;
            return Ok(CalcResult::one(mx.to_rc_expr()));
        }
        1 => {
            let builder = calculate_expression_quiet(ctx, method_obj.func.arguments.get(0).unwrap()).await?;
            if let Expression::Object(builder_obj) = builder.as_ref() {
                if let ObjectType::GaussConfig(config) = &builder_obj.object_type {
                    let mut cfg = GaussianEliminationCalculatorConfig::default();
                    cfg.pivot = config.pivot;
                    cfg.make_identity = config.make_identity;
                    cfg.reduce_upper = config.reduce_upper;
                    cfg.reduce_lower = config.reduce_lower;
                    let mut mx = obj.clone();
                    perform_gaussian_elimination(ctx, &cfg, &mut mx).await?;
                    return Ok(CalcResult::one(mx.to_rc_expr()));
                }
            }
            return raise_invalid_method_argument_type(
                method_obj,
                ObjectData::new(ObjectType::GaussConfig(GaussConfigData::new())).to_rc_expr(),
                builder
            );
        }
        _ => {
            //TODO bad error message
            return Err(CalcError::from_string(format!(
                "Method {}() cannot have any argument.",
                method_obj.func.name
            )));
        }
    }
}

fn calculate_matrix_method_rows(
    ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    obj: &MatrixData,
) -> CalcResultOrErr {
    validate_method_no_argument_expected(method_obj)?;
    let res = ctx.fa().rzu(obj.rows())?;
    return Ok(CalcResult::one(res));
}

fn calculate_matrix_method_columns(
    ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    obj: &MatrixData,
) -> CalcResultOrErr {
    validate_method_no_argument_expected(method_obj)?;
    let res = ctx.fa().rzu(obj.columns())?;
    return Ok(CalcResult::one(res));
}

fn calculate_matrix_method_is_square(
    _ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    obj: &MatrixData,
) -> CalcResultOrErr {
    validate_method_no_argument_expected(method_obj)?;
    let res = BoolData::new(obj.is_square()).to_rc_expr();
    return Ok(CalcResult::one(res));
}

fn calculate_matrix_method_is_zero(
    _ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    obj: &MatrixData,
) -> CalcResultOrErr {
    validate_method_no_argument_expected(method_obj)?;
    let res = BoolData::new(obj.is_zero()).to_rc_expr();
    return Ok(CalcResult::one(res));
}

fn calculate_matrix_method_is_ones(
    _ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    obj: &MatrixData,
) -> CalcResultOrErr {
    validate_method_no_argument_expected(method_obj)?;
    let res = BoolData::new(obj.is_ones()).to_rc_expr();
    return Ok(CalcResult::one(res));
}

fn calculate_matrix_method_is_identity(
    _ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    obj: &MatrixData,
) -> CalcResultOrErr {
    validate_method_no_argument_expected(method_obj)?;
    let res = BoolData::new(obj.is_identity()).to_rc_expr();
    return Ok(CalcResult::one(res));
}

fn calculate_matrix_method_is_upper_triangular(
    _ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    obj: &MatrixData,
) -> CalcResultOrErr {
    validate_method_no_argument_expected(method_obj)?;
    let res = BoolData::new(obj.is_upper_triangular()).to_rc_expr();
    return Ok(CalcResult::one(res));
}

fn calculate_matrix_method_is_strictly_upper_triangular(
    _ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    obj: &MatrixData,
) -> CalcResultOrErr {
    validate_method_no_argument_expected(method_obj)?;
    let res = BoolData::new(obj.is_strictly_upper_triangular()).to_rc_expr();
    return Ok(CalcResult::one(res));
}

fn calculate_matrix_method_is_lower_triangular(
    _ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    obj: &MatrixData,
) -> CalcResultOrErr {
    validate_method_no_argument_expected(method_obj)?;
    let res = BoolData::new(obj.is_lower_triangular()).to_rc_expr();
    return Ok(CalcResult::one(res));
}

fn calculate_matrix_method_is_symmetric(
    _ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    obj: &MatrixData,
) -> CalcResultOrErr {
    validate_method_no_argument_expected(method_obj)?;
    let res = BoolData::new(obj.is_symmetric()).to_rc_expr();
    return Ok(CalcResult::one(res));
}
