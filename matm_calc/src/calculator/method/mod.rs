mod bool_tests;
mod int_tests;
mod matrix_tests;
mod transc_tests;
mod object;
pub mod frac;
pub mod int;
pub mod matrix;
pub mod transc;


use std::rc::Rc;

use crate::calculator::{calculate_expression_step, ModelData};
use crate::calculator::method::frac::calculate_frac_method;
use crate::calculator::method::int::calculate_int_method;
use crate::calculator::method::matrix::calculate_matrix_method;
use crate::calculator::method::object::calculate_object_method;
use crate::calculator::method::transc::calculate_transc_method;
use crate::calculator::CalcResultOrErr;
use crate::calculator::CalcResult;
use crate::calculator::context::CalcContext;
use matm_error::error::CalcError;
use matm_model::expressions::int::IntData;
use matm_model::expressions::method::MethodData;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;

pub(crate) async fn calculate_method_expression(
    ctx: &mut CalcContext<'_>,
    model: &ModelData<'_, Expression, MethodData>,
) -> CalcResultOrErr {
    // called object
    let calc_object = calculate_expression_step(ctx, &model.data.object).await?;
    if calc_object.calculated_count > 0 {
        let args_copy = model.data.func.arguments.iter()
            .map(|e| Rc::clone(e))
            .collect();
        let expr = MethodData::new(
            calc_object.object,
            model.data.func.name.clone(),
            args_copy).to_rc_expr();
        return Ok(CalcResult::new(expr, calc_object.calculated_count));
    }

    match model.data.object.as_ref() {
        Expression::Frac(obj) => {
            calculate_frac_method(ctx, model.data, obj).await
        }
        Expression::Int(obj) => {
            calculate_int_method(ctx, model.data, obj).await
        }
        Expression::Matrix(obj) => {
            calculate_matrix_method(ctx, model.data, obj).await
        }
        Expression::Object(obj) => {
            calculate_object_method(ctx, model.data, obj).await
        }
        Expression::Transc(obj) => {
            calculate_transc_method(ctx, model.data, obj).await
        }
        _ => {
            raise_unknown_method(model.data)
        }
    }
}

pub fn raise_unknown_method(
    method_obj: &MethodData,
) -> CalcResultOrErr {
    return Err(CalcError::from_string(format!(
        "{} {} does not have {}() method.",
        method_obj.object.type_name(),
        method_obj.object,
        method_obj.func.name
    )));
}

pub fn raise_invalid_method_argument_type(
    method_obj: &MethodData,
    expected: Rc<Expression>,
    actual: Rc<Expression>,
) -> CalcResultOrErr {
    return Err(CalcError::from_string(format!(
        "Invalid argument passed to the {}() method. Expected {} {}, found {} {}.",
        method_obj.func.name,
        expected.type_name(),
        expected,
        actual.type_name(),
        actual
    )));
}

pub fn validate_method_no_argument_expected(
    method_obj: &MethodData,
) -> Result<(), CalcError> {
    if !method_obj.func.arguments.is_empty() {
        return Err(CalcError::from_string(format!(
            "Method {}() cannot have any arguments.",
            method_obj.func.name
        )));
    }
    Ok(())
}

pub async fn calculate_arguments(
    ctx: &mut CalcContext<'_>,
    method_obj: &MethodData,
    arg_indices: Vec<usize>,
) -> Result<Option<CalcResult<Rc<Expression>>>, CalcError> {
    let mut calculated_args: Vec<Rc<Expression>> = Vec::new();
    let mut total_calculated: usize = 0;
    for (i, arg) in method_obj.func.arguments.iter().enumerate() {
        if arg_indices.contains(&i) {
            let calc_object = calculate_expression_step(ctx, arg).await?;
            if calc_object.calculated_count > 0 {
                total_calculated += calc_object.calculated_count;
                calculated_args.push(calc_object.object);
            } else {
                calculated_args.push(Rc::clone(arg));
            }
        } else {
            calculated_args.push(Rc::clone(arg));
        }
    }

    if total_calculated > 0 {
        let expr = MethodData::new(
            Rc::clone(&method_obj.object),
            method_obj.func.name.clone(),
            calculated_args).to_rc_expr();
        return Ok(Some(CalcResult::new(expr, total_calculated)));
    } else {
        return Ok(None);
    }
}

pub async fn extract_int_argument(
    arguments: &Vec<Rc<Expression>>,
    index: usize,
) -> Result<&IntData, CalcError> {
    if let Some(arg) = arguments.get(index) {
        match arg.as_ref() {
            Expression::Int(val) => {
                return Ok(val);
            }
            _ => {}
        }
    }
    return Err(CalcError::from_string(format!(
        "Expected argument ({}) of integer type.",
        index+1)));
}

pub async fn extract_limited_i32_argument(
    arguments: &Vec<Rc<Expression>>,
    index: usize,
    min_limit: i32,
    max_limit: i32,
) -> Result<i32, CalcError> {
    let value: &IntData = extract_int_argument(arguments, index).await?;
    if let Some(i32_value) = value.to_i32() {
        if min_limit <= i32_value && i32_value <= max_limit {
            return Ok(i32_value);
        }
    }

    return Err(CalcError::from_string(format!(
        "Expected argument ({}) of integer in range {}..{}.",
        index+1,
        min_limit,
        max_limit)));
}
