#[cfg(test)]
mod tests {
    use rstest::rstest;

    use matm_model::expressions::method::MethodData;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;
    use matm_model::testing::asserts::assert_result_error_that;

    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::testing::asserts::test_calculation;
    use crate::testing::asserts::test_calculation_ok;

    #[rstest]
    #[case(5, 5)]
    #[case(-5, 5)]
    #[case(0, 0)]
    fn test_int_abs(
        #[case] input: i32,
        #[case] expected: i32
    ) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let expr = MethodData::abs(ctx.fa().rzi_u(input)).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_integer()
            .is_int(expected);
    }

    #[rstest]
    #[case(5, false)]
    #[case(-5, false)]
    #[case(0, true)]
    fn test_int_is_zero(
        #[case] input: i32,
        #[case] expected: bool
    ) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let expr = MethodData::new(
            ctx.fa().rzi_u(input),
            "is_zero".to_owned(),
            Vec::new()).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_bool()
            .is_equal_to(expected);
    }

    #[rstest]
    #[case(-5, false)]
    #[case(-1, false)]
    #[case(0, false)]
    #[case(1, true)]
    #[case(5, false)]
    fn test_int_is_one(
        #[case] input: i32,
        #[case] expected: bool
    ) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let expr = MethodData::new(
            ctx.fa().rzi_u(input),
            "is_one".to_owned(),
            Vec::new()).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_bool()
            .is_equal_to(expected);
    }

    #[rstest]
    #[case(5, true)]
    #[case(-5, false)]
    #[case(0, false)]
    fn test_int_is_positive(
        #[case] input: i32,
        #[case] expected: bool
    ) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let expr = MethodData::new(
            ctx.fa().rzi_u(input),
            "is_positive".to_owned(),
            Vec::new()).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_bool()
            .is_equal_to(expected);
    }

    #[rstest]
    #[case(5, false)]
    #[case(-5, true)]
    #[case(0, false)]
    fn test_int_is_negative(
        #[case] input: i32,
        #[case] expected: bool
    ) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let expr = MethodData::new(
            ctx.fa().rzi_u(input),
            "is_negative".to_owned(),
            Vec::new()).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .as_bool()
            .is_equal_to(expected);
    }

    #[test]
    fn test_unknown_method() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ctx.fa().rz1(),
            "abc".to_owned(),
            Vec::new())
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("integer 1 does not have abc() method.");
    }
}

