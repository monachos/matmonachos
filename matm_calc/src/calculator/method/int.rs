use crate::calculator::method::raise_unknown_method;
use crate::calculator::method::validate_method_no_argument_expected;
use crate::calculator::CalcResultOrErr;
use crate::calculator::CalcResult;
use crate::calculator::context::CalcContext;
use matm_model::expressions::bool::BoolData;
use matm_model::expressions::int::IntData;
use matm_model::expressions::method::MethodData;
use matm_model::expressions::ToExpression;

pub async fn calculate_int_method(
    ctx: &CalcContext<'_>,
    method_obj: &MethodData,
    obj: &IntData,
) -> CalcResultOrErr {
    if MethodData::ABS.eq(&method_obj.func.name) {
        calculate_int_abs(ctx, method_obj, obj).await
    } else if MethodData::IS_ZERO.eq(&method_obj.func.name) {
        calculate_int_is_predicate(ctx, method_obj, obj, |o| o.is_zero()).await
    } else if MethodData::IS_ONE.eq(&method_obj.func.name) {
        calculate_int_is_predicate(ctx, method_obj, obj, |o| o.is_one()).await
    } else if MethodData::IS_POSITIVE.eq(&method_obj.func.name) {
        calculate_int_is_predicate(ctx, method_obj, obj, |o| o.is_positive()).await
    } else if MethodData::IS_NEGATIVE.eq(&method_obj.func.name) {
        calculate_int_is_predicate(ctx, method_obj, obj, |o| o.is_negative()).await
    } else {
        raise_unknown_method(method_obj)
    }
}

async fn calculate_int_abs(
    _ctx: &CalcContext<'_>,
    method_obj: &MethodData,
    obj: &IntData,
) -> CalcResultOrErr {
    validate_method_no_argument_expected(method_obj)?;
    let res_expr = obj.calculate_abs().to_rc_expr();
    return Ok(CalcResult::one(res_expr));
}

async fn calculate_int_is_predicate(
    _ctx: &CalcContext<'_>,
    method_obj: &MethodData,
    obj: &IntData,
    predicate: fn(&IntData) -> bool,
) -> CalcResultOrErr {
    validate_method_no_argument_expected(method_obj)?;
    let res_expr = BoolData::new(predicate(obj)).to_rc_expr();
    return Ok(CalcResult::one(res_expr));
}