use std::rc::Rc;

use matm_model::expressions::fractions::FracData;
use matm_model::expressions::method::MethodData;
use matm_model::expressions::ToExpression;

use crate::calculator::method::raise_unknown_method;
use crate::calculator::method::validate_method_no_argument_expected;
use crate::calculator::context::CalcContext;
use crate::calculator::CalcResult;
use crate::calculator::CalcResultOrErr;

pub async fn calculate_frac_method(
    ctx: &CalcContext<'_>,
    method_obj: &MethodData,
    obj: &FracData,
) -> CalcResultOrErr {
   return if MethodData::ABS.eq(&method_obj.func.name) {
      calculate_frac_abs(ctx, method_obj, obj).await
   } else {
      raise_unknown_method(method_obj)
   }
}

async fn calculate_frac_abs(
    _ctx: &CalcContext<'_>,
    method_obj: &MethodData,
    obj: &FracData,
) -> CalcResultOrErr {
   validate_method_no_argument_expected(method_obj)?;
   let num = MethodData::abs(Rc::clone(&obj.numerator)).to_rc_expr();
   let den = MethodData::abs(Rc::clone(&obj.denominator)).to_rc_expr();
   let res_expr = FracData::new(num, den).to_rc_expr();
   return Ok(CalcResult::one(res_expr));
}

#[cfg(test)]
mod tests {
    use once_cell::sync::Lazy;
    use rstest::rstest;

    use matm_model::expressions::method::MethodData;
    use matm_model::expressions::Expression;
    use matm_model::expressions::ToExpression;
    use matm_model::factories::ObjFactory;
    use matm_model::testing::asserts::assert_expression_that;
    use matm_model::testing::asserts::assert_result_error_that;

    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::rules::CalcRules;
    use crate::testing::asserts::test_calculation;
    use crate::testing::asserts::test_calculation_ok;

    //TODO DRY
    const DEFAULT_RULES: Lazy<CalcRules> = Lazy::new(|| {CalcRules::default()});
    //TODO DRY
    const DEFAULT_FA: Lazy<ObjFactory> = Lazy::new(|| {ObjFactory::new(DEFAULT_RULES.radix)});

    #[rstest]
    #[case((1, 2), DEFAULT_FA.efi_u(1, 2))]
    #[case((-1, 2), DEFAULT_FA.efi_u(1, 2))]
    #[case((1, -2), DEFAULT_FA.efi_u(1, 2))]
    #[case((-1, -2), DEFAULT_FA.efi_u(1, 2))]
    #[case((0, 3), DEFAULT_FA.ez0())]
    fn test_frac_abs(
        #[case] input: (i32, i32),
        #[case] expected: Expression,
    ) {
        let rules = DEFAULT_RULES.clone();
        let mut ctx_data = CalcContextData::from_rules(&rules);
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::abs(DEFAULT_FA.rfi_u(input.0, input.1)).to_rc_expr();
        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .is_equal_to(&expected);
    }

    #[test]
    fn test_unknown_method() {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            ctx.fa().rfi_u(1, 2),
            "abc".to_owned(),
            Vec::new())
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message("fraction 1/2 does not have abc() method.");
    }
}

