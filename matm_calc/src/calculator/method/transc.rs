use matm_model::expressions::method::MethodData;
use matm_model::expressions::transcendental::TranscData;
use matm_model::expressions::ToExpression;

use crate::calculator::method::raise_unknown_method;
use crate::calculator::method::validate_method_no_argument_expected;
use crate::calculator::CalcResultOrErr;
use crate::calculator::CalcResult;
use crate::calculator::context::CalcContext;

pub async fn calculate_transc_method(
    ctx: &CalcContext<'_>,
    method_obj: &MethodData,
    obj: &TranscData,
) -> CalcResultOrErr {
    if MethodData::ABS.eq(&method_obj.func.name) {
        calculate_transc_abs(ctx, method_obj, obj).await
    } else {
        raise_unknown_method(method_obj)
    }
}

async fn calculate_transc_abs(
    _ctx: &CalcContext<'_>,
    method_obj: &MethodData,
    obj: &TranscData,
) -> CalcResultOrErr {
    validate_method_no_argument_expected(method_obj)?;
    match obj {
        TranscData::Pi |
        TranscData::Euler => {
            return Ok(CalcResult::one(obj.clone().to_rc_expr()));
        }
    }
}