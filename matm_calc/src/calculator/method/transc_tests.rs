#[cfg(test)]
mod tests {
    use crate::calculator::context::CalcContext;
    use crate::calculator::context::CalcContextData;
    use crate::testing::asserts::test_calculation;
    use crate::testing::asserts::test_calculation_ok;
    use matm_model::expressions::method::MethodData;
    use matm_model::expressions::transcendental::TranscData;
    use matm_model::expressions::Expression;
    use matm_model::expressions::ToExpression;
    use matm_model::testing::asserts::assert_expression_that;
    use matm_model::testing::asserts::assert_result_error_that;
    use rstest::rstest;

    #[rstest]
    #[case(TranscData::Pi, TranscData::Pi.to_expr())]
    #[case(TranscData::Euler, TranscData::Euler.to_expr())]
    fn test_transc_abs(
        #[case] input: TranscData,
        #[case] expected: Expression,
    ) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);
        let expr = MethodData::abs(input.to_rc_expr()).to_rc_expr();

        let result = test_calculation_ok(&mut ctx, &expr);

        // check result
        assert_expression_that(&result)
            .is_equal_to(&expected);
    }

    #[rstest]
    #[case(TranscData::Pi, "pi")]
    #[case(TranscData::Euler, "e")]
    fn test_unknown_method(
        #[case] input: TranscData,
        #[case] obj_name: &str,
    ) {
        let mut ctx_data = CalcContextData::default();
        let mut ctx = CalcContext::from_data(&mut ctx_data);

        let expr = MethodData::new(
            input.to_rc_expr(),
            "abc".to_owned(),
            Vec::new())
            .to_rc_expr();

        let result = test_calculation(&mut ctx, &expr);

        // check result
        assert_result_error_that(result)
            .has_message(format!(
                "transcendental number {} does not have abc() method.",
                obj_name).as_str()
            );
    }

}

