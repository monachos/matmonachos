use matm_model::core::natural::RADIX_10;

#[derive(Clone)]
pub struct CalcRules {
   // use_lowest_common_denominator: bool,

   /// Radix (base) used in calculations
   pub radix: u32,

   // Elementary operations like addition, subtraction
   pub explain_elementary_operations: bool,

   // Factorization of integers
   pub explain_int_factorization: bool,
}

impl CalcRules {
   pub fn default() -> Self {
      Self {
         // use_lowest_common_denominator: false,
         radix: RADIX_10,
         explain_elementary_operations: false,
         explain_int_factorization: false,
      }
   }
}
