use std::collections::VecDeque;


pub struct QueueMultiMapItem<K, V> {
    pub key: K,
    pub values: VecDeque<V>,
}

impl<K, V> QueueMultiMapItem<K, V> {
    pub fn new(key: K, value: V) -> Self {
        let mut value_container: VecDeque<V> = VecDeque::with_capacity(1);
        value_container.push_back(value);
        Self {
            key: key,
            values: value_container,
        }
    }
}

//---------------------------------------

/// Multimap that stores keys in queue (as VecDeque),
/// values are stores also in queue.
pub struct QueueMultiMap<K, V> {
    items: VecDeque<QueueMultiMapItem<K, V>>,
}

impl<K, V> QueueMultiMap<K, V>
    where K: PartialEq {
    pub fn new() -> Self {
        Self {
            items: VecDeque::new(),
        }
    }

    pub fn is_empty(&self) -> bool {
        return self.items.is_empty();
    }

    pub fn keys_len(&self) -> usize {
        return self.items.len();
    }

    pub fn push(&mut self, key: K, value: V) {
        for item in &mut self.items {
            if item.key.eq(&key) {
                item.values.push_back(value);
                return;
            }
        }
        self.items.push_back(QueueMultiMapItem::new(key, value));
    }

    pub fn pop(&mut self) -> Option<QueueMultiMapItem<K, V>> {
        return self.items.pop_front();
    }

    pub fn iter(&self) -> std::collections::vec_deque::Iter<'_, QueueMultiMapItem<K, V>> {
        return self.items.iter();
    }

}

