use matm_model::expressions::transcendental::TranscData;
use matm_model::expressions::Expression;

use super::recognizers::is_int;
use super::recognizers::is_int_pi;
use super::recognizers::is_pi;

pub enum PredefinedAngle {
    ZERO,
    PI12,
    PI6,
    PI4,
    PI3,
    FIVEPI12,
    PI2,
    PI
}

//TODO test
pub async fn get_predefined_angle_from_expression(expr: &Expression) -> Option<PredefinedAngle> {
    match expr {
        Expression::Int(int_value) => {
            if int_value.is_zero() {
                return Some(PredefinedAngle::ZERO);
            }
        }
        Expression::Frac(frac_value) => {
            if is_pi(frac_value.numerator.as_ref()) {
                if let Expression::Int(den_int) = frac_value.denominator.as_ref() {
                    if let Some(den_int_val) = den_int.to_i32() {
                        return match den_int_val {
                            12 => Some(PredefinedAngle::PI12),
                            6 => Some(PredefinedAngle::PI6),
                            4 => Some(PredefinedAngle::PI4),
                            3 => Some(PredefinedAngle::PI3),
                            2 => Some(PredefinedAngle::PI2),
                            _ => None,
                        };
                    }
                }
            } else if is_int_pi(&frac_value.numerator, 5)
                && is_int(&frac_value.denominator, 12) {
                return Some(PredefinedAngle::FIVEPI12);
            }
        }
        Expression::Transc(tr) => {
            if let TranscData::Pi = tr.as_ref() {
                return Some(PredefinedAngle::PI);
            }
        }
        _ => {}
    }
    None
}
