use async_recursion::async_recursion;
use matm_error::error::CalcError;
use matm_model::expressions::fractions::FracData;
use matm_model::expressions::int::IntData;
use matm_model::expressions::sign::Sign;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;
use std::cmp::Ordering;

use crate::calculator::int::multiplication::calculate_int_mul;
use crate::calculator::context::CalcContext;
use crate::result::ResultContainer;
use crate::rules::CalcRules;

/// Compares two expressions if are comparable.
#[async_recursion(?Send)]
pub async fn compare_expressions(
    ctx: &CalcContext<'_>,
    lhs: &Expression,
    rhs: &Expression,
) -> Result<Option<Ordering>, CalcError> {
    match (lhs, rhs) {
        (Expression::Int(lhs_val), Expression::Int(rhs_val)) => compare_int_int(lhs_val, rhs_val).await,
        (Expression::Int(lhs_val), Expression::Frac(rhs_val)) => compare_int_frac(ctx, lhs_val, rhs_val).await,
        (Expression::Frac(lhs_val), Expression::Int(rhs_val)) => compare_frac_int(ctx, lhs_val, rhs_val).await,
        (Expression::Frac(lhs_val), Expression::Frac(rhs_val)) => compare_frac_frac(ctx, lhs_val, rhs_val).await,
        //TODO compare Expression::Transc with 0 
        _ => Ok(None),
    }
}


async fn compare_int_int(
    lhs: &IntData,
    rhs: &IntData,
) -> Result<Option<Ordering>, CalcError> {
    return Ok(lhs.partial_cmp(rhs));
}

async fn compare_int_frac(
    ctx: &CalcContext<'_>,
    lhs: &IntData,
    rhs: &FracData,
) -> Result<Option<Ordering>, CalcError> {
    // only int fractions are compared
    match (rhs.numerator.as_ref(), rhs.denominator.as_ref()) {
        (Expression::Int(_), Expression::Int(rd)) => {
            match rd.sign() {
                Sign::Positive => {
                    let mut future_rules = CalcRules::default();
                    let mut explanation = ResultContainer::new();
                    let mut nested_ctx = CalcContext::new(
                        ctx.rules(),
                        &mut future_rules,
                        &mut explanation);
                    let l = calculate_int_mul(&mut nested_ctx, lhs, rd).await?.to_expr();
                    return compare_expressions(ctx, &l, &rhs.numerator).await;
                }
                Sign::Zero => {
                    // denominator is zero
                    return Ok(None);
                }
                Sign::Negative => {
                    // reverse operator
                    let mut future_rules = CalcRules::default();
                    let mut explanation = ResultContainer::new();
                    let mut nested_ctx = CalcContext::new(
                        ctx.rules(),
                        &mut future_rules,
                        &mut explanation);
                    let l = calculate_int_mul(&mut nested_ctx, lhs, rd).await?.to_expr();
                    if let Some(rel) = compare_expressions(ctx, &l, &rhs.numerator).await? {
                        return Ok(Some(rel.reverse()));
                    }
                }
            }
        }
        _ => {}
    }
    return Ok(None);
}

async fn compare_frac_int(
    ctx: &CalcContext<'_>,
    lhs: &FracData,
    rhs: &IntData,
) -> Result<Option<Ordering>, CalcError> {
    // only int fractions are compared
    match (lhs.numerator.as_ref(), lhs.denominator.as_ref()) {
        (Expression::Int(_), Expression::Int(ld)) => {
            match ld.sign() {
                Sign::Positive => {
                    let mut future_rules = CalcRules::default();
                    let mut explanation = ResultContainer::new();
                    let mut nested_ctx = CalcContext::new(
                        ctx.rules(),
                        &mut future_rules,
                        &mut explanation);
                    let r = calculate_int_mul(&mut nested_ctx, rhs, ld).await?.to_expr();
                    return compare_expressions(ctx, &lhs.numerator, &r).await;
                }
                Sign::Zero => {
                    // denominator is zero
                    return Ok(None);
                }
                Sign::Negative => {
                    // reverse operator
                    let mut future_rules = CalcRules::default();
                    let mut explanation = ResultContainer::new();
                    let mut nested_ctx = CalcContext::new(
                        ctx.rules(),
                        &mut future_rules,
                        &mut explanation);
                    let r = calculate_int_mul(&mut nested_ctx, rhs, ld).await?.to_expr();
                    if let Some(rel) = compare_expressions(ctx, &lhs.numerator, &r).await? {
                        return Ok(Some(rel.reverse()));
                    }
                }
            }
        }
        _ => {}
    }
    return Ok(None);
}

async fn compare_frac_frac(
    ctx: &CalcContext<'_>,
    lhs: &FracData,
    rhs: &FracData,
) -> Result<Option<Ordering>, CalcError> {
    // only int fractions are compared
    match (lhs.numerator.as_ref(), lhs.denominator.as_ref(),
        rhs.numerator.as_ref(), rhs.denominator.as_ref()) {
        (Expression::Int(ln), Expression::Int(ld),
        Expression::Int(rn), Expression::Int(rd)) => {
            if ld.is_zero() || rd.is_zero() {
                // denominator is zero
                return Ok(None);
            }
            let mut future_rules = CalcRules::default();
            let mut explanation = ResultContainer::new();
            let mut nested_ctx = CalcContext::new(
                ctx.rules(),
                &mut future_rules,
                &mut explanation);
            let l = calculate_int_mul(&mut nested_ctx, ln, rd).await?.to_expr();
            let r = calculate_int_mul(&mut nested_ctx, rn, ld).await?.to_expr();
            if rd.sign() != ld.sign() {
                if let Some(rel) = compare_expressions(ctx, &l, &r).await? {
                    return Ok(Some(rel.reverse()));
                }
                return Ok(None);
            } else {
                return compare_expressions(ctx, &l, &r).await;
            }
        }
        _ => {}
    }
    return Ok(None);
}

