#[cfg(test)]
mod tests {
    use futures::executor::block_on;
    use matm_model::expressions::arithmetic_condition::ArithmeticCondition;
    use matm_model::expressions::conditional::ConditionalData;
    use matm_model::expressions::conditional::ConditionalClause;
    use matm_model::expressions::logic_condition::LogicCondition;
    use matm_model::expressions::condition::ToCondition;
    use matm_model::testing::asserts::assert_expression_that;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::Expression;
    use matm_model::expressions::ToExpression;
    use matm_model::expressions::symbol::SymbolData;
    use matm_model::expressions::unary::UnaryData;
    use matm_model::factories::ObjFactory;
    use crate::substitution::SymbolTable;
    use crate::substitution::substitute_expression;

    #[test]
    fn test_substitute_symbol_index_with_int() {
        let fa = ObjFactory::new10();
        let expr = SymbolData::with_index_s("a", "i").to_rc_expr();
        let mut symbols = SymbolTable::new();
        symbols.add(SymbolData::new("i"), fa.rzi_u(5));

        let result = block_on(substitute_expression(expr, &symbols));

        assert_expression_that(&result)
            .as_symbol()
            .has_name("a")
            .has_indices_equal_to(vec![fa.ezi_u(5)]);
    }

    #[test]
    fn test_substitute_symbol_with_int() {
        let fa = ObjFactory::new10();
        let expr = SymbolData::new("a").to_rc_expr();
        let mut symbols = SymbolTable::new();
        symbols.add(
            SymbolData::new("a"),
            fa.rzi_u(3));

        let result = block_on(substitute_expression(expr, &symbols));

        assert_expression_that(&result)
            .as_integer()
            .is_int(3);
    }

    #[test]
    fn test_substitute_symbol_with_frac() {
        let fa = ObjFactory::new10();
        let expr = SymbolData::new("a").to_rc_expr();
        let mut symbols = SymbolTable::new();
        symbols.add(
            SymbolData::new("a"),
            fa.rfi_u(2, 3));

        let result = block_on(substitute_expression(expr, &symbols));

        assert_expression_that(&result)
            .as_fraction()
            .is_ints(2, 3);
    }

    #[test]
    fn test_substitute_symbol_with_unary() {
        let fa = ObjFactory::new10();
        let expr = SymbolData::new("a").to_rc_expr();
        let mut symbols = SymbolTable::new();
        symbols.add(
            SymbolData::new("a"),
            UnaryData::group(fa.rzi_u(4)).to_rc_expr()
        );

        let result = block_on(substitute_expression(expr, &symbols));

        match result.as_ref() {
            Expression::Unary(expr) => {
                assert_expression_that(&expr.value)
                    .as_integer()
                    .is_int(4);
            }
            _ => {
                assert_eq!(false, true);
            }
        }
    }

    #[test]
    fn test_substitute_symbol_with_binary() {
        let fa = ObjFactory::new10();
        let expr = SymbolData::new("a").to_rc_expr();
        let mut symbols = SymbolTable::new();
        symbols.add(
            SymbolData::new("a"),
            BinaryData::add(
                fa.rzi_u(3),
                fa.rzi_u(4)
            ).to_rc_expr()
        );

        let result = block_on(substitute_expression(expr, &symbols));

        match result.as_ref() {
            Expression::Binary(expr) => {
                assert_expression_that(&expr.left)
                    .as_integer()
                    .is_int(3);
                assert_expression_that(&expr.right)
                    .as_integer()
                    .is_int(4);
            }
            _ => {
                assert_eq!(false, true);
            }
        }
    }

    #[test]
    fn test_substitute_unary_with_int() {
        let fa = ObjFactory::new10();
        let expr = UnaryData::group(
            SymbolData::new("a").to_rc_expr(),
        ).to_rc_expr();
        let mut symbols = SymbolTable::new();
        symbols.add(
            SymbolData::new("a"),
            fa.rzi_u(3));

        let result = block_on(substitute_expression(expr, &symbols));
        match result.as_ref() {
            Expression::Unary(unary_expr) => {
                assert_expression_that(&unary_expr.value)
                    .as_integer()
                    .is_int(3);
            }
            _ => {
                assert_eq!(true, false);
            }
        }
    }

    #[test]
    fn test_substitute_binary_with_int() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::add(
            SymbolData::new("a").to_rc_expr(),
            fa.rzi_u(10)
        ).to_rc_expr();
        let mut symbols = SymbolTable::new();
        symbols.add(
            SymbolData::new("a"),
            fa.rzi_u(3));

        let result = block_on(substitute_expression(expr, &symbols));
        match result.as_ref() {
            Expression::Binary(binary_expr) => {
                assert_expression_that(&binary_expr.left)
                    .as_integer()
                    .is_int(3);
                assert_expression_that(&binary_expr.right)
                    .as_integer()
                    .is_int(10);
            }
            _ => {
                assert_eq!(true, false);
            }
        }
    }

    #[test]
    fn test_substitute_arithmetic_condition() {
        let fa = ObjFactory::new10();
        let expr = ConditionalData::from_vector(vec![
            ConditionalClause {
                expression: fa.rsn("e"),
                condition: ArithmeticCondition::gt(fa.rsn("c1"), fa.rsn("c2")).to_rc_cond(),
            }
        ]).to_rc_expr();
        let mut symbols = SymbolTable::new();
        symbols.add(fa.sn("e"), fa.rsn("s_e"));
        symbols.add(fa.sn("c1"), fa.rsn("s_c1"));
        symbols.add(fa.sn("c2"), fa.rsn("s_c2"));

        let result = block_on(substitute_expression(expr, &symbols));
        assert_expression_that(&result)
            .as_conditional()
            .has_i_expression_format(0, "s_e")
            .has_i_condition_format(0, "s_c1 > s_c2");
    }

    #[test]
    fn test_substitute_logic_condition() {
        let fa = ObjFactory::new10();
        let expr = ConditionalData::from_vector(vec![
            ConditionalClause {
                expression: fa.rsn("e"),
                condition: LogicCondition::and(
                    ArithmeticCondition::gt(fa.rsn("c1"), fa.rz0()).to_rc_cond(),
                    ArithmeticCondition::gt(fa.rsn("c2"), fa.rz0()).to_rc_cond()
                ).to_rc_cond(),
            }
        ]).to_rc_expr();
        let mut symbols = SymbolTable::new();
        symbols.add(fa.sn("e"), fa.rsn("s_e"));
        symbols.add(fa.sn("c1"), fa.rsn("s_c1"));
        symbols.add(fa.sn("c2"), fa.rsn("s_c2"));

        let result = block_on(substitute_expression(expr, &symbols));
        assert_expression_that(&result)
            .as_conditional()
            .has_i_expression_format(0, "s_e")
            .has_i_condition_format(0, "s_c1 > 0 and s_c2 > 0");
    }

}