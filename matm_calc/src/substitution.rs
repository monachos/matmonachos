use std::rc::Rc;
use async_recursion::async_recursion;
use futures::future::join_all;

use log::debug;

use matm_error::error::CalcError;
use matm_model::expressions::assignment::AssignmentData;
use matm_model::expressions::func::FunctionData;
use matm_model::expressions::method::MethodData;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;
use matm_model::expressions::fractions::FracData;
use matm_model::expressions::unary::UnaryData;
use matm_model::expressions::binary::BinaryData;
use matm_model::expressions::binomial::BinomialData;
use matm_model::expressions::symbol::SymbolData;
use matm_model::expressions::matrices::MatrixData;
use matm_model::expressions::root::RootData;
use matm_model::expressions::exponentiation::ExpData;
use matm_model::expressions::logarithm::LogData;
use matm_model::expressions::arithmetic_condition::ArithmeticCondition;
use matm_model::expressions::condition::Condition;
use matm_model::expressions::conditional::ConditionalData;
use matm_model::expressions::conditional::ConditionalClause;
use matm_model::expressions::logic_condition::LogicCondition;
use matm_model::expressions::condition::ToCondition;
use matm_model::expressions::cracovians::CracovianData;
use matm_model::expressions::equation::EquationData;
use matm_model::expressions::soe::SysOfEqData;
use matm_model::expressions::iterated_binary::IteratedBinaryData;
use matm_model::factories::ObjFactory;
use matm_model::statements::ToStatement;
use matm_model::statements::text::TextStatement;
use crate::result::ResultContainer;



pub struct SymbolSubstitution {
    //TODO substitute expression
    pub symbol: SymbolData,
    pub value: Rc<Expression>,
}

pub struct SymbolTable {
    substitutions: Vec<SymbolSubstitution>,
}

impl SymbolTable {
    pub fn new() -> Self {
        Self {
            substitutions: Vec::new(),
        }
    }

    pub fn add(&mut self, symbol: SymbolData, value: Rc<Expression>) {
        debug!("Added symbol {} = {}.", symbol, value);
        self.substitutions.push(SymbolSubstitution {
            symbol: symbol,
            value: value,
        });
    }

    pub fn add_matrix_elements(&mut self, radix: u32, matrix_name: &str, value: &MatrixData) {
        let fa = ObjFactory::new(radix);
        for idx in value.iter_right_down() {
            self.add(
                SymbolData::with_index_e_e(matrix_name, fa.rzu_u(idx.row), fa.rzu_u(idx.column)),
                Rc::clone(value.get(&idx).unwrap()));
        }
    }

    pub fn add_matrix_column(&mut self, radix: u32, matrix_name: &str, value: &MatrixData, column: usize)
                             -> Result<(), CalcError> {
            let fa = ObjFactory::new(radix);
        for i in value.iter_rows() {
            self.add(
                SymbolData::with_index_e(matrix_name, fa.rzu(i)?),
                Rc::clone(value.get_i_i(i, column)?));
        }
        Ok(())
    }

    pub fn add_matrix_row(&mut self, radix: u32, matrix_name: &str, value: &MatrixData, row: usize)
                          -> Result<(), CalcError> {
            let fa = ObjFactory::new(radix);
        for i in value.iter_columns() {
            self.add(
                SymbolData::with_index_e(matrix_name, fa.rzu(i)?),
                Rc::clone(value.get_i_i(row, i)?));
        }
        Ok(())
    }

    pub fn find_value_by_symbol(&self, symbol: &SymbolData) -> Option<Rc<Expression>> {
        //TODO substitutions should be ordered from more complex expressions to simple
        match self.substitutions
            .iter()
            .find(|s| s.symbol.eq(symbol)) {
                Some(value) => {
                    Some(Rc::clone(&value.value))
                }
                None => None
            }
    }

    pub fn remove(&mut self, symbol: SymbolData) {
        let mut index: Option<usize> = None;
        for (i, subst) in self.substitutions.iter().enumerate() {
            if subst.symbol.eq(&symbol) {
                index = Some(i);
                break;
            }
        };
        if let Some(i) = index {
            debug!("Removed index {} for symbol {}.", i, symbol);
            self.substitutions.remove(i);
        }
    }

    pub fn dump_variables(&self, explanation: &mut ResultContainer) -> Result<(), CalcError> {
        if !self.substitutions.is_empty() {
            let nested = explanation.nested()?;
            nested.add_statement(TextStatement::section("defined variables:")
                .to_statement());
            for subst in & self.substitutions {
                nested.add_statement(TextStatement::paragraph("{name} = {value}")
                    .and_expression("name", subst.symbol.clone().to_rc_expr())
                    .and_expression("value", Rc::clone(&subst.value))
                    .to_statement());
            }
        }
        Ok(())
    }
}


/// Substitutes symbols (SymbolExpression) with defined substitution
/// 
/// returns substituted expression
#[async_recursion(?Send)]
pub async fn substitute_expression(expr: Rc<Expression>, symbols: &SymbolTable) -> Rc<Expression> {
    return match expr.as_ref() {
        Expression::Undefined => expr,
        Expression::Assignment(o) => substitute_assignment(o, symbols).await,
        Expression::Bool(_) => expr,
        Expression::BoxSign => expr,
        Expression::Equation(o) => substitute_equation(o, symbols).await,
        Expression::SysOfEq(o) => substitute_system_of_equations(o, symbols).await,
        Expression::Int(_) => expr,
        Expression::Frac(o) => substitute_fraction(o, symbols).await,
        Expression::Cracovian(o) => substitute_cracovian(o, symbols).await,
        Expression::Matrix(o) => substitute_matrix(o, symbols).await,
        Expression::Symbol(o) => substitute_symbol(o, symbols).await,
        Expression::Unary(o) => substitute_unary(o, symbols).await,
        Expression::Binary(o) => substitute_binary(o, symbols).await,
        Expression::IteratedBinary(o) => substitute_iterated_binary(o, symbols).await,
        Expression::Function(o) => substitute_function(o, symbols).await,
        Expression::Method(o) => substitute_method(o, symbols).await,
        Expression::Object(_) => expr,
        Expression::Exp(o) => substitute_exp(o, symbols).await,
        Expression::Root(o) => substitute_root(o, symbols).await,
        Expression::Log(o) => substitute_log(o, symbols).await,
        Expression::Im(_) => expr,
        Expression::Transc(_) => expr,
        Expression::Variable(_) => expr,
        Expression::Conditional(o) => substitute_conditional(o, symbols).await,
        Expression::Binomial(o) => substitute_binomial(o, symbols).await,
    }
}

//todo test
async fn substitute_assignment(frac: &AssignmentData, symbols: &SymbolTable) -> Rc<Expression> {
    return FracData::new(
        substitute_expression(Rc::clone(&frac.source), symbols).await,
        substitute_expression(Rc::clone(&frac.target), symbols).await,
    ).to_rc_expr();
}

//todo test
async fn substitute_equation(equation: &EquationData, symbols: &SymbolTable) -> Rc<Expression> {
    let left = substitute_expression(Rc::clone(&equation.left), symbols).await;
    let right = substitute_expression(Rc::clone(&equation.right), symbols).await;
    return EquationData::new(left, right).to_rc_expr();
}

//todo test
async fn substitute_system_of_equations(eqs: &SysOfEqData, symbols: &SymbolTable) -> Rc<Expression> {
    let substituted_equations = join_all(eqs.equations.iter()
        .map(|el| async { substitute_expression(Rc::clone(el), symbols).await })
        .collect::<Vec<_>>()
    ).await;
    let subst_obj = SysOfEqData::from_vector(substituted_equations);
    return subst_obj.to_rc_expr();
}

//todo test
async fn substitute_fraction(frac: &FracData, symbols: &SymbolTable) -> Rc<Expression> {
    return FracData::new(
        substitute_expression(Rc::clone(&frac.numerator), symbols).await,
        substitute_expression(Rc::clone(&frac.denominator), symbols).await,
    ).to_rc_expr();
}

//TODO test
async fn substitute_cracovian(obj: &CracovianData, symbols: &SymbolTable) -> Rc<Expression> {
    let substituted_elements = join_all(obj.grid.data.iter()
        .map(|el| async { substitute_expression(Rc::clone(el), symbols).await })
        .collect::<Vec<_>>()
    ).await;
    let subst_obj = CracovianData::from_vector(
        obj.rows(),
        obj.columns(),
        substituted_elements).unwrap();
    return subst_obj.to_rc_expr();
}

//TODO test
async fn substitute_matrix(matrix: &MatrixData, symbols: &SymbolTable) -> Rc<Expression> {
    let substituted_elements = join_all(matrix.grid.data.iter()
        .map(|el| substitute_expression(Rc::clone(el), symbols))
        .collect::<Vec<_>>()
    ).await;
    let subst_matrix = MatrixData::from_vector(
        matrix.rows(),
        matrix.columns(),
        substituted_elements).unwrap();
    return subst_matrix.to_rc_expr();
}

async fn substitute_symbol(expr: &SymbolData, symbols: &SymbolTable) -> Rc<Expression> {
    let subst_expr = SymbolData {
        name: expr.name.clone(),
        indices: substitute_expression_vector(&expr.indices, symbols).await,
        postfix: expr.postfix.clone(),
    };

    return match symbols.find_value_by_symbol(&subst_expr) {
        Some(subst) => Rc::clone(&subst),
        None => subst_expr.to_rc_expr(),
    };
}

async fn substitute_unary(expr: &UnaryData, symbols: &SymbolTable) -> Rc<Expression> {
    // substitute also det(A)
    let subst_expr = substitute_expression(Rc::clone(&expr.value), symbols).await;
    return UnaryData::from_operator(expr.operator, subst_expr).to_rc_expr();
}

async fn substitute_binary(expr: &BinaryData, symbols: &SymbolTable) -> Rc<Expression> {
    let left_subst_expr = substitute_expression(Rc::clone(&expr.left), symbols).await;
    let right_subst_expr = substitute_expression(Rc::clone(&expr.right), symbols).await;
    return BinaryData::from_operator(expr.operator, left_subst_expr, right_subst_expr).to_rc_expr();
}

async fn substitute_iterated_binary(expr: &IteratedBinaryData, symbols: &SymbolTable) -> Rc<Expression> {
    let subst_lower_bound = substitute_expression(Rc::clone(&expr.lower_bound), symbols).await;
    let subst_upper_bound = substitute_expression(Rc::clone(&expr.upper_bound), symbols).await;
    let subst_value = substitute_expression(Rc::clone(&expr.value), symbols).await;
    return IteratedBinaryData::from_operator(
        expr.operator,
        expr.iterator.clone(),
        subst_lower_bound,
        subst_upper_bound,
        subst_value).to_rc_expr();
}

async fn substitute_expression_vector(
    expressions: &Vec<Rc<Expression>>,
    symbols: &SymbolTable,
) -> Vec<Rc<Expression>> {
    return join_all(expressions.iter()
        .map(|expr| substitute_expression(Rc::clone(expr), symbols))
        .collect::<Vec<_>>()
    ).await;
}

//TODO test
async fn substitute_function(expr: &FunctionData, symbols: &SymbolTable) -> Rc<Expression> {
    let new_args = substitute_expression_vector(&expr.arguments, symbols).await;
    return FunctionData::new(expr.name.clone(), new_args).to_rc_expr();
}

//TODO test
async fn substitute_method(expr: &MethodData, symbols: &SymbolTable) -> Rc<Expression> {
    let new_obj = substitute_expression(Rc::clone(&expr.object), symbols).await;
    let new_args = substitute_expression_vector(&expr.func.arguments, symbols).await;
    return MethodData::new(
        new_obj,
        expr.func.name.clone(),
        new_args).to_rc_expr();
}

async fn substitute_exp(expr: &ExpData, symbols: &SymbolTable) -> Rc<Expression> {
    return ExpData::new(
        substitute_expression(Rc::clone(&expr.base), symbols).await,
        substitute_expression(Rc::clone(&expr.exponent), symbols).await
    ).to_rc_expr();
}

async fn substitute_root(expr: &RootData, symbols: &SymbolTable) -> Rc<Expression> {
    return RootData::new(
        substitute_expression(Rc::clone(&expr.radicand), symbols).await,
        substitute_expression(Rc::clone(&expr.degree), symbols).await
    ).to_rc_expr();
}

async fn substitute_log(expr: &LogData, symbols: &SymbolTable) -> Rc<Expression> {
    return LogData::with_value_and_base(
        substitute_expression(Rc::clone(&expr.value), symbols).await,
        substitute_expression(Rc::clone(&expr.base), symbols).await
    ).to_rc_expr();
}

async fn substitute_conditional(expr: &ConditionalData, symbols: &SymbolTable) -> Rc<Expression> {
    let mut subst_clauses: Vec<ConditionalClause> = Vec::with_capacity(expr.clauses.len());
    for clause in &expr.clauses {
        let subst_clause = ConditionalClause {
            condition: substitute_condition(Rc::clone(&clause.condition), symbols).await,
            expression: substitute_expression(Rc::clone(&clause.expression), symbols).await,
        };
        subst_clauses.push(subst_clause);
    }
    return ConditionalData::from_vector(subst_clauses).to_rc_expr();
}

async fn substitute_binomial(expr: &BinomialData, symbols: &SymbolTable) -> Rc<Expression> {
    return BinomialData::new(
        substitute_expression(Rc::clone(&expr.set), symbols).await,
        substitute_expression(Rc::clone(&expr.subset), symbols).await
    ).to_rc_expr();
}

#[async_recursion(?Send)]
async fn substitute_condition(cond: Rc<Condition>, symbols: &SymbolTable) -> Rc<Condition> {
    return match cond.as_ref() {
        Condition::Bool(_) => cond,
        Condition::Arithmetic(arith) => substitute_arithmetic_condition(arith, symbols).await,
        Condition::Logic(logic) => substitute_logic_condition(logic, symbols).await,
    };
}

async fn substitute_arithmetic_condition(cond: &ArithmeticCondition, symbols: &SymbolTable) -> Rc<Condition> {
    return ArithmeticCondition::from_operator(
        cond.operator,
        substitute_expression(Rc::clone(&cond.left), symbols).await,
        substitute_expression(Rc::clone(&cond.right), symbols).await,
    ).to_rc_cond();
}

async fn substitute_logic_condition(cond: &LogicCondition, symbols: &SymbolTable) -> Rc<Condition> {
    return LogicCondition::from_operator(
        cond.operator,
        substitute_condition(Rc::clone(&cond.left), symbols).await,
        substitute_condition(Rc::clone(&cond.right), symbols).await,
    ).to_rc_cond();
}
