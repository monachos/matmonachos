use matm_error::error::CalcError;
use matm_model::expressions::matrices::MatrixData;
use matm_model::expressions::symbol::SymbolData;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;
use matm_model::expressions::equation::EquationData;
use matm_model::statements::expression::ExpressionStep;
use matm_model::statements::Statement;
use matm_model::statements::ToStatement;
use std::rc::Rc;
use std::slice::Iter;
use async_recursion::async_recursion;
use futures::executor::block_on;
use output_textcanv::output::colors::ColorSchema;
use output_textcanv::output::colors::ResultColorSchema;
use output_textcanv::output::text::TextCanvasContext;
use output_textcanv::output::text::TextCanvasRender;
use textstr::streams::OutputStream;
use textstr::streams::StdoutStream;

/// Calculation item containing result or sub-calculations
pub enum ResultItem {
    Statement(Statement),
    Nested(ResultContainer),
}

pub struct ResultContainer {
    pub indentation: usize,
    pub items: Vec<ResultItem>,
    pub colors: ResultColorSchema,
}

impl ResultContainer {
    pub fn new() -> Self {
        Self {
            indentation: 0,
            items: Vec::new(),
            colors: ResultColorSchema::Color(ColorSchema::new()),
        }
    }

    /// creates nested ResultContainer, associates it with current container
    /// and returns mutable reference to created container.
    pub fn nested(&mut self) -> Result<&mut Self, CalcError> {
        let nested = Self {
            indentation: self.indentation + 1,
            items: Vec::new(),
            colors: self.colors.clone(),
        };
        self.items.push(ResultItem::Nested(nested));
        let items_len = self.items.len();
        let res_item = self.items.get_mut(items_len - 1).unwrap();
        return match res_item {
            ResultItem::Nested(rc) => Ok(rc),
            _ => Err(CalcError::from_str("Invalid result item.")),
        }
    }

    pub fn write_with_indentation(&self, text: &str, stream: &mut dyn OutputStream) {
        let prefix = " ".repeat(self.indentation * 4);
        let mut add_n = false;
        for line in text.split('\n') {
            if add_n {
                self.write_new_line(stream);
            }
            stream.write(format!("{}{}", prefix, line).as_str());
            add_n = true;
        }
    }

    pub fn write_new_line(&self, stream: &mut dyn OutputStream) {
        stream.write("\n");
    }

    pub fn add_expr(&mut self, expr: Rc<Expression>) -> Result<(), CalcError> {
        self.add_statement(ExpressionStep::from_expression(expr).to_statement());
        Ok(())
    }

    pub fn add_expr_and_calc_count(
        &mut self,
        expr: Rc<Expression>,
        calculation_count: usize,
    ) -> Result<(), CalcError> {
        self.add_statement(
            ExpressionStep::from_expression_and_calculated_count(expr, calculation_count)
                .to_statement(),
        );
        Ok(())
    }

    pub fn add_statement(&mut self, stmt: Statement) {
        self.items.push(ResultItem::Statement(stmt));
    }

    pub fn add_statement_and_calc_count(
        &mut self,
        stmt: Statement,
        _calculation_count: usize, //TODO not used
    ) -> Result<(), CalcError> {
        self.add_statement(stmt);
        Ok(())
    }

    pub fn add_name_eq_matrix(&mut self, name: &str, matrix: MatrixData) -> Result<(), CalcError> {
        let symbol = SymbolData::new(name);
        self.add_symbol_eq_matrix(symbol, matrix)
    }

    pub fn add_symbol_eq_matrix(
        &mut self,
        symbol: SymbolData,
        matrix: MatrixData,
    ) -> Result<(), CalcError> {
        self.add_expr_eq_expr(symbol.to_rc_expr(), matrix.to_rc_expr())
    }

    pub fn add_symbol_eq_expr(
        &mut self,
        symbol: SymbolData,
        expr: Rc<Expression>,
    ) -> Result<(), CalcError> {
        self.add_expr_eq_expr(symbol.to_rc_expr(), expr)
    }

    pub fn add_expr_eq_expr(
        &mut self,
        left_expr: Rc<Expression>,
        right_expr: Rc<Expression>,
    ) -> Result<(), CalcError> {
        let eq = EquationData::new(left_expr, right_expr);
        return self.add_expr(eq.to_rc_expr());
    }

    pub fn calculated_count(&self) -> usize {
        let mut count = 0;
        for item in &self.items {
            match item {
                ResultItem::Statement(stmt) => {
                    if let Statement::ExprStep(expr_step) = stmt {
                        if let Some(c) = expr_step.calculated_count {
                            count += c;
                        }
                    }
                }
                ResultItem::Nested(rc) => {
                    count += rc.calculated_count();
                }
            }
        }
        return count;
    }

    pub fn steps(&self) -> &Vec<ResultItem> {
        return &self.items;
    }

    pub fn flat_expressions(&self) -> ResultContainerFlatExpressionIterator {
        return ResultContainerFlatExpressionIterator {
            iterator: self.items.iter(),
        };
    }
}

pub struct ResultContainerFlatExpressionIterator<'a> {
    iterator: Iter<'a, ResultItem>,
}

impl<'a> Iterator for ResultContainerFlatExpressionIterator<'a> {
    type Item = &'a ExpressionStep;

    fn next(&mut self) -> Option<Self::Item> {
        while let Some(item) = self.iterator.next() {
            if let ResultItem::Statement(stmt) = item {
                if let Statement::ExprStep(expr_step) = stmt {
                    return Some(&expr_step);
                }
            }
        }
        return None;
    }
}

pub fn dump_unicode_color(container: &ResultContainer) {
    let mut stdout_str = StdoutStream::new();
    let canv_ctx = TextCanvasContext::unicode_color();
    block_on(write_text(container, &canv_ctx, &mut stdout_str)).unwrap();
}

/// writes container content as text to specified stream
#[async_recursion(?Send)]
pub async fn write_text(
    container: &ResultContainer,
    ctx: &TextCanvasContext,
    stream: &mut dyn OutputStream,
) -> Result<(), CalcError> {
    let mut was_line = false;
    for item in &container.items {
        match item {
            ResultItem::Statement(stmt) => {
                let canvas = stmt.to_text_canvas(ctx)?;
                match canvas.to_colorized_text(&container.colors) {
                    Ok(text) => {
                        if was_line {
                            container.write_new_line(stream);
                            container.write_new_line(stream);
                        }
                        container.write_with_indentation(text.as_str(), stream);
                    }
                    Err(err) => {
                        return Err(CalcError::from_string(err.to_string())); //TODO change error type
                    }
                }
                was_line = true;
            }
            ResultItem::Nested(rc) => {
                if !rc.items.is_empty() {
                    if was_line {
                        container.write_new_line(stream);
                        container.write_new_line(stream);
                    }
                    write_text(&rc, ctx, stream).await?;
                    was_line = true;
                }
            }
        }
    }
    Ok(())
}
