#[macro_export]
macro_rules! scalar {
    ($fa: expr, $v:literal) => {
        match matm_model::macros::ExprLiteralWrapper::from($v) {
            matm_model::macros::ExprLiteralWrapper::Int(i) => $fa.rzi_u(i),
            matm_model::macros::ExprLiteralWrapper::Symbol(s) => $fa.rsn(&s),
        }
    };
    ($fa: expr, (- $v:literal)) => {
        $fa.rzi_u(-$v)
    };
    ($fa: expr, ($num:literal / $den:literal)) => {
        $fa.rfi_u($num, $den)
    };
}

#[macro_export]
macro_rules! matrix {
    // matrix filled with zeros
    ($fa: expr, $rows: literal, $cols: literal) => {
        matm_model::expressions::matrices::MatrixData::zeros(
            $fa.radix(),
            $rows,
            $cols)
    };
    ($fa: expr, $rows: literal, $cols: literal; $($el:tt),+) => {
        matm_model::expressions::matrices::MatrixData::from_vector(
            $rows,
            $cols,
            std::vec![$( crate::scalar!($fa, $el) ),*])
    };
}

