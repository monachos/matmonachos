#[cfg(test)]
mod tests {
    use matm_model::core::index::Index2D;
    use matm_model::expressions::ToExpression;
    use matm_model::expressions::exponentiation::ExpData;
    use matm_model::expressions::symbol::SymbolData;
    use matm_model::factories::ObjFactory;
    use crate::output::colors::TokenType;
    use crate::output::colors::ResultColorSchema;
    use crate::output::colors::ColorSchema;
    use crate::output::text::TextCanvas;
    use crate::output::text::TextCanvasContext;
    use crate::output::text::TextCanvasRender;
    use crate::testing::println_colorized_text;
    
    #[test]
    fn test_text_canvas_new() {
        let canvas = TextCanvas::new();

        match canvas.size() {
            Some(_) => {
                assert_eq!(true, false);
            }
            None => {
            }
        }
    }

    #[test]
    fn test_text_canvas_from_canvases_1_1_1() {
        let canv_ctx = TextCanvasContext::unicode_color();
        let canvases = vec![
            SymbolData::new("x").to_text_canvas(&canv_ctx).unwrap(),
            SymbolData::new("y").to_text_canvas(&canv_ctx).unwrap(),
            SymbolData::new("z").to_text_canvas(&canv_ctx).unwrap(),
        ];
        let canvas = TextCanvas::from_row_of_canvases(Index2D::one(), &canvases).unwrap();

        println_colorized_text(&canvas);
        assert_eq!(vec![
            "xyz",
            ].join("\n").to_string(), canvas.to_string());
    }

    #[test]
    fn test_text_canvas_from_canvases_1l_1() {
        let fa = ObjFactory::new10();
        let canv_ctx = TextCanvasContext::unicode_color();
        let canvases = vec![
            SymbolData::with_index_e("x", fa.rzi_u(1)).to_text_canvas(&canv_ctx).unwrap(),
            SymbolData::new("y").to_text_canvas(&canv_ctx).unwrap(),
        ];
        let canvas = TextCanvas::from_row_of_canvases(Index2D::one(), &canvases).unwrap();

        println_colorized_text(&canvas);
        assert_eq!(vec![
            "x y",
            " 1 ",
            ].join("\n").to_string(), canvas.to_string());
    }

    #[test]
    fn test_text_canvas_from_canvases_1u_1() {
        let fa = ObjFactory::new10();
        let canv_ctx = TextCanvasContext::unicode_color();
        let canvases = vec![
            ExpData::new(
                SymbolData::new("x").to_rc_expr(),
                fa.rzi_u(1)
            ).to_text_canvas(&canv_ctx).unwrap(),
            SymbolData::new("y").to_text_canvas(&canv_ctx).unwrap(),
        ];
        let canvas = TextCanvas::from_row_of_canvases(Index2D::one(), &canvases).unwrap();

        println_colorized_text(&canvas);
        assert_eq!(vec![
            " 1 ",
            "x y",
            ].join("\n").to_string(), canvas.to_string());
    }

    #[test]
    fn test_text_canvas_from_canvases_1l_1u() {
        let fa = ObjFactory::new10();
        let canv_ctx = TextCanvasContext::unicode_color();
        let canvases = vec![
            SymbolData::with_index_e("x", fa.rzi_u(1)).to_text_canvas(&canv_ctx).unwrap(),
            ExpData::new(
                SymbolData::new("y").to_rc_expr(),
                fa.rzi_u(2)
            ).to_text_canvas(&canv_ctx).unwrap(),
        ];
        let canvas = TextCanvas::from_row_of_canvases(Index2D::one(), &canvases).unwrap();

        println_colorized_text(&canvas);
        assert_eq!(vec![
            "   2",
            "x y ",
            " 1  ",
            ].join("\n").to_string(), canvas.to_string());
    }

    #[test]
    fn test_text_canvas_from_row_text_color_default() {
        let canvas = TextCanvas::from_row_text_color("abc", TokenType::Default);

        println_colorized_text(&canvas);
        assert_eq!("abc", canvas.to_string());
    }

    #[test]
    fn test_text_canvas_from_row_text_color_symbol() {
        let canvas = TextCanvas::from_row_text_color("abc", TokenType::Symbol);

        println_colorized_text(&canvas);
        assert_eq!("abc", canvas.to_string());
    }

    #[test]
    fn test_text_canvas_from_row_text() {
        let canvas = TextCanvas::from_row_text("abc");

        println_colorized_text(&canvas);
        assert_eq!("abc", canvas.to_string());
    }

    #[test]
    fn test_text_canvas_from_column_text_color_default() {
        let canvas = TextCanvas::from_column_text_color("abc", TokenType::Default);

        println_colorized_text(&canvas);
        assert_eq!(vec![
            "a",
            "b",
            "c",
            ].join("\n").to_string(), canvas.to_string());
    }

    #[test]
    fn test_text_canvas_from_column_text_color_symbol() {
        let canvas = TextCanvas::from_column_text_color("abc", TokenType::Symbol);

        println_colorized_text(&canvas);
        assert_eq!(vec![
            "a",
            "b",
            "c",
            ].join("\n").to_string(), canvas.to_string());
    }

    #[test]
    fn test_text_canvas_from_column_text_color_with_base_line() {
        let canvas = TextCanvas::from_column_text_color_with_base_line(2, "abc", TokenType::Default);

        println_colorized_text(&canvas);
        assert_eq!(2, canvas.base_line());
        assert_eq!(vec![
            "a",
            "b",
            "c",
            ].join("\n").to_string(), canvas.to_string());
    }

    #[test]
    fn test_text_canvas_from_column_text() {
        let canvas = TextCanvas::from_column_text("abc");

        println_colorized_text(&canvas);
        assert_eq!(vec![
            "a",
            "b",
            "c",
            ].join("\n").to_string(), canvas.to_string());
    }

    #[test]
    fn test_text_canvas_one_char() {
        let mut canvas = TextCanvas::new();
        canvas.write_row(Index2D::one(), "x").unwrap();

        match canvas.size() {
            Some(size) => {
                assert_eq!(1, size.width);
                assert_eq!(1, size.height);
            }
            None => {
                assert_eq!(true, false);
            }
        }
    }

    #[test]
    fn test_text_canvas_one_row() {
        let mut canvas = TextCanvas::new();
        canvas.write_row(Index2D::one(), "abc").unwrap();

        match canvas.size() {
            Some(size) => {
                assert_eq!(3, size.width);
                assert_eq!(1, size.height);
            }
            None => {
                assert_eq!(true, false);
            }
        }
    }

    #[test]
    fn test_text_canvas_one_row_update() {
        let mut canvas = TextCanvas::new();
        canvas.write_row(Index2D::one(), "abc").unwrap();
        canvas.write_row(Index2D::one(), "def").unwrap();

        match canvas.size() {
            Some(size) => {
                assert_eq!(3, size.width);
                assert_eq!(1, size.height);
            }
            None => {
                assert_eq!(true, false);
            }
        }
    }

    #[test]
    fn test_text_canvas_two_rows() {
        let mut canvas = TextCanvas::new();
        canvas.write_row(Index2D::new(1, 1).unwrap(), "abc").unwrap();
        canvas.write_row(Index2D::new(3, 2).unwrap(), "def").unwrap();

        match canvas.size() {
            Some(size) => {
                assert_eq!(4, size.width);
                assert_eq!(3, size.height);
            }
            None => {
                assert_eq!(true, false);
            }
        }
    }

    #[test]
    fn test_text_canvas_one_column() {
        let mut canvas = TextCanvas::new();
        canvas.write_column(Index2D::one(), "abc").unwrap();

        match canvas.size() {
            Some(size) => {
                assert_eq!(1, size.width);
                assert_eq!(3, size.height);
            }
            None => {
                assert_eq!(true, false);
            }
        }
    }

    #[test]
    fn test_text_canvas_two_columns() {
        let mut canvas = TextCanvas::new();
        canvas.write_column(Index2D::new(1, 1).unwrap(), "abc").unwrap();
        canvas.write_column(Index2D::new(2, 3).unwrap(), "def").unwrap();

        match canvas.size() {
            Some(size) => {
                assert_eq!(3, size.width);
                assert_eq!(4, size.height);
            }
            None => {
                assert_eq!(true, false);
            }
        }
    }

    #[test]
    fn test_text_canvas_write_row() {
        let mut canvas = TextCanvas::new();
        canvas.write_row(Index2D::new(1, 1).unwrap(), "abc").unwrap();
        canvas.write_row(Index2D::new(3, 2).unwrap(), "def").unwrap();

        assert_eq!(vec![
            "abc ",
            "    ",
            " def"
            ].join("\n").to_string(), canvas.to_string());
    }

    #[test]
    fn test_text_canvas_write_row_color() {
        let mut canvas = TextCanvas::new();
        canvas.write_row_color(
            Index2D::new(1, 1).unwrap(),
            "sin",
            TokenType::Function).unwrap();
        canvas.write_row_color(
            Index2D::new(1, 5).unwrap(),
            "0",
            TokenType::Number).unwrap();

        let schema = ResultColorSchema::Color(ColorSchema::new());
        let color_text = canvas.to_colorized_text(&schema).unwrap();

        println!("-------------");
        println!("{}", color_text);
        println!("-------------");
        assert_eq!("sin 0", canvas.to_string());
        //TODO does not work on some consoles
        // assert_eq!("\u{1b}[32ms\u{1b}[0m\u{1b}[32mi\u{1b}[0m\u{1b}[32mn\u{1b}[0m \u{1b}[94m0\u{1b}[0m", color_text);
    }

    #[test]
    fn test_text_canvas_row() {
        let mut canvas = TextCanvas::new();
        canvas.row(Index2D::new(1, 1).unwrap())
            .token("sin", TokenType::Function).unwrap()
            .default(" ").unwrap()
            .token("0", TokenType::Number).unwrap();

        let schema = ResultColorSchema::Color(ColorSchema::new());
        let color_text = canvas.to_colorized_text(&schema).unwrap();

        println!("-------------");
        println!("{}", color_text);
        println!("-------------");
        assert_eq!("sin 0", canvas.to_string());
        //TODO does not work on some consoles
        // assert_eq!("\u{1b}[32ms\u{1b}[0m\u{1b}[32mi\u{1b}[0m\u{1b}[32mn\u{1b}[0m\u{1b}[37m \u{1b}[0m\u{1b}[94m0\u{1b}[0m", color_text);
    }

    #[test]
    fn test_text_canvas_write_column_color() {
        let mut canvas = TextCanvas::new();
        canvas.write_column_color(
            Index2D::new(1, 1).unwrap(),
            "123",
            TokenType::Number).unwrap();

        let schema = ResultColorSchema::Color(ColorSchema::new());
        println!("-------------");
        println!("{}", canvas.to_colorized_text(&schema).unwrap());
        println!("-------------");
        assert_eq!(vec![
            "1",
            "2",
            "3"
            ].join("\n").to_string(), canvas.to_string());
    }

    #[test]
    fn test_get_rows_over_base_line_sym() {
        let canv_ctx = TextCanvasContext::unicode_color();
        let canvas = SymbolData::new("x").to_text_canvas(&canv_ctx).unwrap();
        println_colorized_text(&canvas);

        let result = canvas.get_rows_over_base_line().unwrap();

        assert_eq!(0, result);
    }

    #[test]
    fn test_get_rows_over_base_line_sym_index() {
        let fa = ObjFactory::new10();
        let canv_ctx = TextCanvasContext::unicode_color();
        let canvas = SymbolData::with_index_e("x", fa.rzi_u(1)).to_text_canvas(&canv_ctx).unwrap();
        println_colorized_text(&canvas);

        let result = canvas.get_rows_over_base_line().unwrap();

        assert_eq!(0, result);
    }

    #[test]
    fn test_get_rows_over_base_line_frac() {
        let fa = ObjFactory::new10();
        let canv_ctx = TextCanvasContext::unicode_color();
        let canvas = fa.fi_u(2, 3).to_text_canvas(&canv_ctx).unwrap();
        println_colorized_text(&canvas);

        let result = canvas.get_rows_over_base_line().unwrap();

        assert_eq!(1, result);
    }

    #[test]
    fn test_get_rows_under_base_line_sym() {
        let canv_ctx = TextCanvasContext::unicode_color();
        let canvas = SymbolData::new("x").to_text_canvas(&canv_ctx).unwrap();
        println_colorized_text(&canvas);

        let result = canvas.get_rows_under_base_line().unwrap();

        assert_eq!(0, result);
    }

    #[test]
    fn test_get_rows_under_base_line_sym_index() {
        let fa = ObjFactory::new10();
        let canv_ctx = TextCanvasContext::unicode_color();
        let canvas = SymbolData::with_index_e("x", fa.rzi_u(1)).to_text_canvas(&canv_ctx).unwrap();
        println_colorized_text(&canvas);

        let result = canvas.get_rows_under_base_line().unwrap();

        assert_eq!(1, result);
    }

    #[test]
    fn test_get_rows_under_base_line_frac() {
        let fa = ObjFactory::new10();
        let canv_ctx = TextCanvasContext::unicode_color();
        let canvas = fa.fi_u(2, 3).to_text_canvas(&canv_ctx).unwrap();
        println_colorized_text(&canvas);

        let result = canvas.get_rows_under_base_line().unwrap();

        assert_eq!(1, result);
    }

}
