use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use crate::output::console::ConsoleSign;
use crate::output::console::get_console_sign_string;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;


pub fn get_left_curly_bracket_text_canvas(
    ctx: &TextCanvasContext,
    height: usize
) -> Result<TextCanvas, CalcError> {
    let middle_pos = height / 2 + 1;
    let mut canvas = TextCanvas::with_base_line(middle_pos);
    for row_pos in 1..=height {
        let left_bracket_pos = Index2D::new(row_pos, 1)?;
        if height == 1 {
            canvas.write_row(left_bracket_pos, "{")?;
        } else if row_pos == middle_pos {
            canvas.write_row(left_bracket_pos,
                &&get_console_sign_string(ctx.charset, ConsoleSign::LeftCurlyBracketMiddle))?;
        } else if row_pos == 1 {
            canvas.write_row(left_bracket_pos,
                &get_console_sign_string(ctx.charset, ConsoleSign::LeftCurlyBracketUpper))?;
        } else if row_pos == height {
            canvas.write_row(left_bracket_pos,
                &get_console_sign_string(ctx.charset, ConsoleSign::LeftCurlyBracketLower))?;
        } else {
            canvas.write_row(left_bracket_pos,
                &get_console_sign_string(ctx.charset, ConsoleSign::CurlyBracketExtension))?;
        }
    }
    return Ok(canvas);
}

pub fn get_right_curly_bracket_text_canvas(
    ctx: &TextCanvasContext,
    height: usize,
) -> Result<TextCanvas, CalcError> {
    let middle_pos = height / 2 + 1;
    let mut canvas = TextCanvas::with_base_line(middle_pos);
    for row_pos in 1..=height {
        let left_bracket_pos = Index2D::new(row_pos, 1)?;
        if height == 1 {
            canvas.write_row(left_bracket_pos, "}")?;
        } else if row_pos == middle_pos {
            canvas.write_row(left_bracket_pos,
                &get_console_sign_string(ctx.charset, ConsoleSign::RightCurlyBracketMiddle))?;
        } else if row_pos == 1 {
            canvas.write_row(left_bracket_pos,
                &get_console_sign_string(ctx.charset, ConsoleSign::RightCurlyBracketUpper))?;
        } else if row_pos == height {
            canvas.write_row(left_bracket_pos,
                &get_console_sign_string(ctx.charset, ConsoleSign::RightCurlyBracketLower))?;
        } else {
            canvas.write_row(left_bracket_pos,
                &get_console_sign_string(ctx.charset, ConsoleSign::CurlyBracketExtension))?;
        }
    }
    return Ok(canvas);
}
