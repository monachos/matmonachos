use std::collections::HashMap;
use once_cell::sync::Lazy;


#[derive(Copy, Clone)]
pub enum ConsoleCharSet {
    Ansi,
    Unicode,
}

#[derive(Copy, Clone)]
pub enum ConsoleColor {
    Color,
    Mono,
}


#[derive(Eq, PartialEq, Hash, Copy, Clone)]
pub enum ConsoleSign {
    HorizLine,
    Norm,
    Multiplication,
    Division,
    MiddleDot,
    ForAll,
    Exists,
    NotEqualTo,
    LessThanOrEqualTo,
    GreaterThanOrEqualTo,
    LeftParenthesisUpper,
    LeftParenthesisMiddle,
    LeftParenthesisLower,
    RightParenthesisUpper,
    RightParenthesisMiddle,
    RightParenthesisLower,
    LeftSquareBracketUpper,
    LeftSquareBracketMiddle,
    LeftSquareBracketLower,
    RightSquareBracketUpper,
    RightSquareBracketMiddle,
    RightSquareBracketLower,
    LeftCurlyBracketUpper,
    LeftCurlyBracketMiddle,
    LeftCurlyBracketLower,
    RightCurlyBracketUpper,
    RightCurlyBracketMiddle,
    RightCurlyBracketLower,
    CurlyBracketExtension,
    RadicalBottom,
    ItalicSmallPi,
    ItalicSmallE,
    Square,
    SummationTop,
    SummationMiddle,
    SummationBottom,
    SummationTopExt,
    SummationBottomExt,
    ProductTopBar,
    ProductLeftTopBar,
    ProductRightTopBar,
    ProductLeftMiddleBar,
    ProductRightMiddleBar,
}


struct ConsoleCodeItem {
    sign: ConsoleSign,
    unicode: char,
    ansi_string: String,
}

impl ConsoleCodeItem {
    pub fn new(sign: ConsoleSign, unicode: char, ansi: &str) -> Self {
        Self {
            sign: sign,
            unicode: unicode,
            ansi_string: String::from(ansi),
        }
    }
}

static CONSOLE_CODE_ITEMS: Lazy<Vec<ConsoleCodeItem>> = Lazy::new(|| {
    vec![
        ConsoleCodeItem::new(ConsoleSign::HorizLine, '\u{2015}', "-"),
        ConsoleCodeItem::new(ConsoleSign::Norm, '\u{2016}', "||"),
        ConsoleCodeItem::new(ConsoleSign::Multiplication, '\u{00D7}', "x"),
        ConsoleCodeItem::new(ConsoleSign::Division, '\u{00F7}', "/"),
        ConsoleCodeItem::new(ConsoleSign::MiddleDot, '\u{B7}', "*"),
        ConsoleCodeItem::new(ConsoleSign::ForAll, '\u{2200}', "All"), 
        ConsoleCodeItem::new(ConsoleSign::Exists, '\u{2203}', "Each"), 
        ConsoleCodeItem::new(ConsoleSign::NotEqualTo, '\u{2260}', "<>"),
        ConsoleCodeItem::new(ConsoleSign::LessThanOrEqualTo, '\u{2264}', "<="),
        ConsoleCodeItem::new(ConsoleSign::GreaterThanOrEqualTo, '\u{2265}', ">="),
        ConsoleCodeItem::new(ConsoleSign::LeftParenthesisUpper, '\u{239B}', "/"), 
        ConsoleCodeItem::new(ConsoleSign::LeftParenthesisMiddle, '\u{239C}', "|"),
        ConsoleCodeItem::new(ConsoleSign::LeftParenthesisLower, '\u{239D}', r"\"),
        ConsoleCodeItem::new(ConsoleSign::RightParenthesisUpper, '\u{239E}', r"\"),
        ConsoleCodeItem::new(ConsoleSign::RightParenthesisMiddle, '\u{239F}', "|"),
        ConsoleCodeItem::new(ConsoleSign::RightParenthesisLower, '\u{23A0}', "/"),
        ConsoleCodeItem::new(ConsoleSign::LeftSquareBracketUpper, '\u{23A1}', "/"),
        ConsoleCodeItem::new(ConsoleSign::LeftSquareBracketMiddle, '\u{23A2}', "|"),
        ConsoleCodeItem::new(ConsoleSign::LeftSquareBracketLower, '\u{23A3}', r"\"),
        ConsoleCodeItem::new(ConsoleSign::RightSquareBracketUpper, '\u{23A4}', r"\"),
        ConsoleCodeItem::new(ConsoleSign::RightSquareBracketMiddle, '\u{23A5}', "|"),
        ConsoleCodeItem::new(ConsoleSign::RightSquareBracketLower, '\u{23A6}', "/"),
        ConsoleCodeItem::new(ConsoleSign::LeftCurlyBracketUpper, '\u{23A7}', "/"), 
        ConsoleCodeItem::new(ConsoleSign::LeftCurlyBracketMiddle, '\u{23A8}', "{"),
        ConsoleCodeItem::new(ConsoleSign::LeftCurlyBracketLower, '\u{23A9}', r"\"),
        ConsoleCodeItem::new(ConsoleSign::RightCurlyBracketUpper, '\u{23AB}', r"\"),
        ConsoleCodeItem::new(ConsoleSign::RightCurlyBracketMiddle, '\u{23AC}', "}"),
        ConsoleCodeItem::new(ConsoleSign::RightCurlyBracketLower, '\u{23AD}', "/"),
        ConsoleCodeItem::new(ConsoleSign::CurlyBracketExtension, '\u{23AA}', "|"), 
        ConsoleCodeItem::new(ConsoleSign::RadicalBottom, '\u{23B7}', "V"),
        ConsoleCodeItem::new(ConsoleSign::ItalicSmallPi, '\u{1D70B}', "pi"),
        ConsoleCodeItem::new(ConsoleSign::ItalicSmallE, '\u{1D452}', "e"),
        ConsoleCodeItem::new(ConsoleSign::Square, '\u{25A2}', "?"),
        ConsoleCodeItem::new(ConsoleSign::SummationTop, '\u{23B2}', r"\"),
        ConsoleCodeItem::new(ConsoleSign::SummationMiddle, '\u{276D}', ">"),
        ConsoleCodeItem::new(ConsoleSign::SummationBottom, '\u{23B3}', "/"),
        ConsoleCodeItem::new(ConsoleSign::SummationTopExt, '\u{23BA}', "-"),
        ConsoleCodeItem::new(ConsoleSign::SummationBottomExt, '\u{23AF}', "-"),
        ConsoleCodeItem::new(ConsoleSign::ProductTopBar, '\u{23BA}', "-"),
        ConsoleCodeItem::new(ConsoleSign::ProductLeftTopBar, '\u{23A5}', "-"),
        ConsoleCodeItem::new(ConsoleSign::ProductRightTopBar, '\u{23A2}', "-"),
        ConsoleCodeItem::new(ConsoleSign::ProductLeftMiddleBar, '\u{23A5}', "|"),
        ConsoleCodeItem::new(ConsoleSign::ProductRightMiddleBar, '\u{23A2}', "|"),
    ]
});

static SIGN_TO_UNICODE: Lazy<HashMap<ConsoleSign, char>> = Lazy::new(|| {
    let mut result: HashMap<ConsoleSign, char> = HashMap::new();
    for item in CONSOLE_CODE_ITEMS.iter() {
        result.insert(item.sign, item.unicode);
    }
    result
});

static SIGN_TO_ANSI: Lazy<HashMap<ConsoleSign, &str>> = Lazy::new(|| {
    let mut result: HashMap<ConsoleSign, &str> = HashMap::new();
    for item in CONSOLE_CODE_ITEMS.iter() {
        result.insert(item.sign, &item.ansi_string);
    }
    result
});

pub fn get_console_sign_string(charset: ConsoleCharSet, sign: ConsoleSign) -> String {
    return match charset {
        ConsoleCharSet::Ansi => {
            match SIGN_TO_ANSI.get(&sign) {
                Some(c) => String::from(*c),
                None => String::from("?"),
            }
        }
        ConsoleCharSet::Unicode => {
            match SIGN_TO_UNICODE.get(&sign) {
                Some(c) => String::from(*c),
                None => String::from("?"),
            }
        }
    };
}

