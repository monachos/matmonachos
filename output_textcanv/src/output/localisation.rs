use std::collections::HashMap;

pub struct LocaleProvider {
    texts: HashMap<String, String>,
    empty: String,
}

impl LocaleProvider {
    pub fn empty() -> Self {
        Self {
            texts: HashMap::new(),
            empty: String::from(""),
        }
    }

    pub fn default() -> Self {
        let mut map: HashMap<String, String> = HashMap::new();
        map.insert(String::from(locale_keys::AND), String::from("and"));
        map.insert(String::from(locale_keys::OR), String::from("or"));
        Self {
            texts: map,
            empty: String::from(""),
        }
    }

    pub fn get(&self, key: &str) -> &String {
        return match self.texts.get(key) {
            Some(val) => val,
            None => &self.empty,
        };
    }
}


pub mod locale_keys {
    pub static AND: &'static str = "and";
    pub static OR: &'static str = "or";
}
