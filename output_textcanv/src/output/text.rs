use std::collections::HashMap;
use std::fmt;
use std::cmp::max;
use colored::Colorize;
use matm_model::core::dimensions::Coordinates;
use matm_model::core::dimensions::Size;
use matm_error::error::CalcError;
use matm_model::core::index::Index;
use matm_model::core::index::Index2D;
use matm_model::core::index_range::IndexRange;
use crate::output::console::ConsoleCharSet;
use crate::output::console::ConsoleColor;
use crate::output::colors::TokenType;
use crate::output::colors::ResultColorSchema;

use super::localisation::LocaleProvider;


pub struct TextCanvasContext {
    pub charset: ConsoleCharSet,
    pub colors: ConsoleColor,
    pub locale: LocaleProvider,
}

impl TextCanvasContext {
    pub fn unicode_color() -> Self {
        Self {
            charset: ConsoleCharSet::Unicode,
            colors: ConsoleColor::Color,
            locale: LocaleProvider::default(),
        }
    }
}


#[derive(Clone)]
pub struct TextCanvasPixel {
    pub text: char,
    pub token: TokenType,
}

pub struct TextCanvas {
    data: HashMap<Index2D, TextCanvasPixel>,
    base_line: usize,
}

impl TextCanvas {
    pub fn new() -> Self {
        Self {
            data: HashMap::new(),
            base_line: 1,
        }
    }

    pub fn with_base_line(base_line: usize) -> Self {
        Self {
            data: HashMap::new(),
            base_line: base_line,
        }
    }

    //TODO test
    pub fn get_pixel(&self, idx: &Index2D) -> Option<&TextCanvasPixel> {
        self.data.get(idx)
    }

    pub fn hidden_space() -> Self {
        return Self::from_row_text(" ");//TODO change to not visible space
    }

    pub fn from_row_text_color(text: &str, token_type: TokenType) -> Self {
        let mut canvas = Self::with_base_line(1);
        canvas.write_row_color(Index2D::one(), text, token_type).unwrap();//TODO add error handling
        return canvas;
    }

    pub fn from_row_text(text: &str) -> Self {
        return Self::from_row_text_color(text, TokenType::Default);
    }

    pub fn from_column_text_color(text: &str, token_type: TokenType) -> Self {
        let mut canvas = Self::new();
        canvas.write_column_color(Index2D::one(), text, token_type).unwrap();
        return canvas;
    }

    pub fn from_column_text_color_with_base_line(base_line: usize, text: &str, token_type: TokenType) -> Self {
        let mut canvas = Self::with_base_line(base_line);
        canvas.write_column_color(Index2D::one(), text, token_type).unwrap();
        return canvas;
    }

    pub fn from_column_text(text: &str) -> Self {
        return Self::from_column_text_color(text, TokenType::Default);
    }

    pub fn from_row_of_canvases(start: Index2D, canvases: &Vec<TextCanvas>
    ) -> Result<Self, CalcError> {
        // calculate maximum height of all canvases
        let mut max_rows_over_base_line = 0;
        let mut max_rows_under_base_line = 0;
        for canv in canvases {
            max_rows_over_base_line = max(max_rows_over_base_line, canv.get_rows_over_base_line()?);
            max_rows_under_base_line = max(max_rows_under_base_line, canv.get_rows_under_base_line()?);
        }

        // calculate common base line
        let common_base_line = max_rows_over_base_line + 1;

        let mut canvas = Self::with_base_line(common_base_line);

        // write canvases in one row vertically aligned to base line
        let mut horiz_pos = start.column;
        for canv in canvases {
            match canv.size() {
                Some(size) => {
                    let vert_pos = start.row + common_base_line - canv.get_rows_over_base_line()?;
                    let item_start = Index2D::new(vert_pos, horiz_pos)?;
                    canvas.write_canvas(item_start, canv)?;
                    horiz_pos += size.width;
                }
                None => {}
            }
        }
        Ok(canvas)
    }

    pub fn from_row_of_canvases_with_base_line(
        base_line: usize,
        start: Index2D,
        canvases: &Vec<TextCanvas>,
    ) -> Result<Self, CalcError> {
        let mut self_obj = Self::from_row_of_canvases(start, canvases)?;
        self_obj.base_line = base_line;
        return Ok(self_obj);
    }

    pub fn base_line(&self) -> usize {
        return self.base_line;
    }

    pub fn is_empty(&self) -> bool {
        return self.data.is_empty();
    }

    /// Gets index ranges of the canvas
    pub fn coords(&self) -> Option<Coordinates<IndexRange>> {
        let mut min_row = 0;
        let mut max_row = 0;
        let mut min_column = 0;
        let mut max_column = 0;
        for key in self.data.keys() {
            if key.row < min_row || 0 == min_row {
                min_row = key.row;
            }
            if key.row > max_row || 0 == max_row {
                max_row = key.row;
            }
            if key.column < min_column || 0 == min_column {
                min_column = key.column;
            }
            if key.column > max_column || 0 == max_column {
                max_column = key.column;
            }
        }
        if min_column > 0 && max_column > 0 && min_row > 0 && max_row > 0 {
            Some(Coordinates::new(
                IndexRange::range(
                    Index::abs(min_column).unwrap(),
                    Index::abs(max_column).unwrap()).unwrap(),
                IndexRange::range(
                    Index::abs(min_row).unwrap(),
                    Index::abs(max_row).unwrap()).unwrap()
            ))
        } else {
            None
        }
    }

    /// Gets size of the canvas
    pub fn size(&self) -> Option<Size<usize>> {
        match self.coords() {
            Some(coords) => Some(
                Size::new(
                    coords.horizontal.count(1).unwrap(),
                    coords.vertical.count(1).unwrap())),
            None => None
        }
    }

    pub fn get_rows_over_base_line(&self) -> Result<usize, CalcError> {
        return Ok(self.base_line - 1);
    }

    pub fn get_rows_under_base_line(&self) -> Result<usize, CalcError> {
        if let Some(s) = self.size() {
            if s.height >= self.base_line {
                return Ok(s.height - self.base_line);
            }
        }
        Ok(0)
    }

    fn rows(&self) -> Vec<Vec<TextCanvasPixel>> {
        match self.coords() {
            Some(coords) => {
                let mut rows: Vec<Vec<TextCanvasPixel>> = Vec::with_capacity(coords.vertical.count(1).unwrap());
                let width = coords.horizontal.count(1).unwrap();
                for r in coords.vertical.iter(1).unwrap() {
                    let mut row: Vec<TextCanvasPixel> = Vec::with_capacity(width);
                    for c in coords.horizontal.iter(1).unwrap() {
                        let key = Index2D::new(r, c).unwrap();
                        match self.data.get(&key) {
                            Some(pixel) => {
                                row.push(pixel.clone());
                            }
                            None => {
                                row.push(TextCanvasPixel {
                                    text: ' ',
                                    token: TokenType::Default,
                                });
                            }
                        }
                    }
                    rows.push(row);
                }
                return rows;
            }
            None => {
                return Vec::new();
            }
        }
    }

    pub fn write_row_color(&mut self, start: Index2D, text: &str, token_type: TokenType) -> Result<(), CalcError> {
        let mut column = start.column;
        for c in text.chars() {
            self.data.insert(Index2D::new(start.row, column)?,
                TextCanvasPixel {
                    text: c,
                    token: token_type.clone(),
                });
            column += 1;
        }
        Ok(())
    }

    pub fn write_row(&mut self, start: Index2D, text: &str) -> Result<(), CalcError> {
        self.write_row_color(start, text, TokenType::Default)
    }

    pub fn row(&mut self, start: Index2D) -> TextCanvasRowWriter {
        return TextCanvasRowWriter::new(self, start);
    }

    pub fn write_column_color(&mut self, start: Index2D, text: &str, token_type: TokenType) -> Result<(), CalcError> {
        let mut row = start.row;
        for c in text.chars() {
            self.data.insert(Index2D::new(row, start.column)?,
                TextCanvasPixel {
                    text: c,
                    token: token_type.clone(),
                });
            row += 1;
        }
        Ok(())
    }

    pub fn write_column(&mut self, start: Index2D, text: &str) -> Result<(), CalcError> {
        self.write_column_color(start, text, TokenType::Default)
    }

    pub fn write_canvas(&mut self, start: Index2D, canvas: &TextCanvas)
    -> Result<(), CalcError> {
        if !canvas.is_empty() {
            let source_rows = canvas.rows();
            let mut row_index = start.row;
            for row in source_rows {
                let mut column_index = start.column;
                for cell in row {
                    let key = Index2D::new(row_index, column_index)?;
                    self.data.insert(key, cell);
                    column_index += 1;
                }
                row_index += 1;
            }
        }
        return Ok(());
    }

    //TODO DRY
    //TODO deprecated
    pub fn write_row_of_canvases_ref(&mut self, start: Index2D, canvases: &Vec<&TextCanvas>)
    -> Result<(), CalcError> {
        // calculate maximum height of all canvases
        let mut max_rows_over_base_line = 0;
        let mut max_rows_under_base_line = 0;
        for canv in canvases {
            max_rows_over_base_line = max(max_rows_over_base_line, canv.get_rows_over_base_line()?);
            max_rows_under_base_line = max(max_rows_under_base_line, canv.get_rows_under_base_line()?);
        }

        // calculate common base line
        let common_base_line = max_rows_over_base_line + 1;

        // write canvases in one row vertically aligned to base line
        let mut horiz_pos = start.column;
        for canv in canvases {
            match canv.size() {
                Some(size) => {
                    let vert_pos = start.row + common_base_line - canv.get_rows_over_base_line()?;
                    let item_start = Index2D::new(vert_pos, horiz_pos)?;
                    self.write_canvas(item_start, canv)?;
                    horiz_pos += size.width;
                }
                None => {}
            }
        }

        self.base_line = common_base_line;
        Ok(())
    }

    //TODO move to constructor + variant with base_line
    pub fn write_column_of_canvases(&mut self, start: Index2D, canvases: &Vec<TextCanvas>)
    -> Result<(), CalcError> {
        // calculate maximum width of all canvases
        let mut max_width = 0;
        for canv in canvases {
            match canv.size() {
                Some(size) => {
                    max_width = max(max_width, size.width);
                }
                None => {}
            }
        }

        // write canvases in one column
        let mut vert_pos = start.row;
        for canv in canvases {
            match canv.size() {
                Some(size) => {
                    // center item
                    let mut horiz_pos = start.column;
                    if size.width < max_width {
                        let horiz_offset = (max_width - size.width) / 2;
                        horiz_pos += horiz_offset;
                    }
                    let item_start = Index2D::new(vert_pos, horiz_pos)?;
                    self.write_canvas(item_start, canv)?;
                    vert_pos += size.height;
                }
                None => {}
            }
        }
        Ok(())
    }

    pub fn to_colorized_text(&self, colors: &ResultColorSchema) -> Result<String, fmt::Error> {
        let mut items: Vec<String> = Vec::new();
        match self.coords() {
            Some(coords) => {
                for r in coords.vertical.iter(1).unwrap() {//TODO is valid max_index?
                    if r > coords.vertical.calc_start(1).unwrap() {//TODO is valid max_index?
                        items.push(String::from("\n"));
                    }
                    for c in coords.horizontal.iter(1).unwrap() {
                        let key = Index2D::new(r, c).unwrap();
                        match self.data.get(&key) {
                            Some(pixel) => {
                                match colors {
                                    ResultColorSchema::NoColor => {
                                        items.push(String::from(pixel.text));
                                    }
                                    ResultColorSchema::Color(schema) => {
                                        let color = schema.color(&pixel.token);
                                        items.push(String::from(pixel.text).color(color).to_string());
                                    }
                                }
                            }
                            None => {
                                items.push(String::from(" "));
                            }
                        }
                    }
                }
            }
            None => {}
        }
        Ok(items.join(""))
    }

}

impl fmt::Display for TextCanvas {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.to_colorized_text(&ResultColorSchema::NoColor)?)
    }
}

//----------------------

pub struct TextCanvasRowWriter<'c> {
    canvas: &'c mut TextCanvas,
    position: Index2D,
}

impl <'c> TextCanvasRowWriter<'c> {
    pub fn new(canv: &'c mut TextCanvas, start: Index2D) -> Self {
        Self {
            canvas: canv,
            position: start,
        }
    }

    pub fn token(&mut self, text: &str, token_type: TokenType) -> Result<&mut Self, CalcError> {
        let start = Index2D::new(self.position.row, self.position.column)?;
        self.canvas.write_row_color(start, text, token_type)?;
        self.position.column += text.len();
        return Ok(self);
    }

    pub fn default(&mut self, text: &str) -> Result<&mut Self, CalcError> {
        return self.token(text, TokenType::Default);
    }

    pub fn number(&mut self, text: &str) -> Result<&mut Self, CalcError> {
        return self.token(text, TokenType::Number);
    }

    pub fn paragraph(&mut self, text: &str) -> Result<&mut Self, CalcError> {
        return self.token(text, TokenType::Paragraph);
    }

    pub fn canvas(&mut self, canvas: &TextCanvas) -> Result<&mut Self, CalcError> {
        let start = Index2D::new(self.position.row, self.position.column)?;
        self.canvas.write_canvas(start, canvas)?;
        if let Some(s) = canvas.size() {
            self.position.column += s.width;
        }
        return Ok(self);
    }
}

//----------------------

pub trait TextCanvasRender {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError>;
}
