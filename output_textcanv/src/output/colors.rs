use colored::Color;


#[derive(Clone, Copy)]
pub enum TokenType {
    Default,
    ElementaryBorrow,
    ElementaryCarry,
    Operator,
    Negation,
    Number,
    SourceNumber,
    TargetNumber,
    MarkedNumber,
    RadixSeparator,
    RadixValue,
    Function,
    Parenthesis,
    Section,
    Paragraph,
    Symbol,
    SymbolPostfix,
    ImaginaryUnit,
    Error,
}

#[derive(Clone)]
pub enum ResultColorSchema {
    NoColor,
    Color(ColorSchema),
}

#[derive(Clone)]
pub struct ColorSchema {
    pub elementary_borrow: Color,
    pub elementary_carry: Color,
    pub arithmetic_operator: Color,
    pub negation: Color,
    pub number: Color,
    pub source_number: Color,
    pub target_number: Color,
    pub marked_number: Color,
    pub radix_separator: Color,
    pub radix_value: Color,
    pub function: Color,
    pub parenthesis: Color,
    pub section: Color,
    pub paragraph: Color,
    pub symbol: Color,
    pub symbol_postfix: Color,
    pub imaginary_unit: Color,
    pub error: Color,
}

impl ColorSchema {
    pub fn new() -> Self {
        Self {
            elementary_borrow: Color::BrightGreen,
            elementary_carry: Color::BrightGreen,
            arithmetic_operator: Color::BrightMagenta,
            negation: Color::BrightWhite,
            number: Color::BrightBlue,
            source_number: Color::BrightYellow,
            target_number: Color::BrightMagenta,
            marked_number: Color::BrightYellow,
            radix_separator: Color::White,
            radix_value: Color::BrightCyan,
            function: Color::Green,
            parenthesis: Color::BrightYellow,
            section: Color::BrightGreen,
            paragraph: Color::BrightBlack,
            symbol: Color::White,
            symbol_postfix: Color::White,
            imaginary_unit: Color::BrightCyan,
            error: Color::BrightRed,
        }
    }

    pub fn color(&self, token: &TokenType) -> Color {
        return match token {
            TokenType::ElementaryBorrow => self.elementary_borrow,
            TokenType::ElementaryCarry => self.elementary_carry,
            TokenType::Operator => self.arithmetic_operator,
            TokenType::Negation => self.negation,
            TokenType::Number => self.number,
            TokenType::SourceNumber => self.source_number,
            TokenType::TargetNumber => self.target_number,
            TokenType::MarkedNumber => self.marked_number,
            TokenType::RadixSeparator => self.radix_separator,
            TokenType::RadixValue => self.radix_value,
            TokenType::Function => self.function,
            TokenType::Parenthesis => self.parenthesis,
            TokenType::Section => self.section,
            TokenType::Paragraph => self.paragraph,
            TokenType::Symbol => self.symbol,
            TokenType::SymbolPostfix => self.symbol_postfix,
            TokenType::ImaginaryUnit => self.imaginary_unit,
            TokenType::Error => self.error,
            _ => Color::White,
        };
    }
}
