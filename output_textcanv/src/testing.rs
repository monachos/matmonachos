use colored::Colorize;
use colored::Color;
use crate::output::console::ConsoleCharSet;
use crate::output::console::ConsoleColor;
use crate::output::colors::ColorSchema;
use crate::output::colors::ResultColorSchema;
use crate::output::localisation::LocaleProvider;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;


pub fn println_colorized_text(canvas: &TextCanvas) {
    let schema = ResultColorSchema::Color(ColorSchema::new());
    let text = canvas.to_colorized_text(&schema).unwrap();
    println!("{}", text);
}

pub fn println_text_variants(obj: &dyn TextCanvasRender) -> (String, String) {
    let unicode_text;
    let ansi_text;
    {
        let ctx = TextCanvasContext::unicode_color();
        let canvas = obj.to_text_canvas(&ctx).unwrap();
        let schema = ResultColorSchema::Color(ColorSchema::new());
        let text = canvas.to_colorized_text(&schema).unwrap();
        println!("{}", String::from("unicode color").on_color(Color::Yellow).color(Color::Black).bold());
        println!("{}", text);
    }
    {
        let ctx = TextCanvasContext {
            charset: ConsoleCharSet::Unicode,
            colors: ConsoleColor::Mono,
            locale: LocaleProvider::default(),
        };
        let canvas = obj.to_text_canvas(&ctx).unwrap();
        let schema = ResultColorSchema::NoColor;
        unicode_text = canvas.to_colorized_text(&schema).unwrap();
        println!("{}", String::from("unicode no-color").on_color(Color::Yellow).color(Color::Black).bold());
        println!("{}", unicode_text);
    }
    {
        let ctx = TextCanvasContext {
            charset: ConsoleCharSet::Ansi,
            colors: ConsoleColor::Mono,
            locale: LocaleProvider::default(),
        };
        let canvas = obj.to_text_canvas(&ctx).unwrap();
        let schema = ResultColorSchema::NoColor;
        ansi_text = canvas.to_colorized_text(&schema).unwrap();
        println!("{}", String::from("ansi no-color").on_color(Color::Yellow).color(Color::Black).bold());
        println!("{}", ansi_text);
    }

    return (ansi_text, unicode_text);
}