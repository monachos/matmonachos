use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use matm_model::expressions::transcendental::TranscData;
use crate::output::console::ConsoleSign;
use crate::output::console::get_console_sign_string;
use crate::output::colors::TokenType;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;


impl TextCanvasRender for TranscData {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        let mut canvas = TextCanvas::new();
        let symbol = match self {
            Self::Pi => get_console_sign_string(ctx.charset, ConsoleSign::ItalicSmallPi),
            Self::Euler => get_console_sign_string(ctx.charset, ConsoleSign::ItalicSmallE),
        };
        canvas.write_row_color(Index2D::one(), &symbol, TokenType::Default)?;
        return Ok(canvas);
    }
}
