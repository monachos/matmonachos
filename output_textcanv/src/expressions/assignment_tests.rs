#[cfg(test)]
mod tests {
    use matm_model::expressions::assignment::AssignmentData;
    use matm_model::expressions::ToExpression;
    use matm_model::expressions::exponentiation::ExpData;
    use matm_model::expressions::symbol::SymbolData;
    use matm_model::factories::ObjFactory;
    use crate::output::text::{TextCanvasRender, TextCanvasContext};
    use crate::testing::println_text_variants;


    #[test]
    fn test_to_text_canvas() {
        let fa = ObjFactory::new10();
        let asgn = AssignmentData::new(
            fa.rsn("x"),
            fa.rzi_u(3),
        );

        let (ansi, _) = println_text_variants(&asgn);

        assert_eq!("x: 3", ansi);
    }

    #[test]
    fn test_to_text_canvas_symbol_e_exp() {
        let fa = ObjFactory::new10();
        let asgn = AssignmentData::new(
            SymbolData::with_index_e("x", fa.rzi_u(1)).to_rc_expr(),
            ExpData::new(SymbolData::new("y").to_rc_expr(), fa.rzi_u(2)).to_rc_expr()
        );

        let canv_ctx = TextCanvasContext::unicode_color();
        let canvas = asgn.to_text_canvas(&canv_ctx).unwrap();
        assert_eq!(2, canvas.base_line());
        assert_eq!(1, canvas.get_rows_over_base_line().unwrap());
        assert_eq!(1, canvas.get_rows_under_base_line().unwrap());

        let (ansi, _) = println_text_variants(&asgn);
        assert_eq!(vec![
            r"     2",
            r"x : y ",
            r" 1    "
        ].join("\n").to_string(), ansi);
    }
}
