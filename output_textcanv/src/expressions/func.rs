use crate::output::colors::TokenType;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;
use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use matm_model::expressions::func::FunctionData;


use super::unary::wrap_text_canvas_with_parenthesis;

impl TextCanvasRender for FunctionData {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        // arguments
        let mut args_canvases: Vec<TextCanvas> = Vec::new();
        let mut add_separator: bool = false;
        for arg in &self.arguments {
            if add_separator {
                args_canvases.push(TextCanvas::from_row_text_color(", ", TokenType::Default));
            }
            args_canvases.push(arg.to_text_canvas(ctx)?);
            add_separator = true;
        }
        let args_canvas = TextCanvas::from_row_of_canvases(Index2D::one(), &args_canvases)?;

        let canvases = if self.can_have_no_parenthesis() {
            vec![
                TextCanvas::from_row_text_color(&self.name, TokenType::Function),
                TextCanvas::hidden_space(),
                self.arguments[0].to_text_canvas(ctx)?
            ]
        } else {
            vec![
                TextCanvas::from_row_text_color(&self.name, TokenType::Function),
                wrap_text_canvas_with_parenthesis(ctx, args_canvas)?,
            ]
        };

        let canvas = TextCanvas::from_row_of_canvases(Index2D::one(), &canvases)?;
        return Ok(canvas);
    }
}
