use matm_error::error::CalcError;
use matm_model::core::dimensions::Size;
use matm_model::core::index::Index2D;
use matm_model::expressions::soe::SysOfEqData;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;
use crate::output::text_canvas_utils::get_left_curly_bracket_text_canvas;


impl TextCanvasRender for SysOfEqData {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        let mut content_canv = TextCanvas::new();
        let mut row = 1;
        let vert_space = 1;
        for eq in &self.equations {
            let eq_canv = eq.to_text_canvas(ctx)?;
            let height = if let Some(s) = eq_canv.size() {
                s.height
            } else {
                0
            };
            content_canv.write_row_of_canvases_ref(Index2D::new(row, 1)?, &vec![&eq_canv])?;
            row += height + vert_space;
        }

        let content_size = content_canv.size().unwrap_or(Size::new(0, 0));
        let base_line = content_size.height / 2 + 1;

        let bracket_canv = get_left_curly_bracket_text_canvas(ctx, content_size.height)?;

        let mut canvas = TextCanvas::with_base_line(base_line);
        canvas.write_canvas(Index2D::new(1, 1)?, &bracket_canv)?;
        canvas.write_canvas(Index2D::new(1, 3)?, &content_canv)?;

        return Ok(canvas);
    }
}
