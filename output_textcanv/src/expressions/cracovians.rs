use matm_model::core::dimensions::Size;
use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use matm_model::expressions::cracovians::CracovianData;
use crate::expressions::matrices::matrix_content_to_text_canvas;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;
use crate::output::text_canvas_utils::get_left_curly_bracket_text_canvas;
use crate::output::text_canvas_utils::get_right_curly_bracket_text_canvas;


impl TextCanvasRender for CracovianData {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        let augmented_columns: Vec<usize> = Vec::new();
        let content_canv = matrix_content_to_text_canvas(
            ctx,
            &self.grid,
            &augmented_columns,
            CracovianData::format_index)?;

        let content_size = content_canv.size().unwrap_or(Size::new(0, 0));
        let base_line = content_size.height / 2 + 1;

        let content_left_margin = 2;
        let content_right_margin = 2;

        // put canvases into proper places
        let mut canvas = TextCanvas::with_base_line(base_line);
        canvas.write_canvas(Index2D::new(1, 1 + content_left_margin)?, &content_canv)?;

        // write brackets
        let left_bracket_canv = get_left_curly_bracket_text_canvas(ctx, content_size.height)?;
        canvas.write_canvas(Index2D::new(1, 1)?, &left_bracket_canv)?;

        let right_bracket_canv = get_right_curly_bracket_text_canvas(ctx, content_size.height)?;
        canvas.write_canvas(
            Index2D::new(1, content_left_margin + content_size.width + content_right_margin)?,
            &right_bracket_canv)?;

        return Ok(canvas);
    }
}
