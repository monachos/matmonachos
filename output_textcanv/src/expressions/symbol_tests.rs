#[cfg(test)]
mod tests {
    use matm_model::expressions::symbol::SymbolData;
    use matm_model::factories::ObjFactory;
    use crate::testing::println_text_variants;


    #[test]
    fn test_to_text_canvas_name() {
        let symbol = SymbolData::new("a");

        let (ansi, _) = println_text_variants(&symbol);

        assert_eq!("a", ansi);
    }

    #[test]
    fn test_to_text_canvas_with_index() {
        let fa = ObjFactory::new10();
        let symbol = SymbolData::with_indices(
            "a",
            vec![fa.rzi_u(1)]);
        
        let (ansi, _) = println_text_variants(&symbol);

        assert_eq!(vec![
            "a ",
            " 1"
        ].join("\n").to_string(), ansi);
    }

    #[test]
    fn test_to_text_canvas_with_indices() {
        let fa = ObjFactory::new10();
        let symbol = SymbolData::with_indices(
            "a",
            vec![fa.rzi_u(1), fa.rzi_u(2)]);

        let (ansi, _) = println_text_variants(&symbol);

        assert_eq!(vec![
            "a   ",
            " 1,2"
        ].join("\n").to_string(), ansi);
    }

    #[test]
    fn test_to_text_canvas_with_index_and_postfix() {
        let fa = ObjFactory::new10();
        let symbol = SymbolData::with_indices_and_postfix(
            "a",
            vec![fa.rzi_u(1)],
            "'");

        let (ansi, _) = println_text_variants(&symbol);

        assert_eq!(vec![
            "a'",
            " 1"
        ].join("\n").to_string(), ansi);
    }

    #[test]
    fn test_to_text_canvas_with_indices_and_postfix() {
        let fa = ObjFactory::new10();
        let symbol = SymbolData::with_indices_and_postfix(
            "a",
            vec![fa.rzi_u(1), fa.rzi_u(2)],
            "'");

        let (ansi, _) = println_text_variants(&symbol);

        assert_eq!(vec![
            "a'  ",
            " 1,2"
        ].join("\n").to_string(), ansi);
    }

}
