use matm_error::error::CalcError;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;

pub fn undefined_to_text_canvas(_ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
   let canvas = TextCanvas::from_row_text("?");
   return Ok(canvas);
}
