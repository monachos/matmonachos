use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use matm_model::expressions::imaginary::ImData;
use crate::output::colors::TokenType;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;


impl TextCanvasRender for ImData {
    fn to_text_canvas(&self, _ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        let mut canvas = TextCanvas::new();
        canvas.row(Index2D::one())
            .token("%", TokenType::ImaginaryUnit)?
            .token(&self.name, TokenType::ImaginaryUnit)?;
        return Ok(canvas);
    }
}

