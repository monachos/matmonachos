#[cfg(test)]
mod tests {
    use matm_model::expressions::ToExpression;
    use matm_model::expressions::root::RootData;
    use matm_model::factories::ObjFactory;
    use crate::testing::println_text_variants;


    #[test]
    fn test_to_text_canvas_root() {
        let fa = ObjFactory::new10();
        let expr = RootData::new(
            fa.rzi_u(3),
            fa.rzi_u(2),
        ).to_expr();
        
        let (ansi, uni) = println_text_variants(&expr);

        assert_eq!(vec![
            r" _",
            r"V3"
        ].join("\n").to_string(), ansi);
        assert_eq!(vec![
            r" _",
            r"⎷3"
        ].join("\n").to_string(), uni);
    }

    #[test]
    fn test_to_text_canvas_3_root() {
        let fa = ObjFactory::new10();
        let expr = RootData::new(
            fa.rzi_u(5),
            fa.rzi_u(3),
        ).to_expr();
        
        let (ansi, uni) = println_text_variants(&expr);

        assert_eq!(vec![
            r"3_",
            r"V5"
        ].join("\n").to_string(), ansi);
        assert_eq!(vec![
            r"3_",
            r"⎷5"
        ].join("\n").to_string(), uni);
    }

    #[test]
    fn test_to_text_canvas_root_negative() {
        let fa = ObjFactory::new10();
        let expr = RootData::new(
            fa.rzi_u(-3),
            fa.rzi_u(-2),
        ).to_expr();
        
        let (ansi, uni) = println_text_variants(&expr);

        assert_eq!(vec![
            r"-2__",
            r" V-3"
        ].join("\n").to_string(), ansi);
        assert_eq!(vec![
            r"-2__",
            r" ⎷-3"
        ].join("\n").to_string(), uni);
    }

    #[test]
    fn test_to_text_canvas_root_wide() {
        let fa = ObjFactory::new10();
        let expr = RootData::new(
            fa.rzi_u(3000),
            fa.rzi_u(2000),
        ).to_expr();
        
        let (ansi, uni) = println_text_variants(&expr);

        assert_eq!(vec![
            r"2000____",
            r"   V3000"
        ].join("\n").to_string(), ansi);
        assert_eq!(vec![
            r"2000____",
            r"   ⎷3000"
        ].join("\n").to_string(), uni);
    }

    #[test]
    fn test_to_text_canvas_root_rad1() {
        let fa = ObjFactory::new10();
        let expr = RootData::new(
            fa.rzi_u(3),
            fa.rfi_u(1, 31),
        ).to_expr();

        let (ansi, uni) = println_text_variants(&expr);

        assert_eq!(vec![
            r"1  ",
            r"-- ",
            r"31_",
            r" V3",
        ].join("\n").to_string(), ansi);
        assert_eq!(vec![
            r"1  ",
            r"―― ",
            r"31_",
            r" ⎷3",
        ].join("\n").to_string(), uni);
    }

    #[test]
    fn test_to_text_canvas_root_rad3() {
        let fa = ObjFactory::new10();
        let expr = RootData::new(
            fa.rfi_u(2, 3),
            fa.rfi_u(1, 31),
        ).to_expr();
        
        let (ansi, uni) = println_text_variants(&expr);

        assert_eq!(vec![
            r"1    ",
            r"--   ",
            r"31  _",
            r"   /2",
            r"  / -",
            r" V  3",
        ].join("\n").to_string(), ansi);
        assert_eq!(vec![
            r"1    ",
            r"――   ",
            r"31  _",
            r"   /2",
            r"  / ―",
            r" V  3",
        ].join("\n").to_string(), uni);
    }

}
