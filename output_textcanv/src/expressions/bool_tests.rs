#[cfg(test)]
mod tests {
    use ::rstest::rstest;
    use matm_model::expressions::bool::BoolData;
    use crate::testing::println_text_variants;

    #[rstest]
    #[case(false, "false")]
    #[case(true, "true")]
    fn test_bool_to_text_canvas(#[case] value: bool, #[case] expected: &str) {
        let o = BoolData::new(value);
        
        let (ansi, uni) = println_text_variants(&o);

        assert_eq!(expected, ansi);
        assert_eq!(expected, uni);
    }

}

