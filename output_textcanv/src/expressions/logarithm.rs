use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use matm_model::expressions::logarithm::LogData;
use crate::output::colors::TokenType;
use crate::expressions::unary::unary_group_to_text_canvas;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;



impl TextCanvasRender for LogData {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        let log_text = "log";
        let mut log_canvas = TextCanvas::new();
        log_canvas.write_row_color(Index2D::one(), log_text, TokenType::Function)?;

        let base_canvas = self.base.to_text_canvas(ctx)?;

        let value_canvas = if self.should_wrap_value_with_parentheses() {
            unary_group_to_text_canvas(ctx, &self.value)?
        } else {
            self.value.to_text_canvas(ctx)?
        };
        let base_row = value_canvas.get_rows_under_base_line()? + 2;

        let mut log_and_base_canvas = TextCanvas::with_base_line(1);
        let mut col = 1;
        log_and_base_canvas.write_canvas(Index2D::new(1, col)?, &log_canvas)?;
        col += log_canvas.size().unwrap().width;
        log_and_base_canvas.write_canvas(Index2D::new(base_row, col)?, &base_canvas)?;

        let canvases: Vec<TextCanvas> = vec![
            log_and_base_canvas,
            value_canvas,
        ];

        let canvas = TextCanvas::from_row_of_canvases(Index2D::one(), &canvases)?;
        return Ok(canvas);
    }
}
