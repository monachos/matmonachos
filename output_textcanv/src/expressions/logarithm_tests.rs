#[cfg(test)]
mod tests {
    use matm_model::expressions::ToExpression;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::logarithm::LogData;
    use matm_model::factories::ObjFactory;
    use crate::testing::println_text_variants;


    #[test]
    fn test_to_text_canvas_log() {
        let fa = ObjFactory::new10();
        let expr = LogData::with_value_and_base(
            fa.rzi_u(3),
            fa.rzi_u(2),
        ).to_expr();
        
        let (ansi, _) = println_text_variants(&expr);

        assert_eq!(vec![
            "log 3",
            "   2 "
        ].join("\n").to_string(), ansi);
    }

    #[test]
    fn test_to_text_canvas_log_negative() {
        let fa = ObjFactory::new10();
        let expr = LogData::with_value_and_base(
            fa.rzi_u(-3),
            fa.rzi_u(-2),
        ).to_expr();
        
        let (ansi, _) = println_text_variants(&expr);

        assert_eq!(vec![
            "log  (-3)",
            "   -2    "
        ].join("\n").to_string(), ansi);
    }

    #[test]
    fn test_to_text_canvas_log_big() {
        let fa = ObjFactory::new10();
        let expr = LogData::with_value_and_base(
            fa.rfi_u(10, 31),
            fa.rfi_u(1, 5),
        ).to_expr();
        
        let (ansi, _) = println_text_variants(&expr);

        assert_eq!(vec![
            r"    10",
            r"log --",
            r"    31",
            r"   1  ",
            r"   -  ",
            r"   5  ",
        ].join("\n").to_string(), ansi);
    }

    #[test]
    fn test_to_text_canvas_log_1p2() {
        let fa = ObjFactory::new10();
        let expr = LogData::with_value_and_base(
            BinaryData::add(fa.rzi_u(1), fa.rzi_u(2)).to_rc_expr(),
            fa.rzi_u(-2)
        ).to_expr();
        
        let (ansi, _) = println_text_variants(&expr);

        assert_eq!(vec![
            "log  (1 + 2)",
            "   -2       "
        ].join("\n").to_string(), ansi);
    }
}
