#[cfg(test)]
mod tests {
    use matm_model::expressions::ToExpression;
    use matm_model::expressions::symbol::SymbolData;
    use matm_model::factories::ObjFactory;
    use matm_model::expressions::equation::EquationData;
    use crate::testing::println_text_variants;

    #[test]
    fn test_to_text_canvas_simple() {
        let fa = ObjFactory::new10();
        let equation = EquationData::new(
            SymbolData::new("y").to_rc_expr(),
            fa.rzi_u(3)
        );

        let (ansi, _) = println_text_variants(&equation);

        assert_eq!("y = 3", ansi);
    }

}