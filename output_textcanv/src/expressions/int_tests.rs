#[cfg(test)]
mod tests {
    use matm_model::core::natural::RADIX_10;
    use matm_model::core::natural::RADIX_16;
    use matm_model::testing::asserts::assert_result_ok;
    use matm_model::expressions::int::IntData;
    use crate::output::colors::ColorSchema;
    use crate::output::colors::ResultColorSchema;
    use crate::output::text::TextCanvasContext;
    use crate::output::text::TextCanvasRender;


    #[test]
    fn test_to_text_canvas_123() {
        let value = IntData::from_int(RADIX_10, 123).unwrap();
        let canv_ctx = TextCanvasContext::unicode_color();
        let canvas = assert_result_ok(value.to_text_canvas(&canv_ctx));
        let schema = ResultColorSchema::Color(ColorSchema::new());

        let text = canvas.to_colorized_text(&schema).unwrap();

        println!("{}", text);
    }

    #[test]
    fn test_to_text_canvas_m1000000() {
        let value = IntData::from_int(RADIX_10, -1_000_000).unwrap();
        let canv_ctx = TextCanvasContext::unicode_color();
        let canvas = assert_result_ok(value.to_text_canvas(&canv_ctx));
        let schema = ResultColorSchema::Color(ColorSchema::new());

        let text = canvas.to_colorized_text(&schema).unwrap();

        println!("{}", text);
    }

    #[test]
    fn test_to_text_canvas_123f_16() {
        let value = IntData::from_int(RADIX_16, 0x123F).unwrap();
        let canv_ctx = TextCanvasContext::unicode_color();
        let canvas = assert_result_ok(value.to_text_canvas(&canv_ctx));
        let schema = ResultColorSchema::Color(ColorSchema::new());

        let text = canvas.to_colorized_text(&schema).unwrap();

        println!("{}", text);
    }

}
