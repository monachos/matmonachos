use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use matm_model::expressions::method::MethodData;
use crate::expressions::unary::unary_group_to_text_canvas;
use crate::output::colors::TokenType;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;

impl TextCanvasRender for MethodData {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        // object of called method
        let object_canvas = if self.should_wrap_object_with_parentheses() {
            unary_group_to_text_canvas(ctx, &self.object)?
        } else {
            self.object.to_text_canvas(ctx)?
        };

        let canvases = vec![
            object_canvas,
            TextCanvas::from_row_text_color(".", TokenType::Default),
            self.func.to_text_canvas(ctx)?
        ];
        let canvas = TextCanvas::from_row_of_canvases(Index2D::one(), &canvases)?;
        return Ok(canvas);
    }
}
