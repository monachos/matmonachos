use matm_error::error::CalcError;
use matm_model::expressions::Expression;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;

use self::boxsign::boxsign_to_text_canvas;
use self::undefined::undefined_to_text_canvas;


pub mod assignment;
mod assignment_tests;
pub mod binary;
mod binary_tests;
pub mod binomial;
mod binomial_tests;
pub mod bool;
mod bool_tests;
pub mod boxsign;
pub mod conditional;
mod conditional_tests;
pub mod cracovians;
mod cracovians_tests;
pub mod equation;
mod equation_tests;
pub mod exponentiation;
mod exponentiation_tests;
pub mod fractions;
mod fractions_tests;
pub mod func;
mod func_tests;
pub mod imaginary;
mod imaginary_tests;
pub mod int;
mod int_tests;
pub mod iterated_binary;
mod iterated_binary_tests;
pub mod logarithm;
mod logarithm_tests;
pub mod matrices;
mod matrix2d_tests;
pub mod method;
mod method_tests;
pub mod object;
mod object_tests;
pub mod root;
mod root_tests;
pub mod soe;
mod soe_tests;
pub mod symbol;
mod symbol_tests;
pub mod transcendental;
mod transcendental_tests;
pub mod unary;
pub mod undefined;
mod unary_tests;
pub mod variable;
mod variable_tests;

impl TextCanvasRender for Expression {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        return match self {
            Expression::Undefined => undefined_to_text_canvas(ctx),
            Expression::Assignment(obj) => obj.to_text_canvas(ctx),
            Expression::Bool(obj) => obj.to_text_canvas(ctx),
            Expression::BoxSign => boxsign_to_text_canvas(ctx),
            Expression::Equation(obj) => obj.to_text_canvas(ctx),
            Expression::SysOfEq(obj) => obj.to_text_canvas(ctx),
            Expression::Int(obj) => obj.to_text_canvas(ctx),
            Expression::Frac(frac) => frac.to_text_canvas(ctx),
            Expression::Cracovian(obj) => obj.to_text_canvas(ctx),
            Expression::Matrix(matrix) => matrix.to_text_canvas(ctx),
            Expression::Symbol(expr) => expr.to_text_canvas(ctx),
            Expression::Unary(expr) => expr.to_text_canvas(ctx),
            Expression::Binary(expr) => expr.to_text_canvas(ctx),
            Expression::IteratedBinary(expr) => expr.to_text_canvas(ctx),
            Expression::Function(expr) => expr.to_text_canvas(ctx),
            Expression::Method(expr) => expr.to_text_canvas(ctx),
            Expression::Object(expr) => expr.to_text_canvas(ctx),
            Expression::Exp(expr) => expr.to_text_canvas(ctx),
            Expression::Root(expr) => expr.to_text_canvas(ctx),
            Expression::Log(expr) => expr.to_text_canvas(ctx),
            Expression::Im(expr) => expr.to_text_canvas(ctx),
            Expression::Transc(expr) => expr.to_text_canvas(ctx),
            Expression::Variable(expr) => expr.to_text_canvas(ctx),
            Expression::Conditional(expr) => expr.to_text_canvas(ctx),
            Expression::Binomial(expr) => expr.to_text_canvas(ctx),
        };
    }
}

