use matm_error::error::CalcError;
use matm_model::expressions::variable::VariableData;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;


impl TextCanvasRender for VariableData {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        return self.name.to_text_canvas(ctx);
    }
}

