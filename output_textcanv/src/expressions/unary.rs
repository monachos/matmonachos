use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use matm_model::expressions::Expression;
use matm_model::expressions::precedence::OperatorPrecAccess;
use matm_model::expressions::unary::UnaryData;
use matm_model::expressions::unary::UnaryOperator;

use crate::output::colors::TokenType;
use crate::output::console::ConsoleSign;
use crate::output::console::get_console_sign_string;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;

impl TextCanvasRender for UnaryData {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        return match self.operator {
            UnaryOperator::Group => unary_group_to_text_canvas(ctx, &self.value),
            UnaryOperator::Neg => unary_neg_to_text_canvas(ctx, &self.value),
            UnaryOperator::Transpose => unary_transpose_to_text_canvas(ctx, &self.value),
            UnaryOperator::Determinant => unary_determinant_to_text_canvas(ctx, &self.value),
            UnaryOperator::Rank => unary_rank_to_text_canvas(ctx, &self.value),
            UnaryOperator::Inversion => unary_inversion_to_text_canvas(ctx, &self.value),
            UnaryOperator::Factorial => unary_factorial_to_text_canvas(ctx, &self),
        };
    }
}

pub fn wrap_text_canvas_with_parenthesis(ctx: &TextCanvasContext, inner_canvas: TextCanvas) -> Result<TextCanvas, CalcError> {
    match inner_canvas.size() {
        Some(size) => {
            let mut left_chars: Vec<String> = Vec::with_capacity(size.height);
            let mut right_chars: Vec<String> = Vec::with_capacity(size.height);
            if size.height == 1 {
                left_chars.push(String::from("("));
                right_chars.push(String::from(")"));
            } else if size.height > 1 {
                for i in 1..=size.height {
                    if i == 1 {
                        left_chars.push(get_console_sign_string(ctx.charset, ConsoleSign::LeftParenthesisUpper));
                        right_chars.push(get_console_sign_string(ctx.charset, ConsoleSign::RightParenthesisUpper));
                    } else if i == size.height {
                        left_chars.push(get_console_sign_string(ctx.charset, ConsoleSign::LeftParenthesisLower));
                        right_chars.push(get_console_sign_string(ctx.charset, ConsoleSign::RightParenthesisLower));
                    } else {
                        left_chars.push(get_console_sign_string(ctx.charset, ConsoleSign::LeftParenthesisMiddle));
                        right_chars.push(get_console_sign_string(ctx.charset, ConsoleSign::RightParenthesisMiddle));
                    }
                }
            }

            let left_text = left_chars.join("");
            let left_canvas = TextCanvas::from_column_text_color_with_base_line(inner_canvas.base_line(), left_text.as_str(), TokenType::Parenthesis);

            let right_text = right_chars.join("");
            let right_canvas = TextCanvas::from_column_text_color_with_base_line(inner_canvas.base_line(), right_text.as_str(), TokenType::Parenthesis);

            let canvases = vec![left_canvas, inner_canvas, right_canvas];
            let base_line = size.height / 2 + 1;
            let canvas = TextCanvas::from_row_of_canvases_with_base_line(base_line, Index2D::one(), &canvases)?;
            return Ok(canvas);
        }
        None => {
            let canvas = TextCanvas::from_row_text_color("()", TokenType::Parenthesis);
            return Ok(canvas);
        }
    }
}

pub fn unary_group_to_text_canvas(ctx: &TextCanvasContext, expr: &Expression) -> Result<TextCanvas, CalcError> {
    let inner_canvas = expr.to_text_canvas(ctx)?;
    return wrap_text_canvas_with_parenthesis(ctx, inner_canvas);
}

fn unary_neg_to_text_canvas(ctx: &TextCanvasContext, expr: &Expression) -> Result<TextCanvas, CalcError> {
    let canvases = vec![
        TextCanvas::from_row_text_color("-", TokenType::Operator),
        expr.to_text_canvas(ctx)?,
    ];
    let canvas = TextCanvas::from_row_of_canvases(Index2D::one(), &canvases)?;
    return Ok(canvas);
}

fn unary_transpose_to_text_canvas(ctx: &TextCanvasContext, expr: &Expression) -> Result<TextCanvas, CalcError> {
    let inner_canvas = expr.to_text_canvas(ctx)?;
    let mut canvas = TextCanvas::with_base_line(inner_canvas.base_line());
    match inner_canvas.size() {
        Some(inner_size) => {
            canvas.write_canvas(Index2D::one(), &inner_canvas)?;
            canvas.write_row_color(
                Index2D::new(1, inner_size.width + 2)?,
                "T",
                TokenType::Function)?;
        }
        None => {}
    }
    return Ok(canvas);
}

fn unary_determinant_to_text_canvas(ctx: &TextCanvasContext, expr: &Expression) -> Result<TextCanvas, CalcError> {
    let canvases = match expr {
        Expression::Matrix(_) => {
            vec![
                TextCanvas::from_row_text_color("det", TokenType::Function),
                expr.to_text_canvas(ctx)?,
            ]
        }
        _ => {
            vec![
                TextCanvas::from_row_text_color("det", TokenType::Function),
                unary_group_to_text_canvas(ctx, expr)?,
            ]
        }
    };
    let canvas = TextCanvas::from_row_of_canvases(Index2D::one(), &canvases)?;
    return Ok(canvas);
}

fn unary_rank_to_text_canvas(ctx: &TextCanvasContext, expr: &Expression) -> Result<TextCanvas, CalcError> {
    let canvases = match expr {
        Expression::Matrix(_) => {
            vec![
                TextCanvas::from_row_text_color("rank", TokenType::Function),
                expr.to_text_canvas(ctx)?,
            ]
        }
        _ => {
            vec![
                TextCanvas::from_row_text_color("rank", TokenType::Function),
                unary_group_to_text_canvas(ctx, expr)?,
            ]
        }
    };
    let canvas = TextCanvas::from_row_of_canvases(Index2D::one(), &canvases)?;
    return Ok(canvas);
}

fn unary_inversion_to_text_canvas(ctx: &TextCanvasContext, expr: &Expression) -> Result<TextCanvas, CalcError> {
    let canvases = match expr {
        Expression::Matrix(_) => {
            vec![
                TextCanvas::from_row_text_color("inv", TokenType::Function),
                expr.to_text_canvas(ctx)?,
            ]
        }
        _ => {
            vec![
                TextCanvas::from_row_text_color("inv", TokenType::Function),
                unary_group_to_text_canvas(ctx, expr)?,
            ]
        }
    };
    let canvas = TextCanvas::from_row_of_canvases(Index2D::one(), &canvases)?;
    return Ok(canvas);
}

fn unary_factorial_to_text_canvas(ctx: &TextCanvasContext, expr: &UnaryData) -> Result<TextCanvas, CalcError> {
    let canvas = if expr.value.operator_prec() < expr.operator_prec() {
        unary_group_to_text_canvas(ctx, &expr.value)?
    } else {
        expr.value.to_text_canvas(ctx)?
    };

    let canvases = vec![
        canvas,
        TextCanvas::from_row_text_color("!", TokenType::Operator)
    ];
    let canvas = TextCanvas::from_row_of_canvases(Index2D::one(), &canvases)?;
    return Ok(canvas);
}

//TODO Can I render Method::abs() as ||?
// pub fn unary_abs_to_text_canvas(ctx: &TextCanvasContext, expr: &Expression) -> Result<TextCanvas, CalcError> {
//     let inner_canvas = expr.to_text_canvas(ctx)?;
//     match inner_canvas.size() {
//         Some(size) => {
//             let bar_text = "|".repeat(size.height);
//             let left_canvas = TextCanvas::from_column_text_color_with_base_line(inner_canvas.base_line(), bar_text.as_str(), TokenType::Operator);
//             let right_canvas = TextCanvas::from_column_text_color_with_base_line(inner_canvas.base_line(), bar_text.as_str(), TokenType::Operator);

//             let canvases = vec![left_canvas, inner_canvas, right_canvas];
//             let base_line = size.height / 2 + 1;
//             let canvas = TextCanvas::from_row_of_canvases_with_base_line(
//                 base_line,
//                 Index2D::one(),
//                 &canvases)?;
//             return Ok(canvas);
//         }
//         None => {}
//     }

//     return Ok(TextCanvas::new());
// }
