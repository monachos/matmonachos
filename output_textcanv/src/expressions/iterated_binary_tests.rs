#[cfg(test)]
mod tests {
    use matm_model::expressions::ToExpression;
    use matm_model::expressions::iterated_binary::IteratedBinaryData;
    use matm_model::factories::ObjFactory;
    use crate::testing::println_text_variants;


    #[test]
    fn test_to_text_canvas_sum() {
        let fa = ObjFactory::new10();
        let expr = IteratedBinaryData::sum(
            fa.sn("k"),
            fa.rzi_u(1),
            fa.rzi_u(5),
            fa.rsn("x")
        ).to_expr();
        
        let (ansi, uni) = println_text_variants(&expr);

        assert_eq!(vec![
            r"5   ",
            r"\-  ",
            r" > x",
            r"/-  ",
            r"k=1 ",
            ].join("\n").to_string(), ansi);
        assert_eq!(vec![
            r"5   ",
            r"⎲⎺  ",
            r" ❭ x",
            r"⎳⎯  ",
            r"k=1 ",
            ].join("\n").to_string(), uni);
    }

    //-----------------------------------

    #[test]
    fn test_to_text_canvas_product() {
        let fa = ObjFactory::new10();
        let expr = IteratedBinaryData::product(
            fa.sn("k"),
            fa.rzi_u(1),
            fa.rzi_u(5),
            fa.rsn("x")
        ).to_expr();

        let (ansi, uni) = println_text_variants(&expr);

        assert_eq!(vec![
            r"5    ",
            r"---- ",
            r"|  |x",
            r"|  | ",
            r"k=1  ",
            ].join("\n").to_string(), ansi);
        assert_eq!(vec![
            r"5    ",
            r"⎥⎺⎺⎢ ",
            r"⎥  ⎢x",
            r"⎥  ⎢ ",
            r"k=1  ",
            ].join("\n").to_string(), uni);
    }

}
