#[cfg(test)]
mod tests {
    use matm_model::core::natural::RADIX_10;
    use matm_model::expressions::cracovians::CracovianData;
    use matm_model::factories::ObjFactory;
    use crate::testing::println_text_variants;

    #[test]
    fn test_to_text_canvas_1_1() {
        let m1 = CracovianData::from_ivector(
            1, 1,
            RADIX_10,
            vec![11]
        ).unwrap();

        let (ansi, _) = println_text_variants(&m1);

        assert_eq!(vec![
            r"{ 11 }"
            ].join("\n").to_string(),
            ansi);
    }

    #[test]
    fn test_to_text_canvas_1_2() {
        let m1 = CracovianData::from_ivector(
            2, 1,
            RADIX_10,
            vec![
                11, 21]
            ).unwrap();

        let (ansi, _) = println_text_variants(&m1);
        
        assert_eq!(vec![
            r"{ 11  21 }"
            ].join("\n").to_string(),
            ansi);
    }

    #[test]
    fn test_to_text_canvas_int_frac() {
        let fa = ObjFactory::new10();
        let m1 = CracovianData::from_vector(
            2, 1,
            vec![
                fa.rzi_u(11),
                fa.rfi_u(2, 3)
                ]
            ).unwrap();

        let (ansi, uni) = println_text_variants(&m1);

        assert_eq!(vec![
            r"/     2 \",
            r"{ 11  - }",
            r"\     3 /"
            ].join("\n").to_string(),
            ansi);
        assert_eq!(vec![
            r"⎧     2 ⎫",
            r"⎨ 11  ― ⎬",
            r"⎩     3 ⎭"
            ].join("\n").to_string(),
            uni);
    }

    #[test]
    fn test_to_text_canvas_2_2() {
        let m1 = CracovianData::from_ivector(
            2, 2,
            RADIX_10,
            vec![
                11, 21,
                12, 22]
            ).unwrap();

        let (ansi, uni) = println_text_variants(&m1);
        
        assert_eq!(vec![
            r"/ 11  21 \",
            r"{        }",
            r"\ 12  22 /"
            ].join("\n").to_string(),
            ansi);
        assert_eq!(vec![
            r"⎧ 11  21 ⎫",
            r"⎨        ⎬",
            r"⎩ 12  22 ⎭"
            ].join("\n").to_string(),
            uni);
    }

    #[test]
    fn test_to_text_canvas_3_2() {
        let m1 = CracovianData::from_ivector(
            2, 3,
            RADIX_10,
            vec![
                11, 21,
                12, 22,
                13, 23]
            ).unwrap();

        let (ansi, uni) = println_text_variants(&m1);
        
        assert_eq!(vec![
            r"/ 11  21 \",
            r"|        |",
            r"{ 12  22 }",
            r"|        |",
            r"\ 13  23 /",
            ].join("\n").to_string(),
            ansi);
        assert_eq!(vec![
            r"⎧ 11  21 ⎫",
            r"⎪        ⎪",
            r"⎨ 12  22 ⎬",
            r"⎪        ⎪",
            r"⎩ 13  23 ⎭",
            ].join("\n").to_string(),
            uni);
    }

    #[test]
    fn test_to_text_canvas_diff_size() {
        let fa = ObjFactory::new10();
        let m1 = CracovianData::from_vector(
            2, 2, vec![
                fa.rzi_u(11), fa.rfi_u(-30, 1000),
                fa.rzi_u(12), fa.rzi_u(22)]
        ).unwrap();

        let (ansi, uni) = println_text_variants(&m1);
        
        assert_eq!(vec![
            r"/     -30  \",
            r"| 11  ---- |",
            r"{     1000 }",
            r"|          |",
            r"\ 12   22  /"
            ].join("\n").to_string(),
            ansi);
        assert_eq!(vec![
            r"⎧     -30  ⎫",
            r"⎪ 11  ―――― ⎪",
            r"⎨     1000 ⎬",
            r"⎪          ⎪",
            r"⎩ 12   22  ⎭"
            ].join("\n").to_string(),
            uni);
    }

}
