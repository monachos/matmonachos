use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use matm_model::expressions::exponentiation::ExpData;
use crate::expressions::unary::unary_group_to_text_canvas;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;


impl TextCanvasRender for ExpData {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        // Layout:
        //  E
        //  E
        // B
        // B  - original base line of B
        // B
        let base_canvas = if self.should_wrap_base_with_parentheses() {
            unary_group_to_text_canvas(ctx, &self.base)?
        } else {
            self.base.to_text_canvas(ctx)?
        };
        let base_size = base_canvas.size().unwrap();
    
        let exp_canvas = if self.should_wrap_exponent_with_parentheses() {
            unary_group_to_text_canvas(ctx, &self.exponent)?
        } else {
            self.exponent.to_text_canvas(ctx)?
        };
        let exp_size = exp_canvas.size().unwrap();

        let base_pos = Index2D::new(exp_size.height + 1, 1)?;
        let exp_pos = Index2D::new(1, base_size.width + 1)?;
        let base_line = exp_size.height + base_size.height - base_canvas.get_rows_under_base_line()?;

        let mut canvas = TextCanvas::with_base_line(base_line);
        canvas.write_canvas(base_pos, &base_canvas)?;
        canvas.write_canvas(exp_pos, &exp_canvas)?;
        return Ok(canvas);
    }
}
