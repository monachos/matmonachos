#[cfg(test)]
mod tests {
    use rstest::rstest;
    use matm_model::expressions::ToExpression;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::func::FunctionData;
    use matm_model::factories::ObjFactory;
    use crate::testing::println_text_variants;

    #[rstest]
    #[case(FunctionData::SIN, "sin 0")]
    #[case(FunctionData::COS, "cos 0")]
    #[case(FunctionData::TAN, "tan 0")]
    #[case(FunctionData::COT, "cot 0")]
    #[case(FunctionData::SEC, "sec 0")]
    #[case(FunctionData::CSC, "csc 0")]
    #[case(FunctionData::SINH, "sinh 0")]
    #[case(FunctionData::COSH, "cosh 0")]
    #[case(FunctionData::TANH, "tanh 0")]
    #[case(FunctionData::COTH, "coth 0")]
    #[case(FunctionData::SECH, "sech 0")]
    #[case(FunctionData::CSCH, "csch 0")]
    fn test_to_text_canvas_trigonometric(
        #[case] func_name: &str,
        #[case] expected: &str,
    ) {
        let fa = ObjFactory::new10();
        let expr = FunctionData::new(
            func_name.to_owned(),
            vec![fa.rzi_u(0)])
            .to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!(expected, ansi);
    }

    #[test]
    fn test_to_text_canvas_sin_m1() {
        let fa = ObjFactory::new10();
        let expr = FunctionData::new(FunctionData::SIN.to_owned(), vec![fa.rzi_u(-1)]).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("sin(-1)", ansi);
    }

    #[test]
    fn test_to_text_canvas_sin_1p2() {
        let fa = ObjFactory::new10();
        let expr = FunctionData::new(FunctionData::SIN.to_owned(), vec![
            BinaryData::add(fa.rzi_u(1), fa.rzi_u(2)).to_rc_expr()
        ]).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("sin(1 + 2)", ansi);
    }

    #[test]
    fn test_to_text_canvas_no_args() {
        let m = FunctionData::new("foo".to_owned(), Vec::new());

        let (ansi, _) = println_text_variants(&m);

        assert_eq!("foo()", ansi);
    }

    #[test]
    fn test_format_with_args() {
        let fa = ObjFactory::new10();
        let m = FunctionData::new("foo".to_owned(), vec![fa.rz0(), fa.rsn("a")]);

        let (ansi, _) = println_text_variants(&m);

        assert_eq!("foo(0, a)", ansi);
    }
}
