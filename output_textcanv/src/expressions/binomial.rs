use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use matm_model::expressions::binomial::BinomialData;

use crate::expressions::unary::wrap_text_canvas_with_parenthesis;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;

impl TextCanvasRender for BinomialData {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        let set_canvas = self.set.to_text_canvas(ctx)?;
        let subset_canvas = self.subset.to_text_canvas(ctx)?;

        let mut line_canvas = TextCanvas::new();
        line_canvas.write_row(Index2D::one(), " ")?;

        let base_line = 1 + match set_canvas.size() {
            Some(s) => s.height,
            None => 0,
        };
        
        let canvases = vec![
            set_canvas,
            line_canvas,
            subset_canvas
        ];
        let mut coefs = TextCanvas::with_base_line(base_line);
        coefs.write_column_of_canvases(Index2D::one(), &canvases)?;

        let canvas = wrap_text_canvas_with_parenthesis(ctx, coefs)?;
        return Ok(canvas);
    }
}
