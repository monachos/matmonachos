#[cfg(test)]
mod tests {
    use rstest::rstest;
    use matm_model::expressions::object::ObjectData;
    use matm_model::expressions::object::ObjectType;
    use crate::testing::println_text_variants;

    #[rstest]
    #[case(ObjectType::Matrix, "{matrix}")]
    #[case(ObjectType::Cracovian, "{cracovian}")]
    fn test_to_text_canvas(
        #[case] input: ObjectType,
        #[case] expected: &str,
    ) {
        let obj = ObjectData::new(input);

        let (ansi, _) = println_text_variants(&obj);

        assert_eq!(expected, ansi);
    }
}
