#[cfg(test)]
mod tests {
    use matm_model::expressions::symbol::SymbolData;
    use matm_model::expressions::variable::VariableData;
    use matm_model::factories::ObjFactory;
    use crate::testing::println_text_variants;

    #[test]
    fn test_to_text_canvas_name() {
        let fa = ObjFactory::new10();
        let v = VariableData::new(SymbolData::new("a"), fa.rzi_u(2));
        
        let (ansi, _) = println_text_variants(&v);

        assert_eq!("a", ansi);
    }

    #[test]
    fn test_to_text_canvas_name_with_index() {
        let fa = ObjFactory::new10();
        let v = VariableData::new(SymbolData::with_index_e("a", fa.rzi_u(1)), fa.rzi_u(2));

        let (ansi, _) = println_text_variants(&v);

        assert_eq!(vec![
            "a ",
            " 1"
        ].join("\n").to_string(), ansi);
    }

}
