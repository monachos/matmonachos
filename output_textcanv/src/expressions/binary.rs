use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use matm_model::expressions::binary::BinaryData;
use matm_model::expressions::binary::BinaryOperator;
use crate::output::console::get_console_sign_string;
use crate::output::console::ConsoleSign;
use crate::expressions::unary::unary_group_to_text_canvas;
use crate::output::colors::TokenType;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;


impl TextCanvasRender for BinaryOperator {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        let symbol = match self {
            Self::Add => String::from("+"),
            Self::Sub => String::from("-"),
            Self::Mul => get_console_sign_string(ctx.charset, ConsoleSign::MiddleDot),
            Self::Div => String::from("/"),
        };
        return Ok(TextCanvas::from_row_text_color(
            format!("{}", symbol).as_str(),
            TokenType::Operator));
   }
}


impl TextCanvasRender for BinaryData {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        let left_canvas = if self.should_wrap_left_with_parentheses() {
            unary_group_to_text_canvas(ctx, &self.left)?
        } else {
            self.left.to_text_canvas(ctx)?
        };

        let right_canvas = if self.should_wrap_right_with_parentheses() {
            unary_group_to_text_canvas(ctx, &self.right)?
        } else {
            self.right.to_text_canvas(ctx)?
        };

        let mut canvases: Vec<TextCanvas> = Vec::new();

        canvases.push(left_canvas);
        canvases.push(TextCanvas::hidden_space());
        canvases.push(self.operator.to_text_canvas(ctx)?);
        canvases.push(TextCanvas::hidden_space());
        canvases.push(right_canvas);

        let canvas = TextCanvas::from_row_of_canvases(Index2D::one(), &canvases)?;
        return Ok(canvas);
   }
}
