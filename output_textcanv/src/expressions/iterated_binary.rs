use matm_model::core::dimensions::Size;
use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use matm_model::expressions::iterated_binary::IteratedBinaryData;
use matm_model::expressions::iterated_binary::IteratedBinaryOperator;
use crate::output::console::ConsoleSign;
use crate::output::console::get_console_sign_string;
use crate::output::colors::TokenType;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;


impl TextCanvasRender for IteratedBinaryData {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        return match self.operator {
            IteratedBinaryOperator::Sum => sum_to_text_canvas(ctx, self),
            IteratedBinaryOperator::Product => product_to_text_canvas(ctx, self),
        };
    }
}

fn sum_to_text_canvas(ctx: &TextCanvasContext, obj: &IteratedBinaryData) -> Result<TextCanvas, CalcError> {
    let upper_canvas = obj.upper_bound.to_text_canvas(ctx)?;
    let upper_size = upper_canvas.size().unwrap_or(Size::new(0, 0));

    let lower_canv_items = vec![
        obj.iterator.to_text_canvas(ctx)?,
        TextCanvas::from_row_text("="),
        obj.lower_bound.to_text_canvas(ctx)?,
    ];
    let lower_canvas = TextCanvas::from_row_of_canvases(Index2D::one(), &lower_canv_items)?;

    let symbol_base_line = 1 + upper_size.height + 1;

    let mut symbol_canv = TextCanvas::with_base_line(symbol_base_line);
    let mut row = 1;
    
    symbol_canv.write_canvas(Index2D::new(row, 1)?, &upper_canvas)?;
    row += upper_size.height;

    symbol_canv.write_row_color(
        Index2D::new(row, 1)?,
        &get_console_sign_string(ctx.charset, ConsoleSign::SummationTop),
        TokenType::Operator)?;
    symbol_canv.write_row_color(
        Index2D::new(row, 2)?,
        &get_console_sign_string(ctx.charset, ConsoleSign::SummationTopExt),
        TokenType::Operator)?;
    row += 1;

    symbol_canv.write_row_color(
        Index2D::new(row, 2)?,
        &get_console_sign_string(ctx.charset, ConsoleSign::SummationMiddle),
        TokenType::Operator)?;
    row += 1;

    symbol_canv.write_row_color(
        Index2D::new(row, 1)?,
        &get_console_sign_string(ctx.charset, ConsoleSign::SummationBottom),
        TokenType::Operator)?;
    symbol_canv.write_row_color(
        Index2D::new(row, 2)?,
        &get_console_sign_string(ctx.charset, ConsoleSign::SummationBottomExt),
        TokenType::Operator)?;
    row += 1;

    symbol_canv.write_canvas(Index2D::new(row, 1)?, &lower_canvas)?;

    let main_canvases = vec![
        symbol_canv,
        obj.value.to_text_canvas(ctx)?,
    ];
    let canvas = TextCanvas::from_row_of_canvases(Index2D::one(), &main_canvases)?;
    return Ok(canvas);
}

fn product_to_text_canvas(ctx: &TextCanvasContext, obj: &IteratedBinaryData) -> Result<TextCanvas, CalcError> {
    let upper_canvas = obj.upper_bound.to_text_canvas(ctx)?;
    let upper_size = upper_canvas.size().unwrap_or(Size::new(0, 0));

    let lower_canv_items = vec![
        obj.iterator.to_text_canvas(ctx)?,
        TextCanvas::from_row_text("="),
        obj.lower_bound.to_text_canvas(ctx)?,
    ];
    let lower_canvas = TextCanvas::from_row_of_canvases(Index2D::one(), &lower_canv_items)?;

    let symbol_base_line = 1 + upper_size.height + 1;

    let mut symbol_canv = TextCanvas::with_base_line(symbol_base_line);
    let mut row = 1;

    symbol_canv.write_canvas(Index2D::new(row, 1)?, &upper_canvas)?;
    row += upper_size.height;

    symbol_canv.write_row_color(
        Index2D::new(row, 1)?,
        format!(
            "{}{}{}{}",
            get_console_sign_string(ctx.charset, ConsoleSign::ProductLeftTopBar),
            get_console_sign_string(ctx.charset, ConsoleSign::ProductTopBar),
            get_console_sign_string(ctx.charset, ConsoleSign::ProductTopBar),
            get_console_sign_string(ctx.charset, ConsoleSign::ProductRightTopBar),
        ).as_str(),
        TokenType::Operator)?;
    row += 1;

    symbol_canv.write_row_color(
        Index2D::new(row, 1)?,
        format!(
            "{}  {}",
            get_console_sign_string(ctx.charset, ConsoleSign::ProductLeftMiddleBar),
            get_console_sign_string(ctx.charset, ConsoleSign::ProductRightMiddleBar),
        ).as_str(),
        TokenType::Operator)?;
    row += 1;

    symbol_canv.write_row_color(
        Index2D::new(row, 1)?,
        format!(
            "{}  {}",
            get_console_sign_string(ctx.charset, ConsoleSign::ProductLeftMiddleBar),
            get_console_sign_string(ctx.charset, ConsoleSign::ProductRightMiddleBar),
        ).as_str(),
        TokenType::Operator)?;
    row += 1;

    symbol_canv.write_canvas(Index2D::new(row, 1)?, &lower_canvas)?;
    
    let main_canvases = vec![
        symbol_canv,
        obj.value.to_text_canvas(ctx)?,
    ];
    let canvas = TextCanvas::from_row_of_canvases(Index2D::one(), &main_canvases)?;
    return Ok(canvas);
}

