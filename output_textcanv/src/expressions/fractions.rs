use std::cmp::max;
use matm_model::core::index::Index2D;
use matm_error::error::CalcError;
use matm_model::expressions::fractions::FracData;
use crate::output::console::get_console_sign_string;
use crate::output::console::ConsoleSign;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;


impl TextCanvasRender for FracData {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        let mut max_width = 0;

        let num_canvas = self.numerator.to_text_canvas(ctx)?;
        match num_canvas.size() {
            Some(size) => {
                max_width = size.width;
            }
            None => {}
        }
        let den_canvas = self.denominator.to_text_canvas(ctx)?;
        match den_canvas.size() {
            Some(size) => {
                max_width = max(max_width, size.width);
            }
            None => {}
        }

        if self.numerator.is_fraction() || self.denominator.is_fraction() {
            // enlarge line
            max_width += 2;
        }

        let mut line_canvas = TextCanvas::new();
        line_canvas.write_row(
            Index2D::one(),
            get_console_sign_string(ctx.charset, ConsoleSign::HorizLine).repeat(max_width).as_str())?;

        let base_line = 1 + match num_canvas.size() {
            Some(s) => s.height,
            None => 0,
        };
        
        let canvases = vec![
            num_canvas,
            line_canvas,
            den_canvas
        ];

        let mut canvas = TextCanvas::with_base_line(base_line);
        canvas.write_column_of_canvases(Index2D::one(), &canvases)?;
        return Ok(canvas);
    }
}

