#[cfg(test)]
mod tests {
    use matm_model::core::natural::RADIX_10;
    use matm_model::expressions::ToExpression;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::symbol::SymbolData;
    use matm_model::expressions::unary::UnaryData;
    use matm_model::expressions::matrices::MatrixData;
    use matm_model::factories::ObjFactory;
    use crate::output::text::TextCanvasContext;
    use crate::output::text::TextCanvasRender;
    use crate::testing::println_text_variants;


    #[test]
    fn test_to_text_canvas_group_int() {
        let fa = ObjFactory::new10();
        let expr = UnaryData::group(fa.rzi_u(2)).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("(2)", ansi);
    }

    #[test]
    fn test_to_text_canvas_group_frac() {
        let fa = ObjFactory::new10();
        let expr = UnaryData::group(fa.rfi_u(2, 5)).to_expr();
        let canv_ctx = TextCanvasContext::unicode_color();

        let canvas = expr.to_text_canvas(&canv_ctx).unwrap();
        assert_eq!(2, canvas.base_line());

        let (ansi, _) = println_text_variants(&expr);
        assert_eq!(vec![
            r"/2\",
            r"|-|",
            r"\5/"
        ].join("\n").to_string(), ansi);
    }

    #[test]
    fn test_to_text_canvas_group_binary() {
        let fa = ObjFactory::new10();
        let expr = UnaryData::group(
            BinaryData::add(
                fa.rfi_u(2, 5),
                fa.rzi_u(3),
            ).to_rc_expr()
        ).to_expr();

        let (ansi, uni) = println_text_variants(&expr);

        let canv_ctx = TextCanvasContext::unicode_color();
        let canvas = expr.to_text_canvas(&canv_ctx).unwrap();
        assert_eq!(2, canvas.base_line());

        assert_eq!(vec![
            r"/2    \",
            r"|- + 3|",
            r"\5    /"
            ].join("\n").to_string(), ansi);
        assert_eq!(vec![
            r"⎛2    ⎞",
            r"⎜― + 3⎟",
            r"⎝5    ⎠"
            ].join("\n").to_string(), uni);
    }

    #[test]
    fn test_to_text_canvas_neg_int() {
        let fa = ObjFactory::new10();
        let expr = UnaryData::neg(fa.rzi_u(2)).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("-2", ansi);
    }

    #[test]
    fn test_to_text_canvas_neg_frac() {
        //TODO this is not readable
        let fa = ObjFactory::new10();
        let expr = UnaryData::neg(fa.rfi_u(2, 5)).to_expr();
        let canv_ctx = TextCanvasContext::unicode_color();
        let canvas = expr.to_text_canvas(&canv_ctx).unwrap();
        assert_eq!(2, canvas.base_line());

        let (ansi, uni) = println_text_variants(&expr);
        assert_eq!(vec![
            r" 2",
            r"--",
            r" 5"
        ].join("\n").to_string(), ansi);
        assert_eq!(vec![
            r" 2",
            r"-―",
            r" 5"
        ].join("\n").to_string(), uni);
    }

    #[test]
    fn test_to_text_canvas_transpose_matrix() {
        let expr = UnaryData::transpose(
            MatrixData::identity(RADIX_10, 3).unwrap().to_rc_expr()
        ).to_expr();
        let canv_ctx = TextCanvasContext::unicode_color();

        let canvas = expr.to_text_canvas(&canv_ctx).unwrap();
        assert_eq!(3, canvas.base_line());

        let (ansi, _) = println_text_variants(&expr);
        assert_eq!(vec![
            r"/ 1  0  0 \ T",
            r"|         |  ",
            r"| 0  1  0 |  ",
            r"|         |  ",
            r"\ 0  0  1 /  "
            ].join("\n").to_string(),
            ansi);
    }

    #[test]
    fn test_to_text_canvas_determinant_symbol() {
        let expr = UnaryData::det(
            SymbolData::new("A").to_rc_expr()
        ).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("det(A)", ansi);
    }

    #[test]
    fn test_to_text_canvas_determinant_matrix() {
        let expr = UnaryData::det(
            MatrixData::identity(RADIX_10, 3).unwrap().to_rc_expr()
        ).to_expr();
        let canv_ctx = TextCanvasContext::unicode_color();

        let canvas = expr.to_text_canvas(&canv_ctx).unwrap();
        assert_eq!(3, canvas.base_line());

        let (ansi, _) = println_text_variants(&expr);
        assert_eq!(vec![
            r"   / 1  0  0 \",
            r"   |         |",
            r"det| 0  1  0 |",
            r"   |         |",
            r"   \ 0  0  1 /"
            ].join("\n").to_string(),
            ansi);
    }

    #[test]
    fn test_to_text_canvas_rank_symbol() {
        let exp = UnaryData::rank(
            SymbolData::new("A").to_rc_expr()
        ).to_expr();

        let (ansi, _) = println_text_variants(&exp);

        assert_eq!("rank(A)", ansi);
    }

    #[test]
    fn test_to_text_canvas_rank_matrix() {
        let expr = UnaryData::rank(
            MatrixData::identity(RADIX_10, 3).unwrap().to_rc_expr()
        ).to_expr();
        let canv_ctx = TextCanvasContext::unicode_color();

        let canvas = expr.to_text_canvas(&canv_ctx).unwrap();
        assert_eq!(3, canvas.base_line());

        let (ansi, _) = println_text_variants(&expr);
        assert_eq!(vec![
            r"    / 1  0  0 \",
            r"    |         |",
            r"rank| 0  1  0 |",
            r"    |         |",
            r"    \ 0  0  1 /"
            ].join("\n").to_string(),
            ansi);
    }

    #[test]
    fn test_to_text_canvas_factorial() {
        let fa = ObjFactory::new10();
        let expr = UnaryData::factorial(fa.rzi_u(2)).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("2!", ansi);
    }

    #[test]
    fn test_to_text_canvas_factorial_of_binary() {
        let fa = ObjFactory::new10();
        let expr = UnaryData::factorial(
            BinaryData::add(fa.rzi_u(2), fa.rzi_u(3)).to_rc_expr()
        ).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("(2 + 3)!", ansi);
    }

    // #[test]
    // fn test_to_text_canvas_abs_int() {
    //     let fa = ObjFactory::new10();
    //     let expr = Unary::abs(fa.rzi_u(2)).to_expr();

    //     let (ansi, _) = println_text_variants(&expr);

    //     assert_eq!("|2|", ansi);
    // }

    // #[test]
    // fn test_to_text_canvas_abs_frac() {
    //     let fa = ObjFactory::new10();
    //     let expr = Unary::abs(fa.rfi_u(1, 2)).to_expr();
    //     let canv_ctx = TextCanvasContext::unicode_color();
    //     let canvas = expr.to_text_canvas(&canv_ctx).unwrap();
    //     assert_eq!(2, canvas.base_line());

    //     let (ansi, uni) = println_text_variants(&expr);
    //     assert_eq!(vec![
    //         "|1|",
    //         "|-|",
    //         "|2|",
    //         ].join("\n").to_string(), ansi);
    //     assert_eq!(vec![
    //         "|1|",
    //         "|―|",
    //         "|2|",
    //         ].join("\n").to_string(), uni);
    // }

}
