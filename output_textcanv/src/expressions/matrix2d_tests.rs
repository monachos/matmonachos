#[cfg(test)]
mod tests {
    use matm_model::core::index::Index2D;
    use matm_model::expressions::matrices::MatrixData;
    use matm_model::factories::ObjFactory;
    use crate::testing::println_text_variants;
    use crate::matrix;

    #[test]
    fn test_to_text_canvas_1_1() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 1, 1; 11).unwrap();
        
        let (ansi, _) = println_text_variants(&m1);

        assert_eq!(vec![
            r"[ 11 ]"
            ].join("\n").to_string(),
            ansi);
    }

    #[test]
    fn test_to_text_canvas_1_2() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 1, 2; 11, 12).unwrap();
        
        let (ansi, _) = println_text_variants(&m1);
        
        assert_eq!(vec![
            r"[ 11  12 ]"
            ].join("\n").to_string(),
            ansi);
    }

    #[test]
    fn test_to_text_canvas_int_frac() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 1, 2; 11, (2/3)).unwrap();

        let (ansi, uni) = println_text_variants(&m1);

        assert_eq!(vec![
            r"/     2 \",
            r"| 11  - |",
            r"\     3 /"
            ].join("\n").to_string(),
            ansi);
        assert_eq!(vec![
            r"⎡     2 ⎤",
            r"⎢ 11  ― ⎥",
            r"⎣     3 ⎦"
            ].join("\n").to_string(),
            uni);
    }

    #[test]
    fn test_to_text_canvas_2_2() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 2, 2;
            11, 12,
            21, 22
        ).unwrap();

        let (ansi, uni) = println_text_variants(&m1);
        
        assert_eq!(vec![
            r"/ 11  12 \",
            r"|        |",
            r"\ 21  22 /"
            ].join("\n").to_string(),
            ansi);
        assert_eq!(vec![
            r"⎡ 11  12 ⎤",
            r"⎢        ⎥",
            r"⎣ 21  22 ⎦"
            ].join("\n").to_string(),
            uni);
    }

    #[test]
    fn test_to_text_canvas_3_2() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 3, 2;
            11, 12,
            21, 22,
            31, 32
        ).unwrap();

        let (ansi, uni) = println_text_variants(&m1);
        
        assert_eq!(vec![
            r"/ 11  12 \",
            r"|        |",
            r"| 21  22 |",
            r"|        |",
            r"\ 31  32 /",
            ].join("\n").to_string(),
            ansi);
        assert_eq!(vec![
            r"⎡ 11  12 ⎤",
            r"⎢        ⎥",
            r"⎢ 21  22 ⎥",
            r"⎢        ⎥",
            r"⎣ 31  32 ⎦",
            ].join("\n").to_string(),
            uni);
    }

    #[test]
    fn test_to_text_canvas_of_submatrix_with_crossed_lines() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 3, 3;
            11, 12, 13,
            21, 22, 23,
            31, 32, 33
        ).unwrap();
        let crossed = m1.get_submatrix_with_crossed_lines(&Index2D::new(1, 2).unwrap()).unwrap();

        let (ansi, uni) = println_text_variants(&crossed);
        
        assert_eq!(vec![
            r"/  ▢  ▢   ▢ \",
            r"|           |",
            r"| 21  ▢  23 |",
            r"|           |",
            r"\ 31  ▢  33 /",
            ].join("\n").to_string(),
            ansi);
        assert_eq!(vec![
            r"⎡  ▢  ▢   ▢ ⎤",
            r"⎢           ⎥",
            r"⎢ 21  ▢  23 ⎥",
            r"⎢           ⎥",
            r"⎣ 31  ▢  33 ⎦",
            ].join("\n").to_string(),
            uni);
    }

    #[test]
    fn test_to_text_canvas_diff_size() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 2, 2;
                11, (-30/1000),
                21, 22
        ).unwrap();

        let (ansi, uni) = println_text_variants(&m1);
        
        assert_eq!(vec![
            r"/     -30  \",
            r"| 11  ---- |",
            r"|     1000 |",
            r"|          |",
            r"\ 21   22  /"
            ].join("\n").to_string(),
            ansi);
        assert_eq!(vec![
            r"⎡     -30  ⎤",
            r"⎢ 11  ―――― ⎥",
            r"⎢     1000 ⎥",
            r"⎢          ⎥",
            r"⎣ 21   22  ⎦"
            ].join("\n").to_string(),
            uni);
    }

    #[test]
    fn test_to_text_canvas_3_2_aug_3_1() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 3, 2;
            11, 12,
            21, 22,
            31, 32
        ).unwrap();
        let m2 = matrix!(fa, 3, 1;
            10,
            20,
            30
        ).unwrap();
        
        let m = MatrixData::augmented(&m1, &m2).unwrap();

        let (ansi, uni) = println_text_variants(&m);
        
        assert_eq!(vec![
            r"/ 11  12 | 10 \",
            r"|        |    |",
            r"| 21  22 | 20 |",
            r"|        |    |",
            r"\ 31  32 | 30 /",
            ].join("\n").to_string(),
            ansi);
        assert_eq!(vec![
            r"⎡ 11  12 | 10 ⎤",
            r"⎢        |    ⎥",
            r"⎢ 21  22 | 20 ⎥",
            r"⎢        |    ⎥",
            r"⎣ 31  32 | 30 ⎦",
            ].join("\n").to_string(),
            uni);
    }

}
