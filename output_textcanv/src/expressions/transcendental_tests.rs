#[cfg(test)]
mod tests {
    use matm_model::expressions::transcendental::TranscData;
    use crate::testing::println_text_variants;

    #[test]
    fn test_to_text_canvas_pi() {
        let number = TranscData::Pi;

        let (ansi, uni) = println_text_variants(&number);

        assert_eq!("pi", ansi);
        assert_eq!("𝜋", uni);
    }

    #[test]
    fn test_to_text_canvas_euler() {
        let number = TranscData::Euler;

        let (ansi, uni) = println_text_variants(&number);

        assert_eq!("e", ansi);
        assert_eq!("𝑒", uni);
    }

}