use matm_model::core::dimensions::Size;
use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use matm_model::expressions::Expression;
use matm_model::expressions::root::RootData;
use crate::output::console::ConsoleSign;
use crate::output::console::get_console_sign_string;
use crate::output::colors::TokenType;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;


impl TextCanvasRender for RootData {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        let is_square = if let Expression::Int(de) = self.degree.as_ref() {
            if let Some(de_val) = de.to_i32() {
                de_val == 2
            } else {
                false
            }
        } else {
            false
        };
        let (degree_size, degree_canvas) = if is_square {
            (Size::new(1, 1), None)
        } else {
            let degree_canvas = self.degree.to_text_canvas(ctx)?;
            let degree_size = degree_canvas.size().unwrap_or(Size::new(1, 1));
            (degree_size, Some(degree_canvas))
        };
        // let degree_canvas = self.degree.to_text_canvas(ctx)?;
        // let degree_size = degree_canvas.size().unwrap_or(Size::new(1, 1));
        let radicand_canvas = self.radicand.to_text_canvas(ctx)?;
        let radicand_size = radicand_canvas.size().unwrap_or(Size::new(1, 1));
        let sign_canvas = create_root_sign_canvas(ctx, &radicand_size)?;

        let radicand_pos = Index2D::new(degree_size.height + 1, degree_size.width + radicand_size.height)?;

        let mut canvas = TextCanvas::with_base_line(radicand_canvas.base_line() + 1);
        canvas.write_canvas(
            Index2D::new(degree_size.height, degree_size.width)?,
            &sign_canvas)?;
        if let Some(degree_canvas_val) = degree_canvas {
            canvas.write_canvas(
                Index2D::new(1, 1)?,
                &degree_canvas_val)?;
        }
        canvas.write_canvas(radicand_pos, &radicand_canvas)?;
        return Ok(canvas);
    }
}


fn create_root_sign_canvas(ctx: &TextCanvasContext, radicand_size: &Size<usize>) -> Result<TextCanvas, CalcError> {
    let mut sign_canvas = TextCanvas::new();
    let mut row_pos = radicand_size.height + 1;
    let mut col_pos = 1;
    let rad_sign = if radicand_size.height > 1 {
        String::from("V")
    } else {
        get_console_sign_string(ctx.charset, ConsoleSign::RadicalBottom)
    };
    sign_canvas.write_row_color(
        Index2D::new(row_pos, col_pos)?,
        &rad_sign,
        TokenType::Operator)?;
    row_pos -= 1;
    col_pos += 1;
    // slashes
    for _ in 0..radicand_size.height-1 {
        sign_canvas.write_row_color(
            Index2D::new(row_pos, col_pos)?,
            "/",
            TokenType::Operator)?;
        row_pos -= 1;
        col_pos += 1;
    }
    // vertical line
    sign_canvas.write_row_color(
        Index2D::new(row_pos, col_pos)?,
        "_".repeat(radicand_size.width).as_str(),
        TokenType::Operator)?;
    return Ok(sign_canvas);
}
