#[cfg(test)]
mod tests {
    use rstest::rstest;
    use matm_model::expressions::ToExpression;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::conditional::ConditionalData;
    use matm_model::expressions::conditional::ConditionalClause;
    use matm_model::expressions::arithmetic_condition::ArithmeticCondition;
    use matm_model::expressions::arithmetic_operator::ArithmeticOperator;
    use matm_model::expressions::logic_operator::LogicOperator;
    use matm_model::expressions::logic_condition::LogicCondition;
    use matm_model::expressions::condition::ToCondition;
    use matm_model::expressions::exponentiation::ExpData;
    use matm_model::expressions::root::RootData;
    use matm_model::factories::ObjFactory;
    use crate::testing::println_text_variants;


    #[rstest]
    #[case(ArithmeticOperator::Equal, "=", "=")]
    #[case(ArithmeticOperator::NotEqual, "<>", "≠")]
    #[case(ArithmeticOperator::Greater, ">", ">")]
    #[case(ArithmeticOperator::GreaterOrEqual, ">=", "≥")]
    #[case(ArithmeticOperator::Less, "<", "<")]
    #[case(ArithmeticOperator::LessOrEqual, "<=", "≤")]
    fn test_arithmetic_operator_to_text_canvas(
        #[case] op: ArithmeticOperator,
        #[case] expected_ansi: &str,
        #[case] expected_uni: &str,
    ) {
        let (ansi, uni) = println_text_variants(&op);

        assert_eq!(expected_ansi, ansi);
        assert_eq!(expected_uni, uni);
    }

    //--------------------------------------------------

    #[test]
    fn test_arithmetic_condition_to_text_canvas() {
        let fa = ObjFactory::new10();
        let cond = ArithmeticCondition::gt(fa.rsn("x"), fa.rz0()).to_cond();
        
        let (ansi, _) = println_text_variants(&cond);

        assert_eq!("x > 0", ansi);
    }

    //--------------------------------------------------

    #[rstest]
    #[case(LogicOperator::And, "and")]
    #[case(LogicOperator::Or, "or")]
    fn test_logic_operator_to_text_canvas(#[case] op: LogicOperator, #[case] expected: &str) {
        let (ansi, _) = println_text_variants(&op);

        assert_eq!(expected, ansi);
    }

    //--------------------------------------------------

    #[test]
    fn test_logic_condition_to_text_canvas() {
        let fa = ObjFactory::new10();
        let cond = LogicCondition::and(
            ArithmeticCondition::gt(fa.rsn("x"), fa.rz0()).to_rc_cond(),
            ArithmeticCondition::lt(fa.rsn("x"), fa.rzi_u(10)).to_rc_cond()
        );
        
        let (ansi, _) = println_text_variants(&cond);

        assert_eq!("x > 0 and x < 10", ansi);
    }

    #[test]
    fn test_logic_condition_to_text_canvas_groups() {
        let fa = ObjFactory::new10();
        let cond = LogicCondition::and(
            LogicCondition::or(
                ArithmeticCondition::gt(fa.rsn("x"), fa.rz0()).to_rc_cond(),
                ArithmeticCondition::lt(fa.rsn("x"), fa.rzi_u(10)).to_rc_cond()
            ).to_rc_cond(),
            LogicCondition::or(
                ArithmeticCondition::gt(fa.rsn("y"), fa.rz0()).to_rc_cond(),
                ArithmeticCondition::lt(fa.rsn("y"), fa.rzi_u(10)).to_rc_cond()
            ).to_rc_cond()
        );
        
        let (ansi, _) = println_text_variants(&cond);

        assert_eq!("(x > 0 or x < 10) and (y > 0 or y < 10)", ansi);
    }

    //--------------------------------------------------

    #[test]
    fn test_condition_to_text_canvas() {
        let fa = ObjFactory::new10();
        let cond = LogicCondition::and(
            ArithmeticCondition::gt(fa.rsn("x"), fa.rz0()).to_rc_cond(),
            ArithmeticCondition::lt(fa.rsn("x"), fa.rzi_u(10)).to_rc_cond()
        ).to_cond();

        let (ansi, _) = println_text_variants(&cond);

        assert_eq!("x > 0 and x < 10", ansi);
    }

    //--------------------------------------------------

    #[test]
    fn test_to_text_canvas_1_cond() {
        let fa = ObjFactory::new10();
        let expr = ConditionalData::from_vector(
            vec![
                ConditionalClause {
                    expression: fa.rz1(),
                    condition: ArithmeticCondition::gt(fa.rsn("x"), fa.rz0()).to_rc_cond(),
                },
            ]);

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!(vec![
            r"{ 1    x > 0"
            ].join("\n").to_string(),
            ansi);
    }

    #[test]
    fn test_to_text_canvas_2_cond() {
        let fa = ObjFactory::new10();
        let expr = ConditionalData::from_vector(
            vec![
                ConditionalClause {
                    expression: RootData::new(
                        BinaryData::add(
                            ExpData::new(fa.rsn("x"), fa.rzi_u(2)).to_rc_expr(),
                            fa.rzi_u(1)
                        ).to_rc_expr(),
                        fa.rzi_u(3)
                    ).to_rc_expr(),
                    condition: ArithmeticCondition::gt(fa.rsn("x"), fa.rz0()).to_rc_cond(),
                },
                ConditionalClause {
                    expression: fa.rzi_u(-1),
                    condition: ArithmeticCondition::lt(fa.rsn("x"), fa.rz0()).to_rc_cond(),
                },
            ]);

        let (ansi, uni) = println_text_variants(&expr);

        assert_eq!(vec![
            r"/ 3 ______         ",
            r"|  / 2             ",
            r"{ V x  + 1    x > 0",
            r"|                  ",
            r"\ -1          x < 0"
            ].join("\n").to_string(),
            ansi);
        assert_eq!(vec![
            r"⎧ 3 ______         ",
            r"⎪  / 2             ",
            r"⎨ V x  + 1    x > 0",
            r"⎪                  ",
            r"⎩ -1          x < 0"
            ].join("\n").to_string(),
            uni);
    }


}