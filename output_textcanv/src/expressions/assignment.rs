use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use matm_model::expressions::assignment::AssignmentData;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;


impl TextCanvasRender for AssignmentData {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        let target_canvas = self.target.to_text_canvas(ctx)?;
        let source_canvas = self.source.to_text_canvas(ctx)?;

        let canvases: Vec<TextCanvas> = vec![
            target_canvas,
            TextCanvas::from_row_text(":"),
            TextCanvas::hidden_space(),
            source_canvas,
        ];

        let canvas = TextCanvas::from_row_of_canvases(Index2D::one(), &canvases)?;
        return Ok(canvas);
   }
}
