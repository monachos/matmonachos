use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use matm_model::expressions::int::IntData;
use crate::output::colors::TokenType;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;


impl TextCanvasRender for IntData {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        let mut canvas = TextCanvas::new();

        let mut row = canvas.row(Index2D::one());

        if self.is_negative() {
            row.token("-", TokenType::Negation)?;
        }

        let natural_canvas = self.natural().to_text_canvas(ctx)?;
        row.canvas(&natural_canvas)?;

        return Ok(canvas);
    }
}
