#[cfg(test)]
mod tests {
    use matm_model::testing::asserts::assert_result_ok;
    use matm_model::expressions::ToExpression;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::symbol::SymbolData;
    use matm_model::expressions::fractions::FracData;
    use matm_model::factories::ObjFactory;
    use crate::output::text::TextCanvasContext;
    use crate::output::text::TextCanvasRender;
    use crate::testing::println_text_variants;

    #[test]
    fn test_to_text_canvas_2_3() {
        let fa = ObjFactory::new10();
        let f = fa.fi_u(2, 3);
        
        let canv_ctx = TextCanvasContext::unicode_color();
        let canvas = assert_result_ok(f.to_text_canvas(&canv_ctx));

        assert_eq!(2, canvas.base_line());
        assert_eq!(1, canvas.get_rows_over_base_line().unwrap());
        assert_eq!(1, canvas.get_rows_under_base_line().unwrap());

        let (ansi, uni) = println_text_variants(&f);

        assert_eq!(vec![
            "2",
            "-",
            "3"
            ].join("\n").to_string(), ansi);
        assert_eq!(vec![
            "2",
            "―",
            "3"
            ].join("\n").to_string(), uni);
    }

    #[test]
    fn test_to_text_canvas_m2_3() {
        let fa = ObjFactory::new10();
        let f = fa.fi_u(-2, 3);
        
        let (ansi, uni) = println_text_variants(&f);

        assert_eq!(vec![
            "-2",
            "--",
            "3 "
            ].join("\n").to_string(), ansi);
        assert_eq!(vec![
            "-2",
            "――",
            "3 "
            ].join("\n").to_string(), uni);
    }

    #[test]
    fn test_to_text_canvas_2_m3() {
        let fa = ObjFactory::new10();
        let f = fa.fi_u(2, -3);
        
        let (ansi, _) = println_text_variants(&f);

        assert_eq!(vec![
            "2 ",
            "--",
            "-3"
            ].join("\n").to_string(), ansi);
    }

    #[test]
    fn test_to_text_canvas_1_x() {
        let fa = ObjFactory::new10();
        let expr = FracData::new(
            fa.rzi_u(1),
            SymbolData::new("x").to_rc_expr()
        );

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!(vec![
            "1",
            "-",
            "x"
            ].join("\n").to_string(), ansi);
    }

    #[test]
    fn test_to_text_canvas_1_xpy() {
        let fa = ObjFactory::new10();
        let expr = FracData::new(
            fa.rzi_u(1),
            BinaryData::add(
                SymbolData::new("x").to_rc_expr(),
                SymbolData::new("y").to_rc_expr(),
            ).to_rc_expr()
        );

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!(vec![
            "  1  ",
            "-----",
            "x + y"
            ].join("\n").to_string(), ansi);
    }

    #[test]
    fn test_to_text_canvas_frac_int() {
        let fa = ObjFactory::new10();
        let f = fa.fe(fa.efi_u(1, 2), fa.ezi_u(3));
        
        let (ansi, _) = println_text_variants(&f);

        assert_eq!(vec![
            " 1 ",
            " - ",
            " 2 ",
            "---",
            " 3 "
            ].join("\n").to_string(), ansi);
    }

    #[test]
    fn test_to_text_canvas_int_frac() {
        let fa = ObjFactory::new10();
        let f = fa.fe(fa.ezi_u(1), fa.efi_u(2, 3));
        
        let (ansi, _) = println_text_variants(&f);

        assert_eq!(vec![
            " 1 ",
            "---",
            " 2 ",
            " - ",
            " 3 "
            ].join("\n").to_string(), ansi);
    }

    #[test]
    fn test_to_text_canvas_frac_frac() {
        let fa = ObjFactory::new10();
        let f = fa.fe(fa.efi_u(1, 2), fa.efi_u(3, 4));
        
        let (ansi, _) = println_text_variants(&f);

        assert_eq!(vec![
            " 1 ",
            " - ",
            " 2 ",
            "---",
            " 3 ",
            " - ",
            " 4 "
            ].join("\n").to_string(), ansi);
    }
}