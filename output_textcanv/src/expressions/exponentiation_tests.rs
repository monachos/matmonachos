#[cfg(test)]
mod tests {
    use matm_model::expressions::symbol::SymbolData;
    use matm_model::expressions::ToExpression;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::exponentiation::ExpData;
    use matm_model::factories::ObjFactory;
    use matm_model::testing::asserts::assert_result_ok;
    use crate::output::text::TextCanvasContext;
    use crate::output::text::TextCanvasRender;
    use crate::testing::println_text_variants;


    #[test]
    fn test_to_text_canvas_exp() {
        let fa = ObjFactory::new10();
        let expr = ExpData::new(
            fa.rzi_u(2),
            fa.rzi_u(3),
        ).to_expr();

        let canv_ctx = TextCanvasContext::unicode_color();
        let canvas = assert_result_ok(expr.to_text_canvas(&canv_ctx));
        assert_eq!(2, canvas.base_line());
        assert_eq!(1, canvas.get_rows_over_base_line().unwrap());
        assert_eq!(0, canvas.get_rows_under_base_line().unwrap());

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!(vec![
            " 3",
            "2 "
        ].join("\n").to_string(), ansi);
    }

    #[test]
    fn test_to_text_canvas_symbol_e_exp_int() {
        let fa = ObjFactory::new10();
        let expr = ExpData::new(
            SymbolData::with_index_e("x", fa.rzi_u(1)).to_rc_expr(),
            fa.rzi_u(2),
        ).to_expr();

        let canv_ctx = TextCanvasContext::unicode_color();
        let canvas = assert_result_ok(expr.to_text_canvas(&canv_ctx));
        assert_eq!(2, canvas.base_line());
        assert_eq!(1, canvas.get_rows_over_base_line().unwrap());
        assert_eq!(1, canvas.get_rows_under_base_line().unwrap());

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!(vec![
            "  2",
            "x  ",
            " 1 ",
        ].join("\n").to_string(), ansi);
    }

    #[test]
    fn test_to_text_canvas_exp_positive_negative() {
        let fa = ObjFactory::new10();
        let expr = ExpData::new(
            fa.rzi_u(2),
            fa.rzi_u(-3),
        ).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!(vec![
            " (-3)",
            "2    "
        ].join("\n").to_string(), ansi);
    }

    #[test]
    fn test_to_text_canvas_exp_positive_1p2() {
        let fa = ObjFactory::new10();
        let expr = ExpData::new(
            fa.rzi_u(2),
            BinaryData::add(fa.rzi_u(1), fa.rzi_u(2)).to_rc_expr()
        ).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!(vec![
            " (1 + 2)",
            "2       "
        ].join("\n").to_string(), ansi);
    }

    #[test]
    fn test_to_text_canvas_exp_negative_positive() {
        let fa = ObjFactory::new10();
        let expr = ExpData::new(
            fa.rzi_u(-2),
            fa.rzi_u(3),
        ).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!(vec![
            "    3",
            "(-2) "
        ].join("\n").to_string(), ansi);
    }

}
