#[cfg(test)]
mod tests {
    use matm_model::expressions::unary::UnaryData;
    use matm_model::expressions::ToExpression;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::expressions::exponentiation::ExpData;
    use matm_model::expressions::symbol::SymbolData;
    use matm_model::factories::ObjFactory;
    use crate::output::text::{TextCanvasRender, TextCanvasContext};
    use crate::testing::println_text_variants;


    #[test]
    fn test_to_text_canvas_add() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::add(
            fa.rzi_u(2),
            fa.rzi_u(3),
        ).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("2 + 3", ansi);
    }

    #[test]
    fn test_to_text_canvas_add_positive_negative() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::add(
            fa.rzi_u(2),
            fa.rzi_u(-3),
        ).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("2 + (-3)", ansi);
    }

    #[test]
    fn test_to_text_canvas_add_positive_neg() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::add(
            fa.rzi_u(2),
            UnaryData::neg(fa.rzi_u(3)).to_rc_expr(),
        ).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("2 + (-3)", ansi);
    }

    #[test]
    fn test_to_text_canvas_add_negative_positive() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::add(
            fa.rzi_u(-2),
            fa.rzi_u(3),
        ).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("(-2) + 3", ansi);
    }

    #[test]
    fn test_to_text_canvas_add_neg_positive() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::add(
            UnaryData::neg(fa.rzi_u(2)).to_rc_expr(),
            fa.rzi_u(3),
        ).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("(-2) + 3", ansi);
    }

    #[test]
    fn test_to_text_canvas_add_negative_frac() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::add(
            fa.rfi_u(-1, 9),
            fa.rzi_u(3),
        ).to_expr();

        let canv_ctx = TextCanvasContext::unicode_color();
        let canvas = expr.to_text_canvas(&canv_ctx).unwrap();
        assert_eq!(2, canvas.base_line());
        assert_eq!(1, canvas.get_rows_over_base_line().unwrap());
        assert_eq!(1, canvas.get_rows_under_base_line().unwrap());

        let (ansi, uni) = println_text_variants(&expr);

        assert_eq!(vec![
            r"-1    ",
            r"-- + 3",
            r"9     "
        ].join("\n").to_string(), ansi);
        assert_eq!(vec![
            r"-1    ",
            r"―― + 3",
            r"9     "
        ].join("\n").to_string(), uni);
    }

    #[test]
    fn test_to_text_canvas_add_symbol_e_exp() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::add(
            SymbolData::with_index_e("x", fa.rzi_u(1)).to_rc_expr(),
            ExpData::new(SymbolData::new("y").to_rc_expr(), fa.rzi_u(2)).to_rc_expr()
        ).to_expr();

        let canv_ctx = TextCanvasContext::unicode_color();
        let canvas = expr.to_text_canvas(&canv_ctx).unwrap();
        assert_eq!(2, canvas.base_line());
        assert_eq!(1, canvas.get_rows_over_base_line().unwrap());
        assert_eq!(1, canvas.get_rows_under_base_line().unwrap());

        let (ansi, _) = println_text_variants(&expr);
        assert_eq!(vec![
            r"      2",
            r"x  + y ",
            r" 1     "
        ].join("\n").to_string(), ansi);
    }

    #[test]
    fn test_to_text_canvas_add_symbol_e_exp_symbol_e() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::add(
            SymbolData::with_index_e("x", fa.rzi_u(1)).to_rc_expr(),
            ExpData::new(
                SymbolData::with_index_e("y", fa.rzi_u(1)).to_rc_expr(),
                fa.rzi_u(2)).to_rc_expr()
        ).to_expr();

        println_text_variants(&expr);

        let canv_ctx = TextCanvasContext::unicode_color();
        let canvas = expr.to_text_canvas(&canv_ctx).unwrap();
        assert_eq!(2, canvas.base_line());
        assert_eq!(1, canvas.get_rows_over_base_line().unwrap());
        assert_eq!(1, canvas.get_rows_under_base_line().unwrap());

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!(vec![
            r"       2",
            r"x  + y  ",
            r" 1    1 "
        ].join("\n").to_string(), ansi);
    }

    #[test]
    fn test_to_text_canvas_add_int_mul() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::add(
            fa.rzi_u(2),
            BinaryData::mul(fa.rzi_u(3), fa.rzi_u(4)).to_rc_expr()
        ).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("2 + 3 * 4", ansi);
    }

    //---------------------------------------------------

    #[test]
    fn test_to_text_canvas_sub() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::sub(
            fa.rzi_u(2),
            fa.rzi_u(3),
        ).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("2 - 3", ansi);
    }

    #[test]
    fn test_to_text_canvas_sub_positive_negative() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::sub(
            fa.rzi_u(2),
            fa.rzi_u(-3),
        ).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("2 - (-3)", ansi);
    }

    #[test]
    fn test_to_text_canvas_sub_positive_neg() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::sub(
            fa.rzi_u(2),
            UnaryData::neg(fa.rzi_u(3)).to_rc_expr(),
        ).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("2 - (-3)", ansi);
    }

    #[test]
    fn test_to_text_canvas_sub_negative_positive() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::sub(
            fa.rzi_u(-2),
            fa.rzi_u(3),
        ).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("(-2) - 3", ansi);
    }

    #[test]
    fn test_to_text_canvas_sub_neg_positive() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::sub(
            UnaryData::neg(fa.rzi_u(2)).to_rc_expr(),
            fa.rzi_u(3),
        ).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("(-2) - 3", ansi);
    }

    //---------------------------------------------------

    #[test]
    fn test_to_text_canvas_mul() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::mul(
            fa.rzi_u(2),
            fa.rzi_u(3),
        ).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("2 * 3", ansi);
    }

    #[test]
    fn test_to_text_canvas_mul_positive_negative() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::mul(
            fa.rzi_u(2),
            fa.rzi_u(-3),
        ).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("2 * (-3)", ansi);
    }

    #[test]
    fn test_to_text_canvas_mul_positive_neg() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::mul(
            fa.rzi_u(2),
            UnaryData::neg(fa.rzi_u(3)).to_rc_expr(),
        ).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("2 * (-3)", ansi);
    }

    #[test]
    fn test_to_text_canvas_mul_negative_positive() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::mul(
            fa.rzi_u(-2),
            fa.rzi_u(3),
        ).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("(-2) * 3", ansi);
    }

    #[test]
    fn test_to_text_canvas_mul_neg_positive() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::mul(
            UnaryData::neg(fa.rzi_u(2)).to_rc_expr(),
            fa.rzi_u(3),
        ).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("(-2) * 3", ansi);
    }

    #[test]
    fn test_to_text_canvas_mul_int_add() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::mul(
            fa.rzi_u(2),
            BinaryData::add(fa.rzi_u(3), fa.rzi_u(4)).to_rc_expr()
        ).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("2 * (3 + 4)", ansi);
    }

    //---------------------------------------------------

    #[test]
    fn test_to_text_canvas_div() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::div(
            fa.rzi_u(2),
            fa.rzi_u(3),
        ).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("2 / 3", ansi);
    }

    #[test]
    fn test_to_text_canvas_div_positive_negative() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::div(
            fa.rzi_u(2),
            fa.rzi_u(-3),
        ).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("2 / (-3)", ansi);
    }

    #[test]
    fn test_to_text_canvas_div_positive_neg() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::div(
            fa.rzi_u(2),
            UnaryData::neg(fa.rzi_u(3)).to_rc_expr(),
        ).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("2 / (-3)", ansi);
    }

    #[test]
    fn test_to_text_canvas_div_negative_positive() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::div(
            fa.rzi_u(-2),
            fa.rzi_u(3),
        ).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("(-2) / 3", ansi);
    }

    #[test]
    fn test_to_text_canvas_div_neg_positive() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::div(
            UnaryData::neg(fa.rzi_u(2)).to_rc_expr(),
            fa.rzi_u(3),
        ).to_expr();

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("(-2) / 3", ansi);
    }

}
