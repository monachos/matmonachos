use std::collections::HashMap;
use std::rc::Rc;
use matm_model::core::dimensions::Size;
use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use matm_model::expressions::matrices::MatrixData;
use matm_model::grid::FormatIndexType;
use matm_model::grid::Grid;
use crate::output::console::get_console_sign_string;
use crate::output::console::ConsoleSign;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;


impl TextCanvasRender for MatrixData {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        let content_canv = matrix_content_to_text_canvas(
            ctx,
            &self.grid,
            &self.augmented_columns,
            MatrixData::format_index)?;

        let content_size = content_canv.size().unwrap_or(Size::new(0, 0));
        let base_line = content_size.height / 2 + 1;

        let content_left_margin = 2;
        let content_right_margin = 2;

        // put canvases into proper places
        let mut canvas = TextCanvas::with_base_line(base_line);
        canvas.write_canvas(Index2D::new(1, 1 + content_left_margin)?, &content_canv)?;

        // write brackets
        for row_pos in 1..=content_size.height {
            let left_bracket_pos = Index2D::new(row_pos, 1)?;
            if content_size.height == 1 {
                canvas.write_row(left_bracket_pos, "[")?;
            } else if row_pos == 1 {
                canvas.write_row(left_bracket_pos,
                    &get_console_sign_string(ctx.charset, ConsoleSign::LeftSquareBracketUpper))?;
            } else if row_pos == content_size.height {
                canvas.write_row(left_bracket_pos,
                    &get_console_sign_string(ctx.charset, ConsoleSign::LeftSquareBracketLower))?;
            } else {
                canvas.write_row(left_bracket_pos,
                    &get_console_sign_string(ctx.charset, ConsoleSign::LeftSquareBracketMiddle))?;
            }

            // let right_bracket_pos = Index2D::new(row_pos, right_bracket_x)?;
            let right_bracket_pos = Index2D::new(row_pos, content_left_margin + content_size.width + content_right_margin)?;
            if content_size.height == 1 {
                canvas.write_row(right_bracket_pos, "]")?;
            } else if row_pos == 1 {
                canvas.write_row(right_bracket_pos,
                    &get_console_sign_string(ctx.charset, ConsoleSign::RightSquareBracketUpper))?;
            } else if row_pos == content_size.height {
                canvas.write_row(right_bracket_pos,
                    &get_console_sign_string(ctx.charset, ConsoleSign::RightSquareBracketLower))?;
            } else {
                canvas.write_row(right_bracket_pos,
                    &get_console_sign_string(ctx.charset, ConsoleSign::RightSquareBracketMiddle))?;
            }
        }

        return Ok(canvas);
    }
}

pub fn matrix_content_to_text_canvas<T>(
    ctx: &TextCanvasContext,
    grid: &Grid<Rc<T>>,
    augmented_columns: &Vec<usize>,
    idx_fmt: FormatIndexType,
) -> Result<TextCanvas, CalcError>
    where T : TextCanvasRender {
    // create grid with all created canvases
    let mut canvases_vector: Vec<TextCanvas> = Vec::new();
    for el in grid.data.iter() {
        canvases_vector.push(el.to_text_canvas(ctx)?);
    }
    let canvases = Grid::from_vector(
        vec![grid.size_of(MatrixData::ROW_INDEX)?, grid.size_of(MatrixData::COLUMN_INDEX)?],
        canvases_vector)?;

    // calculate width of columns (maximum width of all cells in each column)
    let mut max_column_widths: HashMap<usize, usize> = HashMap::new();
    for c in canvases.dimension_range(MatrixData::COLUMN_INDEX)? {
        let mut max_value = 0;
        for r in canvases.dimension_range(MatrixData::ROW_INDEX)? {
            match canvases.get_at(&vec![r, c], idx_fmt)?.size() {
                Some(size) => {
                    if size.width > max_value || 0 == max_value {
                        max_value = size.width;
                    }
                }
                None => {}
            }
        }
        max_column_widths.insert(c, max_value);
    }
    
    // calculate height of rows (maximum height of all cells in each row)
    let mut max_row_heights: HashMap<usize, usize> = HashMap::new();
    for r in canvases.dimension_range(MatrixData::ROW_INDEX)? {
        let mut max_value = 0;
        for c in canvases.dimension_range(MatrixData::COLUMN_INDEX)? {
            match canvases.get_at(&vec![r, c], idx_fmt)?.size() {
                Some(size) => {
                    if size.height > max_value || 0 == max_value {
                        max_value = size.height;
                    }
                }
                None => {}
            }
        }
        max_row_heights.insert(r, max_value);
    }

    let mut augmented_cols_pos: Vec<usize> = Vec::with_capacity(augmented_columns.len());

    let bracket_width = 1;
    let outer_margin_width = 1;
    let inner_margin_width = 2;
    let inner_margin_height = 1;

    let total_height: usize = max_row_heights.iter().map(|e| e.1).sum::<usize>()
        + inner_margin_height * (grid.size_of(MatrixData::ROW_INDEX)? - 1);
    let base_line = total_height / 2 + 1;

    // put canvases into proper places
    let mut canvas = TextCanvas::with_base_line(base_line);
    let mut row_pos = 1;
    let mut bottom_bracket_pos = 0;
    for r in grid.dimension_range(MatrixData::ROW_INDEX)? {
        if r > 1 {
            row_pos += inner_margin_height;
            bottom_bracket_pos += inner_margin_height;
        }
        let row_height = *max_row_heights.get(&r).unwrap();

        let mut col_pos = 1 + bracket_width + outer_margin_width;

        for c in grid.dimension_range(MatrixData::COLUMN_INDEX)? {
            if c > 1 {
                col_pos += inner_margin_width;
            }
            let column_width = *max_column_widths.get(&c).unwrap();
            let canvas_item = canvases.get_at(&vec![r, c], idx_fmt)?;
            match canvas_item.size() {
                Some(item_size) => {
                    // each item is centered
                    let mut item_row = row_pos;
                    if item_size.height < row_height {
                        let mut diff = row_height - item_size.height;
                        if diff % 2 != 0 {
                            diff += 1;
                        }
                        item_row += diff / 2;
                    }
                    let mut item_col = col_pos;
                    if item_size.width < column_width {
                        let mut diff = column_width - item_size.width;
                        if diff % 2 != 0 {
                            diff += 1;
                        }
                        item_col += diff / 2;
                    }
                    canvas.write_canvas(Index2D::new(item_row, item_col)?, canvas_item)?;
                }
                None => {}
            }

            col_pos += column_width;

            if augmented_columns.contains(&c) {
                col_pos += outer_margin_width;
                augmented_cols_pos.push(col_pos);
            }
        }

        row_pos += row_height;
        bottom_bracket_pos += row_height;
    }

    // write vertical bars for augmented matrices
    for aug_col_pos in &augmented_cols_pos {
        for row_pos in 1..=bottom_bracket_pos {
            let bar_pos = Index2D::new(row_pos, *aug_col_pos)?;
            canvas.write_row(bar_pos, "|")?;
        }
    }

    return Ok(canvas);
}
