use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use matm_model::expressions::equation::EquationData;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;


impl TextCanvasRender for EquationData {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        let canvases: Vec<TextCanvas> = vec![
            self.left.to_text_canvas(ctx)?,
            TextCanvas::from_row_text(" = "),
            self.right.to_text_canvas(ctx)?
        ];
        let canvas = TextCanvas::from_row_of_canvases(Index2D::one(), &canvases)?;
        return Ok(canvas);
    }
}
