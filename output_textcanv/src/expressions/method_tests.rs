#[cfg(test)]
mod tests {
    use matm_model::expressions::method::MethodData;
    use matm_model::expressions::ToExpression;
    use matm_model::expressions::binary::BinaryData;
    use matm_model::factories::ObjFactory;
    use crate::testing::println_text_variants;

    #[test]
    fn test_to_text_canvas_symbol_no_args() {
        let fa = ObjFactory::new10();
        let m = MethodData::new(fa.rsn("x"), "foo".to_owned(), Vec::new());

        let (ansi, _) = println_text_variants(&m);

        assert_eq!("x.foo()", ansi);
    }

    #[test]
    fn test_format_symbol_args() {
        let fa = ObjFactory::new10();
        let m = MethodData::new(fa.rsn("x"), "foo".to_owned(), vec![fa.rz0(), fa.rsn("a")]);

        let (ansi, _) = println_text_variants(&m);

        assert_eq!("x.foo(0, a)", ansi);
    }

    #[test]
    fn test_to_text_canvas_binary_no_args() {
        let fa = ObjFactory::new10();
        let m = MethodData::new(
            BinaryData::add(fa.rzi_u(2), fa.rzi_u(3)).to_rc_expr(),
            "foo".to_owned(),
            Vec::new());

        let (ansi, _) = println_text_variants(&m);

        assert_eq!("(2 + 3).foo()", ansi);
    }

    #[test]
    fn test_format_chain() {
        let fa = ObjFactory::new10();
        let f = MethodData::new(fa.rsn("x"), "first".to_owned(), vec![fa.rz0()]).to_rc_expr();
        let s = MethodData::new(f, "second".to_owned(), vec![fa.rsn("a")]);

        let (ansi, _) = println_text_variants(&s);

        assert_eq!("x.first(0).second(a)", ansi);
    }
}