use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use matm_model::expressions::symbol::SymbolData;
use crate::output::colors::TokenType;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;


impl TextCanvasRender for SymbolData {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        // Layout:
        // N'      - base line
        //  I1,I2
        let mut canvas = TextCanvas::with_base_line(1);
        let mut col = 1;

        canvas.write_row_color(
            Index2D::new(1, col)?,
            self.name.as_str(),
            TokenType::Symbol)?;
        col += self.name.len();

        if !self.indices.is_empty() {
            let mut index_canvases: Vec<TextCanvas> = Vec::with_capacity(self.indices.len() * 2 - 1);
            let mut add_coma = false;
            for i in &self.indices {
                if add_coma {
                    let mut coma_canvas = TextCanvas::new();
                    coma_canvas.write_row(Index2D::one(), ",")?;
                    index_canvases.push(coma_canvas);
                }
                index_canvases.push(i.to_text_canvas(ctx)?);
                add_coma = true;
            }
            let index_canvas = TextCanvas::from_row_of_canvases(Index2D::one(), &index_canvases)?;
            canvas.write_canvas(Index2D::new(2, col)?, &index_canvas)?;
        }

        if !self.postfix.is_empty() {
            canvas.write_row_color(
                Index2D::new(1, col)?,
                self.postfix.as_str(),
                TokenType::SymbolPostfix)?;
        }

        return Ok(canvas);
    }
}

