use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use crate::output::console::get_console_sign_string;
use crate::output::console::ConsoleCharSet;
use crate::output::console::ConsoleSign;
use crate::output::colors::TokenType;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;

pub fn boxsign_to_text_canvas(_ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
   let mut canvas = TextCanvas::new();
   let symbol = get_console_sign_string(ConsoleCharSet::Unicode, ConsoleSign::Square);//TODO create custom expression
   canvas.write_row_color(Index2D::one(), &symbol, TokenType::Symbol)?;
   return Ok(canvas);
}
