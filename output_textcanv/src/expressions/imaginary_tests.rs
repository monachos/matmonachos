#[cfg(test)]
mod tests {
    use matm_model::expressions::imaginary::ImData;
    use crate::testing::println_text_variants;


    #[test]
    fn test_to_text_canvas_name() {
        let expr = ImData::new("i");

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("%i", ansi);
    }
}
