#[cfg(test)]
mod tests {
    use matm_model::expressions::binomial::BinomialData;
    use matm_model::factories::ObjFactory;
    use matm_model::testing::asserts::assert_result_ok;

    use crate::output::text::TextCanvasContext;
    use crate::output::text::TextCanvasRender;
    use crate::testing::println_text_variants;

    #[test]
    fn test_to_text_canvas() {
        let fa = ObjFactory::new10();
        let o = BinomialData::new(
            fa.rzi_u(10),
            fa.rzi_u(3),
        );

        let canv_ctx = TextCanvasContext::unicode_color();
        let canvas = assert_result_ok(o.to_text_canvas(&canv_ctx));

        assert_eq!(2, canvas.base_line());
        assert_eq!(1, canvas.get_rows_over_base_line().unwrap());
        assert_eq!(1, canvas.get_rows_under_base_line().unwrap());

        let (ansi, uni) = println_text_variants(&o);

        assert_eq!(vec![
            r"/10\",
            r"|  |",
            r"\3 /"
            ].join("\n").to_string(), ansi);
        assert_eq!(vec![
            r"⎛10⎞",
            r"⎜  ⎟",
            r"⎝3 ⎠"
        ].join("\n").to_string(), uni);
    }
}