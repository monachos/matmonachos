use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use matm_model::expressions::bool::BoolData;
use crate::output::colors::TokenType;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;


impl TextCanvasRender for BoolData {
    fn to_text_canvas(&self, _ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        let mut canvas = TextCanvas::new();
        let sym = if self.value() {
            "true"
        } else {
            "false"
        };
        canvas.write_row_color(Index2D::one(), sym, TokenType::Number)?;
        return Ok(canvas);
    }
}


