use std::cmp::max;
use matm_model::core::dimensions::Size;
use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use matm_model::expressions::arithmetic_condition::ArithmeticCondition;
use matm_model::expressions::arithmetic_operator::ArithmeticOperator;
use matm_model::expressions::condition::Condition;
use matm_model::expressions::conditional::ConditionalData;
use matm_model::expressions::logic_condition::LogicCondition;
use matm_model::expressions::logic_operator::LogicOperator;
use crate::output::console::ConsoleSign;
use crate::output::console::get_console_sign_string;
use crate::output::colors::TokenType;
use crate::output::localisation::locale_keys;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;
use crate::output::text_canvas_utils::get_left_curly_bracket_text_canvas;

use super::unary::unary_group_to_text_canvas;
use super::unary::wrap_text_canvas_with_parenthesis;


impl TextCanvasRender for ArithmeticOperator {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        let symbol = match self {
            ArithmeticOperator::Equal => String::from("="),
            ArithmeticOperator::NotEqual => get_console_sign_string(ctx.charset, ConsoleSign::NotEqualTo),
            ArithmeticOperator::Greater => String::from(">"),
            ArithmeticOperator::GreaterOrEqual => get_console_sign_string(ctx.charset, ConsoleSign::GreaterThanOrEqualTo),
            ArithmeticOperator::Less => String::from("<"),
            ArithmeticOperator::LessOrEqual => get_console_sign_string(ctx.charset, ConsoleSign::LessThanOrEqualTo),
        };
        let canvas = TextCanvas::from_row_text_color(&symbol, TokenType::Operator);
        return Ok(canvas);
    }
}

//------------------------------------------------

impl TextCanvasRender for ArithmeticCondition {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        let left_canvas = if self.should_wrap_left_with_parentheses() {
            unary_group_to_text_canvas(ctx, &self.left)?
        } else {
            self.left.to_text_canvas(ctx)?
        };

        let right_canvas = if self.should_wrap_right_with_parentheses() {
            unary_group_to_text_canvas(ctx, &self.right)?
        } else {
            self.right.to_text_canvas(ctx)?
        };

        let canvases = vec![
            left_canvas,
            TextCanvas::hidden_space(),
            self.operator.to_text_canvas(ctx)?,
            TextCanvas::hidden_space(),
            right_canvas,
        ];
        let canvas = TextCanvas::from_row_of_canvases(
            Index2D::one(),
            &canvases)?;
        return Ok(canvas);
    }
}

//------------------------------------------------

impl TextCanvasRender for LogicOperator {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        let symbol = match self {
            LogicOperator::And => ctx.locale.get(locale_keys::AND),
            LogicOperator::Or => ctx.locale.get(locale_keys::OR),
        };
        let canvas = TextCanvas::from_row_text_color(symbol, TokenType::Operator);
        return Ok(canvas);
    }
}

//------------------------------------------------

impl TextCanvasRender for LogicCondition {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        let left_canvas = if self.should_wrap_left_with_parentheses() {
            wrap_text_canvas_with_parenthesis(ctx, self.left.to_text_canvas(ctx)?)?
        } else {
            self.left.to_text_canvas(ctx)?
        };

        let right_canvas = if self.should_wrap_right_with_parentheses() {
            wrap_text_canvas_with_parenthesis(ctx, self.right.to_text_canvas(ctx)?)?
        } else {
            self.right.to_text_canvas(ctx)?
        };

        let canvases = vec![
            left_canvas,
            TextCanvas::hidden_space(),
            self.operator.to_text_canvas(ctx)?,
            TextCanvas::hidden_space(),
            right_canvas,
        ];
        let canvas = TextCanvas::from_row_of_canvases(
            Index2D::one(),
            &canvases)?;
        return Ok(canvas);
    }
}

//------------------------------------------------

impl TextCanvasRender for Condition {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        return match self {
            Self::Bool(b) => b.to_text_canvas(ctx),
            Self::Arithmetic(arithmetic) => arithmetic.to_text_canvas(ctx),
            Self::Logic(logic) => logic.to_text_canvas(ctx),
        };
    }
}

//------------------------------------------------

struct ConditionalItemCanvas {
    expression: TextCanvas,
    condition: TextCanvas,
    expression_width: usize,
    height: usize,
}


impl TextCanvasRender for ConditionalData {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        let mut content_canv = TextCanvas::new();

        let mut items_canvases: Vec<ConditionalItemCanvas> = Vec::new();
        let mut max_expr_width = 0;
        for item in &self.clauses {
            let expr_canv = item.expression.to_text_canvas(ctx)?;
            let cond_canv = item.condition.to_text_canvas(ctx)?;
            let mut expr_width = 0;
            let mut height = 0;
            if let Some(s) = expr_canv.size() {
                expr_width = s.width;
                max_expr_width = max(max_expr_width, s.width);
                height = s.height;
            }
            if let Some(s) = cond_canv.size() {
                height = max(height, s.height);
            }
            items_canvases.push(
                ConditionalItemCanvas {
                    expression: expr_canv,
                    condition: cond_canv,
                    expression_width: expr_width,
                    height: height,
                });
        }

        let mut row = 1;
        let vert_space = 1;
        for item in &items_canvases {
            let mut space = TextCanvas::new();
            let spaces_len = 4 + max_expr_width - item.expression_width;
            space.write_row(Index2D::one(), " ".repeat(spaces_len).as_str())?;
            let canvases = vec![
                &item.expression,
                &space,
                &item.condition,
            ];
            content_canv.write_row_of_canvases_ref(Index2D::new(row, 1)?, &canvases)?;
            row += item.height + vert_space;
        }

        let content_size = content_canv.size().unwrap_or(Size::new(0, 0));
        let base_line = content_size.height / 2 + 1;

        let bracket_canv = get_left_curly_bracket_text_canvas(ctx, content_size.height)?;

        let mut canvas = TextCanvas::with_base_line(base_line);
        canvas.write_canvas(Index2D::new(1, 1)?, &bracket_canv)?;
        canvas.write_canvas(Index2D::new(1, 3)?, &content_canv)?;

        return Ok(canvas);
    }
}
