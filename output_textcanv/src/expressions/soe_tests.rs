#[cfg(test)]
mod tests {
    use matm_model::expressions::ToExpression;
    use matm_model::expressions::symbol::SymbolData;
    use matm_model::factories::ObjFactory;
    use matm_model::expressions::equation::EquationData;
    use matm_model::expressions::soe::SysOfEqData;
    use crate::testing::println_text_variants;

    #[test]
    fn test_to_text_canvas_one() {
        let fa = ObjFactory::new10();
        let eqs = SysOfEqData::from_vector(
            vec![
                EquationData::new(
                    SymbolData::new("x").to_rc_expr(),
                    fa.rzi_u(1)
                ).to_rc_expr(),
            ]);

        let (ansi, uni) = println_text_variants(&eqs);

        assert_eq!(vec![
            r"{ x = 1",
        ].join("\n").to_string(), ansi);
        assert_eq!(vec![
            r"{ x = 1",
        ].join("\n").to_string(), uni);
    }

    #[test]
    fn test_to_text_canvas_two() {
        let fa = ObjFactory::new10();
        let eqs = SysOfEqData::from_vector(
            vec![
                EquationData::new(
                    SymbolData::new("x").to_rc_expr(),
                    fa.rzi_u(1)
                ).to_rc_expr(),
                EquationData::new(
                    SymbolData::new("y").to_rc_expr(),
                    fa.rzi_u(2)
                ).to_rc_expr(),
            ]);

        let (ansi, uni) = println_text_variants(&eqs);

        assert_eq!(vec![
            r"/ x = 1",
            r"{      ",
            r"\ y = 2"
        ].join("\n").to_string(), ansi);
        assert_eq!(vec![
            r"⎧ x = 1",
            r"⎨      ",
            r"⎩ y = 2"
        ].join("\n").to_string(), uni);
    }

    #[test]
    fn test_to_text_canvas_three() {
        let fa = ObjFactory::new10();
        let eqs = SysOfEqData::from_vector(
            vec![
                EquationData::new(
                    SymbolData::new("x").to_rc_expr(),
                    fa.rzi_u(1)
                ).to_rc_expr(),
                EquationData::new(
                    SymbolData::new("y").to_rc_expr(),
                    fa.rzi_u(2)
                ).to_rc_expr(),
                EquationData::new(
                    SymbolData::new("z").to_rc_expr(),
                    fa.rzi_u(3)
                ).to_rc_expr(),
            ]);

        let (ansi, uni) = println_text_variants(&eqs);

        assert_eq!(vec![
            r"/ x = 1",
            r"|      ",
            r"{ y = 2",
            r"|      ",
            r"\ z = 3"
        ].join("\n").to_string(), ansi);
        assert_eq!(vec![
            r"⎧ x = 1",
            r"⎪      ",
            r"⎨ y = 2",
            r"⎪      ",
            r"⎩ z = 3"
        ].join("\n").to_string(), uni);
    }

}