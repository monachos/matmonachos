use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use matm_model::expressions::object::ObjectData;
use crate::output::colors::TokenType;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;


impl TextCanvasRender for ObjectData {
    fn to_text_canvas(&self, _ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        return TextCanvas::from_row_of_canvases_with_base_line(
            1,
            Index2D::one(),
            &vec![
                TextCanvas::from_row_text_color("{", TokenType::Parenthesis),
                TextCanvas::from_row_text_color(format!("{}", self.object_type).as_str(), TokenType::Symbol),
                TextCanvas::from_row_text_color("}", TokenType::Parenthesis),
            ]);
    }
}

