use std::cmp::max;

use crate::output::colors::TokenType;
use crate::output::console::get_console_sign_string;
use crate::output::console::ConsoleSign;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;
use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use matm_model::statements::tabular_multiplication::TabularMultiplicationData;
use crate::statements::tabular::write_positional_number;

impl TextCanvasRender for TabularMultiplicationData {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        let mut canvas = TextCanvas::new();

        let sign_length: usize = 1;
        let mut max_length = 0;
        max_length = max(max_length, self.multiplier.to_text().len());
        max_length = max(max_length, self.multiplicand.to_text().len());
        for prod in &self.partial_products {
            max_length = max(max_length, prod.to_string().len());
        }
        for carry in &self.multiplication_carries {
            max_length = max(max_length, carry.to_string().len());
        }
        max_length = max(max_length, self.addition_carry.to_string().len());
        max_length = max(max_length, self.product.to_string().len());
        max_length += sign_length + 1;
        let mut row: usize = 1;

        // multiplication carries
        for carry in self.multiplication_carries.iter().rev() {
            if carry.has_any_digit() {
                let carry_str = carry.to_string();
                canvas.write_row_color(
                    Index2D::new(row, max_length - carry_str.len())?,
                    &carry_str,
                    TokenType::ElementaryCarry)?;
                row += 1;
            }
        }

        // write multiplier
        let multiplier_str = self.multiplier.to_text();
        write_positional_number(
            &mut canvas,
            Index2D::new(row, max_length - multiplier_str.len())?,
            &self.multiplier,
            TokenType::Number)?;
        row += 1;

        // write multiplicand
        canvas.write_row_color(Index2D::new(row, 1)?,
                               &get_console_sign_string(ctx.charset, ConsoleSign::Multiplication),
                               TokenType::Operator)?;
        let multiplicand_str = self.multiplicand.to_text();
        write_positional_number(
            &mut canvas,
            Index2D::new(row, max_length - multiplicand_str.len())?,
            &self.multiplicand,
            TokenType::Number)?;
        row += 1;

        // write horizontal line
        canvas.write_row(
            Index2D::new(row, 1)?,
            get_console_sign_string(ctx.charset, ConsoleSign::HorizLine).repeat(max_length - 1).as_str())?;
        row += 1;

        // addition carry
        if !self.addition_carry.is_empty() {
            let carry_str = self.addition_carry.to_string();
            canvas.write_row_color(
                Index2D::new(row, max_length - carry_str.len())?,
                &carry_str,
                TokenType::ElementaryCarry)?;
            row += 1;
        }

        // write partial products
        for prod in &self.partial_products {
            let prod_str = prod.to_string();
            write_positional_number(
                &mut canvas,
                Index2D::new(row, max_length - prod_str.len())?,
                prod,
                TokenType::Number)?;
            row += 1;
        }

        // write horizontal line
        canvas.write_row(
            Index2D::new(row, 1)?,
            get_console_sign_string(ctx.charset, ConsoleSign::HorizLine).repeat(max_length - 1).as_str())?;
        row += 1;

        // write product
        let prod_str = self.product.to_string();
        write_positional_number(
            &mut canvas,
            Index2D::new(row, max_length - prod_str.len())?,
            &self.product,
            TokenType::Number)?;

        return Ok(canvas);
    }
}


#[cfg(test)]
mod tests {
    use matm_model::core::natural::Natural;
    use matm_model::core::natural::RADIX_10;
    use matm_model::core::positional::PositionalNumber;
    use matm_model::statements::tabular_multiplication::TabularMultiplicationData;
    use crate::testing::println_text_variants;

    #[test]
    fn test_natural_multiplication_step_to_text_canvas() {
        let data = TabularMultiplicationData {
            multiplier: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 10).unwrap()),
            multiplicand: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 5).unwrap()),
            partial_products: Vec::new(),
            multiplication_carries: Vec::new(),
            addition_carry: PositionalNumber::new(RADIX_10),
            product: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 50).unwrap()),
        };

        let (ansi, _) = println_text_variants(&data);

        assert_eq!(vec![
            " 10",
            "x 5",
            "---",
            "---",
            " 50"].join("\n").to_string(), ansi);
    }

    #[test]
    fn test_natural_multiplication_step_multidigits_to_text_canvas() {
        let mut prod1 = PositionalNumber::new(RADIX_10);
        prod1.set_digit(1, 2).unwrap();
        prod1.set_digit(0, 0).unwrap();
        let mut prod2 = PositionalNumber::new(RADIX_10);
        prod2.set_digit(2, 1).unwrap();
        prod2.set_digit(1, 0).unwrap();
        let data = TabularMultiplicationData {
            multiplier: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 10).unwrap()),
            multiplicand: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 12).unwrap()),
            partial_products: vec![prod1, prod2],
            multiplication_carries: Vec::new(),
            addition_carry: PositionalNumber::new(RADIX_10),
            product: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 120).unwrap()),
        };

        let (ansi, _) = println_text_variants(&data);

        assert_eq!(vec![
            "  10",
            "x 12",
            "----",
            "  20",
            " 10 ",
            "----",
            " 120"].join("\n").to_string(), ansi);
    }

    #[test]
    fn test_natural_multiplication_step_mul_carry_to_text_canvas() {
        let mut prod = PositionalNumber::new(RADIX_10);
        prod.set_digit(2, 1).unwrap();
        prod.set_digit(1, 4).unwrap();
        prod.set_digit(0, 4).unwrap();
        let mut carry = PositionalNumber::new(RADIX_10);
        carry.set_digit(2, 1).unwrap();
        carry.set_digit(1, 2).unwrap();
        let data = TabularMultiplicationData {
            multiplier: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 36).unwrap()),
            multiplicand: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 4).unwrap()),
            partial_products: vec![prod],
            multiplication_carries: vec![carry],
            addition_carry: PositionalNumber::new(RADIX_10),
            product: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 144).unwrap()),
        };

        let (ansi, _) = println_text_variants(&data);

        assert_eq!(vec![
            " 12 ",
            "  36",
            "x  4",
            "----",
            " 144",
            "----",
            " 144"].join("\n").to_string(), ansi);
    }

    #[test]
    fn test_natural_multiplication_step_add_carry_to_text_canvas() {
        let mut prod1 = PositionalNumber::new(RADIX_10);
        prod1.set_digit(2, 3).unwrap();
        prod1.set_digit(1, 6).unwrap();
        prod1.set_digit(0, 4).unwrap();
        let mut prod2 = PositionalNumber::new(RADIX_10);
        prod2.set_digit(3, 1).unwrap();
        prod2.set_digit(2, 0).unwrap();
        prod2.set_digit(1, 4).unwrap();
        let mut mul_carry1 = PositionalNumber::new(RADIX_10);
        mul_carry1.set_digit(2, 3).unwrap();
        mul_carry1.set_digit(1, 1).unwrap();
        let mut mul_carry2 = PositionalNumber::new(RADIX_10);
        mul_carry2.set_digit(2, 1).unwrap();
        let mut add_carry = PositionalNumber::new(RADIX_10);
        add_carry.set_digit(2, 1).unwrap();
        let data = TabularMultiplicationData {
            multiplier: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 52).unwrap()),
            multiplicand: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 27).unwrap()),
            partial_products: vec![prod1, prod2],
            multiplication_carries: vec![mul_carry1, mul_carry2],
            addition_carry: add_carry,
            product: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 1404).unwrap()),
        };

        let (ansi, _) = println_text_variants(&data);

        assert_eq!(vec![
            "  1  ",
            "  31 ",
            "   52",
            "x  27",
            "-----",
            "  1  ",
            "  364",
            " 104 ",
            "-----",
            " 1404"].join("\n").to_string(), ansi);
    }
}
