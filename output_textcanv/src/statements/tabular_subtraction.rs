use crate::output::colors::TokenType;
use crate::output::console::get_console_sign_string;
use crate::output::console::ConsoleSign;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;
use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use matm_model::statements::tabular_subtraction::TabularSubtractionData;
use crate::statements::tabular::write_positional_number;

impl TextCanvasRender for TabularSubtractionData {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        let mut canvas = TextCanvas::new();

        let max_length = self.minuend.digits_len() + 2;
        let mut row: usize = 1;

        // write borrowed
        if self.borrowed.has_any_digit() {
            let text = self.borrowed.to_text();
            write_positional_number(
                &mut canvas,
                Index2D::new(row, max_length - text.len())?,
                &self.borrowed,
                TokenType::ElementaryBorrow)?;
        }
        row += 1;

        // write minuend
        let minuend_str = self.minuend.to_text();
        write_positional_number(
            &mut canvas,
            Index2D::new(row, max_length - minuend_str.len())?,
            &self.minuend,
            TokenType::Number)?;

        row += 1;

        // write subtrahend
        canvas.write_row_color(Index2D::new(row, 1)?,
                               "-",
                               TokenType::Operator)?;
        let subtrahend_str = self.subtrahend.to_text();
        write_positional_number(
            &mut canvas,
            Index2D::new(row, max_length - subtrahend_str.len())?,
            &self.subtrahend,
            TokenType::Number)?;

        row += 1;

        // write horizontal line
        canvas.write_row(
            Index2D::new(row, 1)?,
            get_console_sign_string(ctx.charset, ConsoleSign::HorizLine).repeat(max_length - 1).as_str())?;
        row += 1;

        // write difference
        let diff_text = self.difference.to_text();
        write_positional_number(
            &mut canvas,
            Index2D::new(row, max_length - diff_text.len())?,
            &self.difference,
            TokenType::Number)?;

        return Ok(canvas);
    }
}

#[cfg(test)]
mod tests {
    use matm_model::core::natural::Natural;
    use matm_model::core::natural::RADIX_10;
    use matm_model::core::positional::PositionalNumber;
    use matm_model::statements::tabular_subtraction::TabularSubtractionData;
    use crate::testing::println_text_variants;

    #[test]
    fn test_natural_substraction_step_to_text_canvas() {
        let data = TabularSubtractionData {
            minuend: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 456).unwrap()),
            subtrahend: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 23).unwrap()),
            difference: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 433).unwrap()),
            borrowed: PositionalNumber::new(RADIX_10),
        };

        let (ansi, _) = println_text_variants(&data);

        assert_eq!(vec![
            " 456",
            "- 23",
            "----",
            " 433"].join("\n").to_string(), ansi);
    }

    #[test]
    fn test_natural_substraction_step_borrowed_to_text_canvas() {
        let mut borrowed = PositionalNumber::new(RADIX_10);
        borrowed.set_digit(2, 3).unwrap();
        let data = TabularSubtractionData {
            minuend: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 456).unwrap()),
            subtrahend: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 66).unwrap()),
            difference: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 390).unwrap()),
            borrowed: borrowed,
        };

        let (ansi, _) = println_text_variants(&data);

        assert_eq!(vec![
            " 3  ",
            " 456",
            "- 66",
            "----",
            " 390"].join("\n").to_string(), ansi);
    }
}
