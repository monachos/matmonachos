use crate::output::colors::TokenType;
use crate::output::console::get_console_sign_string;
use crate::output::console::ConsoleSign;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;
use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use matm_model::statements::tabular_division::TabularDivisionLine;
use matm_model::statements::tabular_division::TabularDivisionData;
use crate::statements::tabular::write_positional_number;

impl TextCanvasRender for TabularDivisionData {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        let mut canvas = TextCanvas::new();

        let mut max_length = self.dividend.to_string().len();
        max_length += 1;

        let mut row = 1;

        // write quotient
        let quotient_str = self.quotient.to_string();
        write_positional_number(
            &mut canvas,
            Index2D::new(row, max_length - quotient_str.len())?,
            &self.quotient,
            TokenType::Number)?;
        row += 1;

        // write line
        canvas.write_row(
            Index2D::new(row, 1)?,
            get_console_sign_string(ctx.charset, ConsoleSign::HorizLine).repeat(max_length - 1).as_str())?;
        row += 1;

        // write dividend and divisor
        let dividend_str = self.dividend.to_string();
        let mut col = max_length - dividend_str.len();
        write_positional_number(
            &mut canvas,
            Index2D::new(row, col)?,
            &self.dividend,
            TokenType::Number)?;
        col += dividend_str.as_str().len();
        canvas.write_row_color(
            Index2D::new(row, col)?,
            ":",
            TokenType::Operator)?;
        col += 1;
        write_positional_number(
            &mut canvas,
            Index2D::new(row, col)?,
            &self.divisor,
            TokenType::Number)?;
        row += 1;

        // write partial calculations and line
        for line in &self.lines {
            match line {
                TabularDivisionLine::Separator => {
                    canvas.write_row(
                        Index2D::new(row, 1)?,
                        get_console_sign_string(ctx.charset, ConsoleSign::HorizLine).repeat(max_length - 1).as_str())?;
                }
                TabularDivisionLine::Number(num) => {
                    let num_str = num.to_string();
                    let col = max_length - num_str.len();
                    write_positional_number(
                        &mut canvas,
                        Index2D::new(row, col)?,
                        num,
                        TokenType::Number)?;
                }
            }
            row += 1;
        }

        return Ok(canvas);
    }
}


#[cfg(test)]
mod tests {
    use matm_model::core::natural::Natural;
    use matm_model::core::natural::RADIX_10;
    use matm_model::core::positional::PositionalNumber;
    use matm_model::statements::tabular_division::{TabularDivisionLine, TabularDivisionData};
    use crate::testing::println_text_variants;

    #[test]
    fn test_natural_division_step_to_text_canvas() {
        let mut quotient = PositionalNumber::new(RADIX_10);
        quotient.set_digit(1, 1).unwrap();
        quotient.set_digit(0, 2).unwrap();
        let mut line1 = PositionalNumber::new(RADIX_10);
        line1.set_digit(2, 1).unwrap();
        line1.set_digit(1, 2).unwrap();
        let mut line2 = PositionalNumber::new(RADIX_10);
        line2.set_digit(1, 0).unwrap();
        line2.set_digit(0, 0).unwrap();
        let mut line3 = PositionalNumber::new(RADIX_10);
        line3.set_digit(0, 0).unwrap();
        let mut line4 = PositionalNumber::new(RADIX_10);
        line4.set_digit(0, 0).unwrap();
        let data = TabularDivisionData {
            dividend: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 120).unwrap()),
            divisor: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 10).unwrap()),
            lines: vec![
                TabularDivisionLine::Number(line1),
                TabularDivisionLine::Separator,
                TabularDivisionLine::Number(line2),
                TabularDivisionLine::Number(line3),
                TabularDivisionLine::Separator,
                TabularDivisionLine::Number(line4),
            ],
            quotient: quotient,
        };

        let (ansi, _) = println_text_variants(&data);

        assert_eq!(vec![
            " 12   ",
            "---   ",
            "120:10",
            "12    ",
            "---   ",
            " 00   ",
            "  0   ",
            "---   ",
            "  0   "
        ].join("\n").to_string(), ansi);
    }
}
