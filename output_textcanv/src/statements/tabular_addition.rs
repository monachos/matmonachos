use std::cmp::max;

use crate::output::colors::TokenType;
use crate::output::console::get_console_sign_string;
use crate::output::console::ConsoleSign;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;
use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use matm_model::statements::tabular_addition::TabularAdditionData;
use crate::statements::tabular::write_positional_number;

impl TextCanvasRender for TabularAdditionData {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        let mut canvas = TextCanvas::new();

        let sign_length: usize = 1;
        let mut max_length = 0;
        max_length = max(max_length, self.sum.to_string().len());
        max_length = max(max_length, self.carry.to_string().len());
        for v in &self.addends {
            max_length = max(max_length, v.to_string().len());
        }
        max_length += sign_length + 1;
        let mut row: usize = 1;

        // write carry
        if self.carry.has_any_digit() {
            let carry_text = self.carry.to_string();
            write_positional_number(
                &mut canvas,
                Index2D::new(row, max_length - carry_text.len())?,
                &self.carry,
                TokenType::ElementaryCarry)?;
        }
        row += 1;

        // write input addends
        for addend in &self.addends {
            if row > 2 {
                canvas.write_row_color(Index2D::new(row, 1)?,
                                       "+",
                                       TokenType::Operator)?;
            }
            let addend_str = addend.to_text();
            write_positional_number(
                &mut canvas,
                Index2D::new(row, max_length - addend_str.len())?,
                addend,
                TokenType::Number)?;

            row += 1;
        }

        // write horizontal line
        canvas.write_row(
            Index2D::new(row, 1)?,
            get_console_sign_string(ctx.charset, ConsoleSign::HorizLine).repeat(max_length - 1).as_str())?;
        row += 1;

        // write sum
        let sum_text = &self.sum.to_text();
        write_positional_number(
            &mut canvas,
            Index2D::new(row, max_length - sum_text.len())?,
            &self.sum,
            TokenType::Number)?;

        return Ok(canvas);
    }
}

#[cfg(test)]
mod tests {
    use matm_model::core::natural::Natural;
    use matm_model::core::natural::RADIX_10;
    use matm_model::core::positional::PositionalNumber;
    use matm_model::statements::tabular_addition::TabularAdditionData;
    use crate::testing::println_text_variants;

    #[test]
    fn test_natural_addition_step_to_text_canvas() {
        let data = TabularAdditionData {
            addends: vec![
                PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 100).unwrap()),
                PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 23).unwrap()),
            ],
            sum: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 123).unwrap()),
            carry: PositionalNumber::new(RADIX_10),
        };

        let (ansi, _) = println_text_variants(&data);

        assert_eq!(vec![
            " 100",
            "+ 23",
            "----",
            " 123"].join("\n").to_string(), ansi);
    }

    #[test]
    fn test_natural_addition_step_carry_to_text_canvas() {
        let mut carry = PositionalNumber::new(RADIX_10);
        carry.set_digit(3, 1).unwrap();
        let data = TabularAdditionData {
            addends: vec![
                PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 200).unwrap()),
                PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 823).unwrap()),
            ],
            sum: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 1023).unwrap()),
            carry: carry,
        };

        let (ansi, _) = println_text_variants(&data);

        assert_eq!(vec![
            " 1   ",
            "  200",
            "+ 823",
            "-----",
            " 1023"].join("\n").to_string(), ansi);
    }
}
