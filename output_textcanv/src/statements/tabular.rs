use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use matm_model::core::positional::{PositionalItem, PositionalMark, PositionalNumber};
use crate::output::colors::TokenType;
use crate::output::text::TextCanvas;

pub(crate) fn write_positional_number(
    canvas: &mut TextCanvas,
    start: Index2D,
    number: &PositionalNumber,
    default_token: TokenType,
) -> Result<(), CalcError> {
    let mut row = canvas.row(start);
    for digit in number.digits.iter().rev() {
        match digit {
            PositionalItem::Digit(d) => {
                let token = match d.mark() {
                    PositionalMark::Default => default_token,
                    PositionalMark::Source => TokenType::SourceNumber,
                    PositionalMark::Target => TokenType::TargetNumber,
                };
                let text = digit.to_text(number.radix);
                row.token(&text, token)?;
            }
            PositionalItem::Unknown => {
                let text = digit.to_text(number.radix);
                row.default(&text)?;
            }
            PositionalItem::None => {}
        }
    }

    Ok(())
}
