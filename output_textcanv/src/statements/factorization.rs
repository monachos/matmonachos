use std::cmp::max;
use matm_model::core::dimensions::Size;
use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use matm_model::statements::factorization::TrialDivisionStep;
use crate::output::colors::TokenType;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;


impl TextCanvasRender for TrialDivisionStep {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        let mut input_canvases: Vec<(TextCanvas, Option<TextCanvas>)> = Vec::new();
        let mut max_dividends_width = 0;
        for item in &self.items {
            let dividend_canv = item.dividend.to_text_canvas(ctx)?;
            if let Some(s) = dividend_canv.size() {
                max_dividends_width = max(max_dividends_width, s.width);
            }

            let divisor_canv = match &item.divisor {
                Some(divisor) => Some(divisor.to_text_canvas(ctx)?),
                None => None,
            };
            input_canvases.push((dividend_canv, divisor_canv));
        }
        
        let mut canvas = TextCanvas::with_base_line(1);

        let mut row_no = 1;
        for item in &input_canvases {
            let dividend_canv = &item.0;
            let dividend_size = dividend_canv.size().unwrap_or(Size::new(0, 0));
            let mut row = canvas.row(Index2D::new(row_no, 1 + max_dividends_width - dividend_size.width)?);

            row.canvas(&dividend_canv)?
                .token("|", TokenType::Default)?;

            if let Some(divisor_canv) = &item.1 {
                row.canvas(&divisor_canv)?;
            }

            row_no += 1;
        }

        return Ok(canvas);
    }
}
