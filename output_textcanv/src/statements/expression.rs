use matm_error::error::CalcError;
use matm_model::statements::expression::ExpressionStep;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;


impl TextCanvasRender for ExpressionStep {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        return self.expression.to_text_canvas(ctx);
    }
}
