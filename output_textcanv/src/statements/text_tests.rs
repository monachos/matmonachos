#[cfg(test)]
mod tests {
    use matm_model::statements::text::TextStatement;
    use crate::testing::println_text_variants;

    #[test]
    fn test_to_text_canvas_section_short() {
        let expr = TextStatement::section("a");
        
        let (ansi, _) = println_text_variants(&expr);

        assert_eq!(vec![
            "-------",
            "a      ",
            ].join("\n").to_string(), ansi);
    }

    #[test]
    fn test_to_text_canvas_section_long() {
        let expr = TextStatement::section("This is long section");
        
        let (ansi, _) = println_text_variants(&expr);

        assert_eq!(vec![
            "--------------------------",
            "This is long section      ",
            ].join("\n").to_string(), ansi);
    }

    #[test]
    fn test_to_text_canvas_paragraph() {
        let expr = TextStatement::paragraph("a");

        let (ansi, _) = println_text_variants(&expr);

        assert_eq!("a", ansi);
    }
}