use matm_model::statements::text::TextCategory;
use matm_model::statements::text::TextStatement;
use matm_error::error::CalcError;
use matm_model::core::index::Index2D;
use matm_model::statements::text::TextStatementItem;
use crate::output::colors::TokenType;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;


impl TextCanvasRender for TextStatement {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        let mut canvas = TextCanvas::new();

        match self.category {
            TextCategory::Section => {
                let separator = "-".repeat(self.text.len() + 6);
                
                let mut row: usize = 1;
                canvas.write_row_color(Index2D::new(row, 1)?, &separator, TokenType::Section)?;
                row += 1;

                let items_canvases = get_text_statement_canvases(ctx, self, TokenType::Section)?;
                let content_canvas = TextCanvas::from_row_of_canvases(Index2D::one(), &items_canvases)?;
                canvas.write_canvas(Index2D::new(row, 1)?, &content_canvas)?;
            }
            TextCategory::Paragraph => {
                let items_canvases = get_text_statement_canvases(ctx, self, TokenType::Paragraph)?;
                let content_canvas = TextCanvas::from_row_of_canvases(Index2D::one(), &items_canvases)?;
                canvas.write_canvas(Index2D::one(), &content_canvas)?;
            }
        }

        return Ok(canvas);
    }
}

fn get_text_statement_canvases(
    ctx: &TextCanvasContext,
    statement: &TextStatement,
    text_token: TokenType,
) -> Result<Vec<TextCanvas>, CalcError> {
    let mut items_canvases: Vec<TextCanvas> = Vec::new();
    for item in statement.get_items() {
        match item {
            TextStatementItem::Text(t) => {
                let mut item_canv = TextCanvas::new();
                item_canv.write_row_color(Index2D::one(), &t, text_token)?;
                items_canvases.push(item_canv);
            }
            TextStatementItem::Statement(st) => {
                let item_canv = st.to_text_canvas(ctx)?;
                items_canvases.push(item_canv);
            }
        }
    }
    return Ok(items_canvases);
}