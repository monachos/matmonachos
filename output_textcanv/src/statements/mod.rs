pub mod expression;
pub mod factorization;
mod factorization_tests;
pub mod text;
mod text_tests;
mod tabular_addition;
mod tabular_subtraction;
mod tabular_multiplication;
mod tabular_division;
mod tabular;

use matm_error::error::CalcError;
use matm_model::statements::Statement;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;


impl TextCanvasRender for Statement {
    fn to_text_canvas(&self, ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        return match self {
            Statement::ExprStep(obj) => obj.to_text_canvas(ctx),
            Statement::Text(obj) => obj.to_text_canvas(ctx),
            Statement::TabularAdd(obj) => obj.to_text_canvas(ctx),
            Statement::TabularSub(obj) => obj.to_text_canvas(ctx),
            Statement::TabularMul(obj) => obj.to_text_canvas(ctx),
            Statement::TabularDiv(obj) => obj.to_text_canvas(ctx),
            Statement::TrialDivision(obj) => obj.to_text_canvas(ctx),
        };
    }
}

