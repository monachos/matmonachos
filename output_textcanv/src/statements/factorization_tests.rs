#[cfg(test)]
mod tests {
    use matm_model::factories::ObjFactory;
    use matm_model::statements::factorization::TrialDivisionFactor;
    use matm_model::statements::factorization::TrialDivisionStep;
    use crate::testing::println_text_variants;

    #[test]
    fn test_trial_division_to_text_canvas_one_digit() {
        let fa = ObjFactory::new10();
        let mut data = TrialDivisionStep::new();
        data.items.push(TrialDivisionFactor {
            dividend: fa.zi_u(5),
            divisor: Some(fa.zi_u(5)),
        });
        data.items.push(TrialDivisionFactor {
            dividend: fa.zi_u(1),
            divisor: None,
        });

        let (ansi, _) = println_text_variants(&data);

        assert_eq!(vec![
            "5|5",
            "1| ",
            ].join("\n").to_string(), ansi);
    }

    #[test]
    fn test_trial_division_to_text_canvas_two_digits() {
        let fa = ObjFactory::new10();
        let mut data = TrialDivisionStep::new();
        data.items.push(TrialDivisionFactor {
            dividend: fa.zi_u(12),
            divisor: Some(fa.zi_u(2)),
        });
        data.items.push(TrialDivisionFactor {
            dividend: fa.zi_u(6),
            divisor: Some(fa.zi_u(2)),
        });
        data.items.push(TrialDivisionFactor {
            dividend: fa.zi_u(3),
            divisor: Some(fa.zi_u(3)),
        });
        data.items.push(TrialDivisionFactor {
            dividend: fa.zi_u(1),
            divisor: None,
        });
        
        let (ansi, _) = println_text_variants(&data);

        assert_eq!(vec![
            "12|2",
            " 6|2",
            " 3|3",
            " 1| ",
            ].join("\n").to_string(), ansi);
    }

    #[test]
    fn test_trial_division_to_text_canvas_negative_dividend() {
        let fa = ObjFactory::new10();
        let mut data = TrialDivisionStep::new();
        data.items.push(TrialDivisionFactor {
            dividend: fa.zi_u(-12),
            divisor: Some(fa.zi_u(2)),
        });
        data.items.push(TrialDivisionFactor {
            dividend: fa.zi_u(-6),
            divisor: Some(fa.zi_u(2)),
        });
        data.items.push(TrialDivisionFactor {
            dividend: fa.zi_u(-3),
            divisor: Some(fa.zi_u(3)),
        });
        data.items.push(TrialDivisionFactor {
            dividend: fa.zi_u(-1),
            divisor: None,
        });
        
        let (ansi, _) = println_text_variants(&data);

        assert_eq!(vec![
            "-12|2",
            " -6|2",
            " -3|3",
            " -1| ",
            ].join("\n").to_string(), ansi);
    }
}