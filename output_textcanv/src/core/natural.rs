use matm_error::error::CalcError;
use matm_model::core::digit::digit_to_string;
use matm_model::core::index::Index2D;
use matm_model::core::natural::Natural;
use matm_model::core::natural::RADIX_10;
use crate::output::colors::TokenType;
use crate::output::text::TextCanvas;
use crate::output::text::TextCanvasContext;
use crate::output::text::TextCanvasRender;


impl TextCanvasRender for Natural {
    fn to_text_canvas(&self, _ctx: &TextCanvasContext) -> Result<TextCanvas, CalcError> {
        let mut canvas = TextCanvas::new();

        let mut row = canvas.row(Index2D::one());

        if self.radix() != RADIX_10 {
            row.token(format!("{}", self.radix()).as_str(), TokenType::RadixValue)?;
            row.token("#", TokenType::RadixSeparator)?;
        }

        let digits_text = self.iter()
            .map(|d| digit_to_string(self.radix(), *d))
            .collect::<Vec<_>>()
            .join("");
        row.token(digits_text.as_str(), TokenType::Number)?;

        if self.radix() != RADIX_10 {
            row.token("#", TokenType::RadixSeparator)?;
        }

        return Ok(canvas);
    }
}
