use std::fmt::Display;
use std::fmt::Formatter;
use std::rc::Rc;
use crate::MTag;
use crate::ToMTag;

pub struct MSubData {
    pub base: Rc<MTag>,
    pub subscript: Rc<MTag>,
}

impl MSubData {
    pub fn new(base: Rc<MTag>, subscript: Rc<MTag>) -> Self {
        Self {
            base,
            subscript,
        }
    }
}

impl ToMTag for MSubData {
    fn to_tag(self) -> Rc<MTag> {
        Rc::new(MTag::MSub(Rc::new(self)))
    }
}

impl Display for MSubData {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "<msub>")?;
        write!(f, "{}", self.base)?;
        write!(f, "{}", self.subscript)?;
        write!(f, "</msub>")?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::msub::MSubData;
    use crate::mi::MIData;
    use crate::mn::MNData;
    use crate::MValue;
    use crate::ToMTag;

    #[test]
    fn test_display() {
        let data = MSubData::new(
            MIData::new(MValue::Text("log".to_owned())).to_tag(),
            MNData::new(MValue::Text("2".to_owned())).to_tag(),
        ).to_tag();
        let result = format!("{}", data);
        let expected = vec![
            "<msub>",
            "<mi>log</mi>",
            "<mn>2</mn>",
            "</msub>",
        ].join("");
        assert_eq!(expected, result);
    }
}