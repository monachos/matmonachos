use std::fmt::Display;
use std::fmt::Formatter;
use std::rc::Rc;
use crate::MTag;
use crate::mtr::MTrData;
use crate::ToMTag;

pub struct MTableData {
    pub rows: Vec<Rc<MTrData>>,
}

impl MTableData {
    pub fn new() -> Self {
        Self {
            rows: Vec::new(),
        }
    }

    pub fn from_vector(children: Vec<Rc<MTrData>>) -> Self {
        Self {
            rows: children,
        }
    }
}

impl ToMTag for MTableData {
    fn to_tag(self) -> Rc<MTag> {
        Rc::new(MTag::MTable(Rc::new(self)))
    }
}

impl Display for MTableData {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "<mtable>")?;
        for c in &self.rows {
            write!(f, "{}", c)?;
        }
        write!(f, "</mtable>")?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::mtable::MTableData;
    use crate::ToMTag;

    #[test]
    fn test_display() {
        let data = MTableData::new().to_tag();
        let result = format!("{}", data);
        assert_eq!("<mtable></mtable>", result);
    }
}