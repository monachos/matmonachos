use std::fmt::Display;
use std::fmt::Formatter;
use std::rc::Rc;
use crate::MTag;
use crate::ToMTag;

pub struct MUnderOverData {
    pub base: Rc<MTag>,
    pub underscript: Rc<MTag>,
    pub overscript: Rc<MTag>,
}

impl MUnderOverData {
    pub fn new(base: Rc<MTag>, underscript: Rc<MTag>, overscript: Rc<MTag>) -> Self {
        Self {
            base,
            underscript,
            overscript,
        }
    }
}

impl ToMTag for MUnderOverData {
    fn to_tag(self) -> Rc<MTag> {
        Rc::new(MTag::MUnderOver(Rc::new(self)))
    }
}

impl Display for MUnderOverData {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "<munderover>")?;
        write!(f, "{}", self.base)?;
        write!(f, "{}", self.underscript)?;
        write!(f, "{}", self.overscript)?;
        write!(f, "</munderover>")?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::mi::MIData;
    use crate::munderover::MUnderOverData;
    use crate::mn::MNData;
    use crate::MValue;
    use crate::ToMTag;

    #[test]
    fn test_display() {
        let data = MUnderOverData::new(
            MIData::new(MValue::Text("x".to_owned())).to_tag(),
            MNData::new(MValue::Text("2".to_owned())).to_tag(),
            MNData::new(MValue::Text("3".to_owned())).to_tag(),
        ).to_tag();
        let result = format!("{}", data);
        let expected = vec![
            "<munderover>",
            "<mi>x</mi>",
            "<mn>2</mn>",
            "<mn>3</mn>",
            "</munderover>",
        ].join("");
        assert_eq!(expected, result);
    }
}