use std::fmt::Display;
use std::fmt::Formatter;
use std::rc::Rc;
use crate::MTag;
use crate::MValue;
use crate::ToMTag;

pub struct MNData {
    pub value: MValue,
}

impl MNData {
    pub fn new(value: MValue) -> Self {
        Self {
            value
        }
    }

    pub fn from_string(value: &str) -> Self {
        Self {
            value: MValue::Text(String::from(value)),
        }
    }

    pub fn from_i32(value: i32) -> Self {
        Self {
            value: MValue::Text(format!("{}", value))
        }
    }
}

impl ToMTag for MNData {
    fn to_tag(self) -> Rc<MTag> {
        Rc::new(MTag::MN(Rc::new(self)))
    }
}

impl Display for MNData {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "<mn>")?;
        write!(f, "{}", self.value)?;
        write!(f, "</mn>")?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::mn::MNData;
    use crate::MValue;
    use crate::ToMTag;

    #[test]
    fn test_display_new() {
        let data = MNData::new(MValue::Text("123".to_owned())).to_tag();
        let result = format!("{}", data);
        let expected = vec![
            "<mn>",
            "123",
            "</mn>",
        ].join("");
        assert_eq!(expected, result);
    }

    #[test]
    fn test_display_i32() {
        let data = MNData::from_i32(123).to_tag();
        let result = format!("{}", data);
        let expected = vec![
            "<mn>",
            "123",
            "</mn>",
        ].join("");
        assert_eq!(expected, result);
    }
}