pub mod form;
pub mod mfrac;
pub mod mi;
pub mod mn;
pub mod mo;
pub mod mroot;
pub mod mrow;
pub mod mspace;
pub mod msqrt;
pub mod mstyle;
pub mod msub;
pub mod msup;
pub mod mtable;
pub mod mtd;
pub mod mtext;
pub mod mtr;
pub mod munderover;

use crate::mfrac::MFracData;
use crate::mi::MIData;
use crate::mn::MNData;
use crate::mo::MOData;
use crate::mroot::MRootData;
use crate::mrow::MRowData;
use crate::msqrt::MSqrtData;
use crate::mstyle::MStyleData;
use crate::msub::MSubData;
use crate::msup::MSupData;
use crate::mtable::MTableData;
use crate::mtext::MTextData;
use crate::munderover::MUnderOverData;
use std::fmt::Display;
use std::fmt::Formatter;
use std::rc::Rc;
use crate::mspace::MSpaceData;

pub enum MTag {
    MFrac(Rc<MFracData>),
    MI(Rc<MIData>),
    MN(Rc<MNData>),
    MO(Rc<MOData>),
    MRoot(Rc<MRootData>),
    MRow(Rc<MRowData>),
    MSpace(Rc<MSpaceData>),
    MSqrt(Rc<MSqrtData>),
    MStyle(Rc<MStyleData>),
    MSub(Rc<MSubData>),
    MSup(Rc<MSupData>),
    MTable(Rc<MTableData>),
    MText(Rc<MTextData>),
    MUnderOver(Rc<MUnderOverData>),
}

impl Display for MTag {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            MTag::MFrac(data) => write!(f, "{}", data),
            MTag::MI(data) => write!(f, "{}", data),
            MTag::MN(data) => write!(f, "{}", data),
            MTag::MO(data) => write!(f, "{}", data),
            MTag::MRoot(data) => write!(f, "{}", data),
            MTag::MRow(data) => write!(f, "{}", data),
            MTag::MSpace(data) => write!(f, "{}", data),
            MTag::MSqrt(data) => write!(f, "{}", data),
            MTag::MStyle(data) => write!(f, "{}", data),
            MTag::MSub(data) => write!(f, "{}", data),
            MTag::MSup(data) => write!(f, "{}", data),
            MTag::MTable(data) => write!(f, "{}", data),
            MTag::MText(data) => write!(f, "{}", data),
            MTag::MUnderOver(data) => write!(f, "{}", data),
        }
    }
}

pub trait ToMTag {
    fn to_tag(self) -> Rc<MTag>;
}

pub enum MValue {
    Text(String),
    Raw(String),
}

impl Display for MValue {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            MValue::Text(s) => {
                let escaped = s.replace("&", "&amp;")
                    .replace("<", "&lt;")
                    .replace(">", "&gt;")
                    .replace("\"", "&quot;");
                write!(f, "{}", escaped)
            }
            MValue::Raw(s) => write!(f, "{}", s),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::MValue;

    #[test]
    fn test_display_mvalue_text() {
        let data = MValue::Text("abc & x < 1 > 3 \"z\"".to_owned());
        let result = format!("{}", data);
        assert_eq!("abc &amp; x &lt; 1 &gt; 3 &quot;z&quot;", result);
    }

    #[test]
    fn test_display_mvalue_raw() {
        let data = MValue::Raw("&pi;".to_owned());
        let result = format!("{}", data);
        assert_eq!("&pi;", result);
    }
}