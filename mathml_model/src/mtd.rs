use std::fmt::Display;
use std::fmt::Formatter;
use std::rc::Rc;
use crate::MTag;
use crate::MValue;

pub struct MTdData {
    pub content: Option<Rc<MTag>>,
    pub class: Option<MValue>,
}

impl MTdData {
    pub fn empty() -> Self {
        Self {
            content: None,
            class: None,
        }
    }

    pub fn new(content: Rc<MTag>) -> Self {
        Self {
            content: Some(content),
            class: None,
        }
    }

    pub fn with_class(content: Rc<MTag>, class: MValue) -> Self {
        Self {
            content: Some(content),
            class: Some(class),
        }
    }

    pub fn to_rc(self) -> Rc<Self> {
        Rc::new(self)
    }
}

impl Display for MTdData {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "<mtd")?;
        if let Some(the_class) = &self.class {
            write!(f, " class=\"{}\"", the_class)?;
        }
        write!(f, ">")?;
        if let Some(the_content) = &self.content {
            write!(f, "{}", the_content)?;
        }
        write!(f, "</mtd>")?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::mn::MNData;
    use crate::mtd::MTdData;
    use crate::MValue;
    use crate::ToMTag;

    #[test]
    fn test_display_empty() {
        let data = MTdData::empty();
        let result = format!("{}", data);
        assert_eq!("<mtd></mtd>", result);
    }

    #[test]
    fn test_display_content() {
        let data = MTdData::new(MNData::new(MValue::Text("123".to_owned())).to_tag());
        let result = format!("{}", data);
        let expected = vec![
            "<mtd>",
            "<mn>123</mn>",
            "</mtd>",
        ].join("");
        assert_eq!(expected, result);
    }

    #[test]
    fn test_display_content_with_class() {
        let data = MTdData::with_class(
            MNData::new(MValue::Text("123".to_owned())).to_tag(),
            MValue::Text("sep".to_owned())
        );
        let result = format!("{}", data);
        let expected = vec![
            "<mtd class=\"sep\">",
            "<mn>123</mn>",
            "</mtd>",
        ].join("");
        assert_eq!(expected, result);
    }
}