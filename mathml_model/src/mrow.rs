use std::fmt::Display;
use std::fmt::Formatter;
use std::rc::Rc;
use crate::MTag;
use crate::ToMTag;

pub struct MRowData {
    pub children: Vec<Rc<MTag>>,
}

impl MRowData {
    pub fn new() -> Self {
        Self {
            children: Vec::new(),
        }
    }

    pub fn from_vector(children: Vec<Rc<MTag>>) -> Self {
        Self {
            children,
        }
    }
}

impl ToMTag for MRowData {
    fn to_tag(self) -> Rc<MTag> {
        Rc::new(MTag::MRow(Rc::new(self)))
    }
}

impl Display for MRowData {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "<mrow>")?;
        for c in &self.children {
            write!(f, "{}", c)?;
        }
        write!(f, "</mrow>")?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::mrow::MRowData;
    use crate::ToMTag;

    #[test]
    fn test_display() {
        let data = MRowData::new().to_tag();
        let result = format!("{}", data);
        assert_eq!("<mrow></mrow>", result);
    }
}