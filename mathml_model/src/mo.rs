use std::fmt::Display;
use std::fmt::Formatter;
use std::rc::Rc;
use crate::form::MForm;
use crate::MTag;
use crate::MValue;
use crate::ToMTag;

pub struct MOData {
    pub value: MValue,
    pub form: Option<MForm>,
}

impl MOData {
    pub fn new(value: MValue) -> Self {
        Self {
            value,
            form: None,
        }
    }

    pub fn from_string(value: &str) -> Self {
        Self {
            value: MValue::Text(String::from(value)),
            form: None,
        }
    }
}

impl ToMTag for MOData {
    fn to_tag(self) -> Rc<MTag> {
        Rc::new(MTag::MO(Rc::new(self)))
    }
}

impl Display for MOData {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "<mo")?;
        if let Some(the_form) = &self.form {
            write!(f, " form=\"{}\"", the_form)?;
        }
        write!(f, ">")?;
        write!(f, "{}", self.value)?;
        write!(f, "</mo>")?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::form::MForm;
    use crate::mo::MOData;
    use crate::MValue;
    use crate::ToMTag;

    #[test]
    fn test_display() {
        let data = MOData::new(MValue::Text("+".to_owned())).to_tag();
        let result = format!("{}", data);
        let expected = vec![
            "<mo>",
            "+",
            "</mo>",
        ].join("");
        assert_eq!(expected, result);
    }

    #[test]
    fn test_display_with_form() {
        let mut data = MOData::new(MValue::Text("+".to_owned()));
        data.form = Some(MForm::Prefix);
        let result = format!("{}", data.to_tag());
        let expected = vec![
            "<mo form=\"prefix\">",
            "+",
            "</mo>",
        ].join("");
        assert_eq!(expected, result);
    }
}