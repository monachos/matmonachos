use std::fmt::Display;
use std::fmt::Formatter;
use std::rc::Rc;
use crate::MTag;
use crate::ToMTag;

pub struct MFracData {
    pub numerator: Rc<MTag>,
    pub denominator: Rc<MTag>,
    pub line_thickness: Option<i32>,
}

impl MFracData {
    pub fn new(numerator: Rc<MTag>, denominator: Rc<MTag>) -> Self {
        Self {
            numerator,
            denominator,
            line_thickness: None,
        }
    }
}

impl ToMTag for MFracData {
    fn to_tag(self) -> Rc<MTag> {
        Rc::new(MTag::MFrac(Rc::new(self)))
    }
}

impl Display for MFracData {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "<mfrac")?;
        if let Some(the_line_thickness) = self.line_thickness {
            write!(f, " linethickness=\"{}\"", the_line_thickness)?;
        }
        write!(f, ">")?;
        write!(f, "{}", self.numerator)?;
        write!(f, "{}", self.denominator)?;
        write!(f, "</mfrac>")?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::mfrac::MFracData;
    use crate::mn::MNData;
    use crate::MValue;
    use crate::ToMTag;

    #[test]
    fn test_display() {
        let data = MFracData::new(
            MNData::new(MValue::Text("2".to_owned())).to_tag(),
            MNData::new(MValue::Text("3".to_owned())).to_tag(),
        ).to_tag();
        let result = format!("{}", data);
        let expected = vec![
            "<mfrac>",
            "<mn>2</mn>",
            "<mn>3</mn>",
            "</mfrac>",
        ].join("");
        assert_eq!(expected, result);
    }

    #[test]
    fn test_display_with_line_thickness() {
        let mut data = MFracData::new(
            MNData::new(MValue::Text("2".to_owned())).to_tag(),
            MNData::new(MValue::Text("3".to_owned())).to_tag(),
        );
        data.line_thickness = Some(5);
        let result = format!("{}", data);
        let expected = vec![
            "<mfrac linethickness=\"5\">",
            "<mn>2</mn>",
            "<mn>3</mn>",
            "</mfrac>",
        ].join("");
        assert_eq!(expected, result);
    }
}