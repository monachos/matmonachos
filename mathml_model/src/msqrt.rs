use std::fmt::Display;
use std::fmt::Formatter;
use std::rc::Rc;
use crate::MTag;
use crate::ToMTag;

pub struct MSqrtData {
    pub value: Rc<MTag>,
}

impl MSqrtData {
    pub fn new(value: Rc<MTag>) -> Self {
        Self {
            value
        }
    }
}

impl ToMTag for MSqrtData {
    fn to_tag(self) -> Rc<MTag> {
        Rc::new(MTag::MSqrt(Rc::new(self)))
    }
}

impl Display for MSqrtData {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "<msqrt>")?;
        write!(f, "{}", self.value)?;
        write!(f, "</msqrt>")?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::mi::MIData;
    use crate::msqrt::MSqrtData;
    use crate::MValue;
    use crate::ToMTag;

    #[test]
    fn test_display() {
        let data = MSqrtData::new(
            MIData::new(MValue::Text("x".to_owned())).to_tag()
        ).to_tag();
        let result = format!("{}", data);
        let expected = vec![
            "<msqrt>",
            "<mi>x</mi>",
            "</msqrt>",
        ].join("");
        assert_eq!(expected, result);
    }
}