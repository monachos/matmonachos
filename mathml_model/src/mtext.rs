use crate::MTag;
use crate::MValue;
use crate::ToMTag;
use std::fmt::Display;
use std::fmt::Formatter;
use std::rc::Rc;

pub struct MTextData {
    pub value: MValue,
    pub style: Option<MValue>,
}

impl MTextData {
    pub fn new(value: MValue) -> Self {
        Self {
            value,
            style: None,
        }
    }

    pub fn from_string(value: &str) -> Self {
        Self {
            value: MValue::Text(String::from(value)),
            style: None,
        }
    }

    pub fn with_style(value: MValue, style: MValue) -> Self {
        Self {
            value,
            style: Some(style),
        }
    }
}

impl ToMTag for MTextData {
    fn to_tag(self) -> Rc<MTag> {
        Rc::new(MTag::MText(Rc::new(self)))
    }
}

impl Display for MTextData {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "<mtext")?;
        if let Some(the_style) = &self.style {
            write!(f, " style=\"{}\"", the_style)?;
        }
        write!(f, ">")?;
        write!(f, "{}", self.value)?;
        write!(f, "</mtext>")?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::mtext::MTextData;
    use crate::MValue;
    use crate::ToMTag;

    #[test]
    fn test_display() {
        let data = MTextData::from_string("abc").to_tag();
        let result = format!("{}", data);
        let expected = vec![
            "<mtext>",
            "abc",
            "</mtext>",
        ].join("");
        assert_eq!(expected, result);
    }

    #[test]
    fn test_display_style() {
        let data = MTextData::with_style(
            MValue::Text("abc".to_owned()),
            MValue::Text("font-weight:bold".to_owned()),
        ).to_tag();
        let result = format!("{}", data);
        let expected = vec![
            "<mtext style=\"font-weight:bold\">",
            "abc",
            "</mtext>",
        ].join("");
        assert_eq!(expected, result);
    }
}