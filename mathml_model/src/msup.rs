use std::fmt::Display;
use std::fmt::Formatter;
use std::rc::Rc;
use crate::MTag;
use crate::ToMTag;

pub struct MSupData {
    pub base: Rc<MTag>,
    pub superscript: Rc<MTag>,
}

impl MSupData {
    pub fn new(base: Rc<MTag>, superscript: Rc<MTag>) -> Self {
        Self {
            base,
            superscript,
        }
    }
}

impl ToMTag for MSupData {
    fn to_tag(self) -> Rc<MTag> {
        Rc::new(MTag::MSup(Rc::new(self)))
    }
}

impl Display for MSupData {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "<msup>")?;
        write!(f, "{}", self.base)?;
        write!(f, "{}", self.superscript)?;
        write!(f, "</msup>")?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::msup::MSupData;
    use crate::mi::MIData;
    use crate::mn::MNData;
    use crate::MValue;
    use crate::ToMTag;

    #[test]
    fn test_display() {
        let data = MSupData::new(
            MIData::new(MValue::Text("x".to_owned())).to_tag(),
            MNData::new(MValue::Text("2".to_owned())).to_tag(),
        ).to_tag();
        let result = format!("{}", data);
        let expected = vec![
            "<msup>",
            "<mi>x</mi>",
            "<mn>2</mn>",
            "</msup>",
        ].join("");
        assert_eq!(expected, result);
    }
}