use crate::MTag;
use crate::MValue;
use crate::ToMTag;
use std::fmt::Display;
use std::fmt::Formatter;
use std::rc::Rc;

pub struct MSpaceData {
    pub width: Option<MValue>,
}

impl MSpaceData {
    pub fn new() -> Self {
        Self {
            width: None,
        }
    }

    pub fn with_width(value: &str) -> Self {
        Self {
            width: Some(MValue::Text(String::from(value))),
        }
    }
}

impl ToMTag for MSpaceData {
    fn to_tag(self) -> Rc<MTag> {
        Rc::new(MTag::MSpace(Rc::new(self)))
    }
}

impl Display for MSpaceData {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "<mspace")?;
        if let Some(the_width) = &self.width {
            write!(f, " width=\"{}\"", the_width)?;
        }
        write!(f, "/>")?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::mspace::MSpaceData;
    use crate::ToMTag;

    #[test]
    fn test_display() {
        let data = MSpaceData::new().to_tag();
        let result = format!("{}", data);
        let expected = vec![
            "<mspace/>",
        ].join("");
        assert_eq!(expected, result);
    }

    #[test]
    fn test_display_with_width() {
        let data = MSpaceData::with_width("0.5em").to_tag();
        let result = format!("{}", data);
        let expected = vec![
            "<mspace width=\"0.5em\"/>",
        ].join("");
        assert_eq!(expected, result);
    }
}