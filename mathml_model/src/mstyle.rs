use crate::MValue;
use crate::MTag;
use crate::ToMTag;
use std::fmt::Display;
use std::fmt::Formatter;
use std::rc::Rc;

pub struct MStyleData {
    pub content: Option<Rc<MTag>>,
    pub math_size: Option<MValue>,
}

impl MStyleData {
    pub fn empty() -> Self {
        Self {
            content: None,
            math_size: None,
        }
    }

    pub fn new(content: Rc<MTag>) -> Self {
        Self {
            content: Some(content),
            math_size: None,
        }
    }
}

impl ToMTag for MStyleData {
    fn to_tag(self) -> Rc<MTag> {
        Rc::new(MTag::MStyle(Rc::new(self)))
    }
}

impl Display for MStyleData {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "<mstyle")?;
        if let Some(the_size) = &self.math_size {
            write!(f, " mathsize=\"{}\"", the_size)?;
        }
        write!(f, ">")?;
        if let Some(the_cont) = &self.content {
            write!(f, "{}", the_cont)?;
        }
        write!(f, "</mstyle>")?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::mn::MNData;
    use crate::mstyle::MStyleData;
    use crate::MValue;
    use crate::ToMTag;

    #[test]
    fn test_display_empty() {
        let data = MStyleData::empty().to_tag();
        let result = format!("{}", data);
        let expected = vec![
            "<mstyle>",
            "</mstyle>",
        ].join("");
        assert_eq!(expected, result);
    }

    #[test]
    fn test_display_with_content() {
        let data = MStyleData::new(MNData::from_string("1").to_tag());
        let result = format!("{}", data.to_tag());
        let expected = vec![
            "<mstyle>",
            "<mn>1</mn>",
            "</mstyle>",
        ].join("");
        assert_eq!(expected, result);
    }

    #[test]
    fn test_display_with_math_size() {
        let mut data = MStyleData::new(MNData::from_string("1").to_tag());
        data.math_size = Some(MValue::Text("50%".to_owned()));
        let result = format!("{}", data.to_tag());
        let expected = vec![
            "<mstyle mathsize=\"50%\">",
            "<mn>1</mn>",
            "</mstyle>",
        ].join("");
        assert_eq!(expected, result);
    }
}