use std::fmt::Display;
use std::fmt::Formatter;
use std::rc::Rc;
use crate::mtd::MTdData;

pub struct MTrData {
    pub children: Vec<Rc<MTdData>>,
}

impl MTrData {
    pub fn new() -> Self {
        Self {
            children: Vec::new(),
        }
    }

    pub fn from_vector(children: Vec<Rc<MTdData>>) -> Self {
        Self {
            children,
        }
    }

    pub fn to_rc(self) -> Rc<Self> {
        Rc::new(self)
    }
}

impl Display for MTrData {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "<mtr>")?;
        for c in &self.children {
            write!(f, "{}", c)?;
        }
        write!(f, "</mtr>")?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::mn::MNData;
    use crate::mtd::MTdData;
    use crate::mtr::MTrData;
    use crate::MValue;
    use crate::ToMTag;

    #[test]
    fn test_display_empty() {
        let data = MTrData::new();
        let result = format!("{}", data);
        assert_eq!("<mtr></mtr>", result);
    }

    #[test]
    fn test_display_children() {
        let data = MTrData::from_vector(vec![
            MTdData::new(MNData::new(MValue::Text("123".to_owned())).to_tag()).to_rc()
        ]);
        let result = format!("{}", data);
        let expected = vec![
            "<mtr>",
            "<mtd>",
            "<mn>123</mn>",
            "</mtd>",
            "</mtr>",
        ].join("");
        assert_eq!(expected, result);
    }
}