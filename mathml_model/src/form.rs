use std::fmt::Display;
use std::fmt::Formatter;

pub enum MForm {
    Prefix,
    Infix,
    Postfix,
}

impl Display for MForm {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            MForm::Prefix => write!(f, "prefix"),
            MForm::Infix => write!(f, "infix"),
            MForm::Postfix => write!(f, "postfix"),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::form::MForm;

    #[test]
    fn test_display_prefix() {
        let data = MForm::Prefix;
        let result = format!("{}", data);
        assert_eq!("prefix", result);
    }
}