use std::fmt::Display;
use std::fmt::Formatter;
use std::rc::Rc;
use crate::MTag;
use crate::MValue;
use crate::ToMTag;

pub struct MIData {
    pub value: MValue,
}

impl MIData {
    pub fn new(value: MValue) -> Self {
        Self {
            value
        }
    }

    pub fn from_string(value: &str) -> Self {
        Self {
            value: MValue::Text(String::from(value)),
        }
    }
}

impl ToMTag for MIData {
    fn to_tag(self) -> Rc<MTag> {
        Rc::new(MTag::MI(Rc::new(self)))
    }
}

impl Display for MIData {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "<mi>")?;
        write!(f, "{}", self.value)?;
        write!(f, "</mi>")?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::mi::MIData;
    use crate::MValue;
    use crate::ToMTag;

    #[test]
    fn test_display() {
        let data = MIData::new(MValue::Text("x".to_owned())).to_tag();
        let result = format!("{}", data);
        let expected = vec![
            "<mi>",
            "x",
            "</mi>",
        ].join("");
        assert_eq!(expected, result);
    }
}