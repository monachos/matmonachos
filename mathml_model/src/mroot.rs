use std::fmt::Display;
use std::fmt::Formatter;
use std::rc::Rc;
use crate::MTag;
use crate::ToMTag;

pub struct MRootData {
    pub radicand: Rc<MTag>,
    pub degree: Rc<MTag>,
}

impl MRootData {
    pub fn new(radicand: Rc<MTag>, degree: Rc<MTag>) -> Self {
        Self {
            radicand,
            degree,
        }
    }
}

impl ToMTag for MRootData {
    fn to_tag(self) -> Rc<MTag> {
        Rc::new(MTag::MRoot(Rc::new(self)))
    }
}

impl Display for MRootData {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "<mroot>")?;
        write!(f, "{}", self.radicand)?;
        write!(f, "{}", self.degree)?;
        write!(f, "</mroot>")?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::mroot::MRootData;
    use crate::mi::MIData;
    use crate::MValue;
    use crate::ToMTag;

    #[test]
    fn test_display() {
        let data = MRootData::new(
            MIData::new(MValue::Text("2".to_owned())).to_tag(),
            MIData::new(MValue::Text("3".to_owned())).to_tag(),
        ).to_tag();
        let result = format!("{}", data);
        let expected = vec![
            "<mroot>",
            "<mi>2</mi>",
            "<mi>3</mi>",
            "</mroot>",
        ].join("");
        assert_eq!(expected, result);
    }
}