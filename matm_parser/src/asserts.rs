#[cfg(test)]
pub mod tests {
    use crate::errors::ErrorItem;
    use crate::errors::ErrorItemSource;

    pub fn assert_error_items_that(errors: &Vec<ErrorItem>) -> AssertErrorItemsThat {
        AssertErrorItemsThat {
            errors,
        }
    }

    pub(crate) struct AssertErrorItemsThat<'a> {
        errors: &'a Vec<ErrorItem>,
    }

    impl<'a> AssertErrorItemsThat<'a> {
        pub fn log(self) -> Self {
            for err in self.errors {
                println!("{:?} error at ({},{}): {}", err.source, err.line, err.column, err.message);
            }
            return self;
        }

        pub fn empty(self) -> Self {
            assert_eq!(true, self.errors.is_empty(), "expected no errors");
            return self;
        }

        pub fn not_empty(self) -> Self {
            assert_eq!(false, self.errors.is_empty(), "expected 1 or more errors");
            return self;
        }

        pub fn has_lexer_message(self, expected_msg: &str) -> Self {
            let item = self.errors.iter().any(
                |e| e.source == ErrorItemSource::Lexer && expected_msg.eq(&e.message));
            assert_eq!(true, item, "expected lexer error with the message: {}", expected_msg);
            return self;
        }

        pub fn has_parser_message(self, expected_msg: &str) -> Self {
            let item = self.errors.iter().any(
                |e| e.source == ErrorItemSource::Parser && expected_msg.eq(&e.message));
            assert_eq!(true, item, "expected parser error with the message: {}", expected_msg);
            return self;
        }
    }
}