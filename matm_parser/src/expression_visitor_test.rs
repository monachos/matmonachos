#[cfg(test)]
mod tests {
    use std::rc::Rc;

    use matm_model::expressions::Expression;
    use matm_model::expressions::func::FunctionData;
    use matm_model::testing::asserts::assert_expression_that;

    use crate::asserts::tests::assert_error_items_that;
    use crate::errors::ErrorItemsContainer;
    use crate::errors::ErrorItemsConsumer;
    use crate::expression_visitor::VisitorElement;
    use crate::parse_and_run_script_visitor;
    use crate::parse_and_run_visitor;

    fn run_visitor(
        input_text: &str,
        errors: &mut dyn ErrorItemsConsumer,
    ) -> Option<VisitorElement> {
        return parse_and_run_visitor(input_text, errors);
    }

    fn run_expression_visitor(
        input_text: &str,
        errors: &mut ErrorItemsContainer,
    ) -> Rc<Expression> {
        let result = run_visitor(input_text, errors);
        match result {
            Some(element) => {
                return match element {
                    VisitorElement::Expr(expr) => expr,
                    VisitorElement::Script(script) => {
                        if script.len() != 1 {
                            assert!(false, "Expected one expression, found {}", script.len());
                            panic!("Expected expression");
                        }
                        Rc::clone(script.get(0).unwrap())
                    }
                }
            }
            None => {
                for err in errors.all() {
                    println!("{:?} error at ({},{}): {}", err.source, err.line, err.column, err.message);
                }
                assert!(false, "expected visitor element, found errors");
                panic!("expected visitor element, found errors");
            },
        }
    }

    fn run_expression_visitor_ok(input_text: &str) -> Rc<Expression> {
        let mut errors = ErrorItemsContainer::new();
        let result = run_expression_visitor(input_text, &mut errors);
        assert_error_items_that(&errors.all())
            .log()
            .empty();
        return result;
    }

    fn run_script_visitor_ok(input_text: &str) -> Vec<Rc<Expression>> {
        let mut errors = ErrorItemsContainer::new();
        let result = parse_and_run_script_visitor(input_text, &mut errors);
        assert_error_items_that(&errors.all())
            .log()
            .empty();
        return result;
    }

    fn run_script_visitor(
        input_text: &str,
        errors: &mut dyn ErrorItemsConsumer,
    ) -> Vec<Rc<Expression>> {
        return parse_and_run_script_visitor(input_text, errors);
    }

    #[test]
    fn test_int_dec() {
        let result = run_expression_visitor_ok("25");
        assert_expression_that(result.as_ref())
            .as_integer()
            .is_int(25);
    }

    #[test]
    fn test_int_dec_minus() {
        let result = run_expression_visitor_ok("-25");
        assert_expression_that(result.as_ref())
            .as_integer()
            .is_int(-25);
    }

    #[test]
    fn test_int_radix() {
        let result = run_expression_visitor_ok("16#AF#");
        assert_expression_that(result.as_ref())
            .as_integer()
            .is_int(0xAF);
    }

    #[test]
    fn test_int_radix_minus() {
        let result = run_expression_visitor_ok("-16#AF#");
        assert_expression_that(result.as_ref())
            .as_integer()
            .is_int(-0xAF);
    }

    #[test]
    #[ignore = "Not implemented number literal"]
    fn test_int_radix_multiple() {
        let result = run_expression_visitor_ok("60#(IIH)(IHHH)#");
        assert_expression_that(result.as_ref())
            .as_integer();
        //TODO how to represent this integer?
    }

    #[test]
    fn test_symbol_x() {
        let result = run_expression_visitor_ok("x");
        assert_expression_that(result.as_ref())
            .as_symbol()
            .has_name("x");
    }

    #[test]
    fn test_neg_symbol_x() {
        let result = run_expression_visitor_ok("-x");
        assert_expression_that(result.as_ref())
            .as_unary()
            .is_neg()
            .as_symbol()
            .has_name("x");
    }

    #[test]
    fn test_symbol_abc123() {
        let result = run_expression_visitor_ok("abc123");
        assert_expression_that(result.as_ref())
            .as_symbol()
            .has_name("abc123");
    }

    #[test]
    fn test_symbol_a_prim() {
        let result = run_expression_visitor_ok("A'");
        assert_expression_that(result.as_ref())
            .as_symbol()
            .has_name("A'");
    }

    #[test]
    fn test_binary_int_add_int() {
        let result = run_expression_visitor_ok("1 + 2");
        assert_expression_that(result.as_ref())
            .as_binary()
            .is_add()
            .has_left_i32(1)
            .has_right_i32(2)
            ;
    }

    #[test]
    fn test_binary_int_add_nint() {
        let result = run_expression_visitor_ok("1 + -2");
        assert_expression_that(result.as_ref())
            .as_binary()
            .is_add()
            .has_left_i32(1)
            .has_right_i32(-2)
            ;
    }

    #[test]
    fn test_binary_add_add() {
        let result = run_expression_visitor_ok("1 + 2 + 3");
        assert_expression_that(result.as_ref())
            .as_binary()
            .has_left_that(|e| {
                e.as_binary()
                .has_left_i32(1)
                .is_add()
                .has_right_i32(2);
            })
            .is_add()
            .has_right_i32(3)
            ;
    }

    #[test]
    fn test_binary_add_sub() {
        let result = run_expression_visitor_ok("1 + 2 - 3");
        println!("result={}", result);
        assert_expression_that(result.as_ref())
            .as_binary()
            .has_left_that(|e| {
                e.as_binary()
                .has_left_i32(1)
                .is_add()
                .has_right_i32(2);
            })
            .is_sub()
            .has_right_i32(3)
            ;
    }

    #[test]
    fn test_binary_int_sub_int() {
        let result = run_expression_visitor_ok("1 - 2");
        assert_expression_that(result.as_ref())
            .as_binary()
            .is_sub()
            .has_left_i32(1)
            .has_right_i32(2)
            ;
    }

    #[test]
    fn test_binary_int_sub_nint() {
        let result = run_expression_visitor_ok("1 - -2");
        assert_expression_that(result.as_ref())
            .as_binary()
            .is_sub()
            .has_left_i32(1)
            .has_right_i32(-2)
            ;
    }

    #[test]
    fn test_binary_mul() {
        let result = run_expression_visitor_ok("2 * 3");
        assert_expression_that(result.as_ref())
            .as_binary()
            .is_mul()
            .has_left_i32(2)
            .has_right_i32(3)
            ;
    }

    #[test]
    fn test_binary_div() {
        let result = run_expression_visitor_ok("2 div 3");
        assert_expression_that(result.as_ref())
            .as_binary()
            .is_div()
            .has_left_i32(2)
            .has_right_i32(3)
            ;
    }

    #[test]
    fn test_binary_mul_mul() {
        let result = run_expression_visitor_ok("2 * 3 * 4");
        assert_expression_that(result.as_ref())
            .as_binary()
            .has_left_that(|e| {
                e.as_binary()
                .has_left_i32(2)
                .is_mul()
                .has_right_i32(3);
            })
            .is_mul()
            .has_right_i32(4)
            ;
    }

    #[test]
    fn test_binary_mul_div() {
        let result = run_expression_visitor_ok("2 * 3 div 4");
        assert_expression_that(result.as_ref())
            .as_binary()
            .has_left_that(|e| {
                e.as_binary()
                .has_left_i32(2)
                .is_mul()
                .has_right_i32(3);
            })
            .is_div()
            .has_right_i32(4)
            ;
    }

    #[test]
    fn test_binary_mul_add_int() {
        let result = run_expression_visitor_ok("1 * 2 + 3");
        assert_expression_that(result.as_ref())
            .as_binary()
            .is_add()
            .has_left_that(|e| {
                e.as_binary()
                .is_mul()
                .has_left_i32(1)
                .has_right_i32(2)
                ;
            })
            .has_right_that(|e| {
                e.as_integer()
                .is_int(3);
            });
    }

    #[test]
    fn test_binary_int_add_mul() {
        let result = run_expression_visitor_ok("1 + 2 * 3");
        assert_expression_that(result.as_ref())
            .as_binary()
            .is_add()
            .has_left_that(|e| {
                e.as_integer()
                .is_int(1);
            })
            .has_right_that(|e| {
                e.as_binary()
                .is_mul()
                .has_left_i32(2)
                .has_right_i32(3)
                ;
            });
    }

    #[test]
    fn test_binary_mul_add_mul() {
        let result = run_expression_visitor_ok("1 * 2 + 3 * 4");
        assert_expression_that(result.as_ref())
            .as_binary()
            .is_add()
            .has_left_that(|e| {
                e.as_binary()
                .is_mul()
                .has_left_i32(1)
                .has_right_i32(2)
                ;
            })
            .has_right_that(|e| {
                e.as_binary()
                .is_mul()
                .has_left_i32(3)
                .has_right_i32(4)
                ;
            });
    }

    #[test]
    fn test_binary_mul_par_add_int() {
        let result = run_expression_visitor_ok("1 * (2 + 3)");
        assert_expression_that(result.as_ref())
            .as_binary()
            .is_mul()
            .has_left_that(|e| {
                e.as_integer()
                .is_int(1);
            })
            .has_right_that(|e| {
                e.as_binary()
                .is_add()
                .has_left_i32(2)
                .has_right_i32(3)
                ;
            })
            ;
    }

    #[test]
    fn test_frac_int_int() {
        let result = run_expression_visitor_ok("2/3");
        assert_expression_that(result.as_ref())
            .as_fraction()
            .is_ints(2, 3);
    }

    #[test]
    fn test_frac_nint_int() {
        let result = run_expression_visitor_ok("-2/3");
        assert_expression_that(result.as_ref())
            .as_unary()
            .is_neg()
            .as_fraction()
            .is_ints(2, 3);
    }

    #[test]
    fn test_frac_int_nint() {
        let result = run_expression_visitor_ok("2/(-3)");
        assert_expression_that(result.as_ref())
            .as_fraction()
            .is_ints(2, -3);
    }

    #[test]
    #[ignore = "Does not work after adding equation to expr"]
    fn test_frac_int_bad_neg_int() {
        let mut errors = ErrorItemsContainer::new();
        let result = run_expression_visitor("2/-3", &mut errors);

        assert_error_items_that(&errors.all())
            .log()
            .not_empty()
            .has_parser_message("extraneous input '-' expecting {ID, INT_DECIMAL, INT_RADIX_SINGLE, INT_RADIX_MULTIPLE, '('}");

        assert_expression_that(result.as_ref())
            .as_fraction()
            .has_numerator_int(2)
            .has_denominator_int(3);
    }

    #[test]
    fn test_frac_symbol_int() {
        let result = run_expression_visitor_ok("x/2");
        assert_expression_that(result.as_ref())
            .as_fraction()
            .has_numerator_that(|num| {
                num.as_symbol()
                .has_name("x");
            })
            .has_denominator_that(|den| {
                den.as_integer()
                .is_int(2);
            });
    }

    #[test]
    fn test_frac_mul_int() {
        let result = run_expression_visitor_ok("(2*3)/4");
        assert_expression_that(result.as_ref())
            .as_fraction()
            .has_numerator_that(|num| {
                num.as_binary()
                .has_left_that(|e| {e.as_integer().is_int(2);})
                .is_mul()
                .has_right_that(|e| {e.as_integer().is_int(3);})
                ;
            })
            .has_denominator_int(4);
    }

    #[test]
    fn test_frac_int_sum() {
        let result = run_expression_visitor_ok("2/(3+4)");
        assert_expression_that(result.as_ref())
            .as_fraction()
            .has_numerator_int(2)
            .has_denominator_that(|den| {
                den.as_binary()
                .has_left_that(|e| {e.as_integer().is_int(3);})
                .is_add()
                .has_right_that(|e| {e.as_integer().is_int(4);})
                ;
            });
    }

    #[test]
    fn test_int_pow_int() {
        let result = run_expression_visitor_ok("1^2");
        assert_expression_that(result.as_ref())
            .as_exponent()
            .is_base_integer_that(|e| {e.is_int(1);})
            .is_exponent_integer_that(|e| {e.is_int(2);});
    }

    #[test]
    fn test_symbol_pow_int() {
        let result = run_expression_visitor_ok("x^2");
        assert_expression_that(result.as_ref())
            .as_exponent()
            .has_base_that(|e| {e.as_symbol();})
            .is_exponent_integer_that(|e| {e.is_int(2);});
    }

    #[test]
    fn test_int_pow_frac() {
        let result = run_expression_visitor_ok("x^2/3");
        assert_expression_that(result.as_ref())
            .as_exponent()
            .has_base_that(|e| {e.as_symbol();})
            .has_exponent_that(|e| {e.as_fraction().is_ints(2, 3);});
    }

    #[test]
    fn test_frac_pow_frac() {
        let result = run_expression_visitor_ok("1/2^3/4");
        assert_expression_that(result.as_ref())
            .as_exponent()
            .has_base_that(|e| {e.as_fraction().is_ints(1, 2);})
            .has_exponent_that(|e| {e.as_fraction().is_ints(3, 4);});
    }

    #[test]
    fn test_mul_pow() {
        let result = run_expression_visitor_ok("1*2^3");
        assert_expression_that(result.as_ref())
            .as_binary()
            .is_mul()
            .has_left_i32(1)
            .has_right_that(|e| {
                e.as_exponent()
                    .is_base_integer_that(|e| {e.is_int(2);})
                    .is_exponent_integer_that(|e| {e.is_int(3);});
            });
    }

    #[test]
    fn test_pow_mul() {
        let result = run_expression_visitor_ok("1^2*3");
        assert_expression_that(result.as_ref())
            .as_binary()
            .is_mul()
            .has_left_that(|e| {
                e.as_exponent()
                    .is_base_integer_that(|e| {e.is_int(1);})
                    .is_exponent_integer_that(|e| {e.is_int(2);});
            })
            .has_right_i32(3);
    }

    #[test]
    fn test_sin_int() {
        let result = run_expression_visitor_ok("sin(1)");
        assert_expression_that(result.as_ref())
            .as_function()
            .has_name(FunctionData::SIN)
            .has_arguments_size(1)
            .has_argument_i_that(0, |e| {
                e.as_integer().is_int(1);
            });
    }

    #[test]
    fn test_cos_int() {
        let result = run_expression_visitor_ok("cos(1)");
        assert_expression_that(result.as_ref())
            .as_function()
            .has_name(FunctionData::COS)
            .has_arguments_size(1)
            .has_argument_i_that(0, |e| {
                e.as_integer().is_int(1);
            });
    }

    #[test]
    fn test_tan_int() {
        let result = run_expression_visitor_ok("tan(1)");
        assert_expression_that(result.as_ref())
            .as_function()
            .has_name(FunctionData::TAN)
            .has_arguments_size(1)
            .has_argument_i_that(0, |e| {
                e.as_integer().is_int(1);
            });
    }

    #[test]
    fn test_cot_int() {
        let result = run_expression_visitor_ok("cot(1)");
        assert_expression_that(result.as_ref())
            .as_function()
            .has_name(FunctionData::COT)
            .has_arguments_size(1)
            .has_argument_i_that(0, |e| {
                e.as_integer().is_int(1);
            });
    }

    #[test]
    fn test_sec_int() {
        let result = run_expression_visitor_ok("sec(1)");
        assert_expression_that(result.as_ref())
            .as_function()
            .has_name(FunctionData::SEC)
            .has_arguments_size(1)
            .has_argument_i_that(0, |e| {
                e.as_integer().is_int(1);
            });
    }

    #[test]
    fn test_csc_int() {
        let result = run_expression_visitor_ok("csc(1)");
        assert_expression_that(result.as_ref())
            .as_function()
            .has_name(FunctionData::CSC)
            .has_arguments_size(1)
            .has_argument_i_that(0, |e| {
                e.as_integer().is_int(1);
            });
    }

    #[test]
    fn test_sin_binary() {
        let result = run_expression_visitor_ok("sin(1+2)");
        assert_expression_that(result.as_ref())
            .as_function()
            .has_name(FunctionData::SIN)
            .has_arguments_size(1)
            .has_argument_i_that(0, |e| {
                e.as_binary()
                .has_left_i32(1)
                .is_add()
                .has_right_i32(2)
            ;
            });
    }

    #[test]
    fn test_symbol_method_no_args() {
        let result = run_expression_visitor_ok("x.foo()");
        assert_expression_that(result.as_ref())
            .as_method()
            .has_object_that(|e| {e.as_symbol().has_name("x");})
            .has_name("foo")
            .has_arguments_size(0);
    }

    #[test]
    fn test_symbol_method_int() {
        let result = run_expression_visitor_ok("x.foo(2)");
        assert_expression_that(result.as_ref())
            .as_method()
            .has_object_that(|e| {e.as_symbol().has_name("x");})
            .has_name("foo")
            .has_arguments_size(1)
            .has_argument_i_that(0, |e|{e.as_integer().is_int(2);});
    }

    #[test]
    fn test_symbol_method_int_method() {
        let result = run_expression_visitor_ok("x.foo(2).is_empty()");
        assert_expression_that(result.as_ref())
            .as_method()
            .has_object_that(|e| {
                e.as_method()
                .has_object_that(|e| {e.as_symbol().has_name("x");})
                .has_name("foo")
                .has_arguments_size(1)
                .has_argument_i_that(0, |e|{e.as_integer().is_int(2);})
                ;
            })
            .has_name("is_empty")
            .has_arguments_size(0);
    }

    #[test]
    fn test_fraction_method_int() {
        let result = run_expression_visitor_ok("(1/2).foo(5)");
        assert_expression_that(result.as_ref())
            .as_method()
            .has_object_that(|e| {e.as_fraction().is_ints(1, 2);})
            .has_name("foo")
            .has_arguments_size(1)
            .has_argument_i_that(0, |e|{e.as_integer().is_int(5);});
    }

    #[test]
    fn test_equation_symbol_eq_int() {
        let result = run_expression_visitor_ok("x = 1");
        assert_expression_that(result.as_ref())
            .as_equation()
            .is_left_symbol_that(|s| { s.has_name("x"); })
            .has_right_that(|e| {
                e.as_integer().is_int(1)
                ;
            })
        ;
    }

    #[test]
    fn test_equation_symbol_eq_bin() {
        let result = run_expression_visitor_ok("x = 1 + 2");
        assert_expression_that(result.as_ref())
            .as_equation()
            .is_left_symbol_that(|s| { s.has_name("x"); })
            .has_right_that(|e| {
                e.as_binary()
                    .has_left_i32(1)
                    .has_right_i32(2)
                ;
            })
        ;
    }

    #[test]
    fn test_assignment_int() {
        let result = run_expression_visitor_ok("x: 123");
        assert_expression_that(result.as_ref())
            .as_assignment()
            .is_source_integer_that(|e| {e.is_int(123);})
            .is_target_symbol_that(|e| {e.has_name("x");})
        ;
    }

    #[test]
    fn test_assignment_binary() {
        let result = run_expression_visitor_ok("x: 1+2");
        assert_expression_that(result.as_ref())
            .as_assignment()
            .has_source_that(|e| {
                e.as_binary()
                .has_left_i32(1)
                .is_add()
                .has_right_i32(2);
            })
            .is_target_symbol_that(|e| {e.has_name("x");})
        ;
    }

    //-------------------------------------------------

    #[test]
    fn test_script_one_no_end() {
        let result = run_script_visitor_ok("1 + 2");
        assert_eq!(1, result.len());

        assert_expression_that(result.get(0).unwrap())
            .as_binary()
            .is_add()
            .has_left_i32(1)
            .has_right_i32(2)
            ;
    }

    #[test]
    fn test_script_one_end() {
        let result = run_script_visitor_ok("1 + 2;");
        assert_eq!(1, result.len());

        assert_expression_that(result.get(0).unwrap())
            .as_binary()
            .is_add()
            .has_left_i32(1)
            .has_right_i32(2)
            ;
    }

    #[test]
    fn test_script_two_end() {
        let result = run_script_visitor_ok("1 + 2; 3 * 4");
        assert_eq!(2, result.len());

        assert_expression_that(result.get(0).unwrap())
            .as_binary()
            .is_add()
            .has_left_i32(1)
            .has_right_i32(2)
            ;
        assert_expression_that(result.get(1).unwrap())
            .as_binary()
            .is_mul()
            .has_left_i32(3)
            .has_right_i32(4)
            ;
    }

    #[test]
    fn test_script_two_2ends() {
        let result = run_script_visitor_ok("1 + 2; 3 * 4;");
        assert_eq!(2, result.len());

        assert_expression_that(result.get(0).unwrap())
            .as_binary()
            .is_add()
            .has_left_i32(1)
            .has_right_i32(2)
            ;
        assert_expression_that(result.get(1).unwrap())
            .as_binary()
            .is_mul()
            .has_left_i32(3)
            .has_right_i32(4)
            ;
    }

    #[test]
    fn test_syntax_error() {
        let mut errors = ErrorItemsContainer::new();
        let result = run_script_visitor("&", &mut errors);

        assert!(result.is_empty());

        assert_error_items_that(&errors.all())
            .log()
            .not_empty()
            .has_lexer_message("token recognition error at: '&'")
            .has_parser_message("mismatched input '<EOF>' expecting {'-', ID, INT_DECIMAL, INT_RADIX_SINGLE, INT_RADIX_MULTIPLE, '('}");
    }
}
