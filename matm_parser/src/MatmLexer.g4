lexer grammar MatmLexer;


STATEMENT_END : ';';
OPERATOR_EQ : '=';
OPERATOR_POW : '^';
OPERATOR_MUL : '*';
OPERATOR_DIV : 'div';
OPERATOR_FRAC : '/';
OPERATOR_ADD : '+';
OPERATOR_SUB : '-';

ID : ('a'..'z' | 'A'..'Z' | '_')
   ('a'..'z' | 'A'..'Z' | '_' | '0'..'9')*
   ('\'')*;

// integer in decimal radix: 1, 1234
INT_DECIMAL : [0-9]+ ;

// integer in non-decimal radix: 2#1001#, 16#1F0A#
INT_RADIX_SINGLE : [0-9]+ '#' (~('(' | ')'))+ '#';

// integer in non-decimal radix with multi-character digits: 60#(20)(55)#
INT_RADIX_MULTIPLE : [0-9]+ '#' ('(' ~('(' | ')')+ ')')+ '#';

WS : (' '|'\n') -> skip ;

LPAREN : '(';
RPAREN : ')';
COMMA : ',';
DOT : '.';
COLON : ':';
