use crate::errors::ErrorItem;
use crate::errors::ErrorItemsConsumer;

pub(crate) struct ErrorItemsBuffer {
    items: Vec<ErrorItem>,
}

impl ErrorItemsBuffer {
    pub fn new() -> Self {
        Self {
            items: Vec::new(),
        }
    }

    pub fn add(&mut self, item: ErrorItem) {
        self.items.push(item);
    }

    pub fn copy_items(&self, errors: &mut dyn ErrorItemsConsumer) {
        for e in self.items.iter() {
            errors.add_error_item(e.clone());
        }
    }
}

