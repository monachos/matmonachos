use std::cell::RefCell;
use std::rc::Rc;
use antlr_rust::error_listener::ErrorListener;
use antlr_rust::errors::ANTLRError;
use antlr_rust::recognizer::Recognizer;
use antlr_rust::token_factory::TokenFactory;
use crate::errors::ErrorItem;
use crate::errors::ErrorItemSource;
use crate::errors::buffer::ErrorItemsBuffer;

pub(crate) struct LexerErrorsListener {
    pub listener: Rc<RefCell<ErrorItemsBuffer>>,
}

impl LexerErrorsListener {
    pub fn new(listener: &Rc<RefCell<ErrorItemsBuffer>>) -> Self {
        LexerErrorsListener {
            listener: Rc::clone(listener),
        }
    }
}

impl<'a, T: Recognizer<'a>> ErrorListener<'a, T> for LexerErrorsListener {
    fn syntax_error(
        &self,
        _recognizer: &T,
        _offending_symbol: Option<&<<T>::TF as TokenFactory<'a>>::Inner>,
        line: isize,
        column: isize,
        msg: &str,
        _error: Option<&ANTLRError>,
    ) {
        self.listener.borrow_mut().add(ErrorItem {
            source: ErrorItemSource::Lexer,
            line,
            column,
            message: msg.to_owned(),
        });
    }
}