pub(crate) mod parser;
pub(crate) mod lexer;
pub(crate) mod buffer;

#[derive(Clone)]
#[derive(Debug)]
#[derive(PartialEq)]
pub enum ErrorItemSource {
    Lexer,
    Parser,
    Visitor,
}

#[derive(Clone)]
pub struct ErrorItem {
    pub source: ErrorItemSource,
    pub line: isize,
    pub column: isize,
    pub message: String,
}

pub trait ErrorItemsConsumer {
    fn add_error_item(&mut self, item: ErrorItem);
}

//--------------------------------------------------------

pub struct ErrorItemsContainer {
    items: Vec<ErrorItem>,
}

impl ErrorItemsContainer {
    pub fn new() -> Self {
        Self {
            items: Vec::new(),
        }
    }

    pub fn all(&self) -> &Vec<ErrorItem> {
        &self.items
    }
}

impl ErrorItemsConsumer for ErrorItemsContainer {
    fn add_error_item(&mut self, item: ErrorItem) {
        self.items.push(item);
    }
}
