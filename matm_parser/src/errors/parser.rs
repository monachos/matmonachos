use std::cell::RefCell;
use std::rc::Rc;
use antlr_rust::atn_config_set::ATNConfigSet;
use antlr_rust::error_listener::ErrorListener;
use antlr_rust::errors::ANTLRError;
use antlr_rust::recognizer::Recognizer;
use antlr_rust::token_factory::TokenFactory;
use bit_set::BitSet;
use crate::errors::ErrorItem;
use crate::errors::ErrorItemSource;
use crate::errors::buffer::ErrorItemsBuffer;

pub(crate) struct ParserErrorsListener {
    pub listener: Rc<RefCell<ErrorItemsBuffer>>,
}

impl ParserErrorsListener {
    pub fn new(listener: &Rc<RefCell<ErrorItemsBuffer>>) -> Self {
        ParserErrorsListener {
            listener: Rc::clone(listener),
        }
    }
}

impl<'a, T: Recognizer<'a>> ErrorListener<'a, T> for ParserErrorsListener {
    fn syntax_error(
        &self,
        _recognizer: &T,
        _offending_symbol: Option<&<T::TF as TokenFactory<'a>>::Inner>,
        line: isize,
        column: isize,
        msg: &str,
        _e: Option<&ANTLRError>,
    ) {
        self.listener.borrow_mut().add(ErrorItem {
            source: ErrorItemSource::Parser,
            line,
            column,
            message: msg.to_owned(),
        });
    }

    fn report_ambiguity(
        &self,
        _recognizer: &T,
        _dfa: &antlr_rust::dfa::DFA,
        _start_index: isize,
        _stop_index: isize,
        _exact: bool,
        _ambig_alts: &BitSet,
        _configs: &ATNConfigSet,
    ) {
        // Handle ambiguity errors if needed
    }

    fn report_attempting_full_context(
        &self,
        _recognizer: &T,
        _dfa: &antlr_rust::dfa::DFA,
        _start_index: isize,
        _stop_index: isize,
        _conflicting_alts: &BitSet,
        _configs: &ATNConfigSet,
    ) {
        // Handle full context errors if needed
    }

    fn report_context_sensitivity(
        &self,
        _recognizer: &T,
        _dfa: &antlr_rust::dfa::DFA,
        _start_index: isize,
        _stop_index: isize,
        _prediction: isize,
        _configs: &ATNConfigSet,
    ) {
        // Handle context sensitivity errors if needed
    }
}

