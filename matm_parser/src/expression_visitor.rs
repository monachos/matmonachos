use std::cell::RefCell;
use std::rc::Rc;
use antlr_rust::tree::ParseTree;
use antlr_rust::tree::ParseTreeVisitorCompat;
use matm_model::core::natural::RADIX_10;
use matm_model::expressions::assignment::AssignmentData;
use matm_model::expressions::binary::BinaryData;
use matm_model::expressions::fractions::FracData;
use matm_model::expressions::func::FunctionData;
use matm_model::expressions::int::IntData;
use matm_model::expressions::method::MethodData;
use matm_model::expressions::sign::Sign;
use matm_model::expressions::symbol::SymbolData;
use matm_model::expressions::unary::UnaryData;
use matm_model::expressions::Expression;
use matm_model::expressions::ToExpression;
use regex::Regex;
use matm_model::expressions::equation::EquationData;
use matm_model::expressions::exponentiation::ExpData;
use crate::antlr::matmparser;
use crate::antlr::matmparser::Adding_exprContextAttrs;
use crate::antlr::matmparser::EquationContext;
use crate::antlr::matmparser::ExponentContext;
use crate::antlr::matmparser::ExponentContextAttrs;
use crate::antlr::matmparser::ExponentiationContext;
use crate::antlr::matmparser::ExponentiationContextAttrs;
use crate::antlr::matmparser::ArgumentsContextAttrs;
use crate::antlr::matmparser::AssignmentContextAttrs;
use crate::antlr::matmparser::ExprContext;
use crate::antlr::matmparser::FactorContext;
use crate::antlr::matmparser::FunctioninvContextAttrs;
use crate::antlr::matmparser::MatmParserContextType;
use crate::antlr::matmparser::MethodinvContextAttrs;
use crate::antlr::matmparser::NumberContextAttrs;
use crate::antlr::matmparser::Simple_factorContext;
use crate::antlr::matmparser::Simple_factorContextAttrs;
use crate::antlr::matmparser::StartContext;
use crate::antlr::matmparser::StatementsContextAttrs;
use crate::antlr::matmparser::SymbolContextAttrs;
use crate::antlr::matmparser::TermContext;
use crate::antlr::matmparser::ExprContextAttrs;
use crate::antlr::matmparser::FactorContextAttrs;
use crate::antlr::matmparser::StartContextAttrs;
use crate::antlr::matmparser::TermContextAttrs;
use crate::antlr::matmparser::OPERATOR_ADD;
use crate::antlr::matmparser::OPERATOR_DIV;
use crate::antlr::matmparser::OPERATOR_MUL;
use crate::antlr::matmparser::OPERATOR_SUB;
use crate::antlr::matmparservisitor::MatmParserVisitorCompat;
use crate::errors::ErrorItem;
use crate::errors::ErrorItemSource;
use crate::errors::buffer::ErrorItemsBuffer;


pub enum VisitorElement {
    Expr(Rc<Expression>),
    Script(Vec<Rc<Expression>>),
}

pub struct ExpressionVisitor {
    pub expression: Option<VisitorElement>,
    errors: Rc<RefCell<ErrorItemsBuffer>>,
}

impl ExpressionVisitor {
    pub(crate) fn new(error_listener: Rc<RefCell<ErrorItemsBuffer>>) -> Self {
        Self {
            expression: None,
            errors: error_listener,
        }
    }

    fn add_error(&mut self, message: String) {
        self.errors.borrow_mut().add(ErrorItem {
            source: ErrorItemSource::Visitor,
            line: 0,
            column: 0,
            message,
        });
    }

    /// Extract from Option<VisitorElement>
    fn extract_rc_expression_from_ove(
        ove: Option<VisitorElement>,
    ) -> Rc<Expression> {
        if let Some(ve) = ove {
            match ve {
                VisitorElement::Expr(e) => {
                    return e;
                }
                _ => {}
            }
        }
        return Rc::new(Expression::Undefined);
    }

    /// Extract from Option<VisitorElement>
    fn extract_vec_rc_expression_from_ove(
        ove: Option<VisitorElement>,
    ) -> Vec<Rc<Expression>> {
        if let Some(ve) = ove {
            return match ve {
                VisitorElement::Expr(e) => vec![e],
                VisitorElement::Script(scr) => scr,
            };
        }
        return Vec::new();
    }

    fn wrap_expr_with_methodinv(
        &mut self,
        ctx: &ExponentiationContext<'_>,
        called_obj: Rc<Expression>,
    ) -> Rc<Expression> {
        let mut result: Rc<Expression> = called_obj;
        for method in &ctx.methodinv_all() {
            match Self::extract_rc_expression_from_ove(self.visit(&**method)).as_ref() {
                Expression::Function(func) => {
                    result = MethodData::from_function(result, *func.clone()).to_rc_expr();
                }
                _ => {
                    self.errors.borrow_mut().add(ErrorItem {
                        source: ErrorItemSource::Visitor,
                        line: 0,
                        column: 0,
                        message: "Unknown method invocation".to_owned(),
                    });
                }
            }
        }
        return result;
    }

    fn wrap_expr_with_negation(
        expr: Rc<Expression>,
        is_negation: bool,
    ) -> Rc<Expression> {
        let mut result: Rc<Expression> = expr;
        if is_negation {
            if let Expression::Int(int_v) = result.as_ref() {
                match int_v.sign() {
                    Sign::Negative => {
                        result = UnaryData::neg(result).to_rc_expr();
                    }
                    Sign::Zero => {
                        // keep the same
                    }
                    Sign::Positive => {
                        result = int_v.calculate_neg().to_rc_expr();
                    }
                }
            } else {
                result = UnaryData::neg(result).to_rc_expr();
            }
        }
        return result;
    }
}

impl ParseTreeVisitorCompat<'_> for ExpressionVisitor {
    type Node = MatmParserContextType;
    type Return = Option<VisitorElement>;

    fn temp_result(&mut self) -> &mut Self::Return {
        return &mut self.expression;
    }

    fn aggregate_results(&self, _aggregate: Self::Return, _next: Self::Return) -> Self::Return {
        panic!("Should not be reachable")
    }
}

impl MatmParserVisitorCompat<'_> for ExpressionVisitor {
    fn visit_start(&mut self, ctx: &StartContext<'_>) -> Self::Return {
        self.visit(&*ctx.statements().unwrap())
    }

    fn visit_number(&mut self, ctx: &matmparser::NumberContext<'_>) -> Self::Return {
        if let Some(int_dec) = ctx.INT_DECIMAL() {
            let text_value: String = int_dec.get_text();
            return match IntData::from_string(RADIX_10, &text_value) {
                Ok(int_v) => Some(VisitorElement::Expr(int_v.to_rc_expr())),
                Err(err) => {
                    self.add_error(format!("Unknown number format: {}", err.message));
                    Some(VisitorElement::Expr(Rc::new(Expression::Undefined)))
                }
            }
        }
        if let Some(int_radix) = ctx.INT_RADIX_SINGLE() {
            let text_value: String = int_radix.get_text();
            let pattern = Regex::new(r"^(\d+)#([^#()]+)#$").unwrap();
            if let Some(captures) = pattern.captures(&text_value) {
                let radix_text = captures.get(1).unwrap().as_str();
                let value_text = captures.get(2).unwrap().as_str();

                // Parse the base from string to u32
                let radix: u32 = radix_text.parse().unwrap();
                return match IntData::from_string(radix, value_text) {
                    Ok(int_v) => Some(VisitorElement::Expr(int_v.to_rc_expr())),
                    Err(err) => {
                        self.add_error(format!("Unknown number format: {}", err.message));
                        Some(VisitorElement::Expr(Rc::new(Expression::Undefined)))
                    }
                }
            }
        }
        self.add_error("Not implemented number literal".to_owned());
        None
    }

    fn visit_expr(&mut self, ctx: &ExprContext<'_>) -> Self::Return {
        return if let Some(eq_el) = ctx.equation() {
            self.visit(&*eq_el)
        } else if let Some(adding_expr_v) = ctx.adding_expr() {
            self.visit(&*adding_expr_v)
        } else if let Some(assignment_v) = ctx.assignment() {
            self.visit(&*assignment_v)
        } else {
            self.add_error("Not implemented expression".to_owned());
            None
        }
    }

    fn visit_equation(&mut self, ctx: &EquationContext<'_>) -> Self::Return {
        let left: Rc<Expression> = if let Some(left_el) = &ctx.left {
            if let Some(expr) = self.visit(left_el.as_ref()) {
                if let VisitorElement::Expr(expr) = expr {
                    expr
                } else {
                    return None;
                }
            } else {
                return None;
            }
        } else {
            return None;
        };
        let right: Rc<Expression> = if let Some(right_el) = &ctx.right {
            if let Some(expr) = self.visit(right_el.as_ref()) {
                if let VisitorElement::Expr(expr) = expr {
                    expr
                } else {
                    return None;
                }
            } else {
                return None;
            }
        } else {
            return None;
        };

        return Some(VisitorElement::Expr(EquationData::new(left, right).to_rc_expr()));
    }

    fn visit_adding_expr(&mut self, ctx: &matmparser::Adding_exprContext<'_>) -> Self::Return {
        let mut left: Rc<Expression> = Self::extract_rc_expression_from_ove(self.visit(&*ctx.term(0).unwrap()));
        let mut operator_index = 0;
        let operators = ctx.OPERATOR_ADD_all();
        for term in ctx.term_all().iter().skip(1) {
            let right: Rc<Expression> = Self::extract_rc_expression_from_ove(self.visit(term.as_ref()));
            let op = operators.get(operator_index).unwrap();
            let binary_expr = if op.symbol.token_type == OPERATOR_ADD {
                BinaryData::add(left, right).to_rc_expr()
            } else if op.symbol.token_type == OPERATOR_SUB {
                BinaryData::sub(left, right).to_rc_expr()
            } else {
                self.add_error("Unknown adding operator".to_string());
                return None;
            };
            left = binary_expr;
            operator_index += 1;
        }
        return Some(VisitorElement::Expr(left));
    }

    fn visit_term(&mut self, ctx: &TermContext<'_>) -> Self::Return {
        let mut left: Rc<Expression> = Self::extract_rc_expression_from_ove(self.visit(&*ctx.factor(0).unwrap()));
        let mut operator_index = 0;
        let operators = ctx.OPERATOR_MUL_all();
        for term in ctx.factor_all().iter().skip(1) {
            let right: Rc<Expression> = Self::extract_rc_expression_from_ove(self.visit(term.as_ref()));
            let op = operators.get(operator_index).unwrap();
            let binary_expr = if op.symbol.token_type == OPERATOR_MUL {
                BinaryData::mul(left, right).to_rc_expr()
            } else if op.symbol.token_type == OPERATOR_DIV {
                BinaryData::div(left, right).to_rc_expr()
            } else {
                self.add_error("Unknown term operator".to_string());
                return None;
            };
            left = binary_expr;
            operator_index += 1;
        }
        return Some(VisitorElement::Expr(left));
    }

    fn visit_factor(&mut self, ctx: &FactorContext<'_>) -> Self::Return {
        // for a^b^c order is a^(b^c) -> iterate from the last to the first exponent
        let mut current: Option<Rc<Expression>> = None;
        for exp in ctx.exponent_all().iter().rev() {
            let exp_v: Rc<Expression> = Self::extract_rc_expression_from_ove(self.visit(exp.as_ref()));
            if let Some(current_v) = &current {
                current = Some(ExpData::new(exp_v, Rc::clone(current_v)).to_rc_expr());
            } else {
                // first expression
                current = Some(exp_v);
            }
        }

        if let Some(base) = &ctx.base {
            let base_v: Rc<Expression> = Self::extract_rc_expression_from_ove(self.visit(base.as_ref()));
            let result = if let Some(current_v) = &current {
                ExpData::new(base_v, Rc::clone(current_v)).to_rc_expr()
            } else {
                base_v
            };
            return Some(VisitorElement::Expr(result));
        }

        return None;
    }

    fn visit_exponentiation(&mut self, ctx: &ExponentiationContext<'_>) -> Self::Return {
        let is_negation = ctx.negation.is_some();
        if let Some(simp) = ctx.simple_factor() {
            let call_obj: Rc<Expression> = Self::extract_rc_expression_from_ove(self.visit(&*simp));
            let result = self.wrap_expr_with_methodinv(ctx, call_obj);
            let result = Self::wrap_expr_with_negation(result, is_negation);
            return Some(VisitorElement::Expr(result));
        }
        if let Some(frac) = ctx.fraction() {
            let call_obj: Rc<Expression> = Self::extract_rc_expression_from_ove(self.visit(&*frac));
            let result = self.wrap_expr_with_methodinv(ctx, call_obj);
            let result = Self::wrap_expr_with_negation(result, is_negation);
            return Some(VisitorElement::Expr(result));
        }
        self.add_error("Not implemented factor element".to_owned());
        return None;
    }

    fn visit_exponent(&mut self, ctx: &ExponentContext<'_>) -> Self::Return {
        if let Some(exp) = ctx.exponentiation() {
            return self.visit(&*exp);
        }
        return None;
    }

    fn visit_methodinv(&mut self, ctx: &matmparser::MethodinvContext<'_>) -> Self::Return {
        if let Some(func_v) = ctx.functioninv() {
            if let Some(id_v) = func_v.ID() {
                let func_name = id_v.get_text();
                let args: Vec<Rc<Expression>> = match func_v.arguments() {
                    Some(args_v) => Self::extract_vec_rc_expression_from_ove(self.visit(&*args_v)),
                    None => Vec::new(),
                };
                return Some(VisitorElement::Expr(FunctionData::new(
                    func_name,
                    args).to_rc_expr()));
            }
        }
        self.add_error("Unknown function invocation".to_owned());
        None
    }

    fn visit_simple_factor(&mut self, ctx: &Simple_factorContext<'_>) -> Self::Return {
        // LPAREN expr RPAREN
        if let Some(inner_expr) = ctx.expr() {
            return self.visit(&*inner_expr);
        }
        // functioninv
        if let Some(func_v) = ctx.functioninv() {
            return self.visit(&*func_v);
        }
        // number
        if let Some(number_v) = ctx.number() {
            return self.visit(&*number_v);
        }
        // symbol
        if let Some(symbol_v) = ctx.symbol() {
            return self.visit(&*symbol_v);
        }
        self.add_error("Unknown simple factor".to_owned());
        None
    }

    fn visit_fraction(&mut self, ctx: &matmparser::FractionContext<'_>) -> Self::Return {
        let num = Self::extract_rc_expression_from_ove(match &ctx.numerator {
            Some(num) => self.visit(&**num),
            None => {
                self.add_error("Unknown fraction numerator".to_owned());
                None
            },
        });
        let den = Self::extract_rc_expression_from_ove(match &ctx.denominator {
            Some(den) => self.visit(&**den),
            None => {
                self.add_error("Unknown fraction denominator".to_owned());
                None
            },
        });
        return Some(VisitorElement::Expr(FracData::new(num, den).to_rc_expr()));
    }

    fn visit_symbol(&mut self, ctx: &matmparser::SymbolContext<'_>) -> Self::Return {
        if let Some(id_v) = ctx.ID() {
            return Some(VisitorElement::Expr(SymbolData::new(&id_v.get_text()).to_rc_expr()));
        }
        self.add_error("Unknown symbol".to_owned());
        None
    }

    fn visit_functioninv(&mut self, ctx: &matmparser::FunctioninvContext<'_>) -> Self::Return {
        if let Some(id_v) = ctx.ID() {
            let func_name = &id_v.get_text();
            let args: Vec<Rc<Expression>> = match ctx.arguments() {
                Some(args_v) => Self::extract_vec_rc_expression_from_ove(self.visit(&*args_v)),
                None => Vec::new(),
            };
            return Some(VisitorElement::Expr(FunctionData::new(func_name.clone(), args).to_rc_expr()));
        }
        None
    }

    fn visit_arguments(&mut self, ctx: &matmparser::ArgumentsContext<'_>) -> Self::Return {
        let mut arguments: Vec<Rc<Expression>> = Vec::new();
        for arg in ctx.expr_all() {
            let e = Self::extract_rc_expression_from_ove(self.visit(&*arg));
            arguments.push(e);
        }
        return Some(VisitorElement::Script(arguments));
    }

    fn visit_assignment(&mut self, ctx: &matmparser::AssignmentContext<'_>) -> Self::Return {
        let target = Self::extract_rc_expression_from_ove(self.visit(&*ctx.symbol().unwrap()));
        let source = Self::extract_rc_expression_from_ove(self.visit(&*ctx.expr().unwrap()));
        let asgn = AssignmentData::new(target, source);
        return Some(VisitorElement::Expr(asgn.to_rc_expr()));
    }

    fn visit_statements(&mut self, ctx: &matmparser::StatementsContext<'_>) -> Self::Return {
        let mut expressions: Vec<Rc<Expression>> = Vec::new();
        for inner_expr in ctx.expr_all() {
            if let Some(item) = self.visit(&*inner_expr) {
                match item {
                    VisitorElement::Expr(e) => expressions.push(e),
                    _ => {}
                }
            }
        }
        return Some(VisitorElement::Script(expressions));
    }
}