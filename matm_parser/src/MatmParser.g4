parser grammar MatmParser;

options {
  tokenVocab = MatmLexer;
}

start @after {} : statements;

statements
    : expr (STATEMENT_END expr)* STATEMENT_END?
    ;

expr
    : equation
    | adding_expr
    | assignment
    ;

equation
    : left=adding_expr OPERATOR_EQ right=adding_expr
    ;

adding_expr
    : term ((OPERATOR_ADD | OPERATOR_SUB) term)*
    ;

term
    : factor ((OPERATOR_MUL|OPERATOR_DIV) factor)*
    ;

factor
    : base=exponentiation exponent*
    ;

exponentiation
    : negation=OPERATOR_SUB? (simple_factor | fraction) methodinv*
    ;

exponent
    : OPERATOR_POW exponentiation
    ;

simple_factor
    : LPAREN expr RPAREN
    | functioninv
    | number
    | symbol
    ;

fraction
    : numerator=simple_factor OPERATOR_FRAC denominator=simple_factor
    ;

symbol
    : ID
    ;

number
    : INT_DECIMAL
    | INT_RADIX_SINGLE
    | INT_RADIX_MULTIPLE
    ;

// method invocation
methodinv
    : DOT functioninv
    ;

// function invocation
functioninv
    : ID (LPAREN arguments RPAREN | LPAREN RPAREN)
    ;

arguments
    : expr (COMMA expr)*
    ;

assignment
    : symbol COLON expr
    ;
