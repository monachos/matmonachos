use std::cell::RefCell;
use std::rc::Rc;

use antlr::matmlexer::MatmLexer;
use antlr::matmparser::MatmParser;
use antlr_rust::common_token_stream::CommonTokenStream;
use antlr_rust::token_factory::CommonTokenFactory;
use antlr_rust::InputStream;
use antlr_rust::tree::ParseTreeVisitorCompat;
use antlr_rust::Parser;
use expression_visitor::ExpressionVisitor;
use expression_visitor::VisitorElement;
use matm_model::expressions::Expression;
use crate::errors::ErrorItem;
use crate::errors::ErrorItemSource;
use crate::errors::ErrorItemsConsumer;
use crate::errors::buffer::ErrorItemsBuffer;
use crate::errors::lexer::LexerErrorsListener;
use crate::errors::parser::ParserErrorsListener;

pub mod antlr;
pub mod expression_visitor;
mod expression_visitor_test;
mod listener_test;
pub mod errors;
mod asserts;

pub fn parse_and_run_visitor(
    input_text: &str,
    errors: &mut dyn ErrorItemsConsumer,
) -> Option<VisitorElement> {
    let error_listener = Rc::new(RefCell::new(ErrorItemsBuffer::new()));

    // create lexer
    let tf = CommonTokenFactory::default();
    let mut lexer = MatmLexer::new_with_token_factory(
        InputStream::new(input_text),
        &tf);
    let lexer_error_listener = Box::new(LexerErrorsListener::new(&error_listener));
    lexer.remove_error_listeners();
    lexer.add_error_listener(lexer_error_listener);
    let token_source = CommonTokenStream::new(lexer);

    // create parser
    let mut parser = MatmParser::new(token_source);
    let parser_error_listener = Box::new(ParserErrorsListener::new(&error_listener));
    parser.remove_error_listeners();
    parser.add_error_listener(parser_error_listener);

    let parsing_result = parser.start();
    match parsing_result {
        Ok(root) => {
            let mut visitor = ExpressionVisitor::new(Rc::clone(&error_listener));
            let visiting_result = visitor.visit(&*root);
            error_listener.borrow().copy_items(errors);
            return visiting_result;
        }
        Err(err) => {
            errors.add_error_item(ErrorItem {
                source: ErrorItemSource::Parser,
                line: 0,
                column: 0,
                message: err.to_string(),
            });
            return None;
        }
    }
}

pub fn parse_and_run_script_visitor(
    input_text: &str,
    errors: &mut dyn ErrorItemsConsumer,
) -> Vec<Rc<Expression>> {
    return match parse_and_run_visitor(input_text, errors) {
        Some(element) => {
            match element {
                VisitorElement::Expr(expr) => vec![expr],
                VisitorElement::Script(script) => script,
            }
        }
        None => Vec::new(),
    }
}
