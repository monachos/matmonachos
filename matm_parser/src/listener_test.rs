#[cfg(test)]
mod tests {
    use antlr_rust::InputStream;
    use antlr_rust::common_token_stream::CommonTokenStream;
    use antlr_rust::token_factory::CommonTokenFactory;
    use antlr_rust::tree::ParseTreeListener;
    use crate::antlr::matmlexer::MatmLexer;
    use crate::antlr::matmparser::MatmParser;
    use crate::antlr::matmparser::MatmParserContext;
    use crate::antlr::matmparser::MatmParserContextType;
    use crate::antlr::matmparserlistener::MatmParserListener;

    struct Listener<'a> {
        rules: &'a mut Vec<String>,
    }

    impl<'input> ParseTreeListener<'input, MatmParserContextType> for Listener<'input> {
        fn enter_every_rule(&mut self, ctx: &dyn MatmParserContext<'input>) {
            let rule_name = crate::antlr::matmparser::ruleNames
                    .get(ctx.get_rule_index())
                    .unwrap_or(&"error");
            println!("rule entered {}", rule_name);
            self.rules.push(rule_name.to_owned().to_owned());
        }
    }

    impl<'input> MatmParserListener<'input> for Listener<'input> {
    }

    // samples: https://github.com/rrevenantt/antlr4rust/blob/master/tests/general_tests.rs
    fn run_listener(text: &str, visited_rules: &mut Vec<String>) {
        let tf = CommonTokenFactory::default();
        let lexer = MatmLexer::new_with_token_factory(
            InputStream::new(text.into()), &tf);
        let token_source = CommonTokenStream::new(lexer);
        let mut parser = MatmParser::new(token_source);
        
        let listener = Box::new(Listener {
            rules: visited_rules
        });
        parser.add_parse_listener(listener);
        let result = parser.start();
        assert!(result.is_ok());
    }

    #[test]
    fn test_int() {
        let mut visited_rules: Vec<String> = Vec::new();
        run_listener("1234", &mut visited_rules);
        let mut it = visited_rules.iter();
        assert_eq!(it.next(), Some(&"start".to_owned()));
        assert_eq!(it.next(), Some(&"statements".to_owned()));
        assert_eq!(it.next(), Some(&"expr".to_owned()));
        assert_eq!(it.next(), Some(&"adding_expr".to_owned()));
        assert_eq!(it.next(), Some(&"term".to_owned()));
        assert_eq!(it.next(), Some(&"factor".to_owned()));
        assert_eq!(it.next(), Some(&"exponentiation".to_owned()));
        assert_eq!(it.next(), Some(&"simple_factor".to_owned()));
        assert_eq!(it.next(), Some(&"number".to_owned()));
        assert_eq!(it.next(), None);
    }

    #[test]
    fn test_int_radix_single() {
        let mut visited_rules: Vec<String> = Vec::new();
        run_listener("16#12F#", &mut visited_rules);
        let mut it = visited_rules.iter();
        assert_eq!(it.next(), Some(&"start".to_owned()));
        assert_eq!(it.next(), Some(&"statements".to_owned()));
        assert_eq!(it.next(), Some(&"expr".to_owned()));
        assert_eq!(it.next(), Some(&"adding_expr".to_owned()));
        assert_eq!(it.next(), Some(&"term".to_owned()));
        assert_eq!(it.next(), Some(&"factor".to_owned()));
        assert_eq!(it.next(), Some(&"exponentiation".to_owned()));
        assert_eq!(it.next(), Some(&"simple_factor".to_owned()));
        assert_eq!(it.next(), Some(&"number".to_owned()));
        assert_eq!(it.next(), None);
    }

    #[test]
    fn test_int_radix_multiple() {
        let mut visited_rules: Vec<String> = Vec::new();
        run_listener("60#(23)(10)#", &mut visited_rules);
        let mut it = visited_rules.iter();
        assert_eq!(it.next(), Some(&"start".to_owned()));
        assert_eq!(it.next(), Some(&"statements".to_owned()));
        assert_eq!(it.next(), Some(&"expr".to_owned()));
        assert_eq!(it.next(), Some(&"adding_expr".to_owned()));
        assert_eq!(it.next(), Some(&"term".to_owned()));
        assert_eq!(it.next(), Some(&"factor".to_owned()));
        assert_eq!(it.next(), Some(&"exponentiation".to_owned()));
        assert_eq!(it.next(), Some(&"simple_factor".to_owned()));
        assert_eq!(it.next(), Some(&"number".to_owned()));
        assert_eq!(it.next(), None);
    }

    #[test]
    fn test_add_ints() {
        let mut visited_rules: Vec<String> = Vec::new();
        run_listener("1 + 2", &mut visited_rules);
        let mut it = visited_rules.iter();
        assert_eq!(it.next(), Some(&"start".to_owned()));
        assert_eq!(it.next(), Some(&"statements".to_owned()));
        assert_eq!(it.next(), Some(&"expr".to_owned()));
        assert_eq!(it.next(), Some(&"adding_expr".to_owned()));
        assert_eq!(it.next(), Some(&"term".to_owned()));
        assert_eq!(it.next(), Some(&"factor".to_owned()));
        assert_eq!(it.next(), Some(&"exponentiation".to_owned()));
        assert_eq!(it.next(), Some(&"simple_factor".to_owned()));
        assert_eq!(it.next(), Some(&"number".to_owned()));
        assert_eq!(it.next(), Some(&"term".to_owned()));
        assert_eq!(it.next(), Some(&"factor".to_owned()));
        assert_eq!(it.next(), Some(&"exponentiation".to_owned()));
        assert_eq!(it.next(), Some(&"simple_factor".to_owned()));
        assert_eq!(it.next(), Some(&"number".to_owned()));
        assert_eq!(it.next(), None);
    }

    #[test]
    fn test_div_ints() {
        let mut visited_rules: Vec<String> = Vec::new();
        run_listener("1 div 2", &mut visited_rules);
        let mut it = visited_rules.iter();
        assert_eq!(it.next(), Some(&"start".to_owned()));
        assert_eq!(it.next(), Some(&"statements".to_owned()));
        assert_eq!(it.next(), Some(&"expr".to_owned()));
        assert_eq!(it.next(), Some(&"adding_expr".to_owned()));
        assert_eq!(it.next(), Some(&"term".to_owned()));
        assert_eq!(it.next(), Some(&"factor".to_owned()));
        assert_eq!(it.next(), Some(&"exponentiation".to_owned()));
        assert_eq!(it.next(), Some(&"simple_factor".to_owned()));
        assert_eq!(it.next(), Some(&"number".to_owned()));
        assert_eq!(it.next(), Some(&"factor".to_owned()));
        assert_eq!(it.next(), Some(&"exponentiation".to_owned()));
        assert_eq!(it.next(), Some(&"simple_factor".to_owned()));
        assert_eq!(it.next(), Some(&"number".to_owned()));
        assert_eq!(it.next(), None);
    }

    #[test]
    fn test_add_terms() {
        let mut visited_rules: Vec<String> = Vec::new();
        run_listener("1*2 + 3*4", &mut visited_rules);
        let mut it = visited_rules.iter();
        assert_eq!(it.next(), Some(&"start".to_owned()));
        assert_eq!(it.next(), Some(&"statements".to_owned()));
        assert_eq!(it.next(), Some(&"expr".to_owned()));
        assert_eq!(it.next(), Some(&"adding_expr".to_owned()));
        assert_eq!(it.next(), Some(&"term".to_owned()));
        assert_eq!(it.next(), Some(&"factor".to_owned()));
        assert_eq!(it.next(), Some(&"exponentiation".to_owned()));
        assert_eq!(it.next(), Some(&"simple_factor".to_owned()));
        assert_eq!(it.next(), Some(&"number".to_owned()));
        assert_eq!(it.next(), Some(&"factor".to_owned()));
        assert_eq!(it.next(), Some(&"exponentiation".to_owned()));
        assert_eq!(it.next(), Some(&"simple_factor".to_owned()));
        assert_eq!(it.next(), Some(&"number".to_owned()));
        assert_eq!(it.next(), Some(&"term".to_owned()));
        assert_eq!(it.next(), Some(&"factor".to_owned()));
        assert_eq!(it.next(), Some(&"exponentiation".to_owned()));
        assert_eq!(it.next(), Some(&"simple_factor".to_owned()));
        assert_eq!(it.next(), Some(&"number".to_owned()));
        assert_eq!(it.next(), Some(&"factor".to_owned()));
        assert_eq!(it.next(), Some(&"exponentiation".to_owned()));
        assert_eq!(it.next(), Some(&"simple_factor".to_owned()));
        assert_eq!(it.next(), Some(&"number".to_owned()));
        assert_eq!(it.next(), None);
    }

}
