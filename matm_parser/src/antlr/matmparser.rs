// Generated from MatmParser.g4 by ANTLR 4.8
#![allow(dead_code)]
#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]
#![allow(nonstandard_style)]
#![allow(unused_imports)]
#![allow(unused_mut)]
#![allow(unused_braces)]
use antlr_rust::PredictionContextCache;
use antlr_rust::parser::{Parser, BaseParser, ParserRecog, ParserNodeType};
use antlr_rust::token_stream::TokenStream;
use antlr_rust::TokenSource;
use antlr_rust::parser_atn_simulator::ParserATNSimulator;
use antlr_rust::errors::*;
use antlr_rust::rule_context::{BaseRuleContext, CustomRuleContext, RuleContext};
use antlr_rust::recognizer::{Recognizer,Actions};
use antlr_rust::atn_deserializer::ATNDeserializer;
use antlr_rust::dfa::DFA;
use antlr_rust::atn::{ATN, INVALID_ALT};
use antlr_rust::error_strategy::{ErrorStrategy, DefaultErrorStrategy};
use antlr_rust::parser_rule_context::{BaseParserRuleContext, ParserRuleContext,cast,cast_mut};
use antlr_rust::tree::*;
use antlr_rust::token::{TOKEN_EOF,OwningToken,Token};
use antlr_rust::int_stream::EOF;
use antlr_rust::vocabulary::{Vocabulary,VocabularyImpl};
use antlr_rust::token_factory::{CommonTokenFactory,TokenFactory, TokenAware};
use super::matmparserlistener::*;
use super::matmparservisitor::*;

use antlr_rust::lazy_static;
use antlr_rust::{TidAble,TidExt};

use std::marker::PhantomData;
use std::sync::Arc;
use std::rc::Rc;
use std::convert::TryFrom;
use std::cell::RefCell;
use std::ops::{DerefMut, Deref};
use std::borrow::{Borrow,BorrowMut};
use std::any::{Any,TypeId};

		pub const STATEMENT_END:isize=1; 
		pub const OPERATOR_EQ:isize=2; 
		pub const OPERATOR_POW:isize=3; 
		pub const OPERATOR_MUL:isize=4; 
		pub const OPERATOR_DIV:isize=5; 
		pub const OPERATOR_FRAC:isize=6; 
		pub const OPERATOR_ADD:isize=7; 
		pub const OPERATOR_SUB:isize=8; 
		pub const ID:isize=9; 
		pub const INT_DECIMAL:isize=10; 
		pub const INT_RADIX_SINGLE:isize=11; 
		pub const INT_RADIX_MULTIPLE:isize=12; 
		pub const WS:isize=13; 
		pub const LPAREN:isize=14; 
		pub const RPAREN:isize=15; 
		pub const COMMA:isize=16; 
		pub const DOT:isize=17; 
		pub const COLON:isize=18;
	pub const RULE_start:usize = 0; 
	pub const RULE_statements:usize = 1; 
	pub const RULE_expr:usize = 2; 
	pub const RULE_equation:usize = 3; 
	pub const RULE_adding_expr:usize = 4; 
	pub const RULE_term:usize = 5; 
	pub const RULE_factor:usize = 6; 
	pub const RULE_exponentiation:usize = 7; 
	pub const RULE_exponent:usize = 8; 
	pub const RULE_simple_factor:usize = 9; 
	pub const RULE_fraction:usize = 10; 
	pub const RULE_symbol:usize = 11; 
	pub const RULE_number:usize = 12; 
	pub const RULE_methodinv:usize = 13; 
	pub const RULE_functioninv:usize = 14; 
	pub const RULE_arguments:usize = 15; 
	pub const RULE_assignment:usize = 16;
	pub const ruleNames: [&'static str; 17] =  [
		"start", "statements", "expr", "equation", "adding_expr", "term", "factor", 
		"exponentiation", "exponent", "simple_factor", "fraction", "symbol", "number", 
		"methodinv", "functioninv", "arguments", "assignment"
	];


	pub const _LITERAL_NAMES: [Option<&'static str>;19] = [
		None, Some("';'"), Some("'='"), Some("'^'"), Some("'*'"), Some("'div'"), 
		Some("'/'"), Some("'+'"), Some("'-'"), None, None, None, None, None, Some("'('"), 
		Some("')'"), Some("','"), Some("'.'"), Some("':'")
	];
	pub const _SYMBOLIC_NAMES: [Option<&'static str>;19]  = [
		None, Some("STATEMENT_END"), Some("OPERATOR_EQ"), Some("OPERATOR_POW"), 
		Some("OPERATOR_MUL"), Some("OPERATOR_DIV"), Some("OPERATOR_FRAC"), Some("OPERATOR_ADD"), 
		Some("OPERATOR_SUB"), Some("ID"), Some("INT_DECIMAL"), Some("INT_RADIX_SINGLE"), 
		Some("INT_RADIX_MULTIPLE"), Some("WS"), Some("LPAREN"), Some("RPAREN"), 
		Some("COMMA"), Some("DOT"), Some("COLON")
	];
	lazy_static!{
	    static ref _shared_context_cache: Arc<PredictionContextCache> = Arc::new(PredictionContextCache::new());
		static ref VOCABULARY: Box<dyn Vocabulary> = Box::new(VocabularyImpl::new(_LITERAL_NAMES.iter(), _SYMBOLIC_NAMES.iter(), None));
	}


type BaseParserType<'input, I> =
	BaseParser<'input,MatmParserExt<'input>, I, MatmParserContextType , dyn MatmParserListener<'input> + 'input >;

type TokenType<'input> = <LocalTokenFactory<'input> as TokenFactory<'input>>::Tok;
pub type LocalTokenFactory<'input> = CommonTokenFactory;

pub type MatmParserTreeWalker<'input,'a> =
	ParseTreeWalker<'input, 'a, MatmParserContextType , dyn MatmParserListener<'input> + 'a>;

/// Parser for MatmParser grammar
pub struct MatmParser<'input,I,H>
where
    I: TokenStream<'input, TF = LocalTokenFactory<'input> > + TidAble<'input>,
    H: ErrorStrategy<'input,BaseParserType<'input,I>>
{
	base:BaseParserType<'input,I>,
	interpreter:Arc<ParserATNSimulator>,
	_shared_context_cache: Box<PredictionContextCache>,
    pub err_handler: H,
}

impl<'input, I, H> MatmParser<'input, I, H>
where
    I: TokenStream<'input, TF = LocalTokenFactory<'input> > + TidAble<'input>,
    H: ErrorStrategy<'input,BaseParserType<'input,I>>
{
	pub fn get_serialized_atn() -> &'static str { _serializedATN }

    pub fn set_error_strategy(&mut self, strategy: H) {
        self.err_handler = strategy
    }

    pub fn with_strategy(input: I, strategy: H) -> Self {
		antlr_rust::recognizer::check_version("0","3");
		let interpreter = Arc::new(ParserATNSimulator::new(
			_ATN.clone(),
			_decision_to_DFA.clone(),
			_shared_context_cache.clone(),
		));
		Self {
			base: BaseParser::new_base_parser(
				input,
				Arc::clone(&interpreter),
				MatmParserExt{
					_pd: Default::default(),
				}
			),
			interpreter,
            _shared_context_cache: Box::new(PredictionContextCache::new()),
            err_handler: strategy,
        }
    }

}

type DynStrategy<'input,I> = Box<dyn ErrorStrategy<'input,BaseParserType<'input,I>> + 'input>;

impl<'input, I> MatmParser<'input, I, DynStrategy<'input,I>>
where
    I: TokenStream<'input, TF = LocalTokenFactory<'input> > + TidAble<'input>,
{
    pub fn with_dyn_strategy(input: I) -> Self{
    	Self::with_strategy(input,Box::new(DefaultErrorStrategy::new()))
    }
}

impl<'input, I> MatmParser<'input, I, DefaultErrorStrategy<'input,MatmParserContextType>>
where
    I: TokenStream<'input, TF = LocalTokenFactory<'input> > + TidAble<'input>,
{
    pub fn new(input: I) -> Self{
    	Self::with_strategy(input,DefaultErrorStrategy::new())
    }
}

/// Trait for monomorphized trait object that corresponds to the nodes of parse tree generated for MatmParser
pub trait MatmParserContext<'input>:
	for<'x> Listenable<dyn MatmParserListener<'input> + 'x > + 
	for<'x> Visitable<dyn MatmParserVisitor<'input> + 'x > + 
	ParserRuleContext<'input, TF=LocalTokenFactory<'input>, Ctx=MatmParserContextType>
{}

antlr_rust::coerce_from!{ 'input : MatmParserContext<'input> }

impl<'input, 'x, T> VisitableDyn<T> for dyn MatmParserContext<'input> + 'input
where
    T: MatmParserVisitor<'input> + 'x,
{
    fn accept_dyn(&self, visitor: &mut T) {
        self.accept(visitor as &mut (dyn MatmParserVisitor<'input> + 'x))
    }
}

impl<'input> MatmParserContext<'input> for TerminalNode<'input,MatmParserContextType> {}
impl<'input> MatmParserContext<'input> for ErrorNode<'input,MatmParserContextType> {}

antlr_rust::tid! { impl<'input> TidAble<'input> for dyn MatmParserContext<'input> + 'input }

antlr_rust::tid! { impl<'input> TidAble<'input> for dyn MatmParserListener<'input> + 'input }

pub struct MatmParserContextType;
antlr_rust::tid!{MatmParserContextType}

impl<'input> ParserNodeType<'input> for MatmParserContextType{
	type TF = LocalTokenFactory<'input>;
	type Type = dyn MatmParserContext<'input> + 'input;
}

impl<'input, I, H> Deref for MatmParser<'input, I, H>
where
    I: TokenStream<'input, TF = LocalTokenFactory<'input> > + TidAble<'input>,
    H: ErrorStrategy<'input,BaseParserType<'input,I>>
{
    type Target = BaseParserType<'input,I>;

    fn deref(&self) -> &Self::Target {
        &self.base
    }
}

impl<'input, I, H> DerefMut for MatmParser<'input, I, H>
where
    I: TokenStream<'input, TF = LocalTokenFactory<'input> > + TidAble<'input>,
    H: ErrorStrategy<'input,BaseParserType<'input,I>>
{
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.base
    }
}

pub struct MatmParserExt<'input>{
	_pd: PhantomData<&'input str>,
}

impl<'input> MatmParserExt<'input>{
}
antlr_rust::tid! { MatmParserExt<'a> }

impl<'input> TokenAware<'input> for MatmParserExt<'input>{
	type TF = LocalTokenFactory<'input>;
}

impl<'input,I: TokenStream<'input, TF = LocalTokenFactory<'input> > + TidAble<'input>> ParserRecog<'input, BaseParserType<'input,I>> for MatmParserExt<'input>{}

impl<'input,I: TokenStream<'input, TF = LocalTokenFactory<'input> > + TidAble<'input>> Actions<'input, BaseParserType<'input,I>> for MatmParserExt<'input>{
	fn get_grammar_file_name(&self) -> & str{ "MatmParser.g4"}

   	fn get_rule_names(&self) -> &[& str] {&ruleNames}

   	fn get_vocabulary(&self) -> &dyn Vocabulary { &**VOCABULARY }
}
//------------------- start ----------------
pub type StartContextAll<'input> = StartContext<'input>;


pub type StartContext<'input> = BaseParserRuleContext<'input,StartContextExt<'input>>;

#[derive(Clone)]
pub struct StartContextExt<'input>{
ph:PhantomData<&'input str>
}

impl<'input> MatmParserContext<'input> for StartContext<'input>{}

impl<'input,'a> Listenable<dyn MatmParserListener<'input> + 'a> for StartContext<'input>{
		fn enter(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.enter_every_rule(self);
			listener.enter_start(self);
		}
		fn exit(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.exit_start(self);
			listener.exit_every_rule(self);
		}
}

impl<'input,'a> Visitable<dyn MatmParserVisitor<'input> + 'a> for StartContext<'input>{
	fn accept(&self,visitor: &mut (dyn MatmParserVisitor<'input> + 'a)) {
		visitor.visit_start(self);
	}
}

impl<'input> CustomRuleContext<'input> for StartContextExt<'input>{
	type TF = LocalTokenFactory<'input>;
	type Ctx = MatmParserContextType;
	fn get_rule_index(&self) -> usize { RULE_start }
	//fn type_rule_index() -> usize where Self: Sized { RULE_start }
}
antlr_rust::tid!{StartContextExt<'a>}

impl<'input> StartContextExt<'input>{
	fn new(parent: Option<Rc<dyn MatmParserContext<'input> + 'input > >, invoking_state: isize) -> Rc<StartContextAll<'input>> {
		Rc::new(
			BaseParserRuleContext::new_parser_ctx(parent, invoking_state,StartContextExt{
				ph:PhantomData
			}),
		)
	}
}

pub trait StartContextAttrs<'input>: MatmParserContext<'input> + BorrowMut<StartContextExt<'input>>{

fn statements(&self) -> Option<Rc<StatementsContextAll<'input>>> where Self:Sized{
	self.child_of_type(0)
}

}

impl<'input> StartContextAttrs<'input> for StartContext<'input>{}

impl<'input, I, H> MatmParser<'input, I, H>
where
    I: TokenStream<'input, TF = LocalTokenFactory<'input> > + TidAble<'input>,
    H: ErrorStrategy<'input,BaseParserType<'input,I>>
{
	pub fn start(&mut self,)
	-> Result<Rc<StartContextAll<'input>>,ANTLRError> {
		let mut recog = self;
		let _parentctx = recog.ctx.take();
		let mut _localctx = StartContextExt::new(_parentctx.clone(), recog.base.get_state());
        recog.base.enter_rule(_localctx.clone(), 0, RULE_start);
        let mut _localctx: Rc<StartContextAll> = _localctx;
		let result: Result<(), ANTLRError> = (|| {

			//recog.base.enter_outer_alt(_localctx.clone(), 1);
			recog.base.enter_outer_alt(None, 1);
			{
			/*InvokeRule statements*/
			recog.base.set_state(34);
			recog.statements()?;

			}
			let tmp = recog.input.lt(-1).cloned();
			recog.ctx.as_ref().unwrap().set_stop(tmp);
			Ok(())
		})();
		match result {
		Ok(_)=>{},
        Err(e @ ANTLRError::FallThrough(_)) => return Err(e),
		Err(ref re) => {
				//_localctx.exception = re;
				recog.err_handler.report_error(&mut recog.base, re);
				recog.err_handler.recover(&mut recog.base, re)?;
			}
		}
		recog.base.exit_rule();

		Ok(_localctx)
	}
}
//------------------- statements ----------------
pub type StatementsContextAll<'input> = StatementsContext<'input>;


pub type StatementsContext<'input> = BaseParserRuleContext<'input,StatementsContextExt<'input>>;

#[derive(Clone)]
pub struct StatementsContextExt<'input>{
ph:PhantomData<&'input str>
}

impl<'input> MatmParserContext<'input> for StatementsContext<'input>{}

impl<'input,'a> Listenable<dyn MatmParserListener<'input> + 'a> for StatementsContext<'input>{
		fn enter(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.enter_every_rule(self);
			listener.enter_statements(self);
		}
		fn exit(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.exit_statements(self);
			listener.exit_every_rule(self);
		}
}

impl<'input,'a> Visitable<dyn MatmParserVisitor<'input> + 'a> for StatementsContext<'input>{
	fn accept(&self,visitor: &mut (dyn MatmParserVisitor<'input> + 'a)) {
		visitor.visit_statements(self);
	}
}

impl<'input> CustomRuleContext<'input> for StatementsContextExt<'input>{
	type TF = LocalTokenFactory<'input>;
	type Ctx = MatmParserContextType;
	fn get_rule_index(&self) -> usize { RULE_statements }
	//fn type_rule_index() -> usize where Self: Sized { RULE_statements }
}
antlr_rust::tid!{StatementsContextExt<'a>}

impl<'input> StatementsContextExt<'input>{
	fn new(parent: Option<Rc<dyn MatmParserContext<'input> + 'input > >, invoking_state: isize) -> Rc<StatementsContextAll<'input>> {
		Rc::new(
			BaseParserRuleContext::new_parser_ctx(parent, invoking_state,StatementsContextExt{
				ph:PhantomData
			}),
		)
	}
}

pub trait StatementsContextAttrs<'input>: MatmParserContext<'input> + BorrowMut<StatementsContextExt<'input>>{

fn expr_all(&self) ->  Vec<Rc<ExprContextAll<'input>>> where Self:Sized{
	self.children_of_type()
}
fn expr(&self, i: usize) -> Option<Rc<ExprContextAll<'input>>> where Self:Sized{
	self.child_of_type(i)
}
/// Retrieves all `TerminalNode`s corresponding to token STATEMENT_END in current rule
fn STATEMENT_END_all(&self) -> Vec<Rc<TerminalNode<'input,MatmParserContextType>>>  where Self:Sized{
	self.children_of_type()
}
/// Retrieves 'i's TerminalNode corresponding to token STATEMENT_END, starting from 0.
/// Returns `None` if number of children corresponding to token STATEMENT_END is less or equal than `i`.
fn STATEMENT_END(&self, i: usize) -> Option<Rc<TerminalNode<'input,MatmParserContextType>>> where Self:Sized{
	self.get_token(STATEMENT_END, i)
}

}

impl<'input> StatementsContextAttrs<'input> for StatementsContext<'input>{}

impl<'input, I, H> MatmParser<'input, I, H>
where
    I: TokenStream<'input, TF = LocalTokenFactory<'input> > + TidAble<'input>,
    H: ErrorStrategy<'input,BaseParserType<'input,I>>
{
	pub fn statements(&mut self,)
	-> Result<Rc<StatementsContextAll<'input>>,ANTLRError> {
		let mut recog = self;
		let _parentctx = recog.ctx.take();
		let mut _localctx = StatementsContextExt::new(_parentctx.clone(), recog.base.get_state());
        recog.base.enter_rule(_localctx.clone(), 2, RULE_statements);
        let mut _localctx: Rc<StatementsContextAll> = _localctx;
		let mut _la: isize = -1;
		let result: Result<(), ANTLRError> = (|| {

			let mut _alt: isize;
			//recog.base.enter_outer_alt(_localctx.clone(), 1);
			recog.base.enter_outer_alt(None, 1);
			{
			/*InvokeRule expr*/
			recog.base.set_state(36);
			recog.expr()?;

			recog.base.set_state(41);
			recog.err_handler.sync(&mut recog.base)?;
			_alt = recog.interpreter.adaptive_predict(0,&mut recog.base)?;
			while { _alt!=2 && _alt!=INVALID_ALT } {
				if _alt==1 {
					{
					{
					recog.base.set_state(37);
					recog.base.match_token(STATEMENT_END,&mut recog.err_handler)?;

					/*InvokeRule expr*/
					recog.base.set_state(38);
					recog.expr()?;

					}
					} 
				}
				recog.base.set_state(43);
				recog.err_handler.sync(&mut recog.base)?;
				_alt = recog.interpreter.adaptive_predict(0,&mut recog.base)?;
			}
			recog.base.set_state(45);
			recog.err_handler.sync(&mut recog.base)?;
			_la = recog.base.input.la(1);
			if _la==STATEMENT_END {
				{
				recog.base.set_state(44);
				recog.base.match_token(STATEMENT_END,&mut recog.err_handler)?;

				}
			}

			}
			Ok(())
		})();
		match result {
		Ok(_)=>{},
        Err(e @ ANTLRError::FallThrough(_)) => return Err(e),
		Err(ref re) => {
				//_localctx.exception = re;
				recog.err_handler.report_error(&mut recog.base, re);
				recog.err_handler.recover(&mut recog.base, re)?;
			}
		}
		recog.base.exit_rule();

		Ok(_localctx)
	}
}
//------------------- expr ----------------
pub type ExprContextAll<'input> = ExprContext<'input>;


pub type ExprContext<'input> = BaseParserRuleContext<'input,ExprContextExt<'input>>;

#[derive(Clone)]
pub struct ExprContextExt<'input>{
ph:PhantomData<&'input str>
}

impl<'input> MatmParserContext<'input> for ExprContext<'input>{}

impl<'input,'a> Listenable<dyn MatmParserListener<'input> + 'a> for ExprContext<'input>{
		fn enter(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.enter_every_rule(self);
			listener.enter_expr(self);
		}
		fn exit(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.exit_expr(self);
			listener.exit_every_rule(self);
		}
}

impl<'input,'a> Visitable<dyn MatmParserVisitor<'input> + 'a> for ExprContext<'input>{
	fn accept(&self,visitor: &mut (dyn MatmParserVisitor<'input> + 'a)) {
		visitor.visit_expr(self);
	}
}

impl<'input> CustomRuleContext<'input> for ExprContextExt<'input>{
	type TF = LocalTokenFactory<'input>;
	type Ctx = MatmParserContextType;
	fn get_rule_index(&self) -> usize { RULE_expr }
	//fn type_rule_index() -> usize where Self: Sized { RULE_expr }
}
antlr_rust::tid!{ExprContextExt<'a>}

impl<'input> ExprContextExt<'input>{
	fn new(parent: Option<Rc<dyn MatmParserContext<'input> + 'input > >, invoking_state: isize) -> Rc<ExprContextAll<'input>> {
		Rc::new(
			BaseParserRuleContext::new_parser_ctx(parent, invoking_state,ExprContextExt{
				ph:PhantomData
			}),
		)
	}
}

pub trait ExprContextAttrs<'input>: MatmParserContext<'input> + BorrowMut<ExprContextExt<'input>>{

fn equation(&self) -> Option<Rc<EquationContextAll<'input>>> where Self:Sized{
	self.child_of_type(0)
}
fn adding_expr(&self) -> Option<Rc<Adding_exprContextAll<'input>>> where Self:Sized{
	self.child_of_type(0)
}
fn assignment(&self) -> Option<Rc<AssignmentContextAll<'input>>> where Self:Sized{
	self.child_of_type(0)
}

}

impl<'input> ExprContextAttrs<'input> for ExprContext<'input>{}

impl<'input, I, H> MatmParser<'input, I, H>
where
    I: TokenStream<'input, TF = LocalTokenFactory<'input> > + TidAble<'input>,
    H: ErrorStrategy<'input,BaseParserType<'input,I>>
{
	pub fn expr(&mut self,)
	-> Result<Rc<ExprContextAll<'input>>,ANTLRError> {
		let mut recog = self;
		let _parentctx = recog.ctx.take();
		let mut _localctx = ExprContextExt::new(_parentctx.clone(), recog.base.get_state());
        recog.base.enter_rule(_localctx.clone(), 4, RULE_expr);
        let mut _localctx: Rc<ExprContextAll> = _localctx;
		let result: Result<(), ANTLRError> = (|| {

			recog.base.set_state(50);
			recog.err_handler.sync(&mut recog.base)?;
			match  recog.interpreter.adaptive_predict(2,&mut recog.base)? {
				1 =>{
					//recog.base.enter_outer_alt(_localctx.clone(), 1);
					recog.base.enter_outer_alt(None, 1);
					{
					/*InvokeRule equation*/
					recog.base.set_state(47);
					recog.equation()?;

					}
				}
			,
				2 =>{
					//recog.base.enter_outer_alt(_localctx.clone(), 2);
					recog.base.enter_outer_alt(None, 2);
					{
					/*InvokeRule adding_expr*/
					recog.base.set_state(48);
					recog.adding_expr()?;

					}
				}
			,
				3 =>{
					//recog.base.enter_outer_alt(_localctx.clone(), 3);
					recog.base.enter_outer_alt(None, 3);
					{
					/*InvokeRule assignment*/
					recog.base.set_state(49);
					recog.assignment()?;

					}
				}

				_ => {}
			}
			Ok(())
		})();
		match result {
		Ok(_)=>{},
        Err(e @ ANTLRError::FallThrough(_)) => return Err(e),
		Err(ref re) => {
				//_localctx.exception = re;
				recog.err_handler.report_error(&mut recog.base, re);
				recog.err_handler.recover(&mut recog.base, re)?;
			}
		}
		recog.base.exit_rule();

		Ok(_localctx)
	}
}
//------------------- equation ----------------
pub type EquationContextAll<'input> = EquationContext<'input>;


pub type EquationContext<'input> = BaseParserRuleContext<'input,EquationContextExt<'input>>;

#[derive(Clone)]
pub struct EquationContextExt<'input>{
	pub left: Option<Rc<Adding_exprContextAll<'input>>>,
	pub right: Option<Rc<Adding_exprContextAll<'input>>>,
ph:PhantomData<&'input str>
}

impl<'input> MatmParserContext<'input> for EquationContext<'input>{}

impl<'input,'a> Listenable<dyn MatmParserListener<'input> + 'a> for EquationContext<'input>{
		fn enter(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.enter_every_rule(self);
			listener.enter_equation(self);
		}
		fn exit(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.exit_equation(self);
			listener.exit_every_rule(self);
		}
}

impl<'input,'a> Visitable<dyn MatmParserVisitor<'input> + 'a> for EquationContext<'input>{
	fn accept(&self,visitor: &mut (dyn MatmParserVisitor<'input> + 'a)) {
		visitor.visit_equation(self);
	}
}

impl<'input> CustomRuleContext<'input> for EquationContextExt<'input>{
	type TF = LocalTokenFactory<'input>;
	type Ctx = MatmParserContextType;
	fn get_rule_index(&self) -> usize { RULE_equation }
	//fn type_rule_index() -> usize where Self: Sized { RULE_equation }
}
antlr_rust::tid!{EquationContextExt<'a>}

impl<'input> EquationContextExt<'input>{
	fn new(parent: Option<Rc<dyn MatmParserContext<'input> + 'input > >, invoking_state: isize) -> Rc<EquationContextAll<'input>> {
		Rc::new(
			BaseParserRuleContext::new_parser_ctx(parent, invoking_state,EquationContextExt{
				left: None, right: None, 
				ph:PhantomData
			}),
		)
	}
}

pub trait EquationContextAttrs<'input>: MatmParserContext<'input> + BorrowMut<EquationContextExt<'input>>{

/// Retrieves first TerminalNode corresponding to token OPERATOR_EQ
/// Returns `None` if there is no child corresponding to token OPERATOR_EQ
fn OPERATOR_EQ(&self) -> Option<Rc<TerminalNode<'input,MatmParserContextType>>> where Self:Sized{
	self.get_token(OPERATOR_EQ, 0)
}
fn adding_expr_all(&self) ->  Vec<Rc<Adding_exprContextAll<'input>>> where Self:Sized{
	self.children_of_type()
}
fn adding_expr(&self, i: usize) -> Option<Rc<Adding_exprContextAll<'input>>> where Self:Sized{
	self.child_of_type(i)
}

}

impl<'input> EquationContextAttrs<'input> for EquationContext<'input>{}

impl<'input, I, H> MatmParser<'input, I, H>
where
    I: TokenStream<'input, TF = LocalTokenFactory<'input> > + TidAble<'input>,
    H: ErrorStrategy<'input,BaseParserType<'input,I>>
{
	pub fn equation(&mut self,)
	-> Result<Rc<EquationContextAll<'input>>,ANTLRError> {
		let mut recog = self;
		let _parentctx = recog.ctx.take();
		let mut _localctx = EquationContextExt::new(_parentctx.clone(), recog.base.get_state());
        recog.base.enter_rule(_localctx.clone(), 6, RULE_equation);
        let mut _localctx: Rc<EquationContextAll> = _localctx;
		let result: Result<(), ANTLRError> = (|| {

			//recog.base.enter_outer_alt(_localctx.clone(), 1);
			recog.base.enter_outer_alt(None, 1);
			{
			/*InvokeRule adding_expr*/
			recog.base.set_state(52);
			let tmp = recog.adding_expr()?;
			 cast_mut::<_,EquationContext >(&mut _localctx).left = Some(tmp.clone());
			  

			recog.base.set_state(53);
			recog.base.match_token(OPERATOR_EQ,&mut recog.err_handler)?;

			/*InvokeRule adding_expr*/
			recog.base.set_state(54);
			let tmp = recog.adding_expr()?;
			 cast_mut::<_,EquationContext >(&mut _localctx).right = Some(tmp.clone());
			  

			}
			Ok(())
		})();
		match result {
		Ok(_)=>{},
        Err(e @ ANTLRError::FallThrough(_)) => return Err(e),
		Err(ref re) => {
				//_localctx.exception = re;
				recog.err_handler.report_error(&mut recog.base, re);
				recog.err_handler.recover(&mut recog.base, re)?;
			}
		}
		recog.base.exit_rule();

		Ok(_localctx)
	}
}
//------------------- adding_expr ----------------
pub type Adding_exprContextAll<'input> = Adding_exprContext<'input>;


pub type Adding_exprContext<'input> = BaseParserRuleContext<'input,Adding_exprContextExt<'input>>;

#[derive(Clone)]
pub struct Adding_exprContextExt<'input>{
ph:PhantomData<&'input str>
}

impl<'input> MatmParserContext<'input> for Adding_exprContext<'input>{}

impl<'input,'a> Listenable<dyn MatmParserListener<'input> + 'a> for Adding_exprContext<'input>{
		fn enter(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.enter_every_rule(self);
			listener.enter_adding_expr(self);
		}
		fn exit(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.exit_adding_expr(self);
			listener.exit_every_rule(self);
		}
}

impl<'input,'a> Visitable<dyn MatmParserVisitor<'input> + 'a> for Adding_exprContext<'input>{
	fn accept(&self,visitor: &mut (dyn MatmParserVisitor<'input> + 'a)) {
		visitor.visit_adding_expr(self);
	}
}

impl<'input> CustomRuleContext<'input> for Adding_exprContextExt<'input>{
	type TF = LocalTokenFactory<'input>;
	type Ctx = MatmParserContextType;
	fn get_rule_index(&self) -> usize { RULE_adding_expr }
	//fn type_rule_index() -> usize where Self: Sized { RULE_adding_expr }
}
antlr_rust::tid!{Adding_exprContextExt<'a>}

impl<'input> Adding_exprContextExt<'input>{
	fn new(parent: Option<Rc<dyn MatmParserContext<'input> + 'input > >, invoking_state: isize) -> Rc<Adding_exprContextAll<'input>> {
		Rc::new(
			BaseParserRuleContext::new_parser_ctx(parent, invoking_state,Adding_exprContextExt{
				ph:PhantomData
			}),
		)
	}
}

pub trait Adding_exprContextAttrs<'input>: MatmParserContext<'input> + BorrowMut<Adding_exprContextExt<'input>>{

fn term_all(&self) ->  Vec<Rc<TermContextAll<'input>>> where Self:Sized{
	self.children_of_type()
}
fn term(&self, i: usize) -> Option<Rc<TermContextAll<'input>>> where Self:Sized{
	self.child_of_type(i)
}
/// Retrieves all `TerminalNode`s corresponding to token OPERATOR_ADD in current rule
fn OPERATOR_ADD_all(&self) -> Vec<Rc<TerminalNode<'input,MatmParserContextType>>>  where Self:Sized{
	self.children_of_type()
}
/// Retrieves 'i's TerminalNode corresponding to token OPERATOR_ADD, starting from 0.
/// Returns `None` if number of children corresponding to token OPERATOR_ADD is less or equal than `i`.
fn OPERATOR_ADD(&self, i: usize) -> Option<Rc<TerminalNode<'input,MatmParserContextType>>> where Self:Sized{
	self.get_token(OPERATOR_ADD, i)
}
/// Retrieves all `TerminalNode`s corresponding to token OPERATOR_SUB in current rule
fn OPERATOR_SUB_all(&self) -> Vec<Rc<TerminalNode<'input,MatmParserContextType>>>  where Self:Sized{
	self.children_of_type()
}
/// Retrieves 'i's TerminalNode corresponding to token OPERATOR_SUB, starting from 0.
/// Returns `None` if number of children corresponding to token OPERATOR_SUB is less or equal than `i`.
fn OPERATOR_SUB(&self, i: usize) -> Option<Rc<TerminalNode<'input,MatmParserContextType>>> where Self:Sized{
	self.get_token(OPERATOR_SUB, i)
}

}

impl<'input> Adding_exprContextAttrs<'input> for Adding_exprContext<'input>{}

impl<'input, I, H> MatmParser<'input, I, H>
where
    I: TokenStream<'input, TF = LocalTokenFactory<'input> > + TidAble<'input>,
    H: ErrorStrategy<'input,BaseParserType<'input,I>>
{
	pub fn adding_expr(&mut self,)
	-> Result<Rc<Adding_exprContextAll<'input>>,ANTLRError> {
		let mut recog = self;
		let _parentctx = recog.ctx.take();
		let mut _localctx = Adding_exprContextExt::new(_parentctx.clone(), recog.base.get_state());
        recog.base.enter_rule(_localctx.clone(), 8, RULE_adding_expr);
        let mut _localctx: Rc<Adding_exprContextAll> = _localctx;
		let mut _la: isize = -1;
		let result: Result<(), ANTLRError> = (|| {

			//recog.base.enter_outer_alt(_localctx.clone(), 1);
			recog.base.enter_outer_alt(None, 1);
			{
			/*InvokeRule term*/
			recog.base.set_state(56);
			recog.term()?;

			recog.base.set_state(61);
			recog.err_handler.sync(&mut recog.base)?;
			_la = recog.base.input.la(1);
			while _la==OPERATOR_ADD || _la==OPERATOR_SUB {
				{
				{
				recog.base.set_state(57);
				_la = recog.base.input.la(1);
				if { !(_la==OPERATOR_ADD || _la==OPERATOR_SUB) } {
					recog.err_handler.recover_inline(&mut recog.base)?;

				}
				else {
					if  recog.base.input.la(1)==TOKEN_EOF { recog.base.matched_eof = true };
					recog.err_handler.report_match(&mut recog.base);
					recog.base.consume(&mut recog.err_handler);
				}
				/*InvokeRule term*/
				recog.base.set_state(58);
				recog.term()?;

				}
				}
				recog.base.set_state(63);
				recog.err_handler.sync(&mut recog.base)?;
				_la = recog.base.input.la(1);
			}
			}
			Ok(())
		})();
		match result {
		Ok(_)=>{},
        Err(e @ ANTLRError::FallThrough(_)) => return Err(e),
		Err(ref re) => {
				//_localctx.exception = re;
				recog.err_handler.report_error(&mut recog.base, re);
				recog.err_handler.recover(&mut recog.base, re)?;
			}
		}
		recog.base.exit_rule();

		Ok(_localctx)
	}
}
//------------------- term ----------------
pub type TermContextAll<'input> = TermContext<'input>;


pub type TermContext<'input> = BaseParserRuleContext<'input,TermContextExt<'input>>;

#[derive(Clone)]
pub struct TermContextExt<'input>{
ph:PhantomData<&'input str>
}

impl<'input> MatmParserContext<'input> for TermContext<'input>{}

impl<'input,'a> Listenable<dyn MatmParserListener<'input> + 'a> for TermContext<'input>{
		fn enter(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.enter_every_rule(self);
			listener.enter_term(self);
		}
		fn exit(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.exit_term(self);
			listener.exit_every_rule(self);
		}
}

impl<'input,'a> Visitable<dyn MatmParserVisitor<'input> + 'a> for TermContext<'input>{
	fn accept(&self,visitor: &mut (dyn MatmParserVisitor<'input> + 'a)) {
		visitor.visit_term(self);
	}
}

impl<'input> CustomRuleContext<'input> for TermContextExt<'input>{
	type TF = LocalTokenFactory<'input>;
	type Ctx = MatmParserContextType;
	fn get_rule_index(&self) -> usize { RULE_term }
	//fn type_rule_index() -> usize where Self: Sized { RULE_term }
}
antlr_rust::tid!{TermContextExt<'a>}

impl<'input> TermContextExt<'input>{
	fn new(parent: Option<Rc<dyn MatmParserContext<'input> + 'input > >, invoking_state: isize) -> Rc<TermContextAll<'input>> {
		Rc::new(
			BaseParserRuleContext::new_parser_ctx(parent, invoking_state,TermContextExt{
				ph:PhantomData
			}),
		)
	}
}

pub trait TermContextAttrs<'input>: MatmParserContext<'input> + BorrowMut<TermContextExt<'input>>{

fn factor_all(&self) ->  Vec<Rc<FactorContextAll<'input>>> where Self:Sized{
	self.children_of_type()
}
fn factor(&self, i: usize) -> Option<Rc<FactorContextAll<'input>>> where Self:Sized{
	self.child_of_type(i)
}
/// Retrieves all `TerminalNode`s corresponding to token OPERATOR_MUL in current rule
fn OPERATOR_MUL_all(&self) -> Vec<Rc<TerminalNode<'input,MatmParserContextType>>>  where Self:Sized{
	self.children_of_type()
}
/// Retrieves 'i's TerminalNode corresponding to token OPERATOR_MUL, starting from 0.
/// Returns `None` if number of children corresponding to token OPERATOR_MUL is less or equal than `i`.
fn OPERATOR_MUL(&self, i: usize) -> Option<Rc<TerminalNode<'input,MatmParserContextType>>> where Self:Sized{
	self.get_token(OPERATOR_MUL, i)
}
/// Retrieves all `TerminalNode`s corresponding to token OPERATOR_DIV in current rule
fn OPERATOR_DIV_all(&self) -> Vec<Rc<TerminalNode<'input,MatmParserContextType>>>  where Self:Sized{
	self.children_of_type()
}
/// Retrieves 'i's TerminalNode corresponding to token OPERATOR_DIV, starting from 0.
/// Returns `None` if number of children corresponding to token OPERATOR_DIV is less or equal than `i`.
fn OPERATOR_DIV(&self, i: usize) -> Option<Rc<TerminalNode<'input,MatmParserContextType>>> where Self:Sized{
	self.get_token(OPERATOR_DIV, i)
}

}

impl<'input> TermContextAttrs<'input> for TermContext<'input>{}

impl<'input, I, H> MatmParser<'input, I, H>
where
    I: TokenStream<'input, TF = LocalTokenFactory<'input> > + TidAble<'input>,
    H: ErrorStrategy<'input,BaseParserType<'input,I>>
{
	pub fn term(&mut self,)
	-> Result<Rc<TermContextAll<'input>>,ANTLRError> {
		let mut recog = self;
		let _parentctx = recog.ctx.take();
		let mut _localctx = TermContextExt::new(_parentctx.clone(), recog.base.get_state());
        recog.base.enter_rule(_localctx.clone(), 10, RULE_term);
        let mut _localctx: Rc<TermContextAll> = _localctx;
		let mut _la: isize = -1;
		let result: Result<(), ANTLRError> = (|| {

			//recog.base.enter_outer_alt(_localctx.clone(), 1);
			recog.base.enter_outer_alt(None, 1);
			{
			/*InvokeRule factor*/
			recog.base.set_state(64);
			recog.factor()?;

			recog.base.set_state(69);
			recog.err_handler.sync(&mut recog.base)?;
			_la = recog.base.input.la(1);
			while _la==OPERATOR_MUL || _la==OPERATOR_DIV {
				{
				{
				recog.base.set_state(65);
				_la = recog.base.input.la(1);
				if { !(_la==OPERATOR_MUL || _la==OPERATOR_DIV) } {
					recog.err_handler.recover_inline(&mut recog.base)?;

				}
				else {
					if  recog.base.input.la(1)==TOKEN_EOF { recog.base.matched_eof = true };
					recog.err_handler.report_match(&mut recog.base);
					recog.base.consume(&mut recog.err_handler);
				}
				/*InvokeRule factor*/
				recog.base.set_state(66);
				recog.factor()?;

				}
				}
				recog.base.set_state(71);
				recog.err_handler.sync(&mut recog.base)?;
				_la = recog.base.input.la(1);
			}
			}
			Ok(())
		})();
		match result {
		Ok(_)=>{},
        Err(e @ ANTLRError::FallThrough(_)) => return Err(e),
		Err(ref re) => {
				//_localctx.exception = re;
				recog.err_handler.report_error(&mut recog.base, re);
				recog.err_handler.recover(&mut recog.base, re)?;
			}
		}
		recog.base.exit_rule();

		Ok(_localctx)
	}
}
//------------------- factor ----------------
pub type FactorContextAll<'input> = FactorContext<'input>;


pub type FactorContext<'input> = BaseParserRuleContext<'input,FactorContextExt<'input>>;

#[derive(Clone)]
pub struct FactorContextExt<'input>{
	pub base: Option<Rc<ExponentiationContextAll<'input>>>,
ph:PhantomData<&'input str>
}

impl<'input> MatmParserContext<'input> for FactorContext<'input>{}

impl<'input,'a> Listenable<dyn MatmParserListener<'input> + 'a> for FactorContext<'input>{
		fn enter(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.enter_every_rule(self);
			listener.enter_factor(self);
		}
		fn exit(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.exit_factor(self);
			listener.exit_every_rule(self);
		}
}

impl<'input,'a> Visitable<dyn MatmParserVisitor<'input> + 'a> for FactorContext<'input>{
	fn accept(&self,visitor: &mut (dyn MatmParserVisitor<'input> + 'a)) {
		visitor.visit_factor(self);
	}
}

impl<'input> CustomRuleContext<'input> for FactorContextExt<'input>{
	type TF = LocalTokenFactory<'input>;
	type Ctx = MatmParserContextType;
	fn get_rule_index(&self) -> usize { RULE_factor }
	//fn type_rule_index() -> usize where Self: Sized { RULE_factor }
}
antlr_rust::tid!{FactorContextExt<'a>}

impl<'input> FactorContextExt<'input>{
	fn new(parent: Option<Rc<dyn MatmParserContext<'input> + 'input > >, invoking_state: isize) -> Rc<FactorContextAll<'input>> {
		Rc::new(
			BaseParserRuleContext::new_parser_ctx(parent, invoking_state,FactorContextExt{
				base: None, 
				ph:PhantomData
			}),
		)
	}
}

pub trait FactorContextAttrs<'input>: MatmParserContext<'input> + BorrowMut<FactorContextExt<'input>>{

fn exponentiation(&self) -> Option<Rc<ExponentiationContextAll<'input>>> where Self:Sized{
	self.child_of_type(0)
}
fn exponent_all(&self) ->  Vec<Rc<ExponentContextAll<'input>>> where Self:Sized{
	self.children_of_type()
}
fn exponent(&self, i: usize) -> Option<Rc<ExponentContextAll<'input>>> where Self:Sized{
	self.child_of_type(i)
}

}

impl<'input> FactorContextAttrs<'input> for FactorContext<'input>{}

impl<'input, I, H> MatmParser<'input, I, H>
where
    I: TokenStream<'input, TF = LocalTokenFactory<'input> > + TidAble<'input>,
    H: ErrorStrategy<'input,BaseParserType<'input,I>>
{
	pub fn factor(&mut self,)
	-> Result<Rc<FactorContextAll<'input>>,ANTLRError> {
		let mut recog = self;
		let _parentctx = recog.ctx.take();
		let mut _localctx = FactorContextExt::new(_parentctx.clone(), recog.base.get_state());
        recog.base.enter_rule(_localctx.clone(), 12, RULE_factor);
        let mut _localctx: Rc<FactorContextAll> = _localctx;
		let mut _la: isize = -1;
		let result: Result<(), ANTLRError> = (|| {

			//recog.base.enter_outer_alt(_localctx.clone(), 1);
			recog.base.enter_outer_alt(None, 1);
			{
			/*InvokeRule exponentiation*/
			recog.base.set_state(72);
			let tmp = recog.exponentiation()?;
			 cast_mut::<_,FactorContext >(&mut _localctx).base = Some(tmp.clone());
			  

			recog.base.set_state(76);
			recog.err_handler.sync(&mut recog.base)?;
			_la = recog.base.input.la(1);
			while _la==OPERATOR_POW {
				{
				{
				/*InvokeRule exponent*/
				recog.base.set_state(73);
				recog.exponent()?;

				}
				}
				recog.base.set_state(78);
				recog.err_handler.sync(&mut recog.base)?;
				_la = recog.base.input.la(1);
			}
			}
			Ok(())
		})();
		match result {
		Ok(_)=>{},
        Err(e @ ANTLRError::FallThrough(_)) => return Err(e),
		Err(ref re) => {
				//_localctx.exception = re;
				recog.err_handler.report_error(&mut recog.base, re);
				recog.err_handler.recover(&mut recog.base, re)?;
			}
		}
		recog.base.exit_rule();

		Ok(_localctx)
	}
}
//------------------- exponentiation ----------------
pub type ExponentiationContextAll<'input> = ExponentiationContext<'input>;


pub type ExponentiationContext<'input> = BaseParserRuleContext<'input,ExponentiationContextExt<'input>>;

#[derive(Clone)]
pub struct ExponentiationContextExt<'input>{
	pub negation: Option<TokenType<'input>>,
ph:PhantomData<&'input str>
}

impl<'input> MatmParserContext<'input> for ExponentiationContext<'input>{}

impl<'input,'a> Listenable<dyn MatmParserListener<'input> + 'a> for ExponentiationContext<'input>{
		fn enter(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.enter_every_rule(self);
			listener.enter_exponentiation(self);
		}
		fn exit(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.exit_exponentiation(self);
			listener.exit_every_rule(self);
		}
}

impl<'input,'a> Visitable<dyn MatmParserVisitor<'input> + 'a> for ExponentiationContext<'input>{
	fn accept(&self,visitor: &mut (dyn MatmParserVisitor<'input> + 'a)) {
		visitor.visit_exponentiation(self);
	}
}

impl<'input> CustomRuleContext<'input> for ExponentiationContextExt<'input>{
	type TF = LocalTokenFactory<'input>;
	type Ctx = MatmParserContextType;
	fn get_rule_index(&self) -> usize { RULE_exponentiation }
	//fn type_rule_index() -> usize where Self: Sized { RULE_exponentiation }
}
antlr_rust::tid!{ExponentiationContextExt<'a>}

impl<'input> ExponentiationContextExt<'input>{
	fn new(parent: Option<Rc<dyn MatmParserContext<'input> + 'input > >, invoking_state: isize) -> Rc<ExponentiationContextAll<'input>> {
		Rc::new(
			BaseParserRuleContext::new_parser_ctx(parent, invoking_state,ExponentiationContextExt{
				negation: None, 
				ph:PhantomData
			}),
		)
	}
}

pub trait ExponentiationContextAttrs<'input>: MatmParserContext<'input> + BorrowMut<ExponentiationContextExt<'input>>{

fn simple_factor(&self) -> Option<Rc<Simple_factorContextAll<'input>>> where Self:Sized{
	self.child_of_type(0)
}
fn fraction(&self) -> Option<Rc<FractionContextAll<'input>>> where Self:Sized{
	self.child_of_type(0)
}
fn methodinv_all(&self) ->  Vec<Rc<MethodinvContextAll<'input>>> where Self:Sized{
	self.children_of_type()
}
fn methodinv(&self, i: usize) -> Option<Rc<MethodinvContextAll<'input>>> where Self:Sized{
	self.child_of_type(i)
}
/// Retrieves first TerminalNode corresponding to token OPERATOR_SUB
/// Returns `None` if there is no child corresponding to token OPERATOR_SUB
fn OPERATOR_SUB(&self) -> Option<Rc<TerminalNode<'input,MatmParserContextType>>> where Self:Sized{
	self.get_token(OPERATOR_SUB, 0)
}

}

impl<'input> ExponentiationContextAttrs<'input> for ExponentiationContext<'input>{}

impl<'input, I, H> MatmParser<'input, I, H>
where
    I: TokenStream<'input, TF = LocalTokenFactory<'input> > + TidAble<'input>,
    H: ErrorStrategy<'input,BaseParserType<'input,I>>
{
	pub fn exponentiation(&mut self,)
	-> Result<Rc<ExponentiationContextAll<'input>>,ANTLRError> {
		let mut recog = self;
		let _parentctx = recog.ctx.take();
		let mut _localctx = ExponentiationContextExt::new(_parentctx.clone(), recog.base.get_state());
        recog.base.enter_rule(_localctx.clone(), 14, RULE_exponentiation);
        let mut _localctx: Rc<ExponentiationContextAll> = _localctx;
		let mut _la: isize = -1;
		let result: Result<(), ANTLRError> = (|| {

			//recog.base.enter_outer_alt(_localctx.clone(), 1);
			recog.base.enter_outer_alt(None, 1);
			{
			recog.base.set_state(80);
			recog.err_handler.sync(&mut recog.base)?;
			_la = recog.base.input.la(1);
			if _la==OPERATOR_SUB {
				{
				recog.base.set_state(79);
				let tmp = recog.base.match_token(OPERATOR_SUB,&mut recog.err_handler)?;
				 cast_mut::<_,ExponentiationContext >(&mut _localctx).negation = Some(tmp.clone());
				  

				}
			}

			recog.base.set_state(84);
			recog.err_handler.sync(&mut recog.base)?;
			match  recog.interpreter.adaptive_predict(7,&mut recog.base)? {
				1 =>{
					{
					/*InvokeRule simple_factor*/
					recog.base.set_state(82);
					recog.simple_factor()?;

					}
				}
			,
				2 =>{
					{
					/*InvokeRule fraction*/
					recog.base.set_state(83);
					recog.fraction()?;

					}
				}

				_ => {}
			}
			recog.base.set_state(89);
			recog.err_handler.sync(&mut recog.base)?;
			_la = recog.base.input.la(1);
			while _la==DOT {
				{
				{
				/*InvokeRule methodinv*/
				recog.base.set_state(86);
				recog.methodinv()?;

				}
				}
				recog.base.set_state(91);
				recog.err_handler.sync(&mut recog.base)?;
				_la = recog.base.input.la(1);
			}
			}
			Ok(())
		})();
		match result {
		Ok(_)=>{},
        Err(e @ ANTLRError::FallThrough(_)) => return Err(e),
		Err(ref re) => {
				//_localctx.exception = re;
				recog.err_handler.report_error(&mut recog.base, re);
				recog.err_handler.recover(&mut recog.base, re)?;
			}
		}
		recog.base.exit_rule();

		Ok(_localctx)
	}
}
//------------------- exponent ----------------
pub type ExponentContextAll<'input> = ExponentContext<'input>;


pub type ExponentContext<'input> = BaseParserRuleContext<'input,ExponentContextExt<'input>>;

#[derive(Clone)]
pub struct ExponentContextExt<'input>{
ph:PhantomData<&'input str>
}

impl<'input> MatmParserContext<'input> for ExponentContext<'input>{}

impl<'input,'a> Listenable<dyn MatmParserListener<'input> + 'a> for ExponentContext<'input>{
		fn enter(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.enter_every_rule(self);
			listener.enter_exponent(self);
		}
		fn exit(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.exit_exponent(self);
			listener.exit_every_rule(self);
		}
}

impl<'input,'a> Visitable<dyn MatmParserVisitor<'input> + 'a> for ExponentContext<'input>{
	fn accept(&self,visitor: &mut (dyn MatmParserVisitor<'input> + 'a)) {
		visitor.visit_exponent(self);
	}
}

impl<'input> CustomRuleContext<'input> for ExponentContextExt<'input>{
	type TF = LocalTokenFactory<'input>;
	type Ctx = MatmParserContextType;
	fn get_rule_index(&self) -> usize { RULE_exponent }
	//fn type_rule_index() -> usize where Self: Sized { RULE_exponent }
}
antlr_rust::tid!{ExponentContextExt<'a>}

impl<'input> ExponentContextExt<'input>{
	fn new(parent: Option<Rc<dyn MatmParserContext<'input> + 'input > >, invoking_state: isize) -> Rc<ExponentContextAll<'input>> {
		Rc::new(
			BaseParserRuleContext::new_parser_ctx(parent, invoking_state,ExponentContextExt{
				ph:PhantomData
			}),
		)
	}
}

pub trait ExponentContextAttrs<'input>: MatmParserContext<'input> + BorrowMut<ExponentContextExt<'input>>{

/// Retrieves first TerminalNode corresponding to token OPERATOR_POW
/// Returns `None` if there is no child corresponding to token OPERATOR_POW
fn OPERATOR_POW(&self) -> Option<Rc<TerminalNode<'input,MatmParserContextType>>> where Self:Sized{
	self.get_token(OPERATOR_POW, 0)
}
fn exponentiation(&self) -> Option<Rc<ExponentiationContextAll<'input>>> where Self:Sized{
	self.child_of_type(0)
}

}

impl<'input> ExponentContextAttrs<'input> for ExponentContext<'input>{}

impl<'input, I, H> MatmParser<'input, I, H>
where
    I: TokenStream<'input, TF = LocalTokenFactory<'input> > + TidAble<'input>,
    H: ErrorStrategy<'input,BaseParserType<'input,I>>
{
	pub fn exponent(&mut self,)
	-> Result<Rc<ExponentContextAll<'input>>,ANTLRError> {
		let mut recog = self;
		let _parentctx = recog.ctx.take();
		let mut _localctx = ExponentContextExt::new(_parentctx.clone(), recog.base.get_state());
        recog.base.enter_rule(_localctx.clone(), 16, RULE_exponent);
        let mut _localctx: Rc<ExponentContextAll> = _localctx;
		let result: Result<(), ANTLRError> = (|| {

			//recog.base.enter_outer_alt(_localctx.clone(), 1);
			recog.base.enter_outer_alt(None, 1);
			{
			recog.base.set_state(92);
			recog.base.match_token(OPERATOR_POW,&mut recog.err_handler)?;

			/*InvokeRule exponentiation*/
			recog.base.set_state(93);
			recog.exponentiation()?;

			}
			Ok(())
		})();
		match result {
		Ok(_)=>{},
        Err(e @ ANTLRError::FallThrough(_)) => return Err(e),
		Err(ref re) => {
				//_localctx.exception = re;
				recog.err_handler.report_error(&mut recog.base, re);
				recog.err_handler.recover(&mut recog.base, re)?;
			}
		}
		recog.base.exit_rule();

		Ok(_localctx)
	}
}
//------------------- simple_factor ----------------
pub type Simple_factorContextAll<'input> = Simple_factorContext<'input>;


pub type Simple_factorContext<'input> = BaseParserRuleContext<'input,Simple_factorContextExt<'input>>;

#[derive(Clone)]
pub struct Simple_factorContextExt<'input>{
ph:PhantomData<&'input str>
}

impl<'input> MatmParserContext<'input> for Simple_factorContext<'input>{}

impl<'input,'a> Listenable<dyn MatmParserListener<'input> + 'a> for Simple_factorContext<'input>{
		fn enter(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.enter_every_rule(self);
			listener.enter_simple_factor(self);
		}
		fn exit(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.exit_simple_factor(self);
			listener.exit_every_rule(self);
		}
}

impl<'input,'a> Visitable<dyn MatmParserVisitor<'input> + 'a> for Simple_factorContext<'input>{
	fn accept(&self,visitor: &mut (dyn MatmParserVisitor<'input> + 'a)) {
		visitor.visit_simple_factor(self);
	}
}

impl<'input> CustomRuleContext<'input> for Simple_factorContextExt<'input>{
	type TF = LocalTokenFactory<'input>;
	type Ctx = MatmParserContextType;
	fn get_rule_index(&self) -> usize { RULE_simple_factor }
	//fn type_rule_index() -> usize where Self: Sized { RULE_simple_factor }
}
antlr_rust::tid!{Simple_factorContextExt<'a>}

impl<'input> Simple_factorContextExt<'input>{
	fn new(parent: Option<Rc<dyn MatmParserContext<'input> + 'input > >, invoking_state: isize) -> Rc<Simple_factorContextAll<'input>> {
		Rc::new(
			BaseParserRuleContext::new_parser_ctx(parent, invoking_state,Simple_factorContextExt{
				ph:PhantomData
			}),
		)
	}
}

pub trait Simple_factorContextAttrs<'input>: MatmParserContext<'input> + BorrowMut<Simple_factorContextExt<'input>>{

/// Retrieves first TerminalNode corresponding to token LPAREN
/// Returns `None` if there is no child corresponding to token LPAREN
fn LPAREN(&self) -> Option<Rc<TerminalNode<'input,MatmParserContextType>>> where Self:Sized{
	self.get_token(LPAREN, 0)
}
fn expr(&self) -> Option<Rc<ExprContextAll<'input>>> where Self:Sized{
	self.child_of_type(0)
}
/// Retrieves first TerminalNode corresponding to token RPAREN
/// Returns `None` if there is no child corresponding to token RPAREN
fn RPAREN(&self) -> Option<Rc<TerminalNode<'input,MatmParserContextType>>> where Self:Sized{
	self.get_token(RPAREN, 0)
}
fn functioninv(&self) -> Option<Rc<FunctioninvContextAll<'input>>> where Self:Sized{
	self.child_of_type(0)
}
fn number(&self) -> Option<Rc<NumberContextAll<'input>>> where Self:Sized{
	self.child_of_type(0)
}
fn symbol(&self) -> Option<Rc<SymbolContextAll<'input>>> where Self:Sized{
	self.child_of_type(0)
}

}

impl<'input> Simple_factorContextAttrs<'input> for Simple_factorContext<'input>{}

impl<'input, I, H> MatmParser<'input, I, H>
where
    I: TokenStream<'input, TF = LocalTokenFactory<'input> > + TidAble<'input>,
    H: ErrorStrategy<'input,BaseParserType<'input,I>>
{
	pub fn simple_factor(&mut self,)
	-> Result<Rc<Simple_factorContextAll<'input>>,ANTLRError> {
		let mut recog = self;
		let _parentctx = recog.ctx.take();
		let mut _localctx = Simple_factorContextExt::new(_parentctx.clone(), recog.base.get_state());
        recog.base.enter_rule(_localctx.clone(), 18, RULE_simple_factor);
        let mut _localctx: Rc<Simple_factorContextAll> = _localctx;
		let result: Result<(), ANTLRError> = (|| {

			recog.base.set_state(102);
			recog.err_handler.sync(&mut recog.base)?;
			match  recog.interpreter.adaptive_predict(9,&mut recog.base)? {
				1 =>{
					//recog.base.enter_outer_alt(_localctx.clone(), 1);
					recog.base.enter_outer_alt(None, 1);
					{
					recog.base.set_state(95);
					recog.base.match_token(LPAREN,&mut recog.err_handler)?;

					/*InvokeRule expr*/
					recog.base.set_state(96);
					recog.expr()?;

					recog.base.set_state(97);
					recog.base.match_token(RPAREN,&mut recog.err_handler)?;

					}
				}
			,
				2 =>{
					//recog.base.enter_outer_alt(_localctx.clone(), 2);
					recog.base.enter_outer_alt(None, 2);
					{
					/*InvokeRule functioninv*/
					recog.base.set_state(99);
					recog.functioninv()?;

					}
				}
			,
				3 =>{
					//recog.base.enter_outer_alt(_localctx.clone(), 3);
					recog.base.enter_outer_alt(None, 3);
					{
					/*InvokeRule number*/
					recog.base.set_state(100);
					recog.number()?;

					}
				}
			,
				4 =>{
					//recog.base.enter_outer_alt(_localctx.clone(), 4);
					recog.base.enter_outer_alt(None, 4);
					{
					/*InvokeRule symbol*/
					recog.base.set_state(101);
					recog.symbol()?;

					}
				}

				_ => {}
			}
			Ok(())
		})();
		match result {
		Ok(_)=>{},
        Err(e @ ANTLRError::FallThrough(_)) => return Err(e),
		Err(ref re) => {
				//_localctx.exception = re;
				recog.err_handler.report_error(&mut recog.base, re);
				recog.err_handler.recover(&mut recog.base, re)?;
			}
		}
		recog.base.exit_rule();

		Ok(_localctx)
	}
}
//------------------- fraction ----------------
pub type FractionContextAll<'input> = FractionContext<'input>;


pub type FractionContext<'input> = BaseParserRuleContext<'input,FractionContextExt<'input>>;

#[derive(Clone)]
pub struct FractionContextExt<'input>{
	pub numerator: Option<Rc<Simple_factorContextAll<'input>>>,
	pub denominator: Option<Rc<Simple_factorContextAll<'input>>>,
ph:PhantomData<&'input str>
}

impl<'input> MatmParserContext<'input> for FractionContext<'input>{}

impl<'input,'a> Listenable<dyn MatmParserListener<'input> + 'a> for FractionContext<'input>{
		fn enter(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.enter_every_rule(self);
			listener.enter_fraction(self);
		}
		fn exit(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.exit_fraction(self);
			listener.exit_every_rule(self);
		}
}

impl<'input,'a> Visitable<dyn MatmParserVisitor<'input> + 'a> for FractionContext<'input>{
	fn accept(&self,visitor: &mut (dyn MatmParserVisitor<'input> + 'a)) {
		visitor.visit_fraction(self);
	}
}

impl<'input> CustomRuleContext<'input> for FractionContextExt<'input>{
	type TF = LocalTokenFactory<'input>;
	type Ctx = MatmParserContextType;
	fn get_rule_index(&self) -> usize { RULE_fraction }
	//fn type_rule_index() -> usize where Self: Sized { RULE_fraction }
}
antlr_rust::tid!{FractionContextExt<'a>}

impl<'input> FractionContextExt<'input>{
	fn new(parent: Option<Rc<dyn MatmParserContext<'input> + 'input > >, invoking_state: isize) -> Rc<FractionContextAll<'input>> {
		Rc::new(
			BaseParserRuleContext::new_parser_ctx(parent, invoking_state,FractionContextExt{
				numerator: None, denominator: None, 
				ph:PhantomData
			}),
		)
	}
}

pub trait FractionContextAttrs<'input>: MatmParserContext<'input> + BorrowMut<FractionContextExt<'input>>{

/// Retrieves first TerminalNode corresponding to token OPERATOR_FRAC
/// Returns `None` if there is no child corresponding to token OPERATOR_FRAC
fn OPERATOR_FRAC(&self) -> Option<Rc<TerminalNode<'input,MatmParserContextType>>> where Self:Sized{
	self.get_token(OPERATOR_FRAC, 0)
}
fn simple_factor_all(&self) ->  Vec<Rc<Simple_factorContextAll<'input>>> where Self:Sized{
	self.children_of_type()
}
fn simple_factor(&self, i: usize) -> Option<Rc<Simple_factorContextAll<'input>>> where Self:Sized{
	self.child_of_type(i)
}

}

impl<'input> FractionContextAttrs<'input> for FractionContext<'input>{}

impl<'input, I, H> MatmParser<'input, I, H>
where
    I: TokenStream<'input, TF = LocalTokenFactory<'input> > + TidAble<'input>,
    H: ErrorStrategy<'input,BaseParserType<'input,I>>
{
	pub fn fraction(&mut self,)
	-> Result<Rc<FractionContextAll<'input>>,ANTLRError> {
		let mut recog = self;
		let _parentctx = recog.ctx.take();
		let mut _localctx = FractionContextExt::new(_parentctx.clone(), recog.base.get_state());
        recog.base.enter_rule(_localctx.clone(), 20, RULE_fraction);
        let mut _localctx: Rc<FractionContextAll> = _localctx;
		let result: Result<(), ANTLRError> = (|| {

			//recog.base.enter_outer_alt(_localctx.clone(), 1);
			recog.base.enter_outer_alt(None, 1);
			{
			/*InvokeRule simple_factor*/
			recog.base.set_state(104);
			let tmp = recog.simple_factor()?;
			 cast_mut::<_,FractionContext >(&mut _localctx).numerator = Some(tmp.clone());
			  

			recog.base.set_state(105);
			recog.base.match_token(OPERATOR_FRAC,&mut recog.err_handler)?;

			/*InvokeRule simple_factor*/
			recog.base.set_state(106);
			let tmp = recog.simple_factor()?;
			 cast_mut::<_,FractionContext >(&mut _localctx).denominator = Some(tmp.clone());
			  

			}
			Ok(())
		})();
		match result {
		Ok(_)=>{},
        Err(e @ ANTLRError::FallThrough(_)) => return Err(e),
		Err(ref re) => {
				//_localctx.exception = re;
				recog.err_handler.report_error(&mut recog.base, re);
				recog.err_handler.recover(&mut recog.base, re)?;
			}
		}
		recog.base.exit_rule();

		Ok(_localctx)
	}
}
//------------------- symbol ----------------
pub type SymbolContextAll<'input> = SymbolContext<'input>;


pub type SymbolContext<'input> = BaseParserRuleContext<'input,SymbolContextExt<'input>>;

#[derive(Clone)]
pub struct SymbolContextExt<'input>{
ph:PhantomData<&'input str>
}

impl<'input> MatmParserContext<'input> for SymbolContext<'input>{}

impl<'input,'a> Listenable<dyn MatmParserListener<'input> + 'a> for SymbolContext<'input>{
		fn enter(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.enter_every_rule(self);
			listener.enter_symbol(self);
		}
		fn exit(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.exit_symbol(self);
			listener.exit_every_rule(self);
		}
}

impl<'input,'a> Visitable<dyn MatmParserVisitor<'input> + 'a> for SymbolContext<'input>{
	fn accept(&self,visitor: &mut (dyn MatmParserVisitor<'input> + 'a)) {
		visitor.visit_symbol(self);
	}
}

impl<'input> CustomRuleContext<'input> for SymbolContextExt<'input>{
	type TF = LocalTokenFactory<'input>;
	type Ctx = MatmParserContextType;
	fn get_rule_index(&self) -> usize { RULE_symbol }
	//fn type_rule_index() -> usize where Self: Sized { RULE_symbol }
}
antlr_rust::tid!{SymbolContextExt<'a>}

impl<'input> SymbolContextExt<'input>{
	fn new(parent: Option<Rc<dyn MatmParserContext<'input> + 'input > >, invoking_state: isize) -> Rc<SymbolContextAll<'input>> {
		Rc::new(
			BaseParserRuleContext::new_parser_ctx(parent, invoking_state,SymbolContextExt{
				ph:PhantomData
			}),
		)
	}
}

pub trait SymbolContextAttrs<'input>: MatmParserContext<'input> + BorrowMut<SymbolContextExt<'input>>{

/// Retrieves first TerminalNode corresponding to token ID
/// Returns `None` if there is no child corresponding to token ID
fn ID(&self) -> Option<Rc<TerminalNode<'input,MatmParserContextType>>> where Self:Sized{
	self.get_token(ID, 0)
}

}

impl<'input> SymbolContextAttrs<'input> for SymbolContext<'input>{}

impl<'input, I, H> MatmParser<'input, I, H>
where
    I: TokenStream<'input, TF = LocalTokenFactory<'input> > + TidAble<'input>,
    H: ErrorStrategy<'input,BaseParserType<'input,I>>
{
	pub fn symbol(&mut self,)
	-> Result<Rc<SymbolContextAll<'input>>,ANTLRError> {
		let mut recog = self;
		let _parentctx = recog.ctx.take();
		let mut _localctx = SymbolContextExt::new(_parentctx.clone(), recog.base.get_state());
        recog.base.enter_rule(_localctx.clone(), 22, RULE_symbol);
        let mut _localctx: Rc<SymbolContextAll> = _localctx;
		let result: Result<(), ANTLRError> = (|| {

			//recog.base.enter_outer_alt(_localctx.clone(), 1);
			recog.base.enter_outer_alt(None, 1);
			{
			recog.base.set_state(108);
			recog.base.match_token(ID,&mut recog.err_handler)?;

			}
			Ok(())
		})();
		match result {
		Ok(_)=>{},
        Err(e @ ANTLRError::FallThrough(_)) => return Err(e),
		Err(ref re) => {
				//_localctx.exception = re;
				recog.err_handler.report_error(&mut recog.base, re);
				recog.err_handler.recover(&mut recog.base, re)?;
			}
		}
		recog.base.exit_rule();

		Ok(_localctx)
	}
}
//------------------- number ----------------
pub type NumberContextAll<'input> = NumberContext<'input>;


pub type NumberContext<'input> = BaseParserRuleContext<'input,NumberContextExt<'input>>;

#[derive(Clone)]
pub struct NumberContextExt<'input>{
ph:PhantomData<&'input str>
}

impl<'input> MatmParserContext<'input> for NumberContext<'input>{}

impl<'input,'a> Listenable<dyn MatmParserListener<'input> + 'a> for NumberContext<'input>{
		fn enter(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.enter_every_rule(self);
			listener.enter_number(self);
		}
		fn exit(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.exit_number(self);
			listener.exit_every_rule(self);
		}
}

impl<'input,'a> Visitable<dyn MatmParserVisitor<'input> + 'a> for NumberContext<'input>{
	fn accept(&self,visitor: &mut (dyn MatmParserVisitor<'input> + 'a)) {
		visitor.visit_number(self);
	}
}

impl<'input> CustomRuleContext<'input> for NumberContextExt<'input>{
	type TF = LocalTokenFactory<'input>;
	type Ctx = MatmParserContextType;
	fn get_rule_index(&self) -> usize { RULE_number }
	//fn type_rule_index() -> usize where Self: Sized { RULE_number }
}
antlr_rust::tid!{NumberContextExt<'a>}

impl<'input> NumberContextExt<'input>{
	fn new(parent: Option<Rc<dyn MatmParserContext<'input> + 'input > >, invoking_state: isize) -> Rc<NumberContextAll<'input>> {
		Rc::new(
			BaseParserRuleContext::new_parser_ctx(parent, invoking_state,NumberContextExt{
				ph:PhantomData
			}),
		)
	}
}

pub trait NumberContextAttrs<'input>: MatmParserContext<'input> + BorrowMut<NumberContextExt<'input>>{

/// Retrieves first TerminalNode corresponding to token INT_DECIMAL
/// Returns `None` if there is no child corresponding to token INT_DECIMAL
fn INT_DECIMAL(&self) -> Option<Rc<TerminalNode<'input,MatmParserContextType>>> where Self:Sized{
	self.get_token(INT_DECIMAL, 0)
}
/// Retrieves first TerminalNode corresponding to token INT_RADIX_SINGLE
/// Returns `None` if there is no child corresponding to token INT_RADIX_SINGLE
fn INT_RADIX_SINGLE(&self) -> Option<Rc<TerminalNode<'input,MatmParserContextType>>> where Self:Sized{
	self.get_token(INT_RADIX_SINGLE, 0)
}
/// Retrieves first TerminalNode corresponding to token INT_RADIX_MULTIPLE
/// Returns `None` if there is no child corresponding to token INT_RADIX_MULTIPLE
fn INT_RADIX_MULTIPLE(&self) -> Option<Rc<TerminalNode<'input,MatmParserContextType>>> where Self:Sized{
	self.get_token(INT_RADIX_MULTIPLE, 0)
}

}

impl<'input> NumberContextAttrs<'input> for NumberContext<'input>{}

impl<'input, I, H> MatmParser<'input, I, H>
where
    I: TokenStream<'input, TF = LocalTokenFactory<'input> > + TidAble<'input>,
    H: ErrorStrategy<'input,BaseParserType<'input,I>>
{
	pub fn number(&mut self,)
	-> Result<Rc<NumberContextAll<'input>>,ANTLRError> {
		let mut recog = self;
		let _parentctx = recog.ctx.take();
		let mut _localctx = NumberContextExt::new(_parentctx.clone(), recog.base.get_state());
        recog.base.enter_rule(_localctx.clone(), 24, RULE_number);
        let mut _localctx: Rc<NumberContextAll> = _localctx;
		let mut _la: isize = -1;
		let result: Result<(), ANTLRError> = (|| {

			//recog.base.enter_outer_alt(_localctx.clone(), 1);
			recog.base.enter_outer_alt(None, 1);
			{
			recog.base.set_state(110);
			_la = recog.base.input.la(1);
			if { !((((_la) & !0x3f) == 0 && ((1usize << _la) & ((1usize << INT_DECIMAL) | (1usize << INT_RADIX_SINGLE) | (1usize << INT_RADIX_MULTIPLE))) != 0)) } {
				recog.err_handler.recover_inline(&mut recog.base)?;

			}
			else {
				if  recog.base.input.la(1)==TOKEN_EOF { recog.base.matched_eof = true };
				recog.err_handler.report_match(&mut recog.base);
				recog.base.consume(&mut recog.err_handler);
			}
			}
			Ok(())
		})();
		match result {
		Ok(_)=>{},
        Err(e @ ANTLRError::FallThrough(_)) => return Err(e),
		Err(ref re) => {
				//_localctx.exception = re;
				recog.err_handler.report_error(&mut recog.base, re);
				recog.err_handler.recover(&mut recog.base, re)?;
			}
		}
		recog.base.exit_rule();

		Ok(_localctx)
	}
}
//------------------- methodinv ----------------
pub type MethodinvContextAll<'input> = MethodinvContext<'input>;


pub type MethodinvContext<'input> = BaseParserRuleContext<'input,MethodinvContextExt<'input>>;

#[derive(Clone)]
pub struct MethodinvContextExt<'input>{
ph:PhantomData<&'input str>
}

impl<'input> MatmParserContext<'input> for MethodinvContext<'input>{}

impl<'input,'a> Listenable<dyn MatmParserListener<'input> + 'a> for MethodinvContext<'input>{
		fn enter(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.enter_every_rule(self);
			listener.enter_methodinv(self);
		}
		fn exit(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.exit_methodinv(self);
			listener.exit_every_rule(self);
		}
}

impl<'input,'a> Visitable<dyn MatmParserVisitor<'input> + 'a> for MethodinvContext<'input>{
	fn accept(&self,visitor: &mut (dyn MatmParserVisitor<'input> + 'a)) {
		visitor.visit_methodinv(self);
	}
}

impl<'input> CustomRuleContext<'input> for MethodinvContextExt<'input>{
	type TF = LocalTokenFactory<'input>;
	type Ctx = MatmParserContextType;
	fn get_rule_index(&self) -> usize { RULE_methodinv }
	//fn type_rule_index() -> usize where Self: Sized { RULE_methodinv }
}
antlr_rust::tid!{MethodinvContextExt<'a>}

impl<'input> MethodinvContextExt<'input>{
	fn new(parent: Option<Rc<dyn MatmParserContext<'input> + 'input > >, invoking_state: isize) -> Rc<MethodinvContextAll<'input>> {
		Rc::new(
			BaseParserRuleContext::new_parser_ctx(parent, invoking_state,MethodinvContextExt{
				ph:PhantomData
			}),
		)
	}
}

pub trait MethodinvContextAttrs<'input>: MatmParserContext<'input> + BorrowMut<MethodinvContextExt<'input>>{

/// Retrieves first TerminalNode corresponding to token DOT
/// Returns `None` if there is no child corresponding to token DOT
fn DOT(&self) -> Option<Rc<TerminalNode<'input,MatmParserContextType>>> where Self:Sized{
	self.get_token(DOT, 0)
}
fn functioninv(&self) -> Option<Rc<FunctioninvContextAll<'input>>> where Self:Sized{
	self.child_of_type(0)
}

}

impl<'input> MethodinvContextAttrs<'input> for MethodinvContext<'input>{}

impl<'input, I, H> MatmParser<'input, I, H>
where
    I: TokenStream<'input, TF = LocalTokenFactory<'input> > + TidAble<'input>,
    H: ErrorStrategy<'input,BaseParserType<'input,I>>
{
	pub fn methodinv(&mut self,)
	-> Result<Rc<MethodinvContextAll<'input>>,ANTLRError> {
		let mut recog = self;
		let _parentctx = recog.ctx.take();
		let mut _localctx = MethodinvContextExt::new(_parentctx.clone(), recog.base.get_state());
        recog.base.enter_rule(_localctx.clone(), 26, RULE_methodinv);
        let mut _localctx: Rc<MethodinvContextAll> = _localctx;
		let result: Result<(), ANTLRError> = (|| {

			//recog.base.enter_outer_alt(_localctx.clone(), 1);
			recog.base.enter_outer_alt(None, 1);
			{
			recog.base.set_state(112);
			recog.base.match_token(DOT,&mut recog.err_handler)?;

			/*InvokeRule functioninv*/
			recog.base.set_state(113);
			recog.functioninv()?;

			}
			Ok(())
		})();
		match result {
		Ok(_)=>{},
        Err(e @ ANTLRError::FallThrough(_)) => return Err(e),
		Err(ref re) => {
				//_localctx.exception = re;
				recog.err_handler.report_error(&mut recog.base, re);
				recog.err_handler.recover(&mut recog.base, re)?;
			}
		}
		recog.base.exit_rule();

		Ok(_localctx)
	}
}
//------------------- functioninv ----------------
pub type FunctioninvContextAll<'input> = FunctioninvContext<'input>;


pub type FunctioninvContext<'input> = BaseParserRuleContext<'input,FunctioninvContextExt<'input>>;

#[derive(Clone)]
pub struct FunctioninvContextExt<'input>{
ph:PhantomData<&'input str>
}

impl<'input> MatmParserContext<'input> for FunctioninvContext<'input>{}

impl<'input,'a> Listenable<dyn MatmParserListener<'input> + 'a> for FunctioninvContext<'input>{
		fn enter(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.enter_every_rule(self);
			listener.enter_functioninv(self);
		}
		fn exit(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.exit_functioninv(self);
			listener.exit_every_rule(self);
		}
}

impl<'input,'a> Visitable<dyn MatmParserVisitor<'input> + 'a> for FunctioninvContext<'input>{
	fn accept(&self,visitor: &mut (dyn MatmParserVisitor<'input> + 'a)) {
		visitor.visit_functioninv(self);
	}
}

impl<'input> CustomRuleContext<'input> for FunctioninvContextExt<'input>{
	type TF = LocalTokenFactory<'input>;
	type Ctx = MatmParserContextType;
	fn get_rule_index(&self) -> usize { RULE_functioninv }
	//fn type_rule_index() -> usize where Self: Sized { RULE_functioninv }
}
antlr_rust::tid!{FunctioninvContextExt<'a>}

impl<'input> FunctioninvContextExt<'input>{
	fn new(parent: Option<Rc<dyn MatmParserContext<'input> + 'input > >, invoking_state: isize) -> Rc<FunctioninvContextAll<'input>> {
		Rc::new(
			BaseParserRuleContext::new_parser_ctx(parent, invoking_state,FunctioninvContextExt{
				ph:PhantomData
			}),
		)
	}
}

pub trait FunctioninvContextAttrs<'input>: MatmParserContext<'input> + BorrowMut<FunctioninvContextExt<'input>>{

/// Retrieves first TerminalNode corresponding to token ID
/// Returns `None` if there is no child corresponding to token ID
fn ID(&self) -> Option<Rc<TerminalNode<'input,MatmParserContextType>>> where Self:Sized{
	self.get_token(ID, 0)
}
/// Retrieves first TerminalNode corresponding to token LPAREN
/// Returns `None` if there is no child corresponding to token LPAREN
fn LPAREN(&self) -> Option<Rc<TerminalNode<'input,MatmParserContextType>>> where Self:Sized{
	self.get_token(LPAREN, 0)
}
fn arguments(&self) -> Option<Rc<ArgumentsContextAll<'input>>> where Self:Sized{
	self.child_of_type(0)
}
/// Retrieves first TerminalNode corresponding to token RPAREN
/// Returns `None` if there is no child corresponding to token RPAREN
fn RPAREN(&self) -> Option<Rc<TerminalNode<'input,MatmParserContextType>>> where Self:Sized{
	self.get_token(RPAREN, 0)
}

}

impl<'input> FunctioninvContextAttrs<'input> for FunctioninvContext<'input>{}

impl<'input, I, H> MatmParser<'input, I, H>
where
    I: TokenStream<'input, TF = LocalTokenFactory<'input> > + TidAble<'input>,
    H: ErrorStrategy<'input,BaseParserType<'input,I>>
{
	pub fn functioninv(&mut self,)
	-> Result<Rc<FunctioninvContextAll<'input>>,ANTLRError> {
		let mut recog = self;
		let _parentctx = recog.ctx.take();
		let mut _localctx = FunctioninvContextExt::new(_parentctx.clone(), recog.base.get_state());
        recog.base.enter_rule(_localctx.clone(), 28, RULE_functioninv);
        let mut _localctx: Rc<FunctioninvContextAll> = _localctx;
		let result: Result<(), ANTLRError> = (|| {

			//recog.base.enter_outer_alt(_localctx.clone(), 1);
			recog.base.enter_outer_alt(None, 1);
			{
			recog.base.set_state(115);
			recog.base.match_token(ID,&mut recog.err_handler)?;

			recog.base.set_state(122);
			recog.err_handler.sync(&mut recog.base)?;
			match  recog.interpreter.adaptive_predict(10,&mut recog.base)? {
				1 =>{
					{
					recog.base.set_state(116);
					recog.base.match_token(LPAREN,&mut recog.err_handler)?;

					/*InvokeRule arguments*/
					recog.base.set_state(117);
					recog.arguments()?;

					recog.base.set_state(118);
					recog.base.match_token(RPAREN,&mut recog.err_handler)?;

					}
				}
			,
				2 =>{
					{
					recog.base.set_state(120);
					recog.base.match_token(LPAREN,&mut recog.err_handler)?;

					recog.base.set_state(121);
					recog.base.match_token(RPAREN,&mut recog.err_handler)?;

					}
				}

				_ => {}
			}
			}
			Ok(())
		})();
		match result {
		Ok(_)=>{},
        Err(e @ ANTLRError::FallThrough(_)) => return Err(e),
		Err(ref re) => {
				//_localctx.exception = re;
				recog.err_handler.report_error(&mut recog.base, re);
				recog.err_handler.recover(&mut recog.base, re)?;
			}
		}
		recog.base.exit_rule();

		Ok(_localctx)
	}
}
//------------------- arguments ----------------
pub type ArgumentsContextAll<'input> = ArgumentsContext<'input>;


pub type ArgumentsContext<'input> = BaseParserRuleContext<'input,ArgumentsContextExt<'input>>;

#[derive(Clone)]
pub struct ArgumentsContextExt<'input>{
ph:PhantomData<&'input str>
}

impl<'input> MatmParserContext<'input> for ArgumentsContext<'input>{}

impl<'input,'a> Listenable<dyn MatmParserListener<'input> + 'a> for ArgumentsContext<'input>{
		fn enter(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.enter_every_rule(self);
			listener.enter_arguments(self);
		}
		fn exit(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.exit_arguments(self);
			listener.exit_every_rule(self);
		}
}

impl<'input,'a> Visitable<dyn MatmParserVisitor<'input> + 'a> for ArgumentsContext<'input>{
	fn accept(&self,visitor: &mut (dyn MatmParserVisitor<'input> + 'a)) {
		visitor.visit_arguments(self);
	}
}

impl<'input> CustomRuleContext<'input> for ArgumentsContextExt<'input>{
	type TF = LocalTokenFactory<'input>;
	type Ctx = MatmParserContextType;
	fn get_rule_index(&self) -> usize { RULE_arguments }
	//fn type_rule_index() -> usize where Self: Sized { RULE_arguments }
}
antlr_rust::tid!{ArgumentsContextExt<'a>}

impl<'input> ArgumentsContextExt<'input>{
	fn new(parent: Option<Rc<dyn MatmParserContext<'input> + 'input > >, invoking_state: isize) -> Rc<ArgumentsContextAll<'input>> {
		Rc::new(
			BaseParserRuleContext::new_parser_ctx(parent, invoking_state,ArgumentsContextExt{
				ph:PhantomData
			}),
		)
	}
}

pub trait ArgumentsContextAttrs<'input>: MatmParserContext<'input> + BorrowMut<ArgumentsContextExt<'input>>{

fn expr_all(&self) ->  Vec<Rc<ExprContextAll<'input>>> where Self:Sized{
	self.children_of_type()
}
fn expr(&self, i: usize) -> Option<Rc<ExprContextAll<'input>>> where Self:Sized{
	self.child_of_type(i)
}
/// Retrieves all `TerminalNode`s corresponding to token COMMA in current rule
fn COMMA_all(&self) -> Vec<Rc<TerminalNode<'input,MatmParserContextType>>>  where Self:Sized{
	self.children_of_type()
}
/// Retrieves 'i's TerminalNode corresponding to token COMMA, starting from 0.
/// Returns `None` if number of children corresponding to token COMMA is less or equal than `i`.
fn COMMA(&self, i: usize) -> Option<Rc<TerminalNode<'input,MatmParserContextType>>> where Self:Sized{
	self.get_token(COMMA, i)
}

}

impl<'input> ArgumentsContextAttrs<'input> for ArgumentsContext<'input>{}

impl<'input, I, H> MatmParser<'input, I, H>
where
    I: TokenStream<'input, TF = LocalTokenFactory<'input> > + TidAble<'input>,
    H: ErrorStrategy<'input,BaseParserType<'input,I>>
{
	pub fn arguments(&mut self,)
	-> Result<Rc<ArgumentsContextAll<'input>>,ANTLRError> {
		let mut recog = self;
		let _parentctx = recog.ctx.take();
		let mut _localctx = ArgumentsContextExt::new(_parentctx.clone(), recog.base.get_state());
        recog.base.enter_rule(_localctx.clone(), 30, RULE_arguments);
        let mut _localctx: Rc<ArgumentsContextAll> = _localctx;
		let mut _la: isize = -1;
		let result: Result<(), ANTLRError> = (|| {

			//recog.base.enter_outer_alt(_localctx.clone(), 1);
			recog.base.enter_outer_alt(None, 1);
			{
			/*InvokeRule expr*/
			recog.base.set_state(124);
			recog.expr()?;

			recog.base.set_state(129);
			recog.err_handler.sync(&mut recog.base)?;
			_la = recog.base.input.la(1);
			while _la==COMMA {
				{
				{
				recog.base.set_state(125);
				recog.base.match_token(COMMA,&mut recog.err_handler)?;

				/*InvokeRule expr*/
				recog.base.set_state(126);
				recog.expr()?;

				}
				}
				recog.base.set_state(131);
				recog.err_handler.sync(&mut recog.base)?;
				_la = recog.base.input.la(1);
			}
			}
			Ok(())
		})();
		match result {
		Ok(_)=>{},
        Err(e @ ANTLRError::FallThrough(_)) => return Err(e),
		Err(ref re) => {
				//_localctx.exception = re;
				recog.err_handler.report_error(&mut recog.base, re);
				recog.err_handler.recover(&mut recog.base, re)?;
			}
		}
		recog.base.exit_rule();

		Ok(_localctx)
	}
}
//------------------- assignment ----------------
pub type AssignmentContextAll<'input> = AssignmentContext<'input>;


pub type AssignmentContext<'input> = BaseParserRuleContext<'input,AssignmentContextExt<'input>>;

#[derive(Clone)]
pub struct AssignmentContextExt<'input>{
ph:PhantomData<&'input str>
}

impl<'input> MatmParserContext<'input> for AssignmentContext<'input>{}

impl<'input,'a> Listenable<dyn MatmParserListener<'input> + 'a> for AssignmentContext<'input>{
		fn enter(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.enter_every_rule(self);
			listener.enter_assignment(self);
		}
		fn exit(&self,listener: &mut (dyn MatmParserListener<'input> + 'a)) {
			listener.exit_assignment(self);
			listener.exit_every_rule(self);
		}
}

impl<'input,'a> Visitable<dyn MatmParserVisitor<'input> + 'a> for AssignmentContext<'input>{
	fn accept(&self,visitor: &mut (dyn MatmParserVisitor<'input> + 'a)) {
		visitor.visit_assignment(self);
	}
}

impl<'input> CustomRuleContext<'input> for AssignmentContextExt<'input>{
	type TF = LocalTokenFactory<'input>;
	type Ctx = MatmParserContextType;
	fn get_rule_index(&self) -> usize { RULE_assignment }
	//fn type_rule_index() -> usize where Self: Sized { RULE_assignment }
}
antlr_rust::tid!{AssignmentContextExt<'a>}

impl<'input> AssignmentContextExt<'input>{
	fn new(parent: Option<Rc<dyn MatmParserContext<'input> + 'input > >, invoking_state: isize) -> Rc<AssignmentContextAll<'input>> {
		Rc::new(
			BaseParserRuleContext::new_parser_ctx(parent, invoking_state,AssignmentContextExt{
				ph:PhantomData
			}),
		)
	}
}

pub trait AssignmentContextAttrs<'input>: MatmParserContext<'input> + BorrowMut<AssignmentContextExt<'input>>{

fn symbol(&self) -> Option<Rc<SymbolContextAll<'input>>> where Self:Sized{
	self.child_of_type(0)
}
/// Retrieves first TerminalNode corresponding to token COLON
/// Returns `None` if there is no child corresponding to token COLON
fn COLON(&self) -> Option<Rc<TerminalNode<'input,MatmParserContextType>>> where Self:Sized{
	self.get_token(COLON, 0)
}
fn expr(&self) -> Option<Rc<ExprContextAll<'input>>> where Self:Sized{
	self.child_of_type(0)
}

}

impl<'input> AssignmentContextAttrs<'input> for AssignmentContext<'input>{}

impl<'input, I, H> MatmParser<'input, I, H>
where
    I: TokenStream<'input, TF = LocalTokenFactory<'input> > + TidAble<'input>,
    H: ErrorStrategy<'input,BaseParserType<'input,I>>
{
	pub fn assignment(&mut self,)
	-> Result<Rc<AssignmentContextAll<'input>>,ANTLRError> {
		let mut recog = self;
		let _parentctx = recog.ctx.take();
		let mut _localctx = AssignmentContextExt::new(_parentctx.clone(), recog.base.get_state());
        recog.base.enter_rule(_localctx.clone(), 32, RULE_assignment);
        let mut _localctx: Rc<AssignmentContextAll> = _localctx;
		let result: Result<(), ANTLRError> = (|| {

			//recog.base.enter_outer_alt(_localctx.clone(), 1);
			recog.base.enter_outer_alt(None, 1);
			{
			/*InvokeRule symbol*/
			recog.base.set_state(132);
			recog.symbol()?;

			recog.base.set_state(133);
			recog.base.match_token(COLON,&mut recog.err_handler)?;

			/*InvokeRule expr*/
			recog.base.set_state(134);
			recog.expr()?;

			}
			Ok(())
		})();
		match result {
		Ok(_)=>{},
        Err(e @ ANTLRError::FallThrough(_)) => return Err(e),
		Err(ref re) => {
				//_localctx.exception = re;
				recog.err_handler.report_error(&mut recog.base, re);
				recog.err_handler.recover(&mut recog.base, re)?;
			}
		}
		recog.base.exit_rule();

		Ok(_localctx)
	}
}

lazy_static! {
    static ref _ATN: Arc<ATN> =
        Arc::new(ATNDeserializer::new(None).deserialize(_serializedATN.chars()));
    static ref _decision_to_DFA: Arc<Vec<antlr_rust::RwLock<DFA>>> = {
        let mut dfa = Vec::new();
        let size = _ATN.decision_to_state.len();
        for i in 0..size {
            dfa.push(DFA::new(
                _ATN.clone(),
                _ATN.get_decision_state(i),
                i as isize,
            ).into())
        }
        Arc::new(dfa)
    };
}



const _serializedATN:&'static str =
	"\x03\u{608b}\u{a72a}\u{8133}\u{b9ed}\u{417c}\u{3be7}\u{7786}\u{5964}\x03\
	\x14\u{8b}\x04\x02\x09\x02\x04\x03\x09\x03\x04\x04\x09\x04\x04\x05\x09\x05\
	\x04\x06\x09\x06\x04\x07\x09\x07\x04\x08\x09\x08\x04\x09\x09\x09\x04\x0a\
	\x09\x0a\x04\x0b\x09\x0b\x04\x0c\x09\x0c\x04\x0d\x09\x0d\x04\x0e\x09\x0e\
	\x04\x0f\x09\x0f\x04\x10\x09\x10\x04\x11\x09\x11\x04\x12\x09\x12\x03\x02\
	\x03\x02\x03\x03\x03\x03\x03\x03\x07\x03\x2a\x0a\x03\x0c\x03\x0e\x03\x2d\
	\x0b\x03\x03\x03\x05\x03\x30\x0a\x03\x03\x04\x03\x04\x03\x04\x05\x04\x35\
	\x0a\x04\x03\x05\x03\x05\x03\x05\x03\x05\x03\x06\x03\x06\x03\x06\x07\x06\
	\x3e\x0a\x06\x0c\x06\x0e\x06\x41\x0b\x06\x03\x07\x03\x07\x03\x07\x07\x07\
	\x46\x0a\x07\x0c\x07\x0e\x07\x49\x0b\x07\x03\x08\x03\x08\x07\x08\x4d\x0a\
	\x08\x0c\x08\x0e\x08\x50\x0b\x08\x03\x09\x05\x09\x53\x0a\x09\x03\x09\x03\
	\x09\x05\x09\x57\x0a\x09\x03\x09\x07\x09\x5a\x0a\x09\x0c\x09\x0e\x09\x5d\
	\x0b\x09\x03\x0a\x03\x0a\x03\x0a\x03\x0b\x03\x0b\x03\x0b\x03\x0b\x03\x0b\
	\x03\x0b\x03\x0b\x05\x0b\x69\x0a\x0b\x03\x0c\x03\x0c\x03\x0c\x03\x0c\x03\
	\x0d\x03\x0d\x03\x0e\x03\x0e\x03\x0f\x03\x0f\x03\x0f\x03\x10\x03\x10\x03\
	\x10\x03\x10\x03\x10\x03\x10\x03\x10\x05\x10\x7d\x0a\x10\x03\x11\x03\x11\
	\x03\x11\x07\x11\u{82}\x0a\x11\x0c\x11\x0e\x11\u{85}\x0b\x11\x03\x12\x03\
	\x12\x03\x12\x03\x12\x03\x12\x02\x02\x13\x02\x04\x06\x08\x0a\x0c\x0e\x10\
	\x12\x14\x16\x18\x1a\x1c\x1e\x20\x22\x02\x05\x03\x02\x09\x0a\x03\x02\x06\
	\x07\x03\x02\x0c\x0e\x02\u{88}\x02\x24\x03\x02\x02\x02\x04\x26\x03\x02\x02\
	\x02\x06\x34\x03\x02\x02\x02\x08\x36\x03\x02\x02\x02\x0a\x3a\x03\x02\x02\
	\x02\x0c\x42\x03\x02\x02\x02\x0e\x4a\x03\x02\x02\x02\x10\x52\x03\x02\x02\
	\x02\x12\x5e\x03\x02\x02\x02\x14\x68\x03\x02\x02\x02\x16\x6a\x03\x02\x02\
	\x02\x18\x6e\x03\x02\x02\x02\x1a\x70\x03\x02\x02\x02\x1c\x72\x03\x02\x02\
	\x02\x1e\x75\x03\x02\x02\x02\x20\x7e\x03\x02\x02\x02\x22\u{86}\x03\x02\x02\
	\x02\x24\x25\x05\x04\x03\x02\x25\x03\x03\x02\x02\x02\x26\x2b\x05\x06\x04\
	\x02\x27\x28\x07\x03\x02\x02\x28\x2a\x05\x06\x04\x02\x29\x27\x03\x02\x02\
	\x02\x2a\x2d\x03\x02\x02\x02\x2b\x29\x03\x02\x02\x02\x2b\x2c\x03\x02\x02\
	\x02\x2c\x2f\x03\x02\x02\x02\x2d\x2b\x03\x02\x02\x02\x2e\x30\x07\x03\x02\
	\x02\x2f\x2e\x03\x02\x02\x02\x2f\x30\x03\x02\x02\x02\x30\x05\x03\x02\x02\
	\x02\x31\x35\x05\x08\x05\x02\x32\x35\x05\x0a\x06\x02\x33\x35\x05\x22\x12\
	\x02\x34\x31\x03\x02\x02\x02\x34\x32\x03\x02\x02\x02\x34\x33\x03\x02\x02\
	\x02\x35\x07\x03\x02\x02\x02\x36\x37\x05\x0a\x06\x02\x37\x38\x07\x04\x02\
	\x02\x38\x39\x05\x0a\x06\x02\x39\x09\x03\x02\x02\x02\x3a\x3f\x05\x0c\x07\
	\x02\x3b\x3c\x09\x02\x02\x02\x3c\x3e\x05\x0c\x07\x02\x3d\x3b\x03\x02\x02\
	\x02\x3e\x41\x03\x02\x02\x02\x3f\x3d\x03\x02\x02\x02\x3f\x40\x03\x02\x02\
	\x02\x40\x0b\x03\x02\x02\x02\x41\x3f\x03\x02\x02\x02\x42\x47\x05\x0e\x08\
	\x02\x43\x44\x09\x03\x02\x02\x44\x46\x05\x0e\x08\x02\x45\x43\x03\x02\x02\
	\x02\x46\x49\x03\x02\x02\x02\x47\x45\x03\x02\x02\x02\x47\x48\x03\x02\x02\
	\x02\x48\x0d\x03\x02\x02\x02\x49\x47\x03\x02\x02\x02\x4a\x4e\x05\x10\x09\
	\x02\x4b\x4d\x05\x12\x0a\x02\x4c\x4b\x03\x02\x02\x02\x4d\x50\x03\x02\x02\
	\x02\x4e\x4c\x03\x02\x02\x02\x4e\x4f\x03\x02\x02\x02\x4f\x0f\x03\x02\x02\
	\x02\x50\x4e\x03\x02\x02\x02\x51\x53\x07\x0a\x02\x02\x52\x51\x03\x02\x02\
	\x02\x52\x53\x03\x02\x02\x02\x53\x56\x03\x02\x02\x02\x54\x57\x05\x14\x0b\
	\x02\x55\x57\x05\x16\x0c\x02\x56\x54\x03\x02\x02\x02\x56\x55\x03\x02\x02\
	\x02\x57\x5b\x03\x02\x02\x02\x58\x5a\x05\x1c\x0f\x02\x59\x58\x03\x02\x02\
	\x02\x5a\x5d\x03\x02\x02\x02\x5b\x59\x03\x02\x02\x02\x5b\x5c\x03\x02\x02\
	\x02\x5c\x11\x03\x02\x02\x02\x5d\x5b\x03\x02\x02\x02\x5e\x5f\x07\x05\x02\
	\x02\x5f\x60\x05\x10\x09\x02\x60\x13\x03\x02\x02\x02\x61\x62\x07\x10\x02\
	\x02\x62\x63\x05\x06\x04\x02\x63\x64\x07\x11\x02\x02\x64\x69\x03\x02\x02\
	\x02\x65\x69\x05\x1e\x10\x02\x66\x69\x05\x1a\x0e\x02\x67\x69\x05\x18\x0d\
	\x02\x68\x61\x03\x02\x02\x02\x68\x65\x03\x02\x02\x02\x68\x66\x03\x02\x02\
	\x02\x68\x67\x03\x02\x02\x02\x69\x15\x03\x02\x02\x02\x6a\x6b\x05\x14\x0b\
	\x02\x6b\x6c\x07\x08\x02\x02\x6c\x6d\x05\x14\x0b\x02\x6d\x17\x03\x02\x02\
	\x02\x6e\x6f\x07\x0b\x02\x02\x6f\x19\x03\x02\x02\x02\x70\x71\x09\x04\x02\
	\x02\x71\x1b\x03\x02\x02\x02\x72\x73\x07\x13\x02\x02\x73\x74\x05\x1e\x10\
	\x02\x74\x1d\x03\x02\x02\x02\x75\x7c\x07\x0b\x02\x02\x76\x77\x07\x10\x02\
	\x02\x77\x78\x05\x20\x11\x02\x78\x79\x07\x11\x02\x02\x79\x7d\x03\x02\x02\
	\x02\x7a\x7b\x07\x10\x02\x02\x7b\x7d\x07\x11\x02\x02\x7c\x76\x03\x02\x02\
	\x02\x7c\x7a\x03\x02\x02\x02\x7d\x1f\x03\x02\x02\x02\x7e\u{83}\x05\x06\x04\
	\x02\x7f\u{80}\x07\x12\x02\x02\u{80}\u{82}\x05\x06\x04\x02\u{81}\x7f\x03\
	\x02\x02\x02\u{82}\u{85}\x03\x02\x02\x02\u{83}\u{81}\x03\x02\x02\x02\u{83}\
	\u{84}\x03\x02\x02\x02\u{84}\x21\x03\x02\x02\x02\u{85}\u{83}\x03\x02\x02\
	\x02\u{86}\u{87}\x05\x18\x0d\x02\u{87}\u{88}\x07\x14\x02\x02\u{88}\u{89}\
	\x05\x06\x04\x02\u{89}\x23\x03\x02\x02\x02\x0e\x2b\x2f\x34\x3f\x47\x4e\x52\
	\x56\x5b\x68\x7c\u{83}";

