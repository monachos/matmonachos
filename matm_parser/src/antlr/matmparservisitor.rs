#![allow(nonstandard_style)]
// Generated from MatmParser.g4 by ANTLR 4.8
use antlr_rust::tree::{ParseTreeVisitor,ParseTreeVisitorCompat};
use super::matmparser::*;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link MatmParser}.
 */
pub trait MatmParserVisitor<'input>: ParseTreeVisitor<'input,MatmParserContextType>{
	/**
	 * Visit a parse tree produced by {@link MatmParser#start}.
	 * @param ctx the parse tree
	 */
	fn visit_start(&mut self, ctx: &StartContext<'input>) { self.visit_children(ctx) }

	/**
	 * Visit a parse tree produced by {@link MatmParser#statements}.
	 * @param ctx the parse tree
	 */
	fn visit_statements(&mut self, ctx: &StatementsContext<'input>) { self.visit_children(ctx) }

	/**
	 * Visit a parse tree produced by {@link MatmParser#expr}.
	 * @param ctx the parse tree
	 */
	fn visit_expr(&mut self, ctx: &ExprContext<'input>) { self.visit_children(ctx) }

	/**
	 * Visit a parse tree produced by {@link MatmParser#equation}.
	 * @param ctx the parse tree
	 */
	fn visit_equation(&mut self, ctx: &EquationContext<'input>) { self.visit_children(ctx) }

	/**
	 * Visit a parse tree produced by {@link MatmParser#adding_expr}.
	 * @param ctx the parse tree
	 */
	fn visit_adding_expr(&mut self, ctx: &Adding_exprContext<'input>) { self.visit_children(ctx) }

	/**
	 * Visit a parse tree produced by {@link MatmParser#term}.
	 * @param ctx the parse tree
	 */
	fn visit_term(&mut self, ctx: &TermContext<'input>) { self.visit_children(ctx) }

	/**
	 * Visit a parse tree produced by {@link MatmParser#factor}.
	 * @param ctx the parse tree
	 */
	fn visit_factor(&mut self, ctx: &FactorContext<'input>) { self.visit_children(ctx) }

	/**
	 * Visit a parse tree produced by {@link MatmParser#exponentiation}.
	 * @param ctx the parse tree
	 */
	fn visit_exponentiation(&mut self, ctx: &ExponentiationContext<'input>) { self.visit_children(ctx) }

	/**
	 * Visit a parse tree produced by {@link MatmParser#exponent}.
	 * @param ctx the parse tree
	 */
	fn visit_exponent(&mut self, ctx: &ExponentContext<'input>) { self.visit_children(ctx) }

	/**
	 * Visit a parse tree produced by {@link MatmParser#simple_factor}.
	 * @param ctx the parse tree
	 */
	fn visit_simple_factor(&mut self, ctx: &Simple_factorContext<'input>) { self.visit_children(ctx) }

	/**
	 * Visit a parse tree produced by {@link MatmParser#fraction}.
	 * @param ctx the parse tree
	 */
	fn visit_fraction(&mut self, ctx: &FractionContext<'input>) { self.visit_children(ctx) }

	/**
	 * Visit a parse tree produced by {@link MatmParser#symbol}.
	 * @param ctx the parse tree
	 */
	fn visit_symbol(&mut self, ctx: &SymbolContext<'input>) { self.visit_children(ctx) }

	/**
	 * Visit a parse tree produced by {@link MatmParser#number}.
	 * @param ctx the parse tree
	 */
	fn visit_number(&mut self, ctx: &NumberContext<'input>) { self.visit_children(ctx) }

	/**
	 * Visit a parse tree produced by {@link MatmParser#methodinv}.
	 * @param ctx the parse tree
	 */
	fn visit_methodinv(&mut self, ctx: &MethodinvContext<'input>) { self.visit_children(ctx) }

	/**
	 * Visit a parse tree produced by {@link MatmParser#functioninv}.
	 * @param ctx the parse tree
	 */
	fn visit_functioninv(&mut self, ctx: &FunctioninvContext<'input>) { self.visit_children(ctx) }

	/**
	 * Visit a parse tree produced by {@link MatmParser#arguments}.
	 * @param ctx the parse tree
	 */
	fn visit_arguments(&mut self, ctx: &ArgumentsContext<'input>) { self.visit_children(ctx) }

	/**
	 * Visit a parse tree produced by {@link MatmParser#assignment}.
	 * @param ctx the parse tree
	 */
	fn visit_assignment(&mut self, ctx: &AssignmentContext<'input>) { self.visit_children(ctx) }

}

pub trait MatmParserVisitorCompat<'input>:ParseTreeVisitorCompat<'input, Node= MatmParserContextType>{
	/**
	 * Visit a parse tree produced by {@link MatmParser#start}.
	 * @param ctx the parse tree
	 */
		fn visit_start(&mut self, ctx: &StartContext<'input>) -> Self::Return {
			self.visit_children(ctx)
		}

	/**
	 * Visit a parse tree produced by {@link MatmParser#statements}.
	 * @param ctx the parse tree
	 */
		fn visit_statements(&mut self, ctx: &StatementsContext<'input>) -> Self::Return {
			self.visit_children(ctx)
		}

	/**
	 * Visit a parse tree produced by {@link MatmParser#expr}.
	 * @param ctx the parse tree
	 */
		fn visit_expr(&mut self, ctx: &ExprContext<'input>) -> Self::Return {
			self.visit_children(ctx)
		}

	/**
	 * Visit a parse tree produced by {@link MatmParser#equation}.
	 * @param ctx the parse tree
	 */
		fn visit_equation(&mut self, ctx: &EquationContext<'input>) -> Self::Return {
			self.visit_children(ctx)
		}

	/**
	 * Visit a parse tree produced by {@link MatmParser#adding_expr}.
	 * @param ctx the parse tree
	 */
		fn visit_adding_expr(&mut self, ctx: &Adding_exprContext<'input>) -> Self::Return {
			self.visit_children(ctx)
		}

	/**
	 * Visit a parse tree produced by {@link MatmParser#term}.
	 * @param ctx the parse tree
	 */
		fn visit_term(&mut self, ctx: &TermContext<'input>) -> Self::Return {
			self.visit_children(ctx)
		}

	/**
	 * Visit a parse tree produced by {@link MatmParser#factor}.
	 * @param ctx the parse tree
	 */
		fn visit_factor(&mut self, ctx: &FactorContext<'input>) -> Self::Return {
			self.visit_children(ctx)
		}

	/**
	 * Visit a parse tree produced by {@link MatmParser#exponentiation}.
	 * @param ctx the parse tree
	 */
		fn visit_exponentiation(&mut self, ctx: &ExponentiationContext<'input>) -> Self::Return {
			self.visit_children(ctx)
		}

	/**
	 * Visit a parse tree produced by {@link MatmParser#exponent}.
	 * @param ctx the parse tree
	 */
		fn visit_exponent(&mut self, ctx: &ExponentContext<'input>) -> Self::Return {
			self.visit_children(ctx)
		}

	/**
	 * Visit a parse tree produced by {@link MatmParser#simple_factor}.
	 * @param ctx the parse tree
	 */
		fn visit_simple_factor(&mut self, ctx: &Simple_factorContext<'input>) -> Self::Return {
			self.visit_children(ctx)
		}

	/**
	 * Visit a parse tree produced by {@link MatmParser#fraction}.
	 * @param ctx the parse tree
	 */
		fn visit_fraction(&mut self, ctx: &FractionContext<'input>) -> Self::Return {
			self.visit_children(ctx)
		}

	/**
	 * Visit a parse tree produced by {@link MatmParser#symbol}.
	 * @param ctx the parse tree
	 */
		fn visit_symbol(&mut self, ctx: &SymbolContext<'input>) -> Self::Return {
			self.visit_children(ctx)
		}

	/**
	 * Visit a parse tree produced by {@link MatmParser#number}.
	 * @param ctx the parse tree
	 */
		fn visit_number(&mut self, ctx: &NumberContext<'input>) -> Self::Return {
			self.visit_children(ctx)
		}

	/**
	 * Visit a parse tree produced by {@link MatmParser#methodinv}.
	 * @param ctx the parse tree
	 */
		fn visit_methodinv(&mut self, ctx: &MethodinvContext<'input>) -> Self::Return {
			self.visit_children(ctx)
		}

	/**
	 * Visit a parse tree produced by {@link MatmParser#functioninv}.
	 * @param ctx the parse tree
	 */
		fn visit_functioninv(&mut self, ctx: &FunctioninvContext<'input>) -> Self::Return {
			self.visit_children(ctx)
		}

	/**
	 * Visit a parse tree produced by {@link MatmParser#arguments}.
	 * @param ctx the parse tree
	 */
		fn visit_arguments(&mut self, ctx: &ArgumentsContext<'input>) -> Self::Return {
			self.visit_children(ctx)
		}

	/**
	 * Visit a parse tree produced by {@link MatmParser#assignment}.
	 * @param ctx the parse tree
	 */
		fn visit_assignment(&mut self, ctx: &AssignmentContext<'input>) -> Self::Return {
			self.visit_children(ctx)
		}

}

impl<'input,T> MatmParserVisitor<'input> for T
where
	T: MatmParserVisitorCompat<'input>
{
	fn visit_start(&mut self, ctx: &StartContext<'input>){
		let result = <Self as MatmParserVisitorCompat>::visit_start(self, ctx);
        *<Self as ParseTreeVisitorCompat>::temp_result(self) = result;
	}

	fn visit_statements(&mut self, ctx: &StatementsContext<'input>){
		let result = <Self as MatmParserVisitorCompat>::visit_statements(self, ctx);
        *<Self as ParseTreeVisitorCompat>::temp_result(self) = result;
	}

	fn visit_expr(&mut self, ctx: &ExprContext<'input>){
		let result = <Self as MatmParserVisitorCompat>::visit_expr(self, ctx);
        *<Self as ParseTreeVisitorCompat>::temp_result(self) = result;
	}

	fn visit_equation(&mut self, ctx: &EquationContext<'input>){
		let result = <Self as MatmParserVisitorCompat>::visit_equation(self, ctx);
        *<Self as ParseTreeVisitorCompat>::temp_result(self) = result;
	}

	fn visit_adding_expr(&mut self, ctx: &Adding_exprContext<'input>){
		let result = <Self as MatmParserVisitorCompat>::visit_adding_expr(self, ctx);
        *<Self as ParseTreeVisitorCompat>::temp_result(self) = result;
	}

	fn visit_term(&mut self, ctx: &TermContext<'input>){
		let result = <Self as MatmParserVisitorCompat>::visit_term(self, ctx);
        *<Self as ParseTreeVisitorCompat>::temp_result(self) = result;
	}

	fn visit_factor(&mut self, ctx: &FactorContext<'input>){
		let result = <Self as MatmParserVisitorCompat>::visit_factor(self, ctx);
        *<Self as ParseTreeVisitorCompat>::temp_result(self) = result;
	}

	fn visit_exponentiation(&mut self, ctx: &ExponentiationContext<'input>){
		let result = <Self as MatmParserVisitorCompat>::visit_exponentiation(self, ctx);
        *<Self as ParseTreeVisitorCompat>::temp_result(self) = result;
	}

	fn visit_exponent(&mut self, ctx: &ExponentContext<'input>){
		let result = <Self as MatmParserVisitorCompat>::visit_exponent(self, ctx);
        *<Self as ParseTreeVisitorCompat>::temp_result(self) = result;
	}

	fn visit_simple_factor(&mut self, ctx: &Simple_factorContext<'input>){
		let result = <Self as MatmParserVisitorCompat>::visit_simple_factor(self, ctx);
        *<Self as ParseTreeVisitorCompat>::temp_result(self) = result;
	}

	fn visit_fraction(&mut self, ctx: &FractionContext<'input>){
		let result = <Self as MatmParserVisitorCompat>::visit_fraction(self, ctx);
        *<Self as ParseTreeVisitorCompat>::temp_result(self) = result;
	}

	fn visit_symbol(&mut self, ctx: &SymbolContext<'input>){
		let result = <Self as MatmParserVisitorCompat>::visit_symbol(self, ctx);
        *<Self as ParseTreeVisitorCompat>::temp_result(self) = result;
	}

	fn visit_number(&mut self, ctx: &NumberContext<'input>){
		let result = <Self as MatmParserVisitorCompat>::visit_number(self, ctx);
        *<Self as ParseTreeVisitorCompat>::temp_result(self) = result;
	}

	fn visit_methodinv(&mut self, ctx: &MethodinvContext<'input>){
		let result = <Self as MatmParserVisitorCompat>::visit_methodinv(self, ctx);
        *<Self as ParseTreeVisitorCompat>::temp_result(self) = result;
	}

	fn visit_functioninv(&mut self, ctx: &FunctioninvContext<'input>){
		let result = <Self as MatmParserVisitorCompat>::visit_functioninv(self, ctx);
        *<Self as ParseTreeVisitorCompat>::temp_result(self) = result;
	}

	fn visit_arguments(&mut self, ctx: &ArgumentsContext<'input>){
		let result = <Self as MatmParserVisitorCompat>::visit_arguments(self, ctx);
        *<Self as ParseTreeVisitorCompat>::temp_result(self) = result;
	}

	fn visit_assignment(&mut self, ctx: &AssignmentContext<'input>){
		let result = <Self as MatmParserVisitorCompat>::visit_assignment(self, ctx);
        *<Self as ParseTreeVisitorCompat>::temp_result(self) = result;
	}

}