#![allow(nonstandard_style)]
// Generated from MatmParser.g4 by ANTLR 4.8
use antlr_rust::tree::ParseTreeListener;
use super::matmparser::*;

pub trait MatmParserListener<'input> : ParseTreeListener<'input,MatmParserContextType>{
/**
 * Enter a parse tree produced by {@link MatmParser#start}.
 * @param ctx the parse tree
 */
fn enter_start(&mut self, _ctx: &StartContext<'input>) { }
/**
 * Exit a parse tree produced by {@link MatmParser#start}.
 * @param ctx the parse tree
 */
fn exit_start(&mut self, _ctx: &StartContext<'input>) { }
/**
 * Enter a parse tree produced by {@link MatmParser#statements}.
 * @param ctx the parse tree
 */
fn enter_statements(&mut self, _ctx: &StatementsContext<'input>) { }
/**
 * Exit a parse tree produced by {@link MatmParser#statements}.
 * @param ctx the parse tree
 */
fn exit_statements(&mut self, _ctx: &StatementsContext<'input>) { }
/**
 * Enter a parse tree produced by {@link MatmParser#expr}.
 * @param ctx the parse tree
 */
fn enter_expr(&mut self, _ctx: &ExprContext<'input>) { }
/**
 * Exit a parse tree produced by {@link MatmParser#expr}.
 * @param ctx the parse tree
 */
fn exit_expr(&mut self, _ctx: &ExprContext<'input>) { }
/**
 * Enter a parse tree produced by {@link MatmParser#equation}.
 * @param ctx the parse tree
 */
fn enter_equation(&mut self, _ctx: &EquationContext<'input>) { }
/**
 * Exit a parse tree produced by {@link MatmParser#equation}.
 * @param ctx the parse tree
 */
fn exit_equation(&mut self, _ctx: &EquationContext<'input>) { }
/**
 * Enter a parse tree produced by {@link MatmParser#adding_expr}.
 * @param ctx the parse tree
 */
fn enter_adding_expr(&mut self, _ctx: &Adding_exprContext<'input>) { }
/**
 * Exit a parse tree produced by {@link MatmParser#adding_expr}.
 * @param ctx the parse tree
 */
fn exit_adding_expr(&mut self, _ctx: &Adding_exprContext<'input>) { }
/**
 * Enter a parse tree produced by {@link MatmParser#term}.
 * @param ctx the parse tree
 */
fn enter_term(&mut self, _ctx: &TermContext<'input>) { }
/**
 * Exit a parse tree produced by {@link MatmParser#term}.
 * @param ctx the parse tree
 */
fn exit_term(&mut self, _ctx: &TermContext<'input>) { }
/**
 * Enter a parse tree produced by {@link MatmParser#factor}.
 * @param ctx the parse tree
 */
fn enter_factor(&mut self, _ctx: &FactorContext<'input>) { }
/**
 * Exit a parse tree produced by {@link MatmParser#factor}.
 * @param ctx the parse tree
 */
fn exit_factor(&mut self, _ctx: &FactorContext<'input>) { }
/**
 * Enter a parse tree produced by {@link MatmParser#exponentiation}.
 * @param ctx the parse tree
 */
fn enter_exponentiation(&mut self, _ctx: &ExponentiationContext<'input>) { }
/**
 * Exit a parse tree produced by {@link MatmParser#exponentiation}.
 * @param ctx the parse tree
 */
fn exit_exponentiation(&mut self, _ctx: &ExponentiationContext<'input>) { }
/**
 * Enter a parse tree produced by {@link MatmParser#exponent}.
 * @param ctx the parse tree
 */
fn enter_exponent(&mut self, _ctx: &ExponentContext<'input>) { }
/**
 * Exit a parse tree produced by {@link MatmParser#exponent}.
 * @param ctx the parse tree
 */
fn exit_exponent(&mut self, _ctx: &ExponentContext<'input>) { }
/**
 * Enter a parse tree produced by {@link MatmParser#simple_factor}.
 * @param ctx the parse tree
 */
fn enter_simple_factor(&mut self, _ctx: &Simple_factorContext<'input>) { }
/**
 * Exit a parse tree produced by {@link MatmParser#simple_factor}.
 * @param ctx the parse tree
 */
fn exit_simple_factor(&mut self, _ctx: &Simple_factorContext<'input>) { }
/**
 * Enter a parse tree produced by {@link MatmParser#fraction}.
 * @param ctx the parse tree
 */
fn enter_fraction(&mut self, _ctx: &FractionContext<'input>) { }
/**
 * Exit a parse tree produced by {@link MatmParser#fraction}.
 * @param ctx the parse tree
 */
fn exit_fraction(&mut self, _ctx: &FractionContext<'input>) { }
/**
 * Enter a parse tree produced by {@link MatmParser#symbol}.
 * @param ctx the parse tree
 */
fn enter_symbol(&mut self, _ctx: &SymbolContext<'input>) { }
/**
 * Exit a parse tree produced by {@link MatmParser#symbol}.
 * @param ctx the parse tree
 */
fn exit_symbol(&mut self, _ctx: &SymbolContext<'input>) { }
/**
 * Enter a parse tree produced by {@link MatmParser#number}.
 * @param ctx the parse tree
 */
fn enter_number(&mut self, _ctx: &NumberContext<'input>) { }
/**
 * Exit a parse tree produced by {@link MatmParser#number}.
 * @param ctx the parse tree
 */
fn exit_number(&mut self, _ctx: &NumberContext<'input>) { }
/**
 * Enter a parse tree produced by {@link MatmParser#methodinv}.
 * @param ctx the parse tree
 */
fn enter_methodinv(&mut self, _ctx: &MethodinvContext<'input>) { }
/**
 * Exit a parse tree produced by {@link MatmParser#methodinv}.
 * @param ctx the parse tree
 */
fn exit_methodinv(&mut self, _ctx: &MethodinvContext<'input>) { }
/**
 * Enter a parse tree produced by {@link MatmParser#functioninv}.
 * @param ctx the parse tree
 */
fn enter_functioninv(&mut self, _ctx: &FunctioninvContext<'input>) { }
/**
 * Exit a parse tree produced by {@link MatmParser#functioninv}.
 * @param ctx the parse tree
 */
fn exit_functioninv(&mut self, _ctx: &FunctioninvContext<'input>) { }
/**
 * Enter a parse tree produced by {@link MatmParser#arguments}.
 * @param ctx the parse tree
 */
fn enter_arguments(&mut self, _ctx: &ArgumentsContext<'input>) { }
/**
 * Exit a parse tree produced by {@link MatmParser#arguments}.
 * @param ctx the parse tree
 */
fn exit_arguments(&mut self, _ctx: &ArgumentsContext<'input>) { }
/**
 * Enter a parse tree produced by {@link MatmParser#assignment}.
 * @param ctx the parse tree
 */
fn enter_assignment(&mut self, _ctx: &AssignmentContext<'input>) { }
/**
 * Exit a parse tree produced by {@link MatmParser#assignment}.
 * @param ctx the parse tree
 */
fn exit_assignment(&mut self, _ctx: &AssignmentContext<'input>) { }

}

antlr_rust::coerce_from!{ 'input : MatmParserListener<'input> }


