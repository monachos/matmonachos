// Generated from MatmLexer.g4 by ANTLR 4.8
#![allow(dead_code)]
#![allow(nonstandard_style)]
#![allow(unused_imports)]
#![allow(unused_variables)]
use antlr_rust::atn::ATN;
use antlr_rust::char_stream::CharStream;
use antlr_rust::int_stream::IntStream;
use antlr_rust::lexer::{BaseLexer, Lexer, LexerRecog};
use antlr_rust::atn_deserializer::ATNDeserializer;
use antlr_rust::dfa::DFA;
use antlr_rust::lexer_atn_simulator::{LexerATNSimulator, ILexerATNSimulator};
use antlr_rust::PredictionContextCache;
use antlr_rust::recognizer::{Recognizer,Actions};
use antlr_rust::error_listener::ErrorListener;
use antlr_rust::TokenSource;
use antlr_rust::token_factory::{TokenFactory,CommonTokenFactory,TokenAware};
use antlr_rust::token::*;
use antlr_rust::rule_context::{BaseRuleContext,EmptyCustomRuleContext,EmptyContext};
use antlr_rust::parser_rule_context::{ParserRuleContext,BaseParserRuleContext,cast};
use antlr_rust::vocabulary::{Vocabulary,VocabularyImpl};

use antlr_rust::{lazy_static,Tid,TidAble,TidExt};

use std::sync::Arc;
use std::cell::RefCell;
use std::rc::Rc;
use std::marker::PhantomData;
use std::ops::{Deref, DerefMut};


	pub const STATEMENT_END:isize=1; 
	pub const OPERATOR_EQ:isize=2; 
	pub const OPERATOR_POW:isize=3; 
	pub const OPERATOR_MUL:isize=4; 
	pub const OPERATOR_DIV:isize=5; 
	pub const OPERATOR_FRAC:isize=6; 
	pub const OPERATOR_ADD:isize=7; 
	pub const OPERATOR_SUB:isize=8; 
	pub const ID:isize=9; 
	pub const INT_DECIMAL:isize=10; 
	pub const INT_RADIX_SINGLE:isize=11; 
	pub const INT_RADIX_MULTIPLE:isize=12; 
	pub const WS:isize=13; 
	pub const LPAREN:isize=14; 
	pub const RPAREN:isize=15; 
	pub const COMMA:isize=16; 
	pub const DOT:isize=17; 
	pub const COLON:isize=18;
	pub const channelNames: [&'static str;0+2] = [
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	];

	pub const modeNames: [&'static str;1] = [
		"DEFAULT_MODE"
	];

	pub const ruleNames: [&'static str;18] = [
		"STATEMENT_END", "OPERATOR_EQ", "OPERATOR_POW", "OPERATOR_MUL", "OPERATOR_DIV", 
		"OPERATOR_FRAC", "OPERATOR_ADD", "OPERATOR_SUB", "ID", "INT_DECIMAL", 
		"INT_RADIX_SINGLE", "INT_RADIX_MULTIPLE", "WS", "LPAREN", "RPAREN", "COMMA", 
		"DOT", "COLON"
	];


	pub const _LITERAL_NAMES: [Option<&'static str>;19] = [
		None, Some("';'"), Some("'='"), Some("'^'"), Some("'*'"), Some("'div'"), 
		Some("'/'"), Some("'+'"), Some("'-'"), None, None, None, None, None, Some("'('"), 
		Some("')'"), Some("','"), Some("'.'"), Some("':'")
	];
	pub const _SYMBOLIC_NAMES: [Option<&'static str>;19]  = [
		None, Some("STATEMENT_END"), Some("OPERATOR_EQ"), Some("OPERATOR_POW"), 
		Some("OPERATOR_MUL"), Some("OPERATOR_DIV"), Some("OPERATOR_FRAC"), Some("OPERATOR_ADD"), 
		Some("OPERATOR_SUB"), Some("ID"), Some("INT_DECIMAL"), Some("INT_RADIX_SINGLE"), 
		Some("INT_RADIX_MULTIPLE"), Some("WS"), Some("LPAREN"), Some("RPAREN"), 
		Some("COMMA"), Some("DOT"), Some("COLON")
	];
	lazy_static!{
	    static ref _shared_context_cache: Arc<PredictionContextCache> = Arc::new(PredictionContextCache::new());
		static ref VOCABULARY: Box<dyn Vocabulary> = Box::new(VocabularyImpl::new(_LITERAL_NAMES.iter(), _SYMBOLIC_NAMES.iter(), None));
	}


pub type LexerContext<'input> = BaseRuleContext<'input,EmptyCustomRuleContext<'input,LocalTokenFactory<'input> >>;
pub type LocalTokenFactory<'input> = CommonTokenFactory;

type From<'a> = <LocalTokenFactory<'a> as TokenFactory<'a> >::From;

pub struct MatmLexer<'input, Input:CharStream<From<'input> >> {
	base: BaseLexer<'input,MatmLexerActions,Input,LocalTokenFactory<'input>>,
}

antlr_rust::tid! { impl<'input,Input> TidAble<'input> for MatmLexer<'input,Input> where Input:CharStream<From<'input> > }

impl<'input, Input:CharStream<From<'input> >> Deref for MatmLexer<'input,Input>{
	type Target = BaseLexer<'input,MatmLexerActions,Input,LocalTokenFactory<'input>>;

	fn deref(&self) -> &Self::Target {
		&self.base
	}
}

impl<'input, Input:CharStream<From<'input> >> DerefMut for MatmLexer<'input,Input>{
	fn deref_mut(&mut self) -> &mut Self::Target {
		&mut self.base
	}
}


impl<'input, Input:CharStream<From<'input> >> MatmLexer<'input,Input>{
    fn get_rule_names(&self) -> &'static [&'static str] {
        &ruleNames
    }
    fn get_literal_names(&self) -> &[Option<&str>] {
        &_LITERAL_NAMES
    }

    fn get_symbolic_names(&self) -> &[Option<&str>] {
        &_SYMBOLIC_NAMES
    }

    fn get_grammar_file_name(&self) -> &'static str {
        "MatmLexer.g4"
    }

	pub fn new_with_token_factory(input: Input, tf: &'input LocalTokenFactory<'input>) -> Self {
		antlr_rust::recognizer::check_version("0","3");
    	Self {
			base: BaseLexer::new_base_lexer(
				input,
				LexerATNSimulator::new_lexer_atnsimulator(
					_ATN.clone(),
					_decision_to_DFA.clone(),
					_shared_context_cache.clone(),
				),
				MatmLexerActions{},
				tf
			)
	    }
	}
}

impl<'input, Input:CharStream<From<'input> >> MatmLexer<'input,Input> where &'input LocalTokenFactory<'input>:Default{
	pub fn new(input: Input) -> Self{
		MatmLexer::new_with_token_factory(input, <&LocalTokenFactory<'input> as Default>::default())
	}
}

pub struct MatmLexerActions {
}

impl MatmLexerActions{
}

impl<'input, Input:CharStream<From<'input> >> Actions<'input,BaseLexer<'input,MatmLexerActions,Input,LocalTokenFactory<'input>>> for MatmLexerActions{
	}

	impl<'input, Input:CharStream<From<'input> >> MatmLexer<'input,Input>{

}

impl<'input, Input:CharStream<From<'input> >> LexerRecog<'input,BaseLexer<'input,MatmLexerActions,Input,LocalTokenFactory<'input>>> for MatmLexerActions{
}
impl<'input> TokenAware<'input> for MatmLexerActions{
	type TF = LocalTokenFactory<'input>;
}

impl<'input, Input:CharStream<From<'input> >> TokenSource<'input> for MatmLexer<'input,Input>{
	type TF = LocalTokenFactory<'input>;

    fn next_token(&mut self) -> <Self::TF as TokenFactory<'input>>::Tok {
        self.base.next_token()
    }

    fn get_line(&self) -> isize {
        self.base.get_line()
    }

    fn get_char_position_in_line(&self) -> isize {
        self.base.get_char_position_in_line()
    }

    fn get_input_stream(&mut self) -> Option<&mut dyn IntStream> {
        self.base.get_input_stream()
    }

	fn get_source_name(&self) -> String {
		self.base.get_source_name()
	}

    fn get_token_factory(&self) -> &'input Self::TF {
        self.base.get_token_factory()
    }
}



	lazy_static! {
	    static ref _ATN: Arc<ATN> =
	        Arc::new(ATNDeserializer::new(None).deserialize(_serializedATN.chars()));
	    static ref _decision_to_DFA: Arc<Vec<antlr_rust::RwLock<DFA>>> = {
	        let mut dfa = Vec::new();
	        let size = _ATN.decision_to_state.len();
	        for i in 0..size {
	            dfa.push(DFA::new(
	                _ATN.clone(),
	                _ATN.get_decision_state(i),
	                i as isize,
	            ).into())
	        }
	        Arc::new(dfa)
	    };
	}



	const _serializedATN:&'static str =
		"\x03\u{608b}\u{a72a}\u{8133}\u{b9ed}\u{417c}\u{3be7}\u{7786}\u{5964}\x02\
		\x14\x79\x08\x01\x04\x02\x09\x02\x04\x03\x09\x03\x04\x04\x09\x04\x04\x05\
		\x09\x05\x04\x06\x09\x06\x04\x07\x09\x07\x04\x08\x09\x08\x04\x09\x09\x09\
		\x04\x0a\x09\x0a\x04\x0b\x09\x0b\x04\x0c\x09\x0c\x04\x0d\x09\x0d\x04\x0e\
		\x09\x0e\x04\x0f\x09\x0f\x04\x10\x09\x10\x04\x11\x09\x11\x04\x12\x09\x12\
		\x04\x13\x09\x13\x03\x02\x03\x02\x03\x03\x03\x03\x03\x04\x03\x04\x03\x05\
		\x03\x05\x03\x06\x03\x06\x03\x06\x03\x06\x03\x07\x03\x07\x03\x08\x03\x08\
		\x03\x09\x03\x09\x03\x0a\x03\x0a\x07\x0a\x3c\x0a\x0a\x0c\x0a\x0e\x0a\x3f\
		\x0b\x0a\x03\x0a\x07\x0a\x42\x0a\x0a\x0c\x0a\x0e\x0a\x45\x0b\x0a\x03\x0b\
		\x06\x0b\x48\x0a\x0b\x0d\x0b\x0e\x0b\x49\x03\x0c\x06\x0c\x4d\x0a\x0c\x0d\
		\x0c\x0e\x0c\x4e\x03\x0c\x03\x0c\x06\x0c\x53\x0a\x0c\x0d\x0c\x0e\x0c\x54\
		\x03\x0c\x03\x0c\x03\x0d\x06\x0d\x5a\x0a\x0d\x0d\x0d\x0e\x0d\x5b\x03\x0d\
		\x03\x0d\x03\x0d\x06\x0d\x61\x0a\x0d\x0d\x0d\x0e\x0d\x62\x03\x0d\x06\x0d\
		\x66\x0a\x0d\x0d\x0d\x0e\x0d\x67\x03\x0d\x03\x0d\x03\x0e\x03\x0e\x03\x0e\
		\x03\x0e\x03\x0f\x03\x0f\x03\x10\x03\x10\x03\x11\x03\x11\x03\x12\x03\x12\
		\x03\x13\x03\x13\x02\x02\x14\x03\x03\x05\x04\x07\x05\x09\x06\x0b\x07\x0d\
		\x08\x0f\x09\x11\x0a\x13\x0b\x15\x0c\x17\x0d\x19\x0e\x1b\x0f\x1d\x10\x1f\
		\x11\x21\x12\x23\x13\x25\x14\x03\x02\x07\x05\x02\x43\x5c\x61\x61\x63\x7c\
		\x06\x02\x32\x3b\x43\x5c\x61\x61\x63\x7c\x03\x02\x32\x3b\x03\x02\x2a\x2b\
		\x04\x02\x0c\x0c\x22\x22\x02\u{80}\x02\x03\x03\x02\x02\x02\x02\x05\x03\
		\x02\x02\x02\x02\x07\x03\x02\x02\x02\x02\x09\x03\x02\x02\x02\x02\x0b\x03\
		\x02\x02\x02\x02\x0d\x03\x02\x02\x02\x02\x0f\x03\x02\x02\x02\x02\x11\x03\
		\x02\x02\x02\x02\x13\x03\x02\x02\x02\x02\x15\x03\x02\x02\x02\x02\x17\x03\
		\x02\x02\x02\x02\x19\x03\x02\x02\x02\x02\x1b\x03\x02\x02\x02\x02\x1d\x03\
		\x02\x02\x02\x02\x1f\x03\x02\x02\x02\x02\x21\x03\x02\x02\x02\x02\x23\x03\
		\x02\x02\x02\x02\x25\x03\x02\x02\x02\x03\x27\x03\x02\x02\x02\x05\x29\x03\
		\x02\x02\x02\x07\x2b\x03\x02\x02\x02\x09\x2d\x03\x02\x02\x02\x0b\x2f\x03\
		\x02\x02\x02\x0d\x33\x03\x02\x02\x02\x0f\x35\x03\x02\x02\x02\x11\x37\x03\
		\x02\x02\x02\x13\x39\x03\x02\x02\x02\x15\x47\x03\x02\x02\x02\x17\x4c\x03\
		\x02\x02\x02\x19\x59\x03\x02\x02\x02\x1b\x6b\x03\x02\x02\x02\x1d\x6f\x03\
		\x02\x02\x02\x1f\x71\x03\x02\x02\x02\x21\x73\x03\x02\x02\x02\x23\x75\x03\
		\x02\x02\x02\x25\x77\x03\x02\x02\x02\x27\x28\x07\x3d\x02\x02\x28\x04\x03\
		\x02\x02\x02\x29\x2a\x07\x3f\x02\x02\x2a\x06\x03\x02\x02\x02\x2b\x2c\x07\
		\x60\x02\x02\x2c\x08\x03\x02\x02\x02\x2d\x2e\x07\x2c\x02\x02\x2e\x0a\x03\
		\x02\x02\x02\x2f\x30\x07\x66\x02\x02\x30\x31\x07\x6b\x02\x02\x31\x32\x07\
		\x78\x02\x02\x32\x0c\x03\x02\x02\x02\x33\x34\x07\x31\x02\x02\x34\x0e\x03\
		\x02\x02\x02\x35\x36\x07\x2d\x02\x02\x36\x10\x03\x02\x02\x02\x37\x38\x07\
		\x2f\x02\x02\x38\x12\x03\x02\x02\x02\x39\x3d\x09\x02\x02\x02\x3a\x3c\x09\
		\x03\x02\x02\x3b\x3a\x03\x02\x02\x02\x3c\x3f\x03\x02\x02\x02\x3d\x3b\x03\
		\x02\x02\x02\x3d\x3e\x03\x02\x02\x02\x3e\x43\x03\x02\x02\x02\x3f\x3d\x03\
		\x02\x02\x02\x40\x42\x07\x29\x02\x02\x41\x40\x03\x02\x02\x02\x42\x45\x03\
		\x02\x02\x02\x43\x41\x03\x02\x02\x02\x43\x44\x03\x02\x02\x02\x44\x14\x03\
		\x02\x02\x02\x45\x43\x03\x02\x02\x02\x46\x48\x09\x04\x02\x02\x47\x46\x03\
		\x02\x02\x02\x48\x49\x03\x02\x02\x02\x49\x47\x03\x02\x02\x02\x49\x4a\x03\
		\x02\x02\x02\x4a\x16\x03\x02\x02\x02\x4b\x4d\x09\x04\x02\x02\x4c\x4b\x03\
		\x02\x02\x02\x4d\x4e\x03\x02\x02\x02\x4e\x4c\x03\x02\x02\x02\x4e\x4f\x03\
		\x02\x02\x02\x4f\x50\x03\x02\x02\x02\x50\x52\x07\x25\x02\x02\x51\x53\x0a\
		\x05\x02\x02\x52\x51\x03\x02\x02\x02\x53\x54\x03\x02\x02\x02\x54\x52\x03\
		\x02\x02\x02\x54\x55\x03\x02\x02\x02\x55\x56\x03\x02\x02\x02\x56\x57\x07\
		\x25\x02\x02\x57\x18\x03\x02\x02\x02\x58\x5a\x09\x04\x02\x02\x59\x58\x03\
		\x02\x02\x02\x5a\x5b\x03\x02\x02\x02\x5b\x59\x03\x02\x02\x02\x5b\x5c\x03\
		\x02\x02\x02\x5c\x5d\x03\x02\x02\x02\x5d\x65\x07\x25\x02\x02\x5e\x60\x07\
		\x2a\x02\x02\x5f\x61\x0a\x05\x02\x02\x60\x5f\x03\x02\x02\x02\x61\x62\x03\
		\x02\x02\x02\x62\x60\x03\x02\x02\x02\x62\x63\x03\x02\x02\x02\x63\x64\x03\
		\x02\x02\x02\x64\x66\x07\x2b\x02\x02\x65\x5e\x03\x02\x02\x02\x66\x67\x03\
		\x02\x02\x02\x67\x65\x03\x02\x02\x02\x67\x68\x03\x02\x02\x02\x68\x69\x03\
		\x02\x02\x02\x69\x6a\x07\x25\x02\x02\x6a\x1a\x03\x02\x02\x02\x6b\x6c\x09\
		\x06\x02\x02\x6c\x6d\x03\x02\x02\x02\x6d\x6e\x08\x0e\x02\x02\x6e\x1c\x03\
		\x02\x02\x02\x6f\x70\x07\x2a\x02\x02\x70\x1e\x03\x02\x02\x02\x71\x72\x07\
		\x2b\x02\x02\x72\x20\x03\x02\x02\x02\x73\x74\x07\x2e\x02\x02\x74\x22\x03\
		\x02\x02\x02\x75\x76\x07\x30\x02\x02\x76\x24\x03\x02\x02\x02\x77\x78\x07\
		\x3c\x02\x02\x78\x26\x03\x02\x02\x02\x0b\x02\x3d\x43\x49\x4e\x54\x5b\x62\
		\x67\x03\x08\x02\x02";
