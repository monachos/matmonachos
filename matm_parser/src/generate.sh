#!/bin/bash

# List of all ANTLR tool options: https://github.com/antlr/antlr4/blob/master/doc/tool-options.md

java -jar $ANRLR4RUST_PATH -Dlanguage=Rust -o antlr -visitor MatmLexer.g4 MatmParser.g4
