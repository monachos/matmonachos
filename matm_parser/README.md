## Generating files by ANTLR

In order to generate rust source files by ANTLR4 tool you should use [ANTLR4 runtime for Rust programming language](https://github.com/rrevenantt/antlr4rust).

Binary releases can be found [here](https://github.com/rrevenantt/antlr4rust/releases/tag/antlr4-4.8-2-Rust0.3.0-beta).
When the binary file is saved in your local computer you need to define a path to the file using ANRLR4RUST_PATH environment variable.
For example if the tool is located in the file $HOME/antlr4rust/antlr4-4.8-2-Rust0.3.0-beta/antlr4-4.8-2-SNAPSHOT-complete.jar you should define the variable ANRLR4RUST_PATH, e.g. in ~/.bashrc:

```bash
export ANRLR4RUST_PATH=$HOME/antlr4rust/antlr4-4.8-2-Rust0.3.0-beta/antlr4-4.8-2-SNAPSHOT-complete.jar
```

When ANRLR4RUST_PATH variable is defined then you can go into src folder and run generate.sh script.