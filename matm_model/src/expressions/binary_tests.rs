#[cfg(test)]
mod tests {
    use crate::expressions::unary::UnaryData;
    use crate::expressions::ToExpression;
    use crate::expressions::binary::BinaryData;
    use crate::factories::ObjFactory;

    #[test]
    fn test_should_wrap_left_for_int_int() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::mul(
            fa.rzi_u(2),
            fa.rzi_u(3),
        );
        let result = expr.should_wrap_left_with_parentheses();
        assert_eq!(false, result);
    }

    #[test]
    fn test_should_wrap_left_for_bin_int() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::mul(
            BinaryData::add(fa.rzi_u(2), fa.rzi_u(2)).to_rc_expr(),
            fa.rzi_u(3),
        );
        let result = expr.should_wrap_left_with_parentheses();
        assert_eq!(true, result);
    }

    #[test]
    fn test_should_wrap_right_for_int_int() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::mul(
            fa.rzi_u(2),
            fa.rzi_u(3),
        );
        let result = expr.should_wrap_right_with_parentheses();
        assert_eq!(false, result);
    }

    #[test]
    fn test_should_wrap_right_for_int_bin() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::mul(
            fa.rzi_u(3),
            BinaryData::add(fa.rzi_u(2), fa.rzi_u(2)).to_rc_expr(),
        );
        let result = expr.should_wrap_right_with_parentheses();
        assert_eq!(true, result);
    }

    #[test]
    fn test_format_add() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::add(
            fa.rzi_u(2),
            fa.rzi_u(3),
        ).to_expr();

        assert_eq!("2 + 3", format!("{}", expr));
    }

    #[test]
    fn test_format_add_positive_negative() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::add(
            fa.rzi_u(2),
            fa.rzi_u(-3),
        ).to_expr();

        assert_eq!("2 + (-3)", format!("{}", expr));
    }

    #[test]
    fn test_format_add_positive_neg() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::add(
            fa.rzi_u(2),
            UnaryData::neg(fa.rzi_u(3)).to_rc_expr(),
        ).to_expr();

        assert_eq!("2 + (-3)", format!("{}", expr));
    }

    #[test]
    fn test_format_add_negative_positive() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::add(
            fa.rzi_u(-2),
            fa.rzi_u(3),
        ).to_expr();

        assert_eq!("(-2) + 3", format!("{}", expr));
    }

    #[test]
    fn test_format_add_neg_positive() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::add(
            UnaryData::neg(fa.rzi_u(2)).to_rc_expr(),
            fa.rzi_u(3),
        ).to_expr();

        assert_eq!("(-2) + 3", format!("{}", expr));
    }

    //---------------------------------------------------

    #[test]
    fn test_format_sub() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::sub(
            fa.rzi_u(2),
            fa.rzi_u(3),
        ).to_expr();

        assert_eq!("2 - 3", format!("{}", expr));
    }

    #[test]
    fn test_format_sub_positive_negative() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::sub(
            fa.rzi_u(2),
            fa.rzi_u(-3),
        ).to_expr();

        assert_eq!("2 - (-3)", format!("{}", expr));
    }

    #[test]
    fn test_format_sub_negative_positive() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::sub(
            fa.rzi_u(-2),
            fa.rzi_u(3),
        ).to_expr();

        assert_eq!("(-2) - 3", format!("{}", expr));
    }

    //---------------------------------------------------

    #[test]
    fn test_format_mul() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::mul(
            fa.rzi_u(2),
            fa.rzi_u(3),
        ).to_expr();

        assert_eq!("2 * 3", format!("{}", expr));
    }

    #[test]
    fn test_format_mul_positive_negative() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::mul(
            fa.rzi_u(2),
            fa.rzi_u(-3),
        ).to_expr();

        assert_eq!("2 * (-3)", format!("{}", expr));
    }

    #[test]
    fn test_format_mul_negative_positive() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::mul(
            fa.rzi_u(-2),
            fa.rzi_u(3),
        ).to_expr();

        assert_eq!("(-2) * 3", format!("{}", expr));
    }

    //---------------------------------------------------

    #[test]
    fn test_format_div() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::div(
            fa.rzi_u(2),
            fa.rzi_u(3),
        ).to_expr();

        assert_eq!("2 div 3", format!("{}", expr));
    }

    #[test]
    fn test_format_div_positive_negative() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::div(
            fa.rzi_u(2),
            fa.rzi_u(-3),
        ).to_expr();

        assert_eq!("2 div (-3)", format!("{}", expr));
    }

    #[test]
    fn test_format_div_negative_positive() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::div(
            fa.rzi_u(-2),
            fa.rzi_u(3),
        ).to_expr();

        assert_eq!("(-2) div 3", format!("{}", expr));
    }

    //---------------------------------------------------

    #[test]
    fn test_format_sum_sub_sum() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::sub(
            BinaryData::add(
                fa.rzi_u(1),
                fa.rzi_u(2),
            ).to_rc_expr(),
            BinaryData::add(
                fa.rzi_u(3),
                fa.rzi_u(4),
            ).to_rc_expr(),
        ).to_expr();

        assert_eq!("1 + 2 - (3 + 4)", format!("{}", expr));
    }

    #[test]
    fn test_format_diff_sub_diff() {
        let fa = ObjFactory::new10();
        let expr = BinaryData::sub(
            BinaryData::sub(
                fa.rzi_u(1),
                fa.rzi_u(2),
            ).to_rc_expr(),
            BinaryData::sub(
                fa.rzi_u(3),
                fa.rzi_u(4),
            ).to_rc_expr(),
        ).to_expr();

        assert_eq!("1 - 2 - (3 - 4)", format!("{}", expr));
    }

}
