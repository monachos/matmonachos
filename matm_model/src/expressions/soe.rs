use std::fmt;
use std::rc::Rc;

use crate::expressions::Expression;
use crate::expressions::ToExpression;
use crate::expressions::precedence::OperatorPrec;
use crate::expressions::precedence::OperatorPrecAccess;

#[derive(Clone, Debug)]
pub struct SysOfEqData {
    pub equations: Vec<Rc<Expression>>,
}

impl SysOfEqData {
    pub fn from_vector(equations: Vec<Rc<Expression>>) -> Self {
        Self {
            equations,
        }
    }

    pub fn has_symbol(&self) -> bool {
        return self.equations.iter().any(|e| e.has_symbol());
    }
}

impl ToExpression for SysOfEqData {
    fn to_expr(self) -> Expression {
        return Expression::SysOfEq(Box::new(self));
    }

    fn to_rc_expr(self) -> Rc<Expression> {
        return Rc::new(self.to_expr());
    }
}

impl OperatorPrecAccess for SysOfEqData {
    fn operator_prec(&self) -> OperatorPrec {
        return OperatorPrec::EquationSystem;
    }
}

impl PartialEq for SysOfEqData {
    //TODO test
    fn eq(&self, other: &SysOfEqData) -> bool {
        if self.equations.len() != other.equations.len() {
            return false;
        }
        for i in 0..self.equations.len() {
            let left = &self.equations[i];
            let right = &other.equations[i];
            if !left.eq(right) {
                return false;
            }
        }
        return true;
    }
}

impl fmt::Display for SysOfEqData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "{{")?;
        for eq in &self.equations {
            writeln!(f, "{}", eq)?;
        }
        write!(f, "}}")?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::testing::asserts::assert_expression_that;
    use crate::expressions::ToExpression;
    use crate::expressions::symbol::SymbolData;
    use crate::factories::ObjFactory;
    use crate::expressions::equation::EquationData;
    use crate::expressions::soe::SysOfEqData;

    #[test]
    fn test_one() {
        let fa = ObjFactory::new10();
        let eqs = SysOfEqData::from_vector(
            vec![EquationData::new(
                SymbolData::new("y").to_rc_expr(),
                fa.rzi_u(3)
            ).to_rc_expr()]
        ).to_rc_expr();

        assert_expression_that(&eqs)
            .as_soe()
            .has_equations_size(1)
            .has_equation_i_that(
                0,
                |eq| {
                    eq.as_equation()
                        .is_left_symbol_that(|s| { s.has_name("y"); })
                        .has_right_that(|e| { e.as_integer().is_int(3); })
                    ;
                });
    }

    #[test]
    fn test_format_one() {
        let fa = ObjFactory::new10();
        let eqs = SysOfEqData::from_vector(
            vec![EquationData::new(
                SymbolData::new("y").to_rc_expr(),
                fa.rzi_u(3)
            ).to_rc_expr()
            ]);

        assert_eq!(
            vec![
                "{",
                "y = 3",
                "}"].join("\n"),
            format!("{}", eqs));
    }

    #[test]
    fn test_format_two() {
        let fa = ObjFactory::new10();
        let eqs = SysOfEqData::from_vector(
            vec![
                EquationData::new(
                    SymbolData::new("x").to_rc_expr(),
                    fa.rzi_u(1)
                ).to_rc_expr(),
                EquationData::new(
                    SymbolData::new("y").to_rc_expr(),
                    fa.rzi_u(2)
                ).to_rc_expr(),
            ]);

        assert_eq!(
            vec![
                "{",
                "x = 1",
                "y = 2",
                "}"].join("\n"),
            format!("{}", eqs));
    }
}