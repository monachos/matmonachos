use std::fmt;
use std::rc::Rc;
use crate::expressions::Expression;
use crate::expressions::ToExpression;
use crate::expressions::precedence::OperatorPrec;
use crate::expressions::precedence::OperatorPrecAccess;


#[derive(Clone)]
#[derive(PartialEq)]
#[derive(Debug)]
pub struct RootData {
    pub radicand: Rc<Expression>,
    pub degree: Rc<Expression>,
}

impl RootData {
    pub fn new(radicand: Rc<Expression>, degree: Rc<Expression>) -> Self {
        Self {
            radicand: radicand,
            degree: degree,
        }
    }

    pub fn has_symbol(&self) -> bool {
        return self.radicand.has_symbol() || self.degree.has_symbol();
    }
}

impl ToExpression for RootData {
    fn to_expr(self) -> Expression {
        return Expression::Root(Box::new(self));
    }

    fn to_rc_expr(self) -> Rc<Expression> {
        return Rc::new(self.to_expr());
    }
}

impl OperatorPrecAccess for RootData {
    fn operator_prec(&self) -> OperatorPrec {
        return OperatorPrec::Atom;
    }
}

impl fmt::Display for RootData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "root({}, {})", self.radicand, self.degree)?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::expressions::ToExpression;
    use crate::expressions::root::RootData;
    use crate::factories::ObjFactory;


    #[test]
    fn test_format_root() {
        let fa = ObjFactory::new10();
        let expr = RootData::new(
            fa.rzi_u(3),
            fa.rzi_u(2),
        ).to_expr();

        assert_eq!("root(3, 2)", format!("{}", expr));
    }

    #[test]
    fn test_format_root_negative() {
        let fa = ObjFactory::new10();
        let expr = RootData::new(
            fa.rzi_u(-3),
            fa.rzi_u(-2),
        ).to_expr();

        assert_eq!("root(-3, -2)", format!("{}", expr));
    }
}
