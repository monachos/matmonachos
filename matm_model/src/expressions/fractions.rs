use std::fmt;
use std::rc::Rc;
use crate::expressions::Expression;
use crate::expressions::ToExpression;
use crate::expressions::precedence::OperatorPrec;
use crate::expressions::precedence::OperatorPrecAccess;


#[derive(Clone)]
pub struct FracData {
    pub numerator: Rc<Expression>,
    pub denominator: Rc<Expression>,
}

impl FracData {
    pub fn new(num: Rc<Expression>, den: Rc<Expression>) -> Self {
        Self {
            numerator: num,
            denominator: den,
        }
    }

    pub fn is_zero(&self) -> bool {
        self.numerator.is_zero()
    }

    pub fn is_one(&self) -> bool {
        !self.numerator.is_zero() && self.numerator.eq(&self.denominator)
    }

    pub fn has_symbol(&self) -> bool {
        return self.numerator.has_symbol() || self.denominator.has_symbol();
    }

}


impl ToExpression for FracData {
    fn to_expr(self) -> Expression {
        return Expression::Frac(Box::new(self));
    }

    fn to_rc_expr(self) -> Rc<Expression> {
        return Rc::new(self.to_expr());
    }
}

impl OperatorPrecAccess for FracData {
    fn operator_prec(&self) -> OperatorPrec {
        return OperatorPrec::Fraction;
    }
}

impl PartialEq for FracData {
    fn eq(&self, other: &FracData) -> bool {
        // do not calculate
        return self.numerator.eq(&other.numerator)
            && self.denominator.eq(&other.denominator);
    }
}

impl fmt::Display for FracData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let num_t = if needs_parenthesis_for_expression(&self.numerator) {
            format!("({})", self.numerator)
        } else {
            format!("{}", self.numerator)
        };
        let den_t = if needs_parenthesis_for_expression(&self.denominator) {
            format!("({})", self.denominator)
        } else {
            format!("{}", self.denominator)
        };
        write!(f, "{}/{}", num_t, den_t)
    }
}

fn needs_parenthesis_for_expression(expr: &Rc<Expression>) -> bool {
    return OperatorPrec::Atom > expr.operator_prec();
}

impl fmt::Debug for FracData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "Expression::Frac{{ {:#?}, {:#?} }}", self.numerator, self.denominator)
    }
}

#[cfg(test)]
mod tests {
    use crate::expressions::binary::BinaryData;
    use crate::expressions::Expression;
    use crate::expressions::ToExpression;
    use crate::expressions::symbol::SymbolData;
    use crate::expressions::fractions::FracData;
    use crate::factories::ObjFactory;


    #[test]
    fn test_new() {
        let fa = ObjFactory::new10();
        let expr = FracData::new(
            fa.rzi_u(1),
            SymbolData::new("x").to_rc_expr()
        );

        match expr.numerator.as_ref() {
            Expression::Int(_) => {
            }
            _ => {
                assert_eq!(true, false);
            }
        }

        match expr.denominator.as_ref() {
            Expression::Symbol(_) => {
            }
            _ => {
                assert_eq!(true, false);
            }
        }
    }

    #[test]
    fn test_is_zero_0_1() {
        let fa = ObjFactory::new10();
        let expr = FracData::new(
            fa.rzi_u(0),
            fa.rzi_u(1)
        );

        assert_eq!(true, expr.is_zero());
    }

    #[test]
    fn test_is_zero_0_x() {
        let fa = ObjFactory::new10();
        let expr = FracData::new(
            fa.rzi_u(0),
            SymbolData::new("x").to_rc_expr()
        );

        assert_eq!(true, expr.is_zero());
    }

    #[test]
    fn test_is_zero_1_1() {
        let fa = ObjFactory::new10();
        let expr = FracData::new(
            fa.rzi_u(1),
            fa.rzi_u(1),
        );

        assert_eq!(false, expr.is_zero());
    }

    #[test]
    fn test_is_one_1_1() {
        let fa = ObjFactory::new10();
        let expr = FracData::new(
            fa.rzi_u(1),
            fa.rzi_u(1),
        );

        assert_eq!(true, expr.is_one());
    }

    #[test]
    fn test_is_one_2_2() {
        let fa = ObjFactory::new10();
        let expr = FracData::new(
            fa.rzi_u(2),
            fa.rzi_u(2),
        );

        assert_eq!(true, expr.is_one());
    }

    #[test]
    fn test_is_one_x_x() {
        let expr = FracData::new(
            SymbolData::new("x").to_rc_expr(),
            SymbolData::new("x").to_rc_expr()
        );

        assert_eq!(true, expr.is_one());
    }

    #[test]
    fn test_is_one_0_1() {
        let fa = ObjFactory::new10();
        let expr = FracData::new(
            fa.rzi_u(0),
            fa.rzi_u(1),
        );

        assert_eq!(false, expr.is_one());
    }

    #[test]
    fn test_is_one_1_2() {
        let fa = ObjFactory::new10();
        let expr = FracData::new(
            fa.rzi_u(1),
            fa.rzi_u(2),
        );

        assert_eq!(false, expr.is_one());
    }

    #[test]
    fn test_format_int_int() {
        let fa = ObjFactory::new10();
        let expr = FracData::new(
            fa.rzi_u(1),
            fa.rzi_u(2),
        );

        assert_eq!("1/2", format!("{}", expr));
    }

    #[test]
    fn test_format_int_symbol() {
        let fa = ObjFactory::new10();
        let expr = FracData::new(
            fa.rzi_u(1),
            fa.rsn("x"),
        );

        assert_eq!("1/x", format!("{}", expr));
    }

    #[test]
    fn test_format_int_add() {
        let fa = ObjFactory::new10();
        let expr = FracData::new(
            fa.rzi_u(1),
            BinaryData::add(fa.rzi_u(2), fa.rzi_u(3)).to_rc_expr(),
        );

        assert_eq!("1/(2 + 3)", format!("{}", expr));
    }

    #[test]
    fn test_format_add_int() {
        let fa = ObjFactory::new10();
        let expr = FracData::new(
            BinaryData::add(fa.rzi_u(1), fa.rzi_u(2)).to_rc_expr(),
            fa.rzi_u(3),
        );

        assert_eq!("(1 + 2)/3", format!("{}", expr));
    }

    #[test]
    fn test_format_add_sub() {
        let fa = ObjFactory::new10();
        let expr = FracData::new(
            BinaryData::add(fa.rzi_u(1), fa.rzi_u(2)).to_rc_expr(),
            BinaryData::sub(fa.rzi_u(3), fa.rzi_u(4)).to_rc_expr(),
        );

        assert_eq!("(1 + 2)/(3 - 4)", format!("{}", expr));
    }
}
