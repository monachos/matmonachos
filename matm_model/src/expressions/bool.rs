use std::fmt;
use std::rc::Rc;
use crate::expressions::Expression;
use crate::expressions::ToExpression;
use crate::expressions::condition::Condition;
use crate::expressions::condition::ToCondition;
use crate::expressions::precedence::OperatorPrec;
use crate::expressions::precedence::OperatorPrecAccess;


#[derive(Clone, Debug, PartialEq)]
pub struct BoolData {
    value: bool,
}

impl BoolData {
    pub fn new(value: bool) -> Self {
        Self {
            value: value
        }
    }

    pub fn true_value() -> Self {
        Self::new(true)
    }

    pub fn false_value() -> Self {
        Self::new(false)
    }

    pub fn value(&self) -> bool {
        self.value
    }
}

impl OperatorPrecAccess for BoolData {
    fn operator_prec(&self) -> OperatorPrec {
        return OperatorPrec::Atom;
    }
}

impl ToExpression for BoolData {
    fn to_expr(self) -> Expression {
        Expression::Bool(Box::new(self))
    }

    fn to_rc_expr(self) -> Rc<Expression> {
        Rc::new(self.to_expr())
    }
}

impl ToCondition for BoolData {
    fn to_cond(self) -> Condition {
        Condition::Bool(self)
    }

    fn to_rc_cond(self) -> Rc<Condition> {
        Rc::new(self.to_cond())
    }
}

impl fmt::Display for BoolData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.value {
            write!(f, "true")
        } else {
            write!(f, "false")
        }
    }
}

#[cfg(test)]
mod tests {
    use ::rstest::rstest;
    use crate::expressions::bool::BoolData;

    #[rstest]
    #[case(false)]
    #[case(true)]
    fn test_bool_new(#[case] value: bool) {
        let o = BoolData::new(value);

        assert_eq!(value, o.value());
    }

    #[rstest]
    #[case(false, "false")]
    #[case(true, "true")]
    fn test_bool_format(#[case] value: bool, #[case] expected: &str) {
        let o = BoolData::new(value);

        assert_eq!(expected, format!("{}", o));
    }
}
