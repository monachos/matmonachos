#[cfg(test)]
mod tests {
    use crate::expressions::Expression;

    #[test]
    fn test_boxsign_format() {
        let expr = Expression::BoxSign;
        assert_eq!("()", format!("{}", expr));
    }
}
