use std::fmt;
use std::rc::Rc;
use crate::expressions::condition::Condition;
use crate::expressions::arithmetic_operator::ArithmeticOperator;
use crate::expressions::condition::ToCondition;
use crate::expressions::Expression;
use crate::expressions::precedence::OperatorPrec;
use crate::expressions::precedence::OperatorPrecAccess;
use crate::write_wrapped_obj;

#[derive(Clone, Debug, PartialEq)]
pub struct ArithmeticCondition {
    pub operator: ArithmeticOperator,
    pub left: Rc<Expression>,
    pub right: Rc<Expression>,
}

impl ArithmeticCondition {
    pub fn from_operator(
        operator: ArithmeticOperator,
        left: Rc<Expression>,
        right: Rc<Expression>) -> Self {
        Self {
            operator: operator,
            left: left,
            right: right,
        }
    }

    pub fn eq(left: Rc<Expression>, right: Rc<Expression>) -> Self {
        Self::from_operator(ArithmeticOperator::Equal, left, right)
    }

    pub fn neq(left: Rc<Expression>, right: Rc<Expression>) -> Self {
        Self::from_operator(ArithmeticOperator::NotEqual, left, right)
    }

    pub fn gt(left: Rc<Expression>, right: Rc<Expression>) -> Self {
        Self::from_operator(ArithmeticOperator::Greater, left, right)
    }

    pub fn gte(left: Rc<Expression>, right: Rc<Expression>) -> Self {
        Self::from_operator(ArithmeticOperator::GreaterOrEqual, left, right)
    }

    pub fn lt(left: Rc<Expression>, right: Rc<Expression>) -> Self {
        Self::from_operator(ArithmeticOperator::Less, left, right)
    }

    pub fn lte(left: Rc<Expression>, right: Rc<Expression>) -> Self {
        Self::from_operator(ArithmeticOperator::LessOrEqual, left, right)
    }

    pub fn has_symbol(&self) -> bool {
        return self.left.has_symbol() || self.right.has_symbol();
    }

    pub fn should_wrap_left_with_parentheses(&self) -> bool {
        self.left.operator_prec() < self.operator_prec()
    }

    pub fn should_wrap_right_with_parentheses(&self) -> bool {
        self.right.operator_prec() < self.operator_prec()
    }
}

impl OperatorPrecAccess for ArithmeticCondition {
    fn operator_prec(&self) -> OperatorPrec {
        return self.operator.operator_prec();
    }
}

impl ToCondition for ArithmeticCondition {
    fn to_cond(self) -> Condition {
        Condition::Arithmetic(self)
    }

    fn to_rc_cond(self) -> Rc<Condition> {
        Rc::new(self.to_cond())
    }
}

impl fmt::Display for ArithmeticCondition {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write_wrapped_obj(f, &self.left, self.should_wrap_left_with_parentheses())?;
        write!(f, " {} ", self.operator)?;
        write_wrapped_obj(f, &self.right, self.should_wrap_right_with_parentheses())?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::expressions::arithmetic_condition::ArithmeticCondition;
    use crate::expressions::binary::BinaryData;
    use crate::expressions::condition::ToCondition;
    use crate::expressions::ToExpression;
    use crate::factories::ObjFactory;

    #[test]
    fn test_arithmetic_condition_format() {
        let fa = ObjFactory::new10();
        let cond = ArithmeticCondition::gt(fa.rsn("x"), fa.rz0()).to_cond();
        let result = format!("{}", cond);
        assert_eq!("x > 0", result);
    }

    #[test]
    fn test_should_wrap_left_for_int_int() {
        let fa = ObjFactory::new10();
        let expr = ArithmeticCondition::gt(
            fa.rzi_u(2),
            fa.rzi_u(3),
        );
        let result = expr.should_wrap_left_with_parentheses();
        assert_eq!(false, result);
    }

    #[test]
    fn test_should_wrap_left_for_bin_int() {
        let fa = ObjFactory::new10();
        let expr = ArithmeticCondition::gt(
            BinaryData::add(fa.rzi_u(2), fa.rzi_u(2)).to_rc_expr(),
            fa.rzi_u(3),
        );
        let result = expr.should_wrap_left_with_parentheses();
        assert_eq!(false, result);
    }

    #[test]
    fn test_should_wrap_right_for_int_int() {
        let fa = ObjFactory::new10();
        let expr = ArithmeticCondition::gt(
            fa.rzi_u(2),
            fa.rzi_u(3),
        );
        let result = expr.should_wrap_right_with_parentheses();
        assert_eq!(false, result);
    }

    #[test]
    fn test_should_wrap_right_for_int_bin() {
        let fa = ObjFactory::new10();
        let expr = ArithmeticCondition::gt(
            fa.rzi_u(3),
            BinaryData::add(fa.rzi_u(2), fa.rzi_u(2)).to_rc_expr(),
        );
        let result = expr.should_wrap_right_with_parentheses();
        assert_eq!(false, result);
    }
}
