#[cfg(test)]
mod tests {
    use rstest::rstest;
    use assert_matches::assert_matches;
    use std::cmp::Ordering;
    use crate::core::natural::Natural;
    use crate::core::natural::RADIX_10;
    use crate::core::natural::RADIX_16;
    use crate::core::positional::PositionalNumber;
    use crate::testing::asserts::assert_result_error_that;
    use crate::testing::asserts::assert_result_ok;
    use crate::expressions::int::IntData;
    use crate::expressions::sign::Sign;
    use crate::factories::ObjFactory;

    
    #[test]
    fn test_zero() {
        let v = assert_result_ok(IntData::zero(RADIX_10));
        assert_eq!(false, v.is_negative());
        assert_eq!(true, v.is_zero());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&0));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_one() {
        let v = assert_result_ok(IntData::one(RADIX_10));
        assert_eq!(false, v.is_negative());
        assert_eq!(false, v.is_zero());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&1));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_int_0() {
        let v = assert_result_ok(IntData::from_int(RADIX_10, 0));
        assert_eq!(false, v.is_negative());
        assert_eq!(true, v.is_zero());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&0));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_int_1() {
        let v = assert_result_ok(IntData::from_int(RADIX_10, 1));

        assert_eq!(false, v.is_negative());
        assert_eq!(false, v.is_zero());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&1));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_int_9() {
        let v = assert_result_ok(IntData::from_int(RADIX_10, 9));

        assert_eq!(false, v.is_negative());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&9));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_int_10() {
        let v = assert_result_ok(IntData::from_int(RADIX_10, 10));

        assert_eq!(false, v.is_negative());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&1));
        assert_eq!(iter.next(), Some(&0));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_int_1234() {
        let v = assert_result_ok(IntData::from_int(RADIX_10, 1234));

        assert_eq!(false, v.is_negative());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&1));
        assert_eq!(iter.next(), Some(&2));
        assert_eq!(iter.next(), Some(&3));
        assert_eq!(iter.next(), Some(&4));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_int_m1() {
        let v = assert_result_ok(IntData::from_int(RADIX_10, -1));
        
        assert_eq!(true, v.is_negative());
        assert_eq!(false, v.is_zero());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&1));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_int_m250() {
        let v = assert_result_ok(IntData::from_int(RADIX_10, -250));
        
        assert_eq!(true, v.is_negative());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&2));
        assert_eq!(iter.next(), Some(&5));
        assert_eq!(iter.next(), Some(&0));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_int_max() {
        let v = assert_result_ok(IntData::from_int(RADIX_10, i32::MAX));
        
        assert_eq!(false, v.is_negative());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&2));
        assert_eq!(iter.next(), Some(&1));
        assert_eq!(iter.next(), Some(&4));
        assert_eq!(iter.next(), Some(&7));
        assert_eq!(iter.next(), Some(&4));
        assert_eq!(iter.next(), Some(&8));
        assert_eq!(iter.next(), Some(&3));
        assert_eq!(iter.next(), Some(&6));
        assert_eq!(iter.next(), Some(&4));
        assert_eq!(iter.next(), Some(&7));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_int_min() {
        let v = assert_result_ok(IntData::from_int(RADIX_10, i32::MIN));
        
        assert_eq!(true, v.is_negative());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&2));
        assert_eq!(iter.next(), Some(&1));
        assert_eq!(iter.next(), Some(&4));
        assert_eq!(iter.next(), Some(&7));
        assert_eq!(iter.next(), Some(&4));
        assert_eq!(iter.next(), Some(&8));
        assert_eq!(iter.next(), Some(&3));
        assert_eq!(iter.next(), Some(&6));
        assert_eq!(iter.next(), Some(&4));
        assert_eq!(iter.next(), Some(&8));
        assert_eq!(iter.next(), None);
    }

    #[rstest]
    #[case(0x0)]
    #[case(0x5)]
    #[case(0x9)]
    #[case(0xA)]
    #[case(0xA)]
    #[case(0xF)]
    #[case(0x12AD)]
    #[case(-0x1)]
    #[case(-0xAF)]
    fn test_from_int_radix16(#[case] value: i32) {
        let expected = value;
        let v = assert_result_ok(IntData::from_int(RADIX_16, value));
        assert_eq!(expected < 0, v.is_negative());
        assert_eq!(v.to_i32(), Some(expected));
    }

    #[test]
    fn test_from_int_max_radix() {
        let v = assert_result_ok(IntData::from_int(36, 1));
        
        assert_eq!(false, v.is_negative());
        assert_eq!(36, v.radix());
    }

    #[test]
    fn test_from_int_bad_radix() {
        assert_result_error_that(IntData::from_int(37, 1))
            .has_message("Specified radix 37 cannot be greater than 36.");
    }

    #[test]
    fn test_from_vector_empty_positive() {
        assert_result_error_that(IntData::from_vector(RADIX_10, Vec::new(), Sign::Positive))
            .has_message("Cannot create number from empty vector.");
    }

    #[test]
    fn test_from_vector_empty_zero() {
        assert_result_error_that(IntData::from_vector(RADIX_10, Vec::new(), Sign::Zero))
            .has_message("Cannot create number from empty vector.");
    }

    #[test]
    fn test_from_vector_empty_negative() {
        assert_result_error_that(IntData::from_vector(RADIX_10, Vec::new(), Sign::Negative))
            .has_message("Cannot create number from empty vector.");
    }

    #[test]
    fn test_from_vector_0_zero() {
        let v = assert_result_ok(IntData::from_vector(RADIX_10, vec![0], Sign::Zero));
        
        assert!(v.is_zero());
        assert_eq!(false, v.is_negative());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&0));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_vector_0_positive() {
        let v = assert_result_ok(IntData::from_vector(RADIX_10, vec![0], Sign::Positive));
        
        assert!(v.is_zero());
        assert_eq!(false, v.is_negative());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&0));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_vector_0_negative() {
        let v = assert_result_ok(IntData::from_vector(RADIX_10, vec![0], Sign::Negative));
        
        assert!(v.is_zero());
        assert_eq!(false, v.is_negative());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&0));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_vector_000_zero() {
        let v = assert_result_ok(IntData::from_vector(RADIX_10, vec![0, 0, 0], Sign::Zero));
        
        assert!(v.is_zero());
        assert_eq!(false, v.is_negative());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&0));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_vector_000_positive() {
        let v = assert_result_ok(IntData::from_vector(RADIX_10, vec![0, 0, 0], Sign::Positive));
        
        assert!(v.is_zero());
        assert_eq!(false, v.is_negative());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&0));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_vector_000_negative() {
        let v = assert_result_ok(IntData::from_vector(RADIX_10, vec![0, 0, 0], Sign::Negative));
        
        assert!(v.is_zero());
        assert_eq!(false, v.is_negative());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&0));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_vector_03() {
        let v = assert_result_ok(IntData::from_vector(RADIX_10, vec![0, 3], Sign::Positive));
        
        assert_eq!(false, v.is_negative());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&3));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_vector_3() {
        let v = assert_result_ok(IntData::from_vector(RADIX_10, vec![3], Sign::Positive));
        
        assert_eq!(false, v.is_negative());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&3));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_vector_m3() {
        let v = assert_result_ok(IntData::from_vector(RADIX_10, vec![3], Sign::Negative));
        
        assert_eq!(true, v.is_negative());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&3));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_vector_1234() {
        let v = assert_result_ok(IntData::from_vector(RADIX_10, vec![1, 2, 3, 4], Sign::Positive));
        
        assert_eq!(false, v.is_negative());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&1));
        assert_eq!(iter.next(), Some(&2));
        assert_eq!(iter.next(), Some(&3));
        assert_eq!(iter.next(), Some(&4));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_vector_max_radix() {
        let v = assert_result_ok(IntData::from_vector(36, vec![1, 2], Sign::Positive));
        
        assert_eq!(false, v.is_negative());
        assert_eq!(36, v.radix());
    }

    #[test]
    fn test_from_vector_bad_radix() {
        assert_result_error_that(IntData::from_vector(37, vec![1, 2], Sign::Positive))
            .has_message("Specified radix 37 cannot be greater than 36.");
    }

    #[test]
    fn test_from_vector_bad_digit() {
        assert_result_error_that(IntData::from_vector(RADIX_10, vec![1, 10], Sign::Positive))
            .has_message("Specified digit 10 is invalid in radix 10.");
    }

    #[test]
    fn test_from_string_0() {
        let v = assert_result_ok(IntData::from_string(RADIX_10, "0"));
        assert_eq!(false, v.is_negative());
        assert_eq!(true, v.is_zero());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&0));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_string_01() {
        let v = assert_result_ok(IntData::from_string(RADIX_10, "01"));

        assert_eq!(false, v.is_negative());
        assert_eq!(false, v.is_zero());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&1));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_string_1() {
        let v = assert_result_ok(IntData::from_string(RADIX_10, "1"));

        assert_eq!(false, v.is_negative());
        assert_eq!(false, v.is_zero());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&1));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_string_1234() {
        let v = assert_result_ok(IntData::from_string(RADIX_10, "1234"));

        assert_eq!(false, v.is_negative());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&1));
        assert_eq!(iter.next(), Some(&2));
        assert_eq!(iter.next(), Some(&3));
        assert_eq!(iter.next(), Some(&4));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_string_m2() {
        let v = assert_result_ok(IntData::from_string(RADIX_10, "-2"));
        
        assert_eq!(true, v.is_negative());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&2));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_string_m250() {
        let v = assert_result_ok(IntData::from_string(RADIX_10, "-250"));
        
        assert_eq!(true, v.is_negative());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&2));
        assert_eq!(iter.next(), Some(&5));
        assert_eq!(iter.next(), Some(&0));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_string_empty() {
        assert_result_error_that(IntData::from_string(RADIX_10, ""))
            .has_message("Cannot create number from empty string.");
    }

    #[test]
    fn test_from_string_bad_digit() {
        assert_result_error_that(IntData::from_string(RADIX_10, "k"))
            .has_message("Cannot create number using the string: 'k'");
    }

    #[test]
    fn test_from_string_bad_character() {
        assert_result_error_that(IntData::from_string(RADIX_10, "*"))
            .has_message("Cannot create number using the string: '*'");
    }

    #[rstest]
    #[case("0", 0x0)]
    #[case("5", 0x5)]
    #[case("9", 0x9)]
    #[case("a", 0xA)]
    #[case("A", 0xA)]
    #[case("F", 0xF)]
    #[case("12AD", 0x12AD)]
    #[case("-1", -0x1)]
    #[case("-AF", -0xAF)]
    fn test_from_string_radix16(
        #[case] input: &str,
        #[case] expected: i32,
    ) {
        let v = assert_result_ok(IntData::from_string(RADIX_16, input));
        assert_eq!(expected < 0, v.is_negative());
        match v.to_i32() {
            Some(i_val) => {
                assert_eq!(expected, i_val);
            }
            None => {
                assert!(false);
            }
        }
    }

    #[test]
    fn test_from_string_max_radix() {
        let v = assert_result_ok(IntData::from_string(36, "1"));
        
        assert_eq!(false, v.is_negative());
        assert_eq!(36, v.radix());
    }

    #[test]
    fn test_from_string_bad_radix() {
        assert_result_error_that(IntData::from_string(37, "1"))
            .has_message("Specified radix 37 cannot be greater than 36.");
    }

    #[test]
    fn test_from_natural_positive() {
        let natural = Natural::from_string(RADIX_10, "123456789").unwrap();
        let result = IntData::from_natural(natural, Sign::Positive);
        assert_eq!("123456789", format!("{}", result));
    }

    #[test]
    fn test_from_natural_zero() {
        let natural = Natural::zero(RADIX_10).unwrap();
        let result = IntData::from_natural(natural, Sign::Negative);
        assert_eq!("0", format!("{}", result));
    }

    #[test]
    fn test_from_natural_negative() {
        let natural = Natural::from_string(RADIX_10, "123456789").unwrap();
        let result = IntData::from_natural(natural, Sign::Negative);
        assert_eq!("-123456789", format!("{}", result));
    }

    #[test]
    fn test_from_natural_custom_radix() {
        let natural = Natural::from_string(13, "1").unwrap();
        let result = IntData::from_natural(natural, Sign::Positive);
        assert_eq!(13, result.radix());
    }

    #[test]
    fn test_from_positional_number() {
        let mut positional = PositionalNumber::new(RADIX_10);
        positional.set_digit(2, 8).unwrap();
        positional.set_digit(1, 4).unwrap();
        let result = IntData::from_positional_number(&positional).unwrap();
        assert_eq!("840", format!("{}", result));
    }

    #[rstest]
    #[case(-250, false)]
    #[case(-5, false)]
    #[case(-1, false)]
    #[case(0, false)]
    #[case(1, true)]
    #[case(10, true)]
    #[case(35, true)]
    fn test_is_positive(#[case] input: i32, #[case] expected: bool) {
        let value = IntData::from_int(RADIX_10, input).unwrap();
        assert_eq!(expected, value.is_positive());
    }

    #[rstest]
    #[case(-250, true)]
    #[case(-5, true)]
    #[case(-1, true)]
    #[case(0, false)]
    #[case(1, false)]
    #[case(10, false)]
    #[case(35, false)]
    fn test_is_negative(#[case] input: i32, #[case] expected: bool) {
        let value = IntData::from_int(RADIX_10, input).unwrap();
        assert_eq!(expected, value.is_negative());
    }

    #[rstest]
    #[case(1, true)]
    #[case(0, false)]
    #[case(-1, false)]
    #[case(5, false)]
    #[case(-5, false)]
    fn test_is_one(#[case] input: i32, #[case] expected: bool) {
        let value = IntData::from_int(RADIX_10, input).unwrap();
        assert_eq!(expected, value.is_one());
    }

    #[rstest]
    #[case(-1, true)]
    #[case(0, false)]
    #[case(1, false)]
    #[case(5, false)]
    #[case(-5, false)]
    fn test_is_minus_one(#[case] input: i32, #[case] expected: bool) {
        let value = IntData::from_int(RADIX_10, input).unwrap();
        assert_eq!(expected, value.is_minus_one());
    }

    fn get_integers() -> Vec<i32> {
        return vec![
            0,
            1,
            2,
            10,
            45,
            1234,
            -1,
            -5,
            -100,
        ];
    }

    #[test]
    fn test_equal() {
        let items = get_integers();
        for value in items {
            let left = IntData::from_int(RADIX_10, value).unwrap();
            let right = IntData::from_int(RADIX_10, value).unwrap();
            assert_eq!(true, left.eq(&right));
        }
    }

    #[test]
    fn test_equal_diff_radix() {
        let items = get_integers();
        for value in items {
            let left = IntData::from_int(RADIX_10, value).unwrap();
            let right = IntData::from_int(RADIX_16, value).unwrap();
            assert_eq!(false, left.eq(&right));
        }
    }

    #[test]
    fn test_partial_cmp_equal() {
        let items = get_integers();
        for item in items {
            let left = IntData::from_int(RADIX_10, item).unwrap();
            let right = IntData::from_int(RADIX_10, item).unwrap();
            assert_matches!(left.partial_cmp(&right), Some(Ordering::Equal));
        }
    }

    #[test]
    fn test_partial_cmp_equal_diff_radix() {
        let items = get_integers();
        for item in items {
            let left = IntData::from_int(RADIX_10, item).unwrap();
            let right = IntData::from_int(RADIX_16, item).unwrap();
            assert_matches!(left.partial_cmp(&right), None);
        }
    }

    fn get_greater_integers() -> Vec<(i32, i32)> {
        return vec![
            (1, 0),
            (3, 0),
            (10, 0),
            (123, 0),
            (2, 1),
            (7, 3),
            (20, 4),
            (23, 22),
            (32, 29),
            (100, 55),
            (0, -1),
            (0, -4),
            (0, -10),
            (3, -3),
            (30, -3),
            (3, -30),
            (-1, -2),
            (-1, -300),
            (-20, -200),
        ];
    }

    #[test]
    fn test_partial_cmp_greater() {
        let items = get_greater_integers();
        for item in items {
            let left = IntData::from_int(RADIX_10, item.0).unwrap();
            let right = IntData::from_int(RADIX_10, item.1).unwrap();
            assert_matches!(left.partial_cmp(&right), Some(Ordering::Greater));
        }
    }

    #[test]
    fn test_partial_cmp_less() {
        let items = get_greater_integers();
        for item in items {
            let left = IntData::from_int(RADIX_10, item.1).unwrap();
            let right = IntData::from_int(RADIX_10, item.0).unwrap();
            assert_matches!(left.partial_cmp(&right), Some(Ordering::Less));
        }
    }

    #[test]
    fn test_partial_cmp_diff_radix() {
        let items = get_greater_integers();
        for item in items {
            let left = IntData::from_int(RADIX_10, item.0).unwrap();
            let right = IntData::from_int(RADIX_16, item.1).unwrap();
            assert_matches!(left.partial_cmp(&right), None);
        }
    }

    #[rstest]
    #[case(0)]
    #[case(1)]
    #[case(2)]
    #[case(10)]
    #[case(45)]
    #[case(1234)]
    #[case(i32::MAX)]
    #[case(-1)]
    #[case(-5)]
    #[case(-100)]
    #[case(i32::MIN)]
    fn test_to_i32(#[case] value: i32) {
        println!("Checking to_i32() for {}", value);
        let v = IntData::from_int(RADIX_10, value).unwrap();
        assert_matches!(v.to_i32(), Some(_value));
    }

    #[rstest]
    #[case("-2147483649")]
    #[case("2147483648")]
    fn test_to_i32_over(#[case] value: &str) {
        println!("Checking to_i32() for {}", value);
        let v = IntData::from_string(RADIX_10, value).unwrap();
        assert_matches!(v.to_i32(), None);
    }

    #[rstest]
    #[case(IntData::zero(RADIX_10).unwrap(), IntData::zero(RADIX_10).unwrap())]
    #[case(IntData::from_int(RADIX_10, 1).unwrap(), IntData::from_int(RADIX_10, -1).unwrap())]
    #[case(IntData::from_int(RADIX_10, 5).unwrap(), IntData::from_int(RADIX_10, -5).unwrap())]
    fn test_neg(
        #[case] left: IntData,
        #[case] right: IntData,
    ) {
        println!("Checking neg() of {}", left);
        // negate left
        assert_eq!(right, left.calculate_neg());
        // negate right
        assert_eq!(left, right.calculate_neg());
    }

    #[test]
    fn test_neg_if() {
        let orig = IntData::from_int(RADIX_10, 1).unwrap();
        assert_eq!(orig, orig.calculate_neg_if(false));
        assert_eq!(orig.calculate_neg(), orig.calculate_neg_if(true));
    }

    #[test]
    fn test_abs() {
        let orig_1 = IntData::from_int(RADIX_10, 1).unwrap();
        assert_eq!(orig_1, orig_1.calculate_abs());

        let orig_m1 = IntData::from_int(RADIX_10, -1).unwrap();
        assert_eq!(orig_m1.calculate_neg(), orig_m1.calculate_abs());
    }

    #[rstest]
    #[case(0, "0")]
    #[case(1, "1")]
    #[case(23, "23")]
    #[case(1234, "1234")]
    #[case(-1, "-1")]
    #[case(-40, "-40")]
    #[case(-1234, "-1234")]
    fn test_format_radix10(
        #[case] input: i32,
        #[case] expected: &str,
    ) {
        assert_eq!(expected, format!("{}", IntData::from_int(RADIX_10, input).unwrap()));
    }

    #[rstest]
    #[case(0x0, "16#0#")]
    #[case(0x1, "16#1#")]
    #[case(0x23, "16#23#")]
    #[case(0x1AF, "16#1af#")]
    #[case(-0x1, "-16#1#")]
    #[case(-0x1AF, "-16#1af#")]
    fn test_format_radix16(
        #[case] input: i32,
        #[case] expected: &str,
    ) {
        assert_eq!(expected, format!("{}", IntData::from_int(RADIX_16, input).unwrap()));
    }

    #[test]
    fn test_partial_eq_int_int() {
        let fa = ObjFactory::new10();
        let e1 = fa.ezi_u(2);
        let e2 = fa.ezi_u(2);

        assert_eq!(true, e1 == e2);
    }

    #[test]
    fn test_partial_neq_int_int() {
        let fa = ObjFactory::new10();
        let e1 = fa.ezi_u(2);
        let e2 = fa.ezi_u(3);

        assert_eq!(false, e1 == e2);
    }

    #[test]
    fn test_partial_eq_int_int16() {
        let fa10 = ObjFactory::new10();
        let e1 = fa10.ezi_u(2);

        let fa16 = ObjFactory::new(RADIX_16);
        let e2 = fa16.ezi_u(2);

        assert_eq!(false, e1 == e2);
    }

}
