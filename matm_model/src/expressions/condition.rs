use std::fmt;
use std::rc::Rc;
use crate::expressions::arithmetic_condition::ArithmeticCondition;
use crate::expressions::bool::BoolData;
use crate::expressions::logic_condition::LogicCondition;
use crate::expressions::precedence::OperatorPrec;
use crate::expressions::precedence::OperatorPrecAccess;


pub trait ToCondition {
    fn to_cond(self) -> Condition;
    fn to_rc_cond(self) -> Rc<Condition>;
}

//------------------------------------------------

#[derive(Clone, Debug, PartialEq)]
pub enum Condition {
    Bool(BoolData),
    Arithmetic(ArithmeticCondition),
    Logic(Box<LogicCondition>),
}

impl Condition {
    pub fn has_symbol(&self) -> bool {
        return match self {
            Self::Bool(_) => false,
            Self::Arithmetic(arithmetic) => arithmetic.has_symbol(),
            Self::Logic(logic) => logic.has_symbol(),
        };
    }
}

impl OperatorPrecAccess for Condition {
    fn operator_prec(&self) -> OperatorPrec {
        return match self {
            Self::Bool(b) => b.operator_prec(),
            Self::Arithmetic(arithmetic) => arithmetic.operator_prec(),
            Self::Logic(logic) => logic.operator_prec(),
        };
    }
}

impl fmt::Display for Condition {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Bool(b) => write!(f, "{}", b),
            Self::Arithmetic(arithmetic) => write!(f, "{}", arithmetic),
            Self::Logic(logic) => write!(f, "{}", logic),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::expressions::arithmetic_condition::ArithmeticCondition;
    use crate::expressions::condition::ToCondition;
    use crate::expressions::logic_condition::LogicCondition;
    use crate::factories::ObjFactory;

    #[test]
    fn test_display_condition() {
        let fa = ObjFactory::new10();
        let cond = LogicCondition::and(
            ArithmeticCondition::gt(fa.rsn("x"), fa.rz0()).to_rc_cond(),
            ArithmeticCondition::lt(fa.rsn("x"), fa.rzi_u(10)).to_rc_cond()
        ).to_cond();
        let result = format!("{}", cond);
        assert_eq!("x > 0 and x < 10", result);
    }
}