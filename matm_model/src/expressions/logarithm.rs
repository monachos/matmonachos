use std::fmt;
use std::rc::Rc;
use crate::expressions::Expression;
use crate::expressions::ToExpression;
use crate::expressions::precedence::OperatorPrec;
use crate::expressions::precedence::OperatorPrecAccess;
use crate::write_wrapped_obj;

#[derive(Clone)]
#[derive(PartialEq)]
#[derive(Debug)]
pub struct LogData {
    pub value: Rc<Expression>,
    pub base: Rc<Expression>,
}

impl LogData {
    pub fn with_value_and_base(value: Rc<Expression>, base: Rc<Expression>) -> Self {
        Self {
            value: value,
            base: base,
        }
    }

    pub fn has_symbol(&self) -> bool {
        return self.base.has_symbol() || self.value.has_symbol();
    }

    pub fn should_wrap_value_with_parentheses(&self) -> bool {
        self.value.operator_prec() < self.operator_prec()
    }

    pub fn should_wrap_base_with_parentheses(&self) -> bool {
        self.base.operator_prec() < self.operator_prec()
    }
}

impl ToExpression for LogData {
    fn to_expr(self) -> Expression {
        return Expression::Log(Box::new(self));
    }

    fn to_rc_expr(self) -> Rc<Expression> {
        return Rc::new(self.to_expr());
    }
}

impl OperatorPrecAccess for LogData {
    fn operator_prec(&self) -> OperatorPrec {
        return OperatorPrec::Function;
    }
}

impl fmt::Display for LogData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "log_")?;
        write_wrapped_obj(f, &self.base, self.should_wrap_base_with_parentheses())?;
        write!(f, "({})", self.value)?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::expressions::ToExpression;
    use crate::expressions::logarithm::LogData;
    use crate::factories::ObjFactory;

    #[test]
    fn test_format_log() {
        let fa = ObjFactory::new10();
        let expr = LogData::with_value_and_base(
            fa.rzi_u(3),
            fa.rzi_u(10),
        ).to_expr();

        assert_eq!("log_10(3)", format!("{}", expr));
    }

    #[test]
    fn test_format_log_negative() {
        let fa = ObjFactory::new10();
        let expr = LogData::with_value_and_base(
            fa.rzi_u(-3),
            fa.rzi_u(-2),
        ).to_expr();

        assert_eq!("log_(-2)(-3)", format!("{}", expr));
    }

}
