use std::fmt;
use std::rc::Rc;
use crate::expressions::Expression;
use crate::expressions::ToExpression;
use crate::expressions::precedence::OperatorPrec;
use crate::expressions::precedence::OperatorPrecAccess;


#[derive(Clone)]
#[derive(PartialEq)]
#[derive(Debug)]
pub enum TranscData {
    Pi,
    Euler,
}

impl TranscData {
}

impl ToExpression for TranscData {
    fn to_expr(self) -> Expression {
        Expression::Transc(Box::new(self))
    }

    fn to_rc_expr(self) -> Rc<Expression> {
        Rc::new(self.to_expr())
    }
}

impl OperatorPrecAccess for TranscData {
    fn operator_prec(&self) -> OperatorPrec {
        return OperatorPrec::Atom;
    }
}

impl fmt::Display for TranscData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Pi => write!(f, "pi")?,
            Self::Euler => write!(f, "e")?,
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::expressions::transcendental::TranscData;

    #[test]
    fn test_format_pi() {
        let number = TranscData::Pi;

        assert_eq!("pi", format!("{}", number));
    }

    #[test]
    fn test_format_euler() {
        let number = TranscData::Euler;

        assert_eq!("e", format!("{}", number));
    }

    #[test]
    fn test_partial_eq_pi_pi() {
        let o1 = TranscData::Pi;
        let o2 = TranscData::Pi;

        assert_eq!(true, o1 == o2);
    }

    #[test]
    fn test_partial_eq_pi_e() {
        let o1 = TranscData::Pi;
        let o2 = TranscData::Euler;

        assert_eq!(false, o1 == o2);
    }

    #[test]
    fn test_partial_neq_pi_e() {
        let o1 = TranscData::Pi;
        let o2 = TranscData::Euler;

        assert_eq!(true, o1 != o2);
    }
}
