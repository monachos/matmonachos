use crate::expressions::Expression;
use crate::expressions::ToExpression;
use crate::expressions::precedence::OperatorPrec;
use crate::expressions::precedence::OperatorPrecAccess;
use std::fmt;
use std::rc::Rc;



#[derive(Clone)]
#[derive(PartialEq)]
#[derive(Debug)]
pub struct SymbolData {
    pub name: String,
    pub indices: Vec<Rc<Expression>>,
    pub postfix: String,
}

impl SymbolData {
    pub fn new(name: &str) -> Self {
        Self {
            name: String::from(name),
            indices: Vec::new(),
            postfix: String::new(),
        }
    }

    pub fn with_indices(name: &str, indices: Vec<Rc<Expression>>) -> Self {
        Self {
            name: String::from(name),
            indices: indices,
            postfix: String::new(),
        }
    }

    pub fn with_index_e(name: &str, i1: Rc<Expression>) -> Self {
        Self {
            name: String::from(name),
            indices: vec![i1],
            postfix: String::new(),
        }
    }

    pub fn with_index_e_e(name: &str, i1: Rc<Expression>, i2: Rc<Expression>) -> Self {
        Self {
            name: String::from(name),
            indices: vec![i1, i2],
            postfix: String::new(),
        }
    }

    pub fn with_index_s(name: &str, i: &str) -> Self {
        Self {
            name: String::from(name),
            indices: vec![SymbolData::new(i).to_rc_expr()],
            postfix: String::new(),
        }
    }

    pub fn with_index_s_s(name: &str, i1: &str, i2: &str) -> Self {
        Self {
            name: String::from(name),
            indices: vec![SymbolData::new(i1).to_rc_expr(), SymbolData::new(i2).to_rc_expr()],
            postfix: String::new(),
        }
    }

    pub fn with_indices_and_postfix(
        name: &str,
        indices: Vec<Rc<Expression>>,
        postfix: &str) -> Self {
        Self {
            name: String::from(name),
            indices: indices,
            postfix: String::from(postfix),
        }
    }

    pub fn with_index_e_and_postfix(
        name: &str,
        i1: Rc<Expression>,
        postfix: &str
    ) -> Self {
        Self::with_indices_and_postfix(name, vec![i1], postfix)
    }

    pub fn with_index_e_e_and_postfix(
        name: &str,
        i1: Rc<Expression>,
        i2: Rc<Expression>,
        postfix: &str
    ) -> Self {
        Self::with_indices_and_postfix(name, vec![i1, i2], postfix)
    }

    pub fn with_index_s_and_postfix(name: &str, i: &str, postfix: &str) -> Self {
        Self {
            name: String::from(name),
            indices: vec![SymbolData::new(i).to_rc_expr()],
            postfix: String::from(postfix),
        }
    }

    pub fn with_index_s_s_and_postfix(name: &str, i1: &str, i2: &str, postfix: &str) -> Self {
        Self {
            name: String::from(name),
            indices: vec![SymbolData::new(i1).to_rc_expr(), SymbolData::new(i2).to_rc_expr()],
            postfix: String::from(postfix),
        }
    }

}

impl ToExpression for SymbolData {
    fn to_expr(self) -> Expression {
        Expression::Symbol(Box::new(self))
    }

    fn to_rc_expr(self) -> Rc<Expression> {
        Rc::new(self.to_expr())
    }
}

impl OperatorPrecAccess for SymbolData {
    fn operator_prec(&self) -> OperatorPrec {
        return OperatorPrec::Atom;
    }
}

impl fmt::Display for SymbolData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.name)?;
        if !self.indices.is_empty() {
            let ind = self.indices.iter()
                .map(|i| i.to_string())
                .collect::<Vec<_>>()
                .join(",");
            write!(f, "({})", ind)?;
        }
        if !self.postfix.is_empty() {
            write!(f, "{}", self.postfix)?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::expressions::symbol::SymbolData;
    use crate::factories::ObjFactory;
    use crate::testing::asserts::assert_expression_that;


    #[test]
    fn test_symbol_new() {
        let symbol = SymbolData::new("a");

        assert_eq!(symbol.name, "a");
    }

    #[test]
    fn test_symbol_with_indices_1() {
        let fa = ObjFactory::new10();
        let symbol = SymbolData::with_indices(
            "a",
            vec![fa.rzi_u(1)]);

        assert_eq!(symbol.name, "a");
        assert_eq!(1, symbol.indices.len());
        assert_expression_that(symbol.indices.get(0).unwrap())
            .as_integer()
            .is_int(1);
    }

    #[test]
    fn test_symbol_with_indices_1_2() {
        let fa = ObjFactory::new10();
        let symbol = SymbolData::with_indices(
            "a",
            vec![fa.rzi_u(1), fa.rzi_u(2)]);

        assert_eq!(symbol.name, "a");
        assert_eq!(2, symbol.indices.len());
        assert_expression_that(symbol.indices.get(0).unwrap())
            .as_integer()
            .is_int(1);
        assert_expression_that(symbol.indices.get(1).unwrap())
            .as_integer()
            .is_int(2);
    }

    #[test]
    fn test_with_index_s() {
        let symbol = SymbolData::with_index_s("a", "k");

        assert_eq!("a", symbol.name);
        assert_eq!(1, symbol.indices.len());
        assert_expression_that(symbol.indices.get(0).unwrap())
            .as_symbol()
            .has_name("k");
    }

    #[test]
    fn test_with_index_s_s() {
        let symbol = SymbolData::with_index_s_s("a", "k", "j");

        assert_eq!("a", symbol.name);
        assert_eq!(2, symbol.indices.len());
        assert_expression_that(symbol.indices.get(0).unwrap())
            .as_symbol()
            .has_name("k");
        assert_expression_that(symbol.indices.get(1).unwrap())
            .as_symbol()
            .has_name("j");
    }

    #[test]
    fn test_with_indices_and_postfix() {
        let fa = ObjFactory::new10();
        let symbol = SymbolData::with_indices_and_postfix(
            "a",
            vec![fa.rzi_u(3)],
            "'");

        assert_eq!("a", symbol.name);
        assert_eq!(1, symbol.indices.len());
        assert_expression_that(symbol.indices.get(0).unwrap())
            .as_integer()
            .is_int(3);
        assert_eq!("'", symbol.postfix);
    }

    #[test]
    fn test_format_name() {
        let symbol = SymbolData::new("a");

        assert_eq!("a", format!("{}", symbol));
    }

    #[test]
    fn test_format_with_indices_1() {
        let fa = ObjFactory::new10();
        let symbol = SymbolData::with_indices(
            "a",
            vec![fa.rzi_u(1)]);

        assert_eq!("a(1)", format!("{}", symbol));
    }

    #[test]
    fn test_format_with_indices_1_2() {
        let fa = ObjFactory::new10();
        let symbol = SymbolData::with_indices(
            "a",
            vec![fa.rzi_u(1), fa.rzi_u(2)]);

        assert_eq!("a(1,2)", format!("{}", symbol));
    }

    #[test]
    fn test_format_with_indices_and_postfix() {
        let fa = ObjFactory::new10();
        let symbol = SymbolData::with_indices_and_postfix(
            "a",
            vec![fa.rzi_u(1), fa.rzi_u(2)],
            "'");

        assert_eq!("a(1,2)'", format!("{}", symbol));
    }

    #[test]
    fn test_partial_eq_name() {
        let s1 = SymbolData::new("a");
        let s2 = SymbolData::new("a");

        assert_eq!(true, s1 == s2);
    }

    #[test]
    fn test_partial_neq_name() {
        let s1 = SymbolData::new("a");
        let s2 = SymbolData::new("b");

        assert_eq!(false, s1 == s2);
    }

    #[test]
    fn test_partial_eq_index() {
        let fa = ObjFactory::new10();
        let s1 = SymbolData::with_indices("a", vec![fa.rzi_u(1)]);
        let s2 = SymbolData::with_indices("a", vec![fa.rzi_u(1)]);

        assert_eq!(true, s1 == s2);
    }

    #[test]
    fn test_partial_neq_index() {
        let fa = ObjFactory::new10();
        let s1 = SymbolData::with_indices("a", vec![fa.rzi_u(1)]);
        let s2 = SymbolData::with_indices("a", vec![fa.rzi_u(2)]);

        assert_eq!(false, s1 == s2);
    }

    #[test]
    fn test_partial_eq_name_index() {
        let fa = ObjFactory::new10();
        let s1 = SymbolData::new("a");
        let s2 = SymbolData::with_indices("a", vec![fa.rzi_u(2)]);

        assert_eq!(false, s1 == s2);
    }

    #[test]
    fn test_partial_eq_postfix() {
        let fa = ObjFactory::new10();
        let s1 = SymbolData::with_indices_and_postfix("a", vec![fa.rzi_u(1)], "'");
        let s2 = SymbolData::with_indices_and_postfix("a", vec![fa.rzi_u(1)], "'");

        assert_eq!(true, s1 == s2);
    }

    #[test]
    fn test_partial_neq_postfix() {
        let fa = ObjFactory::new10();
        let s1 = SymbolData::with_indices_and_postfix("a", vec![fa.rzi_u(1)], "'");
        let s2 = SymbolData::with_indices_and_postfix("a", vec![fa.rzi_u(1)], "''");

        assert_eq!(false, s1 == s2);
    }
}
