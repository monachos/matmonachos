use std::cmp::Ordering;
use std::fmt;
use std::ops::Neg;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Sign2 {
    Positive,
    Negative,
}

impl Sign2 {
    pub fn mul(&self, other: Self) -> Self {
        return if self.eq(&other) {
            Self::Positive
        } else {
            Self::Negative
        };
    }
}

impl Neg for Sign2 {
    type Output = Self;

    fn neg(self) -> Self::Output {
        match self {
            Self::Negative => Self::Positive,
            Self::Positive => Self::Negative,
        }
    }
}

impl PartialOrd for Sign2 {
    /// compares sign. If both signs are the same then None is returned.
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        match self {
            Self::Positive => match other {
                Self::Positive => {
                    // positive comparison
                    return None;
                }
                Self::Negative => {
                    return Some(Ordering::Greater);
                }
            }
            Self::Negative => match other {
                Self::Positive => {
                    return Some(Ordering::Less);
                }
                Self::Negative => {
                    // negative comparison
                    return None;
                }
            }
        }
    }
}

impl fmt::Display for Sign2 {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let value = match self {
            Sign2::Positive => "+",
            Sign2::Negative => "-",
        };
        write!(f, "{}", value)
    }
}

#[cfg(test)]
mod tests {
    use rstest::rstest;
    use assert_matches::assert_matches;
    use std::cmp::Ordering;
    use crate::expressions::sign2::Sign2;

    #[rstest]
    #[case(Sign2::Positive, Sign2::Negative)]
    #[case(Sign2::Negative, Sign2::Positive)]
    fn test_sign_neg(#[case] input: Sign2, #[case] expected: Sign2) {
        let result = -input;
        assert_eq!(expected, result);
    }

    #[rstest]
    #[case(Sign2::Positive, Sign2::Negative)]
    fn test_sign_partial_cmp_greater(#[case] left: Sign2, #[case] right: Sign2) {
        match left.partial_cmp(&right) {
            Some(result) => {
                assert_matches!(result, Ordering::Greater);
            }
            None => {
                assert!(false);
            }
        }
    }

    #[rstest]
    #[case(Sign2::Negative, Sign2::Positive)]
    fn test_sign_partial_cmp_less(#[case] left: Sign2, #[case] right: Sign2) {
        match left.partial_cmp(&right) {
            Some(result) => {
                assert_matches!(result, Ordering::Less);
            }
            None => {
                assert!(false);
            }
        }
    }

    #[rstest]
    #[case(Sign2::Positive, Sign2::Positive)]
    #[case(Sign2::Negative, Sign2::Negative)]
    fn test_sign_partial_cmp_non_comparable(#[case] left: Sign2, #[case] right: Sign2) {
        let result = left.partial_cmp(&right);
        assert_matches!(result, None);
    }
}
