use std::cmp::Ordering;
use std::cmp::PartialEq;
use std::cmp::PartialOrd;
use std::fmt;
use std::rc::Rc;

use matm_error::error::CalcError;
use crate::core::natural::Natural;
use crate::core::positional::PositionalNumber;
use crate::expressions::Expression;
use crate::expressions::ToExpression;
use crate::expressions::sign::Sign;
use crate::expressions::precedence::OperatorPrec;
use crate::expressions::precedence::OperatorPrecAccess;


#[derive(Clone, Debug)]
pub struct IntData {
    sign: Sign,
    natural: Natural,
}

impl IntData {
    pub fn zero(radix: u32) -> Result<Self, CalcError> {
        return Self::from_int(radix, 0);
    }

    pub fn one(radix: u32) -> Result<Self, CalcError> {
        return Self::from_int(radix, 1);
    }

    pub fn from_int(radix: u32, value: i32) -> Result<Self, CalcError> {
        let (sign, natural) = if value > 0 {
            (Sign::Positive,
            Natural::from_int(radix, value as u32)?)
        } else if value < 0 {
            let positive_val = value.wrapping_abs() as u32;
            (Sign::Negative,
            Natural::from_int(radix, positive_val)?)
        } else {
            (Sign::Zero,
            Natural::from_int(radix, 0)?)
        };
        
        Ok(Self {
            sign: sign,
            natural: natural,
        })
    }

    pub fn from_vector(radix: u32, digits: Vec<u8>, sign: Sign) -> Result<Self, CalcError> {
        let natural = Natural::from_vector(radix, digits)?;
        Ok(Self {
            sign: Self::normalize_sign(sign, &natural),
            natural: natural,
        })
    }

    pub fn from_string_and_neg(radix: u32, value: &str, negative: bool) -> Result<Self, CalcError> {
        let natural = Natural::from_string(radix, value)?;
        let sign = if negative {
            Sign::Negative
        } else if natural.is_zero() {
            Sign::Zero
        } else {
            Sign::Positive
        };

        Ok(Self {
            sign: sign,
            natural: natural,
        })
    }

    pub fn from_string(radix: u32, value: &str) -> Result<Self, CalcError> {
        let (negative, digits_str) = if value.starts_with('-') {
            let (_, dig) = value.split_at(1);
            (true, dig)
        } else {
            (false, value)
        };
        return Self::from_string_and_neg(radix, digits_str, negative);
    }

    pub fn from_natural(natural: Natural, sign: Sign) -> Self {
        Self {
            sign: Self::normalize_sign(sign, &natural),
            natural: natural,
        }
    }

    fn normalize_sign(sign: Sign, natural: &Natural) -> Sign {
        if natural.is_zero() {
            return Sign::Zero;
        } else {
            return sign;
        }
    }

    pub fn from_positional_number(number: &PositionalNumber) -> Result<Self, CalcError> {
        let natural = Natural::from_positional_number(number)?;
        Ok(Self {
            sign: Self::normalize_sign(Sign::Positive, &natural),
            natural: natural,
        })
    }

    pub fn to_i32(&self) -> Option<i32> {
        match self.sign {
            Sign::Zero => {
                return Some(0);
            }
            Sign::Positive => {
                match self.natural.to_u32() {
                    Some(v) => {
                        if v <= i32::MAX as u32 {
                            return Some(v as i32);
                        }
                    }
                    None => {}
                }
            }
            Sign::Negative => {
                match self.natural.to_u32() {
                    Some(v) => {
                        if v <= 2147483648u32 {
                            return Some(v.wrapping_neg() as i32);
                        }
                    }
                    None => {}
                }
            }
        }

        return None;
    }

    /// negates value
    pub fn calculate_neg(&self) -> Self {
        return match self.sign {
            Sign::Zero => self.clone(),
            _ => Self {
                sign: -self.sign,
                natural: self.natural.clone(),
            }
        };
    }

    /// negates value if cond is true
    pub fn calculate_neg_if(&self, cond: bool) -> Self {
        return if cond {
            self.calculate_neg()
        } else {
            self.clone()
        };
    }

    /// returns absolute value of the number
    pub fn calculate_abs(&self) -> Self {
        return if self.is_negative() {
            self.calculate_neg()
        } else {
            self.clone()
        };
    }

    pub fn radix(&self) -> u32 {
        return self.natural.radix();
    }

    pub fn sign(&self) -> Sign {
        return self.sign;
    }

    pub fn natural(&self) -> &Natural {
        return &self.natural;
    }
 
    pub fn is_zero(&self) -> bool {
        return self.natural.is_zero();
    }

    pub fn is_one(&self) -> bool {
        return match self.sign {
            Sign::Positive => self.natural.is_one(),
            _ => false,
        };
    }

    pub fn is_minus_one(&self) -> bool {
        return match self.sign {
            Sign::Negative => self.natural.is_one(),
            _ => false,
        };
    }

    pub fn is_positive(&self) -> bool {
        return match self.sign {
            Sign::Positive => true,
            _ => false,
        };
    }

    pub fn is_negative(&self) -> bool {
        return match self.sign {
            Sign::Negative => true,
            _ => false,
        };
    }

    pub fn iter(&self) -> std::iter::Rev<std::slice::Iter<u8>> {
        return self.natural.iter();
    }

}


impl PartialEq for IntData {
    fn eq(&self, other: &IntData) -> bool {
        if self.sign == other.sign {
            return self.natural.eq(&other.natural);
        }
        return false;
    }
}

impl PartialOrd for IntData {
    fn partial_cmp(&self, other: &IntData) -> Option<std::cmp::Ordering> {
        if self.radix() != other.radix() {
            return None;
        }

        if self.is_negative() && !other.is_negative() {
            return Some(Ordering::Less);
        } else if !self.is_negative() && other.is_negative() {
            return Some(Ordering::Greater);
        } else if self.is_negative() && other.is_negative() {
            let result = self.natural.partial_cmp(&other.natural);
            match result {
                Some(ord) => {
                    return match ord {
                        Ordering::Greater => Some(Ordering::Less),
                        Ordering::Less => Some(Ordering::Greater),
                        Ordering::Equal => Some(Ordering::Equal),
                    };
                }
                None => {
                    return None;
                }
            }
        } else {
            // all are positive
            return self.natural.partial_cmp(&other.natural);
        }
    }
}

//-----------------------------------------------------

impl ToExpression for IntData {
    fn to_expr(self) -> Expression {
        Expression::Int(Box::new(self))
    }

    fn to_rc_expr(self) -> Rc<Expression> {
        Rc::new(self.to_expr())
    }
}

impl OperatorPrecAccess for IntData {
    fn operator_prec(&self) -> OperatorPrec {
        if self.is_negative() {
            return OperatorPrec::Minus;
        } else {
            return OperatorPrec::Atom;
        }
    }
}

impl fmt::Display for IntData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.is_negative() {
            write!(f, "-")?;
        }
        write!(f, "{}", self.natural)?;
        Ok(())
    }
}

