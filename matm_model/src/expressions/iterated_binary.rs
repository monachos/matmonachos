use std::fmt;
use std::rc::Rc;
use crate::expressions::Expression;
use crate::expressions::ToExpression;
use crate::expressions::precedence::OperatorPrec;
use crate::expressions::precedence::OperatorPrecAccess;
use crate::expressions::symbol::SymbolData;


#[derive(Clone, Copy)]
#[derive(PartialEq)]
#[derive(Debug)]
pub enum IteratedBinaryOperator {
    Sum,
    Product,
}


#[derive(Clone)]
#[derive(PartialEq)]
#[derive(Debug)]
pub struct IteratedBinaryData {
    pub operator: IteratedBinaryOperator,
    pub iterator: SymbolData,
    pub lower_bound: Rc<Expression>,
    pub upper_bound: Rc<Expression>,
    pub value: Rc<Expression>,
}

impl IteratedBinaryData {
    pub fn from_operator(
        operator: IteratedBinaryOperator,
        iterator: SymbolData,
        lower_bound: Rc<Expression>,
        upper_bound: Rc<Expression>,
        value: Rc<Expression>,
    ) -> Self {
        Self {
            operator: operator,
            iterator: iterator,
            lower_bound: lower_bound,
            upper_bound: upper_bound,
            value: value,
        }
    }

    pub fn sum(
        iterator: SymbolData,
        lower_bound: Rc<Expression>,
        upper_bound: Rc<Expression>,
        value: Rc<Expression>,
    ) -> Self {
        Self {
            operator: IteratedBinaryOperator::Sum,
            iterator: iterator,
            lower_bound: lower_bound,
            upper_bound: upper_bound,
            value: value,
        }
    }

    pub fn product(
        iterator: SymbolData,
        lower_bound: Rc<Expression>,
        upper_bound: Rc<Expression>,
        value: Rc<Expression>,
    ) -> Self {
        Self {
            operator: IteratedBinaryOperator::Product,
            iterator: iterator,
            lower_bound: lower_bound,
            upper_bound: upper_bound,
            value: value,
        }
    }
}

impl ToExpression for IteratedBinaryData {
    fn to_expr(self) -> Expression {
        return Expression::IteratedBinary(Box::new(self));
    }

    fn to_rc_expr(self) -> Rc<Expression> {
        return Rc::new(self.to_expr());
    }
}

impl OperatorPrecAccess for IteratedBinaryData {
    fn operator_prec(&self) -> OperatorPrec {
        return OperatorPrec::IteratedBinary;
    }
}

impl fmt::Display for IteratedBinaryData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let symbol = match self.operator {
            IteratedBinaryOperator::Sum => "sum",
            IteratedBinaryOperator::Product => "product",
        };
        write!(f, "{}({}={}..{}, {})",
            symbol,
            self.iterator,
            self.lower_bound,
            self.upper_bound,
            self.value
        )
    }
}


#[cfg(test)]
mod tests {
    use crate::expressions::ToExpression;
    use crate::expressions::iterated_binary::IteratedBinaryData;
    use crate::factories::ObjFactory;

    #[test]
    fn test_format_sum() {
        let fa = ObjFactory::new10();
        let expr = IteratedBinaryData::sum(
            fa.sn("k"),
            fa.rzi_u(1),
            fa.rzi_u(5),
            fa.rsn("x")
        ).to_expr();

        assert_eq!("sum(k=1..5, x)", format!("{}", expr));
    }

    #[test]
    fn test_format_product() {
        let fa = ObjFactory::new10();
        let expr = IteratedBinaryData::product(
            fa.sn("k"),
            fa.rzi_u(1),
            fa.rzi_u(5),
            fa.rsn("x")
        ).to_expr();

        assert_eq!("product(k=1..5, x)", format!("{}", expr));
    }
}
