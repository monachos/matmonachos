use core::ops::Range;
use std::fmt;
use std::rc::Rc;

use matm_error::error::CalcError;
use crate::core::index::Index;
use crate::core::index::Index2D;
use crate::core::index::IndexND;
use crate::core::index_range::IndexRange;
use crate::core::size::Size2D;
use crate::expressions::int::IntData;
use crate::expressions::Expression;
use crate::expressions::ToExpression;
use crate::expressions::precedence::OperatorPrec;
use crate::expressions::precedence::OperatorPrecAccess;
use crate::grid::Grid;
use crate::grid::iterators::DownRight2DElementIterator;
use crate::grid::iterators::DownRightLowerTriElementIterator;
use crate::grid::iterators::RightDown2DElementIterator;


pub struct IndexAndCount {
    pub index: usize,
    pub count: usize,
}

impl IndexAndCount {
    pub fn new(index: usize, count: usize) -> Self {
        Self {
            index: index,
            count: count,
        }
    }
}


#[derive(PartialEq)]
#[derive(Debug)]
pub struct MatrixData {
    pub grid: Grid<Rc<Expression>>,

    /// vector of zero-based indexes of first column of augmented matrices
    pub augmented_columns: Vec<usize>,
}

impl MatrixData {
    pub const ROW_INDEX: usize = 0;
    pub const COLUMN_INDEX: usize = 1;

    pub fn zeros(radix: u32, rows: usize, cols: usize) -> Result<Self, CalcError> {
        Ok(Self {
            grid: Grid::from_vector(
                vec![rows, cols],
                vec![IntData::zero(radix)?.to_rc_expr(); rows * cols])?,
            augmented_columns: Vec::new(),
        })
    }

    pub fn ones(radix: u32, rows: usize, cols: usize) -> Result<Self, CalcError> {
        Ok(Self {
            grid: Grid::from_vector(
                vec![rows, cols],
                vec![IntData::one(radix)?.to_rc_expr(); rows * cols])?,
            augmented_columns: Vec::new(),
        })
    }

    pub fn identity(radix: u32, size: usize) -> Result<Self, CalcError> {
        let mut elements: Vec<Rc<Expression>> = Vec::with_capacity(size * size);
        for r in 1..=size {
            for c in 1..=size {
                let el = if r == c {
                    IntData::one(radix)?.to_rc_expr()
                } else {
                    IntData::zero(radix)?.to_rc_expr()
                };
                elements.push(el);
            }
        }
        let matrix = Self::from_vector(size, size, elements)?;
        Ok(matrix)
    }

    pub fn from_vector(rows: usize, cols: usize, elements: Vec<Rc<Expression>>) -> Result<Self, CalcError> {
        Ok(Self {
            grid: Grid::from_vector(vec![rows, cols], elements)?,
            augmented_columns: Vec::new(),
        })
    }

    pub fn from_ivector(rows: usize, cols: usize, radix: u32, elements: Vec<i32>) -> Result<Self, CalcError> {
        return Self::from_vector(rows, cols,
            elements.iter()
            .map(|v| IntData::from_int(radix, *v).unwrap().to_rc_expr())
            .collect());
    }

    pub fn scalar(value: Rc<Expression>) -> Self {
        Self {
            grid: Grid::from_vector(
                vec![1, 1],
                vec![value; 1]).unwrap(),
            augmented_columns: Vec::new(),
        }
    }

    pub fn augmented(m1: &Self, m2: &Self) -> Result<Self, CalcError> {
        // see https://en.wikipedia.org/wiki/Augmented_matrix
        if m1.grid.size_of(Self::ROW_INDEX)? != m2.grid.size_of(Self::ROW_INDEX)? {
            return Err(CalcError::from_str("Matrices of horizontaly augmented matrix have to have the same number of rows."));
        }
        let cols = m1.grid.size_of(Self::COLUMN_INDEX)? + m2.grid.size_of(Self::COLUMN_INDEX)?;
        let mut elements: Vec<Rc<Expression>> = Vec::with_capacity(m1.grid.size_of(Self::ROW_INDEX)? * cols);
        for r in m1.iter_rows() {
            for c in m1.iter_columns() {
                elements.push(Rc::clone(m1.get_i_i(r, c)?));
            }
            for c in m2.iter_columns() {
                elements.push(Rc::clone(m2.get_i_i(r, c)?));
            }
        }
        let matrix = Self {
            grid: Grid::from_vector(
                vec![m1.grid.size_of(Self::ROW_INDEX)?, cols],
                elements)?,
            augmented_columns: vec![m1.grid.size_of(Self::COLUMN_INDEX)?],
        };
        return Ok(matrix);
    }

    pub fn rows(&self) -> usize {
        return self.size().rows;
    }

    pub fn columns(&self) -> usize {
        return self.size().columns;
    }

    pub fn size(&self) -> Size2D {
        return Size2D::from_nd(&self.grid.size).unwrap();
    }

    pub fn iter_rows(&self) -> Range<usize> {
        return self.grid.dimension_range(Self::ROW_INDEX).unwrap();
    }

    pub fn iter_columns(&self) -> Range<usize> {
        return self.grid.dimension_range(Self::COLUMN_INDEX).unwrap();
    }

    pub fn iter_right_down(&self) -> RightDown2DElementIterator {
        RightDown2DElementIterator::new(self.rows(), self.columns())
    }

    pub fn iter_down_right(&self) -> DownRight2DElementIterator {
        DownRight2DElementIterator::new(self.rows(), self.columns())
    }

    pub fn iter_down_right_lower_tri(&self) -> DownRightLowerTriElementIterator {
        DownRightLowerTriElementIterator::new(self.rows(), self.columns())
    }

    pub fn is_square(&self) -> bool {
        return self.grid.is_square();
    }

    pub fn is_zero(&self) -> bool {
        for e in &self.grid.data {
            if !e.is_zero() {
                return false;
            }
        }
        return true;
    }

    pub fn is_ones(&self) -> bool {
        for e in &self.grid.data {
            if !e.is_one() {
                return false;
            }
        }
        return true;
    }

    pub fn is_identity(&self) -> bool {
        for idx in self.iter_right_down() {
            if idx.row == idx.column && !self.get(&idx).unwrap().is_one()
                || idx.row != idx.column && !self.get(&idx).unwrap().is_zero() {
                return false;
            }
        }
        return true;
    }

    pub fn is_upper_triangular(&self) -> bool {
        for idx in self.iter_right_down() {
            if idx.row > idx.column {
                let item = self.get(&idx).unwrap();
                if !item.is_zero() {
                    return false;
                }
            }
        }
        return true;
    }

    pub fn is_strictly_upper_triangular(&self) -> bool {
        for idx in self.iter_right_down() {
            if idx.row >= idx.column && !self.get(&idx).unwrap().is_zero() {
                return false;
            }
        }
        return true;
    }

    pub fn is_lower_triangular(&self) -> bool {
        for idx in self.iter_right_down() {
            if idx.row < idx.column {
                let item = self.get(&idx).unwrap();
                if !item.is_zero() {
                    return false;
                }
            }
        }
        return true;
    }

    pub fn is_symmetric(&self) -> bool {
        if !self.is_square() {
            return false;
        }
        for i in self.iter_rows().skip(1) {
            for j in 1..=i {
                let aij = self.get_i_i(i, j).unwrap();
                let aji = self.get_i_i(j, i).unwrap();
                if !aij.eq(aji) {
                    return false;
                }
            }
        }
        return true;
    }
    
    pub fn has_symbol(&self) -> bool {
        for idx in self.iter_right_down() {
            if self.get(&idx).unwrap().has_symbol() {
                return true;
            }
        }
        return false;
    }

    pub fn get_i_i(&self, row: usize, col: usize) -> Result<&Rc<Expression>, CalcError> {
        return self.grid.get_at(&vec![row, col], Self::format_index);
    }

    pub fn get_mut_i_i(&mut self, row: usize, col: usize) -> Result<&mut Rc<Expression>, CalcError> {
        return self.grid.get_mut_at(&vec![row, col], Self::format_index);
    }

    pub fn get(&self, index: &Index2D) -> Result<&Rc<Expression>, CalcError> {
        return self.grid.get(&IndexND::from_2d(index), Self::format_index);
    }

    pub fn get_mut(&mut self, index: &Index2D) -> Result<&mut Rc<Expression>, CalcError> {
        return self.grid.get_mut(&IndexND::from_2d(index));
    }

    pub fn set_i_i(&mut self, row: usize, col: usize, el: Rc<Expression>) -> Result<(), CalcError> {
        return self.grid.set_at(&vec![row, col], el, Self::format_index);
    }

    pub fn set(&mut self, index: &Index2D, el: Rc<Expression>) -> Result<(), CalcError> {
        return self.grid.set(&IndexND::from_2d(index), el, Self::format_index);
    }

    pub fn format_index(idx: &Vec<usize>) -> String {
        return IndexND::format_index(idx);
    }

    /// Transposes the matrix and returns transposed matrix
    pub fn transpose(&self) -> Self {
        let elements: Vec<Rc<Expression>> = self.iter_down_right()
            .map(|idx| self.get(&idx).unwrap().clone())
            .collect();

        //TODO what about augmented matrix?
        return Self::from_vector(
            self.grid.size_of(Self::COLUMN_INDEX).unwrap(),
            self.grid.size_of(Self::ROW_INDEX).unwrap(),
            elements).unwrap();
    }

    pub fn get_submatrix(
        &self,
        rows: &IndexRange,
        cols: &IndexRange) -> Result<Self, CalcError> {
        if !self.grid.is_index_range_valid(Self::ROW_INDEX, rows) {
            return Err(CalcError::from_string(format!("Invalid row index ({})", rows)));
        }
        if !self.grid.is_index_range_valid(Self::COLUMN_INDEX, cols) {
            return Err(CalcError::from_string(format!("Invalid column index ({})", cols)));
        }

        let target_rows = rows.count(self.rows())?;
        let target_cols = cols.count(self.columns())?;

        let row_start = rows.calc_start(self.rows())?;
        let col_start = cols.calc_start(self.columns())?;

        let elements = RightDown2DElementIterator::new(target_rows, target_cols)
            .map(|idx| self.get_i_i(idx.row+row_start-1, idx.column+col_start-1)
                .unwrap()
                .clone())
            .collect();

        return Self::from_vector(target_rows, target_cols, elements);
    }

    pub fn get_submatrix_with_crossed_lines(
        &self,
        center: &Index2D,
    ) -> Result<MatrixData, CalcError> {
        if !self.grid.size.is_index_valid(Self::ROW_INDEX, center.row) {
            return Err(CalcError::from_string(format!("Invalid row index [{}]", center.row)));
        }
        if !self.grid.size.is_index_valid(Self::COLUMN_INDEX, center.column) {
            return Err(CalcError::from_string(format!("Invalid column index [{}]", center.column)));
        }
        let mut elements: Vec<Rc<Expression>> = Vec::new();
        for idx in self.iter_right_down() {
            let el = if idx.row == center.row || idx.column == center.column {
                Rc::new(Expression::BoxSign)
            } else {
                Rc::clone(self.get(&idx)?)
            };
            elements.push(el);
        }
        return MatrixData::from_vector(
            self.rows(),
            self.columns(),
            elements);
    }

    pub fn get_submatrix_by_removing(
        &self,
        removed: &Index2D,
    ) -> Result<MatrixData, CalcError> {
        if !self.grid.size.is_index_valid(Self::ROW_INDEX, removed.row) {
            return Err(CalcError::from_string(format!("Invalid row index [{}]", removed.row)));
        }
        if !self.grid.size.is_index_valid(Self::COLUMN_INDEX, removed.column) {
            return Err(CalcError::from_string(format!("Invalid column index [{}]", removed.column)));
        }
        let indexes: Vec<Index2D> = self.iter_right_down()
            .filter(|i| i.row != removed.row && i.column != removed.column)
            .collect();
        let mut elements: Vec<Rc<Expression>> = Vec::with_capacity(indexes.len());
        for idx in &indexes {
            let el = self.get(idx)?.clone();
            elements.push(el);
        }
        return MatrixData::from_vector(
            self.rows() - 1,
            self.columns() - 1,
            elements);
    }

    pub fn get_row_ref(&self, row: usize) -> Matrix2DRowRef {
        Matrix2DRowRef {
            matrix_ref: self,
            row: row,
        }
    }

    pub fn get_row_ref_mut(&mut self, row: usize) -> Matrix2DRowRefMut {
        Matrix2DRowRefMut {
            matrix_ref: self,
            row: row,
        }
    }

    /// Gets submatrix of specified row
    pub fn get_row(&self, row: Index) -> Result<Self, CalcError> {
        let rows = IndexRange::one(row)?;
        let cols = IndexRange::All;
        return self.get_submatrix(&rows, &cols);
    }

    /// Gets submatrix of specified column
    pub fn get_column(&self, col: Index) -> Result<Self, CalcError> {
        let rows = IndexRange::All;
        let cols = IndexRange::one(col)?;
        return self.get_submatrix(&rows, &cols);
    }

    pub fn set_matrix(&mut self, start: Index2D, source: &MatrixData) -> Result<(), CalcError> {
        for idx in source.iter_right_down() {
            let index = Index2D::new(
                idx.row + start.row - 1,
                idx.column + start.column - 1)?;
            let el = source.get(&idx)?;
            self.set(&index, Rc::clone(el))?;
        }
        Ok(())
    }

    pub fn swap_rows(&mut self, r1: usize, r2: usize) -> Result<bool, CalcError> {
        if !self.grid.size.is_index_valid(Self::ROW_INDEX, r1) {
            return Err(CalcError::from_string(format!("Invalid row index [{r1}]")));
        }
        if !self.grid.size.is_index_valid(Self::ROW_INDEX, r2) {
            return Err(CalcError::from_string(format!("Invalid row index [{r2}]")));
        }
        if r1 != r2 {
            for c in self.iter_columns() {
                let f1 = Rc::clone(self.get_i_i(r1, c)?);
                let f2 = Rc::clone(self.get_i_i(r2, c)?);
                self.set_i_i(r1, c, f2)?;
                self.set_i_i(r2, c, f1)?;
            }
            return Ok(true);
        } else {
            return Ok(false);
        }
    }

    pub fn align_column(&self, col: usize) -> Result<usize, CalcError> {
        return self.grid.align_index(Self::COLUMN_INDEX, col);
    }

    pub fn align_row(&self, col: usize) -> Result<usize, CalcError> {
        return self.grid.align_index(Self::ROW_INDEX, col);
    }

    pub fn count_zeros_in_row(&self, row: usize) -> Result<usize, CalcError> {
        let mut result: usize = 0;
        for c in self.iter_columns() {
            if self.get_i_i(row, c)?.is_zero() {
                result += 1;
            }
        }
        return Ok(result);
    }

    /// Returns Some(row index and count) or None(not found)
    pub fn find_row_with_max_zeros(&self) -> Option<IndexAndCount> {
        let mut result: Option<IndexAndCount> = None;
        for r in self.iter_rows() {
            let count = self.count_zeros_in_row(r).unwrap();
            if count > 0 {
                match &result {
                    Some(i) => {
                        if count > i.count {
                            result = Some(IndexAndCount::new(r, count));
                        }
                    }
                    None => {
                        result = Some(IndexAndCount::new(r, count));
                    }
                }
            }
        }
        return result;
    }

    pub fn count_zeros_in_column(&self, col: usize) -> Result<usize, CalcError> {
        let mut result: usize = 0;
        for r in self.iter_rows() {
            if self.get_i_i(r, col)?.is_zero() {
                result += 1;
            }
        }
        return Ok(result);
    }

    /// Returns Some(column index) or None(not found)
    pub fn find_column_with_max_zeros(&self) -> Option<IndexAndCount> {
        let mut result: Option<IndexAndCount> = None;
        for c in self.iter_columns() {
            let count = self.count_zeros_in_column(c).unwrap();
            if count > 0 {
                match &result {
                    Some(i) => {
                        if count > i.count {
                            result = Some(IndexAndCount::new(c, count));
                        }
                    }
                    None => {
                        result = Some(IndexAndCount::new(c, count));
                    }
                }
            }
        }
        return result;
    }

    pub fn is_zero_row(&self, row: usize) -> Result<bool, CalcError> {
        if !self.grid.size.is_index_valid(Self::ROW_INDEX, row) {
            return Err(CalcError::from_string(format!("Invalid row index [{}]", row)));
        }
        for c in self.iter_columns() {
            if !self.get_i_i(row, c)?.is_zero() {
                return Ok(false);
            }
        }
        return Ok(true);
    }

    pub fn count_non_zero_rows(&self) -> usize {
        let mut result = 0;
        for r in self.iter_rows() {
            if !self.is_zero_row(r).unwrap() {
                result += 1;
            }
        }
        return result;
    }

    pub fn is_zero_column(&self, col: usize) -> Result<bool, CalcError> {
        if !self.grid.size.is_index_valid(Self::COLUMN_INDEX, col) {
            return Err(CalcError::from_string(format!("Invalid column index [{}]", col)));
        }
        for r in self.iter_rows() {
            if !self.get_i_i(r, col)?.is_zero() {
                return Ok(false);
            }
        }
        return Ok(true);
    }

    pub fn count_non_zero_columns(&self) -> usize {
        let mut result = 0;
        for c in self.iter_columns() {
            if !self.is_zero_column(c).unwrap() {
                result += 1;
            }
        }
        return result;
    }

}

impl Clone for MatrixData {
    fn clone(&self) -> MatrixData {
        let grid = Grid::from_vector(
            vec![self.rows(), self.columns()],
            self.iter_right_down()
            .map(|idx| self.get(&idx).unwrap().clone())
            .collect()
        ).unwrap();
        Self {
            grid: grid,
            augmented_columns: self.augmented_columns.clone(),
        }
    }
}

impl ToExpression for MatrixData {
    fn to_expr(self) -> Expression {
        Expression::Matrix(Box::new(self))
    }

    fn to_rc_expr(self) -> Rc<Expression> {
        Rc::new(self.to_expr())
    }
}

impl OperatorPrecAccess for MatrixData {
    fn operator_prec(&self) -> OperatorPrec {
        return OperatorPrec::Atom;
    }
}

//--------------------------------------

impl fmt::Display for MatrixData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "[")?;
        for r in self.iter_rows() {
            if r > 1 {
                write!(f, "; ")?;
            }
            for c in self.iter_columns() {
                if c > 1 {
                    write!(f, ", ")?;
                }
                write!(f, "{}", *self.get_i_i(r, c).unwrap())?;
            }
        }
        write!(f, "]")?;
        Ok(())
    }
}

//-----------------------------------------

pub struct Matrix2DRowRef<'a> {
    matrix_ref: &'a MatrixData,
    row: usize,
}

impl<'a> Matrix2DRowRef<'a> {
    pub fn len(&self) -> usize {
        return self.matrix_ref.columns();
    }

    pub fn get(&self, i: usize) -> Result<&Rc<Expression>, CalcError> {
        return self.matrix_ref.get_i_i(self.row, i);
    }

    // pub fn iter(&self) -> Matrix2DRowRefIterator<'a> {
    //     // Matrix2DRowRefIterator::new(self)
    //     Matrix2DRowRefIterator {
    //         row_ref: self,
    //         cur_column: 0,
    //     }
    // }
}

pub struct Matrix2DRowRefMut<'a> {
    matrix_ref: &'a mut MatrixData,
    row: usize,
}

impl<'a> Matrix2DRowRefMut<'a> {
    pub fn len(&self) -> usize {
        return self.matrix_ref.columns();
    }

    pub fn get(&self, i: usize) -> Result<&Rc<Expression>, CalcError> {
        return self.matrix_ref.get_i_i(self.row, i);
    }

    pub fn set(&mut self, i: usize, expr: Rc<Expression>) -> Result<(), CalcError> {
        self.matrix_ref.set_i_i(self.row, i, expr)
    }
}

// struct Matrix2DRowRefIterator<'a> {
//     row_ref: &'a Matrix2DRowRef<'a>,
//     cur_column: usize,
// }

// impl<'b> Matrix2DRowRefIterator<'b> {
//     pub fn new<'a>(row_ref: &'a Matrix2DRowRef<'a>) -> Self {
//         Matrix2DRowRefIterator {
//             row_ref: row_ref,
//             cur_column: 0,
//         }
//     }
// }

// impl<'a> Iterator for Matrix2DRowRefIterator<'a> {
//     type Item = &'a Expression;

//     fn next(&mut self) -> Option<Self::Item> {
//         let columns = self.row_ref.matrix_ref.get_columns();
//         if 0 == self.cur_column || self.cur_column < columns {
//             self.cur_column += 1;
//             match self.row_ref.matrix_ref.get(self.row_ref.row, self.cur_column) {
//                 Ok(el) => {
//                     Some(el)
//                 }
//                 Err(_) => {
//                     None
//                 }
//             }
//         } else {
//             None
//         }
//     }
// }