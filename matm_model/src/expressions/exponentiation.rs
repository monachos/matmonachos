use std::fmt;
use std::rc::Rc;

use crate::expressions::Expression;
use crate::expressions::ToExpression;
use crate::expressions::precedence::OperatorPrec;
use crate::expressions::precedence::OperatorPrecAccess;
use crate::write_wrapped_obj;

#[derive(Clone)]
#[derive(PartialEq)]
#[derive(Debug)]
pub struct ExpData {
    pub base: Rc<Expression>,
    pub exponent: Rc<Expression>,
}

impl ExpData {
    pub fn new(base: Rc<Expression>, exponent: Rc<Expression>) -> Self {
        Self {
            base: base,
            exponent: exponent,
        }
    }

    pub fn has_symbol(&self) -> bool {
        return self.base.has_symbol() || self.exponent.has_symbol();
    }

    pub fn should_wrap_base_with_parentheses(&self) -> bool {
        self.base.operator_prec() < self.operator_prec()
    }

    pub fn should_wrap_exponent_with_parentheses(&self) -> bool {
        self.exponent.operator_prec() < self.operator_prec()
    }
}

impl ToExpression for ExpData {
    fn to_expr(self) -> Expression {
        return Expression::Exp(Box::new(self));
    }

    fn to_rc_expr(self) -> Rc<Expression> {
        return Rc::new(self.to_expr());
    }
}

impl OperatorPrecAccess for ExpData {
    fn operator_prec(&self) -> OperatorPrec {
        return OperatorPrec::Exp;
    }
}

impl fmt::Display for ExpData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write_wrapped_obj(f, &self.base, self.should_wrap_base_with_parentheses())?;
        write!(f, " ^ ")?;
        write_wrapped_obj(f, &self.exponent, self.should_wrap_exponent_with_parentheses())?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::expressions::ToExpression;
    use crate::expressions::exponentiation::ExpData;
    use crate::factories::ObjFactory;


    #[test]
    fn test_format_exp() {
        let fa = ObjFactory::new10();
        let expr = ExpData::new(
            fa.rzi_u(2),
            fa.rzi_u(3),
        ).to_expr();

        assert_eq!("2 ^ 3", format!("{}", expr));
    }

    #[test]
    fn test_format_exp_positive_negative() {
        let fa = ObjFactory::new10();
        let expr = ExpData::new(
            fa.rzi_u(2),
            fa.rzi_u(-3),
        ).to_expr();

        assert_eq!("2 ^ (-3)", format!("{}", expr));
    }

    #[test]
    fn test_format_exp_negative_positive() {
        let fa = ObjFactory::new10();
        let expr = ExpData::new(
            fa.rzi_u(-2),
            fa.rzi_u(3),
        ).to_expr();

        assert_eq!("(-2) ^ 3", format!("{}", expr));
    }

}
