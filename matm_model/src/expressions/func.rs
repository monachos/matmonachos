use std::fmt;
use std::rc::Rc;
use crate::expressions::Expression;
use crate::expressions::ToExpression;
use crate::expressions::precedence::OperatorPrec;
use crate::expressions::precedence::OperatorPrecAccess;


#[derive(Clone, PartialEq, Debug)]
pub struct FunctionData {
    pub name: String,
    pub arguments: Vec<Rc<Expression>>,
}

impl FunctionData {
    pub const MATRIX: &'static str = "matrix";
    pub const DET_CONFIG: &'static str = "det_config";
    pub const GAUSS_CONFIG: &'static str = "gauss_config";
    pub const INV_CONFIG: &'static str = "inv_config";
    pub const CRACOVIAN: &'static str = "cracovian";
    pub const PASCAL_MATRIX_CONFIG: &'static str = "pascal_matrix_config";
    pub const SIN: &'static str = "sin";
    pub const COS: &'static str = "cos";
    pub const TAN: &'static str = "tan";
    pub const COT: &'static str = "cot";
    pub const SEC: &'static str = "sec";
    pub const CSC: &'static str = "csc";
    pub const SINH: &'static str = "sinh";
    pub const COSH: &'static str = "cosh";
    pub const TANH: &'static str = "tanh";
    pub const COTH: &'static str = "coth";
    pub const SECH: &'static str = "sech";
    pub const CSCH: &'static str = "csch";
    pub const RADIX: &'static str = "radix";
    pub const BINOMIAL: &'static str = "binomial";
    pub const FACTORIAL: &'static str = "factorial";
    pub const LOG: &'static str = "log";
    pub const LOG2: &'static str = "log2";
    pub const LOG10: &'static str = "log10";
    pub const ROOT: &'static str = "root";
    pub const SQRT: &'static str = "sqrt";
    pub const EQUATIONS: &'static str = "equations";

    pub fn new(name: String, args: Vec<Rc<Expression>>) -> Self {
        Self {
            name: name,
            arguments: args,
        }
    }

    pub fn has_symbol(&self) -> bool {
        return self.arguments.iter().any(|e| e.has_symbol());
    }

    pub fn is_trigonometric(&self) -> bool {
        Self::SIN.eq(&self.name)
            || Self::COS.eq(&self.name)
            || Self::TAN.eq(&self.name)
            || Self::COT.eq(&self.name)
            || Self::SEC.eq(&self.name)
            || Self::CSC.eq(&self.name)
            || Self::SINH.eq(&self.name)
            || Self::COSH.eq(&self.name)
            || Self::TANH.eq(&self.name)
            || Self::COTH.eq(&self.name)
            || Self::SECH.eq(&self.name)
            || Self::CSCH.eq(&self.name)
    }

    pub fn can_have_no_parenthesis(&self) -> bool {
        if self.is_trigonometric() && self.arguments.len() == 1 {
            if self.arguments[0].operator_prec() < self.operator_prec() {
                false
            } else {
                true
            }
        } else {
            false
        }
    }
}

impl ToExpression for FunctionData {
    fn to_expr(self) -> Expression {
        return Expression::Function(Box::new(self));
    }

    fn to_rc_expr(self) -> Rc<Expression> {
        return Rc::new(self.to_expr());
    }
}

impl OperatorPrecAccess for FunctionData {
    fn operator_prec(&self) -> OperatorPrec {
        return if self.is_trigonometric() {
            OperatorPrec::Function
        } else {
            OperatorPrec::Method
        };
    }
}

impl fmt::Display for FunctionData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}({})",
            self.name,
            self.arguments.iter().map(|e| format!("{}", e)).collect::<Vec<String>>().join(", ")
        )?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use rstest::rstest;
    use crate::expressions::binary::BinaryData;
    use crate::expressions::func::FunctionData;
    use crate::expressions::ToExpression;
    use crate::factories::ObjFactory;

    #[test]
    fn test_new() {
        let fa = ObjFactory::new10();
        let m = FunctionData::new("foo".to_owned(), vec![fa.rzi_u(1)]);

        assert_eq!("foo", m.name);
        assert_eq!(vec![fa.rzi_u(1)], m.arguments);
    }

    #[rstest]
    #[case(FunctionData::SIN, "sin(0)")]
    #[case(FunctionData::COS, "cos(0)")]
    #[case(FunctionData::TAN, "tan(0)")]
    #[case(FunctionData::COT, "cot(0)")]
    #[case(FunctionData::SEC, "sec(0)")]
    #[case(FunctionData::CSC, "csc(0)")]
    #[case(FunctionData::SINH, "sinh(0)")]
    #[case(FunctionData::COSH, "cosh(0)")]
    #[case(FunctionData::TANH, "tanh(0)")]
    #[case(FunctionData::COTH, "coth(0)")]
    #[case(FunctionData::SECH, "sech(0)")]
    #[case(FunctionData::CSCH, "csch(0)")]
    fn test_format_trigonometric(
        #[case] func_name: &str,
        #[case] expected: &str,
    ) {
        let fa = ObjFactory::new10();
        let f = FunctionData::new(func_name.to_owned(), vec![fa.rzi_u(0)]);

        assert_eq!(expected, format!("{}", f));
    }

    #[test]
    fn test_has_symbol_in_arg_int() {
        let fa = ObjFactory::new10();
        let m = FunctionData::new("foo".to_owned(), vec![fa.rzi_u(1)]);

        assert_eq!(false, m.has_symbol());
    }

    #[test]
    fn test_has_symbol_in_arg_symbol() {
        let fa = ObjFactory::new10();
        let m = FunctionData::new("foo".to_owned(), vec![fa.rsn("x")]);

        assert_eq!(true, m.has_symbol());
    }

    #[test]
    fn test_format_no_args() {
        let m = FunctionData::new("foo".to_owned(), Vec::new());

        assert_eq!("foo()", format!("{}", m));
    }

    #[test]
    fn test_format_with_args() {
        let fa = ObjFactory::new10();
        let m = FunctionData::new("foo".to_owned(), vec![fa.rz0(), fa.rsn("a")]);

        assert_eq!("foo(0, a)", format!("{}", m));
    }

    #[test]
    fn test_can_have_no_parenthesis_for_sin_of_int() {
        let fa = ObjFactory::new10();
        let f = FunctionData::new("sin".to_owned(), vec![fa.rzi_u(0)]);

        let result = f.can_have_no_parenthesis();
        assert_eq!(true, result);
    }

    #[test]
    fn test_can_have_no_parenthesis_for_sin_of_bin() {
        let fa = ObjFactory::new10();
        let f = FunctionData::new("sin".to_owned(), vec![
            BinaryData::add(
                fa.rzi_u(2),
                fa.rzi_u(-3),
            ).to_rc_expr(),
        ]);

        let result = f.can_have_no_parenthesis();
        assert_eq!(false, result);
    }

    #[test]
    fn test_can_have_no_parenthesis_for_custom() {
        let fa = ObjFactory::new10();
        let f = FunctionData::new("custom".to_owned(), vec![fa.rzi_u(0)]);

        let result = f.can_have_no_parenthesis();
        assert_eq!(false, result);
    }
}
