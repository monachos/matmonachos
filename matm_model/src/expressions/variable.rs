use std::fmt;
use std::rc::Rc;
use crate::expressions::Expression;
use crate::expressions::ToExpression;
use crate::expressions::precedence::OperatorPrec;
use crate::expressions::precedence::OperatorPrecAccess;
use crate::expressions::symbol::SymbolData;


#[derive(Clone)]
#[derive(PartialEq)]
#[derive(Debug)]
pub struct VariableData {
    pub name: SymbolData,
    pub value: Rc<Expression>,
}

impl VariableData {
    pub fn new(name: SymbolData, value: Rc<Expression>) -> Self {
        Self {
            name: name,
            value: value,
        }
    }
}

impl ToExpression for VariableData {
    fn to_expr(self) -> Expression {
        return Expression::Variable(Box::new(self));
    }

    fn to_rc_expr(self) -> Rc<Expression> {
        return Rc::new(self.to_expr());
    }
}

impl OperatorPrecAccess for VariableData {
    fn operator_prec(&self) -> OperatorPrec {
        return OperatorPrec::Atom;
    }
}

impl fmt::Display for VariableData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.name)
    }
}

#[cfg(test)]
mod tests {
    use rstest::rstest;
    use crate::expressions::symbol::SymbolData;
    use crate::expressions::variable::VariableData;
    use crate::factories::ObjFactory;
    use crate::testing::asserts::assert_variable_that;


    #[test]
    fn test_variable_new() {
        let fa = ObjFactory::new10();
        let v = VariableData::new(SymbolData::new("a"), fa.rzi_u(2));

        assert_variable_that(&v)
            .has_name("a");
    }

    #[test]
    fn test_format_name() {
        let fa = ObjFactory::new10();
        let v = VariableData::new(SymbolData::new("a"), fa.rzi_u(2));

        assert_eq!("a", format!("{}", v));
    }

    #[test]
    fn test_format_name_with_index() {
        let fa = ObjFactory::new10();
        let v = VariableData::new(SymbolData::with_index_e("a", fa.rzi_u(1)), fa.rzi_u(2));

        assert_eq!("a(1)", format!("{}", v));
    }

    #[test]
    fn test_partial_eq_name() {
        let fa = ObjFactory::new10();
        let v1 = VariableData::new(SymbolData::new("a"), fa.rzi_u(2));
        let v2 = VariableData::new(SymbolData::new("a"), fa.rzi_u(2));

        assert_eq!(true, v1 == v2);
    }

    #[rstest]
    #[case(("a", 2), ("b", 2))]
    #[case(("a", 2), ("a", 3))]
    fn test_partial_neq_name(
        #[case] left: (&str, i32),
        #[case] right: (&str, i32),
    ) {
        let fa = ObjFactory::new10();
        let v1 = VariableData::new(SymbolData::new(left.0), fa.rzi_u(left.1));
        let v2 = VariableData::new(SymbolData::new(right.0), fa.rzi_u(right.1));

        assert_eq!(false, v1 == v2);
    }

}
