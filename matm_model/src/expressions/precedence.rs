// Operator precedence
// Rules of arithmetic: https://www.mathcentre.ac.uk/resources/uploaded/mc-ty-rules-2009-1.pdf

use std::cmp::Ordering;
use std::cmp::PartialOrd;
use std::fmt;


#[derive(PartialEq, Copy, Clone)]
pub enum OperatorPrec {
    Atom,
    Factorial,
    Method,
    Minus,
    Exp,
    Fraction,
    MatrixFunction,
    MulDiv,
    Function,
    AddSub,
    IteratedBinary,
    ArithmeticComparison,
    LogicAnd,
    LogicOr,
    Equation,
    EquationSystem,
    Assignment,
}

// Functions of monomial:
// sin 2x = sin (2*x)
// sin 2*x = sin (2*x)
// sin x + y = sin (x) + y

// Matrix functions
// det [a b] * [c d] = (det [a b]) * [c d]
// ?? det A*B = (det A) * B

impl PartialOrd for OperatorPrec {
    fn partial_cmp(&self, other: &OperatorPrec) -> Option<std::cmp::Ordering> {
        let left = *self as isize;
        let right = *other as isize;

        // exception for minus - has always the lowest priority
        if left == right {
            return Some(Ordering::Equal);
        }

        if let OperatorPrec::Minus = self {
            return Some(Ordering::Less);
        }
        if let OperatorPrec::Minus = other {
            return Some(Ordering::Less);
        }

        // position in hierarchy is inverted enum value
        if left < right {
            Some(Ordering::Greater)
        } else if left > right {
            Some(Ordering::Less)
        } else {
            Some(Ordering::Equal)
        }
    }
}

impl fmt::Display for OperatorPrec {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let text = match *self {
            Self::Atom => "x",
            Self::Method => "method",
            Self::Factorial => "!",
            Self::Exp => "^",
            Self::Fraction => "/",
            Self::MatrixFunction => "mx-fn",
            Self::MulDiv => "*/",
            Self::Function => "fn",
            Self::AddSub => "+-",
            Self::Minus => "-",
            Self::IteratedBinary => "iter bin",
            Self::LogicAnd => "and",
            Self::LogicOr => "or",
            Self::ArithmeticComparison => "arithm op",
            Self::Equation => "=",
            Self::EquationSystem => "eq system",
            Self::Assignment => ":",
        };
        write!(f, "{}", text)
    }
}

pub trait OperatorPrecAccess {
    fn operator_prec(&self) -> OperatorPrec;
}

#[cfg(test)]
mod tests {
    use rstest::rstest;
    use crate::expressions::precedence::OperatorPrec;
    use std::cmp::Ordering;

    #[rstest]
    #[case(OperatorPrec::Atom, OperatorPrec::Factorial)]
    #[case(OperatorPrec::Factorial, OperatorPrec::Method)]
    #[case(OperatorPrec::Method, OperatorPrec::Exp)]
    #[case(OperatorPrec::Exp, OperatorPrec::Fraction)]
    #[case(OperatorPrec::Fraction, OperatorPrec::MatrixFunction)]
    #[case(OperatorPrec::MulDiv, OperatorPrec::Function)]
    #[case(OperatorPrec::Function, OperatorPrec::AddSub)]
    #[case(OperatorPrec::LogicOr, OperatorPrec::Assignment)]
    #[case(OperatorPrec::Atom, OperatorPrec::Exp)]
    fn test_partial_ord_greater(
        #[case] left: OperatorPrec,
        #[case] right: OperatorPrec,
    ) {
        let result = left.partial_cmp(&right).unwrap();
        println!("Comparing {} and {}", left, right);
        assert_eq!(result, Ordering::Greater);
    }

    #[rstest]
    #[case(OperatorPrec::Assignment, OperatorPrec::EquationSystem)]
    #[case(OperatorPrec::EquationSystem, OperatorPrec::Equation)]
    #[case(OperatorPrec::Equation, OperatorPrec::AddSub)]
    #[case(OperatorPrec::AddSub, OperatorPrec::Function)]
    #[case(OperatorPrec::Function, OperatorPrec::MulDiv)]
    #[case(OperatorPrec::MulDiv, OperatorPrec::MatrixFunction)]
    #[case(OperatorPrec::MatrixFunction, OperatorPrec::Fraction)]
    #[case(OperatorPrec::Fraction, OperatorPrec::Exp)]
    #[case(OperatorPrec::Exp, OperatorPrec::Method)]
    #[case(OperatorPrec::Method, OperatorPrec::Factorial)]
    #[case(OperatorPrec::Factorial, OperatorPrec::Atom)]
    #[case(OperatorPrec::Exp, OperatorPrec::Atom)]
    fn test_partial_ord_less(
        #[case] left: OperatorPrec,
        #[case] right: OperatorPrec,
    ) {
        let result = left.partial_cmp(&right).unwrap();
        println!("Comparing {} and {}", left, right);
        assert_eq!(result, Ordering::Less);
    }

    #[rstest]
    #[case(OperatorPrec::Minus, OperatorPrec::AddSub)]
    #[case(OperatorPrec::AddSub, OperatorPrec::Minus)]
    #[case(OperatorPrec::Minus, OperatorPrec::Factorial)]
    #[case(OperatorPrec::Factorial, OperatorPrec::Minus)]
    fn test_partial_ord_minus(
        #[case] left: OperatorPrec,
        #[case] right: OperatorPrec,
    ) {
        // Minus is always less
        let result = left.partial_cmp(&right).unwrap();
        println!("Comparing {} and {}", left, right);
        assert_eq!(result, Ordering::Less);
    }
}
