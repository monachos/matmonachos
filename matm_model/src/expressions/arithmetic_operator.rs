use std::fmt;
use crate::expressions::precedence::OperatorPrec;
use crate::expressions::precedence::OperatorPrecAccess;

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum ArithmeticOperator {
    Equal,
    NotEqual,
    Greater,
    GreaterOrEqual,
    Less,
    LessOrEqual,
}

impl OperatorPrecAccess for ArithmeticOperator {
    fn operator_prec(&self) -> OperatorPrec {
        return OperatorPrec::ArithmeticComparison;
    }
}

impl fmt::Display for ArithmeticOperator {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let symbol = match self {
            ArithmeticOperator::Equal => "=",
            ArithmeticOperator::NotEqual => "<>",
            ArithmeticOperator::Greater => ">",
            ArithmeticOperator::GreaterOrEqual => ">=",
            ArithmeticOperator::Less => "<",
            ArithmeticOperator::LessOrEqual => "<=",
        };
        write!(f, "{}", symbol)
    }
}


#[cfg(test)]
mod tests {
    use rstest::rstest;
    use crate::expressions::arithmetic_operator::ArithmeticOperator;

    #[rstest]
    #[case(ArithmeticOperator::Equal, "=")]
    #[case(ArithmeticOperator::NotEqual, "<>")]
    #[case(ArithmeticOperator::Greater, ">")]
    #[case(ArithmeticOperator::GreaterOrEqual, ">=")]
    #[case(ArithmeticOperator::Less, "<")]
    #[case(ArithmeticOperator::LessOrEqual, "<=")]
    fn test_arithmetic_operator_format(#[case] op: ArithmeticOperator, #[case] expected: &str) {
        let result = format!("{}", op);
        assert_eq!(expected, result);
    }
}
