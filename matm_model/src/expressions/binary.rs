use std::fmt;
use std::rc::Rc;
use crate::expressions::Expression;
use crate::expressions::ToExpression;
use crate::expressions::precedence::OperatorPrec;
use crate::expressions::precedence::OperatorPrecAccess;
use crate::expressions::sign2::Sign2;
use crate::write_wrapped_obj;

#[derive(Clone, Copy)]
#[derive(PartialEq)]
#[derive(Debug)]
pub enum BinaryOperator {
    Add,
    Sub,
    Mul,
    Div,
}

impl fmt::Display for BinaryOperator {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let symbol = match self {
            Self::Add => String::from("+"),
            Self::Sub => String::from("-"),
            Self::Mul => String::from("*"),
            Self::Div => String::from("div"),
        };
        write!(f, "{}", symbol)
    }
}


#[derive(Clone)]
#[derive(PartialEq)]
#[derive(Debug)]
pub struct BinaryData {
    pub operator: BinaryOperator,
    pub left: Rc<Expression>,
    pub right: Rc<Expression>,
}

impl BinaryData {
    pub fn from_operator(op: BinaryOperator, left: Rc<Expression>, right: Rc<Expression>) -> Self {
        Self {
            operator: op,
            left: left,
            right: right,
        }
    }

    pub fn add(left: Rc<Expression>, right: Rc<Expression>) -> Self {
        Self::from_operator(BinaryOperator::Add, left, right)
    }

    pub fn sub(left: Rc<Expression>, right: Rc<Expression>) -> Self {
        Self::from_operator(BinaryOperator::Sub, left, right)
    }

    pub fn add_or_sub(left: Rc<Expression>, right: Rc<Expression>, sign: Sign2) -> Self {
        match sign {
            Sign2::Positive => Self::add(left, right),
            Sign2::Negative => Self::sub(left, right),
        }
    }

    pub fn mul(left: Rc<Expression>, right: Rc<Expression>) -> Self {
        Self::from_operator(BinaryOperator::Mul, left, right)
    }

    pub fn div(left: Rc<Expression>, right: Rc<Expression>) -> Self {
        Self::from_operator(BinaryOperator::Div, left, right)
    }

    pub fn as_text_operator(&self) -> String {
        return self.operator.to_string();
    }

    pub fn has_symbol(&self) -> bool {
        return self.left.has_symbol() || self.right.has_symbol();
    }

    pub fn should_wrap_left_with_parentheses(&self) -> bool {
        self.left.operator_prec() < self.operator_prec()
    }

    pub fn should_wrap_right_with_parentheses(&self) -> bool {
        if self.right.operator_prec() < self.operator_prec() {
            return true;
        }

        if let BinaryOperator::Sub = self.operator {
            if let Expression::Binary(bin) = self.right.as_ref() {
                match bin.operator {
                    BinaryOperator::Add | BinaryOperator::Sub => {
                        return true;
                    }
                    _ => {}
                }
            }
        }
        return false;
    }
}

impl ToExpression for BinaryData {
    fn to_expr(self) -> Expression {
        return Expression::Binary(Box::new(self));
    }
    
    fn to_rc_expr(self) -> Rc<Expression> {
        return Rc::new(self.to_expr());
    }
}

impl OperatorPrecAccess for BinaryData {
    fn operator_prec(&self) -> OperatorPrec {
        return match self.operator {
            BinaryOperator::Add | BinaryOperator::Sub => OperatorPrec::AddSub,
            BinaryOperator::Mul | BinaryOperator::Div => OperatorPrec::MulDiv,
        };
    }
}

impl fmt::Display for BinaryData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write_wrapped_obj(f, &self.left, self.should_wrap_left_with_parentheses())?;
        write!(f, " {} ", self.as_text_operator())?;
        write_wrapped_obj(f, &self.right, self.should_wrap_right_with_parentheses())?;
        Ok(())
    }
}

