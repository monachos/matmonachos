use std::fmt;
use std::rc::Rc;
use crate::expressions::Expression;
use crate::expressions::ToExpression;
use crate::expressions::precedence::OperatorPrec;
use crate::expressions::precedence::OperatorPrecAccess;


#[derive(Clone)]
#[derive(PartialEq)]
#[derive(Debug)]
pub struct ImData {
    pub name: String,
}

impl ImData {
    pub fn new(name: &str) -> Self {
        Self {
            name: String::from(name),
        }
    }
}

impl ToExpression for ImData {
    fn to_expr(self) -> Expression {
        Expression::Im(Box::new(self))
    }

    fn to_rc_expr(self) -> Rc<Expression> {
        Rc::new(self.to_expr())
    }
}

impl OperatorPrecAccess for ImData {
    fn operator_prec(&self) -> OperatorPrec {
        return OperatorPrec::Atom;
    }
}

impl fmt::Display for ImData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "%{}", self.name)
    }
}

#[cfg(test)]
mod tests {
    use crate::expressions::imaginary::ImData;

    #[test]
    fn test_im_new() {
        let expr = ImData::new("i");

        assert_eq!(expr.name, "i");
    }

    #[test]
    fn test_format_symbol() {
        let expr = ImData::new("i");

        assert_eq!("%i", format!("{}", expr));
    }
}
