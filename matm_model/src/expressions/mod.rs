use std::cmp::PartialEq;
use std::fmt;
use std::rc::Rc;
use crate::expressions::int::IntData;
use crate::expressions::binary::BinaryData;
use crate::expressions::binary::BinaryOperator;
use crate::expressions::binomial::BinomialData;
use crate::expressions::bool::BoolData;
use crate::expressions::conditional::ConditionalData;
use crate::expressions::cracovians::CracovianData;
use crate::expressions::exponentiation::ExpData;
use crate::expressions::equation::EquationData;
use crate::expressions::soe::SysOfEqData;
use crate::expressions::fractions::FracData;
use crate::expressions::imaginary::ImData;
use crate::expressions::iterated_binary::IteratedBinaryData;
use crate::expressions::logarithm::LogData;
use crate::expressions::matrices::MatrixData;
use crate::expressions::precedence::OperatorPrec;
use crate::expressions::precedence::OperatorPrecAccess;
use crate::expressions::root::RootData;
use crate::expressions::symbol::SymbolData;
use crate::expressions::transcendental::TranscData;
use crate::expressions::unary::UnaryData;
use crate::expressions::variable::VariableData;

use self::assignment::AssignmentData;
use self::func::FunctionData;
use self::method::MethodData;
use self::object::ObjectData;


pub mod assignment;
pub mod binary;
mod binary_tests;
pub mod binomial;
pub mod bool;
pub mod conditional;
pub mod cracovians;
mod cracovians_tests;
pub mod equation;
pub mod soe;
pub mod exponentiation;
pub mod fractions;
pub mod func;
pub mod imaginary;
pub mod int;
mod int_tests;
pub mod iterated_binary;
pub mod logarithm;
pub mod matrices;
mod matrices_tests;
pub mod method;
pub mod object;
pub mod precedence;
pub mod root;
pub mod sign;
pub mod sign2;
pub mod symbol;
mod testing;
pub mod transcendental;
pub mod unary;
pub mod variable;
pub mod logic_operator;
pub mod logic_condition;
pub mod arithmetic_condition;
pub mod arithmetic_operator;
pub mod condition;

#[derive(Clone)]
#[derive(Debug)]
pub enum Expression {
    Undefined,
    Assignment(Box<AssignmentData>),
    Binary(Box<BinaryData>),
    Bool(Box<BoolData>),
    BoxSign,
    Conditional(Box<ConditionalData>),
    Cracovian(Box<CracovianData>),
    Exp(Box<ExpData>),
    Equation(Box<EquationData>),
    SysOfEq(Box<SysOfEqData>),
    Function(Box<FunctionData>),
    Im(Box<ImData>),
    Int(Box<IntData>),
    IteratedBinary(Box<IteratedBinaryData>),
    Frac(Box<FracData>),
    Log(Box<LogData>),
    Matrix(Box<MatrixData>),
    Method(Box<MethodData>),
    Object(Box<ObjectData>),
    Root(Box<RootData>),
    Symbol(Box<SymbolData>),
    Transc(Box<TranscData>),
    Unary(Box<UnaryData>),
    Variable(Box<VariableData>),
    Binomial(Box<BinomialData>),
}

impl Expression {
    pub fn binary_mul_if_needed(left: Rc<Expression>, right: Rc<Expression>) -> Rc<Expression> {
        if left.is_one() {
            return right;
        } else if right.is_one() {
            return left;
        } else {
            return BinaryData::mul(left, right).to_rc_expr();
        }
    }

    /// This function cannot be used for rendering
    pub fn is_zero(&self) -> bool {
        return match self {
            Expression::Int(value) => value.is_zero(),
            Expression::Frac(value) => value.is_zero(),
            _ => false,
        };
    }

    /// This function cannot be used for rendering
    pub fn is_one(&self) -> bool {
        return match self {
            Expression::Int(value) => value.is_one(),
            Expression::Frac(value) => value.is_one(),
            _ => false,
        };
    }

    pub fn is_int_2(&self) -> bool {
        if let Expression::Int(v) = self {
            if let Some(the_v) = v.as_ref().to_i32() {
                return the_v == 2;
            }
        }
        false
    }

    pub fn is_minus_one(&self) -> bool {
        return match self {
            Expression::Int(value) => value.is_minus_one(),
            _ => false,
        };
    }

    /// Returns name of the expression type.
    pub fn type_name(&self) -> String {
        return match self {
            Expression::Undefined => String::from("undefined"),
            Expression::Assignment(_) => String::from("assignment"),
            Expression::Bool(_) => String::from("bool"),
            Expression::BoxSign => String::from("box"),
            Expression::Equation(_) => String::from("equation"),
            Expression::SysOfEq(_) => String::from("equation system"),
            Expression::Int(_) => String::from("integer"),
            Expression::Frac(_) => String::from("fraction"),
            Expression::Cracovian(_) => String::from("cracovian"),
            Expression::Matrix(_) => String::from("matrix"),
            Expression::Symbol(_) => String::from("symbol"),
            Expression::Unary(_) => String::from("unary"),
            Expression::Binary(_) => String::from("binary"),
            Expression::IteratedBinary(_) => String::from("iterated binary"),
            Expression::Function(_) => String::from("function"),
            Expression::Method(_) => String::from("method"),
            Expression::Object(_) => String::from("object"),
            Expression::Exp(_) => String::from("expression"),
            Expression::Root(_) => String::from("root"),
            Expression::Log(_) => String::from("logarithm"),
            Expression::Im(_) => String::from("im"),
            Expression::Transc(_) => String::from("transcendental number"),
            Expression::Variable(_) => String::from("variable"),
            Expression::Conditional(_) => String::from("conditional"),
            Expression::Binomial(_) => String::from("binomial"),
        }
    }

    pub fn is_fraction(&self) -> bool {
        return match self {
            Expression::Frac(_) => true,
            _ => false,
        };
    }

    pub fn is_product(&self) -> bool {
        match self {
            Expression::Binary(bin) => {
                if let BinaryOperator::Mul = bin.operator {
                    return true;
                }
            }
            _ => {}
        }
    
        return false;
    }

    pub fn has_symbol(&self) -> bool {
        return match self {
            Expression::Undefined => false,
            Expression::Assignment(o) => o.has_symbol(),
            Expression::Bool(_) => false,
            Expression::BoxSign => false,
            Expression::Equation(o) => o.has_symbol(),
            Expression::SysOfEq(o) => o.has_symbol(),
            Expression::Int(_) => false,
            Expression::Frac(o) => o.has_symbol(),
            Expression::Cracovian(o) => o.has_symbol(),
            Expression::Matrix(o) => o.has_symbol(),
            Expression::Symbol(_) => true,
            Expression::Unary(o) => o.has_symbol(),
            Expression::Function(o) => o.has_symbol(),
            Expression::Method(o) => o.has_symbol(),
            Expression::Object(_) => false,
            Expression::Exp(o) => o.has_symbol(),
            Expression::Root(o) => o.has_symbol(),
            Expression::Log(o) => o.has_symbol(),
            Expression::Binary(o) => o.has_symbol(),
            Expression::IteratedBinary(_) => true,
            Expression::Im(_) => false,
            Expression::Transc(_) => false,
            Expression::Variable(_) => true,
            Expression::Conditional(o) => o.has_symbol(),
            Expression::Binomial(o) => o.has_symbol(),
        };
    }
}

impl fmt::Display for Expression {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        return match self {
            Expression::Undefined => write!(f, "?"),
            Expression::Assignment(obj) => write!(f, "{}", obj),
            Expression::Bool(obj) => write!(f, "{}", obj),
            Expression::BoxSign => write!(f, "()"),
            Expression::Equation(obj) => write!(f, "{}", obj),
            Expression::SysOfEq(obj) => write!(f, "{}", obj),
            Expression::Int(obj) => write!(f, "{}", obj),
            Expression::Frac(obj) => write!(f, "{}", obj),
            Expression::Cracovian(obj) => write!(f, "{}", obj),
            Expression::Matrix(obj) => write!(f, "{}", obj),
            Expression::Symbol(obj) => write!(f, "{}", obj),
            Expression::Unary(obj) => write!(f, "{}", obj),
            Expression::Binary(obj) => write!(f, "{}", obj),
            Expression::IteratedBinary(obj) => write!(f, "{}", obj),
            Expression::Function(obj) => write!(f, "{}", obj),
            Expression::Method(obj) => write!(f, "{}", obj),
            Expression::Object(obj) => write!(f, "{}", obj),
            Expression::Exp(obj) => write!(f, "{}", obj),
            Expression::Root(obj) => write!(f, "{}", obj),
            Expression::Log(obj) => write!(f, "{}", obj),
            Expression::Im(obj) => write!(f, "{}", obj),
            Expression::Transc(obj) => write!(f, "{}", obj),
            Expression::Variable(obj) => write!(f, "{}", obj),
            Expression::Conditional(obj) => write!(f, "{}", obj),
            Expression::Binomial(obj) => write!(f, "{}", obj),
        };
    }
}

impl PartialEq for Expression {
    fn eq(&self, other: &Expression) -> bool {
        return match (self, other) {
            (Expression::BoxSign, Expression::BoxSign) => true,
            (Expression::Assignment(o1), Expression::Assignment(o2)) => o1 == o2,
            (Expression::Equation(o1), Expression::Equation(o2)) => o1 == o2,
            (Expression::SysOfEq(o1), Expression::SysOfEq(o2)) => o1 == o2,
            (Expression::Int(o1), Expression::Int(o2)) => o1 == o2,
            (Expression::Frac(o1), Expression::Frac(o2)) => o1 == o2,
            (Expression::Cracovian(o1), Expression::Cracovian(o2)) => o1 == o2,
            (Expression::Matrix(o1), Expression::Matrix(o2)) => o1 == o2,
            (Expression::Symbol(o1), Expression::Symbol(o2)) => o1 == o2,
            (Expression::Unary(o1), Expression::Unary(o2)) => o1 == o2,
            (Expression::Binary(o1), Expression::Binary(o2)) => o1 == o2,
            (Expression::IteratedBinary(o1), Expression::IteratedBinary(o2)) => o1 == o2,
            (Expression::Function(o1), Expression::Function(o2)) => o1 == o2,
            (Expression::Method(o1), Expression::Method(o2)) => o1 == o2,
            (Expression::Object(o1), Expression::Object(o2)) => o1 == o2,
            (Expression::Exp(o1), Expression::Exp(o2)) => o1 == o2,
            (Expression::Root(o1), Expression::Root(o2)) => o1 == o2,
            (Expression::Log(o1), Expression::Log(o2)) => o1 == o2,
            (Expression::Im(o1), Expression::Im(o2)) => o1 == o2,
            (Expression::Transc(o1), Expression::Transc(o2)) => o1 == o2,
            (Expression::Variable(o1), Expression::Variable(o2)) => o1 == o2,
            (Expression::Conditional(o1), Expression::Conditional(o2)) => o1 == o2,
            (Expression::Binomial(o1), Expression::Binomial(o2)) => o1 == o2,
            _ => false,
        }
    }
}

//----------------------------------------------------------

pub trait ToExpression {
    fn to_expr(self) -> Expression;
    fn to_rc_expr(self) -> Rc<Expression>;
}

impl OperatorPrecAccess for Expression {
    fn operator_prec(&self) -> OperatorPrec {
        return match self {
            Expression::Undefined => OperatorPrec::Atom,
            Expression::Assignment(expr) => expr.operator_prec(),
            Expression::Bool(expr) => expr.operator_prec(),
            Expression::BoxSign => OperatorPrec::Atom,
            Expression::Equation(expr) => expr.operator_prec(),
            Expression::SysOfEq(expr) => expr.operator_prec(),
            Expression::Int(expr) => expr.operator_prec(),
            Expression::Frac(expr) => expr.operator_prec(),
            Expression::Cracovian(expr) => expr.operator_prec(),
            Expression::Matrix(expr) => expr.operator_prec(),
            Expression::Symbol(expr) => expr.operator_prec(),
            Expression::Unary(expr) => expr.operator_prec(),
            Expression::Binary(expr) => expr.operator_prec(),
            Expression::IteratedBinary(expr) => expr.operator_prec(),
            Expression::Function(expr) => expr.operator_prec(),
            Expression::Method(expr) => expr.operator_prec(),
            Expression::Object(expr) => expr.operator_prec(),
            Expression::Exp(expr) => expr.operator_prec(),
            Expression::Root(expr) => expr.operator_prec(),
            Expression::Log(expr) => expr.operator_prec(),
            Expression::Im(expr) => expr.operator_prec(),
            Expression::Transc(expr) => expr.operator_prec(),
            Expression::Variable(expr) => expr.operator_prec(),
            Expression::Conditional(expr) => expr.operator_prec(),
            Expression::Binomial(expr) => expr.operator_prec(),
        };
    }
}

#[cfg(test)]
mod tests {
    use std::mem::size_of;
    use crate::expressions::Expression;
    use crate::factories::ObjFactory;

    #[test]
    fn test_max_expr_size() {
        let result = size_of::<Expression>();
        assert!(result <= 16, "Actual size {} is too big.", result);
    }

    #[test]
    fn test_is_int_2_of_2() {
        let fa = ObjFactory::new10();
        let expr = fa.ezi_u(2);
        let result = expr.is_int_2();
        assert_eq!(true, result);
    }

    #[test]
    fn test_is_int_2_of_3() {
        let fa = ObjFactory::new10();
        let expr = fa.ezi_u(3);
        let result = expr.is_int_2();
        assert_eq!(false, result);
    }

    #[test]
    fn test_is_int_2_of_symbol() {
        let fa = ObjFactory::new10();
        let expr = fa.esn("x");
        let result = expr.is_int_2();
        assert_eq!(false, result);
    }
}
