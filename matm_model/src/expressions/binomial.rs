use std::fmt;
use std::rc::Rc;

use crate::expressions::Expression;
use crate::expressions::ToExpression;
use crate::expressions::precedence::OperatorPrec;
use crate::expressions::precedence::OperatorPrecAccess;

/// Binomial coefficients
#[derive(Debug)]
#[derive(PartialEq)]
#[derive(Clone)]
pub struct BinomialData {
    pub set: Rc<Expression>,
    pub subset: Rc<Expression>,
}

impl BinomialData {
    pub fn new(set: Rc<Expression>, subset: Rc<Expression>) -> Self {
        Self {
            set,
            subset,
        }
    }

    pub fn has_symbol(&self) -> bool {
        return self.set.has_symbol() || self.subset.has_symbol();
    }
}

impl OperatorPrecAccess for BinomialData {
    fn operator_prec(&self) -> OperatorPrec {
        return OperatorPrec::Atom;
    }
}

impl ToExpression for BinomialData {
    fn to_expr(self) -> Expression {
        Expression::Binomial(Box::new(self))
    }

    fn to_rc_expr(self) -> Rc<Expression> {
        return Rc::new(self.to_expr());
    }
}

impl fmt::Display for BinomialData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{{{}, {}}}", self.set, self.subset)
    }
}

#[cfg(test)]
mod tests {
    use crate::expressions::ToExpression;
    use crate::expressions::symbol::SymbolData;
    use crate::expressions::binomial::BinomialData;
    use crate::factories::ObjFactory;
    use crate::testing::asserts::assert_expression_that;


    #[test]
    fn test_new() {
        let expr = BinomialData::new(
            SymbolData::new("n").to_rc_expr(),
            SymbolData::new("k").to_rc_expr()
        );

        assert_expression_that(&expr.set)
            .as_symbol()
            .has_name("n");
        assert_expression_that(&expr.subset)
            .as_symbol()
            .has_name("k");
    }

    #[test]
    fn test_format_symbol_symbol() {
        let expr = BinomialData::new(
            SymbolData::new("n").to_rc_expr(),
            SymbolData::new("k").to_rc_expr()
        );

        assert_eq!("{n, k}", format!("{}", expr));
    }

    #[test]
    fn test_format_int_int() {
        let fa = ObjFactory::new10();
        let expr = BinomialData::new(
            fa.rzi_u(10),
            fa.rzi_u(3),
        );

        assert_eq!("{10, 3}", format!("{}", expr));
    }
}
