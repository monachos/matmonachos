use crate::expressions::precedence::OperatorPrec;
use crate::expressions::precedence::OperatorPrecAccess;
use crate::expressions::Expression;
use crate::expressions::ToExpression;
use std::fmt;
use std::rc::Rc;

use super::func::FunctionData;

#[derive(Clone, PartialEq, Debug)]
pub struct MethodData {
    pub object: Rc<Expression>,
    pub func: FunctionData,
}

impl MethodData {
    pub const ABS: &'static str = "abs";
    pub const ADJ: &'static str = "adj";
    pub const BAREISS: &'static str = "bareiss";
    pub const CHOLESKY: &'static str = "cholesky";
    pub const COLUMNS: &'static str = "columns";
    pub const DET: &'static str = "det";
    pub const DIAGONAL: &'static str = "diagonal";
    pub const GAUSS: &'static str = "gauss";
    pub const HILBERT: &'static str = "hilbert";
    pub const IDENTITY: &'static str = "identity";
    pub const INV: &'static str = "inv";
    pub const IS_IDENTITY: &'static str = "is_identity";
    pub const IS_LOWER_TRIANGULAR: &'static str = "is_lower_triangular";
    pub const IS_NEGATIVE: &'static str = "is_negative";
    pub const IS_ONE: &'static str = "is_one";
    pub const IS_ONES: &'static str = "is_ones";
    pub const IS_POSITIVE: &'static str = "is_positive";
    pub const IS_SQUARE: &'static str = "is_square";
    pub const IS_STRICTLY_UPPER_TRIANGULAR: &'static str = "is_strictly_upper_triangular";
    pub const IS_SYMMETRIC: &'static str = "is_symmetric";
    pub const IS_UPPER_TRIANGULAR: &'static str = "is_upper_triangular";
    pub const IS_ZERO: &'static str = "is_zero";
    pub const LAPLACE: &'static str = "laplace";
    pub const LOWER: &'static str = "lower";
    pub const OF: &'static str = "of";
    pub const ONES: &'static str = "ones";
    pub const PASCAL: &'static str = "pascal";
    pub const PIVOT_LESS: &'static str = "pivot_less";
    pub const PIVOT_ZERO: &'static str = "pivot_zero";
    pub const RANK: &'static str = "rank";
    pub const REDUCE_LOWER: &'static str = "reduce_lower";
    pub const REDUCE_UPPER: &'static str = "reduce_upper";
    pub const ROWS: &'static str = "rows";
    pub const SYMMETRIC: &'static str = "symmetric";
    pub const SYSTEM: &'static str = "system";
    pub const TRANSPOSE: &'static str = "transpose";
    pub const UPPER: &'static str = "upper";
    pub const VANDERMONDE: &'static str = "vandermonde";
    pub const ZEROS: &'static str = "zeros";

    pub fn new(obj: Rc<Expression>, name: String, args: Vec<Rc<Expression>>) -> Self {
        Self {
            object: obj,
            func: FunctionData::new(name, args),
        }
    }

    pub fn from_function(obj: Rc<Expression>, func: FunctionData) -> Self {
        Self {
            object: obj,
            func: func,
        }
    }

    pub fn has_symbol(&self) -> bool {
        return self.object.has_symbol() || self.func.arguments.iter().any(|e| e.has_symbol());
    }

    pub fn abs(expr: Rc<Expression>) -> Self {
        return MethodData::new(expr, Self::ABS.to_owned(), Vec::new());
    }

    pub fn should_wrap_object_with_parentheses(&self) -> bool {
        self.object.operator_prec() < self.operator_prec()
    }
}

impl ToExpression for MethodData {
    fn to_expr(self) -> Expression {
        return Expression::Method(Box::new(self));
    }

    fn to_rc_expr(self) -> Rc<Expression> {
        return Rc::new(self.to_expr());
    }
}

impl OperatorPrecAccess for MethodData {
    fn operator_prec(&self) -> OperatorPrec {
        return OperatorPrec::Method;
    }
}

impl fmt::Display for MethodData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // object of called method
        let call_obj = if self.should_wrap_object_with_parentheses() {
            format!("({})", self.object)
        } else {
            format!("{}", self.object)
        };

        write!(f, "{}.{}", call_obj, self.func)?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::expressions::binary::BinaryData;
    use crate::expressions::method::MethodData;
    use crate::expressions::ToExpression;
    use crate::factories::ObjFactory;

    #[test]
    fn test_new() {
        let fa = ObjFactory::new10();
        let m = MethodData::new(fa.rz0(), "foo".to_owned(), vec![fa.rzi_u(1)]);

        assert_eq!("foo", m.func.name);
        assert_eq!(fa.rzi_u(0), m.object);
        assert_eq!(vec![fa.rzi_u(1)], m.func.arguments);
    }

    #[test]
    fn test_has_symbol_in_object_int() {
        let fa = ObjFactory::new10();
        let m = MethodData::new(fa.rz0(), "foo".to_owned(), vec![fa.rzi_u(1)]);

        assert_eq!(false, m.has_symbol());
    }

    #[test]
    fn test_has_symbol_in_object_symbol() {
        let fa = ObjFactory::new10();
        let m = MethodData::new(fa.rsn("x"), "foo".to_owned(), vec![fa.rzi_u(1)]);

        assert_eq!(true, m.has_symbol());
    }

    #[test]
    fn test_has_symbol_in_arg_int() {
        let fa = ObjFactory::new10();
        let m = MethodData::new(fa.rz0(), "foo".to_owned(), vec![fa.rzi_u(1)]);

        assert_eq!(false, m.has_symbol());
    }

    #[test]
    fn test_has_symbol_in_arg_symbol() {
        let fa = ObjFactory::new10();
        let m = MethodData::new(fa.rz0(), "foo".to_owned(), vec![fa.rsn("x")]);

        assert_eq!(true, m.has_symbol());
    }

    #[test]
    fn test_format_symbol_no_args() {
        let fa = ObjFactory::new10();
        let m = MethodData::new(fa.rsn("x"), "foo".to_owned(), Vec::new());

        assert_eq!("x.foo()", format!("{}", m));
    }

    #[test]
    fn test_format_symbol_args() {
        let fa = ObjFactory::new10();
        let m = MethodData::new(fa.rsn("x"), "foo".to_owned(), vec![fa.rz0(), fa.rsn("a")]);

        assert_eq!("x.foo(0, a)", format!("{}", m));
    }

    #[test]
    fn test_format_binary_no_args() {
        let fa = ObjFactory::new10();
        let m = MethodData::new(
            BinaryData::add(fa.rzi_u(2), fa.rzi_u(3)).to_rc_expr(),
            "foo".to_owned(),
            Vec::new(),
        );

        assert_eq!("(2 + 3).foo()", format!("{}", m));
    }

    #[test]
    fn test_format_chain() {
        let fa = ObjFactory::new10();
        let f = MethodData::new(fa.rsn("x"), "first".to_owned(), vec![fa.rz0()]).to_rc_expr();
        let s = MethodData::new(f, "second".to_owned(), vec![fa.rsn("a")]);

        assert_eq!("x.first(0).second(a)", format!("{}", s));
    }
}
