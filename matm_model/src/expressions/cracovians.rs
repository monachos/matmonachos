use core::ops::Range;
use std::fmt;
use std::rc::Rc;

use matm_error::error::CalcError;
use crate::core::index::Index2D;
use crate::core::index::IndexND;
use crate::expressions::int::IntData;
use crate::expressions::Expression;
use crate::expressions::ToExpression;
use crate::expressions::precedence::OperatorPrec;
use crate::expressions::precedence::OperatorPrecAccess;
use crate::grid::Grid;
use crate::core::size::Size2D;
use crate::grid::iterators::DownRight2DElementIterator;
use crate::grid::iterators::RightDown2DElementIterator;


#[derive(PartialEq)]
#[derive(Debug)]
pub struct CracovianData {
    pub grid: Grid<Rc<Expression>>,
}

impl CracovianData {
    const ROW_INDEX: usize = 0;
    const COLUMN_INDEX: usize = 1;

    pub fn zeros(radix: u32, cols: usize, rows: usize) -> Result<Self, CalcError> {
        Ok(Self {
            grid: Grid::from_vector(
                vec![rows, cols],
                vec![IntData::zero(radix)?.to_rc_expr(); rows * cols])?,
        })
    }
    
    pub fn from_vector(cols: usize, rows: usize, elements: Vec<Rc<Expression>>) -> Result<Self, CalcError> {
        Ok(Self {
            grid: Grid::from_vector(vec![rows, cols], elements)?,
        })
    }

    pub fn from_ivector(cols: usize, rows: usize, radix: u32, elements: Vec<i32>) -> Result<Self, CalcError> {
        return Self::from_vector(cols, rows,
            elements.iter()
            .map(|v| IntData::from_int(radix, *v).unwrap().to_rc_expr())
            .collect());
    }

    pub fn rows(&self) -> usize {
        return self.size().rows;
    }

    pub fn columns(&self) -> usize {
        return self.size().columns;
    }

    pub fn size(&self) -> Size2D {
        return Size2D::from_nd(&self.grid.size).unwrap();
    }

    pub fn iter_rows(&self) -> Range<usize> {
        return self.grid.dimension_range(Self::ROW_INDEX).unwrap();
    }

    pub fn iter_columns(&self) -> Range<usize> {
        return self.grid.dimension_range(Self::COLUMN_INDEX).unwrap();
    }

    pub fn iter_right_down(&self) -> RightDown2DElementIterator {
        RightDown2DElementIterator::new(self.rows(), self.columns())
    }

    pub fn iter_down_right(&self) -> DownRight2DElementIterator {
        DownRight2DElementIterator::new(self.rows(), self.columns())
    }

    pub fn is_square(&self) -> bool {
        return self.grid.is_square();
    }

    pub fn has_symbol(&self) -> bool {
        for idx in self.iter_right_down() {
            if self.get(&idx).unwrap().has_symbol() {
                return true;
            }
        }
        return false;
    }

    pub fn get_i_i(&self, col: usize, row: usize) -> Result<&Rc<Expression>, CalcError> {
        return self.grid.get_at(&vec![row, col], Self::format_index);
    }

    pub fn get_mut_i_i(&mut self, col: usize, row: usize) -> Result<&mut Rc<Expression>, CalcError> {
        return self.grid.get_mut_at(&vec![row, col], Self::format_index);
    }

    pub fn get(&self, index: &Index2D) -> Result<&Rc<Expression>, CalcError> {
        return self.grid.get(&IndexND::from_2d(index), Self::format_index);
    }

    pub fn get_mut(&mut self, index: &Index2D) -> Result<&mut Rc<Expression>, CalcError> {
        return self.grid.get_mut(&IndexND::from_2d(index));
    }

    pub fn set_i_i(&mut self, col: usize, row: usize, el: Rc<Expression>) -> Result<(), CalcError> {
        return self.grid.set_at(&vec![row, col], el, Self::format_index);
    }

    pub fn set(&mut self, index: &Index2D, el: Rc<Expression>) -> Result<(), CalcError> {
        return self.grid.set(&IndexND::from_2d(index), el, Self::format_index);
    }

    pub fn format_index(idx: &Vec<usize>) -> String {
        let mut result: String = String::from("(");
        let mut add_sep = false;
        for value in idx.iter().rev() {
            if add_sep {
                result += ", ";
            }
            result += format!("{}", value).as_str();
            add_sep = true;
        }
        result += ")";
        return result;
    }
}

impl Clone for CracovianData {
    fn clone(&self) -> CracovianData {
        let grid = Grid::from_vector(
            vec![self.rows(), self.columns()],
            self.iter_right_down()
            .map(|idx| self.get(&idx).unwrap().clone())
            .collect()
        ).unwrap();
        Self {
            grid: grid,
        }
    }
}

impl ToExpression for CracovianData {
    fn to_expr(self) -> Expression {
        Expression::Cracovian(Box::new(self))
    }

    fn to_rc_expr(self) -> Rc<Expression> {
        Rc::new(self.to_expr())
    }
}

impl OperatorPrecAccess for CracovianData {
    fn operator_prec(&self) -> OperatorPrec {
        return OperatorPrec::Atom;
    }
}

impl fmt::Display for CracovianData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{{")?;
        for r in self.iter_rows() {
            if r > 1 {
                write!(f, "; ")?;
            }
            for c in self.iter_columns() {
                if c > 1 {
                    write!(f, ", ")?;
                }
                write!(f, "{}", *self.get_i_i(c, r).unwrap())?;
            }
        }
        write!(f, "}}")?;
        Ok(())
    }
}
