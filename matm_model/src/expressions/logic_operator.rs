use std::fmt;
use crate::expressions::precedence::OperatorPrec;
use crate::expressions::precedence::OperatorPrecAccess;

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum LogicOperator {
    And,
    Or,
}

impl OperatorPrecAccess for LogicOperator {
    fn operator_prec(&self) -> OperatorPrec {
        return match self {
            Self::And => OperatorPrec::LogicAnd,
            Self::Or => OperatorPrec::LogicOr,
        };
    }
}

impl fmt::Display for LogicOperator {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let symbol = match self {
            LogicOperator::And => "and",
            LogicOperator::Or => "or",
        };
        write!(f, "{}", symbol)
    }
}


#[cfg(test)]
mod tests {
    use rstest::rstest;
    use crate::expressions::logic_operator::LogicOperator;

    #[rstest]
    #[case(LogicOperator::And, "and")]
    #[case(LogicOperator::Or, "or")]
    fn test_logic_operator_format(
        #[case] op: LogicOperator,
        #[case] expected: &str
    ) {
        let result = format!("{}", op);
        assert_eq!(expected, result);
    }
}
