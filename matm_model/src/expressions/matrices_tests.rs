#[cfg(test)]
mod tests {
    use crate::testing::asserts::assert_expression_that;
    use crate::testing::asserts::assert_result_error_that;
    use crate::testing::asserts::assert_result_ok;
    use crate::testing::asserts::assert_matrix_that;
    use crate::core::index::Index;
    use crate::core::index::Index2D;
    use crate::core::index_range::IndexRange;
    use crate::core::natural::RADIX_10;
    use crate::expressions::matrices::MatrixData;
    use crate::factories::ObjFactory;
    use assert_matches::assert_matches;
    use crate::matrix;

    #[test]
    fn test_zeros() {
        let m = MatrixData::zeros(RADIX_10, 2, 3).unwrap();
        assert_matrix_that(&m)
            .has_rows(2)
            .has_columns(3)
            .has_i_i_zero(1, 1)
            .has_i_i_zero(1, 2)
            .has_i_i_zero(2, 1)
            .has_i_i_zero(2, 2);
    }

    #[test]
    fn test_zeros_invalid_row_size() {
        let result = MatrixData::zeros(RADIX_10, 0, 3);
        assert_result_error_that(result)
            .has_message("You need to specify positive size of all dimensions.");
    }

    #[test]
    fn test_zeros_invalid_column_size() {
        let result = MatrixData::zeros(RADIX_10, 2, 0);
        assert_result_error_that(result)
            .has_message("You need to specify positive size of all dimensions.");
    }

    #[test]
    fn test_ones() {
        let m = MatrixData::ones(RADIX_10, 2, 3).unwrap();
        assert_matrix_that(&m)
            .has_rows(2)
            .has_columns(3)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(1);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(1);})
            .is_i_i_integer_that(1, 3, |e| {e.is_int(1);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(1);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(1);})
            .is_i_i_integer_that(2, 3, |e| {e.is_int(1);});
    }

    #[test]
    fn test_ones_invalid_row_size() {
        let result = MatrixData::ones(RADIX_10, 0, 3);
        assert_result_error_that(result)
            .has_message("You need to specify positive size of all dimensions.");
    }

    #[test]
    fn test_ones_invalid_column_size() {
        let result = MatrixData::ones(RADIX_10, 2, 0);
        assert_result_error_that(result)
            .has_message("You need to specify positive size of all dimensions.");
    }

    #[test]
    fn test_identity() {
        let m = MatrixData::identity(RADIX_10, 3).unwrap();
        assert_matrix_that(&m)
            .has_rows(3)
            .has_columns(3);
        assert_eq!(true, m.get_i_i(1, 1).unwrap().is_one());
        assert_eq!(true, m.get_i_i(1, 2).unwrap().is_zero());
        assert_eq!(true, m.get_i_i(1, 3).unwrap().is_zero());
        assert_eq!(true, m.get_i_i(2, 1).unwrap().is_zero());
        assert_eq!(true, m.get_i_i(2, 2).unwrap().is_one());
        assert_eq!(true, m.get_i_i(2, 3).unwrap().is_zero());
        assert_eq!(true, m.get_i_i(3, 1).unwrap().is_zero());
        assert_eq!(true, m.get_i_i(3, 2).unwrap().is_zero());
        assert_eq!(true, m.get_i_i(3, 3).unwrap().is_one());
    }

    #[test]
    fn test_identity_invalid_size() {
        let result = MatrixData::identity(RADIX_10, 0);
        assert_result_error_that(result)
            .has_message("You need to specify positive size of all dimensions.");
    }

    #[test]
    fn test_from_vector() {
        let fa = ObjFactory::new10();
        let m = MatrixData::from_vector(2, 3,
                                        vec![
                fa.rzi_u(1),
                fa.rzi_u(2),
                fa.rzi_u(3),
                fa.rzi_u(4),
                fa.rzi_u(5),
                fa.rzi_u(6)]).unwrap();
        assert_matrix_that(&m)
            .has_rows(2)
            .has_columns(3)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(1);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(2);})
            .is_i_i_integer_that(1, 3, |e| {e.is_int(3);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(4);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(5);})
            .is_i_i_integer_that(2, 3, |e| {e.is_int(6);});
    }

    #[test]
    fn test_from_vector_invalid_row_size() {
        let fa = ObjFactory::new10();
        assert_result_error_that(MatrixData::from_vector(0, 1, vec![fa.rzi_u(1)]))
            .has_message("You need to specify positive size of all dimensions.");
    }

    #[test]
    fn test_from_vector_invalid_column_size() {
        let fa = ObjFactory::new10();
        assert_result_error_that(MatrixData::from_vector(1, 0, vec![fa.rzi_u(1)]))
            .has_message("You need to specify positive size of all dimensions.");
    }

    #[test]
    fn test_from_vector_invalid_element_count() {
        let fa = ObjFactory::new10();
        assert_result_error_that(MatrixData::from_vector(2, 3, vec![fa.rzi_u(1)]))
            .has_message("Invalid number of elements, 1 given, 6 expected.");
    }

    #[test]
    fn test_from_i32_vector() {
        let m = MatrixData::from_ivector(2, 3,
                                         RADIX_10,
                                         vec![
                1, 2, 3,
                4, 5, 6
                ]).unwrap();
        assert_matrix_that(&m)
            .has_rows(2)
            .has_columns(3)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(1);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(2);})
            .is_i_i_integer_that(1, 3, |e| {e.is_int(3);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(4);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(5);})
            .is_i_i_integer_that(2, 3, |e| {e.is_int(6);});
    }

    #[test]
    fn test_scalar() {
        let fa = ObjFactory::new10();
        let m = MatrixData::scalar(fa.rzi_u(3));
        assert_matrix_that(&m)
            .has_rows(1)
            .has_columns(1)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(3);});
    }

    #[test]
    fn test_is_square() {
        assert_eq!(true, MatrixData::zeros(RADIX_10, 2, 2).unwrap().is_square());
    }

    #[test]
    fn test_is_square_1_2() {
        assert_eq!(false, MatrixData::zeros(RADIX_10, 1, 2).unwrap().is_square());
    }

    #[test]
    fn test_is_square_2_1() {
        assert_eq!(false, MatrixData::zeros(RADIX_10, 2, 1).unwrap().is_square());
    }

    #[test]
    fn test_is_zero() {
        assert_eq!(true, MatrixData::zeros(RADIX_10, 2, 2).unwrap().is_zero());
    }

    #[test]
    fn test_is_zero_1() {
        assert_eq!(false, MatrixData::ones(RADIX_10, 2, 2).unwrap().is_zero());
    }

    #[test]
    fn test_is_ones() {
        assert_eq!(true, MatrixData::ones(RADIX_10, 2, 2).unwrap().is_ones());
    }

    #[test]
    fn test_is_ones_0() {
        assert_eq!(false, MatrixData::zeros(RADIX_10, 2, 2).unwrap().is_ones());
    }

    #[test]
    fn test_is_identity() {
        assert_eq!(true, MatrixData::identity(RADIX_10, 2).unwrap().is_identity());
    }

    #[test]
    fn test_is_identity_ones() {
        assert_eq!(false, MatrixData::ones(RADIX_10, 2, 2).unwrap().is_identity());
    }

    #[test]
    fn test_is_upper_triangular_11_11() {
        let fa = ObjFactory::new10();
        assert_eq!(false, matrix!(fa, 2, 2;
            1, 1,
            1, 1).unwrap()
            .is_upper_triangular());
    }

    #[test]
    fn test_is_upper_triangular_11_01() {
        let fa = ObjFactory::new10();
        assert_eq!(true, matrix!(fa, 2, 2;
            1, 1,
            0, 1).unwrap()
            .is_upper_triangular());
    }

    #[test]
    fn test_is_upper_triangular_01_01() {
        let fa = ObjFactory::new10();
        assert_eq!(true, matrix!(fa, 2, 2;
            0, 1,
            0, 1).unwrap()
            .is_upper_triangular());
    }

    #[test]
    fn test_is_upper_triangular_01_00() {
        let fa = ObjFactory::new10();
        assert_eq!(true, matrix!(fa, 2, 2;
            0, 1,
            0, 0).unwrap()
            .is_upper_triangular());
    }

    #[test]
    fn test_is_strictly_upper_triangular_11_11() {
        let fa = ObjFactory::new10();
        assert_eq!(false, matrix!(fa, 2, 2;
            1, 1,
            1, 1).unwrap()
            .is_strictly_upper_triangular());
    }

    #[test]
    fn test_is_strictly_upper_triangular_11_01() {
        let fa = ObjFactory::new10();
        assert_eq!(false, matrix!(fa, 2, 2;
            1, 1,
            0, 1).unwrap()
            .is_strictly_upper_triangular());
    }

    #[test]
    fn test_is_strictly_upper_triangular_01_01() {
        let fa = ObjFactory::new10();
        assert_eq!(false, matrix!(fa, 2, 2;
            0, 1,
            0, 1).unwrap()
            .is_strictly_upper_triangular());
    }

    #[test]
    fn test_is_strictly_upper_triangular_01_00() {
        let fa = ObjFactory::new10();
        assert_eq!(true, matrix!(fa, 2, 2;
            0, 1,
            0, 0).unwrap()
            .is_strictly_upper_triangular());
    }

    #[test]
    fn test_is_lower_triangular_11_11() {
        let fa = ObjFactory::new10();
        assert_eq!(false, matrix!(fa, 2, 2;
            1, 1,
            1, 1).unwrap()
            .is_lower_triangular());
    }

    #[test]
    fn test_is_lower_triangular_10_11() {
        let fa = ObjFactory::new10();
        assert_eq!(true, matrix!(fa, 2, 2;
            1, 0,
            1, 1).unwrap()
            .is_lower_triangular());
    }

    #[test]
    fn test_is_lower_triangular_10_10() {
        let fa = ObjFactory::new10();
        assert_eq!(true, matrix!(fa, 2, 2;
            1, 0,
            1, 0).unwrap()
            .is_lower_triangular());
    }

    #[test]
    fn test_is_lower_triangular_00_10() {
        let fa = ObjFactory::new10();
        assert_eq!(true, matrix!(fa, 2, 2;
            0, 0,
            1, 0).unwrap()
            .is_lower_triangular());
    }

    #[test]
    fn test_is_symmetrix_01() {
        let fa = ObjFactory::new10();
        assert_eq!(false, matrix!(fa, 1, 2; 0, 1).unwrap()
            .is_symmetric());
    }

    #[test]
    fn test_is_symmetrix_01_12() {
        let fa = ObjFactory::new10();
        assert_eq!(true, matrix!(fa, 2, 2;
                0, 1,
                1, 2
            ).unwrap()
            .is_symmetric());
    }

    #[test]
    fn test_is_symmetrix_01_22() {
        let fa = ObjFactory::new10();
        assert_eq!(false, matrix!(fa, 2, 2;
                0, 1,
                2, 2
            ).unwrap()
            .is_symmetric());
    }

    #[test]
    fn test_is_symmetrix_015_103_532() {
        let fa = ObjFactory::new10();
        assert_eq!(true, matrix!(fa, 3, 3;
                0, 1, 5,
                1, 0, 3,
                5, 3, 2
            ).unwrap()
            .is_symmetric());
    }

    #[test]
    fn test_is_symmetrix_015_103_432() {
        let fa = ObjFactory::new10();
        assert_eq!(false, matrix!(fa, 3, 3;
                0, 1, 5,
                1, 0, 3,
                4, 3, 2
            ).unwrap()
            .is_symmetric());
    }

    #[test]
    fn test_augmented() {
        let m1 = MatrixData::zeros(RADIX_10, 2, 2).unwrap();
        let m2 = MatrixData::ones(RADIX_10, 2, 2).unwrap();
        let m = assert_result_ok(
            MatrixData::augmented(&m1, &m2));
        assert_matrix_that(&m)
            .has_rows(2)
            .has_columns(4);
        assert_eq!(1, m.augmented_columns.len());
        assert_matches!(m.augmented_columns.get(0), Some(2));

        assert_eq!(true, m.get_i_i(1, 1).unwrap().is_zero());
        assert_eq!(true, m.get_i_i(1, 2).unwrap().is_zero());
        assert_eq!(true, m.get_i_i(2, 1).unwrap().is_zero());
        assert_eq!(true, m.get_i_i(2, 2).unwrap().is_zero());

        assert_eq!(true, m.get_i_i(1, 3).unwrap().is_one());
        assert_eq!(true, m.get_i_i(1, 4).unwrap().is_one());
        assert_eq!(true, m.get_i_i(2, 3).unwrap().is_one());
        assert_eq!(true, m.get_i_i(2, 4).unwrap().is_one());
    }

    #[test]
    fn test_augmented_invalid_row_size() {
        let m1 = MatrixData::zeros(RADIX_10, 1, 2).unwrap();
        let m2 = MatrixData::ones(RADIX_10, 2, 2).unwrap();
        assert_result_error_that(MatrixData::augmented(&m1, &m2))
            .has_message("Matrices of horizontaly augmented matrix have to have the same number of rows.");
    }

    #[test]
    fn test_rows() {
        let m = MatrixData::zeros(RADIX_10, 2, 3).unwrap();
        assert_eq!(2, m.rows());
    }

    #[test]
    fn test_columns() {
        let m = MatrixData::zeros(RADIX_10, 2, 3).unwrap();
        assert_eq!(3, m.columns());
    }

    #[test]
    fn test_size() {
        let m = MatrixData::zeros(RADIX_10, 2, 3).unwrap();
        let result = m.size();
        assert_eq!(2, result.rows);
        assert_eq!(3, result.columns);
    }

    #[test]
    fn test_get_bounded() {
        let m = MatrixData::zeros(RADIX_10, 1, 2).unwrap();
        assert_eq!(true, m.get_i_i(1, 1).unwrap().is_zero());
        assert_eq!(true, m.get_i_i(1, 2).unwrap().is_zero());
    }

    #[test]
    fn test_get_unbounded_row_too_small() {
        let m = MatrixData::zeros(RADIX_10, 1, 2).unwrap();
        assert_result_error_that(m.get_i_i(0, 1))
            .has_message("Invalid element index (0, 1).");
    }

    #[test]
    fn test_get_unbounded_row_too_big() {
        let m = MatrixData::zeros(RADIX_10, 1, 2).unwrap();
        assert_result_error_that(m.get_i_i(2, 1))
            .has_message("Invalid element index (2, 1).");
    }

    #[test]
    fn test_get_unbounded_column_too_small() {
        let m = MatrixData::zeros(RADIX_10, 1, 2).unwrap();
        assert_result_error_that(m.get_i_i(1, 0))
            .has_message("Invalid element index (1, 0).");
    }

    #[test]
    fn test_get_unbounded_column_too_big() {
        let m = MatrixData::zeros(RADIX_10, 1, 2).unwrap();
        assert_result_error_that(m.get_i_i(1, 3))
            .has_message("Invalid element index (1, 3).");
    }

    #[test]
    fn test_set_bounded() {
        let fa = ObjFactory::new10();
        let mut m = MatrixData::zeros(RADIX_10, 1, 2).unwrap();
        m.set_i_i(1, 1, fa.rzi_u(1)).unwrap();
        assert_eq!(true, m.get_i_i(1, 1).unwrap().is_one());
        assert_eq!(true, m.get_i_i(1, 2).unwrap().is_zero());
    }

    #[test]
    fn test_set_unbounded_row_too_small() {
        let fa = ObjFactory::new10();
        let mut m = MatrixData::zeros(RADIX_10, 1, 2).unwrap();
        assert_result_error_that(m.set_i_i(0, 1, fa.rzi_u(1)))
            .has_message("Invalid element index (0, 1).");
    }

    #[test]
    fn test_set_unbounded_row_too_big() {
        let fa = ObjFactory::new10();
        let mut m = MatrixData::zeros(RADIX_10, 1, 2).unwrap();
        assert_result_error_that(m.set_i_i(2, 1, fa.rzi_u(1)))
            .has_message("Invalid element index (2, 1).");
    }

    #[test]
    fn test_set_unbounded_column_too_small() {
        let fa = ObjFactory::new10();
        let mut m = MatrixData::zeros(RADIX_10, 1, 2).unwrap();
        assert_result_error_that(m.set_i_i(1, 0, fa.rzi_u(1)))
            .has_message("Invalid element index (1, 0).");
    }

    #[test]
    fn test_set_unbounded_column_too_big() {
        let fa = ObjFactory::new10();
        let mut m = MatrixData::zeros(RADIX_10, 1, 2).unwrap();
        assert_result_error_that(m.set_i_i(1, 3, fa.rzi_u(1)))
            .has_message("Invalid element index (1, 3).");
    }

    #[test]
    fn test_clone() {
        let fa = ObjFactory::new10();
        let mut m1 = MatrixData::zeros(RADIX_10, 2, 2).unwrap();
        m1.set_i_i(1, 1, fa.rzi_u(1)).unwrap();
        m1.set_i_i(1, 2, fa.rzi_u(2)).unwrap();
        m1.set_i_i(2, 1, fa.rzi_u(3)).unwrap();
        m1.set_i_i(2, 2, fa.rzi_u(4)).unwrap();

        let mut m2 = m1.clone();
        
        assert_matrix_that(&m1)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(1);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(2);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(3);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(4);});
        assert_matrix_that(&m2)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(1);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(2);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(3);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(4);});

        m2.set_i_i(1, 1, fa.rzi_u(5)).unwrap();
        m2.set_i_i(1, 2, fa.rzi_u(6)).unwrap();
        m2.set_i_i(2, 1, fa.rzi_u(7)).unwrap();
        m2.set_i_i(2, 2, fa.rzi_u(8)).unwrap();

        assert_matrix_that(&m1)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(1);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(2);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(3);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(4);});
        assert_matrix_that(&m2)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(5);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(6);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(7);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(8);});
    }

    #[test]
    fn test_transpose() {
        let fa = ObjFactory::new10();
        let mut m1 = MatrixData::zeros(RADIX_10, 2, 3).unwrap();
        m1.set_i_i(1, 1, fa.rzi_u(1)).unwrap();
        m1.set_i_i(1, 2, fa.rzi_u(2)).unwrap();
        m1.set_i_i(1, 3, fa.rzi_u(3)).unwrap();
        m1.set_i_i(2, 1, fa.rzi_u(4)).unwrap();
        m1.set_i_i(2, 2, fa.rzi_u(5)).unwrap();
        m1.set_i_i(2, 3, fa.rzi_u(6)).unwrap();

        let m2 = m1.transpose();
        assert_matrix_that(&m2)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(1);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(4);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(2);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(5);})
            .is_i_i_integer_that(3, 1, |e| {e.is_int(3);})
            .is_i_i_integer_that(3, 2, |e| {e.is_int(6);});
    }

    #[test]
    fn test_get_submatrix_12_12() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 2, 3;
                11, 12, 13,
                21, 22, 23).unwrap();

        let m2 = assert_result_ok(
            m1.get_submatrix(
            &IndexRange::range(
                Index::abs(1).unwrap(),
                Index::abs(2).unwrap()).unwrap(),
            &IndexRange::range(
                Index::abs(1).unwrap(),
                Index::abs(2).unwrap()).unwrap()));
        assert_matrix_that(&m2)
            .has_rows(2)
            .has_columns(2)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(11);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(12);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(21);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(22);});
    }

    #[test]
    fn test_get_submatrix_12_23() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 2, 3;
            11, 12, 13,
            21, 22, 23).unwrap();

        let m2 = assert_result_ok(
            m1.get_submatrix(
        &IndexRange::range(
            Index::abs(1).unwrap(),
            Index::abs(2).unwrap()).unwrap(),
        &IndexRange::range(
            Index::abs(2).unwrap(),
            Index::abs(3).unwrap()).unwrap()));
        assert_matrix_that(&m2)
            .has_rows(2)
            .has_columns(2)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(12);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(13);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(22);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(23);});
    }

    #[test]
    fn test_get_submatrix_12_13() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 2, 3;
            11, 12, 13,
            21, 22, 23).unwrap();

        let m2 = assert_result_ok(
            m1.get_submatrix(
        &IndexRange::range(
            Index::abs(1).unwrap(),
            Index::abs(2).unwrap()).unwrap(),
        &IndexRange::range(
            Index::abs(1).unwrap(),
            Index::abs(3).unwrap()).unwrap()));
        assert_matrix_that(&m2)
            .has_rows(2)
            .has_columns(3)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(11);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(12);})
            .is_i_i_integer_that(1, 3, |e| {e.is_int(13);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(21);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(22);})
            .is_i_i_integer_that(2, 3, |e| {e.is_int(23);});
    }

    #[test]
    fn test_get_submatrix_12_22() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 2, 3;
            11, 12, 13,
            21, 22, 23).unwrap();

        let m2 = assert_result_ok(
            m1.get_submatrix(
        &IndexRange::range(
            Index::abs(1).unwrap(),
            Index::abs(2).unwrap()).unwrap(),
        &IndexRange::range(
            Index::abs(2).unwrap(),
            Index::abs(2).unwrap()).unwrap()));
        assert_matrix_that(&m2)
            .has_rows(2)
            .has_columns(1)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(12);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(22);});
    }

    #[test]
    fn test_get_submatrix_11_12() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 2, 3;
            11, 12, 13,
            21, 22, 23).unwrap();

        let m2 = assert_result_ok(
            m1.get_submatrix(
        &IndexRange::range(
            Index::abs(1).unwrap(),
            Index::abs(1).unwrap()).unwrap(),
        &IndexRange::range(
            Index::abs(1).unwrap(),
            Index::abs(2).unwrap()).unwrap()));
        assert_matrix_that(&m2)
            .has_rows(1)
            .has_columns(2)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(11);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(12);});
    }

    #[test]
    fn test_get_submatrix_22_12() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 2, 3;
            11, 12, 13,
            21, 22, 23).unwrap();

        let m2 = assert_result_ok(
            m1.get_submatrix(
        &IndexRange::range(
            Index::abs(2).unwrap(),
            Index::abs(2).unwrap()).unwrap(),
        &IndexRange::range(
            Index::abs(1).unwrap(),
            Index::abs(2).unwrap()).unwrap()));
        assert_matrix_that(&m2)
            .has_rows(1)
            .has_columns(2)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(21);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(22);});
    }

    #[test]
    fn test_get_submatrix_13_11() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 2, 2;
            11, 12,
            21, 22).unwrap();

        assert_result_error_that(
            m1.get_submatrix(
            &IndexRange::range(
                Index::abs(1).unwrap(),
                Index::abs(3).unwrap()).unwrap(),
            &IndexRange::range(
                Index::abs(1).unwrap(),
                Index::abs(1).unwrap()).unwrap()))
            .has_message("Invalid row index (1:3)");
    }

    #[test]
    fn test_get_submatrix_11_13() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 2, 2;
            11, 12,
            21, 22).unwrap();

        assert_result_error_that(
            m1.get_submatrix(
            &IndexRange::range(
                Index::abs(1).unwrap(),
                Index::abs(1).unwrap()).unwrap(),
            &IndexRange::range(
                Index::abs(1).unwrap(),
                Index::abs(3).unwrap()).unwrap()))
            .has_message("Invalid column index (1:3)");
    }

    #[test]
    fn test_get_submatrix_1end_2end1() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 3, 3;
            11, 12, 13,
            21, 22, 23,
            31, 32, 33
        ).unwrap();

        let result = m1.get_submatrix(
            &IndexRange::range(
                Index::abs(1).unwrap(),
                Index::end()).unwrap(),
            &IndexRange::range(
                Index::abs(2).unwrap(),
                Index::end_minus(1)).unwrap()
        );

        let m2 = assert_result_ok(result);
        assert_matrix_that(&m2)
            .has_rows(3)
            .has_columns(1)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(12);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(22);})
            .is_i_i_integer_that(3, 1, |e| {e.is_int(32);})
            ;
    }
    
    #[test]
    fn test_get_submatrix_with_crossed_lines() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 3, 3;
            11, 12, 13,
            21, 22, 23,
            31, 32, 33
        ).unwrap();
        let index = Index2D::new(2, 3).unwrap();

        let result = m1.get_submatrix_with_crossed_lines(&index);

        let m2 = assert_result_ok(result);
        assert_matrix_that(&m2)
            .has_rows(3)
            .has_columns(3)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(11);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(12);})
            .is_i_i_boxsign(1, 3)
            .is_i_i_boxsign(2, 1)
            .is_i_i_boxsign(2, 2)
            .is_i_i_boxsign(2, 3)
            .is_i_i_integer_that(3, 1, |e| {e.is_int(31);})
            .is_i_i_integer_that(3, 2, |e| {e.is_int(32);})
            .is_i_i_boxsign(3, 3)
            ;
    }

    #[test]
    fn test_get_submatrix_with_crossed_lines_invalid_row() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 3, 3;
            11, 12, 13,
            21, 22, 23,
            31, 32, 33
        ).unwrap();
        let index = Index2D::new(4, 3).unwrap();

        let result = m1.get_submatrix_with_crossed_lines(&index);

        assert_result_error_that(result)
            .has_message("Invalid row index [4]");
    }

    #[test]
    fn test_get_submatrix_with_crossed_lines_invalid_column() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 3, 3;
            11, 12, 13,
            21, 22, 23,
            31, 32, 33
        ).unwrap();
        let index = Index2D::new(2, 5).unwrap();

        let result = m1.get_submatrix_with_crossed_lines(&index);

        assert_result_error_that(result)
            .has_message("Invalid column index [5]");
    }

    #[test]
    fn test_get_submatrix_by_removing() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 3, 3;
            11, 12, 13,
            21, 22, 23,
            31, 32, 33
        ).unwrap();
        let index = Index2D::new(2, 3).unwrap();

        let result = m1.get_submatrix_by_removing(&index);

        let m2 = assert_result_ok(result);
        assert_matrix_that(&m2)
            .has_rows(2)
            .has_columns(2)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(11);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(12);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(31);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(32);})
            ;
    }

    #[test]
    fn test_get_submatrix_by_removing_invalid_row() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 3, 3;
            11, 12, 13,
            21, 22, 23,
            31, 32, 33
        ).unwrap();
        let index = Index2D::new(4, 3).unwrap();

        let result = m1.get_submatrix_by_removing(&index);

        assert_result_error_that(result)
            .has_message("Invalid row index [4]");
    }

    #[test]
    fn test_get_submatrix_by_removing_invalid_column() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 3, 3;
            11, 12, 13,
            21, 22, 23,
            31, 32, 33
        ).unwrap();
        let index = Index2D::new(2, 5).unwrap();

        let result = m1.get_submatrix_by_removing(&index);

        assert_result_error_that(result)
            .has_message("Invalid column index [5]");
    }

    #[test]
    fn test_swap_rows_1_2() {
        let fa = ObjFactory::new10();
        let mut m1 = matrix!(fa, 3, 2;
            11, 12,
            21, 22,
            31, 32).unwrap();

        let result = assert_result_ok(
            m1.swap_rows(1, 2));
        assert_eq!(true, result);
        assert_matrix_that(&m1)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(21);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(22);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(11);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(12);})
            .is_i_i_integer_that(3, 1, |e| {e.is_int(31);})
            .is_i_i_integer_that(3, 2, |e| {e.is_int(32);});
    }

    #[test]
    fn test_swap_rows_3_2() {
        let fa = ObjFactory::new10();
        let mut m1 = matrix!(fa, 3, 2;
            11, 12,
            21, 22,
            31, 32).unwrap();

        let result = assert_result_ok(
            m1.swap_rows(3, 2));
            assert_eq!(true, result);
        assert_matrix_that(&m1)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(11);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(12);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(31);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(32);})
            .is_i_i_integer_that(3, 1, |e| {e.is_int(21);})
            .is_i_i_integer_that(3, 2, |e| {e.is_int(22);});
    }

    #[test]
    fn test_swap_rows_2_2() {
        let fa = ObjFactory::new10();
        let mut m1 = matrix!(fa, 3, 2;
            11, 12,
            21, 22,
            31, 32).unwrap();

        let result = assert_result_ok(
            m1.swap_rows(2, 2));
        assert_eq!(false, result);
        assert_matrix_that(&m1)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(11);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(12);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(21);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(22);})
            .is_i_i_integer_that(3, 1, |e| {e.is_int(31);})
            .is_i_i_integer_that(3, 2, |e| {e.is_int(32);});
    }

    #[test]
    fn test_swap_rows_1_4() {
        let fa = ObjFactory::new10();
        let mut m1 = matrix!(fa, 3, 2;
            11, 12,
            21, 22,
            31, 32).unwrap();

        assert_result_error_that(m1.swap_rows(1, 4))
            .has_message("Invalid row index [4]");
    }

    #[test]
    fn test_swap_rows_5_1() {
        let fa = ObjFactory::new10();
        let mut m1 = matrix!(fa, 3, 2;
            11, 12,
            21, 22,
            31, 32).unwrap();

        assert_result_error_that(m1.swap_rows(5, 1))
            .has_message("Invalid row index [5]");
    }

    #[test]
    fn test_count_zeros_in_row() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 3, 3;
            11, 12, 0,
            0, 22, 0,
            31, 0, 0
        ).unwrap();

        assert_eq!(1, m1.count_zeros_in_row(1).unwrap());
        assert_eq!(2, m1.count_zeros_in_row(2).unwrap());
        assert_eq!(2, m1.count_zeros_in_row(3).unwrap());
    }

    #[test]
    fn test_count_zeros_in_row_invalid() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 3, 3;
            11, 12, 0,
            0, 22, 0,
            31, 0, 0
        ).unwrap();

        assert_result_error_that(m1.count_zeros_in_row(5))
            .has_message("Invalid element index (5, 1).");
    }

    #[test]
    fn test_count_zeros_in_column() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 3, 3;
            11, 12, 0,
            0, 22, 0,
            31, 0, 0
        ).unwrap();

        assert_eq!(1, m1.count_zeros_in_column(1).unwrap());
        assert_eq!(1, m1.count_zeros_in_column(2).unwrap());
        assert_eq!(3, m1.count_zeros_in_column(3).unwrap());
    }

    #[test]
    fn test_count_zeros_in_column_invalid() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 3, 3;
            11, 12, 0,
            0, 22, 0,
            31, 0, 0
        ).unwrap();

        assert_result_error_that(m1.count_zeros_in_column(5))
            .has_message("Invalid element index (1, 5).");
    }

    #[test]
    fn test_find_row_with_max_zeros_1() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 3, 3;
            0, 12, 0,
            0, 22, 23,
            31, 32, 33
        ).unwrap();

        match m1.find_row_with_max_zeros() {
            Some(row) => {
                assert_eq!(true, row.index == 1);
                assert_eq!(2, row.count);
            }
            None => {
                assert_eq!(true, false);
            }
        }
    }

    #[test]
    fn test_find_row_with_max_zeros_2_3() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 3, 3;
            11, 12, 0,
            0, 22, 0,
            31, 0, 0
        ).unwrap();

        match m1.find_row_with_max_zeros() {
            Some(row) => {
                assert_eq!(true, row.index == 2 || row.index == 3);
                assert_eq!(2, row.count);
            }
            None => {
                assert_eq!(true, false);
            }
        }
    }

    #[test]
    fn test_find_row_with_max_zeros_none() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 3, 3;
            11, 12, 13,
            21, 22, 23,
            31, 32, 33
        ).unwrap();

        match m1.find_row_with_max_zeros() {
            None => {}
            Some(_) => {
                assert_eq!(true, false);
            }
        }
    }

    #[test]
    fn test_find_column_with_max_zeros_1() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 3, 3;
            0, 12, 0,
            0, 22, 23,
            31, 32, 33
        ).unwrap();

        match m1.find_column_with_max_zeros() {
            Some(col) => {
                assert_eq!(true, col.index == 1);
                assert_eq!(2, col.count);
            }
            None => {
                assert_eq!(true, false);
            }
        }
    }

    #[test]
    fn test_find_column_with_max_zeros_2_3() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 3, 3;
            11, 0, 13,
            0, 22, 0,
            31, 0, 0
        ).unwrap();

        match m1.find_column_with_max_zeros() {
            Some(col) => {
                assert_eq!(true, col.index == 2 || col.index == 3);
                assert_eq!(2, col.count);
            }
            None => {
                assert_eq!(true, false);
            }
        }
    }

    #[test]
    fn test_find_column_with_max_zeros_none() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 3, 3;
            11, 12, 13,
            21, 22, 23,
            31, 32, 33
        ).unwrap();

        match m1.find_column_with_max_zeros() {
            None => {}
            Some(_) => {
                assert_eq!(true, false);
            }
        }
    }

    #[test]
    fn test_is_zero_row() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 3, 3;
            11, 0, 13,
            0, 22, 0,
            0, 0, 0
        ).unwrap();

        assert_eq!(false, m1.is_zero_row(1).unwrap());
        assert_eq!(false, m1.is_zero_row(2).unwrap());
        assert_eq!(true, m1.is_zero_row(3).unwrap());
    }

    #[test]
    fn test_is_zero_row_invalid_index() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 3, 3;
            11, 0, 13,
            0, 22, 0,
            0, 0, 0
        ).unwrap();

        let result = m1.is_zero_row(4);

        assert_result_error_that(result)
            .has_message("Invalid row index [4]");
    }

    #[test]
    fn test_count_non_zero_rows() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 3, 3;
            11, 0, 13,
            0, 22, 0,
            0, 0, 0
        ).unwrap();

        assert_eq!(2, m1.count_non_zero_rows());
    }

    #[test]
    fn test_is_zero_column() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 3, 3;
            11, 0, 0,
            0, 22, 0,
            31, 0, 0
        ).unwrap();

        assert_eq!(false, m1.is_zero_column(1).unwrap());
        assert_eq!(false, m1.is_zero_column(2).unwrap());
        assert_eq!(true, m1.is_zero_column(3).unwrap());
    }

    #[test]
    fn test_is_zero_column_invalid_index() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 3, 3;
            11, 0, 0,
            0, 22, 0,
            31, 0, 0
        ).unwrap();

        let result = m1.is_zero_column(4);

        assert_result_error_that(result)
            .has_message("Invalid column index [4]");
    }

    #[test]
    fn test_count_non_zero_columns() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 3, 3;
            11, 0, 0,
            0, 22, 0,
            31, 0, 0
        ).unwrap();

        assert_eq!(2, m1.count_non_zero_columns());
    }

    #[test]
    fn test_format_1_1() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 1, 1; 11).unwrap();

        assert_eq!("[11]", format!("{}", m1));
    }

    #[test]
    fn test_format_1_2() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 1, 2; 11, 12).unwrap();

        assert_eq!("[11, 12]", format!("{}", m1));
    }

    #[test]
    fn test_format_2_2() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 2, 2;
            11, 12,
            21, 22
        ).unwrap();
        
        assert_eq!("[11, 12; 21, 22]", format!("{}", m1));
    }

    #[test]
    fn test_format_3_2() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 3, 2;
            11, 12,
            21, 22,
            31, 32
        ).unwrap();
        
        assert_eq!("[11, 12; 21, 22; 31, 32]", format!("{}", m1));
    }

    #[test]
    fn test_partial_eq() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 2, 2;
            1, 2,
            3, 4
        ).unwrap();
        let m2 = matrix!(fa, 2, 2;
            1, 2,
            3, 4
        ).unwrap();

        assert_eq!(true, m1 == m2);
    }

    #[test]
    fn test_partial_neq() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 2, 2;
            1, 2,
            3, 4
        ).unwrap();
        let m2 = matrix!(fa, 2, 2;
            1, 2,
            3, 10
        ).unwrap();

        assert_eq!(false, m1 == m2);
    }

    #[test]
    fn test_partial_eq_the_same_diff_type() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 2, 2;
            1, 2,
            3, 4
        ).unwrap();
        let m2 = matrix!(fa, 2, 2;
            1, 2,
            3, 4
        ).unwrap();

        assert_eq!(true, m1 == m2);
    }

    #[test]
    fn test_row_ref() {
        let fa = ObjFactory::new10();
        let mut m1 = matrix!(fa, 2, 2;
            1, 2,
            3, 4
        ).unwrap();

        {
            let r1 = m1.get_row_ref(1);
            assert_eq!(2, r1.len());
            assert_expression_that(r1.get(1).unwrap())
                .as_integer()
                .is_int(1);
            assert_expression_that(r1.get(2).unwrap())
                .as_integer()
                .is_int(2);
        }

        m1.set_i_i(1, 1, fa.rzi_u(10)).unwrap();

        {
            let r1 = m1.get_row_ref(1);
            assert_expression_that(r1.get(1).unwrap())
                .as_integer()
                .is_int(10);
            assert_expression_that(r1.get(2).unwrap())
                .as_integer()
                .is_int(2);
        }
    }

    #[test]
    fn test_row_ref_mut() {
        let fa = ObjFactory::new10();
        let mut m1 = matrix!(fa, 2, 2;
            1, 2,
            3, 4
        ).unwrap();

        {
            let r1 = m1.get_row_ref_mut(1);
            assert_eq!(2, r1.len());
            assert_expression_that(r1.get(1).unwrap())
                .as_integer()
                .is_int(1);
            assert_expression_that(r1.get(2).unwrap())
                .as_integer()
                .is_int(2);
        }

        {
            let mut r1 = m1.get_row_ref_mut(1);
            r1.set(1, fa.rzi_u(10)).unwrap();
        }

        {
            let r1 = m1.get_row_ref(1);
            assert_expression_that(r1.get(1).unwrap())
                .as_integer()
                .is_int(10);
            assert_expression_that(r1.get(2).unwrap())
                .as_integer()
                .is_int(2);
        }
    }

    #[test]
    fn test_get_row() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 2, 2;
            1, 2,
            3, 4
        ).unwrap();

        let r1 = m1.get_row(Index::abs(1).unwrap()).unwrap();
        assert_matrix_that(&r1)
            .has_rows(1)
            .has_columns(2)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(1);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(2);});
        
        let r2 = m1.get_row(Index::abs(2).unwrap()).unwrap();
        assert_matrix_that(&r2)
            .has_rows(1)
            .has_columns(2)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(3);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(4);});
    }

    #[test]
    fn test_get_row_unbounded_to_big() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 2, 2;
            1, 2,
            3, 4
        ).unwrap();

        assert_result_error_that(m1.get_row(Index::abs(3).unwrap()))
            .has_message("Invalid row index (3:3)");
    }

    #[test]
    fn test_get_column() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 2, 2;
            1, 2,
            3, 4
        ).unwrap();

        let c1 = m1.get_column(Index::abs(1).unwrap()).unwrap();
        assert_matrix_that(&c1)
            .has_rows(2)
            .has_columns(1)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(1);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(3);});
        
        let c2 = m1.get_column(Index::abs(2).unwrap()).unwrap();
        assert_matrix_that(&c2)
            .has_rows(2)
            .has_columns(1)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(2);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(4);});
    }

    #[test]
    fn test_get_column_unbounded_to_big() {
        let fa = ObjFactory::new10();
        let m1 = matrix!(fa, 2, 2;
            1, 2,
            3, 4
        ).unwrap();

        assert_result_error_that(m1.get_column(Index::abs(3).unwrap()))
            .has_message("Invalid column index (3:3)");
    }

    #[test]
    fn test_set_matrix_1_1() {
        let fa = ObjFactory::new10();
        let mut m1 = matrix!(fa, 2, 3;
            1, 2, 3,
            4, 5, 6
        ).unwrap();
        let m2 = matrix!(fa, 2, 2;
            10, 20,
            30, 40
        ).unwrap();

        m1.set_matrix(Index2D::new(1, 1).unwrap(), &m2).unwrap();

        assert_eq!(true, matrix!(fa, 2, 3;
            10, 20, 3,
            30, 40, 6
        ).unwrap() == m1);
    }

    #[test]
    fn test_set_matrix_1_2() {
        let fa = ObjFactory::new10();
        let mut m1 = matrix!(fa, 2, 3;
            1, 2, 3,
            4, 5, 6
        ).unwrap();
        let m2 = matrix!(fa, 2, 2;
            10, 20,
            30, 40
        ).unwrap();

        m1.set_matrix(Index2D::new(1, 2).unwrap(), &m2).unwrap();

        assert_eq!(true, matrix!(fa, 2, 3;
            1, 10, 20,
            4, 30, 40
        ).unwrap() == m1);
    }

    #[test]
    fn test_set_matrix_outside() {
        let fa = ObjFactory::new10();
        let mut m1 = matrix!(fa, 2, 3;
            1, 2, 3,
            4, 5, 6
        ).unwrap();
        let m2 = matrix!(fa, 2, 2;
            10, 20,
            30, 40
        ).unwrap();

        assert_result_error_that(m1.set_matrix(Index2D::new(1, 3).unwrap(), &m2))
            .has_message("Invalid element index (1, 4).");
    }

    #[test]
    fn test_right_down_element_iterator() {
        let fa = ObjFactory::new10();
        let m = matrix!(fa, 2, 3;
            1, 1, 1,
            1, 1, 1
        ).unwrap();
        let mut it = m.iter_right_down();
        assert_eq!(true, it.next() == Some(Index2D::new(1, 1).unwrap()));
        assert_eq!(true, it.next() == Some(Index2D::new(1, 2).unwrap()));
        assert_eq!(true, it.next() == Some(Index2D::new(1, 3).unwrap()));
        assert_eq!(true, it.next() == Some(Index2D::new(2, 1).unwrap()));
        assert_eq!(true, it.next() == Some(Index2D::new(2, 2).unwrap()));
        assert_eq!(true, it.next() == Some(Index2D::new(2, 3).unwrap()));
        assert_eq!(true, it.next() == None);
    }

    #[test]
    fn test_down_right_element_iterator() {
        let fa = ObjFactory::new10();
        let m = matrix!(fa, 2, 3;
            1, 1, 1,
            1, 1, 1
        ).unwrap();
        let mut it = m.iter_down_right();
        assert_eq!(true, it.next() == Some(Index2D::new(1, 1).unwrap()));
        assert_eq!(true, it.next() == Some(Index2D::new(2, 1).unwrap()));
        assert_eq!(true, it.next() == Some(Index2D::new(1, 2).unwrap()));
        assert_eq!(true, it.next() == Some(Index2D::new(2, 2).unwrap()));
        assert_eq!(true, it.next() == Some(Index2D::new(1, 3).unwrap()));
        assert_eq!(true, it.next() == Some(Index2D::new(2, 3).unwrap()));
        assert_eq!(true, it.next() == None);
    }

    #[test]
    fn test_down_right_lower_tri_element_iterator() {
        let fa = ObjFactory::new10();
        let m = matrix!(fa, 3, 3;
            1, 1, 1,
            1, 1, 1,
            1, 1, 1
        ).unwrap();
        let mut it = m.iter_down_right_lower_tri();
        assert_eq!(true, it.next() == Some(Index2D::new(1, 1).unwrap()));
        assert_eq!(true, it.next() == Some(Index2D::new(2, 1).unwrap()));
        assert_eq!(true, it.next() == Some(Index2D::new(3, 1).unwrap()));
        assert_eq!(true, it.next() == Some(Index2D::new(2, 2).unwrap()));
        assert_eq!(true, it.next() == Some(Index2D::new(3, 2).unwrap()));
        assert_eq!(true, it.next() == Some(Index2D::new(3, 3).unwrap()));
        assert_eq!(true, it.next() == None);
    }

}
