use std::cmp::Ordering;
use std::cmp::PartialEq;
use std::cmp::PartialOrd;
use std::ops::Neg;


#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Sign {
    Zero,
    Positive,
    Negative,
}

impl Sign {
    pub fn mul(&self, other: Self) -> Self {
        return match self {
            Self::Positive => match other {
                Self::Positive => Self::Positive,
                Self::Zero => Self::Zero,
                Self::Negative => Self::Negative,
            }
            Self::Zero => Self::Zero,
            Self::Negative => match other {
                Self::Positive => Self::Negative,
                Self::Zero => Self::Zero,
                Self::Negative => Self::Positive,
            }
        }
    }
}

impl Neg for Sign {
    type Output = Self;

    fn neg(self) -> Self::Output {
        match self {
            Self::Zero => Self::Zero,
            Self::Negative => Self::Positive,
            Self::Positive => Self::Negative,
        }
    }
}

impl PartialOrd for Sign {
    /// compares sign. If both signs are the same then None is returned.
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        match self {
            Self::Positive => match other {
                Self::Positive => {
                    // positive comparison
                    return None;
                }
                Self::Zero | Self::Negative => {
                    return Some(Ordering::Greater);
                }
            }
            Self::Zero => match other {
                Self::Positive => {
                    return Some(Ordering::Less);
                }
                Self::Zero => {
                    return Some(Ordering::Equal);
                }
                Self::Negative => {
                    return Some(Ordering::Greater);
                }
            }
            Self::Negative => match other {
                Self::Positive | Self::Zero => {
                    return Some(Ordering::Less);
                }
                Self::Negative => {
                    // negative comparison
                    return None;
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use rstest::rstest;
    use assert_matches::assert_matches;
    use std::cmp::Ordering;
    use crate::expressions::sign::Sign;

    #[rstest]
    #[case(Sign::Positive, Sign::Positive, Sign::Positive)]
    #[case(Sign::Positive, Sign::Zero, Sign::Zero)]
    #[case(Sign::Positive, Sign::Negative, Sign::Negative)]
    #[case(Sign::Zero, Sign::Positive, Sign::Zero)]
    #[case(Sign::Zero, Sign::Zero, Sign::Zero)]
    #[case(Sign::Zero, Sign::Negative, Sign::Zero)]
    #[case(Sign::Negative, Sign::Positive, Sign::Negative)]
    #[case(Sign::Negative, Sign::Zero, Sign::Zero)]
    #[case(Sign::Negative, Sign::Negative, Sign::Positive)]
    fn test_sign_mul(#[case] left: Sign, #[case] right: Sign, #[case] expected: Sign) {
        let result = left.mul(right);
        assert_eq!(expected, result);
    }

    #[rstest]
    #[case(Sign::Positive, Sign::Negative)]
    #[case(Sign::Zero, Sign::Zero)]
    #[case(Sign::Negative, Sign::Positive)]
    fn test_sign_neg(#[case] input: Sign, #[case] expected: Sign) {
        let result = -input;
        assert_eq!(expected, result);
    }

    #[rstest]
    #[case(Sign::Positive, Sign::Zero)]
    #[case(Sign::Positive, Sign::Negative)]
    #[case(Sign::Zero, Sign::Negative)]
    fn test_sign_partial_cmp_greater(#[case] left: Sign, #[case] right: Sign) {
        match left.partial_cmp(&right) {
            Some(result) => {
                assert_matches!(result, Ordering::Greater);
            }
            None => {
                assert!(false);
            }
        }
    }

    #[rstest]
    #[case(Sign::Zero, Sign::Zero)]
    fn test_sign_partial_cmp_equal(#[case] left: Sign, #[case] right: Sign) {
        match left.partial_cmp(&right) {
            Some(result) => {
                assert_matches!(result, Ordering::Equal);
            }
            None => {
                assert!(false);
            }
        }
    }

    #[rstest]
    #[case(Sign::Zero, Sign::Positive)]
    #[case(Sign::Negative, Sign::Positive)]
    #[case(Sign::Negative, Sign::Zero)]
    fn test_sign_partial_cmp_less(#[case] left: Sign, #[case] right: Sign) {
        match left.partial_cmp(&right) {
            Some(result) => {
                assert_matches!(result, Ordering::Less);
            }
            None => {
                assert!(false);
            }
        }
    }

    #[rstest]
    #[case(Sign::Positive, Sign::Positive)]
    #[case(Sign::Negative, Sign::Negative)]
    fn test_sign_partial_cmp_non_comparable(#[case] left: Sign, #[case] right: Sign) {
        let result = left.partial_cmp(&right);
        assert_matches!(result, None);
    }
}
