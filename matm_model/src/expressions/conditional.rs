use crate::expressions::condition::Condition;
use crate::expressions::precedence::OperatorPrec;
use crate::expressions::precedence::OperatorPrecAccess;
use crate::expressions::Expression;
use crate::expressions::ToExpression;
use std::fmt;
use std::rc::Rc;


#[derive(Clone)]
#[derive(PartialEq)]
#[derive(Debug)]
pub struct ConditionalClause {
    pub expression: Rc<Expression>,
    pub condition: Rc<Condition>,
}

impl ConditionalClause {
    pub fn has_symbol(&self) -> bool {
        self.expression.has_symbol() || self.condition.has_symbol()
    }
}

impl fmt::Display for ConditionalClause {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}, {}", self.expression, self.condition)
    }
}

//------------------------------------------------

#[derive(Clone)]
#[derive(PartialEq)]
#[derive(Debug)]
pub struct ConditionalData {
    pub clauses: Vec<ConditionalClause>,
}

impl ConditionalData {
    pub fn from_vector(clauses: Vec<ConditionalClause>) -> Self {
        Self {
            clauses,
        }
    }

    pub fn has_symbol(&self) -> bool {
        for clause in &self.clauses {
            if clause.has_symbol() {
                return true;
            }
        }
        return false;
    }
}

impl ToExpression for ConditionalData {
    fn to_expr(self) -> Expression {
        Expression::Conditional(Box::new(self))
    }

    fn to_rc_expr(self) -> Rc<Expression> {
        Rc::new(self.to_expr())
    }
}

impl OperatorPrecAccess for ConditionalData {
    fn operator_prec(&self) -> OperatorPrec {
        return OperatorPrec::Atom;
    }
}

impl fmt::Display for ConditionalData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{{")?;
        let mut add_sep = false;
        for clause in &self.clauses {
            if add_sep {
                write!(f, "; ")?;
            }
            write!(f, "{}", clause)?;
            add_sep = true;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use std::rc::Rc;
    use crate::expressions::arithmetic_condition::ArithmeticCondition;
    use crate::expressions::bool::BoolData;
    use crate::expressions::condition::{Condition, ToCondition};
    use crate::expressions::conditional::ConditionalData;
    use crate::expressions::conditional::ConditionalClause;
    use crate::factories::ObjFactory;

    #[test]
    fn test_format_1_cond() {
        let fa = ObjFactory::new10();
        let expr = ConditionalData::from_vector(
            vec![
                ConditionalClause {
                    expression: fa.rz1(),
                    condition: ArithmeticCondition::gt(fa.rsn("x"), fa.rz0()).to_rc_cond(),
                },
            ]);

        assert_eq!("{1, x > 0", format!("{}", expr));
    }

    #[test]
    fn test_format_2_cond() {
        let fa = ObjFactory::new10();
        let expr = ConditionalData::from_vector(
            vec![
                ConditionalClause {
                    expression: fa.rz1(),
                    condition: ArithmeticCondition::gt(fa.rsn("x"), fa.rz0()).to_rc_cond(),
                },
                ConditionalClause {
                    expression: fa.rzi_u(-1),
                    condition: ArithmeticCondition::lt(fa.rsn("x"), fa.rz0()).to_rc_cond(),
                },
            ]);

        assert_eq!("{1, x > 0; -1, x < 0", format!("{}", expr));
    }

    #[test]
    fn test_clause_has_symbol_no() {
        let fa = ObjFactory::new10();
        let clause = ConditionalClause {
            expression: fa.rz0(),
            condition: Rc::new(Condition::Bool(BoolData::true_value())),
        };
        let result = clause.has_symbol();
        assert_eq!(false, result);
    }

    #[test]
    fn test_clause_has_symbol_in_expr() {
        let fa = ObjFactory::new10();
        let clause = ConditionalClause {
            expression: fa.rsn("a"),
            condition: Rc::new(Condition::Bool(BoolData::true_value())),
        };
        let result = clause.has_symbol();
        assert_eq!(true, result);
    }

    #[test]
    fn test_clause_has_symbol_in_cond() {
        let fa = ObjFactory::new10();
        let clause = ConditionalClause {
            expression: fa.rz0(),
            condition: Rc::new(Condition::Arithmetic(ArithmeticCondition::eq(
                fa.rsn("a"),
                fa.rz0()
            ))),
        };
        let result = clause.has_symbol();
        assert_eq!(true, result);
    }

    #[test]
    fn test_conditional_has_symbol_no() {
        let fa = ObjFactory::new10();
        let conditional = ConditionalData::from_vector(vec![
            ConditionalClause {
                expression: fa.rz0(),
                condition: Rc::new(Condition::Bool(BoolData::true_value())),
            }
        ]);
        let result = conditional.has_symbol();
        assert_eq!(false, result);
    }

    #[test]
    fn test_conditional_has_symbol_in_expr() {
        let fa = ObjFactory::new10();
        let conditional = ConditionalData::from_vector(vec![
            ConditionalClause {
                expression: fa.rsn("a"),
                condition: Rc::new(Condition::Bool(BoolData::true_value())),
            }
        ]);
        let result = conditional.has_symbol();
        assert_eq!(true, result);
    }
}
