use crate::expressions::precedence::OperatorPrec;
use crate::expressions::precedence::OperatorPrecAccess;
use crate::expressions::Expression;
use crate::expressions::ToExpression;
use std::fmt;
use std::rc::Rc;

#[derive(Clone, PartialEq, Debug)]
pub struct AssignmentData {
    pub target: Rc<Expression>,
    pub source: Rc<Expression>,
}

impl AssignmentData {
    pub fn new(target: Rc<Expression>, source: Rc<Expression>) -> Self {
        Self {
            target: target,
            source: source,
        }
    }

    pub fn has_symbol(&self) -> bool {
        return self.target.has_symbol() || self.source.has_symbol();
    }
}

impl ToExpression for AssignmentData {
    fn to_expr(self) -> Expression {
        Expression::Assignment(Box::new(self))
    }

    fn to_rc_expr(self) -> Rc<Expression> {
        Rc::new(self.to_expr())
    }
}

impl OperatorPrecAccess for AssignmentData {
    fn operator_prec(&self) -> OperatorPrec {
        return OperatorPrec::Assignment;
    }
}

impl fmt::Display for AssignmentData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}: {}", self.target, self.source)
    }
}

#[cfg(test)]
mod tests {
    use crate::expressions::assignment::AssignmentData;
    use crate::expressions::Expression;
    use crate::expressions::ToExpression;
    use crate::factories::ObjFactory;
    use crate::testing::asserts::assert_expression_that;

    #[test]
    fn test_has_symbol_assign_to_symbol() {
        let fa = ObjFactory::new10();
        let asgn = AssignmentData::new(
            fa.rsn("x"),
            fa.rzi_u(3),
        );

        assert_eq!(true, asgn.has_symbol());
    }

    #[test]
    fn test_format_symbol_int() {
        let fa = ObjFactory::new10();
        let asgn = AssignmentData::new(
            fa.rsn("x"),
            fa.rzi_u(3),
        );

        assert_eq!("x: 3", format!("{}", asgn));
    }

    #[test]
    fn test_to_expr() {
        let fa = ObjFactory::new10();
        let expr = AssignmentData::new(
            fa.rsn("x"),
            fa.rzi_u(3),
        ).to_expr();

        match expr {
            Expression::Assignment(o) => {
                assert_expression_that(&o.target)
                    .as_symbol()
                    .has_name("x");
            }
            _ => {
                assert!(false, "Expected assignment, found {}", expr.type_name());
            }
        }
    }

    #[test]
    fn test_to_rc_expr() {
        let fa = ObjFactory::new10();
        let expr = AssignmentData::new(
            fa.rsn("x"),
            fa.rzi_u(3),
        ).to_rc_expr();

        match expr.as_ref() {
            Expression::Assignment(o) => {
                assert_expression_that(&o.target)
                    .as_symbol()
                    .has_name("x");
            }
            _ => {
                assert!(false, "Expected assignment, found {}", expr.type_name());
            }
        }
    }

}
