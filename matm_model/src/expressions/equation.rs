use std::fmt;
use std::rc::Rc;

use crate::expressions::Expression;
use crate::expressions::ToExpression;
use crate::expressions::precedence::OperatorPrec;
use crate::expressions::precedence::OperatorPrecAccess;

#[derive(Clone, Debug)]
pub struct EquationData {
    pub left: Rc<Expression>,
    pub right: Rc<Expression>,
}

impl EquationData {
    pub fn new(left: Rc<Expression>, right: Rc<Expression>) -> Self {
        Self {
            left: left,
            right: right,
        }
    }

    pub fn has_symbol(&self) -> bool {
        return self.left.has_symbol() || self.left.has_symbol();
    }
}

impl ToExpression for EquationData {
    fn to_expr(self) -> Expression {
        return Expression::Equation(Box::new(self));
    }

    fn to_rc_expr(self) -> Rc<Expression> {
        return Rc::new(self.to_expr());
    }
}

impl OperatorPrecAccess for EquationData {
    fn operator_prec(&self) -> OperatorPrec {
        return OperatorPrec::Equation;
    }
}

impl PartialEq for EquationData {
    fn eq(&self, other: &EquationData) -> bool {
        // do not calculate
        return self.left.eq(&other.left)
            && self.right.eq(&other.right);
    }
}

impl fmt::Display for EquationData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} = {}", self.left, self.right)
    }
}

#[cfg(test)]
mod tests {
    use crate::testing::asserts::assert_expression_that;
    use crate::expressions::Expression;
    use crate::expressions::ToExpression;
    use crate::expressions::symbol::SymbolData;
    use crate::factories::ObjFactory;
    use crate::expressions::equation::EquationData;

    #[test]
    fn test_simple() {
        let fa = ObjFactory::new10();
        let equation = EquationData::new(
            SymbolData::new("y").to_rc_expr(),
            fa.rzi_u(3)
        );

        match equation.left.as_ref() {
            Expression::Symbol(symbol) => {
                assert_eq!("y", symbol.name);
            }
            _ => {
                assert_eq!(false, true);
            }
        }
        assert_expression_that(&equation.right)
            .as_integer()
            .is_int(3);
    }

    #[test]
    fn test_format_simple() {
        let fa = ObjFactory::new10();
        let equation = EquationData::new(
            SymbolData::new("y").to_rc_expr(),
            fa.rzi_u(3)
        );

        assert_eq!("y = 3", format!("{}", equation));
    }
}
