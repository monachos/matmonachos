use std::fmt;
use std::rc::Rc;
use crate::expressions::Expression;
use crate::expressions::ToExpression;
use crate::expressions::precedence::OperatorPrec;
use crate::expressions::precedence::OperatorPrecAccess;
use crate::write_wrapped_obj;

#[derive(Clone, Copy)]
#[derive(PartialEq)]
#[derive(Debug)]
pub enum UnaryOperator {
    Group,
    Neg,
    Transpose,
    Determinant,
    Rank,
    Inversion,
    Factorial,
}

impl fmt::Display for UnaryOperator {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s = match self {
            UnaryOperator::Group => "group",
            UnaryOperator::Neg => "negation",
            UnaryOperator::Transpose => "transposition",
            UnaryOperator::Determinant => "determinant",
            UnaryOperator::Rank => "rank",
            UnaryOperator::Inversion => "inversion",
            UnaryOperator::Factorial => "factorial",
        };
        write!(f, "{}", s)
    }
}


#[derive(Clone)]
#[derive(PartialEq)]
#[derive(Debug)]
pub struct UnaryData {
    pub operator: UnaryOperator,
    pub value: Rc<Expression>,
}

impl UnaryData {
    pub fn from_operator(op: UnaryOperator, expr: Rc<Expression>) -> Self {
        Self {
            operator: op,
            value: expr,
        }
    }

    pub fn group(expr: Rc<Expression>) -> Self {
        Self::from_operator(UnaryOperator::Group, expr)
    }

    pub fn neg(expr: Rc<Expression>) -> Self {
        Self::from_operator(UnaryOperator::Neg, expr)
    }

    pub fn transpose(expr: Rc<Expression>) -> Self {
        Self::from_operator(UnaryOperator::Transpose, expr)
    }

    pub fn det(expr: Rc<Expression>) -> Self {
        Self::from_operator(UnaryOperator::Determinant, expr)
    }

    pub fn rank(expr: Rc<Expression>) -> Self {
        Self::from_operator(UnaryOperator::Rank, expr)
    }

    pub fn inv(expr: Rc<Expression>) -> Self {
        Self::from_operator(UnaryOperator::Inversion, expr)
    }

    pub fn factorial(expr: Rc<Expression>) -> Self {
        Self::from_operator(UnaryOperator::Factorial, expr)
    }

    pub fn has_symbol(&self) -> bool {
        return self.value.has_symbol();
    }
}

impl ToExpression for UnaryData {
    fn to_expr(self) -> Expression {
        return Expression::Unary(Box::new(self));
    }

    fn to_rc_expr(self) -> Rc<Expression> {
        return Rc::new(self.to_expr());
    }
}

impl OperatorPrecAccess for UnaryData {
    fn operator_prec(&self) -> OperatorPrec {
        return match self.operator {
            UnaryOperator::Group => OperatorPrec::Atom,
            UnaryOperator::Neg => OperatorPrec::Minus,
            UnaryOperator::Transpose => OperatorPrec::Exp,
            UnaryOperator::Determinant => OperatorPrec::MatrixFunction,
            UnaryOperator::Rank => OperatorPrec::MatrixFunction,
            UnaryOperator::Inversion => OperatorPrec::MatrixFunction,
            UnaryOperator::Factorial => OperatorPrec::Factorial,
        };
    }
}

impl fmt::Display for UnaryData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.operator {
            UnaryOperator::Group => write_wrapped_obj(f, &self.value, true),
            UnaryOperator::Neg => {
                write!(f, "-")?;
                let wrap = self.value.operator_prec() < self.operator_prec()
                    && self.value.operator_prec() != OperatorPrec::Atom;
                write_wrapped_obj(f, &self.value, wrap)
            },
            UnaryOperator::Transpose => write!(f, "{}'", self.value),
            UnaryOperator::Determinant => write!(f, "det({})", self.value),
            UnaryOperator::Rank => write!(f, "rank({})", self.value),
            UnaryOperator::Inversion => write!(f, "inv({})", self.value),
            UnaryOperator::Factorial => write!(f, "{}!", self.value),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::core::natural::RADIX_10;
    use crate::expressions::ToExpression;
    use crate::expressions::binary::BinaryData;
    use crate::expressions::symbol::SymbolData;
    use crate::expressions::unary::UnaryData;
    use crate::expressions::matrices::MatrixData;
    use crate::factories::ObjFactory;


    #[test]
    fn test_format_group() {
        let fa = ObjFactory::new10();
        let expr = UnaryData::group(
            BinaryData::add(
                fa.rfi_u(2, 5),
                fa.rzi_u(3),
            ).to_rc_expr()
        ).to_expr();

        assert_eq!("(2/5 + 3)", format!("{}", expr));
    }

    #[test]
    fn test_format_neg_int() {
        let fa = ObjFactory::new10();
        let expr = UnaryData::neg(
            fa.rzi_u(2)
        ).to_expr();

        assert_eq!("-2", format!("{}", expr));
    }

    #[test]
    fn test_format_neg_frac() {
        let fa = ObjFactory::new10();
        let expr = UnaryData::neg(
            fa.rfi_u(2, 5)
        ).to_expr();

        assert_eq!("-(2/5)", format!("{}", expr));
    }

    #[test]
    fn test_format_transpose_matrix() {
        let expr = UnaryData::transpose(
            MatrixData::identity(RADIX_10, 2).unwrap().to_rc_expr()
        ).to_expr();

        assert_eq!("[1, 0; 0, 1]'", format!("{}", expr));
    }

    #[test]
    fn test_format_determinant_symbol() {
        let expr = UnaryData::det(
            SymbolData::new("A").to_rc_expr()
        ).to_expr();

        assert_eq!("det(A)", format!("{}", expr));
    }

    #[test]
    fn test_format_determinant_matrix() {
        let expr = UnaryData::det(
            MatrixData::identity(RADIX_10, 2).unwrap().to_rc_expr()
        ).to_expr();

        assert_eq!("det([1, 0; 0, 1])", format!("{}", expr));
    }

    #[test]
    fn test_format_rank_symbol() {
        let expr = UnaryData::rank(
            SymbolData::new("A").to_rc_expr()
        ).to_expr();

        assert_eq!("rank(A)", format!("{}", expr));
    }

    #[test]
    fn test_format_rank_matrix() {
        let expr = UnaryData::rank(
            MatrixData::identity(RADIX_10, 2).unwrap().to_rc_expr()
        ).to_expr();

        assert_eq!("rank([1, 0; 0, 1])", format!("{}", expr));
    }

    #[test]
    fn test_format_factorial() {
        let fa = ObjFactory::new10();
        let expr = UnaryData::factorial(fa.rzi_u(2)).to_expr();

        assert_eq!("2!", format!("{}", expr));
    }

    // #[test]
    // fn test_format_abs() {
    //     let fa = ObjFactory::new10();
    //     let expr = Unary::abs(fa.rzi_u(2)).to_expr();

    //     assert_eq!("|2|", format!("{}", expr));
    // }

}
