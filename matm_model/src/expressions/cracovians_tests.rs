#[cfg(test)]
mod tests {
    use crate::testing::asserts::assert_cracovian_that;
    use crate::testing::asserts::assert_result_error_that;
    use crate::core::natural::RADIX_10;
    use crate::expressions::cracovians::CracovianData;
    use crate::factories::ObjFactory;

    #[test]
    fn test_zeros() {
        let m = CracovianData::zeros(RADIX_10, 3, 2).unwrap();
        assert_cracovian_that(&m)
            .has_columns(3)
            .has_rows(2)
            .has_i_i_zero(1, 1)
            .has_i_i_zero(1, 2)
            .has_i_i_zero(2, 1)
            .has_i_i_zero(2, 2)
            .has_i_i_zero(3, 1)
            .has_i_i_zero(3, 2);
    }

    #[test]
    fn test_zeros_invalid_row_size() {
        let result = CracovianData::zeros(RADIX_10, 0, 3);
        assert_result_error_that(result)
            .has_message("You need to specify positive size of all dimensions.");
    }

    #[test]
    fn test_zeros_invalid_column_size() {
        let result = CracovianData::zeros(RADIX_10, 2, 0);
        assert_result_error_that(result)
            .has_message("You need to specify positive size of all dimensions.");
    }

    #[test]
    fn test_from_vector() {
        let fa = ObjFactory::new10();
        let m = CracovianData::from_vector(3, 2,
                                           vec![
                fa.rzi_u(1),
                fa.rzi_u(2),
                fa.rzi_u(3),
                fa.rzi_u(4),
                fa.rzi_u(5),
                fa.rzi_u(6)]).unwrap();
                assert_cracovian_that(&m)
            .has_columns(3)
            .has_rows(2)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(1);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(2);})
            .is_i_i_integer_that(3, 1, |e| {e.is_int(3);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(4);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(5);})
            .is_i_i_integer_that(3, 2, |e| {e.is_int(6);});
    }

    #[test]
    fn test_from_vector_invalid_row_size() {
        let fa = ObjFactory::new10();
        assert_result_error_that(CracovianData::from_vector(0, 1, vec![fa.rzi_u(1)]))
            .has_message("You need to specify positive size of all dimensions.");
    }

    #[test]
    fn test_from_vector_invalid_column_size() {
        let fa = ObjFactory::new10();
        assert_result_error_that(CracovianData::from_vector(1, 0, vec![fa.rzi_u(1)]))
            .has_message("You need to specify positive size of all dimensions.");
    }

    #[test]
    fn test_from_vector_invalid_element_count() {
        let fa = ObjFactory::new10();
        assert_result_error_that(CracovianData::from_vector(3, 2, vec![fa.rzi_u(1)]))
            .has_message("Invalid number of elements, 1 given, 6 expected.");
    }

    #[test]
    fn test_from_ivector() {
        let m = CracovianData::from_ivector(3, 2,
                                            RADIX_10,
                                            vec![1, 2, 3, 4, 5, 6]).unwrap();
        assert_cracovian_that(&m)
            .has_columns(3)
            .has_rows(2)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(1);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(2);})
            .is_i_i_integer_that(3, 1, |e| {e.is_int(3);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(4);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(5);})
            .is_i_i_integer_that(3, 2, |e| {e.is_int(6);});
    }

    #[test]
    fn test_rows() {
        let m = CracovianData::zeros(RADIX_10, 3, 2).unwrap();
        assert_eq!(2, m.rows());
    }

    #[test]
    fn test_columns() {
        let m = CracovianData::zeros(RADIX_10, 3, 2).unwrap();
        assert_eq!(3, m.columns());
    }

    #[test]
    fn test_size() {
        let m = CracovianData::zeros(RADIX_10, 3, 2).unwrap();
        let result = m.size();
        assert_eq!(2, result.rows);
        assert_eq!(3, result.columns);
    }

    #[test]
    fn test_is_square() {
        assert_eq!(true, CracovianData::zeros(RADIX_10, 2, 2).unwrap().is_square());
    }

    #[test]
    fn test_is_square_1_2() {
        assert_eq!(false, CracovianData::zeros(RADIX_10, 1, 2).unwrap().is_square());
    }

    #[test]
    fn test_is_square_2_1() {
        assert_eq!(false, CracovianData::zeros(RADIX_10, 2, 1).unwrap().is_square());
    }

    #[test]
    fn test_get_bounded() {
        let m = CracovianData::zeros(RADIX_10, 1, 2).unwrap();
        assert_eq!(true, m.get_i_i(1, 1).unwrap().is_zero());
        assert_eq!(true, m.get_i_i(1, 2).unwrap().is_zero());
    }

    #[test]
    fn test_get_unbounded_row_too_small() {
        let m = CracovianData::zeros(RADIX_10, 1, 2).unwrap();
        assert_result_error_that(m.get_i_i(0, 1))
            .has_message("Invalid element index (0, 1).");
    }

    #[test]
    fn test_get_unbounded_row_too_big() {
        let m = CracovianData::zeros(RADIX_10, 1, 2).unwrap();
        assert_result_error_that(m.get_i_i(2, 1))
            .has_message("Invalid element index (2, 1).");
    }

    #[test]
    fn test_get_unbounded_column_too_small() {
        let m = CracovianData::zeros(RADIX_10, 1, 2).unwrap();
        assert_result_error_that(m.get_i_i(1, 0))
            .has_message("Invalid element index (1, 0).");
    }

    #[test]
    fn test_get_unbounded_column_too_big() {
        let m = CracovianData::zeros(RADIX_10, 1, 2).unwrap();
        assert_result_error_that(m.get_i_i(1, 3))
            .has_message("Invalid element index (1, 3).");
    }

    #[test]
    fn test_set_bounded() {
        let fa = ObjFactory::new10();
        let mut m = CracovianData::zeros(RADIX_10, 1, 2).unwrap();
        m.set_i_i(1, 1, fa.rzi_u(1)).unwrap();
        assert_eq!(true, m.get_i_i(1, 1).unwrap().is_one());
        assert_eq!(true, m.get_i_i(1, 2).unwrap().is_zero());
    }

    #[test]
    fn test_set_unbounded_row_too_small() {
        let fa = ObjFactory::new10();
        let mut m = CracovianData::zeros(RADIX_10, 1, 2).unwrap();
        assert_result_error_that(m.set_i_i(0, 1, fa.rzi_u(1)))
            .has_message("Invalid element index (0, 1).");
    }

    #[test]
    fn test_set_unbounded_row_too_big() {
        let fa = ObjFactory::new10();
        let mut m = CracovianData::zeros(RADIX_10, 1, 2).unwrap();
        assert_result_error_that(m.set_i_i(2, 1, fa.rzi_u(1)))
            .has_message("Invalid element index (2, 1).");
    }

    #[test]
    fn test_set_unbounded_column_too_small() {
        let fa = ObjFactory::new10();
        let mut m = CracovianData::zeros(RADIX_10, 1, 2).unwrap();
        assert_result_error_that(m.set_i_i(1, 0, fa.rzi_u(1)))
            .has_message("Invalid element index (1, 0).");
    }

    #[test]
    fn test_set_unbounded_column_too_big() {
        let fa = ObjFactory::new10();
        let mut m = CracovianData::zeros(RADIX_10, 1, 2).unwrap();
        assert_result_error_that(m.set_i_i(1, 3, fa.rzi_u(1)))
            .has_message("Invalid element index (1, 3).");
    }

    #[test]
    fn test_clone() {
        let fa = ObjFactory::new10();
        let mut m1 = CracovianData::zeros(RADIX_10, 2, 2).unwrap();
        m1.set_i_i(1, 1, fa.rzi_u(1)).unwrap();
        m1.set_i_i(1, 2, fa.rzi_u(2)).unwrap();
        m1.set_i_i(2, 1, fa.rzi_u(3)).unwrap();
        m1.set_i_i(2, 2, fa.rzi_u(4)).unwrap();

        let mut m2 = m1.clone();
        
        assert_cracovian_that(&m1)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(1);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(2);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(3);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(4);});
        assert_cracovian_that(&m2)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(1);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(2);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(3);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(4);});

        m2.set_i_i(1, 1, fa.rzi_u(5)).unwrap();
        m2.set_i_i(1, 2, fa.rzi_u(6)).unwrap();
        m2.set_i_i(2, 1, fa.rzi_u(7)).unwrap();
        m2.set_i_i(2, 2, fa.rzi_u(8)).unwrap();

        assert_cracovian_that(&m1)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(1);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(2);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(3);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(4);});
        assert_cracovian_that(&m2)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(5);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(6);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(7);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(8);});
    }


    #[test]
    fn test_format_1_1() {
        let m1 = CracovianData::from_ivector(
            1, 1,
            RADIX_10,
            vec![11]).unwrap();

        assert_eq!("{11}", format!("{}", m1));
    }

    #[test]
    fn test_format_2_1() {
        let m1 = CracovianData::from_ivector(
            2, 1,
            RADIX_10,
            vec![11, 21]).unwrap();

        assert_eq!("{11, 21}", format!("{}", m1));
    }

    #[test]
    fn test_format_2_2() {
        let m1 = CracovianData::from_ivector(
            2, 2,
            RADIX_10,
            vec![
                11, 21,
                12, 22]
            ).unwrap();
        
        assert_eq!("{11, 21; 12, 22}", format!("{}", m1));
    }

    #[test]
    fn test_format_2_3() {
        let m1 = CracovianData::from_ivector(
            2, 3,
            RADIX_10,
            vec![
                11, 21,
                12, 22,
                13, 23]
            ).unwrap();
        
        assert_eq!("{11, 21; 12, 22; 13, 23}", format!("{}", m1));
    }

    #[test]
    fn test_partial_eq() {
        let m1 = CracovianData::from_ivector(2, 2,
                                             RADIX_10,
                                             vec![
            1, 2,
            3, 4
        ]).unwrap();
        let m2 = CracovianData::from_ivector(2, 2,
                                             RADIX_10,
                                             vec![
            1, 2,
            3, 4
        ]).unwrap();

        assert_eq!(true, m1 == m2);
    }

    #[test]
    fn test_partial_neq() {
        let m1 = CracovianData::from_ivector(2, 2,
                                             RADIX_10,
                                             vec![
            1, 2,
            3, 4
        ]).unwrap();
        let m2 = CracovianData::from_ivector(2, 2,
                                             RADIX_10,
                                             vec![
            1, 2,
            3, 10
        ]).unwrap();

        assert_eq!(false, m1 == m2);
    }

    #[test]
    fn test_partial_eq_the_same_diff_type() {
        let m1 = CracovianData::from_ivector(2, 2,
                                             RADIX_10,
                                             vec![
            1, 2,
            3, 4
        ]).unwrap();
        let m2 = CracovianData::from_ivector(2, 2,
                                             RADIX_10,
                                             vec![
            1, 2,
            3, 4
        ]).unwrap();

        assert_eq!(true, m1 == m2);
    }

}
