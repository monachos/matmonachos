use std::fmt;
use std::rc::Rc;
use crate::expressions::condition::Condition;
use crate::expressions::condition::ToCondition;
use crate::expressions::logic_operator::LogicOperator;
use crate::expressions::precedence::OperatorPrec;
use crate::expressions::precedence::OperatorPrecAccess;
use crate::write_wrapped_obj;

#[derive(Clone, Debug, PartialEq)]
pub struct LogicCondition {
    pub operator: LogicOperator,
    pub left: Rc<Condition>,
    pub right: Rc<Condition>,
}

impl LogicCondition {
    pub fn from_operator(
        operator: LogicOperator,
        left: Rc<Condition>,
        right: Rc<Condition>) -> Self {
        Self {
            operator: operator,
            left: left,
            right: right,
        }
    }

    pub fn and(left: Rc<Condition>, right: Rc<Condition>) -> Self {
        Self::from_operator(LogicOperator::And, left, right)
    }

    pub fn or(left: Rc<Condition>, right: Rc<Condition>) -> Self {
        Self::from_operator(LogicOperator::Or, left, right)
    }

    pub fn has_symbol(&self) -> bool {
        return self.left.has_symbol() || self.right.has_symbol();
    }

    pub fn should_wrap_left_with_parentheses(&self) -> bool {
        self.left.operator_prec() < self.operator_prec()
    }

    pub fn should_wrap_right_with_parentheses(&self) -> bool {
        self.right.operator_prec() < self.operator_prec()
    }
}

impl OperatorPrecAccess for LogicCondition {
    fn operator_prec(&self) -> OperatorPrec {
        return self.operator.operator_prec();
    }
}

impl ToCondition for LogicCondition {
    fn to_cond(self) -> Condition {
        Condition::Logic(Box::new(self))
    }

    fn to_rc_cond(self) -> Rc<Condition> {
        Rc::new(self.to_cond())
    }
}

impl fmt::Display for LogicCondition {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write_wrapped_obj(f, &self.left, self.should_wrap_left_with_parentheses())?;
        write!(f, " {} ", self.operator)?;
        write_wrapped_obj(f, &self.right, self.should_wrap_right_with_parentheses())?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::expressions::arithmetic_condition::ArithmeticCondition;
    use crate::expressions::bool::BoolData;
    use crate::expressions::condition::ToCondition;
    use crate::expressions::logic_condition::LogicCondition;
    use crate::factories::ObjFactory;

    #[test]
    fn test_display_logic_condition() {
        let fa = ObjFactory::new10();
        let cond = LogicCondition::and(
            ArithmeticCondition::gt(fa.rsn("x"), fa.rz0()).to_rc_cond(),
            ArithmeticCondition::lt(fa.rsn("x"), fa.rzi_u(10)).to_rc_cond()
        );
        let result = format!("{}", cond);
        assert_eq!("x > 0 and x < 10", result);
    }

    #[test]
    fn test_logic_condition_format_groups() {
        let fa = ObjFactory::new10();
        let cond = LogicCondition::and(
            LogicCondition::or(
                ArithmeticCondition::gt(fa.rsn("x"), fa.rz0()).to_rc_cond(),
                ArithmeticCondition::lt(fa.rsn("x"), fa.rzi_u(10)).to_rc_cond()
            ).to_rc_cond(),
            LogicCondition::or(
                ArithmeticCondition::gt(fa.rsn("y"), fa.rz0()).to_rc_cond(),
                ArithmeticCondition::lt(fa.rsn("y"), fa.rzi_u(10)).to_rc_cond()
            ).to_rc_cond()
        );
        let result = format!("{}", cond);
        assert_eq!("(x > 0 or x < 10) and (y > 0 or y < 10)", result);
    }

    #[test]
    fn test_logic_condition_should_wrap_left_for_true_true() {
        let cond = LogicCondition::or(
            BoolData::new(true).to_rc_cond(),
            BoolData::new(false).to_rc_cond(),
        );
        let result = cond.should_wrap_left_with_parentheses();
        assert_eq!(false, result);
    }

    #[test]
    fn test_logic_condition_should_wrap_left_for_and_true() {
        let cond = LogicCondition::or(
            LogicCondition::and(
                BoolData::new(true).to_rc_cond(),
                BoolData::new(false).to_rc_cond(),
            ).to_rc_cond(),
            BoolData::new(false).to_rc_cond(),
        );
        let result = cond.should_wrap_left_with_parentheses();
        assert_eq!(false, result);
    }

    #[test]
    fn test_logic_condition_should_wrap_left_for_or_true() {
        let cond = LogicCondition::and(
            LogicCondition::or(
                BoolData::new(true).to_rc_cond(),
                BoolData::new(false).to_rc_cond(),
            ).to_rc_cond(),
            BoolData::new(false).to_rc_cond(),
        );
        let result = cond.should_wrap_left_with_parentheses();
        assert_eq!(true, result);
    }

    #[test]
    fn test_logic_condition_should_wrap_right_for_true_true() {
        let cond = LogicCondition::or(
            BoolData::new(true).to_rc_cond(),
            BoolData::new(false).to_rc_cond(),
        );
        let result = cond.should_wrap_right_with_parentheses();
        assert_eq!(false, result);
    }

    #[test]
    fn test_logic_condition_should_wrap_right_for_and_true() {
        let cond = LogicCondition::or(
            BoolData::new(false).to_rc_cond(),
            LogicCondition::and(
                BoolData::new(true).to_rc_cond(),
                BoolData::new(false).to_rc_cond(),
            ).to_rc_cond(),
        );
        let result = cond.should_wrap_right_with_parentheses();
        assert_eq!(false, result);
    }

    #[test]
    fn test_logic_condition_should_wrap_right_for_or_true() {
        let cond = LogicCondition::and(
            BoolData::new(false).to_rc_cond(),
            LogicCondition::or(
                BoolData::new(true).to_rc_cond(),
                BoolData::new(false).to_rc_cond(),
            ).to_rc_cond(),
        );
        let result = cond.should_wrap_right_with_parentheses();
        assert_eq!(true, result);
    }

}
