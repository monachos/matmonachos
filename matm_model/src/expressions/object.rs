use crate::expressions::Expression;
use crate::expressions::ToExpression;
use crate::expressions::precedence::OperatorPrec;
use crate::expressions::precedence::OperatorPrecAccess;
use std::fmt;
use std::rc::Rc;


#[derive(Clone)]
#[derive(PartialEq)]
#[derive(Debug)]
pub struct DetConfigData {
    pub method: DetMethod,
}

impl DetConfigData {
    pub fn new() -> Self {
        Self {
            method: DetMethod::Default,
        }
    }
}

#[derive(Clone)]
#[derive(PartialEq)]
#[derive(Debug)]
#[derive(Copy)]
pub enum DetMethod {
    /// Sarrus for size 1-3, Laplace expansion for larger
    Default,
    /// Laplace's expansion for matrices 3x3 and larger
    CofactorExpansion,
    /// Bareiss-Montante for matrices 3x3 and larger
    Bareiss,
}

//-------------------------------------------------

#[derive(Clone)]
#[derive(PartialEq)]
#[derive(Debug)]
pub struct InvConfigData {
    pub method: InvMethod,
}

impl InvConfigData {
    pub fn new() -> Self {
        Self {
            method: InvMethod::Adjugate,
        }
    }
}

#[derive(Clone)]
#[derive(PartialEq)]
#[derive(Debug)]
#[derive(Copy)]
pub enum InvMethod {
    /// Matrix inversion by creating adjugate matrix and determinant
    Adjugate,
    /// Matrix inversion by Gaussian elimination of augmented matrix
    Gaussian,
}

//-------------------------------------------------

#[derive(Clone)]
#[derive(PartialEq)]
#[derive(Debug)]
pub struct GaussConfigData {
    pub pivot: GaussPivotType,
    pub make_identity: bool,
    pub reduce_upper: bool,
    pub reduce_lower: bool,
}

impl GaussConfigData {
    pub fn new() -> Self {
        Self {
            pivot: GaussPivotType::No,
            make_identity: false,
            reduce_upper: false,
            reduce_lower: false,
        }
    }
}

#[derive(Clone)]
#[derive(PartialEq)]
#[derive(Debug)]
#[derive(Copy)]
pub enum GaussPivotType {
    No,
    IfLessThanMaxAbs,
    IfZero,
}

//-------------------------------------------------

#[derive(Clone)]
#[derive(PartialEq)]
#[derive(Debug)]
pub struct PascalMatrixConfigData {
    pub variant: PascalMatrixVariant,
}

impl PascalMatrixConfigData {
    pub fn new() -> Self {
        Self {
            variant: PascalMatrixVariant::Symmetric,
        }
    }
}

#[derive(Clone)]
#[derive(PartialEq)]
#[derive(Debug)]
#[derive(Copy)]
pub enum PascalMatrixVariant {
    Symmetric,
    UpperTriangular,
    LowerTriangular,
}

//-------------------------------------------------

//TODO It will be replaced with object definition in the future.
/// Object type for predefined types.
#[derive(Clone)]
#[derive(PartialEq)]
#[derive(Debug)]
pub enum ObjectType {
    Cracovian,
    DetConfig(DetConfigData),
    GaussConfig(GaussConfigData),
    InvConfig(InvConfigData),
    Matrix,
    PascalMatrixConfig(PascalMatrixConfigData),
    Equations,
}

#[derive(Clone)]
#[derive(PartialEq)]
#[derive(Debug)]
pub struct ObjectData {
    pub object_type: ObjectType,
}

impl fmt::Display for ObjectType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let name = match self {
            Self::Cracovian => "cracovian",
            Self::DetConfig(_) => "det_config",
            Self::GaussConfig(_) => "gauss_config",
            Self::InvConfig(_) => "inv_config",
            Self::Matrix => "matrix",
            Self::PascalMatrixConfig(_) => "pascal_matrix_config",
            Self::Equations => "equations",
        };
        write!(f, "{}", name)
    }
}

//-------------------------------------------------

impl ObjectData {
    pub fn new(obj_type: ObjectType) -> Self {
        Self {
            object_type: obj_type,
        }
    }
}

impl ToExpression for ObjectData {
    fn to_expr(self) -> Expression {
        Expression::Object(Box::new(self))
    }

    fn to_rc_expr(self) -> Rc<Expression> {
        Rc::new(self.to_expr())
    }
}

impl OperatorPrecAccess for ObjectData {
    fn operator_prec(&self) -> OperatorPrec {
        return OperatorPrec::Atom;
    }
}

impl fmt::Display for ObjectData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{{{}}}", self.object_type)?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use rstest::rstest;
    use crate::expressions::object::DetConfigData;
    use crate::expressions::object::DetMethod;
    use crate::expressions::object::GaussConfigData;
    use crate::expressions::object::InvConfigData;
    use crate::expressions::object::ObjectData;
    use crate::expressions::object::ObjectType;
    use crate::expressions::object::PascalMatrixConfigData;
    use crate::expressions::precedence::{OperatorPrec, OperatorPrecAccess};
    use crate::expressions::ToExpression;
    use crate::testing::asserts::assert_expression_that;

    #[test]
    fn test_new() {
        let obj = ObjectData::new(ObjectType::Matrix);

        assert_eq!(obj.object_type, ObjectType::Matrix);
    }

    #[rstest]
    #[case(ObjectType::Cracovian, "{cracovian}")]
    #[case(ObjectType::DetConfig(DetConfigData::new()), "{det_config}")]
    #[case(ObjectType::GaussConfig(GaussConfigData::new()), "{gauss_config}")]
    #[case(ObjectType::InvConfig(InvConfigData::new()), "{inv_config}")]
    #[case(ObjectType::Matrix, "{matrix}")]
    #[case(ObjectType::PascalMatrixConfig(PascalMatrixConfigData::new()), "{pascal_matrix_config}")]
    fn test_format(
        #[case] input: ObjectType,
        #[case] expected: &str,
    ) {
        let obj = ObjectData::new(input);

        assert_eq!(expected, format!("{}", obj));
    }

    #[rstest]
    #[case(true, ObjectType::Cracovian, ObjectType::Cracovian)]
    #[case(false, ObjectType::Cracovian, ObjectType::Matrix)]
    #[case(true, ObjectType::DetConfig(DetConfigData::new()), ObjectType::DetConfig(DetConfigData::new()))]
    #[case(true, ObjectType::GaussConfig(GaussConfigData::new()), ObjectType::GaussConfig(GaussConfigData::new()))]
    #[case(true, ObjectType::InvConfig(InvConfigData::new()), ObjectType::InvConfig(InvConfigData::new()))]
    #[case(true, ObjectType::Matrix, ObjectType::Matrix)]
    #[case(false, ObjectType::Matrix, ObjectType::Cracovian)]
    #[case(true, ObjectType::PascalMatrixConfig(PascalMatrixConfigData::new()), ObjectType::PascalMatrixConfig(PascalMatrixConfigData::new()))]
    #[case(false, ObjectType::PascalMatrixConfig(PascalMatrixConfigData::new()), ObjectType::Matrix)]
    fn test_partial_eq(
        #[case] expected: bool,
        #[case] left: ObjectType,
        #[case] right: ObjectType,
    ) {
        let s1 = ObjectData::new(left);
        let s2 = ObjectData::new(right);

        assert_eq!(expected, s1 == s2);
    }

    #[test]
    fn test_to_expr() {
        let obj = ObjectData::new(ObjectType::Matrix);
        let expr = obj.to_expr();

        assert_expression_that(&expr)
            .as_object()
            .is_type(ObjectType::Matrix);
    }

    #[test]
    fn test_to_rc_expr() {
        let obj = ObjectData::new(ObjectType::Matrix);
        let expr = obj.to_rc_expr();

        assert_expression_that(&expr)
            .as_object()
            .is_type(ObjectType::Matrix);
    }

    #[test]
    fn test_operator_prec() {
        let obj = ObjectData::new(ObjectType::Matrix);

        assert!(OperatorPrec::Atom == obj.operator_prec());
    }

    #[rstest]
    #[case(ObjectType::Cracovian)]
    #[case(ObjectType::DetConfig(DetConfigData::new()))]
    #[case(ObjectType::DetConfig(DetConfigData {method: DetMethod::Bareiss}))]
    #[case(ObjectType::GaussConfig(GaussConfigData::new()))]
    #[case(ObjectType::InvConfig(InvConfigData::new()))]
    #[case(ObjectType::Matrix)]
    #[case(ObjectType::PascalMatrixConfig(PascalMatrixConfigData::new()))]
    fn test_clone_object_type(
        #[case] object_type: ObjectType,
    ) {
        let other = object_type.clone();
        assert_eq!(object_type, other);
    }

    #[rstest]
    #[case(ObjectData::new(ObjectType::Cracovian))]
    #[case(ObjectData::new(ObjectType::DetConfig(DetConfigData::new())))]
    #[case(ObjectData::new(ObjectType::DetConfig(DetConfigData {method: DetMethod::Bareiss})))]
    #[case(ObjectData::new(ObjectType::GaussConfig(GaussConfigData::new())))]
    #[case(ObjectData::new(ObjectType::InvConfig(InvConfigData::new())))]
    #[case(ObjectData::new(ObjectType::Matrix))]
    #[case(ObjectData::new(ObjectType::PascalMatrixConfig(PascalMatrixConfigData::new())))]
    fn test_clone_object(
        #[case] obj: ObjectData,
    ) {
        let other = obj.clone();
        assert_eq!(obj, other);
    }

    #[rstest]
    #[case(ObjectData::new(ObjectType::Cracovian))]
    #[case(ObjectData::new(ObjectType::DetConfig(DetConfigData::new())))]
    #[case(ObjectData::new(ObjectType::GaussConfig(GaussConfigData::new())))]
    #[case(ObjectData::new(ObjectType::InvConfig(InvConfigData::new())))]
    #[case(ObjectData::new(ObjectType::Matrix))]
    #[case(ObjectData::new(ObjectType::PascalMatrixConfig(PascalMatrixConfigData::new())))]
    fn test_debug_object(
        #[case] obj: ObjectData,
    ) {
        let s = format!("{:?}", obj);
        assert_eq!(false, s.is_empty());
    }
}
