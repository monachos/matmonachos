use crate::testing::asserts::assert_expression_that;
use crate::testing::asserts::int_that::IntegerAssertThat;
use crate::testing::asserts::frac_that::FractionAssertThat;
use crate::testing::asserts::symbol_that::SymbolAssertThat;
use crate::expressions::binary::BinaryData;
use crate::expressions::binary::BinaryOperator;

use super::expr_that::ExpressionAssertThat;


pub struct BinaryAssertThat<'a> {
    pub value: &'a BinaryData,
}

impl <'a> BinaryAssertThat<'a> {
    pub fn is_operator(self, expected: BinaryOperator) -> BinaryAssertThat<'a> {
        assert_eq!(expected, self.value.operator,
                   "Expected operator {}, actual is {}",
                   expected,
                   self.value.operator);
        return self;
    }

    pub fn is_add(self) -> BinaryAssertThat<'a> {
        return self.is_operator(BinaryOperator::Add);
    }

    pub fn is_sub(self) -> BinaryAssertThat<'a> {
        return self.is_operator(BinaryOperator::Sub);
    }

    pub fn is_mul(self) -> BinaryAssertThat<'a> {
        return self.is_operator(BinaryOperator::Mul);
    }

    pub fn is_div(self) -> BinaryAssertThat<'a> {
        return self.is_operator(BinaryOperator::Div);
    }

    pub fn has_left_that(self, f: fn(ExpressionAssertThat) -> ()) -> BinaryAssertThat<'a> {
        let handler = assert_expression_that(&self.value.left);
        f(handler);
        return self;
    }

    pub fn has_right_that(self, f: fn(ExpressionAssertThat) -> ()) -> BinaryAssertThat<'a> {
        let handler = assert_expression_that(&self.value.right);
        f(handler);
        return self;
    }

    pub fn is_left_integer_that(self, f: fn(IntegerAssertThat) -> ()) -> BinaryAssertThat<'a> {
        let handler = assert_expression_that(&self.value.left)
            .as_integer();
        f(handler);
        return self;
    }

    pub fn is_right_integer_that(self, f: fn(IntegerAssertThat) -> ()) -> BinaryAssertThat<'a> {
        let handler = assert_expression_that(&self.value.right)
            .as_integer();
        f(handler);
        return self;
    }

    pub fn has_left_i32(self, expected: i32) -> BinaryAssertThat<'a> {
        assert_expression_that(&self.value.left)
            .as_integer()
            .is_int(expected);
        return self;
    }

    pub fn has_right_i32(self, expected: i32) -> BinaryAssertThat<'a> {
        assert_expression_that(&self.value.right)
            .as_integer()
            .is_int(expected);
        return self;
    }

    pub fn is_left_fraction_that(self, f: fn(FractionAssertThat) -> ()) -> BinaryAssertThat<'a> {
        let handler = assert_expression_that(&self.value.left)
            .as_fraction();
        f(handler);
        return self;
    }

    pub fn is_right_fraction_that(self, f: fn(FractionAssertThat) -> ()) -> BinaryAssertThat<'a> {
        let handler = assert_expression_that(&self.value.right)
            .as_fraction();
        f(handler);
        return self;
    }

    pub fn is_left_symbol_that(self, f: fn(SymbolAssertThat) -> ()) -> BinaryAssertThat<'a> {
        let handler = assert_expression_that(&self.value.left)
            .as_symbol();
        f(handler);
        return self;
    }

    pub fn is_right_symbol_that(self, f: fn(SymbolAssertThat) -> ()) -> BinaryAssertThat<'a> {
        let handler = assert_expression_that(&self.value.right)
            .as_symbol();
        f(handler);
        return self;
    }
}
