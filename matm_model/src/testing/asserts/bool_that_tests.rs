#[cfg(test)]

mod tests {
    use crate::expressions::bool::BoolData;
    use crate::testing::asserts::bool_that::BoolAssertThat;

    #[test]
    fn test_is_true() {
        let obj = BoolAssertThat {
            value: &BoolData::new(true),
        };

        obj.is_true();
    }

    #[test]
    #[should_panic(expected = "Expected true, actual is false")]
    fn test_fail_is_true() {
        let obj = BoolAssertThat {
            value: &BoolData::new(false),
        };

        obj.is_true();
    }

    #[test]
    fn test_is_false() {
        let obj = BoolAssertThat {
            value: &BoolData::new(false),
        };

        obj.is_false();
    }

    #[test]
    #[should_panic(expected = "Expected false, actual is true")]
    fn test_fail_is_false() {
        let obj = BoolAssertThat {
            value: &BoolData::new(true),
        };

        obj.is_false();
    }

    #[test]
    fn test_is_equal_to() {
        let obj = BoolAssertThat {
            value: &BoolData::new(true),
        };

        obj.is_equal_to(true);
    }

    #[test]
    #[should_panic(expected = "Expected true, actual is false")]
    fn test_fail_is_equal_to() {
        let obj = BoolAssertThat {
            value: &BoolData::new(false),
        };

        obj.is_equal_to(true);
    }
}
