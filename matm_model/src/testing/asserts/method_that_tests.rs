#[cfg(test)]

mod tests {
    use crate::expressions::method::MethodData;
    use crate::factories::ObjFactory;
    use crate::testing::asserts::method_that::MethodAssertThat;

    #[test]
    fn test_has_name() {
        let fa = ObjFactory::new10();
        let obj = MethodAssertThat {
            method: &MethodData::new(
                fa.rz1(),
                String::from("foo"),
                vec![]),
        };

        obj.has_name("foo");
    }

    #[test]
    #[should_panic(expected = "Expected method foo, actual is sin")]
    fn test_fail_has_name() {
        let fa = ObjFactory::new10();
        let obj = MethodAssertThat {
            method: &MethodData::new(
                fa.rz1(),
                String::from("sin"),
                vec![]),
        };

        obj.has_name("foo");
    }

    #[test]
    fn test_has_object_that() {
        let fa = ObjFactory::new10();
        let obj = MethodAssertThat {
            method: &MethodData::new(
                fa.rz1(),
                String::from("foo"),
                vec![]),
        };

        obj.has_object_that(|e| { e.as_integer().is_int(1); } );
    }

    #[test]
    #[should_panic(expected = "Expected integer, actual is symbol: a")]
    fn test_fail_has_object_that() {
        let fa = ObjFactory::new10();
        let obj = MethodAssertThat {
            method: &MethodData::new(
                fa.rsn("a"),
                String::from("foo"),
                vec![]),
        };

        obj.has_object_that(|e| { e.as_integer(); });
    }

    #[test]
    fn test_has_arguments_size() {
        let fa = ObjFactory::new10();
        let obj = MethodAssertThat {
            method: &MethodData::new(
                fa.rz1(),
                String::from("foo"),
                vec![fa.rz0(), fa.rz0()]),
        };

        obj.has_arguments_size(2);
    }

    #[test]
    #[should_panic(expected = "Expected number of method arguments to be 2, actual is 1")]
    fn test_fail_has_arguments_size() {
        let fa = ObjFactory::new10();
        let obj = MethodAssertThat {
            method: &MethodData::new(
                fa.rz1(),
                String::from("foo"),
                vec![fa.rz0()]),
        };

        obj.has_arguments_size(2);
    }

    #[test]
    fn test_has_argument_i_that() {
        let fa = ObjFactory::new10();
        let obj = MethodAssertThat {
            method: &MethodData::new(
                fa.rz1(),
                String::from("foo"),
                vec![fa.rzi_u(1), fa.rzi_u(2)]),
        };

        obj.has_argument_i_that(1, |e| {e.as_integer().is_int(2); });
    }

    #[test]
    #[should_panic(expected = "Expected method argument index 2 to be present, maximum index 1 is allowed")]
    fn test_fail_has_argument_i_that_unbounded() {
        let fa = ObjFactory::new10();
        let obj = MethodAssertThat {
            method: &MethodData::new(
                fa.rz1(),
                String::from("foo"),
                vec![fa.rzi_u(1), fa.rsn("a")]),
        };

        obj.has_argument_i_that(2, |_| ());
    }

    #[test]
    #[should_panic(expected = "Expected integer, actual is symbol")]
    fn test_fail_has_argument_i_that() {
        let fa = ObjFactory::new10();
        let obj = MethodAssertThat {
            method: &MethodData::new(
                fa.rz1(),
                String::from("foo"),
                vec![fa.rzi_u(1), fa.rsn("a")]),
        };

        obj.has_argument_i_that(1, |e| {e.as_integer(); });
    }
}
