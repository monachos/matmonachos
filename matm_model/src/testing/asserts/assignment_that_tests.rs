#[cfg(test)]

mod tests {
    use crate::expressions::assignment::AssignmentData;
    use crate::factories::ObjFactory;
    use crate::testing::asserts::assignment_that::AssignmentAssertThat;

    #[test]
    fn test_has_source_that() {
        let fa = ObjFactory::new10();
        let obj = AssignmentAssertThat {
            value: &AssignmentData::new(fa.rsn("x"), fa.rz1()),
        };

        obj.has_source_that(|e| {
            e.as_integer()
                .is_int(1);
        });
    }

    #[test]
    #[should_panic(expected = "Expected integer, actual is symbol: a")]
    fn test_fail_has_source_that() {
        let fa = ObjFactory::new10();
        let obj = AssignmentAssertThat {
            value: &AssignmentData::new(fa.rsn("x"), fa.rsn("a")),
        };

        obj.has_source_that(|e| { e.as_integer(); });
    }

    #[test]
    fn test_is_source_integer_that() {
        let fa = ObjFactory::new10();
        let obj = AssignmentAssertThat {
            value: &AssignmentData::new(fa.rsn("x"), fa.rz1()),
        };

        obj.is_source_integer_that(|e| { e.is_int(1); });
    }

    #[test]
    fn test_is_source_fraction_that() {
        let fa = ObjFactory::new10();
        let obj = AssignmentAssertThat {
            value: &AssignmentData::new(
                fa.rsn("x"),
                fa.rfi_u(1, 2)),
        };

        obj.is_source_fraction_that(|e| {
            e.has_numerator_int(1)
                .has_denominator_int(2);
        });
    }

    #[test]
    fn test_is_source_symbol_that() {
        let fa = ObjFactory::new10();
        let obj = AssignmentAssertThat {
            value: &AssignmentData::new(
                fa.rsn("x"),
                fa.rsn("a")),
        };

        obj.is_source_symbol_that(|e| { e.has_name("a"); });
    }

    #[test]
    fn test_has_target_that() {
        let fa = ObjFactory::new10();
        let obj = AssignmentAssertThat {
            value: &AssignmentData::new(fa.rsn("x"), fa.rz1()),
        };

        obj.has_target_that(|e| {
            e.as_symbol()
                .has_name("x");
        });
    }

    #[test]
    #[should_panic(expected = "Expected symbol, actual is integer: 1")]
    fn test_fail_has_target_that() {
        let fa = ObjFactory::new10();
        let obj = AssignmentAssertThat {
            value: &AssignmentData::new(fa.rz1(), fa.rsn("a")),
        };

        obj.has_target_that(|e| { e.as_symbol(); });
    }

    #[test]
    fn test_is_target_symbol_that() {
        let fa = ObjFactory::new10();
        let obj = AssignmentAssertThat {
            value: &AssignmentData::new(fa.rsn("x"), fa.rz1()),
        };

        obj.is_target_symbol_that(|e| {
            e.has_name("x");
        });
    }
}
