use crate::testing::asserts::assert_expression_that;
use crate::testing::asserts::int_that::IntegerAssertThat;
use crate::expressions::root::RootData;

use super::expr_that::ExpressionAssertThat;


pub struct RootAssertThat<'a> {
    pub value: &'a RootData,
}

impl <'a> RootAssertThat<'a> {
    pub fn is_radicand_integer_that(self, f: fn(IntegerAssertThat) -> ()) -> RootAssertThat<'a> {
        let handler = assert_expression_that(&self.value.radicand)
            .as_integer();
        f(handler);
        return self;
    }

    pub fn is_radicand_i32(self, expected: i32) -> RootAssertThat<'a> {
        assert_expression_that(&self.value.radicand)
            .as_integer()
            .is_int(expected);
        return self;
    }

    pub fn has_radicand_format(self, expected: &str) -> RootAssertThat<'a> {
        let actual = self.value.radicand.to_string();
        assert_eq!(expected, actual,
                   "Expected radicand format {}, actual is {}",
                   expected,
                   actual);
        return self;
    }

    pub fn has_radicand_that(self, f: fn(ExpressionAssertThat) -> ()) -> RootAssertThat<'a> {
        let handler = assert_expression_that(&self.value.radicand);
        f(handler);
        return self;
    }

    pub fn is_degree_integer_that(self, f: fn(IntegerAssertThat) -> ()) -> RootAssertThat<'a> {
        let handler = assert_expression_that(&self.value.degree)
            .as_integer();
        f(handler);
        return self;
    }

    pub fn is_degree_i32(self, expected: i32) -> RootAssertThat<'a> {
        assert_expression_that(&self.value.degree)
            .as_integer()
            .is_int(expected);
        return self;
    }

    pub fn has_degree_format(self, expected: &str) -> RootAssertThat<'a> {
        let actual = self.value.degree.to_string();
        assert_eq!(expected, actual,
                   "Expected degree format {}, actual is {}",
                   expected,
                   actual);
        return self;
    }

    pub fn has_degree_that(self, f: fn(ExpressionAssertThat) -> ()) -> RootAssertThat<'a> {
        let handler = assert_expression_that(&self.value.degree);
        f(handler);
        return self;
    }

    pub fn is_sqrt_i32(self, expected: i32) -> RootAssertThat<'a> {
        assert_expression_that(&self.value.radicand)
            .as_integer()
            .is_int(expected);
        assert_expression_that(&self.value.degree)
            .as_integer()
            .is_int(2);
        return self;
    }
}

