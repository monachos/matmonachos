#[cfg(test)]

mod tests {
    use crate::expressions::equation::EquationData;
    use crate::expressions::soe::SysOfEqData;
    use crate::expressions::ToExpression;
    use crate::factories::ObjFactory;
    use crate::testing::asserts::soe_that::SystemOfEquationsAssertThat;

    #[test]
    fn test_has_equations_size() {
        let fa = ObjFactory::new10();
        let obj = SystemOfEquationsAssertThat {
            system: &SysOfEqData::from_vector(
                vec![
                    EquationData::new(fa.rsn("a"), fa.rzi_u(3)).to_rc_expr()
                ])
        };

        obj.has_equations_size(1);
    }

    #[test]
    #[should_panic(expected = "Expected number of equations to be 2, actual is 1")]
    fn test_fail_has_equations_size() {
        let fa = ObjFactory::new10();
        let obj = SystemOfEquationsAssertThat {
            system: &SysOfEqData::from_vector(
                vec![
                    EquationData::new(fa.rsn("a"), fa.rzi_u(3)).to_rc_expr()
                ])
        };

        obj.has_equations_size(2);
    }

    #[test]
    fn test_has_equation_i_that() {
        let fa = ObjFactory::new10();
        let obj = SystemOfEquationsAssertThat {
            system: &SysOfEqData::from_vector(
                vec![
                    EquationData::new(fa.rsn("a"), fa.rzi_u(3)).to_rc_expr()
                ])
        };

        obj.has_equation_i_that(
            0,
            |e| {
                e.as_equation().is_left_symbol_that(|s| { s.has_name("a"); });
            });
    }

    #[test]
    #[should_panic(expected = "Expected equation index 2 to be present, maximum index 0 is allowed")]
    fn test_fail_has_equation_i_that_unbounded() {
        let fa = ObjFactory::new10();
        let obj = SystemOfEquationsAssertThat {
            system: &SysOfEqData::from_vector(
                vec![
                    EquationData::new(fa.rsn("a"), fa.rzi_u(3)).to_rc_expr()
                ])
        };

        obj.has_equation_i_that(2, |_| ());
    }

    #[test]
    #[should_panic(expected = "Expected integer, actual is equation")]
    fn test_fail_has_equation_i_that() {
        let fa = ObjFactory::new10();
        let obj = SystemOfEquationsAssertThat {
            system: &SysOfEqData::from_vector(
                vec![
                    EquationData::new(fa.rsn("a"), fa.rzi_u(3)).to_rc_expr()
                ])
        };

        obj.has_equation_i_that(0, |e| {e.as_integer(); });
    }
}
