use crate::testing::asserts::assert_expression_that;
use crate::testing::asserts::int_that::IntegerAssertThat;
use crate::testing::asserts::frac_that::FractionAssertThat;
use crate::expressions::matrices::MatrixData;

use super::symbol_that::SymbolAssertThat;


pub struct MatrixAssertThat<'a> {
    pub matrix: &'a MatrixData,
}

impl <'a> MatrixAssertThat<'a> {
    pub fn has_rows(self, expected: usize) -> MatrixAssertThat<'a> {
        assert_eq!(expected, self.matrix.rows(),
                   "Expected number of rows to be {}, actual is {}",
                   expected,
                   self.matrix.rows());
        return self;
    }

    pub fn has_columns(self, expected: usize) -> MatrixAssertThat<'a> {
        assert_eq!(expected, self.matrix.columns(),
                   "Expected number of columns to be {}, actual is {}",
                   expected,
                   self.matrix.columns());
        return self;
    }

    pub fn is_equal_to(self, expected: MatrixData) -> MatrixAssertThat<'a> {
        assert_eq!(expected, *self.matrix,
                   "Expected matrix to be {}, actual is {}",
                   expected,
                   self.matrix);
        return self;
    }

    pub fn has_i_i_format(self, row: usize, col: usize, expected: &str) -> MatrixAssertThat<'a> {
        let actual = self.matrix.get_i_i(row, col).unwrap().to_string();
        assert_eq!(expected, actual,
                   "Expected format {}, actual is {}",
                   expected,
                   actual);
        return self;
    }

    pub fn is_i_i_integer_that(self, row: usize, col: usize, f: fn(IntegerAssertThat) -> ()) -> MatrixAssertThat<'a> {
        let element = self.matrix.get_i_i(row, col).unwrap();
        let handler = assert_expression_that(element).as_integer();
        f(handler);
        return self;
    }

    pub fn is_i_i_fraction_that(self, row: usize, col: usize, f: fn(FractionAssertThat) -> ()) -> MatrixAssertThat<'a> {
        let element = self.matrix.get_i_i(row, col).unwrap();
        let handler = assert_expression_that(element).as_fraction();
        f(handler);
        return self;
    }

    pub fn is_i_i_symbol_that(self, row: usize, col: usize, f: fn(SymbolAssertThat) -> ()) -> MatrixAssertThat<'a> {
        let element = self.matrix.get_i_i(row, col).unwrap();
        let handler = assert_expression_that(element).as_symbol();
        f(handler);
        return self;
    }

    pub fn is_i_i_boxsign(self, row: usize, col: usize) -> MatrixAssertThat<'a> {
        let element = self.matrix.get_i_i(row, col).unwrap();
        assert_expression_that(element).is_boxsign();
        return self;
    }

    pub fn has_i_i_zero(self, row: usize, col: usize) -> MatrixAssertThat<'a> {
        return self.is_i_i_integer_that(row, col, |e| {e.is_zero();});
    }

    pub fn has_i_i_one(self, row: usize, col: usize) -> MatrixAssertThat<'a> {
        return self.is_i_i_integer_that(row, col, |e| {e.is_int(1);});
    }
}

