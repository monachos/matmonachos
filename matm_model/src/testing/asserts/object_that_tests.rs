#[cfg(test)]

mod tests {
    use crate::expressions::object::{ObjectData, ObjectType};
    use crate::testing::asserts::object_that::ObjectAssertThat;

    #[test]
    fn test_is_type() {
        let obj = ObjectAssertThat {
            object: &ObjectData::new(ObjectType::Matrix),
        };

        obj.is_type(ObjectType::Matrix);
    }

    #[test]
    #[should_panic(expected = "Expected object type to be cracovian, actual is matrix")]
    fn test_fail_is_type() {
        let obj = ObjectAssertThat {
            object: &ObjectData::new(ObjectType::Matrix),
        };

        obj.is_type(ObjectType::Cracovian);
    }
}
