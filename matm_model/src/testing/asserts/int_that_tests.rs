#[cfg(test)]

mod tests {
    use crate::core::natural::RADIX_10;
    use crate::core::natural::RADIX_16;
    use crate::expressions::int::IntData;
    use crate::testing::asserts::int_that::IntegerAssertThat;

    #[test]
    fn test_has_radix() {
        let obj = IntegerAssertThat {
            value: &IntData::one(RADIX_16).unwrap(),
        };

        obj.has_radix(RADIX_16);
    }

    #[test]
    #[should_panic(expected = "Expected integer of radix 12, actual is 16")]
    fn test_fail_has_radix() {
        let obj = IntegerAssertThat {
            value: &IntData::one(RADIX_16).unwrap(),
        };

        obj.has_radix(12);
    }

    #[test]
    fn test_is_int() {
        let obj = IntegerAssertThat {
            value: &IntData::one(RADIX_10).unwrap(),
        };

        obj.is_int(1);
    }

    #[test]
    #[should_panic(expected = "Expected integer 2, actual is 1")]
    fn test_fail_is_int() {
        let obj = IntegerAssertThat {
            value: &IntData::one(RADIX_10).unwrap(),
        };

        obj.is_int(2);
    }

    #[test]
    fn test_is_positive() {
        let obj = IntegerAssertThat {
            value: &IntData::one(RADIX_10).unwrap(),
        };

        obj.is_positive();
    }

    #[test]
    #[should_panic(expected = "Expected positive integer")]
    fn test_fail_is_positive() {
        let obj = IntegerAssertThat {
            value: &IntData::from_int(RADIX_10, -1).unwrap(),
        };

        obj.is_positive();
    }

    #[test]
    fn test_is_negative() {
        let obj = IntegerAssertThat {
            value: &IntData::from_int(RADIX_10, -1).unwrap(),
        };

        obj.is_negative();
    }

    #[test]
    #[should_panic(expected = "Expected negative integer")]
    fn test_fail_is_negative() {
        let obj = IntegerAssertThat {
            value: &IntData::from_int(RADIX_10, 1).unwrap(),
        };

        obj.is_negative();
    }

    #[test]
    fn test_is_zero() {
        let obj = IntegerAssertThat {
            value: &IntData::zero(RADIX_10).unwrap(),
        };

        obj.is_zero();
    }

    #[test]
    #[should_panic(expected = "Expected zero integer")]
    fn test_fail_is_zero() {
        let obj = IntegerAssertThat {
            value: &IntData::one(RADIX_10).unwrap(),
        };

        obj.is_zero();
    }

    #[test]
    fn test_is_equal_to() {
        let obj = IntegerAssertThat {
            value: &IntData::one(RADIX_10).unwrap(),
        };

        obj.is_equal_to(IntData::one(RADIX_10).unwrap());
    }

    #[test]
    #[should_panic(expected = "Expected integer 1, actual is 2")]
    fn test_fail_is_equal_to() {
        let obj = IntegerAssertThat {
            value: &IntData::from_int(RADIX_10, 2).unwrap(),
        };

        obj.is_equal_to(IntData::one(RADIX_10).unwrap());
    }
}
