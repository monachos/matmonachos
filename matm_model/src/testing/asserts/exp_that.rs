use crate::testing::asserts::assert_expression_that;
use crate::testing::asserts::int_that::IntegerAssertThat;
use crate::expressions::exponentiation::ExpData;

use super::expr_that::ExpressionAssertThat;


pub struct ExpAssertThat<'a> {
    pub value: &'a ExpData,
}

impl <'a> ExpAssertThat<'a> {
    pub fn has_base_that(self, f: fn(ExpressionAssertThat) -> ()) -> ExpAssertThat<'a> {
        let handler = assert_expression_that(&self.value.base);
        f(handler);
        return self;
    }

    pub fn is_base_integer_that(self, f: fn(IntegerAssertThat) -> ()) -> ExpAssertThat<'a> {
        let handler = assert_expression_that(&self.value.base)
            .as_integer();
        f(handler);
        return self;
    }

    pub fn has_exponent_that(self, f: fn(ExpressionAssertThat) -> ()) -> ExpAssertThat<'a> {
        let handler = assert_expression_that(&self.value.exponent);
        f(handler);
        return self;
    }

    pub fn is_exponent_integer_that(self, f: fn(IntegerAssertThat) -> ()) -> ExpAssertThat<'a> {
        let handler = assert_expression_that(&self.value.exponent)
            .as_integer();
        f(handler);
        return self;
    }
}

