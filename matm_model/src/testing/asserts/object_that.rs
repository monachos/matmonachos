use crate::expressions::object::ObjectData;
use crate::expressions::object::ObjectType;

pub struct ObjectAssertThat<'a> {
    pub object: &'a ObjectData,
}

impl<'a> ObjectAssertThat<'a> {
    pub fn is_type(self, exp_type: ObjectType) -> ObjectAssertThat<'a> {
        assert_eq!(exp_type, self.object.object_type,
                   "Expected object type to be {}, actual is {}",
                   exp_type,
                   self.object.object_type);
        return self;
    }
}
