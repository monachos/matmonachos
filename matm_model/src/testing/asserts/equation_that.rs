use crate::expressions::equation::EquationData;
use crate::testing::asserts::assert_expression_that;
use crate::testing::asserts::symbol_that::SymbolAssertThat;

use super::expr_that::ExpressionAssertThat;


pub struct EquationAssertThat<'a> {
    pub value: &'a EquationData,
}

impl <'a> EquationAssertThat<'a> {
    pub fn has_left_that(self, f: fn(ExpressionAssertThat) -> ()) -> EquationAssertThat<'a> {
        let handler = assert_expression_that(&self.value.left);
        f(handler);
        return self;
    }

    pub fn has_right_that(self, f: fn(ExpressionAssertThat) -> ()) -> EquationAssertThat<'a> {
        let handler = assert_expression_that(&self.value.right);
        f(handler);
        return self;
    }

    pub fn is_left_symbol_that(self, f: fn(SymbolAssertThat) -> ()) -> EquationAssertThat<'a> {
        let handler = assert_expression_that(&self.value.left)
            .as_symbol();
        f(handler);
        return self;
    }

    pub fn is_right_symbol_that(self, f: fn(SymbolAssertThat) -> ()) -> EquationAssertThat<'a> {
        let handler = assert_expression_that(&self.value.right)
            .as_symbol();
        f(handler);
        return self;
    }
}

