use crate::expressions::transcendental::TranscData;
use crate::expressions::Expression;
use crate::testing::asserts::exp_that::ExpAssertThat;
use crate::testing::asserts::int_that::IntegerAssertThat;
use crate::testing::asserts::frac_that::FractionAssertThat;
use crate::testing::asserts::symbol_that::SymbolAssertThat;
use crate::testing::asserts::matrix_that::MatrixAssertThat;
use crate::testing::asserts::unary_that::UnaryAssertThat;
use crate::testing::asserts::binary_that::BinaryAssertThat;
use crate::testing::asserts::root_that::RootAssertThat;
use crate::testing::asserts::assert_matrix_that;
use crate::testing::asserts::assert_cracovian_that;
use crate::testing::asserts::assignment_that::AssignmentAssertThat;
use crate::testing::asserts::binomial_that::BinomialAssertThat;
use crate::testing::asserts::bool_that::BoolAssertThat;
use crate::testing::asserts::conditional_that::ConditionalAssertThat;
use crate::testing::asserts::cracovian_that::CracovianAssertThat;
use crate::testing::asserts::soe_that::SystemOfEquationsAssertThat;
use crate::testing::asserts::equation_that::EquationAssertThat;
use crate::testing::asserts::func_that::FunctionAssertThat;
use crate::testing::asserts::log_that::LogAssertThat;
use crate::testing::asserts::method_that::MethodAssertThat;
use crate::testing::asserts::object_that::ObjectAssertThat;


pub struct ExpressionAssertThat<'a> {
    pub expression: &'a Expression,
}

impl <'a> ExpressionAssertThat<'a> {
    pub fn is_undefined(self) -> ExpressionAssertThat<'a> {
        match &self.expression {
            Expression::Undefined => {
                return self;
            }
            _ => {
                println!("Expected undefined, actual is {}: {}", self.expression.type_name(), self.expression);
                assert!(false, "Expected undefined, actual is {}: {}", self.expression.type_name(), self.expression);
                unreachable!();
            }
        }
    }

    pub fn is_equal_to(self, expected: &Expression) -> ExpressionAssertThat<'a> {
        assert_eq!(expected, self.expression,
                   "Expected {} {}, actual is {} {}",
                   expected.type_name(),
                   expected,
                   self.expression.type_name(),
                   self.expression);
        return self;
    }

    pub fn as_assignment(self) -> AssignmentAssertThat<'a> {
        match &self.expression {
            Expression::Assignment(value) => {
                return AssignmentAssertThat {
                    value: value,
                };
            }
            _ => {
                println!("Expected assignment, actual is {}: {}", self.expression.type_name(), self.expression);
                assert!(false, "Expected assignment, actual is {}: {}", self.expression.type_name(), self.expression);
                unreachable!();
            }
        }
    }

    pub fn is_pi(self) -> ExpressionAssertThat<'a> {
        if let Expression::Transc(value) = &self.expression {
            if *value.as_ref() == TranscData::Pi {
                return self;
            }
        }
        println!("Expected pi, actual is {}: {}", self.expression.type_name(), self.expression);
        assert!(false, "Expected pi, actual is {}: {}", self.expression.type_name(), self.expression);
        unreachable!();
    }

    pub fn is_e(self) -> ExpressionAssertThat<'a> {
        if let Expression::Transc(value) = &self.expression {
            if *value.as_ref() == TranscData::Euler {
                return self;
            }
        }
        println!("Expected e, actual is {}: {}", self.expression.type_name(), self.expression);
        assert!(false, "Expected e, actual is {}: {}", self.expression.type_name(), self.expression);
        unreachable!();
    }

    pub fn as_integer(self) -> IntegerAssertThat<'a> {
        match &self.expression {
            Expression::Int(value) => {
                return IntegerAssertThat {
                    value: value,
                };
            }
            _ => {
                println!("Expected integer, actual is {}: {}", self.expression.type_name(), self.expression);
                assert!(false, "Expected integer, actual is {}: {}", self.expression.type_name(), self.expression);
                unreachable!();
            }
        }
    }

    pub fn as_bool(self) -> BoolAssertThat<'a> {
        match &self.expression {
            Expression::Bool(value) => {
                return BoolAssertThat {
                    value: value,
                };
            }
            _ => {
                println!("Expected bool, actual is {}: {}", self.expression.type_name(), self.expression);
                assert!(false, "Expected bool, actual is {}: {}", self.expression.type_name(), self.expression);
                unreachable!();
            }
        }
    }

    pub fn as_fraction(self) -> FractionAssertThat<'a> {
        match &self.expression {
            Expression::Frac(frac) => {
                return FractionAssertThat {
                    frac: frac,
                };
            }
            _ => {
                println!("Expected fraction, actual is {}: {}", self.expression.type_name(), self.expression);
                assert!(false, "Expected fraction, actual is {}: {}", self.expression.type_name(), self.expression);
                unreachable!();
            }
        }
    }

    pub fn as_symbol(self) -> SymbolAssertThat<'a> {
        match &self.expression {
            Expression::Symbol(obj) => {
                return SymbolAssertThat {
                    symbol: obj,
                };
            }
            _ => {
                println!("Expected symbol, actual is {}: {}", self.expression.type_name(), self.expression);
                assert!(false, "Expected symbol, actual is {}: {}", self.expression.type_name(), self.expression);
                unreachable!();
            }
        }
    }

    pub fn is_boxsign(self) {
        match &self.expression {
            Expression::BoxSign => {}
            _ => {
                println!("Expected box, actual is {}: {}", self.expression.type_name(), self.expression);
                assert!(false, "Expected box, actual is {}: {}", self.expression.type_name(), self.expression);
                unreachable!();
            }
        }
    }

    pub fn as_matrix(self) -> MatrixAssertThat<'a> {
        match &self.expression {
            Expression::Matrix(mx) => {
                return assert_matrix_that(mx);
            }
            _ => {
                println!("Expected matrix, actual is {}: {}", self.expression.type_name(), self.expression);
                assert!(false, "Expected matrix, actual is {}: {}", self.expression.type_name(), self.expression);
                unreachable!();
            }
        }
    }

    pub fn as_cracovian(self) -> CracovianAssertThat<'a> {
        match &self.expression {
            Expression::Cracovian(crac) => {
                return assert_cracovian_that(crac);
            }
            _ => {
                println!("Expected cracovian, actual is {}: {}", self.expression.type_name(), self.expression);
                assert!(false, "Expected cracovian, actual is {}: {}", self.expression.type_name(), self.expression);
                unreachable!();
            }
        }
    }

    pub fn as_unary(self) -> UnaryAssertThat<'a> {
        match &self.expression {
            Expression::Unary(value) => {
                return UnaryAssertThat {
                    value: value,
                };
            }
            _ => {
                println!("Expected unary, actual is {}: {}", self.expression.type_name(), self.expression);
                assert!(false, "Expected unary, actual is {}: {}", self.expression.type_name(), self.expression);
                unreachable!();
            }
        }
    }

    pub fn as_binary(self) -> BinaryAssertThat<'a> {
        match &self.expression {
            Expression::Binary(value) => {
                return BinaryAssertThat {
                    value: value,
                };
            }
            _ => {
                println!("Expected binary, actual is {}: {}", self.expression.type_name(), self.expression);
                assert!(false, "Expected binary, actual is {}: {}", self.expression.type_name(), self.expression);
                unreachable!();
            }
        }
    }

    pub fn as_binomial(self) -> BinomialAssertThat<'a> {
        match &self.expression {
            Expression::Binomial(value) => {
                return BinomialAssertThat {
                    value: value,
                };
            }
            _ => {
                println!("Expected binomial, actual is {}: {}", self.expression.type_name(), self.expression);
                assert!(false, "Expected binomial, actual is {}: {}", self.expression.type_name(), self.expression);
                unreachable!();
            }
        }
    }

    pub fn as_root(self) -> RootAssertThat<'a> {
        match &self.expression {
            Expression::Root(value) => {
                return RootAssertThat {
                    value: value,
                };
            }
            _ => {
                println!("Expected root, actual is {}: {}", self.expression.type_name(), self.expression);
                assert!(false, "Expected root, actual is {}: {}", self.expression.type_name(), self.expression);
                unreachable!();
            }
        }
    }

    pub fn as_log(self) -> LogAssertThat<'a> {
        match &self.expression {
            Expression::Log(value) => {
                return LogAssertThat {
                    value: value,
                };
            }
            _ => {
                println!("Expected log, actual is {}: {}", self.expression.type_name(), self.expression);
                assert!(false, "Expected log, actual is {}: {}", self.expression.type_name(), self.expression);
                unreachable!();
            }
        }
    }

    pub fn as_equation(self) -> EquationAssertThat<'a> {
        match &self.expression {
            Expression::Equation(value) => {
                return EquationAssertThat {
                    value,
                };
            }
            _ => {
                println!("Expected equation, actual is {}: {}", self.expression.type_name(), self.expression);
                assert!(false, "Expected equation, actual is {}: {}", self.expression.type_name(), self.expression);
                unreachable!();
            }
        }
    }

    pub fn as_soe(self) -> SystemOfEquationsAssertThat<'a> {
        match &self.expression {
            Expression::SysOfEq(value) => {
                return SystemOfEquationsAssertThat {
                    system: value,
                };
            }
            _ => {
                println!("Expected equation system, actual is {}: {}", self.expression.type_name(), self.expression);
                assert!(false, "Expected equation system, actual is {}: {}", self.expression.type_name(), self.expression);
                unreachable!();
            }
        }
    }

    pub fn as_exponent(self) -> ExpAssertThat<'a> {
        match &self.expression {
            Expression::Exp(value) => {
                return ExpAssertThat {
                    value: value,
                };
            }
            _ => {
                println!("Expected exponent, actual is {}: {}", self.expression.type_name(), self.expression);
                assert!(false, "Expected exponent, actual is {}: {}", self.expression.type_name(), self.expression);
                unreachable!();
            }
        }
    }

    pub fn as_function(self) -> FunctionAssertThat<'a> {
        match &self.expression {
            Expression::Function(func) => {
                return FunctionAssertThat {
                    func: func,
                };
            }
            _ => {
                println!("Expected function, actual is {}: {}", self.expression.type_name(), self.expression);
                assert!(false, "Expected function, actual is {}: {}", self.expression.type_name(), self.expression);
                unreachable!();
            }
        }
    }

    pub fn as_method(self) -> MethodAssertThat<'a> {
        match &self.expression {
            Expression::Method(method) => {
                return MethodAssertThat {
                    method: method,
                };
            }
            _ => {
                println!("Expected method, actual is {}: {}", self.expression.type_name(), self.expression);
                assert!(false, "Expected method, actual is {}: {}", self.expression.type_name(), self.expression);
                unreachable!();
            }
        }
    }

    pub fn as_object(self) -> ObjectAssertThat<'a> {
        match &self.expression {
            Expression::Object(obj) => {
                return ObjectAssertThat {
                    object: obj,
                };
            }
            _ => {
                println!("Expected object, actual is {}: {}", self.expression.type_name(), self.expression);
                assert!(false, "Expected object, actual is {}: {}", self.expression.type_name(), self.expression);
                unreachable!();
            }
        }
    }

    pub fn as_conditional(self) -> ConditionalAssertThat<'a> {
        match &self.expression {
            Expression::Conditional(value) => {
                return ConditionalAssertThat {
                    value: value,
                };
            }
            _ => {
                println!("Expected conditional, actual is {}: {}", self.expression.type_name(), self.expression);
                assert!(false, "Expected conditional, actual is {}: {}", self.expression.type_name(), self.expression);
                unreachable!();
            }
        }
    }
}
