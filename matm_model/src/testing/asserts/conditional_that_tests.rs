#[cfg(test)]

mod tests {
    use crate::expressions::arithmetic_condition::ArithmeticCondition;
    use crate::expressions::conditional::ConditionalData;
    use crate::expressions::conditional::ConditionalClause;
    use crate::expressions::condition::ToCondition;
    use crate::factories::ObjFactory;
    use crate::testing::asserts::conditional_that::ConditionalAssertThat;

    fn sample_condition(fa: &ObjFactory) -> ConditionalData {
        ConditionalData::from_vector(
            vec![
                ConditionalClause {
                    expression: fa.rzi_u(1),
                    condition: ArithmeticCondition::gt(
                        fa.rsn("x"),
                        fa.rz0()
                    ).to_rc_cond()
                },
                ConditionalClause {
                    expression: fa.rz0(),
                    condition: ArithmeticCondition::eq(
                        fa.rsn("x"),
                        fa.rz0()
                    ).to_rc_cond()
                },
            ]
        )
    }

    #[test]
    fn test_has_items_size() {
        let fa = ObjFactory::new10();
        let obj = ConditionalAssertThat {
            value: &sample_condition(&fa),
        };

        obj.has_items_size(2);
    }

    #[test]
    #[should_panic(expected = "Expected number of conditions to be 1, actual is 2")]
    fn test_fail_has_items_size() {
        let fa = ObjFactory::new10();
        let obj = ConditionalAssertThat {
            value: &sample_condition(&fa),
        };

        obj.has_items_size(1);
    }

    #[test]
    fn test_has_i_expression_format() {
        let fa = ObjFactory::new10();
        let obj = ConditionalAssertThat {
            value: &sample_condition(&fa),
        };

        obj.has_i_expression_format(0, "1");
    }

    #[test]
    #[should_panic(expected = "Expected expression format abc, actual is 1")]
    fn test_fail_has_i_expression_format() {
        let fa = ObjFactory::new10();
        let obj = ConditionalAssertThat {
            value: &sample_condition(&fa),
        };

        obj.has_i_expression_format(0, "abc");
    }

    #[test]
    fn test_has_i_condition_format() {
        let fa = ObjFactory::new10();
        let obj = ConditionalAssertThat {
            value: &sample_condition(&fa),
        };

        obj.has_i_condition_format(0, "x > 0");
    }

    #[test]
    #[should_panic(expected = "Expected condition format abc, actual is x > 0")]
    fn test_fail_has_i_condition_format() {
        let fa = ObjFactory::new10();
        let obj = ConditionalAssertThat {
            value: &sample_condition(&fa),
        };

        obj.has_i_condition_format(0, "abc");
    }

    #[test]
    fn test_has_i_expression_that() {
        let fa = ObjFactory::new10();
        let obj = ConditionalAssertThat {
            value: &sample_condition(&fa),
        };

        obj.has_i_expression_that(0, |e| { e.as_integer().is_int(1); });
    }

    #[test]
    #[should_panic(expected = "Expected condition index 2 to be present, maximum index 1 is allowed")]
    fn test_fail_has_i_expression_that() {
        let fa = ObjFactory::new10();
        let obj = ConditionalAssertThat {
            value: &sample_condition(&fa),
        };

        obj.has_i_expression_that(2, |_| ());
    }
}
