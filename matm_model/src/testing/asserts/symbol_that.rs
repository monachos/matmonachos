use crate::expressions::{symbol::SymbolData, Expression};


pub struct SymbolAssertThat<'a> {
    pub symbol: &'a SymbolData,
}

impl <'a> SymbolAssertThat<'a> {
    pub fn has_name(self, expected: &str) -> SymbolAssertThat<'a> {
        assert_eq!(expected, self.symbol.name,
                   "Expected symbol name to be {}, actual is {}",
                   expected,
                   self.symbol.name);
        return self;
    }

    pub fn has_indices_equal_to(self, expected: Vec<Expression>) -> SymbolAssertThat<'a> {
        let actual: Vec<Expression> = self.symbol.indices.iter().map(|e| e.as_ref().clone()).collect();
        assert_eq!(expected, actual);
        return self;
    }
}
