#[cfg(test)]

mod tests {
    use crate::expressions::equation::EquationData;
    use crate::factories::ObjFactory;
    use crate::testing::asserts::equation_that::EquationAssertThat;

    #[test]
    fn test_has_left_that() {
        let fa = ObjFactory::new10();
        let obj = EquationAssertThat {
            value: &EquationData::new(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.has_left_that(|e| { e.as_integer().is_int(2); } );
    }

    #[test]
    #[should_panic(expected = "Expected symbol, actual is integer: 2")]
    fn test_fail_has_left_that() {
        let fa = ObjFactory::new10();
        let obj = EquationAssertThat {
            value: &EquationData::new(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.has_left_that(|e| { e.as_symbol(); } );
    }

    #[test]
    fn test_has_right_that() {
        let fa = ObjFactory::new10();
        let obj = EquationAssertThat {
            value: &EquationData::new(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.has_right_that(|e| { e.as_integer().is_int(3); } );
    }

    #[test]
    #[should_panic(expected = "Expected symbol, actual is integer: 3")]
    fn test_fail_has_right_that() {
        let fa = ObjFactory::new10();
        let obj = EquationAssertThat {
            value: &EquationData::new(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.has_right_that(|e| { e.as_symbol(); } );
    }

    #[test]
    fn test_is_is_left_symbol_that() {
        let fa = ObjFactory::new10();
        let obj = EquationAssertThat {
            value: &EquationData::new(fa.rsn("a"), fa.rzi_u(5)),
        };

        obj.is_left_symbol_that(|e| { e.has_name("a"); } );
    }

    #[test]
    #[should_panic(expected = "Expected symbol, actual is integer: 2")]
    fn test_fail_is_left_symbol_that() {
        let fa = ObjFactory::new10();
        let obj = EquationAssertThat {
            value: &EquationData::new(fa.rzi_u(2), fa.rzi_u(5)),
        };

        obj.is_left_symbol_that(|_| ());
    }

    #[test]
    fn test_is_is_right_symbol_that() {
        let fa = ObjFactory::new10();
        let obj = EquationAssertThat {
            value: &EquationData::new(fa.rzi_u(2), fa.rsn("a")),
        };

        obj.is_right_symbol_that(|e| { e.has_name("a"); } );
    }

    #[test]
    #[should_panic(expected = "Expected symbol, actual is integer: 5")]
    fn test_fail_is_right_symbol_that() {
        let fa = ObjFactory::new10();
        let obj = EquationAssertThat {
            value: &EquationData::new(fa.rzi_u(2), fa.rzi_u(5)),
        };

        obj.is_right_symbol_that(|_| ());
    }
}
