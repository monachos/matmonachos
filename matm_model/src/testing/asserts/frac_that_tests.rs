#[cfg(test)]

mod tests {
    use crate::expressions::fractions::FracData;
    use crate::factories::ObjFactory;
    use crate::testing::asserts::frac_that::FractionAssertThat;

    #[test]
    fn test_has_numerator_int() {
        let fa = ObjFactory::new10();
        let obj = FractionAssertThat {
            frac: &FracData::new(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.has_numerator_int(2);
    }

    #[test]
    #[should_panic(expected = "Expected integer 10, actual is 2")]
    fn test_fail_has_numerator_int() {
        let fa = ObjFactory::new10();
        let obj = FractionAssertThat {
            frac: &FracData::new(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.has_numerator_int(10);
    }

    #[test]
    fn test_has_denominator_int() {
        let fa = ObjFactory::new10();
        let obj = FractionAssertThat {
            frac: &FracData::new(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.has_denominator_int(3);
    }

    #[test]
    #[should_panic(expected = "Expected integer 10, actual is 3")]
    fn test_fail_has_denominator_int() {
        let fa = ObjFactory::new10();
        let obj = FractionAssertThat {
            frac: &FracData::new(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.has_denominator_int(10);
    }

    #[test]
    fn test_is_ints() {
        let fa = ObjFactory::new10();
        let obj = FractionAssertThat {
            frac: &FracData::new(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.is_ints(2, 3);
    }

    #[test]
    #[should_panic(expected = "Expected integer 10, actual is 2")]
    fn test_fail_is_ints_numerator() {
        let fa = ObjFactory::new10();
        let obj = FractionAssertThat {
            frac: &FracData::new(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.is_ints(10, 3);
    }

    #[test]
    #[should_panic(expected = "Expected integer 10, actual is 3")]
    fn test_fail_is_ints_denominator() {
        let fa = ObjFactory::new10();
        let obj = FractionAssertThat {
            frac: &FracData::new(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.is_ints(2, 10);
    }

    #[test]
    fn test_has_numerator_format() {
        let fa = ObjFactory::new10();
        let obj = FractionAssertThat {
            frac: &FracData::new(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.has_numerator_format("2");
    }

    #[test]
    #[should_panic(expected = "Expected numerator 2, actual is a")]
    fn test_fail_has_numerator_format() {
        let fa = ObjFactory::new10();
        let obj = FractionAssertThat {
            frac: &FracData::new(fa.rsn("a"), fa.rzi_u(3)),
        };

        obj.has_numerator_format("2");
    }

    #[test]
    fn test_has_denominator_format() {
        let fa = ObjFactory::new10();
        let obj = FractionAssertThat {
            frac: &FracData::new(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.has_denominator_format("3");
    }

    #[test]
    #[should_panic(expected = "Expected denominator 3, actual is a")]
    fn test_fail_has_denominator_format() {
        let fa = ObjFactory::new10();
        let obj = FractionAssertThat {
            frac: &FracData::new(fa.rzi_u(2), fa.rsn("a")),
        };

        obj.has_denominator_format("3");
    }

    #[test]
    fn test_has_numerator_that() {
        let fa = ObjFactory::new10();
        let obj = FractionAssertThat {
            frac: &FracData::new(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.has_numerator_that(|e| { e.as_integer().is_int(2); } );
    }

    #[test]
    #[should_panic(expected = "Expected integer, actual is symbol: a")]
    fn test_fail_has_numerator_that() {
        let fa = ObjFactory::new10();
        let obj = FractionAssertThat {
            frac: &FracData::new(fa.rsn("a"), fa.rzi_u(3)),
        };

        obj.has_numerator_that(|e| { e.as_integer(); });
    }

    #[test]
    fn test_has_denominator_that() {
        let fa = ObjFactory::new10();
        let obj = FractionAssertThat {
            frac: &FracData::new(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.has_denominator_that(|e| { e.as_integer().is_int(3); } );
    }

    #[test]
    #[should_panic(expected = "Expected integer, actual is symbol: a")]
    fn test_fail_has_denominator_that() {
        let fa = ObjFactory::new10();
        let obj = FractionAssertThat {
            frac: &FracData::new(fa.rzi_u(2), fa.rsn("a")),
        };

        obj.has_denominator_that(|e| { e.as_integer(); });
    }
}
