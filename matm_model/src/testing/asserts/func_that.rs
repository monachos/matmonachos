use super::expr_that::ExpressionAssertThat;
use crate::expressions::func::FunctionData;
use crate::testing::asserts::assert_expression_that;

pub struct FunctionAssertThat<'a> {
    pub func: &'a FunctionData,
}

impl<'a> FunctionAssertThat<'a> {
    pub fn has_name(self, exp_name: &str) -> FunctionAssertThat<'a> {
        assert_eq!(exp_name, self.func.name,
                   "Expected function {}, actual is {}",
                   exp_name,
                   self.func.name);
        return self;
    }

    pub fn has_arguments_size(self, exp_len: usize) -> FunctionAssertThat<'a> {
        assert_eq!(exp_len, self.func.arguments.len(),
                   "Expected number of function arguments to be {}, actual is {}",
                   exp_len,
                   self.func.arguments.len());
        return self;
    }

    pub fn has_argument_i_that(self, index: usize, f: fn(ExpressionAssertThat) -> ()) -> FunctionAssertThat<'a> {
        assert!(index < self.func.arguments.len(),
                "Expected function argument index {} to be present, maximum index {} is allowed",
                index,
                self.func.arguments.len() - 1);
        let handler = assert_expression_that(&self.func.arguments[index]);
        f(handler);
        return self;
    }
}
