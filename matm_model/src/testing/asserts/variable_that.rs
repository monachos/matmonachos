use crate::expressions::variable::VariableData;


pub struct VariableAssertThat<'a> {
    pub variable: &'a VariableData,
}

impl <'a> VariableAssertThat<'a> {
    pub fn has_name(self, expected: &str) -> VariableAssertThat<'a> {
        assert_eq!(expected, self.variable.name.name,
                   "Expected variable name to be {}, actual is {}",
                   expected,
                   self.variable.name.name);
        return self;
    }
}
