use crate::expressions::bool::BoolData;


pub struct BoolAssertThat<'a> {
    pub value: &'a BoolData,
}

impl <'a> BoolAssertThat<'a> {
    pub fn is_true(self) -> BoolAssertThat<'a> {
        return self.is_equal_to(true);
    }

    pub fn is_false(self) -> BoolAssertThat<'a> {
        return self.is_equal_to(false);
    }

    pub fn is_equal_to(self, expected: bool) -> BoolAssertThat<'a> {
        assert_eq!(expected, self.value.value(),
                   "Expected {}, actual is {}",
                    expected,
                   self.value.value());
        return self;
    }
}

