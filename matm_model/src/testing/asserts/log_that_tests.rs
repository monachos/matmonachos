#[cfg(test)]

mod tests {
    use crate::expressions::logarithm::LogData;
    use crate::factories::ObjFactory;
    use crate::testing::asserts::log_that::LogAssertThat;

    #[test]
    fn test_is_base_integer_that() {
        let fa = ObjFactory::new10();
        let obj = LogAssertThat {
            value: &LogData::with_value_and_base(
                fa.rzi_u(3),
                fa.rzi_u(2),
            ),
        };

        obj.is_base_integer_that(|e| { e.is_int(2); } );
    }

    #[test]
    #[should_panic(expected = "Expected integer, actual is symbol: a")]
    fn test_fail_is_base_integer_that() {
        let fa = ObjFactory::new10();
        let obj = LogAssertThat {
            value: &LogData::with_value_and_base(
                fa.rzi_u(3),
                fa.rsn("a"),
            ),
        };

        obj.is_base_integer_that(|_| ());
    }

    #[test]
    fn test_has_base_that() {
        let fa = ObjFactory::new10();
        let obj = LogAssertThat {
            value: &LogData::with_value_and_base(
                fa.rzi_u(3),
                fa.rzi_u(2),
            ),
        };

        obj.has_base_that(|e| { e.as_integer().is_int(2); } );
    }

    #[test]
    #[should_panic(expected = "Expected integer, actual is symbol: a")]
    fn test_fail_has_base_that() {
        let fa = ObjFactory::new10();
        let obj = LogAssertThat {
            value: &LogData::with_value_and_base(
                fa.rzi_u(3),
                fa.rsn("a"),
            ),
        };

        obj.has_base_that(|e| { e.as_integer(); });
    }

    #[test]
    fn test_is_value_integer_that() {
        let fa = ObjFactory::new10();
        let obj = LogAssertThat {
            value: &LogData::with_value_and_base(
                fa.rzi_u(3),
                fa.rzi_u(2),
            ),
        };

        obj.is_value_integer_that(|e| { e.is_int(3); } );
    }

    #[test]
    #[should_panic(expected = "Expected integer, actual is symbol: a")]
    fn test_fail_is_value_integer_that() {
        let fa = ObjFactory::new10();
        let obj = LogAssertThat {
            value: &LogData::with_value_and_base(
                fa.rsn("a"),
                fa.rzi_u(2),
            ),
        };

        obj.is_value_integer_that(|_| ());
    }

    #[test]
    fn test_has_value_that() {
        let fa = ObjFactory::new10();
        let obj = LogAssertThat {
            value: &LogData::with_value_and_base(
                fa.rzi_u(3),
                fa.rzi_u(2),
            ),
        };

        obj.has_value_that(|e| { e.as_integer().is_int(3); } );
    }

    #[test]
    #[should_panic(expected = "Expected integer, actual is symbol: a")]
    fn test_fail_has_value_that() {
        let fa = ObjFactory::new10();
        let obj = LogAssertThat {
            value: &LogData::with_value_and_base(
                fa.rsn("a"),
                fa.rzi_u(2),
            ),
        };

        obj.has_value_that(|e| { e.as_integer(); });
    }
}
