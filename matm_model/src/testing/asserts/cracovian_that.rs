use crate::testing::asserts::assert_expression_that;
use crate::testing::asserts::int_that::IntegerAssertThat;
use crate::testing::asserts::frac_that::FractionAssertThat;
use crate::expressions::cracovians::CracovianData;

use super::symbol_that::SymbolAssertThat;


pub struct CracovianAssertThat<'a> {
    pub object: &'a CracovianData,
}

impl <'a> CracovianAssertThat<'a> {
    pub fn has_rows(self, expected: usize) -> CracovianAssertThat<'a> {
        assert_eq!(expected, self.object.rows(),
                   "Expected number of rows to be {}, actual is {}",
                   expected,
                   self.object.rows());
        return self;
    }

    pub fn has_columns(self, expected: usize) -> CracovianAssertThat<'a> {
        assert_eq!(expected, self.object.columns(),
                   "Expected number of columns to be {}, actual is {}",
                   expected,
                   self.object.columns());
        return self;
    }

    pub fn is_equal_to(self, expected: CracovianData) -> CracovianAssertThat<'a> {
        assert_eq!(expected, *self.object,
                "Expected cracovian to be {}, actual is {}",
                expected,
                self.object);
        return self;
    }

    pub fn has_i_i_format(self, col: usize, row: usize, expected: &str) -> CracovianAssertThat<'a> {
        let actual = self.object.get_i_i(col, row).unwrap().to_string();
        assert_eq!(expected, actual,
                   "Expected format {}, actual is {}",
                   expected,
                   actual);
        return self;
    }

    pub fn is_i_i_integer_that(self, col: usize, row: usize, f: fn(IntegerAssertThat) -> ()) -> CracovianAssertThat<'a> {
        let element = self.object.get_i_i(col, row).unwrap();
        let handler = assert_expression_that(element).as_integer();
        f(handler);
        return self;
    }

    pub fn is_i_i_fraction_that(self, col: usize, row: usize, f: fn(FractionAssertThat) -> ()) -> CracovianAssertThat<'a> {
        let element = self.object.get_i_i(col, row).unwrap();
        let handler = assert_expression_that(element).as_fraction();
        f(handler);
        return self;
    }

    pub fn is_i_i_symbol_that(self, col: usize, row: usize, f: fn(SymbolAssertThat) -> ()) -> CracovianAssertThat<'a> {
        let element = self.object.get_i_i(col, row).unwrap();
        let handler = assert_expression_that(element).as_symbol();
        f(handler);
        return self;
    }

    pub fn has_i_i_zero(self, col: usize, row: usize) -> CracovianAssertThat<'a> {
        return self.is_i_i_integer_that(col, row, |e| {e.is_zero();});
    }
}

