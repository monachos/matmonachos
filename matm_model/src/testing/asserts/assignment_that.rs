use crate::expressions::assignment::AssignmentData;
use crate::testing::asserts::assert_expression_that;
use crate::testing::asserts::int_that::IntegerAssertThat;
use crate::testing::asserts::frac_that::FractionAssertThat;
use crate::testing::asserts::symbol_that::SymbolAssertThat;

use super::expr_that::ExpressionAssertThat;


pub struct AssignmentAssertThat<'a> {
    pub value: &'a AssignmentData,
}

impl <'a> AssignmentAssertThat<'a> {
    pub fn has_source_that(self, f: fn(ExpressionAssertThat) -> ()) -> AssignmentAssertThat<'a> {
        let handler = assert_expression_that(&self.value.source);
        f(handler);
        return self;
    }

    pub fn is_source_integer_that(self, f: fn(IntegerAssertThat) -> ()) -> AssignmentAssertThat<'a> {
        let handler = assert_expression_that(&self.value.source)
            .as_integer();
        f(handler);
        return self;
    }

    pub fn is_source_fraction_that(self, f: fn(FractionAssertThat) -> ()) -> AssignmentAssertThat<'a> {
        let handler = assert_expression_that(&self.value.source)
            .as_fraction();
        f(handler);
        return self;
    }

    pub fn is_source_symbol_that(self, f: fn(SymbolAssertThat) -> ()) -> AssignmentAssertThat<'a> {
        let handler = assert_expression_that(&self.value.source)
            .as_symbol();
        f(handler);
        return self;
    }

    pub fn has_target_that(self, f: fn(ExpressionAssertThat) -> ()) -> AssignmentAssertThat<'a> {
        let handler = assert_expression_that(&self.value.target);
        f(handler);
        return self;
    }

    pub fn is_target_symbol_that(self, f: fn(SymbolAssertThat) -> ()) -> AssignmentAssertThat<'a> {
        let handler = assert_expression_that(&self.value.target)
            .as_symbol();
        f(handler);
        return self;
    }
}
