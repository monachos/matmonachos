#[cfg(test)]

mod tests {
    use crate::expressions::binomial::BinomialData;
    use crate::factories::ObjFactory;
    use crate::testing::asserts::binomial_that::BinomialAssertThat;

    #[test]
    fn test_has_set_that() {
        let fa = ObjFactory::new10();
        let obj = BinomialAssertThat {
            value: &BinomialData::new(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.has_set_that(|e| { e.as_integer().is_int(2); } );
    }

    #[test]
    #[should_panic(expected = "Expected symbol, actual is integer: 2")]
    fn test_fail_has_set_that() {
        let fa = ObjFactory::new10();
        let obj = BinomialAssertThat {
            value: &BinomialData::new(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.has_set_that(|e| { e.as_symbol(); } );
    }

    #[test]
    fn test_has_subset_that() {
        let fa = ObjFactory::new10();
        let obj = BinomialAssertThat {
            value: &BinomialData::new(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.has_subset_that(|e| { e.as_integer().is_int(3); } );
    }

    #[test]
    #[should_panic(expected = "Expected symbol, actual is integer: 3")]
    fn test_fail_has_subset_that() {
        let fa = ObjFactory::new10();
        let obj = BinomialAssertThat {
            value: &BinomialData::new(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.has_subset_that(|e| { e.as_symbol(); } );
    }

    #[test]
    fn test_is_set_integer_that() {
        let fa = ObjFactory::new10();
        let obj = BinomialAssertThat {
            value: &BinomialData::new(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.is_set_integer_that(|e| { e.is_int(2); } );
    }

    #[test]
    #[should_panic(expected = "Expected integer, actual is symbol: a")]
    fn test_fail_is_set_integer_that() {
        let fa = ObjFactory::new10();
        let obj = BinomialAssertThat {
            value: &BinomialData::new(fa.rsn("a"), fa.rzi_u(3)),
        };

        obj.is_set_integer_that(|_| ());
    }

    #[test]
    fn test_is_subset_integer_that() {
        let fa = ObjFactory::new10();
        let obj = BinomialAssertThat {
            value: &BinomialData::new(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.is_subset_integer_that(|e| { e.is_int(3); } );
    }

    #[test]
    #[should_panic(expected = "Expected integer, actual is symbol: a")]
    fn test_fail_is_subset_integer_that() {
        let fa = ObjFactory::new10();
        let obj = BinomialAssertThat {
            value: &BinomialData::new(fa.rzi_u(2), fa.rsn("a")),
        };

        obj.is_subset_integer_that(|_| ());
    }

    #[test]
    fn test_is_set_fraction_that() {
        let fa = ObjFactory::new10();
        let obj = BinomialAssertThat {
            value: &BinomialData::new(fa.rfi_u(2, 3), fa.rzi_u(5)),
        };

        obj.is_set_fraction_that(|e| { e.is_ints(2, 3); } );
    }

    #[test]
    #[should_panic(expected = "Expected fraction, actual is integer: 2")]
    fn test_fail_is_set_fraction_that() {
        let fa = ObjFactory::new10();
        let obj = BinomialAssertThat {
            value: &BinomialData::new(fa.rzi_u(2), fa.rzi_u(5)),
        };

        obj.is_set_fraction_that(|_| ());
    }

    #[test]
    fn test_is_subset_fraction_that() {
        let fa = ObjFactory::new10();
        let obj = BinomialAssertThat {
            value: &BinomialData::new(fa.rzi_u(2), fa.rfi_u(5, 6)),
        };

        obj.is_subset_fraction_that(|e| { e.is_ints(5, 6); } );
    }

    #[test]
    #[should_panic(expected = "Expected fraction, actual is integer: 5")]
    fn test_fail_is_subset_fraction_that() {
        let fa = ObjFactory::new10();
        let obj = BinomialAssertThat {
            value: &BinomialData::new(fa.rzi_u(2), fa.rzi_u(5)),
        };

        obj.is_subset_fraction_that(|_| ());
    }

    #[test]
    fn test_is_is_set_symbol_that() {
        let fa = ObjFactory::new10();
        let obj = BinomialAssertThat {
            value: &BinomialData::new(fa.rsn("a"), fa.rzi_u(5)),
        };

        obj.is_set_symbol_that(|e| { e.has_name("a"); } );
    }

    #[test]
    #[should_panic(expected = "Expected symbol, actual is integer: 2")]
    fn test_fail_is_set_symbol_that() {
        let fa = ObjFactory::new10();
        let obj = BinomialAssertThat {
            value: &BinomialData::new(fa.rzi_u(2), fa.rzi_u(5)),
        };

        obj.is_set_symbol_that(|_| ());
    }

    #[test]
    fn test_is_is_subset_symbol_that() {
        let fa = ObjFactory::new10();
        let obj = BinomialAssertThat {
            value: &BinomialData::new(fa.rzi_u(2), fa.rsn("a")),
        };

        obj.is_subset_symbol_that(|e| { e.has_name("a"); } );
    }

    #[test]
    #[should_panic(expected = "Expected symbol, actual is integer: 5")]
    fn test_fail_is_subset_symbol_that() {
        let fa = ObjFactory::new10();
        let obj = BinomialAssertThat {
            value: &BinomialData::new(fa.rzi_u(2), fa.rzi_u(5)),
        };

        obj.is_subset_symbol_that(|_| ());
    }
}
