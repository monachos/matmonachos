use crate::expressions::soe::SysOfEqData;
use super::expr_that::ExpressionAssertThat;
use crate::testing::asserts::assert_expression_that;

pub struct SystemOfEquationsAssertThat<'a> {
    pub system: &'a SysOfEqData,
}

impl<'a> SystemOfEquationsAssertThat<'a> {
    pub fn has_equations_size(self, exp_len: usize) -> SystemOfEquationsAssertThat<'a> {
        assert_eq!(exp_len, self.system.equations.len(),
                   "Expected number of equations to be {}, actual is {}",
                   exp_len,
                   self.system.equations.len());
        return self;
    }

    pub fn has_equation_i_that(self, index: usize, f: fn(ExpressionAssertThat) -> ()) -> SystemOfEquationsAssertThat<'a> {
        assert!(index < self.system.equations.len(),
                "Expected equation index {} to be present, maximum index {} is allowed",
                index,
                self.system.equations.len() - 1);
        let handler = assert_expression_that(&self.system.equations[index]);
        f(handler);
        return self;
    }
}
