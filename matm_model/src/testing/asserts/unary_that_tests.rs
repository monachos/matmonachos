#[cfg(test)]

mod tests {
    use crate::expressions::binary::BinaryData;
    use crate::expressions::matrices::MatrixData;
    use crate::expressions::ToExpression;
    use crate::expressions::unary::UnaryData;
    use crate::expressions::unary::UnaryOperator;
    use crate::factories::ObjFactory;
    use crate::testing::asserts::unary_that::UnaryAssertThat;

    #[test]
    fn test_is_operator() {
        let fa = ObjFactory::new10();
        let obj = UnaryAssertThat {
            value: &UnaryData::det(fa.rsn("a")),
        };

        obj.is_operator(UnaryOperator::Determinant);
    }

    #[test]
    #[should_panic(expected = "Expected operator to be negation, actual is determinant")]
    fn test_fail_is_operator() {
        let fa = ObjFactory::new10();
        let obj = UnaryAssertThat {
            value: &UnaryData::det(fa.rsn("a")),
        };

        obj.is_operator(UnaryOperator::Neg);
    }

    #[test]
    fn test_is_group() {
        let fa = ObjFactory::new10();
        let obj = UnaryAssertThat {
            value: &UnaryData::group(fa.rsn("a")),
        };

        obj.is_group();
    }

    #[test]
    #[should_panic(expected = "Expected operator to be group, actual is determinant")]
    fn test_fail_is_group() {
        let fa = ObjFactory::new10();
        let obj = UnaryAssertThat {
            value: &UnaryData::det(fa.rsn("a")),
        };

        obj.is_group();
    }

    #[test]
    fn test_is_neg() {
        let fa = ObjFactory::new10();
        let obj = UnaryAssertThat {
            value: &UnaryData::neg(fa.rsn("a")),
        };

        obj.is_neg();
    }

    #[test]
    #[should_panic(expected = "Expected operator to be negation, actual is determinant")]
    fn test_fail_is_neg() {
        let fa = ObjFactory::new10();
        let obj = UnaryAssertThat {
            value: &UnaryData::det(fa.rsn("a")),
        };

        obj.is_neg();
    }

    #[test]
    fn test_is_transpose() {
        let fa = ObjFactory::new10();
        let obj = UnaryAssertThat {
            value: &UnaryData::transpose(fa.rsn("a")),
        };

        obj.is_transpose();
    }

    #[test]
    #[should_panic(expected = "Expected operator to be transposition, actual is determinant")]
    fn test_fail_is_transpose() {
        let fa = ObjFactory::new10();
        let obj = UnaryAssertThat {
            value: &UnaryData::det(fa.rsn("a")),
        };

        obj.is_transpose();
    }

    #[test]
    fn test_is_determinant() {
        let fa = ObjFactory::new10();
        let obj = UnaryAssertThat {
            value: &UnaryData::det(fa.rsn("a")),
        };

        obj.is_determinant();
    }

    #[test]
    #[should_panic(expected = "Expected operator to be determinant, actual is negation")]
    fn test_fail_is_determinant() {
        let fa = ObjFactory::new10();
        let obj = UnaryAssertThat {
            value: &UnaryData::neg(fa.rsn("a")),
        };

        obj.is_determinant();
    }

    #[test]
    fn test_is_rank() {
        let fa = ObjFactory::new10();
        let obj = UnaryAssertThat {
            value: &UnaryData::rank(fa.rsn("a")),
        };

        obj.is_rank();
    }

    #[test]
    #[should_panic(expected = "Expected operator to be rank, actual is determinant")]
    fn test_fail_is_rank() {
        let fa = ObjFactory::new10();
        let obj = UnaryAssertThat {
            value: &UnaryData::det(fa.rsn("a")),
        };

        obj.is_rank();
    }

    #[test]
    fn test_is_inversion() {
        let fa = ObjFactory::new10();
        let obj = UnaryAssertThat {
            value: &UnaryData::inv(fa.rsn("a")),
        };

        obj.is_inversion();
    }

    #[test]
    #[should_panic(expected = "Expected operator to be inversion, actual is determinant")]
    fn test_fail_is_inversion() {
        let fa = ObjFactory::new10();
        let obj = UnaryAssertThat {
            value: &UnaryData::det(fa.rsn("a")),
        };

        obj.is_inversion();
    }

    #[test]
    fn test_is_factorial() {
        let fa = ObjFactory::new10();
        let obj = UnaryAssertThat {
            value: &UnaryData::factorial(fa.rsn("a")),
        };

        obj.is_factorial();
    }

    #[test]
    #[should_panic(expected = "Expected operator to be factorial, actual is determinant")]
    fn test_fail_is_factorial() {
        let fa = ObjFactory::new10();
        let obj = UnaryAssertThat {
            value: &UnaryData::det(fa.rsn("a")),
        };

        obj.is_factorial();
    }

    #[test]
    fn test_has_format() {
        let fa = ObjFactory::new10();
        let obj = UnaryAssertThat {
            value: &UnaryData::neg(fa.rfi_u(1, 2)),
        };

        obj.has_format("-(1/2)");
    }

    #[test]
    #[should_panic(expected = "Expected format -(1/2), actual is -2")]
    fn test_fail_has_format() {
        let fa = ObjFactory::new10();
        let obj = UnaryAssertThat {
            value: &UnaryData::neg(fa.rzi_u(2)),
        };

        obj.has_format("-(1/2)");
    }

    #[test]
    fn test_as_integer() {
        let fa = ObjFactory::new10();
        let obj = UnaryAssertThat {
            value: &UnaryData::neg(fa.rzi_u(2)),
        };

        obj.as_integer();
    }

    #[test]
    #[should_panic(expected = "Expected integer, actual is fraction: 1/2")]
    fn test_fail_as_integer() {
        let fa = ObjFactory::new10();
        let obj = UnaryAssertThat {
            value: &UnaryData::neg(fa.rfi_u(1, 2)),
        };

        obj.as_integer();
    }

    #[test]
    fn test_as_fraction() {
        let fa = ObjFactory::new10();
        let obj = UnaryAssertThat {
            value: &UnaryData::neg(fa.rfi_u(1, 2)),
        };

        obj.as_fraction();
    }

    #[test]
    #[should_panic(expected = "Expected fraction, actual is integer: 2")]
    fn test_fail_as_fraction() {
        let fa = ObjFactory::new10();
        let obj = UnaryAssertThat {
            value: &UnaryData::neg(fa.rzi_u(2)),
        };

        obj.as_fraction();
    }

    #[test]
    fn test_as_symbol() {
        let fa = ObjFactory::new10();
        let obj = UnaryAssertThat {
            value: &UnaryData::neg(fa.rsn("a")),
        };

        obj.as_symbol();
    }

    #[test]
    #[should_panic(expected = "Expected symbol, actual is integer: 2")]
    fn test_fail_as_symbol() {
        let fa = ObjFactory::new10();
        let obj = UnaryAssertThat {
            value: &UnaryData::neg(fa.rzi_u(2)),
        };

        obj.as_symbol();
    }

    #[test]
    fn test_as_matrix() {
        let fa = ObjFactory::new10();
        let obj = UnaryAssertThat {
            value: &UnaryData::neg(MatrixData::zeros(fa.radix(), 2, 2).unwrap().to_rc_expr()),
        };

        obj.as_matrix();
    }

    #[test]
    #[should_panic(expected = "Expected matrix, actual is integer: 2")]
    fn test_fail_as_matrix() {
        let fa = ObjFactory::new10();
        let obj = UnaryAssertThat {
            value: &UnaryData::neg(fa.rzi_u(2)),
        };

        obj.as_matrix();
    }

    #[test]
    fn test_as_unary() {
        let fa = ObjFactory::new10();
        let obj = UnaryAssertThat {
            value: &UnaryData::neg(UnaryData::neg(fa.rz1()).to_rc_expr()),
        };

        obj.as_unary();
    }

    #[test]
    #[should_panic(expected = "Expected unary, actual is integer: 2")]
    fn test_fail_as_unary() {
        let fa = ObjFactory::new10();
        let obj = UnaryAssertThat {
            value: &UnaryData::neg(fa.rzi_u(2)),
        };

        obj.as_unary();
    }

    #[test]
    fn test_as_binary() {
        let fa = ObjFactory::new10();
        let obj = UnaryAssertThat {
            value: &UnaryData::neg(BinaryData::add(fa.rsn("a"), fa.rz0()).to_rc_expr()),
        };

        obj.as_binary();
    }

    #[test]
    #[should_panic(expected = "Expected binary, actual is integer: 2")]
    fn test_fail_as_binary() {
        let fa = ObjFactory::new10();
        let obj = UnaryAssertThat {
            value: &UnaryData::neg(fa.rzi_u(2)),
        };

        obj.as_binary();
    }
}
