use crate::testing::asserts::assert_expression_that;
use crate::expressions::fractions::FracData;

use super::expr_that::ExpressionAssertThat;


pub struct FractionAssertThat<'a> {
    pub frac: &'a FracData,
}

impl <'a> FractionAssertThat<'a> {
    pub fn has_numerator_int(self, expected: i32) -> FractionAssertThat<'a> {
        assert_expression_that(&self.frac.numerator)
            .as_integer()
            .is_int(expected);
        return self;
    }

    pub fn has_denominator_int(self, expected: i32) -> FractionAssertThat<'a> {
        assert_expression_that(&self.frac.denominator)
            .as_integer()
            .is_int(expected);
        return self;
    }

    pub fn is_ints(self, exp_num: i32, exp_den: i32) -> FractionAssertThat<'a> {
        assert_expression_that(&self.frac.numerator)
            .as_integer()
            .is_int(exp_num);
        assert_expression_that(&self.frac.denominator)
            .as_integer()
            .is_int(exp_den);
        return self;
    }

    pub fn has_numerator_format(self, expected: &str) -> FractionAssertThat<'a> {
        assert_eq!(expected, self.frac.numerator.to_string(),
                   "Expected numerator {}, actual is {}",
                   expected,
                   self.frac.numerator.to_string());
        return self;
    }

    pub fn has_denominator_format(self, expected: &str) -> FractionAssertThat<'a> {
        assert_eq!(expected, self.frac.denominator.to_string(),
                   "Expected denominator {}, actual is {}",
                   expected,
                   self.frac.denominator.to_string());
        return self;
    }

    pub fn has_numerator_that(self, f: fn(ExpressionAssertThat) -> ()) -> FractionAssertThat<'a> {
        let handler = assert_expression_that(&self.frac.numerator);
        f(handler);
        return self;
    }

    pub fn has_denominator_that(self, f: fn(ExpressionAssertThat) -> ()) -> FractionAssertThat<'a> {
        let handler = assert_expression_that(&self.frac.denominator);
        f(handler);
        return self;
    }

}
