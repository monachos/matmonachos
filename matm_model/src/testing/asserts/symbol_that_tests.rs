#[cfg(test)]

mod tests {
    use crate::expressions::symbol::SymbolData;
    use crate::factories::ObjFactory;
    use crate::testing::asserts::symbol_that::SymbolAssertThat;

    #[test]
    fn test_has_name() {
        let obj = SymbolAssertThat {
            symbol: &SymbolData::new("a"),
        };

        obj.has_name("a");
    }

    #[test]
    #[should_panic(expected = "Expected symbol name to be a, actual is b")]
    fn test_fail_has_name() {
        let obj = SymbolAssertThat {
            symbol: &SymbolData::new("b"),
        };

        obj.has_name("a");
    }

    #[test]
    fn test_has_indices_equal_to() {
        let fa = ObjFactory::new10();
        let obj = SymbolAssertThat {
            symbol: &SymbolData::with_index_s("a", "i"),
        };

        obj.has_indices_equal_to(vec![fa.esn("i")]);
    }

    #[test]
    #[should_panic(expected = "Expected")]
    #[ignore = "Fix the assertion message"]
    fn test_fail_has_indices_equal_to() {
        let fa = ObjFactory::new10();
        let obj = SymbolAssertThat {
            symbol: &SymbolData::with_index_e("a", fa.rzi_u(1)),
        };

        obj.has_indices_equal_to(vec![fa.esn("i")]);
    }
}
