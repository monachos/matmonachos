#[cfg(test)]

mod tests {
    use crate::expressions::symbol::SymbolData;
    use crate::expressions::variable::VariableData;
    use crate::factories::ObjFactory;
    use crate::testing::asserts::variable_that::VariableAssertThat;

    #[test]
    fn test_has_name() {
        let fa = ObjFactory::new10();
        let obj = VariableAssertThat {
            variable: &VariableData::new(SymbolData::new("a"), fa.rz0()),
        };

        obj.has_name("a");
    }

    #[test]
    #[should_panic(expected = "Expected variable name to be a, actual is b")]
    fn test_fail_has_name() {
        let fa = ObjFactory::new10();
        let obj = VariableAssertThat {
            variable: &VariableData::new(SymbolData::new("b"), fa.rz0()),
        };

        obj.has_name("a");
    }
}
