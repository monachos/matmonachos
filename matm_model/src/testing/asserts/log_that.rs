use crate::expressions::logarithm::LogData;
use crate::testing::asserts::assert_expression_that;
use crate::testing::asserts::int_that::IntegerAssertThat;

use super::expr_that::ExpressionAssertThat;


pub struct LogAssertThat<'a> {
    pub value: &'a LogData,
}

impl <'a> LogAssertThat<'a> {
    pub fn is_value_integer_that(self, f: fn(IntegerAssertThat) -> ()) -> LogAssertThat<'a> {
        let handler = assert_expression_that(&self.value.value)
            .as_integer();
        f(handler);
        return self;
    }

    pub fn has_value_that(self, f: fn(ExpressionAssertThat) -> ()) -> LogAssertThat<'a> {
        let handler = assert_expression_that(&self.value.value);
        f(handler);
        return self;
    }

    pub fn is_base_integer_that(self, f: fn(IntegerAssertThat) -> ()) -> LogAssertThat<'a> {
        let handler = assert_expression_that(&self.value.base)
            .as_integer();
        f(handler);
        return self;
    }

    pub fn has_base_that(self, f: fn(ExpressionAssertThat) -> ()) -> LogAssertThat<'a> {
        let handler = assert_expression_that(&self.value.base);
        f(handler);
        return self;
    }
}

