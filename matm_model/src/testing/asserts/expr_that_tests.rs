#[cfg(test)]

mod tests {
    use crate::expressions::Expression;
    use crate::expressions::ToExpression;
    use crate::expressions::assignment::AssignmentData;
    use crate::expressions::binary::BinaryData;
    use crate::expressions::binomial::BinomialData;
    use crate::expressions::bool::BoolData;
    use crate::expressions::conditional::ConditionalData;
    use crate::expressions::cracovians::CracovianData;
    use crate::expressions::exponentiation::ExpData;
    use crate::expressions::func::FunctionData;
    use crate::expressions::logarithm::LogData;
    use crate::expressions::matrices::MatrixData;
    use crate::expressions::method::MethodData;
    use crate::expressions::object::ObjectData;
    use crate::expressions::object::ObjectType;
    use crate::expressions::root::RootData;
    use crate::expressions::unary::UnaryData;
    use crate::factories::ObjFactory;
    use crate::testing::asserts::expr_that::ExpressionAssertThat;

    #[test]
    fn test_is_undefined() {
        let obj = ExpressionAssertThat {
            expression: &Expression::Undefined,
        };

        obj.is_undefined();
    }

    #[test]
    #[should_panic(expected = "Expected undefined, actual is integer: 2")]
    fn test_fail_is_undefined() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &fa.ezi_u(2),
        };

        obj.is_undefined();
    }

    #[test]
    fn test_is_equal_to() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &fa.ezi_u(2),
        };

        let expected = fa.ezi_u(2);
        obj.is_equal_to(&expected);
    }

    #[test]
    #[should_panic(expected = "Expected integer 10, actual is integer 2")]
    fn test_fail_is_equal_to() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &fa.ezi_u(2),
        };

        let expected = fa.ezi_u(10);
        obj.is_equal_to(&expected);
    }

    #[test]
    fn test_as_assignment() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &AssignmentData::new(fa.rsn("a"), fa.rz0()).to_expr(),
        };

        obj.as_assignment();
    }

    #[test]
    #[should_panic(expected = "Expected assignment, actual is integer: 2")]
    fn test_fail_as_assignment() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &fa.ezi_u(2),
        };

        obj.as_assignment();
    }

    #[test]
    fn test_is_pi() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &fa.rpi(),
        };

        obj.is_pi();
    }

    #[test]
    #[should_panic(expected = "Expected pi, actual is integer: 2")]
    fn test_fail_is_pi() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &fa.ezi_u(2),
        };

        obj.is_pi();
    }

    #[test]
    fn test_is_e() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &fa.re(),
        };

        obj.is_e();
    }

    #[test]
    #[should_panic(expected = "Expected e, actual is integer: 2")]
    fn test_fail_is_e() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &fa.ezi_u(2),
        };

        obj.is_e();
    }

    #[test]
    fn test_as_integer() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &fa.rzi_u(2),
        };

        obj.as_integer();
    }

    #[test]
    #[should_panic(expected = "Expected integer, actual is fraction: 1/2")]
    fn test_fail_as_integer() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &fa.efi_u(1, 2),
        };

        obj.as_integer();
    }

    #[test]
    fn test_as_bool() {
        let obj = ExpressionAssertThat {
            expression: &BoolData::true_value().to_expr(),
        };

        obj.as_bool();
    }

    #[test]
    #[should_panic(expected = "Expected bool, actual is integer: 2")]
    fn test_fail_as_bool() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &fa.ezi_u(2),
        };

        obj.as_bool();
    }

    #[test]
    fn test_as_fraction() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &fa.rfi_u(1, 2),
        };

        obj.as_fraction();
    }

    #[test]
    #[should_panic(expected = "Expected fraction, actual is integer: 2")]
    fn test_fail_as_fraction() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &fa.ezi_u(2),
        };

        obj.as_fraction();
    }

    #[test]
    fn test_as_symbol() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &fa.rsn("a"),
        };

        obj.as_symbol();
    }

    #[test]
    #[should_panic(expected = "Expected symbol, actual is integer: 2")]
    fn test_fail_as_symbol() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &fa.ezi_u(2),
        };

        obj.as_symbol();
    }

    #[test]
    fn test_is_boxsign() {
        let obj = ExpressionAssertThat {
            expression: &Expression::BoxSign,
        };

        obj.is_boxsign();
    }

    #[test]
    #[should_panic(expected = "Expected box, actual is integer: 2")]
    fn test_fail_is_boxsign() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &fa.ezi_u(2),
        };

        obj.is_boxsign();
    }

    #[test]
    fn test_as_matrix() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &MatrixData::zeros(fa.radix(), 2, 2).unwrap().to_expr(),
        };

        obj.as_matrix();
    }

    #[test]
    #[should_panic(expected = "Expected matrix, actual is integer: 2")]
    fn test_fail_as_matrix() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &fa.ezi_u(2),
        };

        obj.as_matrix();
    }

    #[test]
    fn test_as_cracovian() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &CracovianData::zeros(fa.radix(), 2, 2).unwrap().to_expr(),
        };

        obj.as_cracovian();
    }

    #[test]
    #[should_panic(expected = "Expected cracovian, actual is integer: 2")]
    fn test_fail_as_cracovian() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &fa.ezi_u(2),
        };

        obj.as_cracovian();
    }

    #[test]
    fn test_as_unary() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &UnaryData::neg(fa.rz1()).to_expr(),
        };

        obj.as_unary();
    }

    #[test]
    #[should_panic(expected = "Expected unary, actual is integer: 2")]
    fn test_fail_as_unary() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &fa.ezi_u(2),
        };

        obj.as_unary();
    }

    #[test]
    fn test_as_binary() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &BinaryData::add(fa.rsn("a"), fa.rz0()).to_expr(),
        };

        obj.as_binary();
    }

    #[test]
    #[should_panic(expected = "Expected binary, actual is integer: 2")]
    fn test_fail_as_binary() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &fa.ezi_u(2),
        };

        obj.as_binary();
    }

    #[test]
    fn test_as_binomial() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &BinomialData::new(fa.rsn("a"), fa.rzi_u(2)).to_expr(),
        };

        obj.as_binomial();
    }

    #[test]
    #[should_panic(expected = "Expected binomial, actual is integer: 2")]
    fn test_fail_as_binomial() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &fa.ezi_u(2),
        };

        obj.as_binomial();
    }

    #[test]
    fn test_as_root() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &RootData::new(fa.rsn("a"), fa.rzi_u(2)).to_expr(),
        };

        obj.as_root();
    }

    #[test]
    #[should_panic(expected = "Expected root, actual is integer: 2")]
    fn test_fail_as_root() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &fa.ezi_u(2),
        };

        obj.as_root();
    }

    #[test]
    fn test_as_log() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &LogData::with_value_and_base(
                fa.rzi_u(2),
                fa.rsn("a"),
            ).to_expr(),
        };

        obj.as_log();
    }

    #[test]
    #[should_panic(expected = "Expected log, actual is integer: 2")]
    fn test_fail_as_log() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &fa.ezi_u(2),
        };

        obj.as_log();
    }

    #[test]
    fn test_as_exponent() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &ExpData::new(fa.rsn("a"), fa.rzi_u(2)).to_expr(),
        };

        obj.as_exponent();
    }

    #[test]
    #[should_panic(expected = "Expected exponent, actual is integer: 2")]
    fn test_fail_as_exponent() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &fa.ezi_u(2),
        };

        obj.as_exponent();
    }

    #[test]
    fn test_as_function() {
        let obj = ExpressionAssertThat {
            expression: &FunctionData::new(String::from("foo"), vec![]).to_expr(),
        };

        obj.as_function();
    }

    #[test]
    #[should_panic(expected = "Expected function, actual is integer: 2")]
    fn test_fail_as_function() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &fa.ezi_u(2),
        };

        obj.as_function();
    }

    #[test]
    fn test_as_method() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &MethodData::new(fa.rz1(), String::from("foo"), vec![]).to_expr(),
        };

        obj.as_method();
    }

    #[test]
    #[should_panic(expected = "Expected method, actual is integer: 2")]
    fn test_fail_as_method() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &fa.ezi_u(2),
        };

        obj.as_method();
    }

    #[test]
    fn test_as_object() {
        let obj = ExpressionAssertThat {
            expression: &ObjectData::new(ObjectType::Matrix).to_expr(),
        };

        obj.as_object();
    }

    #[test]
    #[should_panic(expected = "Expected object, actual is integer: 2")]
    fn test_fail_as_object() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &fa.ezi_u(2),
        };

        obj.as_object();
    }

    #[test]
    fn test_as_conditional() {
        let obj = ExpressionAssertThat {
            expression: &ConditionalData::from_vector(vec![]).to_expr(),
        };

        obj.as_conditional();
    }

    #[test]
    #[should_panic(expected = "Expected conditional, actual is integer: 2")]
    fn test_fail_as_conditional() {
        let fa = ObjFactory::new10();
        let obj = ExpressionAssertThat {
            expression: &fa.ezi_u(2),
        };

        obj.as_conditional();
    }
}
