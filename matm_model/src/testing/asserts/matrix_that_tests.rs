#[cfg(test)]

mod tests {
    use std::rc::Rc;

    use crate::expressions::Expression;
    use crate::expressions::matrices::MatrixData;
    use crate::factories::ObjFactory;
    use crate::matrix;
    use crate::testing::asserts::matrix_that::MatrixAssertThat;

    fn sample_1_2_14_a(fa: &ObjFactory) -> MatrixData {
        matrix!(fa, 2, 2;
                0, 2,
                (1/4), "a"
            ).unwrap()
    }

    #[test]
    fn test_has_rows() {
        let fa = ObjFactory::new10();
        let obj = MatrixAssertThat {
            matrix: &sample_1_2_14_a(&fa)
        };

        obj.has_rows(2);
    }

    #[test]
    #[should_panic(expected = "Expected number of rows to be 1, actual is 2")]
    fn test_fail_has_rows() {
        let fa = ObjFactory::new10();
        let obj = MatrixAssertThat {
            matrix: &sample_1_2_14_a(&fa)
        };

        obj.has_rows(1);
    }

    #[test]
    fn test_has_columns() {
        let fa = ObjFactory::new10();
        let obj = MatrixAssertThat {
            matrix: &sample_1_2_14_a(&fa)
        };

        obj.has_columns(2);
    }

    #[test]
    #[should_panic(expected = "Expected number of columns to be 3, actual is 2")]
    fn test_fail_has_columns() {
        let fa = ObjFactory::new10();
        let obj = MatrixAssertThat {
            matrix: &sample_1_2_14_a(&fa)
        };

        obj.has_columns(3);
    }

    #[test]
    fn test_is_equal_to() {
        let fa = ObjFactory::new10();
        let obj = MatrixAssertThat {
            matrix: &sample_1_2_14_a(&fa)
        };

        let expected = sample_1_2_14_a(&fa);
        obj.is_equal_to(expected);
    }

    #[test]
    #[should_panic(expected = "Expected matrix to be [1, 2; 0, 0], actual is [0, 2; 1/4, a]")]
    fn test_fail_is_equal_to() {
        let fa = ObjFactory::new10();
        let obj = MatrixAssertThat {
            matrix: &sample_1_2_14_a(&fa)
        };

        let expected = matrix!(fa, 2, 2;
                1, 2,
                0, 0
            ).unwrap();
        obj.is_equal_to(expected);
    }

    #[test]
    fn test_has_i_i_format() {
        let fa = ObjFactory::new10();
        let obj = MatrixAssertThat {
            matrix: &sample_1_2_14_a(&fa)
        };

        obj.has_i_i_format(1, 2, "2");
    }

    #[test]
    #[should_panic(expected = "Expected format 2, actual is 1/4")]
    fn test_fail_has_i_i_format() {
        let fa = ObjFactory::new10();
        let obj = MatrixAssertThat {
            matrix: &sample_1_2_14_a(&fa)
        };

        obj.has_i_i_format(2, 1, "2");
    }

    #[test]
    fn test_is_i_i_integer_that() {
        let fa = ObjFactory::new10();
        let obj = MatrixAssertThat {
            matrix: &sample_1_2_14_a(&fa)
        };

        obj.is_i_i_integer_that(1, 2, |e| { e.is_int(2); });
    }

    #[test]
    #[should_panic(expected = "Expected integer, actual is fraction: 1/4")]
    fn test_fail_is_i_i_integer_that() {
        let fa = ObjFactory::new10();
        let obj = MatrixAssertThat {
            matrix: &sample_1_2_14_a(&fa)
        };

        obj.is_i_i_integer_that(2, 1, |_| ());
    }

    #[test]
    fn test_is_i_i_fraction_that() {
        let fa = ObjFactory::new10();
        let obj = MatrixAssertThat {
            matrix: &sample_1_2_14_a(&fa)
        };

        obj.is_i_i_fraction_that(2, 1, |e| { e.is_ints(1, 4); });
    }

    #[test]
    #[should_panic(expected = "Expected fraction, actual is integer: 2")]
    fn test_fail_is_i_i_fraction_that() {
        let fa = ObjFactory::new10();
        let obj = MatrixAssertThat {
            matrix: &sample_1_2_14_a(&fa)
        };

        obj.is_i_i_fraction_that(1, 2, |_| ());
    }

    #[test]
    fn test_is_i_i_symbol_that() {
        let fa = ObjFactory::new10();
        let obj = MatrixAssertThat {
            matrix: &sample_1_2_14_a(&fa)
        };

        obj.is_i_i_symbol_that(2, 2, |e| { e.has_name("a"); });
    }

    #[test]
    #[should_panic(expected = "Expected symbol, actual is integer: 2")]
    fn test_fail_is_i_i_symbol_that() {
        let fa = ObjFactory::new10();
        let obj = MatrixAssertThat {
            matrix: &sample_1_2_14_a(&fa)
        };

        obj.is_i_i_symbol_that(1, 2, |_| ());
    }

    #[test]
    fn test_is_i_i_boxsign() {
        let fa = ObjFactory::new10();
        let obj = MatrixAssertThat {
            matrix: &MatrixData::from_vector(
                2, 2,
                vec![
                    fa.rz0(), fa.rz0(),
                    fa.rz0(), Rc::new(Expression::BoxSign)])
                .unwrap()
        };

        obj.is_i_i_boxsign(2, 2);
    }

    #[test]
    #[should_panic(expected = "Expected box, actual is integer: 2")]
    fn test_fail_is_i_i_boxsign() {
        let fa = ObjFactory::new10();
        let obj = MatrixAssertThat {
            matrix: &sample_1_2_14_a(&fa)
        };

        obj.is_i_i_boxsign(1, 2);
    }

    #[test]
    fn test_has_i_i_zero() {
        let fa = ObjFactory::new10();
        let obj = MatrixAssertThat {
            matrix: &sample_1_2_14_a(&fa)
        };

        obj.has_i_i_zero(1, 1);
    }

    #[test]
    #[should_panic(expected = "Expected zero integer")]
    fn test_fail_has_i_i_zero() {
        let fa = ObjFactory::new10();
        let obj = MatrixAssertThat {
            matrix: &sample_1_2_14_a(&fa)
        };

        obj.has_i_i_zero(1, 2);
    }

    #[test]
    fn test_has_i_i_one() {
        let fa = ObjFactory::new10();
        let obj = MatrixAssertThat {
            matrix: &matrix!(fa, 2, 2;
                1, 2,
                (1/4), "a"
            ).unwrap()
        };

        obj.has_i_i_one(1, 1);
    }

    #[test]
    #[should_panic(expected = "Expected integer 1, actual is 2")]
    fn test_fail_has_i_i_one() {
        let fa = ObjFactory::new10();
        let obj = MatrixAssertThat {
            matrix: &sample_1_2_14_a(&fa)
        };

        obj.has_i_i_one(1, 2);
    }
}
