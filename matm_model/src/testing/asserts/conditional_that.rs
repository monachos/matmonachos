use crate::expressions::conditional::ConditionalData;
use crate::testing::asserts::assert_expression_that;
use crate::testing::asserts::expr_that::ExpressionAssertThat;


pub struct ConditionalAssertThat<'a> {
    pub value: &'a ConditionalData,
}

impl <'a> ConditionalAssertThat<'a> {
    pub fn has_items_size(self, expected: usize) -> ConditionalAssertThat<'a> {
        assert_eq!(expected, self.value.clauses.len(),
                   "Expected number of conditions to be {}, actual is {}",
                   expected,
                   self.value.clauses.len());
        return self;
    }

    pub fn has_i_expression_format(self, index: usize, expected: &str) -> ConditionalAssertThat<'a> {
        let item = self.value.clauses.get(index).unwrap();
        let actual = item.expression.to_string();
        assert_eq!(expected, actual,
                   "Expected expression format {}, actual is {}",
                   expected,
                   actual);
        return self;
    }

    pub fn has_i_condition_format(self, index: usize, expected: &str) -> ConditionalAssertThat<'a> {
        let item = self.value.clauses.get(index).unwrap();
        let actual = item.condition.to_string();
        assert_eq!(expected, actual,
                   "Expected condition format {}, actual is {}",
                   expected,
                   actual);
        return self;
    }

    pub fn has_i_expression_that(self, index: usize, f: fn(ExpressionAssertThat) -> ()) -> ConditionalAssertThat<'a> {
        assert!(index < self.value.clauses.len(),
                "Expected condition index {} to be present, maximum index {} is allowed",
                index,
                self.value.clauses.len() - 1);
        let item = &self.value.clauses[index];
        let handler = assert_expression_that(&item.expression);
        f(handler);
        return self;
    }
}

