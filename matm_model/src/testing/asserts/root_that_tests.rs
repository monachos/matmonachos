#[cfg(test)]

mod tests {
    use crate::expressions::root::RootData;
    use crate::factories::ObjFactory;
    use crate::testing::asserts::root_that::RootAssertThat;

    #[test]
    fn test_is_radicand_integer_that() {
        let fa = ObjFactory::new10();
        let obj = RootAssertThat {
            value: &RootData::new(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.is_radicand_integer_that(|e| { e.is_int(2); } );
    }

    #[test]
    #[should_panic(expected = "Expected integer, actual is symbol: a")]
    fn test_fail_is_radicand_integer_that() {
        let fa = ObjFactory::new10();
        let obj = RootAssertThat {
            value: &RootData::new(fa.rsn("a"), fa.rzi_u(3)),
        };

        obj.is_radicand_integer_that(|_| ());
    }

    #[test]
    fn test_has_radicand_that() {
        let fa = ObjFactory::new10();
        let obj = RootAssertThat {
            value: &RootData::new(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.has_radicand_that(|e| { e.as_integer().is_int(2); } );
    }

    #[test]
    #[should_panic(expected = "Expected integer, actual is symbol: a")]
    fn test_fail_has_radicand_that() {
        let fa = ObjFactory::new10();
        let obj = RootAssertThat {
            value: &RootData::new(fa.rsn("a"), fa.rzi_u(3)),
        };

        obj.has_radicand_that(|e| { e.as_integer(); });
    }

    #[test]
    fn test_is_degree_integer_that() {
        let fa = ObjFactory::new10();
        let obj = RootAssertThat {
            value: &RootData::new(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.is_degree_integer_that(|e| { e.is_int(3); } );
    }

    #[test]
    #[should_panic(expected = "Expected integer, actual is symbol: a")]
    fn test_fail_is_degree_integer_that() {
        let fa = ObjFactory::new10();
        let obj = RootAssertThat {
            value: &RootData::new(fa.rzi_u(2), fa.rsn("a")),
        };

        obj.is_degree_integer_that(|_| ());
    }

    #[test]
    fn test_has_degree_that() {
        let fa = ObjFactory::new10();
        let obj = RootAssertThat {
            value: &RootData::new(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.has_degree_that(|e| { e.as_integer().is_int(3); } );
    }

    #[test]
    #[should_panic(expected = "Expected integer, actual is symbol: a")]
    fn test_fail_has_degree_that() {
        let fa = ObjFactory::new10();
        let obj = RootAssertThat {
            value: &RootData::new(fa.rzi_u(2), fa.rsn("a")),
        };

        obj.has_degree_that(|e| { e.as_integer(); });
    }
}
