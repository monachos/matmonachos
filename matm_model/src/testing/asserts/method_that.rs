use super::expr_that::ExpressionAssertThat;
use crate::expressions::method::MethodData;
use crate::testing::asserts::assert_expression_that;

pub struct MethodAssertThat<'a> {
    pub method: &'a MethodData,
}

impl<'a> MethodAssertThat<'a> {
    pub fn has_name(self, exp_name: &str) -> MethodAssertThat<'a> {
        assert_eq!(exp_name, self.method.func.name,
                   "Expected method {}, actual is {}",
                   exp_name,
                   self.method.func.name);
        return self;
    }

    pub fn has_object_that(self, f: fn(ExpressionAssertThat) -> ()) -> MethodAssertThat<'a> {
        let handler = assert_expression_that(&self.method.object);
        f(handler);
        return self;
    }

    pub fn has_arguments_size(self, exp_len: usize) -> MethodAssertThat<'a> {
        assert_eq!(exp_len, self.method.func.arguments.len(),
                   "Expected number of method arguments to be {}, actual is {}",
                   exp_len,
                   self.method.func.arguments.len());
        return self;
    }

    pub fn has_argument_i_that(self, index: usize, f: fn(ExpressionAssertThat) -> ()) -> MethodAssertThat<'a> {
        assert!(index < self.method.func.arguments.len(),
                "Expected method argument index {} to be present, maximum index {} is allowed",
                index,
                self.method.func.arguments.len() - 1);
        let handler = assert_expression_that(&self.method.func.arguments[index]);
        f(handler);
        return self;
    }
}
