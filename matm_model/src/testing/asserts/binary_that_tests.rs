#[cfg(test)]

mod tests {
    use crate::expressions::binary::{BinaryData, BinaryOperator};
    use crate::factories::ObjFactory;
    use crate::testing::asserts::binary_that::BinaryAssertThat;

    #[test]
    fn test_is_operator() {
        let fa = ObjFactory::new10();
        let obj = BinaryAssertThat {
            value: &BinaryData::add(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.is_operator(BinaryOperator::Add);
    }

    #[test]
    #[should_panic(expected = "Expected operator -, actual is +")]
    fn test_fail_is_operator() {
        let fa = ObjFactory::new10();
        let obj = BinaryAssertThat {
            value: &BinaryData::add(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.is_operator(BinaryOperator::Sub);
    }

    #[test]
    fn test_is_add() {
        let fa = ObjFactory::new10();
        let obj = BinaryAssertThat {
            value: &BinaryData::add(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.is_add();
    }

    #[test]
    #[should_panic(expected = "Expected operator +, actual is -")]
    fn test_fail_is_add() {
        let fa = ObjFactory::new10();
        let obj = BinaryAssertThat {
            value: &BinaryData::sub(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.is_add();
    }

    #[test]
    fn test_is_sub() {
        let fa = ObjFactory::new10();
        let obj = BinaryAssertThat {
            value: &BinaryData::sub(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.is_sub();
    }

    #[test]
    #[should_panic(expected = "Expected operator -, actual is +")]
    fn test_fail_is_sub() {
        let fa = ObjFactory::new10();
        let obj = BinaryAssertThat {
            value: &BinaryData::add(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.is_sub();
    }

    #[test]
    fn test_is_mul() {
        let fa = ObjFactory::new10();
        let obj = BinaryAssertThat {
            value: &BinaryData::mul(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.is_mul();
    }

    #[test]
    #[should_panic(expected = "Expected operator *, actual is -")]
    fn test_fail_is_mul() {
        let fa = ObjFactory::new10();
        let obj = BinaryAssertThat {
            value: &BinaryData::sub(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.is_mul();
    }

    #[test]
    fn test_is_div() {
        let fa = ObjFactory::new10();
        let obj = BinaryAssertThat {
            value: &BinaryData::div(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.is_div();
    }

    #[test]
    #[should_panic(expected = "Expected operator div, actual is -")]
    fn test_fail_is_div() {
        let fa = ObjFactory::new10();
        let obj = BinaryAssertThat {
            value: &BinaryData::sub(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.is_div();
    }

    #[test]
    fn test_has_left_that() {
        let fa = ObjFactory::new10();
        let obj = BinaryAssertThat {
            value: &BinaryData::add(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.has_left_that(|e| { e.as_integer().is_int(2); } );
    }

    #[test]
    #[should_panic(expected = "Expected symbol, actual is integer: 2")]
    fn test_fail_has_left_that() {
        let fa = ObjFactory::new10();
        let obj = BinaryAssertThat {
            value: &BinaryData::add(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.has_left_that(|e| { e.as_symbol(); } );
    }

    #[test]
    fn test_has_right_that() {
        let fa = ObjFactory::new10();
        let obj = BinaryAssertThat {
            value: &BinaryData::add(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.has_right_that(|e| { e.as_integer().is_int(3); } );
    }

    #[test]
    #[should_panic(expected = "Expected symbol, actual is integer: 3")]
    fn test_fail_has_right_that() {
        let fa = ObjFactory::new10();
        let obj = BinaryAssertThat {
            value: &BinaryData::add(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.has_right_that(|e| { e.as_symbol(); } );
    }

    #[test]
    fn test_is_left_integer_that() {
        let fa = ObjFactory::new10();
        let obj = BinaryAssertThat {
            value: &BinaryData::add(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.is_left_integer_that(|e| { e.is_int(2); } );
    }

    #[test]
    #[should_panic(expected = "Expected integer, actual is symbol: a")]
    fn test_fail_is_left_integer_that() {
        let fa = ObjFactory::new10();
        let obj = BinaryAssertThat {
            value: &BinaryData::add(fa.rsn("a"), fa.rzi_u(3)),
        };

        obj.is_left_integer_that(|_| ());
    }

    #[test]
    fn test_is_right_integer_that() {
        let fa = ObjFactory::new10();
        let obj = BinaryAssertThat {
            value: &BinaryData::add(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.is_right_integer_that(|e| { e.is_int(3); } );
    }

    #[test]
    #[should_panic(expected = "Expected integer, actual is symbol: a")]
    fn test_fail_is_right_integer_that() {
        let fa = ObjFactory::new10();
        let obj = BinaryAssertThat {
            value: &BinaryData::add(fa.rzi_u(2), fa.rsn("a")),
        };

        obj.is_right_integer_that(|_| ());
    }

    #[test]
    fn test_is_left_fraction_that() {
        let fa = ObjFactory::new10();
        let obj = BinaryAssertThat {
            value: &BinaryData::add(fa.rfi_u(2, 3), fa.rzi_u(5)),
        };

        obj.is_left_fraction_that(|e| { e.is_ints(2, 3); } );
    }

    #[test]
    #[should_panic(expected = "Expected fraction, actual is integer: 2")]
    fn test_fail_is_left_fraction_that() {
        let fa = ObjFactory::new10();
        let obj = BinaryAssertThat {
            value: &BinaryData::add(fa.rzi_u(2), fa.rzi_u(5)),
        };

        obj.is_left_fraction_that(|_| ());
    }

    #[test]
    fn test_is_right_fraction_that() {
        let fa = ObjFactory::new10();
        let obj = BinaryAssertThat {
            value: &BinaryData::add(fa.rzi_u(2), fa.rfi_u(5, 6)),
        };

        obj.is_right_fraction_that(|e| { e.is_ints(5, 6); } );
    }

    #[test]
    #[should_panic(expected = "Expected fraction, actual is integer: 5")]
    fn test_fail_is_right_fraction_that() {
        let fa = ObjFactory::new10();
        let obj = BinaryAssertThat {
            value: &BinaryData::add(fa.rzi_u(2), fa.rzi_u(5)),
        };

        obj.is_right_fraction_that(|_| ());
    }

    #[test]
    fn test_is_is_left_symbol_that() {
        let fa = ObjFactory::new10();
        let obj = BinaryAssertThat {
            value: &BinaryData::add(fa.rsn("a"), fa.rzi_u(5)),
        };

        obj.is_left_symbol_that(|e| { e.has_name("a"); } );
    }

    #[test]
    #[should_panic(expected = "Expected symbol, actual is integer: 2")]
    fn test_fail_is_left_symbol_that() {
        let fa = ObjFactory::new10();
        let obj = BinaryAssertThat {
            value: &BinaryData::add(fa.rzi_u(2), fa.rzi_u(5)),
        };

        obj.is_left_symbol_that(|_| ());
    }

    #[test]
    fn test_is_is_right_symbol_that() {
        let fa = ObjFactory::new10();
        let obj = BinaryAssertThat {
            value: &BinaryData::add(fa.rzi_u(2), fa.rsn("a")),
        };

        obj.is_right_symbol_that(|e| { e.has_name("a"); } );
    }

    #[test]
    #[should_panic(expected = "Expected symbol, actual is integer: 5")]
    fn test_fail_is_right_symbol_that() {
        let fa = ObjFactory::new10();
        let obj = BinaryAssertThat {
            value: &BinaryData::add(fa.rzi_u(2), fa.rzi_u(5)),
        };

        obj.is_right_symbol_that(|_| ());
    }
}
