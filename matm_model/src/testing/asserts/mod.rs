pub mod assignment_that;
mod assignment_that_tests;
pub mod binary_that;
mod binary_that_tests;
pub mod binomial_that;
mod binomial_that_tests;
pub mod bool_that;
mod bool_that_tests;
pub mod conditional_that;
mod conditional_that_tests;
pub mod cracovian_that;
mod cracovian_that_that;
pub mod equation_that;
mod equation_that_tests;
pub mod error_that;
pub mod exp_that;
mod exp_that_tests;
pub mod expr_that;
mod expr_that_tests;
pub mod frac_that;
mod frac_that_tests;
pub mod func_that;
mod func_that_tests;
pub mod int_that;
mod int_that_tests;
pub mod log_that;
mod log_that_tests;
pub mod matrix_that;
mod matrix_that_tests;
pub mod method_that;
mod method_that_tests;
pub mod object_that;
mod object_that_tests;
pub mod root_that;
mod root_that_tests;
pub mod soe_that;
mod soe_that_tests;
pub mod symbol_that;
mod symbol_that_tests;
mod testing;
pub mod unary_that;
mod unary_that_tests;
pub mod variable_that;
mod variable_that_tests;


use matm_error::error::CalcError;
use crate::expressions::Expression;
use crate::expressions::cracovians::CracovianData;
use crate::expressions::matrices::MatrixData;
use crate::expressions::variable::VariableData;
use crate::testing::asserts::cracovian_that::CracovianAssertThat;
use crate::testing::asserts::expr_that::ExpressionAssertThat;
use crate::testing::asserts::matrix_that::MatrixAssertThat;
use crate::testing::asserts::variable_that::VariableAssertThat;

use self::error_that::ErrorAssertThat;


pub fn assert_expression_that<'a>(expr: &'a Expression
) -> ExpressionAssertThat<'a> {
    return ExpressionAssertThat {
        expression: expr,
    };
}

pub fn assert_matrix_that(mx: &MatrixData) -> MatrixAssertThat {
    MatrixAssertThat {
        matrix: mx,
    }
}

pub fn assert_cracovian_that(obj: &CracovianData) -> CracovianAssertThat {
    CracovianAssertThat {
        object: obj,
    }
}

pub fn assert_variable_that(v: &VariableData) -> VariableAssertThat {
    VariableAssertThat {
        variable: v,
    }
}

pub fn assert_result_ok<T>(expr: Result<T, CalcError>) -> T {
    match expr {
        Ok(res) => {
            return res;
        }
        Err(err) => {
            eprintln!("{}", err.message);//TODO write red color
            assert!(false, "Result not available");
            unreachable!()
        }
    }
}

pub fn assert_result_error_that<T>(result: Result<T, CalcError>) -> ErrorAssertThat {
    match result {
        Err(err) => {
            return ErrorAssertThat {
                error: err,
            };
        }
        Ok(..) => {
            assert!(false, "Expected error, Ok was returned");
            unreachable!()
        }
    }
}
