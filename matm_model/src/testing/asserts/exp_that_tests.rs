#[cfg(test)]

mod tests {
    use crate::expressions::exponentiation::ExpData;
    use crate::factories::ObjFactory;
    use crate::testing::asserts::exp_that::ExpAssertThat;

    #[test]
    fn test_has_base_that() {
        let fa = ObjFactory::new10();
        let obj = ExpAssertThat {
            value: &ExpData::new(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.has_base_that(|e| { e.as_integer().is_int(2); });
    }

    #[test]
    #[should_panic(expected = "Expected integer, actual is symbol: a")]
    fn test_fail_has_base_that() {
        let fa = ObjFactory::new10();
        let obj = ExpAssertThat {
            value: &ExpData::new(fa.rsn("a"), fa.rzi_u(3)),
        };

        obj.has_base_that(|e| { e.as_integer(); });
    }

    #[test]
    fn test_is_base_integer_that() {
        let fa = ObjFactory::new10();
        let obj = ExpAssertThat {
            value: &ExpData::new(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.is_base_integer_that(|e| { e.is_int(2); });
    }

    #[test]
    #[should_panic(expected = "Expected integer, actual is symbol: a")]
    fn test_fail_is_base_integer_that() {
        let fa = ObjFactory::new10();
        let obj = ExpAssertThat {
            value: &ExpData::new(fa.rsn("a"), fa.rzi_u(3)),
        };

        obj.is_base_integer_that(|_| ());
    }

    #[test]
    fn test_has_exponent_that() {
        let fa = ObjFactory::new10();
        let obj = ExpAssertThat {
            value: &ExpData::new(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.has_exponent_that(|e| { e.as_integer().is_int(3); });
    }

    #[test]
    #[should_panic(expected = "Expected integer, actual is symbol: a")]
    fn test_fail_has_exponent_that() {
        let fa = ObjFactory::new10();
        let obj = ExpAssertThat {
            value: &ExpData::new(fa.rzi_u(2), fa.rsn("a")),
        };

        obj.has_exponent_that(|e| { e.as_integer(); });
    }

    #[test]
    fn test_is_exponent_integer_that() {
        let fa = ObjFactory::new10();
        let obj = ExpAssertThat {
            value: &ExpData::new(fa.rzi_u(2), fa.rzi_u(3)),
        };

        obj.is_exponent_integer_that(|e| { e.is_int(3); });
    }

    #[test]
    #[should_panic(expected = "Expected integer, actual is symbol: a")]
    fn test_fail_is_exponent_integer_that() {
        let fa = ObjFactory::new10();
        let obj = ExpAssertThat {
            value: &ExpData::new(fa.rzi_u(2), fa.rsn("a")),
        };

        obj.is_exponent_integer_that(|_| ());
    }
}
