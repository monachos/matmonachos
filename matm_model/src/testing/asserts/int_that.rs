use crate::expressions::int::IntData;


pub struct IntegerAssertThat<'a> {
    pub value: &'a IntData,
}

impl <'a> IntegerAssertThat<'a> {
    pub fn has_radix(self, expected: u32) -> IntegerAssertThat<'a> {
        assert_eq!(expected, self.value.radix(),
                   "Expected integer of radix {}, actual is {}",
                   expected,
                   self.value.radix());
        return self;
    }

    pub fn is_int(self, expected: i32) -> IntegerAssertThat<'a> {
        assert_eq!(expected,
                   self.value.to_i32().unwrap(),
                   "Expected integer {}, actual is {}",
                   expected,
                   self.value.to_i32().unwrap());
        return self;
    }

    pub fn is_positive(self) -> IntegerAssertThat<'a> {
        assert_eq!(true, self.value.is_positive(),
                   "Expected positive integer");
        return self;
    }

    pub fn is_negative(self) -> IntegerAssertThat<'a> {
        assert_eq!(true, self.value.is_negative(),
                   "Expected negative integer");
        return self;
    }

    pub fn is_zero(self) -> IntegerAssertThat<'a> {
        assert_eq!(true, self.value.is_zero(),
                   "Expected zero integer");
        return self;
    }

    pub fn is_equal_to(self, expected: IntData) -> IntegerAssertThat<'a> {
        assert_eq!(expected, *self.value,
                   "Expected integer {}, actual is {}",
                   expected,
                   self.value);
        return self;
    }
}

