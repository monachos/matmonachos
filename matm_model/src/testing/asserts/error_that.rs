use matm_error::error::CalcError;


pub struct ErrorAssertThat {
    pub error: CalcError,
}

impl <'a> ErrorAssertThat {
    pub fn has_message(self, expected: &str) -> ErrorAssertThat {
        assert_eq!(expected, self.error.message);
        return self;
    }
}
