use crate::expressions::binomial::BinomialData;
use crate::testing::asserts::assert_expression_that;
use crate::testing::asserts::frac_that::FractionAssertThat;
use crate::testing::asserts::int_that::IntegerAssertThat;
use crate::testing::asserts::symbol_that::SymbolAssertThat;

use super::expr_that::ExpressionAssertThat;

pub struct BinomialAssertThat<'a> {
    pub value: &'a BinomialData,
}

impl <'a> BinomialAssertThat<'a> {
    pub fn has_set_that(self, f: fn(ExpressionAssertThat) -> ()) -> BinomialAssertThat<'a> {
        let handler = assert_expression_that(&self.value.set);
        f(handler);
        return self;
    }

    pub fn has_subset_that(self, f: fn(ExpressionAssertThat) -> ()) -> BinomialAssertThat<'a> {
        let handler = assert_expression_that(&self.value.subset);
        f(handler);
        return self;
    }

    pub fn is_set_integer_that(self, f: fn(IntegerAssertThat) -> ()) -> BinomialAssertThat<'a> {
        let handler = assert_expression_that(&self.value.set)
            .as_integer();
        f(handler);
        return self;
    }

    pub fn is_subset_integer_that(self, f: fn(IntegerAssertThat) -> ()) -> BinomialAssertThat<'a> {
        let handler = assert_expression_that(&self.value.subset)
            .as_integer();
        f(handler);
        return self;
    }

    pub fn is_set_fraction_that(self, f: fn(FractionAssertThat) -> ()) -> BinomialAssertThat<'a> {
        let handler = assert_expression_that(&self.value.set)
            .as_fraction();
        f(handler);
        return self;
    }

    pub fn is_subset_fraction_that(self, f: fn(FractionAssertThat) -> ()) -> BinomialAssertThat<'a> {
        let handler = assert_expression_that(&self.value.subset)
            .as_fraction();
        f(handler);
        return self;
    }

    pub fn is_set_symbol_that(self, f: fn(SymbolAssertThat) -> ()) -> BinomialAssertThat<'a> {
        let handler = assert_expression_that(&self.value.set)
            .as_symbol();
        f(handler);
        return self;
    }

    pub fn is_subset_symbol_that(self, f: fn(SymbolAssertThat) -> ()) -> BinomialAssertThat<'a> {
        let handler = assert_expression_that(&self.value.subset)
            .as_symbol();
        f(handler);
        return self;
    }
}
