#[cfg(test)]
mod tests {
    use matm_error::error::CalcError;
    use crate::testing::asserts::assert_result_error_that;
    use crate::testing::asserts::assert_result_ok;

    #[test]
    #[should_panic(expected = "Result not available")]
    fn test_assert_result_ok_fail() {
        let input: Result<i32, CalcError> = Err(CalcError::from_str("abc"));

        assert_result_ok(input);
    }

    #[test]
    #[should_panic(expected = "Expected error, Ok was returned")]
    fn test_assert_result_error_that_fail() {
        let input = Ok(1);

        assert_result_error_that(input);
    }
}
