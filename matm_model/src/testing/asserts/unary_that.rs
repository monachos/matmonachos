use crate::testing::asserts::assert_expression_that;
use crate::testing::asserts::int_that::IntegerAssertThat;
use crate::testing::asserts::frac_that::FractionAssertThat;
use crate::testing::asserts::symbol_that::SymbolAssertThat;
use crate::testing::asserts::matrix_that::MatrixAssertThat;
use crate::testing::asserts::binary_that::BinaryAssertThat;
use crate::expressions::unary::UnaryData;
use crate::expressions::unary::UnaryOperator;


pub struct UnaryAssertThat<'a> {
    pub value: &'a UnaryData,
}

impl <'a> UnaryAssertThat<'a> {
    pub fn is_operator(self, expected: UnaryOperator) -> UnaryAssertThat<'a> {
        assert_eq!(expected, self.value.operator,
                   "Expected operator to be {}, actual is {}",
                   expected,
                   self.value.operator);
        return self;
    }

    pub fn is_group(self) -> UnaryAssertThat<'a> {
        return self.is_operator(UnaryOperator::Group);
    }

    pub fn is_neg(self) -> UnaryAssertThat<'a> {
        return self.is_operator(UnaryOperator::Neg);
    }

    pub fn is_transpose(self) -> UnaryAssertThat<'a> {
        return self.is_operator(UnaryOperator::Transpose);
    }

    pub fn is_determinant(self) -> UnaryAssertThat<'a> {
        return self.is_operator(UnaryOperator::Determinant);
    }

    pub fn is_rank(self) -> UnaryAssertThat<'a> {
        return self.is_operator(UnaryOperator::Rank);
    }

    pub fn is_inversion(self) -> UnaryAssertThat<'a> {
        return self.is_operator(UnaryOperator::Inversion);
    }

    pub fn is_factorial(self) -> UnaryAssertThat<'a> {
        return self.is_operator(UnaryOperator::Factorial);
    }

    pub fn has_format(self, expected: &str) -> UnaryAssertThat<'a> {
        let actual = self.value.to_string();
        assert_eq!(expected, actual,
                   "Expected format {}, actual is {}",
                   expected,
                   actual);
        return self;
    }

    pub fn as_integer(self) -> IntegerAssertThat<'a> {
        return assert_expression_that(&self.value.value).as_integer();
    }

    pub fn as_fraction(self) -> FractionAssertThat<'a> {
        return assert_expression_that(&self.value.value).as_fraction();
    }

    pub fn as_symbol(self) -> SymbolAssertThat<'a> {
        return assert_expression_that(&self.value.value).as_symbol();
    }

    pub fn as_matrix(self) -> MatrixAssertThat<'a> {
        return assert_expression_that(&self.value.value).as_matrix();
    }

    pub fn as_unary(self) -> UnaryAssertThat<'a> {
        return assert_expression_that(&self.value.value).as_unary();
    }

    pub fn as_binary(self) -> BinaryAssertThat<'a> {
        return assert_expression_that(&self.value.value).as_binary();
    }

}
