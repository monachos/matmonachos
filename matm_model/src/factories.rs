use std::rc::Rc;

use matm_error::error::CalcError;
use crate::core::natural::RADIX_10;
use crate::expressions::int::IntData;
use crate::expressions::root::RootData;
use crate::expressions::transcendental::TranscData;
use crate::expressions::Expression;
use crate::expressions::ToExpression;
use crate::expressions::fractions::FracData;
use crate::expressions::symbol::SymbolData;


pub struct ObjFactory {
    radix: u32,
}

impl ObjFactory {
    pub fn new(radix: u32) -> Self {
        Self {
            radix: radix,
        }
    }

    pub fn new10() -> Self {
        Self::new(RADIX_10)
    }

    pub fn radix(&self) -> u32 {
        self.radix
    }

    /// creates Int object from i32 value
    pub fn zi(&self, value: i32) -> Result<IntData, CalcError> {
        IntData::from_int(self.radix, value)
    }

    /// creates Int object from i32 value
    pub fn zi_u(&self, value: i32) -> IntData {
        self.zi(value).unwrap()
    }

    /// creates Int object from usize value
    pub fn zu(&self, value: usize) -> Result<IntData, CalcError> {
        return self.zi(value as i32);
    }

    /// creates Int object from usize value
    pub fn zu_u(&self, value: usize) -> IntData {
        return self.zi_u(value as i32);
    }

    /// creates Int object from string
    pub fn zs(&self, value: &str) -> Result<IntData, CalcError> {
        return IntData::from_string(self.radix, value);
    }

    /// creates Int object from string
    pub fn zs_u(&self, value: &str) -> IntData {
        return self.zs(value).unwrap();
    }

    /// creates Int object representing zero
    pub fn z0(&self) -> IntData {
        return self.zi_u(0);
    }

    /// creates Int object representing one
    pub fn z1(&self) -> IntData {
        return self.zi_u(1);
    }

    pub fn ez(&self, value: IntData) -> Expression {
        Expression::Int(Box::new(value))
    }

    pub fn rz(&self, value: IntData) -> Rc<Expression> {
        Rc::new(Expression::Int(Box::new(value)))
    }

    pub fn ezi(&self, value: i32) -> Result<Expression, CalcError> {
        return Ok(Expression::Int(Box::new(self.zi(value)?)));
    }

    pub fn rzi(&self, value: i32) -> Result<Rc<Expression>, CalcError> {
        return Ok(Rc::new(Expression::Int(Box::new(self.zi(value)?))));
    }

    pub fn ezi_u(&self, value: i32) -> Expression {
        return self.ezi(value).unwrap();
    }

    pub fn rzi_u(&self, value: i32) -> Rc<Expression> {
        return Rc::new(self.ezi_u(value));
    }

    pub fn ezu(&self, value: usize) -> Result<Expression, CalcError> {
        return self.ezi(value as i32);
    }

    pub fn rzu(&self, value: usize) -> Result<Rc<Expression>, CalcError> {
        return self.rzi(value as i32);
    }

    pub fn ezu_u(&self, value: usize) -> Expression {
        return self.ezi_u(value as i32);
    }

    pub fn rzu_u(&self, value: usize) -> Rc<Expression> {
        return self.rzi_u(value as i32);
    }

    pub fn ezs(&self, value: &str) -> Result<Expression, CalcError> {
        return Ok(self.zs(value)?.to_expr());
    }

    pub fn rzs(&self, value: &str) -> Result<Rc<Expression>, CalcError> {
        return Ok(self.zs(value)?.to_rc_expr());
    }

    pub fn ezs_u(&self, value: &str) -> Expression {
        return self.ezs(value).unwrap();
    }

    pub fn rzs_u(&self, value: &str) -> Rc<Expression> {
        return self.rzs(value).unwrap();
    }

    pub fn ez0(&self) -> Expression {
        return self.z0().to_expr();
    }

    pub fn rz0(&self) -> Rc<Expression> {
        return Rc::new(self.ez0());
    }

    pub fn ez1(&self) -> Expression {
        return self.z1().to_expr();
    }

    pub fn rz1(&self) -> Rc<Expression> {
        return Rc::new(self.ez1());
    }

    pub fn fe(&self, num: Expression, den: Expression) -> FracData {
        return FracData::new(Rc::new(num), Rc::new(den));
    }

    pub fn fr(&self, num: Rc<Expression>, den: Rc<Expression>) -> FracData {
        return FracData::new(num, den);
    }

    pub fn fi(&self, num: i32, den: i32) -> Result<FracData, CalcError> {
        return Ok(self.fe(self.ezi(num)?, self.ezi(den)?));
    }

    pub fn fi_u(&self, num: i32, den: i32) -> FracData {
        return self.fi(num, den).unwrap();
    }

    pub fn efe(&self, num: Expression, den: Expression) -> Expression {
        return self.fe(num, den).to_expr();
    }

    pub fn rfe(&self, num: Expression, den: Expression) -> Rc<Expression> {
        return self.fe(num, den).to_rc_expr();
    }

    pub fn efr(&self, num: Rc<Expression>, den: Rc<Expression>) -> Expression {
        return self.fr(num, den).to_expr();
    }

    pub fn rfr(&self, num: Rc<Expression>, den: Rc<Expression>) -> Rc<Expression> {
        return self.fr(num, den).to_rc_expr();
    }

    pub fn efb(&self, num: IntData, den: IntData) -> Expression {
        return self.efe(self.ez(num), self.ez(den));
    }

    pub fn rfb(&self, num: IntData, den: IntData) -> Rc<Expression> {
        return self.rfe(self.ez(num), self.ez(den));
    }

    pub fn efi(&self, num: i32, den: i32) -> Result<Expression, CalcError> {
        return Ok(self.fi(num, den)?.to_expr());
    }

    pub fn rfi(&self, num: i32, den: i32) -> Result<Rc<Expression>, CalcError> {
        return Ok(self.fi(num, den)?.to_rc_expr());
    }

    pub fn efi_u(&self, num: i32, den: i32) -> Expression {
        return self.efi(num, den).unwrap();
    }

    pub fn rfi_u(&self, num: i32, den: i32) -> Rc<Expression> {
        return Rc::new(self.efi(num, den).unwrap());
    }

    pub fn sn(&self, name: &str) -> SymbolData {
        return SymbolData::new(name);
    }

    pub fn esn(&self, name: &str) -> Expression {
        return self.sn(name).to_expr();
    }

    pub fn rsn(&self, name: &str) -> Rc<Expression> {
        return Rc::new(self.esn(name));
    }

    pub fn epi(&self) -> Expression {
        return Expression::Transc(Box::new(TranscData::Pi));
    }

    pub fn rpi(&self) -> Rc<Expression> {
        return Rc::new(Expression::Transc(Box::new(TranscData::Pi)));
    }

    pub fn ee(&self) -> Expression {
        return Expression::Transc(Box::new(TranscData::Euler));
    }

    pub fn re(&self) -> Rc<Expression> {
        return Rc::new(Expression::Transc(Box::new(TranscData::Euler)));
    }

    pub fn rootz(&self, radicand: i32) -> Result<RootData, CalcError> {
        return Ok(RootData::new(self.rzi(radicand)?, self.rzi_u(2)));
    }

    pub fn erootz(&self, radicand: i32) -> Result<Expression, CalcError> {
        return Ok(self.rootz(radicand)?.to_expr());
    }

    pub fn rrootz(&self, radicand: i32) -> Result<Rc<Expression>, CalcError> {
        return Ok(self.rootz(radicand)?.to_rc_expr());
    }

}

