use std::fmt;

pub mod core;
pub mod expressions;
pub mod factories;
mod factories_tests;
pub mod grid;
pub mod macros;
pub mod statements;
pub mod testing;


fn write_wrapped_obj(
    f: &mut fmt::Formatter<'_>,
    obj: &dyn fmt::Display,
    should_wrap: bool,
) -> fmt::Result {
    if should_wrap {
        write!(f, "({})", obj)
    } else {
        write!(f, "{}", obj)
    }
}
