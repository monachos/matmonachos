use std::fmt;
use crate::core::positional::PositionalNumber;
use crate::statements::Statement;
use crate::statements::ToStatement;

#[derive(Clone, Debug)]
pub struct TabularDivisionData {
    pub dividend: PositionalNumber,
    pub divisor: PositionalNumber,
    pub lines: Vec<TabularDivisionLine>,
    pub quotient: PositionalNumber,
}

impl ToStatement for TabularDivisionData {
    fn to_statement(self) -> Statement {
        return Statement::TabularDiv(self);
    }
}

impl fmt::Display for TabularDivisionData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.quotient)
    }
}

//---------------------------------------------------------

#[derive(Clone, Debug)]
pub enum TabularDivisionLine {
    Separator,
    Number(PositionalNumber),
}


#[cfg(test)]
mod tests {
    use crate::core::natural::Natural;
    use crate::core::natural::RADIX_10;
    use crate::core::positional::PositionalNumber;
    use crate::statements::tabular_division::TabularDivisionData;

    #[test]
    fn test_format_natural_division_step() {
        let step = TabularDivisionData {
            dividend: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 1).unwrap()),
            divisor: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 2).unwrap()),
            lines: Vec::new(),
            quotient: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 123).unwrap()),
        };
        assert_eq!("123", format!("{}", step));
    }
}
