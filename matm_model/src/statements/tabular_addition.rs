use std::fmt;
use crate::core::positional::PositionalNumber;
use crate::statements::Statement;
use crate::statements::ToStatement;

#[derive(Clone, Debug)]
pub struct TabularAdditionData {
    pub addends: Vec<PositionalNumber>,
    pub sum: PositionalNumber,
    pub carry: PositionalNumber,
}

impl ToStatement for TabularAdditionData {
    fn to_statement(self) -> Statement {
        return Statement::TabularAdd(self);
    }
}

impl fmt::Display for TabularAdditionData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.sum)
    }
}


#[cfg(test)]
mod tests {
    use crate::core::natural::{Natural, RADIX_10};
    use crate::core::positional::PositionalNumber;
    use crate::statements::tabular_addition::TabularAdditionData;

    #[test]
    fn test_format_natural_addition_step() {
        let step = TabularAdditionData {
            addends: Vec::new(),
            sum: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 123).unwrap()),
            carry: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 10).unwrap()),
        };
        assert_eq!("123", format!("{}", step));
    }
}
