use std::fmt;
use crate::core::positional::PositionalNumber;
use crate::statements::Statement;
use crate::statements::ToStatement;

#[derive(Clone, Debug)]
pub struct TabularMultiplicationData {
    pub multiplier: PositionalNumber,
    pub multiplicand: PositionalNumber,
    pub partial_products: Vec<PositionalNumber>,
    pub multiplication_carries: Vec<PositionalNumber>,
    pub addition_carry: PositionalNumber,
    pub product: PositionalNumber,
}

impl ToStatement for TabularMultiplicationData {
    fn to_statement(self) -> Statement {
        return Statement::TabularMul(self);
    }
}

impl fmt::Display for TabularMultiplicationData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.product)
    }
}


#[cfg(test)]
mod tests {
    use crate::core::natural::Natural;
    use crate::core::natural::RADIX_10;
    use crate::core::positional::PositionalNumber;
    use crate::statements::tabular_multiplication::TabularMultiplicationData;

    #[test]
    fn test_format_natural_multiplication_step() {
        let step = TabularMultiplicationData {
            multiplier: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 1).unwrap()),
            multiplicand: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 2).unwrap()),
            partial_products: Vec::new(),
            multiplication_carries: Vec::new(),
            addition_carry: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 3).unwrap()),
            product: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 123).unwrap()),
        };
        assert_eq!("123", format!("{}", step));
    }
}
