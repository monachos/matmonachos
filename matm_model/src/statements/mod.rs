pub mod expression;
pub mod factorization;
pub mod text;
pub mod tabular_addition;
pub mod tabular_subtraction;
pub mod tabular_multiplication;
pub mod tabular_division;

use crate::statements::expression::ExpressionStep;
use crate::statements::text::TextStatement;
use std::fmt;
use crate::statements::tabular_addition::TabularAdditionData;
use crate::statements::tabular_division::TabularDivisionData;
use crate::statements::tabular_multiplication::TabularMultiplicationData;
use crate::statements::tabular_subtraction::TabularSubtractionData;
use self::factorization::TrialDivisionStep;


#[derive(Clone, Debug)]
pub enum Statement {
    ExprStep(ExpressionStep),
    Text(TextStatement),
    TabularAdd(TabularAdditionData),
    TabularSub(TabularSubtractionData),
    TabularMul(TabularMultiplicationData),
    TabularDiv(TabularDivisionData),
    TrialDivision(TrialDivisionStep),
}

impl Statement {
    pub fn type_name(&self) -> String {
        return match self {
            Statement::ExprStep(_) => String::from("expression step"),
            Statement::Text(_) => String::from("text"),
            Statement::TabularAdd(_) => String::from("tabular addition"),
            Statement::TabularSub(_) => String::from("tabular subtraction"),
            Statement::TabularMul(_) => String::from("tabular multiplication"),
            Statement::TabularDiv(_) => String::from("tabular division"),
            Statement::TrialDivision(_) => String::from("trial division"),
        }
    }
}

impl fmt::Display for Statement {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Statement::ExprStep(obj) => write!(f, "{}", obj),
            Statement::Text(obj) => write!(f, "{}", obj),
            Statement::TabularAdd(obj) => write!(f, "{}", obj),
            Statement::TabularSub(obj) => write!(f, "{}", obj),
            Statement::TabularMul(obj) => write!(f, "{}", obj),
            Statement::TabularDiv(obj) => write!(f, "{}", obj),
            Statement::TrialDivision(obj) => write!(f, "{}", obj),
        }
    }
}

//---------------------------------------

pub trait ToStatement {
    fn to_statement(self) -> Statement;
}

