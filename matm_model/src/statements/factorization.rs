use std::fmt;
use crate::expressions::int::IntData;
use super::Statement;
use super::ToStatement;


#[derive(Clone, Debug)]
pub struct TrialDivisionFactor {
    pub dividend: IntData,
    pub divisor: Option<IntData>,
}

#[derive(Clone, Debug)]
pub struct TrialDivisionStep {
    pub items: Vec<TrialDivisionFactor>,
}

impl TrialDivisionStep {
    pub fn new() -> Self {
        Self {
            items: Vec::new(),
        }
    }

    pub fn from_vector(items: Vec<TrialDivisionFactor>) -> Self {
        Self {
            items,
        }
    }
}

impl ToStatement for TrialDivisionStep {
    fn to_statement(self) -> Statement {
        return Statement::TrialDivision(self);
    }
}

impl fmt::Display for TrialDivisionStep {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut add_sep = false;
        for item in &self.items {
            if add_sep {
                write!(f, ",")?;
            }
            write!(f, "{}", item.dividend)?;
            if let Some(div) = &item.divisor {
                write!(f, "|{}", div)?;
            }
            add_sep = true;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::factories::ObjFactory;
    use crate::statements::factorization::TrialDivisionFactor;
    use crate::statements::factorization::TrialDivisionStep;

    #[test]
    fn test_trial_division_new() {
        let data = TrialDivisionStep::new();

        assert!(data.items.is_empty());
    }

    #[test]
    fn test_trial_division_format() {
        let fa = ObjFactory::new10();
        let mut data = TrialDivisionStep::new();
        data.items.push(TrialDivisionFactor {
            dividend: fa.zi_u(12),
            divisor: Some(fa.zi_u(2)),
        });
        data.items.push(TrialDivisionFactor {
            dividend: fa.zi_u(6),
            divisor: Some(fa.zi_u(2)),
        });
        data.items.push(TrialDivisionFactor {
            dividend: fa.zi_u(3),
            divisor: Some(fa.zi_u(3)),
        });
        data.items.push(TrialDivisionFactor {
            dividend: fa.zi_u(1),
            divisor: None,
        });

        let result = format!("{}", data);

        assert_eq!("12|2,6|2,3|3,1", result);
    }

}
