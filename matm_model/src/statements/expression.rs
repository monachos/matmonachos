use std::fmt;
use std::rc::Rc;
use crate::expressions::Expression;
use crate::statements::Statement;
use crate::statements::ToStatement;


#[derive(Clone, Debug)]
pub struct ExpressionStep {
    pub expression: Rc<Expression>,
    pub calculated_count: Option<usize>,
}

impl ExpressionStep {
    pub fn from_expression(expr: Rc<Expression>) -> Self {
        Self {
            expression: expr,
            calculated_count: None,
        }
    }

    pub fn from_expression_and_calculated_count(expr: Rc<Expression>, cc: usize) -> Self {
        Self {
            expression: expr,
            calculated_count: Some(cc),
        }
    }
}

impl ToStatement for ExpressionStep {
    fn to_statement(self) -> Statement {
        return Statement::ExprStep(self);
    }
}

impl fmt::Display for ExpressionStep {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.expression)
    }
}

