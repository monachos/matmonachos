use std::fmt;
use crate::core::positional::PositionalNumber;
use crate::statements::Statement;
use crate::statements::ToStatement;

#[derive(Clone, Debug)]
pub struct TabularSubtractionData {
    pub minuend: PositionalNumber,
    pub subtrahend: PositionalNumber,
    pub difference: PositionalNumber,
    pub borrowed: PositionalNumber,
}

impl ToStatement for TabularSubtractionData {
    fn to_statement(self) -> Statement {
        return Statement::TabularSub(self);
    }
}

impl fmt::Display for TabularSubtractionData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.difference)
    }
}


#[cfg(test)]
mod tests {
    use crate::core::natural::Natural;
    use crate::core::natural::RADIX_10;
    use crate::core::positional::PositionalNumber;
    use crate::statements::tabular_subtraction::TabularSubtractionData;

    #[test]
    fn test_format_natural_substraction_step() {
        let step = TabularSubtractionData {
            minuend: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 456).unwrap()),
            subtrahend: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 23).unwrap()),
            difference: PositionalNumber::from_natural(&Natural::from_int(RADIX_10, 433).unwrap()),
            borrowed: PositionalNumber::new(RADIX_10),
        };
        assert_eq!("433", format!("{}", step));
    }
}
