use std::collections::HashMap;
use std::fmt;
use std::rc::Rc;
use log::warn;
use crate::expressions::equation::EquationData;

use crate::expressions::Expression;
use crate::expressions::ToExpression;
use crate::statements::Statement;
use crate::statements::ToStatement;
use crate::statements::expression::ExpressionStep;


#[derive(Debug)]
pub enum TextStatementItem<'a> {
    Text(String),
    Statement(&'a Statement),
}

#[derive(Clone, PartialEq, Debug)]
pub enum TextCategory {
    Section,
    Paragraph,
}

#[derive(Clone, Debug)]
pub struct TextStatement {
    pub category: TextCategory,

    /// template message with placeholders for example "Returned value: {value}"
    pub text: String,

    pub params: HashMap<String, Statement>,
}

impl TextStatement {
    pub fn section(text: &str) -> Self {
        Self {
            category: TextCategory::Section,
            text: String::from(text),
            params: HashMap::new(),
        }
    }

    pub fn paragraph(text: &str) -> Self {
        Self {
            category: TextCategory::Paragraph,
            text: String::from(text),
            params: HashMap::new(),
        }
    }

    pub fn and_param(mut self, key: &str, value: Statement) -> Self {
        self.params.insert(String::from(key), value);
        return self;
    }

    pub fn and_expression(self, key: &str, expr: Rc<Expression>) -> Self {
        return self.and_param(key, Statement::ExprStep(ExpressionStep::from_expression(expr)));
    }

    pub fn and_equation(self, key: &str, left: Rc<Expression>, right: Rc<Expression>) -> Self {
        return self.and_expression(key, EquationData::new(left, right).to_rc_expr());
    }

    /// parses text template and returns parts of the text
    /// containing text (from message) or statement (from mapped placeholder).
    pub fn get_items(&self) -> Vec<TextStatementItem> {
        let mut result: Vec<TextStatementItem> = Vec::new();
        let mut in_placeholder = false;
        let mut current_token = String::new();
        for c in self.text.chars() {
            if in_placeholder {
                if c == '}' {
                    if !current_token.is_empty() {
                        if let Some(value) = self.params.get(&current_token) {
                            result.push(TextStatementItem::Statement(value));
                        } else {
                            warn!("Cannot find placeholder '{}' in text statement.", current_token);
                            result.push(TextStatementItem::Text(String::from("?")));
                        }
                    }
                    in_placeholder = false;
                    current_token = String::new();
                } else {
                    current_token.push(c);
                }

            } else {
                if c == '{' {
                    if !current_token.is_empty() {
                        result.push(TextStatementItem::Text(current_token));
                    }
                    in_placeholder = true;
                    current_token = String::new();
                } else {
                    current_token.push(c);
                }
            }
        }
        if !current_token.is_empty() {
            result.push(TextStatementItem::Text(current_token));
        }
        return result;
    }
}

impl ToStatement for TextStatement {
    fn to_statement(self) -> Statement {
        return Statement::Text(self);
    }
}

impl fmt::Display for TextStatement {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.category {
            TextCategory::Section => {
                write!(f, "# {}", self.text)
            }
            TextCategory::Paragraph => {
                write!(f, "{}", self.text)
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use assert_matches::assert_matches;

    use crate::factories::ObjFactory;
    use crate::statements::Statement;
    use crate::statements::expression::ExpressionStep;
    use crate::statements::text::TextStatement;
    use crate::statements::text::TextStatementItem;
    use crate::testing::asserts::assert_expression_that;

    #[test]
    fn test_new_section() {
        let expr = TextStatement::section("a");

        assert_eq!("a", expr.text);
    }

    #[test]
    fn test_new_paragraph() {
        let expr = TextStatement::paragraph("a");

        assert_eq!("a", expr.text);
    }

    #[test]
    fn test_format_section() {
        let expr = TextStatement::section("a");

        assert_eq!("# a", format!("{}", expr));
    }

    #[test]
    fn test_format_paragraph() {
        let expr = TextStatement::paragraph("a");

        assert_eq!("a", format!("{}", expr));
    }

    #[test]
    fn test_get_items_text() {
        let stmt = TextStatement::paragraph("Simple message");

        let mut it = stmt.get_items().into_iter();

        let item1 = it.next().unwrap();
        if let TextStatementItem::Text(t) = item1 {
            assert_eq!("Simple message", t);
        } else {
            assert!(false);
        }

        assert_matches!(it.next(), None);
    }

    #[test]
    fn test_get_items_text_ph() {
        let fa = ObjFactory::new10();
        let stmt = TextStatement::paragraph("My value: {value}")
            .and_param("value", Statement::ExprStep(ExpressionStep::from_expression(fa.rzi_u(23))));

        let mut it = stmt.get_items().into_iter();

        let item = it.next().unwrap();
        if let TextStatementItem::Text(t) = item {
            assert_eq!("My value: ", t);
        } else {
            assert!(false);
        }

        let item = it.next().unwrap();
        if let TextStatementItem::Statement(st) = item {
            if let Statement::ExprStep(expr) = st {
                assert_expression_that(&expr.expression)
                    .as_integer()
                    .is_int(23);
            } else {
                assert!(false);
            }

        } else {
            assert!(false);
        }

        assert_matches!(it.next(), None);
    }

    #[test]
    fn test_get_items_text_ph_text() {
        let fa = ObjFactory::new10();
        let stmt = TextStatement::paragraph("My value: {value}.")
            .and_param("value", Statement::ExprStep(ExpressionStep::from_expression(fa.rzi_u(23))));

        let mut it = stmt.get_items().into_iter();

        let item = it.next().unwrap();
        if let TextStatementItem::Text(t) = item {
            assert_eq!("My value: ", t);
        } else {
            assert!(false);
        }

        let item = it.next().unwrap();
        if let TextStatementItem::Statement(st) = item {
            if let Statement::ExprStep(expr) = st {
                assert_expression_that(&expr.expression)
                    .as_integer()
                    .is_int(23);
            } else {
                assert!(false);
            }

        } else {
            assert!(false);
        }

        let item = it.next().unwrap();
        if let TextStatementItem::Text(t) = item {
            assert_eq!(".", t);
        } else {
            assert!(false);
        }

        assert_matches!(it.next(), None);
    }

    #[test]
    fn test_get_items_text_unknown() {
        let stmt = TextStatement::paragraph("My value: {value}");

        let mut it = stmt.get_items().into_iter();

        let item = it.next().unwrap();
        if let TextStatementItem::Text(t) = item {
            assert_eq!("My value: ", t);
        } else {
            assert!(false);
        }

        let item = it.next().unwrap();
        if let TextStatementItem::Text(t) = item {
            assert_eq!("?", t);
        } else {
            assert!(false);
        }

        assert_matches!(it.next(), None);
    }

}
