use std::fmt;
use matm_error::error::CalcError;
use crate::core::index_range::IndexRange;


#[derive(PartialEq)]
#[derive(Debug)]
#[derive(Clone)]
pub struct SizeND {
    // Dimensions: ..., pages, rows, columns
    pub dimensions: Vec<usize>,
}

impl SizeND {
    pub fn new(dims: Vec<usize>) -> Result<Self, CalcError> {
        if dims.is_empty() {
            return Err(CalcError::from_str(
                "You need to specify at least one dimension."));
        }
        for d in &dims {
            if *d == 0 {
                return Err(CalcError::from_str(
                    "You need to specify positive size of all dimensions."));
            }
        }
        Ok(Self {
            dimensions: dims,
        })
    }

    pub fn get_elements_count(&self) -> usize {
        let mut p: usize = 1;
        for d in &self.dimensions {
            p *= d;
        }
        return p;
    }

    pub fn is_square(&self) -> bool {
        let mut it = self.dimensions.iter();
        let first_dim: &usize = it.next().unwrap();
        while let Some(d) = it.next() {
            if *d != *first_dim {
                return false;
            }
        }
        return true;
    }

    pub fn is_index_valid(&self, dimension_index: usize, index: usize) -> bool {
        if dimension_index >= self.dimensions.len() {
            return false;
        }
        let d = self.dimensions[dimension_index];
        return index >= 1 && index <= d;
    }

    pub fn is_index_range_valid(&self, dimension_index: usize, range: &IndexRange) -> bool {
        if dimension_index >= self.dimensions.len() {
            return false;
        }
        match range {
            IndexRange::Range(start, end) => {
                start.is_valid(self.dimensions[dimension_index])
                    && end.is_valid(self.dimensions[dimension_index])
            }
            IndexRange::All => {
                true
            }
        }
    }
}

impl fmt::Display for SizeND {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut add_sep = false;
        for d in &self.dimensions {
            if add_sep {
                f.write_str("x")?;
            }
            f.write_str(format!("{}", *d).as_str())?;
            add_sep = true;
        }
        return Ok(());
    }
}

//-----------------------------------------

#[derive(PartialEq)]
#[derive(Debug)]
#[derive(Clone)]
pub struct Size2D {
    pub rows: usize,
    pub columns: usize,
}

impl Size2D {
    pub fn new(r: usize, c: usize) -> Result<Self, CalcError> {
        if r == 0 || c == 0 {
            return Err(CalcError::from_str(
                "You need to specify positive size of all dimensions."));
        }
        Ok(Self {
            rows: r,
            columns: c,
        })
    }

    pub fn from_nd(size: &SizeND) -> Result<Self, CalcError> {
        if size.dimensions.len() != 2 {
            return Err(CalcError::from_string(format!(
                "Cannot convert multidimensional size {} to 2D size.",
                size)));
        }
        return Size2D::new(
            size.dimensions[0],
            size.dimensions[1]
        );
    }

    pub fn get_elements_count(&self) -> usize {
        return self.rows * self.columns;
    }

    pub fn is_square(&self) -> bool {
        return self.rows == self.columns;
    }

    pub fn is_row_valid(&self, r: usize) -> bool {
        return r >= 1 && r <= self.rows;
    }

    pub fn is_column_valid(&self, c: usize) -> bool {
        return c >= 1 && c <= self.columns;
    }

    pub fn is_row_range_valid(&self, range: &IndexRange) -> bool {
        match range {
            IndexRange::Range(start, end) => {
                start.is_valid(self.rows)
                    && end.is_valid(self.rows)
            }
            IndexRange::All => {
                true
            }
        }
    }

    pub fn is_column_range_valid(&self, range: &IndexRange) -> bool {
        match range {
            IndexRange::Range(start, end) => {
                start.is_valid(self.columns)
                    && end.is_valid(self.columns)
            }
            IndexRange::All => {
                true
            }
        }
    }
}

impl fmt::Display for Size2D {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str(format!("{}x{}", self.rows, self.columns).as_str())
    }
}

//-----------------------------------------
