use std::fmt;
use std::cmp::PartialEq;
use matm_error::error::CalcError;


#[derive(Clone)]
pub enum Index {
    /// absolute value of the index (1-base value)
    Absolute(usize),

    /// relative index value from end-value; value can be negative
    End(usize),
}

impl Index {
    pub fn one() -> Self {
        Self::Absolute(1)
    }

    pub fn abs(value: usize) -> Result<Self, CalcError> {
        if value >= 1 {
            Ok(Self::Absolute(value))
        } else {
            Err(CalcError::from_string(format!("Invalid index {value}")))
        }
    }

    pub fn end() -> Self {
        Self::End(0)
    }

    pub fn end_minus(offset: usize) -> Self {
        Self::End(offset)
    }

    pub fn calc_index(&self, max_index: usize) -> Result<usize, CalcError> {
        return match self {
            Index::Absolute(value) => {
                Ok(*value)
            }
            Index::End(offset) => {
                if max_index > *offset {
                    Ok(max_index - offset)
                } else {
                    Err(CalcError::from_string(format!("Invalid index offset {}, maximum offset is {}",
                                                       offset,
                                                       max_index - 1)))
                }
            }
        }
    }

    pub fn is_valid(&self, max_index: usize) -> bool {
        return match self {
            Index::Absolute(value) => {
                *value >= 1 && *value <= max_index
            }
            Index::End(offset) => {
                *offset < max_index
            }
        }
    }
}

impl fmt::Display for Index {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Index::Absolute(value) => {
                write!(f, "{}", value)
            }
            Index::End(offset) => {
                if *offset > 0 {
                    write!(f, "end-{}", *offset)
                } else {
                    write!(f, "end")
                }
            }
        }
    }
}

//------------------------------------------------

/// 1-based index of matrix
#[derive(PartialEq, Eq, Hash)]
pub struct IndexND {
    value: Vec<usize>,
}

impl IndexND {
    pub fn new(val: Vec<usize>) -> Result<Self, CalcError> {
        if val.is_empty() {
            return Err(CalcError::from_str(
                "You need to specify at least one index value."));
        }
        for v in &val {
            if *v == 0 {
                return Err(CalcError::from_string(format!(
                    "Invalid index {}.", Self::format_index(&val))));
            }
        }
        return Ok(Self {
            value: val,
        });
    }

    pub fn one2d() -> Self {
        Self {
            value: vec![1, 1],
        }
    }

    pub fn ones(dim_size: usize) -> Result<Self, CalcError> {
        if dim_size == 0 {
            return Err(CalcError::from_str(
                "You need to specify at least one dimension."));
        }
        Ok(Self {
            value: vec![1; dim_size],
        })
    }

    pub fn from_2d(idx: &Index2D) -> Self {
        return Self::new(vec![idx.row, idx.column]).unwrap();
    }

    pub fn value(&self) -> &Vec<usize> {
        &self.value
    }

    pub fn value_of(&self, dim_index: usize) -> Result<usize, CalcError> {
        if dim_index >= self.value.len() {
            return Err(CalcError::from_string(format!(
                "Invalid dimension index {}.",
                dim_index)));
        }
        return Ok(self.value[dim_index]);
    }

    pub fn format_index(idx: &Vec<usize>) -> String {
        let mut result: String = String::from("(");
        let mut add_sep = false;
        for value in idx {
            if add_sep {
                result += ", ";
            }
            result += format!("{}", value).as_str();
            add_sep = true;
        }
        result += ")";
        return result;
    }
}

impl fmt::Display for IndexND {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", Self::format_index(&self.value))
    }
}

//---------------------------------------

/// 1-based index of matrix
#[derive(PartialEq, Eq, Hash)]
pub struct Index2D {
    pub row: usize,
    pub column: usize,
}

impl Index2D {
    pub fn new(row: usize, col: usize) -> Result<Self, CalcError> {
        if row >= 1 && col >= 1 {
            Ok(Self {
                row,
                column: col,
            })
        } else {
            Err(CalcError::from_string(format!("Invalid index {}", Self::format_index(row, col))))
        }
    }

    pub fn one() -> Self {
        Self {
            row: 1,
            column: 1,
        }
    }

    pub fn left(&self) -> Result<Self, CalcError> {
        Self::new(self.row, self.column - 1)
    }

    pub fn left_up(&self) -> Result<Self, CalcError> {
        Self::new(self.row - 1, self.column - 1)
    }

    pub fn up(&self) -> Result<Self, CalcError> {
        Self::new(self.row - 1, self.column)
    }

    pub fn right_up(&self) -> Result<Self, CalcError> {
        Self::new(self.row - 1, self.column + 1)
    }

    pub fn right(&self) -> Result<Self, CalcError> {
        Self::new(self.row, self.column + 1)
    }

    pub fn right_down(&self) -> Result<Self, CalcError> {
        Self::new(self.row + 1, self.column + 1)
    }

    pub fn down(&self) -> Result<Self, CalcError> {
        Self::new(self.row + 1, self.column)
    }

    pub fn left_down(&self) -> Result<Self, CalcError> {
        Self::new(self.row + 1, self.column - 1)
    }

    pub fn format_index(row: usize, col: usize) -> String {
        return format!("({}, {})", row, col);
    }
}

impl fmt::Display for Index2D {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", Self::format_index(self.row, self.column))
    }
}

