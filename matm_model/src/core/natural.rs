use std::cmp::Ordering;
use std::cmp::PartialEq;
use std::cmp::PartialOrd;
use std::fmt;

use matm_error::error::CalcError;
use crate::core::positional::PositionalNumber;
use crate::core::positional::PositionalItem;
use super::digit::digit_to_string;


pub static RADIX_2: u32 = 2;
pub static RADIX_10: u32 = 10;
pub static RADIX_16: u32 = 16;
pub static RADIX_MIN: u32 = 1;
pub static RADIX_MAX: u32 = 36;


#[derive(Clone, Debug)]
pub struct Natural {
    radix: u32,
    digits: Vec<u8>,
}

impl Natural {
    pub fn zero(radix: u32) -> Result<Self, CalcError> {
        return Self::from_int(radix, 0);
    }

    pub fn one(radix: u32) -> Result<Self, CalcError> {
        return Self::from_int(radix, 1);
    }

    pub fn from_int(radix: u32, value: u32) -> Result<Self, CalcError> {
        Self::validate_radix(radix)?;

        let mut digits: Vec<u8> = Vec::new();

        if value > 0 {
            let mut v = value;
            loop {
                digits.push((v % radix) as u8);
                v /= radix;
                if v == 0 {
                    break;
                }
            }
        } else {
            digits.push(0);
        }

        Ok(Self {
            radix: radix,
            digits: digits.clone(),
        })
    }

    pub fn from_vector(radix: u32, digits: Vec<u8>) -> Result<Self, CalcError> {
        Self::validate_radix(radix)?;
        if digits.is_empty() {
            return Err(CalcError::from_str("Cannot create number from empty vector."));
        }
        for d in &digits {
            if u32::from(*d) >= radix {
                return Err(CalcError::from_string(format!("Specified digit {} is invalid in radix {}.", d, radix)));
            }
        }
        let normalized_digits: Vec<u8> = digits.iter()
            .copied()
            .skip_while(|d| *d == 0)
            .collect::<Vec<_>>()
            .iter()
            .rev()
            .copied()
            .collect();
        if normalized_digits.is_empty() {
            return Self::zero(radix);
        }

        Ok(Self {
            radix: radix,
            digits: normalized_digits.clone(),
        })
    }

    pub fn from_string(radix: u32, value: &str) -> Result<Self, CalcError> {
        Self::validate_radix(radix)?;
        let mut dig: Vec<u8> = Vec::new();
        if value.is_empty() {
            return Err(CalcError::from_str("Cannot create number from empty string."));
        }

        for s in value.chars() {
            if s.is_digit(radix) {
                match s.to_digit(radix) {
                    Some(v) => {
                        dig.push(v as u8);
                    }
                    None => {
                        return Err(CalcError::from_string(Self::get_error_bad_string(value)));
                    }
                }
            } else {
                return Err(CalcError::from_string(Self::get_error_bad_string(value)));
            }
        }
        return Self::from_vector(radix, dig);
    }

    pub fn from_positional_number(number: &PositionalNumber) -> Result<Self, CalcError> {
        let mut digits: Vec<u8> = Vec::new();
        for digit in number.digits.iter().rev() {
            match *digit {
                PositionalItem::None | PositionalItem::Unknown => {
                    if !digits.is_empty() {
                        digits.push(0);
                    }
                }
                PositionalItem::Digit(d) => {
                    digits.push(d.value());
                }
            }
        }
        return Self::from_vector(number.radix, digits);
    }

    fn get_error_bad_string(value: &str) -> String {
        return format!("Cannot create number using the string: '{}'", value);
    }

    fn validate_radix(radix: u32) -> Result<(), CalcError> {
        if radix > RADIX_MAX {
            return Err(CalcError::from_string(format!(
                "Specified radix {} cannot be greater than {}.",
                radix,
                RADIX_MAX)));
        }
        return Ok(());
    }

    pub fn to_u32(&self) -> Option<u32> {
        let max_value = Self::from_int(self.radix, u32::MAX).unwrap();
        return if self.le(&max_value) {
            let result = self.to_u32_nocheck();
            Some(result)
        } else {
            None
        }
    }

    fn to_u32_nocheck(&self) -> u32 {
        let mut result: u32 = 0;
        let mut factor: u32 = 1;
        let mut update_factor = false;
        for d in &self.digits {
            if update_factor {
                factor *= self.radix;
            }
            let item_value = *d as u32 * factor;
            result += item_value;
            update_factor = true;
        }
        return result;
    }

    pub fn radix(&self) -> u32 {
        return self.radix;
    }

    pub fn digits(&self) -> &Vec<u8> {
        return &self.digits;
    }

    pub fn is_zero(&self) -> bool {
        return self.digits.len() == 1 && self.digits[0] == 0;
    }

    pub fn is_one(&self) -> bool {
        return self.digits.len() == 1 && self.digits[0] == 1;
    }

    pub fn get_digit(&self, pos: usize) -> Option<u8> {
        return self.digits.get(pos).copied();
    }

    pub fn get_digit_else_error(&self, pos: usize) -> Result<u8, CalcError> {
        return match self.get_digit(pos) {
            Some(d) => Ok(d),
            None => Err(CalcError::from_string(format!("Digit not found at index {}.", pos))),
        };
    }

    pub fn get_digit_else_zero(&self, pos: usize) -> u8 {
        return match self.get_digit(pos) {
            Some(d) => d,
            None => 0,
        };
    }

    pub fn set_digit(&mut self, pos: usize, digit: u8) -> Result<(), CalcError> {
        if u32::from(digit) >= self.radix {
            return Err(CalcError::from_string(format!("Invalid digit value {} in radix {}.", digit, self.radix)));
        }
        while pos >= self.digits.len() {
            self.digits.push(0);
        }
        if let Some(elem) = self.digits.get_mut(pos) {
            *elem = digit;
        }
        self.remove_unnecessary_zeros();
        return Ok(());
    }

    fn remove_unnecessary_zeros(&mut self) {
        while self.digits.len() > 1 && match self.digits.last() {
            Some(d) => *d == 0,
            None => false,
        } {
            self.digits.remove(self.digits.len() - 1);
        }
    }

    pub fn append_digit_right(&mut self, digit: u8) -> Result<(), CalcError> {
        if u32::from(digit) >= self.radix {
            return Err(CalcError::from_string(format!("Invalid digit value {} in radix {}.", digit, self.radix)));
        }
        self.digits.insert(0, digit);
        self.remove_unnecessary_zeros();
        return Ok(());
    }

    /// Gets number og the digits in the number without sign.
    pub fn digits_len(&self) -> usize {
        return self.digits.len();
    }

    pub fn iter(&self) -> std::iter::Rev<std::slice::Iter<u8>> {
        return self.digits.iter().rev();
    }

    pub fn get_max_digits_length(items: &Vec<Natural>) -> usize {
        return match items.iter()
            .map(|v| v.digits_len())
            .max() {
                Some(len) => len,
                None => 0,
            };
    }

}


impl PartialEq for Natural {
    fn eq(&self, other: &Natural) -> bool {
        if self.radix == other.radix && self.digits.len() == other.digits.len() {
            return self.digits.eq(&other.digits);
        }
        return false;
    }
}

impl PartialOrd for Natural {
    fn partial_cmp(&self, other: &Natural) -> Option<Ordering> {
        if self.radix != other.radix {
            return None;
        }

        if self.digits.len() < other.digits.len() {
            return Some(Ordering::Less);
        } else if self.digits.len() > other.digits.len() {
            return Some(Ordering::Greater);
        } else {
            for i in (0..self.digits.len()).rev() {
                if self.digits[i] < other.digits[i] {
                    return Some(Ordering::Less);
                } else if self.digits[i] > other.digits[i] {
                    return Some(Ordering::Greater);
                }
            }
        }
        return Some(Ordering::Equal);
    }
}

//-----------------------------------------------------

impl fmt::Display for Natural {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.radix != RADIX_10 {
            write!(f, "{}#", self.radix)?;
        }
        write!(f, "{}", &self.iter()
            .map(|d| digit_to_string(self.radix, *d))
            .collect::<Vec<_>>()
            .join(""))?;
        if self.radix != RADIX_10 {
            write!(f, "#")?;
        }
        Ok(())
    }
}

