#[cfg(test)]
mod tests {
    use rstest::rstest;
    use crate::core::index::Index;
    use crate::core::index_range::IndexRange;
    use crate::core::size::Size2D;
    use crate::core::size::SizeND;
    use crate::testing::asserts::assert_result_error_that;

    #[test]
    fn test_new() {
        let s = Size2D::new(2, 3).unwrap();
        assert_eq!(2, s.rows);
        assert_eq!(3, s.columns);
    }

    #[test]
    fn test_new_invalid_row_size() {
        let result = Size2D::new(0, 1);
        assert_result_error_that(result)
            .has_message("You need to specify positive size of all dimensions.");
    }

    #[test]
    fn test_from_nd() {
        let s = Size2D::from_nd(
            &SizeND::new(vec![2, 3]).unwrap())
            .unwrap();
        assert_eq!(2, s.rows);
        assert_eq!(3, s.columns);
    }

    #[test]
    fn test_from_nd_invalid_dimension() {
        let s = Size2D::from_nd(
            &SizeND::new(vec![2, 3, 4]).unwrap());
        assert_result_error_that(s)
            .has_message("Cannot convert multidimensional size 2x3x4 to 2D size.");
    }

    #[test]
    fn test_get_elements_count() {
        let s = Size2D::new(2, 3).unwrap();
        assert_eq!(6, s.get_elements_count());
    }

    #[test]
    fn test_is_square_2_2() {
        let s = Size2D::new(2, 2).unwrap();
        assert_eq!(true, s.is_square());
    }

    #[test]
    fn test_is_square_2_3() {
        let s = Size2D::new(2, 3).unwrap();
        assert_eq!(false, s.is_square());
    }

    #[rstest]
    #[case(0, false)]
    #[case(1, true)]
    #[case(2, true)]
    #[case(3, false)]
    fn test_is_row_valid(
        #[case] index: usize,
        #[case] expected: bool,
    ) {
        let s = Size2D::new(2, 3).unwrap();
        assert_eq!(expected, s.is_row_valid(index));
    }

    #[rstest]
    #[case(0, false)]
    #[case(1, true)]
    #[case(2, true)]
    #[case(3, true)]
    #[case(4, false)]
    fn test_is_column_valid(
        #[case] index: usize,
        #[case] expected: bool,
    ) {
        let s = Size2D::new(2, 3).unwrap();
        assert_eq!(expected, s.is_column_valid(index));
    }

    #[test]
    fn test_is_index_range_valid_row() {
        let s = Size2D::new(2, 3).unwrap();
        assert_eq!(true, s.is_row_range_valid(&IndexRange::All));
        assert_eq!(true, s.is_row_range_valid( &IndexRange::range(
            Index::abs(1).unwrap(),
            Index::abs(2).unwrap()).unwrap()));
        assert_eq!(false, s.is_row_range_valid(&IndexRange::range(
            Index::abs(1).unwrap(),
            Index::abs(4).unwrap()).unwrap()));
    }

    #[test]
    fn test_is_index_range_valid_column() {
        let s = Size2D::new(2, 3).unwrap();
        assert_eq!(true, s.is_column_range_valid(&IndexRange::All));
        assert_eq!(true, s.is_column_range_valid( &IndexRange::range(
            Index::abs(1).unwrap(),
            Index::abs(2).unwrap()).unwrap()));
        assert_eq!(false, s.is_column_range_valid( &IndexRange::range(
            Index::abs(1).unwrap(),
            Index::end_minus(5)).unwrap()));
    }

    #[test]
    fn test_format_2_3() {
        let s = Size2D::new(2, 3).unwrap();
        assert_eq!("2x3", format!("{}", s));
    }
}
