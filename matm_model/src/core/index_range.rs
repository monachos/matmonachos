use std::fmt;
use std::ops::RangeInclusive;

use matm_error::error::CalcError;
use crate::core::index::Index;

pub enum IndexRange {
    Range(Index, Index),
    All,
}

impl IndexRange {
    pub fn range(start: Index, end: Index) -> Result<Self, CalcError> {
        match (&start, &end) {
            (Index::Absolute(start_v), Index::Absolute(end_v)) => {
                if *start_v > *end_v {
                    return Err(CalcError::from_string(format!("Invalid index range {}", Self::format_range(&start, &end))));
                }
            }
            (Index::End(start_offset), Index::End(end_offset)) => {
                if *start_offset < *end_offset {
                    return Err(CalcError::from_string(format!("Invalid index range {}", Self::format_range(&start, &end))));
                }
            }
            _ => {
            }
        }
        Ok(Self::Range(start, end))
    }

    pub fn one(start_end: Index) -> Result<Self, CalcError> {
        Ok(Self::Range(start_end.clone(), start_end))
    }

    pub fn calc_start(&self, max_index: usize) -> Result<usize, CalcError> {
        match self {
            Self::Range(start, _) => {
                start.calc_index(max_index)
            }
            Self::All => {
                Ok(1)
            }
        }
    }

    pub fn iter(&self, max_index: usize) -> Result<RangeInclusive<usize>, CalcError> {
        match self {
            Self::Range(start, end) => {
                let i1 = start.calc_index(max_index)?;
                let i2 = end.calc_index(max_index)?;
                Ok(i1..=i2)
            }
            Self::All => {
                Ok(1..=max_index)
            }
        }
    }

    pub fn count(&self, max_index: usize) -> Result<usize, CalcError> {
        match self {
            Self::Range(start, end) => {
                let i1 = start.calc_index(max_index)?;
                let i2 = end.calc_index(max_index)?;
                Ok(1 + i2 - i1)
            }
            Self::All => {
                Ok(max_index)
            }
        }
    }

    fn format_range(i1: &Index, i2: &Index) -> String {
        return format!("{}:{}", i1, i2);
    }
}

impl fmt::Display for IndexRange {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Range(start, end) => {
                write!(f, "{}", Self::format_range(&start, &end))
            }
            Self::All => {
                write!(f, ":")
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::testing::asserts::assert_result_error_that;
    use crate::core::index::Index;
    use crate::core::index_range::IndexRange;

    #[test]
    fn test_new_abs1_abs2() {
        let r = IndexRange::range(
            Index::abs(1).unwrap(),
            Index::abs(2).unwrap()).unwrap();
        match r {
            IndexRange::Range(start, end) => {
                match start {
                    Index::Absolute(v) => {
                        assert_eq!(1, v);
                    }
                    _ => {
                        assert_eq!(true, false);
                    }
                }
                match end {
                    Index::Absolute(v) => {
                        assert_eq!(2, v);
                    }
                    _ => {
                        assert_eq!(true, false);
                    }
                }
            }
            IndexRange::All => {
                assert_eq!(true, false);
            }
        }
    }

    #[test]
    fn test_new_abs3_abs2() {
        assert_result_error_that(
            IndexRange::range(
                Index::abs(3).unwrap(),
                Index::abs(2).unwrap()))
            .has_message("Invalid index range 3:2");
    }

    #[test]
    fn test_new_abs1_end() {
        let r = IndexRange::range(
            Index::abs(1).unwrap(),
            Index::end()).unwrap();
        match r {
            IndexRange::Range(start, end) => {
                match start {
                    Index::Absolute(v) => {
                        assert_eq!(1, v);
                    }
                    _ => {
                        assert_eq!(true, false);
                    }
                }
                match end {
                    Index::End(offset) => {
                        assert_eq!(0, offset);
                    }
                    _ => {
                        assert_eq!(true, false);
                    }
                }
            }
            IndexRange::All => {
                assert_eq!(true, false);
            }
        }
    }

    #[test]
    fn test_new_end1_end() {
        let r = IndexRange::range(
            Index::end_minus(1),
            Index::end()).unwrap();
        match r {
            IndexRange::Range(start, end) => {
                match start {
                    Index::End(offset) => {
                        assert_eq!(1, offset);
                    }
                    _ => {
                        assert_eq!(true, false);
                    }
                }
                match end {
                    Index::End(offset) => {
                        assert_eq!(0, offset);
                    }
                    _ => {
                        assert_eq!(true, false);
                    }
                }
            }
            IndexRange::All => {
                assert_eq!(true, false);
            }
        }
    }

    #[test]
    fn test_new_end1_end2() {
        assert_result_error_that(
            IndexRange::range(
                Index::end_minus(1),
                Index::end_minus(2)))
            .has_message("Invalid index range end-1:end-2");
    }

    #[test]
    fn test_one() {
        let r = IndexRange::one(
            Index::abs(2).unwrap()).unwrap();
        match r {
            IndexRange::Range(start, end) => {
                match start {
                    Index::Absolute(v) => {
                        assert_eq!(2, v);
                    }
                    _ => {
                        assert_eq!(true, false);
                    }
                }
                match end {
                    Index::Absolute(v) => {
                        assert_eq!(2, v);
                    }
                    _ => {
                        assert_eq!(true, false);
                    }
                }
            }
            IndexRange::All => {
                assert_eq!(true, false);
            }
        }
    }

    #[test]
    fn test_calc_start_abs_1() {
        let range = IndexRange::range(
            Index::abs(1).unwrap(),
            Index::abs(3).unwrap()).unwrap();
        let i = range.calc_start(5).unwrap();

        assert_eq!(1, i);
    }

    #[test]
    fn test_calc_start_abs_3() {
        let range = IndexRange::range(
            Index::abs(3).unwrap(),
            Index::abs(3).unwrap()).unwrap();
        let i = range.calc_start(5).unwrap();

        assert_eq!(3, i);
    }

    #[test]
    fn test_calc_start_all() {
        let range = IndexRange::All;
        let i = range.calc_start(5).unwrap();

        assert_eq!(1, i);
    }

    #[test]
    fn test_format_range() {
        assert_eq!("1:2", format!("{}", IndexRange::range(
            Index::abs(1).unwrap(),
            Index::abs(2).unwrap()).unwrap()));
    }

    #[test]
    fn test_format_all() {
        assert_eq!(":", format!("{}", IndexRange::All));
    }

    #[test]
    fn test_iter_range() {
        let range = IndexRange::range(
            Index::abs(1).unwrap(),
            Index::abs(3).unwrap()).unwrap();
        let mut it = range.iter(5).unwrap();

        assert_eq!(Some(1usize), it.next());
        assert_eq!(Some(2usize), it.next());
        assert_eq!(Some(3usize), it.next());
        assert_eq!(None, it.next());
    }

    #[test]
    fn test_iter_all() {
        let range = IndexRange::All;
        let mut it = range.iter(5).unwrap();

        assert_eq!(Some(1usize), it.next());
        assert_eq!(Some(2usize), it.next());
        assert_eq!(Some(3usize), it.next());
        assert_eq!(Some(4usize), it.next());
        assert_eq!(Some(5usize), it.next());
        assert_eq!(None, it.next());
    }

    #[test]
    fn test_count_range() {
        let range = IndexRange::range(
            Index::abs(1).unwrap(),
            Index::abs(3).unwrap()).unwrap();

        assert_eq!(3, range.count(10).unwrap());
    }

    #[test]
    fn test_count_all() {
        let range = IndexRange::All;
        assert_eq!(10, range.count(10).unwrap());
    }
}
