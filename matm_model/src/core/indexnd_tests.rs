#[cfg(test)]
mod tests {
    use crate::core::index::IndexND;
    use crate::testing::asserts::assert_result_error_that;

    #[test]
    fn test_new() {
        let idx = IndexND::new(vec![1, 2]).unwrap();
        assert_eq!(1, idx.value()[0]);
        assert_eq!(2, idx.value()[1]);
    }

    #[test]
    fn test_new_empty() {
        assert_result_error_that(IndexND::new(Vec::new()))
            .has_message("You need to specify at least one index value.");
    }

    #[test]
    fn test_new_0_2() {
        assert_result_error_that(IndexND::new(vec![0, 2]))
            .has_message("Invalid index (0, 2).");
    }

    #[test]
    fn test_new_2_0() {
        assert_result_error_that(IndexND::new(vec![2, 0]))
            .has_message("Invalid index (2, 0).");
    }

    #[test]
    fn test_new_0_0() {
        assert_result_error_that(IndexND::new(vec![0, 0]))
            .has_message("Invalid index (0, 0).");
    }

    #[test]
    fn test_one() {
        let idx = IndexND::one2d();
        assert_eq!(1, idx.value()[0]);
        assert_eq!(1, idx.value()[1]);
    }

    #[test]
    fn test_ones() {
        let idx = IndexND::ones(3).unwrap();
        assert_eq!(3, idx.value().len());
        assert_eq!(1, idx.value()[0]);
        assert_eq!(1, idx.value()[1]);
        assert_eq!(1, idx.value()[2]);
    }

    #[test]
    fn test_ones_invalid() {
        let idx = IndexND::ones(0);

        assert_result_error_that(idx)
            .has_message("You need to specify at least one dimension.");
    }

    #[test]
    fn test_value_of() {
        let idx = IndexND::new(vec![10, 20]).unwrap();
        
        assert_eq!(10, idx.value_of(0).unwrap());
        assert_eq!(20, idx.value_of(1).unwrap());
    }

    #[test]
    fn test_value_of_invalid() {
        let idx = IndexND::new(vec![10, 20]).unwrap();

        assert_result_error_that(idx.value_of(2))
            .has_message("Invalid dimension index 2.");
    }

    #[test]
    fn test_format() {
        let idx = IndexND::new(vec![1, 2]).unwrap();
        assert_eq!("(1, 2)", format!("{}", idx));
    }

}