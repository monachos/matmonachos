#[cfg(test)]
mod tests {
    use rstest::rstest;
    use assert_matches::assert_matches;
    use std::cmp::Ordering;
    use crate::core::natural::Natural;
    use crate::core::natural::RADIX_10;
    use crate::core::natural::RADIX_16;
    use crate::core::positional::PositionalNumber;
    use crate::testing::asserts::assert_result_error_that;
    use crate::testing::asserts::assert_result_ok;


    #[test]
    fn test_zero() {
        let v = assert_result_ok(Natural::zero(RADIX_10));

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&0));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_one() {
        let v = assert_result_ok(Natural::one(RADIX_10));

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&1));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_int_0() {
        let v = assert_result_ok(Natural::from_int(RADIX_10, 0));

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&0));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_int_1() {
        let v = assert_result_ok(Natural::from_int(RADIX_10, 1));

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&1));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_int_9() {
        let v = assert_result_ok(Natural::from_int(RADIX_10, 9));

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&9));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_int_10() {
        let v = assert_result_ok(Natural::from_int(RADIX_10, 10));

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&1));
        assert_eq!(iter.next(), Some(&0));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_int_1234() {
        let v = assert_result_ok(Natural::from_int(RADIX_10, 1234));

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&1));
        assert_eq!(iter.next(), Some(&2));
        assert_eq!(iter.next(), Some(&3));
        assert_eq!(iter.next(), Some(&4));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_u32_max() {
        let v = assert_result_ok(Natural::from_int(RADIX_10, u32::MAX));
        
        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&4));
        assert_eq!(iter.next(), Some(&2));
        assert_eq!(iter.next(), Some(&9));
        assert_eq!(iter.next(), Some(&4));
        assert_eq!(iter.next(), Some(&9));
        assert_eq!(iter.next(), Some(&6));
        assert_eq!(iter.next(), Some(&7));
        assert_eq!(iter.next(), Some(&2));
        assert_eq!(iter.next(), Some(&9));
        assert_eq!(iter.next(), Some(&5));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_i32_max() {
        let v = assert_result_ok(Natural::from_int(RADIX_10, i32::MAX as u32));
        
        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&2));
        assert_eq!(iter.next(), Some(&1));
        assert_eq!(iter.next(), Some(&4));
        assert_eq!(iter.next(), Some(&7));
        assert_eq!(iter.next(), Some(&4));
        assert_eq!(iter.next(), Some(&8));
        assert_eq!(iter.next(), Some(&3));
        assert_eq!(iter.next(), Some(&6));
        assert_eq!(iter.next(), Some(&4));
        assert_eq!(iter.next(), Some(&7));
        assert_eq!(iter.next(), None);
    }

    #[rstest]
    #[case(0x0)]
    #[case(0x5)]
    #[case(0x9)]
    #[case(0xA)]
    #[case(0xA)]
    #[case(0xF)]
    #[case(0x12AD)]
    fn test_from_int_radix16(#[case] value: u32) {
        let v = assert_result_ok(Natural::from_int(RADIX_16, value));
        assert_eq!(v.to_u32(), Some(value));
    }

    #[test]
    fn test_from_int_max_radix() {
        let v = assert_result_ok(Natural::from_int(36, 1));
        
        assert_eq!(36, v.radix());
    }

    #[test]
    fn test_from_int_bad_radix() {
        assert_result_error_that(Natural::from_int(37, 1))
            .has_message("Specified radix 37 cannot be greater than 36.");
    }

    #[test]
    fn test_from_vector_empty() {
        assert_result_error_that(Natural::from_vector(RADIX_10, Vec::new()))
            .has_message("Cannot create number from empty vector.");
    }

    #[test]
    fn test_from_vector_0() {
        let v = assert_result_ok(Natural::from_vector(RADIX_10, vec![0]));
        
        assert!(v.is_zero());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&0));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_vector_000() {
        let v = assert_result_ok(Natural::from_vector(RADIX_10, vec![0, 0, 0]));
        
        assert!(v.is_zero());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&0));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_vector_03() {
        let v = assert_result_ok(Natural::from_vector(RADIX_10, vec![0, 3]));
        
        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&3));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_vector_3() {
        let v = assert_result_ok(Natural::from_vector(RADIX_10, vec![3]));
        
        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&3));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_vector_1234() {
        let v = assert_result_ok(Natural::from_vector(RADIX_10, vec![1, 2, 3, 4]));
        
        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&1));
        assert_eq!(iter.next(), Some(&2));
        assert_eq!(iter.next(), Some(&3));
        assert_eq!(iter.next(), Some(&4));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_vector_max_radix() {
        let v = assert_result_ok(Natural::from_vector(36, vec![1, 2]));
        
        assert_eq!(36, v.radix());
    }

    #[test]
    fn test_from_vector_bad_radix() {
        assert_result_error_that(Natural::from_vector(37, vec![1, 2]))
            .has_message("Specified radix 37 cannot be greater than 36.");
    }

    #[test]
    fn test_from_vector_bad_digit() {
        assert_result_error_that(Natural::from_vector(RADIX_10, vec![1, 10]))
            .has_message("Specified digit 10 is invalid in radix 10.");
    }

    #[test]
    fn test_from_string_0() {
        let v = assert_result_ok(Natural::from_string(RADIX_10, "0"));
        assert_eq!(true, v.is_zero());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&0));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_string_01() {
        let v = assert_result_ok(Natural::from_string(RADIX_10, "01"));

        assert_eq!(false, v.is_zero());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&1));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_string_1() {
        let v = assert_result_ok(Natural::from_string(RADIX_10, "1"));

        assert_eq!(false, v.is_zero());

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&1));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_string_1234() {
        let v = assert_result_ok(Natural::from_string(RADIX_10, "1234"));

        let mut iter = v.iter();
        assert_eq!(iter.next(), Some(&1));
        assert_eq!(iter.next(), Some(&2));
        assert_eq!(iter.next(), Some(&3));
        assert_eq!(iter.next(), Some(&4));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_from_string_empty() {
        assert_result_error_that(Natural::from_string(RADIX_10, ""))
            .has_message("Cannot create number from empty string.");
    }

    #[test]
    fn test_from_string_bad_digit() {
        assert_result_error_that(Natural::from_string(RADIX_10, "k"))
            .has_message("Cannot create number using the string: 'k'");
    }

    #[test]
    fn test_from_string_bad_character() {
        assert_result_error_that(Natural::from_string(RADIX_10, "*"))
            .has_message("Cannot create number using the string: '*'");
    }

    #[rstest]
    #[case("0", 0x0)]
    #[case("5", 0x5)]
    #[case("9", 0x9)]
    #[case("a", 0xA)]
    #[case("A", 0xA)]
    #[case("F", 0xF)]
    #[case("12AD", 0x12AD)]
    fn test_from_string_radix16(
        #[case] input: &str,
        #[case] expected: u32,
    ) {
        let v = assert_result_ok(Natural::from_string(RADIX_16, input));
        assert_eq!(v.to_u32(), Some(expected));
    }

    #[test]
    #[ignore = "Cannot create number using the string"]
    fn test_from_string_min_radix() {
        let v = assert_result_ok(Natural::from_string(1, "111"));

        assert_eq!(1, v.radix());
        assert_eq!(v.to_u32(), Some(3));
    }

    #[test]
    fn test_from_string_max_radix() {
        let v = assert_result_ok(Natural::from_string(36, "1"));
        
        assert_eq!(36, v.radix());
    }

    #[test]
    fn test_from_string_bad_radix() {
        assert_result_error_that(Natural::from_string(37, "1"))
            .has_message("Specified radix 37 cannot be greater than 36.");
    }

    #[test]
    fn test_from_positional_number() {
        let mut positional = PositionalNumber::new(RADIX_10);
        positional.set_digit(2, 8).unwrap();
        positional.set_digit(1, 4).unwrap();
        let result = Natural::from_positional_number(&positional).unwrap();
        assert_eq!("840", format!("{}", result));
    }

    #[test]
    fn test_is_one() {
        assert_eq!(false, Natural::from_int(RADIX_10, 0).unwrap().is_one());
        assert_eq!(true, Natural::from_int(RADIX_10, 1).unwrap().is_one());
        assert_eq!(false, Natural::from_int(RADIX_10, 11).unwrap().is_one());
        assert_eq!(false, Natural::from_int(RADIX_10, 4).unwrap().is_one());
    }

    #[test]
    fn test_get_digit_0() {
        let v = Natural::from_int(RADIX_10, 0).unwrap();
        assert_matches!(v.get_digit(0), Some(0));
        assert_matches!(v.get_digit(1), None);
        assert_matches!(v.get_digit(2), None);
    }

    #[test]
    fn test_get_digit_12345() {
        let v = Natural::from_int(RADIX_10, 12345).unwrap();
        assert_matches!(v.get_digit(0), Some(5));
        assert_matches!(v.get_digit(1), Some(4));
        assert_matches!(v.get_digit(2), Some(3));
        assert_matches!(v.get_digit(3), Some(2));
        assert_matches!(v.get_digit(4), Some(1));
        assert_matches!(v.get_digit(5), None);
        assert_matches!(v.get_digit(6), None);
    }

    #[test]
    fn test_get_digit_else_error_0() {
        let v = Natural::from_int(RADIX_10, 0).unwrap();
        assert_eq!(0, assert_result_ok(v.get_digit_else_error(0)));
        assert_result_error_that(v.get_digit_else_error(1))
            .has_message("Digit not found at index 1.");
        assert_result_error_that(v.get_digit_else_error(2))
            .has_message("Digit not found at index 2.");
    }

    #[test]
    fn test_get_digit_else_error_12345() {
        let v = Natural::from_int(RADIX_10, 12345).unwrap();
        assert_eq!(5, assert_result_ok(v.get_digit_else_error(0)));
        assert_eq!(4, assert_result_ok(v.get_digit_else_error(1)));
        assert_eq!(3, assert_result_ok(v.get_digit_else_error(2)));
        assert_eq!(2, assert_result_ok(v.get_digit_else_error(3)));
        assert_eq!(1, assert_result_ok(v.get_digit_else_error(4)));
        assert_result_error_that(v.get_digit_else_error(5))
            .has_message("Digit not found at index 5.");
        assert_result_error_that(v.get_digit_else_error(6))
            .has_message("Digit not found at index 6.");
    }

    #[test]
    fn test_get_digit_else_zero_0() {
        let v = Natural::from_int(RADIX_10, 0).unwrap();
        assert_eq!(0, v.get_digit_else_zero(0));
        assert_eq!(0, v.get_digit_else_zero(1));
        assert_eq!(0, v.get_digit_else_zero(2));
    }

    #[test]
    fn test_get_digit_else_zero_12345() {
        let v = Natural::from_int(RADIX_10, 12345).unwrap();
        assert_eq!(5, v.get_digit_else_zero(0));
        assert_eq!(4, v.get_digit_else_zero(1));
        assert_eq!(3, v.get_digit_else_zero(2));
        assert_eq!(2, v.get_digit_else_zero(3));
        assert_eq!(1, v.get_digit_else_zero(4));
        assert_eq!(0, v.get_digit_else_zero(5));
        assert_eq!(0, v.get_digit_else_zero(6));
    }

    #[test]
    fn test_set_digit() {
        let mut v = Natural::zero(RADIX_10).unwrap();
        assert_eq!(1, v.digits_len());
        assert_eq!("0", v.to_string());
        
        v.set_digit(0, 1).unwrap();
        assert_eq!(1, v.digits_len());
        assert_eq!("1", v.to_string());

        v.set_digit(2, 2).unwrap();
        assert_eq!(3, v.digits_len());
        assert_eq!("201", v.to_string());

        v.set_digit(4, 3).unwrap();
        assert_eq!(5, v.digits_len());
        assert_eq!("30201", v.to_string());

        v.set_digit(3, 8).unwrap();
        assert_eq!(5, v.digits_len());
        assert_eq!("38201", v.to_string());

        v.set_digit(4, 0).unwrap();
        assert_eq!(4, v.digits_len());
        assert_eq!("8201", v.to_string());

        v.set_digit(3, 0).unwrap();
        assert_eq!(3, v.digits_len());
        assert_eq!("201", v.to_string());

        v.set_digit(2, 0).unwrap();
        assert_eq!(1, v.digits_len());
        assert_eq!("1", v.to_string());
    }

    #[test]
    fn test_set_digit_valid_value() {
        let mut v = Natural::zero(RADIX_10).unwrap();
        for d in 0..RADIX_10 as u8 {
            assert_result_ok(v.set_digit(0, d));
        }
    }

    #[test]
    fn test_set_digit_invalid_value() {
        let mut v = Natural::zero(RADIX_10).unwrap();
        assert_result_error_that(v.set_digit(0, 10))
            .has_message("Invalid digit value 10 in radix 10.");
    }

    #[test]
    fn test_append_digit_right_to_zero() {
        let mut v = Natural::zero(RADIX_10).unwrap();
        v.append_digit_right(2).unwrap();
        assert_eq!("2", v.to_string());
    }

    #[test]
    fn test_append_digit_right_to_existing_digits() {
        let mut v = Natural::from_int(RADIX_10, 1).unwrap();
        v.append_digit_right(2).unwrap();
        assert_eq!("12", v.to_string());
    }

    #[test]
    fn test_append_digit_right_invalid_value() {
        let mut v = Natural::from_int(RADIX_10, 1).unwrap();
        assert_result_error_that(v.append_digit_right(13))
            .has_message("Invalid digit value 13 in radix 10.");
    }

    #[rstest]
    #[case(0)]
    #[case(1)]
    #[case(2)]
    #[case(10)]
    #[case(45)]
    #[case(1234)]
    fn test_equal(#[case] value: u32) {
        let left = Natural::from_int(RADIX_10, value).unwrap();
        let right = Natural::from_int(RADIX_10, value).unwrap();
        assert_eq!(true, left.eq(&right));
    }

    #[rstest]
    #[case(0)]
    #[case(1)]
    #[case(2)]
    #[case(10)]
    #[case(45)]
    #[case(1234)]
    fn test_equal_diff_radix(#[case] value: u32) {
        let left = Natural::from_int(RADIX_10, value).unwrap();
        let right = Natural::from_int(RADIX_16, value).unwrap();
        assert_eq!(false, left.eq(&right));
    }

    #[rstest]
    #[case(0)]
    #[case(1)]
    #[case(2)]
    #[case(10)]
    #[case(45)]
    #[case(1234)]
    fn test_partial_cmp_equal(#[case] value: u32) {
        let left = Natural::from_int(RADIX_10, value).unwrap();
        let right = Natural::from_int(RADIX_10, value).unwrap();
        assert_matches!(left.partial_cmp(&right), Some(Ordering::Equal));
    }

    #[rstest]
    #[case(0)]
    #[case(1)]
    #[case(2)]
    #[case(10)]
    #[case(45)]
    #[case(1234)]
    fn test_partial_cmp_equal_diff_radix(#[case] value: u32) {
        let left = Natural::from_int(RADIX_10, value).unwrap();
        let right = Natural::from_int(RADIX_16, value).unwrap();
        assert_matches!(left.partial_cmp(&right), None);
    }

    #[rstest]
    #[case(1, 0)]
    #[case(3, 0)]
    #[case(10, 0)]
    #[case(123, 0)]
    #[case(2, 1)]
    #[case(7, 3)]
    #[case(20, 4)]
    #[case(23, 22)]
    #[case(32, 29)]
    #[case(100, 55)]
    fn test_partial_cmp_greater(
        #[case] left_input: u32,
        #[case] right_input: u32,
    ) {
        let left = Natural::from_int(RADIX_10, left_input).unwrap();
        let right = Natural::from_int(RADIX_10, right_input).unwrap();
        assert_matches!(left.partial_cmp(&right), Some(Ordering::Greater));
    }

    #[rstest]
    #[case(0, 1)]
    #[case(0, 3)]
    #[case(0, 10)]
    #[case(0, 123)]
    #[case(1, 2)]
    #[case(3, 7)]
    #[case(4, 20)]
    #[case(22, 23)]
    #[case(29, 32)]
    #[case(55, 100)]
    fn test_partial_cmp_less(
        #[case] left_input: u32,
        #[case] right_input: u32,
    ) {
        let left = Natural::from_int(RADIX_10, left_input).unwrap();
        let right = Natural::from_int(RADIX_10, right_input).unwrap();
        assert_matches!(left.partial_cmp(&right), Some(Ordering::Less));
    }

    #[rstest]
    #[case(1, 0)]
    #[case(3, 0)]
    #[case(10, 0)]
    #[case(123, 0)]
    #[case(2, 1)]
    #[case(7, 3)]
    #[case(20, 4)]
    #[case(23, 22)]
    #[case(32, 29)]
    #[case(100, 55)]
    fn test_partial_cmp_diff_radix(
        #[case] left_input: u32,
        #[case] right_input: u32,
    ) {
        let left = Natural::from_int(RADIX_10, left_input).unwrap();
        let right = Natural::from_int(RADIX_16, right_input).unwrap();
        assert_matches!(left.partial_cmp(&right), None);
    }

    #[rstest]
    #[case(0)]
    #[case(1)]
    #[case(2)]
    #[case(10)]
    #[case(45)]
    #[case(1234)]
    #[case(u32::MAX)]
    fn test_to_u32(#[case] value: u32) {
        println!("Checking to_u32() for {}", value);
        let v = Natural::from_int(RADIX_10, value).unwrap();
        assert_matches!(v.to_u32(), Some(_value));
    }

    #[rstest]
    // max is 4294967295
    #[case("4294967296")]
    fn test_to_u32_over(#[case] value: &str) {
        println!("Checking to_u32() for {}", value);
        let v = Natural::from_string(RADIX_10, value).unwrap();
        assert_matches!(v.to_u32(), None);
    }

    #[test]
    fn test_format_radix10() {
        assert_eq!("0", format!("{}", Natural::from_int(RADIX_10, 0).unwrap()));
        assert_eq!("1", format!("{}", Natural::from_int(RADIX_10, 1).unwrap()));
        assert_eq!("23", format!("{}", Natural::from_int(RADIX_10, 23).unwrap()));
        assert_eq!("1234", format!("{}", Natural::from_int(RADIX_10, 1234).unwrap()));
    }

    #[test]
    fn test_format_radix16() {
        assert_eq!("16#0#", format!("{}", Natural::from_int(RADIX_16, 0x0).unwrap()));
        assert_eq!("16#1#", format!("{}", Natural::from_int(RADIX_16, 0x1).unwrap()));
        assert_eq!("16#23#", format!("{}", Natural::from_int(RADIX_16, 0x23).unwrap()));
        assert_eq!("16#1af#", format!("{}", Natural::from_int(RADIX_16, 0x1AF).unwrap()));
    }

    #[test]
    fn test_get_max_digits_length_empty() {
        let items: Vec<Natural> = Vec::new();
        assert_eq!(0, Natural::get_max_digits_length(&items));
    }

    #[test]
    fn test_get_max_digits_length_one() {
        let items: Vec<Natural> = vec![
            Natural::from_int(RADIX_10, 4).unwrap(),
        ];
        assert_eq!(1, Natural::get_max_digits_length(&items));
    }

    #[test]
    fn test_get_max_digits_length_many() {
        let items: Vec<Natural> = vec![
            Natural::from_int(RADIX_10, 123).unwrap(),
            Natural::from_int(RADIX_10, 123456).unwrap(),
            Natural::from_int(RADIX_10, 4).unwrap(),
        ];
        assert_eq!(6, Natural::get_max_digits_length(&items));
    }

}
