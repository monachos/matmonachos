
pub fn digit_to_string(radix: u32, digit: u8) -> String {
   let code: u32 = u32::from(digit);
   return char::from_digit(code, radix).unwrap().to_string();
}

#[cfg(test)]
mod tests {
   use crate::core::digit::digit_to_string;
   use crate::core::natural::RADIX_2;
   use crate::core::natural::RADIX_10;
   use crate::core::natural::RADIX_16;


   #[test]
   fn test_digit_to_string_radix10() {
      assert_eq!("0", digit_to_string(RADIX_10, 0));
      assert_eq!("1", digit_to_string(RADIX_10, 1));
      assert_eq!("9", digit_to_string(RADIX_10, 9));
   }

   #[test]
   fn test_digit_to_string_radix16() {
      assert_eq!("0", digit_to_string(RADIX_16, 0x0));
      assert_eq!("1", digit_to_string(RADIX_16, 0x1));
      assert_eq!("9", digit_to_string(RADIX_16, 0x9));
      assert_eq!("a", digit_to_string(RADIX_16, 0xA));
      assert_eq!("f", digit_to_string(RADIX_16, 0xF));
   }

   #[test]
   fn test_digit_to_string_radix2() {
      assert_eq!("0", digit_to_string(RADIX_2, 0b0));
      assert_eq!("1", digit_to_string(RADIX_2, 0b1));
   }
}
