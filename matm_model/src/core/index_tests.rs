#[cfg(test)]
mod tests {
    use rstest::rstest;
    use crate::testing::asserts::assert_result_error_that;
    use crate::core::index::Index;

    #[test]
    fn test_one() {
        match Index::one() {
            Index::Absolute(v) => {
                assert_eq!(1, v);
            }
            _ => {
                assert_eq!(true, false);
            }
        }
    }

    #[test]
    fn test_abs() {
        match Index::abs(1).unwrap() {
            Index::Absolute(v) => {
                assert_eq!(1, v);
            }
            _ => {
                assert_eq!(true, false);
            }
        }
    }

    #[test]
    fn test_abs_0() {
        assert_result_error_that(Index::abs(0))
            .has_message("Invalid index 0");
    }

    #[test]
    fn test_end() {
        match Index::end() {
            Index::End(offset) => {
                assert_eq!(0, offset);
            }
            _ => {
                assert_eq!(true, false);
            }
        }
    }

    #[test]
    fn test_end_minus() {
        match Index::end_minus(3) {
            Index::End(offset) => {
                assert_eq!(3, offset);
            }
            _ => {
                assert_eq!(true, false);
            }
        }
    }

    #[test]
    fn test_format_abs() {
        assert_eq!("1", format!("{}", Index::abs(1).unwrap()));
    }

    #[test]
    fn test_format_end() {
        assert_eq!("end", format!("{}", Index::end()));
    }

    #[test]
    fn test_format_end_minus() {
        assert_eq!("end-2", format!("{}", Index::end_minus(2)));
    }

    #[test]
    fn test_calc_index_abs() {
        let i1 = Index::abs(2).unwrap();
        assert_eq!(2, i1.calc_index(10).unwrap());
    }

    #[test]
    fn test_calc_index_end() {
        let i1 = Index::end();
        assert_eq!(10, i1.calc_index(10).unwrap());
    }

    #[test]
    fn test_calc_index_end_minus() {
        let i1 = Index::end_minus(3);
        assert_eq!(7, i1.calc_index(10).unwrap());
    }

    #[test]
    fn test_calc_index_end_minus_invalid() {
        let i1 = Index::end_minus(10);
        assert_result_error_that(i1.calc_index(10))
            .has_message("Invalid index offset 10, maximum offset is 9");
    }

    #[rstest]
    #[case(1, 10, true)]
    #[case(3, 10, true)]
    #[case(10, 10, true)]
    #[case(11, 10, false)]
    fn test_is_valid(
        #[case] value: usize,
        #[case] max_index: usize,
        #[case] expected: bool,
    ) {
        let i1 = Index::abs(value).unwrap();
        assert_eq!(expected, i1.is_valid(max_index));
    }

    #[test]
    fn test_is_valid_end_in_10() {
        let i1 = Index::end();
        assert_eq!(true, i1.is_valid(10));
    }

    #[rstest]
    #[case(2, 10, true)]
    #[case(9, 10, true)]
    #[case(10, 10, false)]
    fn test_is_valid_end_minus(
        #[case] value: usize,
        #[case] max_index: usize,
        #[case] expected: bool,
    ) {
        let i1 = Index::end_minus(value);
        assert_eq!(expected, i1.is_valid(max_index));
    }

}