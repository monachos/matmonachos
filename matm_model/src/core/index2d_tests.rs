#[cfg(test)]
mod tests {
    use crate::core::index::Index2D;
    use crate::testing::asserts::assert_result_error_that;

    #[test]
    fn test_new() {
        let idx = Index2D::new(1, 2).unwrap();
        assert_eq!(1, idx.row);
        assert_eq!(2, idx.column);
    }

    #[test]
    fn test_new_0_2() {
        assert_result_error_that(Index2D::new(0, 2))
            .has_message("Invalid index (0, 2)");
    }

    #[test]
    fn test_new_2_0() {
        assert_result_error_that(Index2D::new(2, 0))
            .has_message("Invalid index (2, 0)");
    }

    #[test]
    fn test_new_0_0() {
        assert_result_error_that(Index2D::new(0, 0))
            .has_message("Invalid index (0, 0)");
    }

    #[test]
    fn test_one() {
        let idx = Index2D::one();
        assert_eq!(1, idx.row);
        assert_eq!(1, idx.column);
    }

    #[test]
    fn test_left() {
        let original = Index2D::new(3, 2).unwrap();
        let idx = original.left().unwrap();
        assert_eq!(3, idx.row);
        assert_eq!(1, idx.column);
    }

    #[test]
    fn test_left_up() {
        let original = Index2D::new(3, 2).unwrap();
        let idx = original.left_up().unwrap();
        assert_eq!(2, idx.row);
        assert_eq!(1, idx.column);
    }

    #[test]
    fn test_up() {
        let original = Index2D::new(3, 2).unwrap();
        let idx = original.up().unwrap();
        assert_eq!(2, idx.row);
        assert_eq!(2, idx.column);
    }

    #[test]
    fn test_right_up() {
        let original = Index2D::new(3, 2).unwrap();
        let idx = original.right_up().unwrap();
        assert_eq!(2, idx.row);
        assert_eq!(3, idx.column);
    }

    #[test]
    fn test_right() {
        let original = Index2D::new(3, 2).unwrap();
        let idx = original.right().unwrap();
        assert_eq!(3, idx.row);
        assert_eq!(3, idx.column);
    }

    #[test]
    fn test_right_down() {
        let original = Index2D::new(3, 2).unwrap();
        let idx = original.right_down().unwrap();
        assert_eq!(4, idx.row);
        assert_eq!(3, idx.column);
    }

    #[test]
    fn test_down() {
        let original = Index2D::new(3, 2).unwrap();
        let idx = original.down().unwrap();
        assert_eq!(4, idx.row);
        assert_eq!(2, idx.column);
    }

    #[test]
    fn test_left_down() {
        let original = Index2D::new(3, 2).unwrap();
        let idx = original.left_down().unwrap();
        assert_eq!(4, idx.row);
        assert_eq!(1, idx.column);
    }

    #[test]
    fn test_format() {
        assert_eq!("(1, 2)", format!("{}", Index2D::new(1, 2).unwrap()));
    }

}