use std::fmt;
use matm_error::error::CalcError;
use crate::core::digit::digit_to_string;
use crate::core::natural::Natural;

#[derive(Clone, Debug, Copy)]
pub enum PositionalMark {
    Default,
    Source,
    Target,
}


#[derive(Clone, Debug, Copy)]
pub struct PositionalDigit {
    value: u8,
    mark: PositionalMark,
}

impl PositionalDigit {
    pub fn value(&self) -> u8 {
        self.value
    }

    pub fn mark(&self) -> PositionalMark {
        self.mark
    }
}


#[derive(Clone, Debug, Copy)]
pub enum PositionalItem {
    None,
    Unknown,
    Digit(PositionalDigit),
}

impl PositionalItem {
    pub fn from_digit(d: u8) -> Self {
        Self::from_digit_mark(d, PositionalMark::Default)
    }

    pub fn from_digit_mark(d: u8, mark: PositionalMark) -> Self {
        PositionalItem::Digit(PositionalDigit {
            value: d,
            mark: mark,
        })
    }

    pub fn to_text(&self, radix: u32) -> String {
        return match &self {
            PositionalItem::None => {
                String::from(" ")
            }
            PositionalItem::Unknown => {
                String::from("?")
            }
            PositionalItem::Digit(d) => {
                digit_to_string(radix, d.value)
            }
        };
    }

    pub fn to_digit_as_string(&self, radix: u32) -> String {
        return match &self {
            PositionalItem::None |
            PositionalItem::Unknown => {
                String::from("0")
            }
            PositionalItem::Digit(d) => {
                digit_to_string(radix, d.value)
            }
        };
    }
}

#[derive(Clone, Debug)]
pub struct PositionalNumber {
    pub radix: u32,
    pub digits: Vec<PositionalItem>,
}

impl PositionalNumber {
    pub fn new(radix: u32) -> Self {
        Self {
            radix: radix,
            digits: Vec::new(),
        }
    }

    pub fn from_natural(natural: &Natural) -> Self {
        let digits = natural.digits()
            .iter()
            .map(|d| PositionalItem::from_digit(*d))
            .collect();
        Self {
            radix: natural.radix(),
            digits: digits,
        }
    }

    pub fn is_empty(&self) -> bool {
        return !self.digits.iter().any(|d| match d {
            PositionalItem::None => false,
            _ => true,
        });
    }

    pub fn has_any_digit(&self) -> bool {
        return self.digits.iter().any(|d| match d {
            PositionalItem::Digit(_) => true,
            _ => false,
        });
    }

    pub fn get_digit(&self, pos: usize) -> Option<u8> {
        return match self.digits.get(pos).copied() {
            Some(d) => {
                match d {
                    PositionalItem::Digit(d) => Some(d.value),
                    _ => None,
                }
            }
            None => None,
        };
    }

    pub fn get_digit_else_zero(&self, pos: usize) -> u8 {
        return match self.digits.get(pos).copied() {
            Some(d) => {
                match d {
                    PositionalItem::Digit(d) => d.value,
                    _ => 0,
                }
            }
            None => 0,
        };
    }

    pub fn has_digit(&self, pos: usize) -> bool {
        return match self.digits.get(pos).copied() {
            Some(d) => {
                match d {
                    PositionalItem::Digit(_) => true,
                    _ => false,
                }
            }
            None => false,
        };
    }

    pub fn set_digit_mark(&mut self, pos: usize, digit: u8, mark: PositionalMark) -> Result<(), CalcError> {
        while pos >= self.digits.len() {
            self.digits.push(PositionalItem::None);
        }
        if let Some(elem) = self.digits.get_mut(pos) {
            *elem = PositionalItem::from_digit_mark(digit, mark);
        }
        return Ok(());
    }

    pub fn set_digit(&mut self, pos: usize, digit: u8) -> Result<(), CalcError> {
        self.set_digit_mark(pos, digit, PositionalMark::Default)
    }

    pub fn mark_digit(&mut self, pos: usize, mark: PositionalMark) -> Result<(), CalcError> {
        if let Some(item) = self.digits.get_mut(pos) {
            match item {
                PositionalItem::Digit(d) => {
                    d.mark = mark;
                }
                PositionalItem::Unknown => {}
                PositionalItem::None => {}
            }
        }
        return Ok(());
    }

    pub fn mark_digit_as_source(&mut self, pos: usize) -> Result<(), CalcError> {
        self.mark_digit(pos, PositionalMark::Source)
    }

    pub fn mark_digit_as_target(&mut self, pos: usize) -> Result<(), CalcError> {
        self.mark_digit(pos, PositionalMark::Target)
    }

    pub fn mark_number(&mut self, mark: PositionalMark) {
        for item in &mut self.digits {
            match item {
                PositionalItem::Digit(d) => {
                    d.mark = mark;
                }
                PositionalItem::Unknown => {}
                PositionalItem::None => {}
            }
        }
    }

    pub fn mark_number_as_source(&mut self) {
        self.mark_number(PositionalMark::Source);
    }

    pub fn mark_number_as_target(&mut self) {
        self.mark_number(PositionalMark::Target);
    }

    pub fn remove_marks(&mut self) {
        self.mark_number(PositionalMark::Default);
    }

    pub fn get_digit_mark(&self, pos: usize) -> Option<PositionalMark> {
        if let Some(item) = self.digits.get(pos) {
            if let PositionalItem::Digit(pos_dig) = item {
                return Some(pos_dig.mark());
            }
        }
        return None;
    }

    pub fn set_unknown(&mut self, pos: usize) -> Result<(), CalcError> {
        while pos >= self.digits.len() {
            self.digits.push(PositionalItem::None);
        }
        if let Some(elem) = self.digits.get_mut(pos) {
            *elem = PositionalItem::Unknown;
        }
        return Ok(());
    }

    pub fn set_value_at(&mut self, start_at: usize, value: &Natural) -> Result<(), CalcError> {
        for i in 0..value.digits_len() {
            let digit = value.get_digit(i).unwrap();
            self.set_digit(start_at + i, digit)?;
        }

        Ok(())
    }

    /// Adds value to digit at specified index.
    /// If the sum of current digit and passed value is greater than radix
    /// then value is split into indexes and added at proper indexes
    /// of next digits.
    pub fn add_value_to_digit(&mut self, pos: usize, value: u32, mark: PositionalMark
    ) -> Result<(), CalcError> {
        let mut current_pos = pos;
        let mut new_value = value;
        loop {
            let current_digit = self.get_digit_else_zero(current_pos);
            new_value = current_digit as u32 + new_value;
            let new_digit = (new_value % self.radix) as u8;

            if new_digit != current_digit || !self.has_digit(current_pos) {
                self.set_digit_mark(current_pos, new_digit, mark)?;
            }

            new_value /= self.radix;
            if new_value == 0 {
                break;
            }
            current_pos += 1;
        }
        Ok(())
    }

    pub fn append_digit_right(&mut self, digit: u8) -> Result<(), CalcError> {
        if u32::from(digit) >= self.radix {
            return Err(CalcError::from_string(format!("Invalid digit value {} in radix {}.", digit, self.radix)));
        }
        if self.digits.len() == 1 && self.get_digit_else_zero(0) == 0 {
            self.set_digit(0, digit)?;
        } else {
            self.digits.insert(0, PositionalItem::from_digit(digit));
        }
        return Ok(());
    }

    pub fn shift_right(&mut self, offset: usize) -> Result<(), CalcError> {
        if offset > self.digits.len() {
            return Err(CalcError::from_string(format!(
                "Offset {} is greater than length of the number {}.",
                offset,
                self.digits.len())));
        }
        for _ in 0..offset {
            self.digits.remove(0);
        }
        return Ok(());
    }

    /// Gets number og the digits in the number without sign.
    pub fn digits_len(&self) -> usize {
        return self.digits.len();
    }

    pub fn to_text(&self) -> String {
        return self.digits.iter().rev().copied()
            .map(|d| d.to_text(self.radix))
            .collect::<Vec<_>>()
            .join("");
    }

    /// sets all digits as a string in order to initialize Natural
    pub fn to_digits_as_string(&self) -> String {
        return self.digits.iter().rev().copied()
            .map(|d| d.to_digit_as_string(self.radix))
            .collect::<Vec<_>>()
            .join("");
    }
}


impl fmt::Display for PositionalNumber {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.to_text())
    }
}

