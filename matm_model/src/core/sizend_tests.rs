#[cfg(test)]
mod tests {
    use rstest::rstest;
    use crate::core::index::Index;
    use crate::core::index_range::IndexRange;
    use crate::core::size::SizeND;
    use crate::testing::asserts::assert_result_error_that;

    #[test]
    fn test_new() {
        let s = SizeND::new(vec![2, 3]).unwrap();
        assert_eq!(2, s.dimensions.len());
        assert_eq!(2, s.dimensions[0]);
        assert_eq!(3, s.dimensions[1]);
    }

    #[test]
    fn test_new_empty_dimensions() {
        let result = SizeND::new(Vec::new());
        assert_result_error_that(result)
            .has_message("You need to specify at least one dimension.");
    }

    #[test]
    fn test_new_invalid_row_size() {
        let result = SizeND::new(vec![0, 1]);
        assert_result_error_that(result)
            .has_message("You need to specify positive size of all dimensions.");
    }

    #[test]
    fn test_get_elements_count() {
        let s = SizeND::new(vec![2, 3, 4]).unwrap();
        assert_eq!(24, s.get_elements_count());
    }

    #[test]
    fn test_is_square_2_2() {
        let s = SizeND::new(vec![2, 2]).unwrap();
        assert_eq!(true, s.is_square());
    }

    #[test]
    fn test_is_square_2_3() {
        let s = SizeND::new(vec![2, 3]).unwrap();
        assert_eq!(false, s.is_square());
    }

    #[rstest]
    // row
    #[case(0, 0, false)]
    #[case(0, 1, true)]
    #[case(0, 2, true)]
    #[case(0, 3, false)]
    // column
    #[case(1, 0, false)]
    #[case(1, 1, true)]
    #[case(1, 2, true)]
    #[case(1, 3, true)]
    #[case(1, 4, false)]
    // unbounded
    #[case(2, 0, false)]
    #[case(2, 1, false)]
    #[case(2, 2, false)]
    #[case(2, 3, false)]
    #[case(2, 4, false)]
    fn test_is_index_valid(
        #[case] dim: usize,
        #[case] index: usize,
        #[case] expected: bool,
    ) {
        let s = SizeND::new(vec![2, 3]).unwrap();
        assert_eq!(expected, s.is_index_valid(dim, index));
    }

    #[test]
    fn test_is_index_range_valid_row() {
        let s = SizeND::new(vec![2, 3]).unwrap();
        assert_eq!(true, s.is_index_range_valid(0, &IndexRange::All));
        assert_eq!(true, s.is_index_range_valid(0, &IndexRange::range(
            Index::abs(1).unwrap(),
            Index::abs(2).unwrap()).unwrap()));
        assert_eq!(false, s.is_index_range_valid(0, &IndexRange::range(
            Index::abs(1).unwrap(),
            Index::abs(4).unwrap()).unwrap()));
    }

    #[test]
    fn test_is_index_range_valid_column() {
        let s = SizeND::new(vec![2, 3]).unwrap();
        assert_eq!(true, s.is_index_range_valid(1, &IndexRange::All));
        assert_eq!(true, s.is_index_range_valid(1, &IndexRange::range(
            Index::abs(1).unwrap(),
            Index::abs(2).unwrap()).unwrap()));
        assert_eq!(false, s.is_index_range_valid(1, &IndexRange::range(
            Index::abs(1).unwrap(),
            Index::end_minus(5)).unwrap()));
    }

    #[test]
    fn test_is_index_range_valid_unbounded() {
        let s = SizeND::new(vec![2, 3]).unwrap();
        assert_eq!(false, s.is_index_range_valid(2, &IndexRange::All));
    }

    #[test]
    fn test_format_2_3() {
        let s = SizeND::new(vec![2, 3]).unwrap();
        assert_eq!("2x3", format!("{}", s));
    }

    #[test]
    fn test_format_2_3_4_5() {
        let s = SizeND::new(vec![2, 3, 4, 5]).unwrap();
        assert_eq!("2x3x4x5", format!("{}", s));
    }

}
