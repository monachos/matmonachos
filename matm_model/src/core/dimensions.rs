
pub struct Size<T> {
    pub width: T,
    pub height: T,
}

impl<T> Size<T> {
    pub fn new(w: T, h: T) -> Self {
        Self {
            width: w,
            height: h,
        }
    }
}

//----------------------

pub struct Coordinates<T> {
    pub horizontal: T,
    pub vertical: T,
}

impl<T> Coordinates<T> {
    pub fn new(horiz: T, vert: T) -> Self {
        Self {
            horizontal: horiz,
            vertical: vert,
        }
    }
}
