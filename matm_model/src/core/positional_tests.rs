#[cfg(test)]
mod tests {
    use assert_matches::assert_matches;
    use crate::core::natural::RADIX_10;
    use crate::core::natural::RADIX_16;
    use crate::core::natural::Natural;
    use crate::core::positional::PositionalItem;
    use crate::core::positional::PositionalMark;
    use crate::core::positional::PositionalNumber;
    use crate::testing::asserts::assert_result_error_that;

    #[test]
    fn test_positional_item_from_digit() {
        let item = PositionalItem::from_digit(3);

        match item {
            PositionalItem::Digit(dig) => {
                assert_eq!(3, dig.value());
                assert_matches!(dig.mark(), PositionalMark::Default);
            }
            _ => {
                assert!(false);
            }
        }
    }

    #[test]
    fn test_positional_item_from_digit_mark() {
        let item = PositionalItem::from_digit_mark(3, PositionalMark::Source);

        match item {
            PositionalItem::Digit(dig) => {
                assert_eq!(3, dig.value());
                assert_matches!(dig.mark(), PositionalMark::Source);
            }
            _ => {
                assert!(false);
            }
        }
    }

    #[test]
    fn test_positional_item_get_text_none() {
        let digit = PositionalItem::None;

        assert_eq!(" ", digit.to_text(RADIX_10));
    }

    #[test]
    fn test_positional_item_get_text_unknown() {
        let digit = PositionalItem::Unknown;

        assert_eq!("?", digit.to_text(RADIX_10));
    }

    #[test]
    fn test_positional_item_get_text_5() {
        let digit = PositionalItem::from_digit(5);

        assert_eq!("5", digit.to_text(RADIX_10));
    }

    #[test]
    fn test_positional_item_get_text_0xf() {
        let digit = PositionalItem::from_digit(0xF);

        assert_eq!("f", digit.to_text(RADIX_16));
    }

    #[test]
    fn test_positional_item_get_digit_as_string_none() {
        let digit = PositionalItem::None;

        assert_eq!("0", digit.to_digit_as_string(RADIX_10));
    }

    #[test]
    fn test_positional_item_get_digit_as_string_unknown() {
        let digit = PositionalItem::Unknown;

        assert_eq!("0", digit.to_digit_as_string(RADIX_10));
    }

    #[test]
    fn test_positional_item_get_digit_as_string_5() {
        let digit = PositionalItem::from_digit(5);

        assert_eq!("5", digit.to_digit_as_string(RADIX_10));
    }

    //---------------------------------------------

    #[test]
    fn test_positional_number_new() {
        let number = PositionalNumber::new(RADIX_16);

        assert_eq!(RADIX_16, number.radix);
        assert_eq!(true, number.digits.is_empty());
    }

    #[test]
    fn test_positional_number_is_empty() {
        let number = PositionalNumber::new(RADIX_10);

        assert_eq!(true, number.is_empty());
    }

    #[test]
    fn test_positional_number_is_empty_digit() {
        let mut number = PositionalNumber::new(RADIX_10);
        number.set_digit(0, 1).unwrap();

        assert_eq!(false, number.is_empty());
    }

    #[test]
    fn test_positional_number_is_empty_unknown() {
        let mut number = PositionalNumber::new(RADIX_10);
        number.set_unknown(0).unwrap();

        assert_eq!(false, number.is_empty());
    }

    #[test]
    fn test_positional_number_has_any_digit() {
        let mut number = PositionalNumber::new(RADIX_10);
        number.set_unknown(0).unwrap();
        assert_eq!(false, number.has_any_digit());

        number.set_unknown(3).unwrap();
        assert_eq!(false, number.has_any_digit());

        number.set_digit(1, 0).unwrap();
        assert_eq!(true, number.has_any_digit());

        number.set_unknown(1).unwrap();
        assert_eq!(false, number.has_any_digit());
    }

    #[test]
    fn test_positional_number_get_digit_set_digit() {
        // number: 5~1
        let mut number = PositionalNumber::new(RADIX_10);
        number.set_digit(0, 1).unwrap();
        number.set_digit(2, 5).unwrap();

        assert_eq!("5 1", number.to_text());
        assert_eq!(Some(1), number.get_digit(0));
        assert_eq!(None, number.get_digit(1));
        assert_eq!(Some(5), number.get_digit(2));
        assert_eq!(None, number.get_digit(3));
    }

    #[test]
    fn test_positional_number_set_digit_unknown() {
        // number: 5~?
        let mut number = PositionalNumber::new(RADIX_10);
        number.set_unknown(0).unwrap();
        number.set_digit(2, 5).unwrap();

        assert_eq!("5 ?", number.to_text());
    }

    #[test]
    fn test_positional_number_get_digit_else_zero() {
        let mut number = PositionalNumber::new(RADIX_10);
        number.set_digit(0, 1).unwrap();
        number.set_digit(2, 5).unwrap();
        number.set_unknown(3).unwrap();

        assert_eq!(1, number.get_digit_else_zero(0));
        assert_eq!(0, number.get_digit_else_zero(1));
        assert_eq!(5, number.get_digit_else_zero(2));
        assert_eq!(0, number.get_digit_else_zero(3));
        assert_eq!(0, number.get_digit_else_zero(4));
    }

    #[test]
    fn test_positional_number_has_digit() {
        let mut number = PositionalNumber::new(RADIX_10);
        number.set_digit(0, 1).unwrap();
        number.set_digit(2, 5).unwrap();
        number.set_unknown(3).unwrap();

        assert_eq!(true, number.has_digit(0));
        assert_eq!(false, number.has_digit(1));
        assert_eq!(true, number.has_digit(2));
        assert_eq!(false, number.has_digit(3));
        assert_eq!(false, number.has_digit(4));
    }

    #[test]
    fn test_positional_number_set_digit_mark() {
        let mut number = PositionalNumber::new(RADIX_10);

        number.set_digit_mark(2, 5, PositionalMark::Source).unwrap();

        assert_eq!(3, number.digits.len());
        assert_matches!(number.digits.get(0), Some(&PositionalItem::None));
        assert_matches!(number.digits.get(1), Some(&PositionalItem::None));
        assert_matches!(number.digits.get(2), Some(&_));
        match number.digits.get(2).unwrap() {
            PositionalItem::Digit(pos_dig) => {
                assert_eq!(5, pos_dig.value());
                assert_matches!(pos_dig.mark(), PositionalMark::Source);
            }
            _ => {
                assert!(false);
            }
        }
    }

    #[test]
    fn test_positional_number_mark_digit() {
        let mut number = PositionalNumber::new(RADIX_10);
        number.set_digit(1, 5).unwrap();

        number.mark_digit(1, PositionalMark::Source).unwrap();

        assert_matches!(number.get_digit_mark(1), Some(PositionalMark::Source));
    }

    #[test]
    fn test_positional_number_mark_digit_as_source() {
        let mut number = PositionalNumber::new(RADIX_10);
        number.set_digit(1, 5).unwrap();

        number.mark_digit_as_source(1).unwrap();
        
        assert_matches!(number.get_digit_mark(1), Some(PositionalMark::Source));
    }

    #[test]
    fn test_positional_number_mark_digit_as_target() {
        let mut number = PositionalNumber::new(RADIX_10);
        number.set_digit(1, 5).unwrap();

        number.mark_digit_as_target(1).unwrap();

        assert_matches!(number.get_digit_mark(1), Some(PositionalMark::Target));
    }

    #[test]
    fn test_positional_number_mark_number() {
        let mut number = PositionalNumber::new(RADIX_10);
        number.set_digit(0, 5).unwrap();
        number.set_digit(1, 3).unwrap();

        number.mark_number(PositionalMark::Source);

        assert_matches!(number.get_digit_mark(0), Some(PositionalMark::Source));
        assert_matches!(number.get_digit_mark(1), Some(PositionalMark::Source));
    }

    #[test]
    fn test_positional_number_mark_number_as_source() {
        let mut number = PositionalNumber::new(RADIX_10);
        number.set_digit(0, 5).unwrap();
        number.set_digit(1, 3).unwrap();

        number.mark_number_as_source();

        assert_matches!(number.get_digit_mark(0), Some(PositionalMark::Source));
        assert_matches!(number.get_digit_mark(1), Some(PositionalMark::Source));
    }

    #[test]
    fn test_positional_number_mark_number_as_target() {
        let mut number = PositionalNumber::new(RADIX_10);
        number.set_digit(0, 5).unwrap();
        number.set_digit(1, 3).unwrap();

        number.mark_number_as_target();

        assert_matches!(number.get_digit_mark(0), Some(PositionalMark::Target));
        assert_matches!(number.get_digit_mark(1), Some(PositionalMark::Target));
    }

    #[test]
    fn test_positional_number_remove_marks() {
        let mut number = PositionalNumber::new(RADIX_10);
        number.set_digit_mark(0, 5, PositionalMark::Target).unwrap();
        number.set_digit_mark(1, 3, PositionalMark::Target).unwrap();

        number.remove_marks();

        assert_matches!(number.get_digit_mark(0), Some(PositionalMark::Default));
        assert_matches!(number.get_digit_mark(1), Some(PositionalMark::Default));
    }

    #[test]
    fn test_positional_number_set_value_at() {
        let mut number = PositionalNumber::new(RADIX_10);

        let n1 = Natural::from_int(RADIX_10, 123).unwrap();
        number.set_value_at(0, &n1).unwrap();

        assert_eq!(Some(3), number.get_digit(0));
        assert_eq!(Some(2), number.get_digit(1));
        assert_eq!(Some(1), number.get_digit(2));
        assert_eq!(None, number.get_digit(3));
        assert_eq!(None, number.get_digit(4));

        let n2 = Natural::from_int(RADIX_10, 456).unwrap();
        number.set_value_at(1, &n2).unwrap();

        assert_eq!(Some(3), number.get_digit(0));
        assert_eq!(Some(6), number.get_digit(1));
        assert_eq!(Some(5), number.get_digit(2));
        assert_eq!(Some(4), number.get_digit(3));
        assert_eq!(None, number.get_digit(4));
    }

    #[test]
    fn test_positional_number_get_text() {
        // number: ?2~f
        let mut number = PositionalNumber::new(RADIX_16);
        number.set_digit(0, 0xF).unwrap();
        number.set_digit(2, 0x2).unwrap();
        number.set_unknown(3).unwrap();

        assert_eq!("?2 f", number.to_text());
    }

    #[test]
    fn test_positional_number_format() {
        // number: ?2~f
        let mut number = PositionalNumber::new(RADIX_16);
        number.set_digit(0, 0xF).unwrap();
        number.set_digit(2, 0x2).unwrap();
        number.set_unknown(3).unwrap();

        assert_eq!("?2 f", format!("{}", number));
    }

    #[test]
    fn test_positional_number_get_digits_as_string() {
        // number: ?2~f
        let mut number = PositionalNumber::new(RADIX_16);
        number.set_digit(0, 0xF).unwrap();
        number.set_digit(2, 0x2).unwrap();
        number.set_unknown(3).unwrap();

        assert_eq!("020f", number.to_digits_as_string());
    }

    #[test]
    fn test_add_value_to_digit_0_0() {
        let mut v = PositionalNumber::new(RADIX_10);
        v.set_digit(0, 0).unwrap();

        v.add_value_to_digit(0, 0, PositionalMark::Default).unwrap();
        assert_eq!("0", v.to_string());

        v.add_value_to_digit(1, 0, PositionalMark::Default).unwrap();
        assert_eq!("00", v.to_string()); //TODO is it expected?
    }

    #[test]
    fn test_add_value_to_digit_100_5() {
        let mut v = PositionalNumber::new(RADIX_10);
        v.set_digit(0, 0).unwrap();
        v.set_digit(1, 0).unwrap();
        v.set_digit(2, 1).unwrap();

        v.add_value_to_digit(0, 5, PositionalMark::Default).unwrap();
        assert_eq!("105", v.to_string());

        v.add_value_to_digit(1, 5, PositionalMark::Default).unwrap();
        assert_eq!("155", v.to_string());

        v.add_value_to_digit(2, 5, PositionalMark::Default).unwrap();
        assert_eq!("655", v.to_string());

        v.add_value_to_digit(3, 5, PositionalMark::Default).unwrap();
        assert_eq!("5655", v.to_string());
    }

    #[test]
    fn test_add_value_to_digit_9_1() {
        let mut v = PositionalNumber::new(RADIX_10);
        v.set_digit(0, 9).unwrap();

        v.add_value_to_digit(0, 1, PositionalMark::Default).unwrap();
        assert_eq!("10", v.to_string());
    }

    #[test]
    fn test_add_value_to_digit_99_1() {
        let mut v = PositionalNumber::new(RADIX_10);
        v.set_digit(0, 9).unwrap();
        v.set_digit(1, 9).unwrap();

        v.add_value_to_digit(0, 1, PositionalMark::Default).unwrap();
        assert_eq!("100", v.to_string());
    }

    #[test]
    fn test_add_value_to_digit_999_1() {
        let mut v = PositionalNumber::new(RADIX_10);
        v.set_digit(0, 9).unwrap();
        v.set_digit(1, 9).unwrap();
        v.set_digit(2, 9).unwrap();

        v.add_value_to_digit(0, 1, PositionalMark::Default).unwrap();
        assert_eq!("1000", v.to_string());
    }

    #[test]
    fn test_add_value_to_digit_9_5() {
        let mut v = PositionalNumber::new(RADIX_10);
        v.set_digit(0, 9).unwrap();

        v.add_value_to_digit(0, 5, PositionalMark::Default).unwrap();
        assert_eq!("14", v.to_string());
    }

    #[test]
    fn test_add_value_to_digit_99_5() {
        let mut v = PositionalNumber::new(RADIX_10);
        v.set_digit(0, 9).unwrap();
        v.set_digit(1, 9).unwrap();

        v.add_value_to_digit(0, 5, PositionalMark::Default).unwrap();
        assert_eq!("104", v.to_string());
    }

    #[test]
    fn test_add_value_to_digit_9_10() {
        let mut v = PositionalNumber::new(RADIX_10);
        v.set_digit(0, 9).unwrap();

        v.add_value_to_digit(0, 10, PositionalMark::Default).unwrap();
        assert_eq!("19", v.to_string());
    }

    #[test]
    fn test_add_value_to_digit_9_100() {
        let mut v = PositionalNumber::new(RADIX_10);
        v.set_digit(0, 9).unwrap();

        v.add_value_to_digit(0, 100, PositionalMark::Default).unwrap();
        assert_eq!("109", v.to_string());
    }

    #[test]
    fn test_add_value_to_digit_90_100() {
        let mut v = PositionalNumber::new(RADIX_10);
        v.set_digit(0, 0).unwrap();
        v.set_digit(1, 9).unwrap();

        v.add_value_to_digit(0, 100, PositionalMark::Default).unwrap();
        assert_eq!("190", v.to_string());
    }

    #[test]
    fn test_add_value_to_digit_90_3_at_1() {
        let mut v = PositionalNumber::new(RADIX_10);
        v.set_digit(0, 0).unwrap();
        v.set_digit(1, 9).unwrap();

        v.add_value_to_digit(1, 3, PositionalMark::Default).unwrap();
        assert_eq!("120", v.to_string());
    }

    #[test]
    fn test_add_value_to_digit_90_100_at_1() {
        let mut v = PositionalNumber::new(RADIX_10);
        v.set_digit(0, 0).unwrap();
        v.set_digit(1, 9).unwrap();

        v.add_value_to_digit(1, 100, PositionalMark::Default).unwrap();
        assert_eq!("1090", v.to_string());
    }

    #[test]
    fn test_append_digit_right_to_empty() {
        let mut v = PositionalNumber::new(RADIX_10);

        v.append_digit_right(2).unwrap();
        assert_eq!("2", v.to_string());
    }

    #[test]
    fn test_append_digit_right_to_zero() {
        let mut v = PositionalNumber::new(RADIX_10);
        v.set_digit(0, 0).unwrap();

        v.append_digit_right(2).unwrap();
        assert_eq!("2", v.to_string());
    }

    #[test]
    fn test_append_digit_right_to_existing_digits() {
        let mut v = PositionalNumber::new(RADIX_10);
        v.set_digit(0, 1).unwrap();

        v.append_digit_right(2).unwrap();
        assert_eq!("12", v.to_string());
    }

    #[test]
    fn test_append_digit_right_invalid_value() {
        let mut v = PositionalNumber::new(RADIX_10);
        v.set_digit(0, 1).unwrap();

        assert_result_error_that(v.append_digit_right(13))
            .has_message("Invalid digit value 13 in radix 10.");
    }

    #[test]
    fn test_shift_right_1() {
        let mut v = PositionalNumber::new(RADIX_10);
        v.set_digit(2, 1).unwrap();
        v.set_digit(1, 2).unwrap();
        v.set_digit(0, 3).unwrap();

        v.shift_right(1).unwrap();
        assert_eq!("12", v.to_string());
    }

    #[test]
    fn test_shift_right_2() {
        let mut v = PositionalNumber::new(RADIX_10);
        v.set_digit(2, 1).unwrap();
        v.set_digit(1, 2).unwrap();
        v.set_digit(0, 3).unwrap();

        v.shift_right(2).unwrap();
        assert_eq!("1", v.to_string());
    }

    #[test]
    fn test_shift_right_all() {
        let mut v = PositionalNumber::new(RADIX_10);
        v.set_digit(2, 1).unwrap();
        v.set_digit(1, 2).unwrap();
        v.set_digit(0, 3).unwrap();

        v.shift_right(3).unwrap();
        assert_eq!("", v.to_string());
    }

    #[test]
    fn test_shift_right_over() {
        let mut v = PositionalNumber::new(RADIX_10);
        v.set_digit(2, 1).unwrap();
        v.set_digit(1, 2).unwrap();
        v.set_digit(0, 3).unwrap();

        let result = v.shift_right(4);
        assert_result_error_that(result)
            .has_message("Offset 4 is greater than length of the number 3.");
    }
}