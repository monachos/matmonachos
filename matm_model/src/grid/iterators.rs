use crate::core::index::Index2D;
use crate::core::index::IndexND;
use crate::core::size::SizeND;


enum IteratorState<T> {
    Initial,
    Iterating(T),
    Finished,
}

//--------------------------------------

struct IteratorData<T> {
    size: SizeND,
    state: IteratorState<T>,
}

impl <T> IteratorData<T> {
    pub fn new(size: SizeND) -> Self {
        IteratorData {
            size: size,
            state: IteratorState::Initial,
        }
    }
}

//--------------------------------------

pub struct RightDown2DElementIterator {
    iterator: RightDownElementIterator,
}

impl RightDown2DElementIterator {
    pub fn new(rows: usize, cols: usize) -> Self {
        RightDown2DElementIterator {
            iterator: RightDownElementIterator::new(SizeND::new(
                vec![rows, cols]
            ).unwrap())
        }
    }
}

impl Iterator for RightDown2DElementIterator {
    type Item = Index2D;

    fn next(&mut self) -> Option<Self::Item> {
        return match self.iterator.next() {
            Some(idx) => Some(Index2D::new(
                    idx.value_of(0).unwrap(),
                    idx.value_of(1).unwrap()
                ).unwrap()),
            None => None,
        };
    }
}

//--------------------------------------

pub struct DownRight2DElementIterator {
    iterator: DownRightElementIterator,
}

impl DownRight2DElementIterator {
    pub fn new(rows: usize, cols: usize) -> Self {
        DownRight2DElementIterator {
            iterator: DownRightElementIterator::new(SizeND::new(
                vec![rows, cols]
            ).unwrap())
        }
    }
}

impl Iterator for DownRight2DElementIterator {
    type Item = Index2D;

    fn next(&mut self) -> Option<Self::Item> {
        return match self.iterator.next() {
            Some(idx) => Some(Index2D::new(
                    idx.value_of(0).unwrap(),
                    idx.value_of(1).unwrap()
                ).unwrap()),
            None => None,
        };
    }
}

//-----------------------------------------------

pub struct DownRightLowerTriElementIterator {
    data: IteratorData<Index2D>,
}

impl DownRightLowerTriElementIterator {
    pub fn new(rows: usize, cols: usize) -> Self {
        DownRightLowerTriElementIterator {
            data: IteratorData::new(SizeND::new(vec![rows, cols]).unwrap()),
        }
    }
}

impl Iterator for DownRightLowerTriElementIterator {
    type Item = Index2D;

    fn next(&mut self) -> Option<Self::Item> {
        match &mut self.data.state {
            IteratorState::Initial => {
                self.data.state = IteratorState::Iterating(Index2D::one());
                return Some(Index2D::one());
            }
            IteratorState::Iterating(cur) => {
                if cur.row < self.data.size.dimensions[0] {
                    cur.row += 1;
                    return Some(Index2D::new(cur.row, cur.column).unwrap());
                } else {
                    cur.column += 1;
                    cur.row = cur.column;
                    if cur.column <= self.data.size.dimensions[1] {
                        return Some(Index2D::new(cur.row, cur.column).unwrap());
                    } else {
                        self.data.state = IteratorState::Finished;
                    }
                }
            }
            IteratorState::Finished => {
            }
        }
        return None;
    }
}

//--------------------------------------

pub struct RightDownElementIterator {
    data: IteratorData<Vec<usize>>,
}

impl RightDownElementIterator {
    pub fn new(size: SizeND) -> Self {
        RightDownElementIterator {
            data: IteratorData::new(size),
        }
    }
}

impl Iterator for RightDownElementIterator {
    type Item = IndexND;

    fn next(&mut self) -> Option<Self::Item> {
        match &mut self.data.state {
            IteratorState::Initial => {
                let dim = self.data.size.dimensions.len();
                self.data.state = IteratorState::Iterating(vec![1; dim]);
                return Some(IndexND::ones(dim).unwrap());
            }
            IteratorState::Iterating(cur) => {
                for i in (0..self.data.size.dimensions.len()).rev() {
                    if cur[i] < self.data.size.dimensions[i] {
                        cur[i] += 1;
                        return Some(IndexND::new(cur.clone()).unwrap());
                    } else {
                        cur[i] = 1;
                    }
                }
                self.data.state = IteratorState::Finished;
            }
            IteratorState::Finished => {}
        }
        return None;
    }
}

//--------------------------------------

pub struct DownRightElementIterator {
    data: IteratorData<Vec<usize>>,
}

impl DownRightElementIterator {
    pub fn new(size: SizeND) -> Self {
        DownRightElementIterator {
            data: IteratorData::new(size),
        }
    }
}

impl Iterator for DownRightElementIterator {
    type Item = IndexND;

    fn next(&mut self) -> Option<Self::Item> {
        match &mut self.data.state {
            IteratorState::Initial => {
                let dim = self.data.size.dimensions.len();
                self.data.state = IteratorState::Iterating(vec![1; dim]);
                return Some(IndexND::ones(dim).unwrap());
            }
            IteratorState::Iterating(cur) => {
                for i in 0..self.data.size.dimensions.len() {
                    if cur[i] < self.data.size.dimensions[i] {
                        cur[i] += 1;
                        return Some(IndexND::new(cur.clone()).unwrap());
                    } else {
                        cur[i] = 1;
                    }
                }
                self.data.state = IteratorState::Finished;
            }
            IteratorState::Finished => {}
        }
        return None;
    }
}
