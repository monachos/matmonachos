pub mod iterators;
mod testing;

use core::ops::Range;
use matm_error::error::CalcError;
use crate::core::index::IndexND;
use crate::core::index_range::IndexRange;
use crate::core::size::SizeND;
use crate::grid::iterators::RightDownElementIterator;
use crate::grid::iterators::DownRightElementIterator;

pub type FormatIndexType = fn(&Vec<usize>) -> String;


#[derive(PartialEq)]
#[derive(Debug)]
pub struct Grid<T> {
    pub size: SizeND,

    /// vector of data: all elements in 1st row, 2nd row etc.
    pub data: Vec<T>,
}

impl<T> Grid<T> {
    pub fn from_vector(dims: Vec<usize>, elements: Vec<T>) -> Result<Self, CalcError> {
        let dimensions = SizeND::new(dims)?;
        let expected_size = dimensions.get_elements_count();
        if elements.len() != expected_size {
            return Err(CalcError::from_string(format!("Invalid number of elements, {} given, {} expected.",
            elements.len(),
            expected_size)));
        }
        Ok(Self {
            size: dimensions,
            data: elements,
        })
    }

    pub fn size_of(&self, dimension_index: usize) -> Result<usize, CalcError> {
        return match self.size.dimensions.get(dimension_index) {
            Some(d) => Ok(*d),
            None => Err(CalcError::from_string(format!(
                "Invalid dimension index {}.",
                dimension_index)))
        };
    }

    pub fn is_square(&self) -> bool {
        self.size.is_square()
    }

    pub fn is_index_valid(&self, idx: &Vec<usize>) -> bool {
        if idx.len() != self.size.dimensions.len() {
            return false;
        }
        for (i, index_value) in idx.iter().enumerate() {
            if !self.size.is_index_valid(i, *index_value) {
                return false;
            }
        }
        return true;
    }

    pub fn is_index_range_valid(&self, dimension_index: usize, range: &IndexRange) -> bool {
        self.size.is_index_range_valid(dimension_index, range)
    }

    /// ..., page, row, column - one-based index of element
    pub fn get_index(
        &self,
        idx: &Vec<usize>,
        idx_fmt: FormatIndexType,
    ) -> Result<usize, CalcError> {
        match self.is_index_valid(idx) {
            true => {
                let mut result: usize = 0;
                let mut factor: usize = 1;
                let mut dim_it = self.size.dimensions.iter().rev();
                for idx_value in idx.iter().rev() {
                    result += (*idx_value - 1) * factor;
                    factor = *dim_it.next().unwrap();
                }
                return Ok(result);
            }
            false => Err(CalcError::from_string(format!("Invalid element index {}.",
                idx_fmt(idx))))
        }
    }

    pub fn get_at(
        &self,
        idx: &Vec<usize>,
        idx_fmt: FormatIndexType,
    ) -> Result<&T, CalcError> {
        let i = self.get_index(idx, idx_fmt)?;
        let opt_el = self.data.get(i);
        match opt_el {
            Some(el) => Ok(el),
            None => Err(CalcError::from_string(format!("Invalid element index {}.",
                idx_fmt(idx))))
        }
    }

    pub fn get_mut_at(
        &mut self,
        idx: &Vec<usize>,
        idx_fmt: FormatIndexType,
    ) -> Result<&mut T, CalcError> {
        let i = self.get_index(idx, idx_fmt)?;
        let opt_el = self.data.get_mut(i);
        match opt_el {
            Some(el) => Ok(el),
            None => Err(CalcError::from_string(format!("Invalid element index {}.",
                idx_fmt(idx))))
        }
    }

    pub fn get(
        &self,
        index: &IndexND,
        idx_fmt: FormatIndexType,
    ) -> Result<&T, CalcError> {
        return self.get_at(index.value(), idx_fmt);
    }

    pub fn get_mut(&mut self, index: &IndexND) -> Result<&mut T, CalcError> {
        return self.get_mut_at(index.value(), IndexND::format_index);
    }

    pub fn set_at(
        &mut self,
        idx: &Vec<usize>,
        el: T,
        idx_fmt: FormatIndexType,
    ) -> Result<(), CalcError> {
        let i = self.get_index(idx, idx_fmt)?;
        if let Some(fr) = self.data.get_mut(i) {
            *fr = el;
            Ok(())
        } else {
            Err(CalcError::from_string(format!("Invalid element index {}.",
                idx_fmt(idx))))
        }
    }

    pub fn set(
        &mut self,
        index: &IndexND,
        el: T,
        idx_fmt: FormatIndexType,
    ) -> Result<(), CalcError> {
        self.set_at(index.value(), el, idx_fmt)
    }

    pub fn align_index(&self, dimension_index: usize, index: usize) -> Result<usize, CalcError> {
        if index == 0 {
            return Err(CalcError::from_string(format!("Invalid index {}.", index)));
        }
        return Ok((index - 1) % self.size_of(dimension_index)? + 1);
    }

    // Gets range for specified 0-based dimension
    pub fn dimension_range(&self, dimension_index: usize) -> Result<Range<usize>, CalcError> {
        let s = self.size_of(dimension_index)?;
        return Ok(1..s+1);
    }

    pub fn right_down_iter(&self) -> RightDownElementIterator {
        RightDownElementIterator::new(self.size.clone())
    }

    pub fn down_right_iter(&self) -> DownRightElementIterator {
        DownRightElementIterator::new(self.size.clone())
    }
}

