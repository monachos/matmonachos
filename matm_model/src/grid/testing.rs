#[cfg(test)]
mod tests {
    use rstest::rstest;
    use crate::core::index::Index;
    use crate::core::index::IndexND;
    use crate::core::index_range::IndexRange;
    use crate::testing::asserts::assert_result_error_that;
    use crate::grid::Grid;

    #[test]
    fn test_from_vector() {
        // 3 columns, 2 rows (reverted order than in matrices)
        let g = Grid::from_vector(vec![2, 3],
            vec![1, 2, 3, 4, 5, 6]).unwrap();
        assert_eq!(2, g.size_of(0).unwrap());
        assert_eq!(3, g.size_of(1).unwrap());
        assert_eq!(1, *g.get_at(&vec![1, 1], IndexND::format_index).unwrap());
        assert_eq!(2, *g.get_at(&vec![1, 2], IndexND::format_index).unwrap());
        assert_eq!(3, *g.get_at(&vec![1, 3], IndexND::format_index).unwrap());
        assert_eq!(4, *g.get_at(&vec![2, 1], IndexND::format_index).unwrap());
        assert_eq!(5, *g.get_at(&vec![2, 2], IndexND::format_index).unwrap());
        assert_eq!(6, *g.get_at(&vec![2, 3], IndexND::format_index).unwrap());
    }

    #[test]
    fn test_from_vector_invalid_row_size() {
        let g = Grid::from_vector(vec![3, 0], vec![1; 1]);
        assert_result_error_that(g)
            .has_message("You need to specify positive size of all dimensions.");
    }

    #[test]
    fn test_from_vector_invalid_column_size() {
        let g = Grid::from_vector(vec![0, 2], vec![1; 1]);
        assert_result_error_that(g)
            .has_message("You need to specify positive size of all dimensions.");
    }

    #[test]
    fn test_from_vector_invalid_element_count() {
        let g = Grid::from_vector(vec![3, 2], vec![1; 1]);
        assert_result_error_that(g)
            .has_message("Invalid number of elements, 1 given, 6 expected.");
    }

    #[test]
    fn test_size_of() {
        let m = Grid::from_vector(vec![2, 3],
            vec![1, 2, 3, 4, 5, 6]).unwrap();
        assert_eq!(2, m.size_of(0).unwrap());
        assert_eq!(3, m.size_of(1).unwrap());
    }

    #[test]
    fn test_size_of_unbounded() {
        let m = Grid::from_vector(vec![2, 3],
            vec![1, 2, 3, 4, 5, 6]).unwrap();
        assert_result_error_that(m.size_of(2))
            .has_message("Invalid dimension index 2.");
    }

    #[test]
    fn test_get_index_23_12() {
        let g = Grid::from_vector(vec![2, 3],
            vec![11, 12, 13, 21, 22, 23]).unwrap();
        let index = g.get_index(&vec![1, 2], IndexND::format_index).unwrap();
        assert_eq!(1, index);
    }

    #[test]
    fn test_get_index_23_21() {
        let g = Grid::from_vector(vec![2, 3],
            vec![11, 12, 13, 21, 22, 23]).unwrap();
        let index = g.get_index(&vec![2, 1], IndexND::format_index).unwrap();
        assert_eq!(3, index);
    }

    #[test]
    fn test_get_index_23_22() {
        let g = Grid::from_vector(vec![2, 3],
            vec![11, 12, 13, 21, 22, 23]).unwrap();
        let index = g.get_index(&vec![2, 2], IndexND::format_index).unwrap();
        assert_eq!(4, index);
    }

    #[test]
    fn test_get_index_43_42() {
        let g = Grid::from_vector(vec![4, 3],
            vec![11, 12, 13, 21, 22, 23, 31, 32, 33, 41, 42, 43]).unwrap();
        let index = g.get_index(&vec![4, 2], IndexND::format_index).unwrap();
        assert_eq!(10, index);
    }

    #[test]
    fn test_get_index_43_52_unbounded() {
        let g = Grid::from_vector(vec![4, 3],
            vec![11, 12, 13, 21, 22, 23, 31, 32, 33, 41, 42, 43]).unwrap();
        let r = g.get_index(&vec![5, 2], IndexND::format_index);
        assert_result_error_that(r)
            .has_message("Invalid element index (5, 2).");
    }

    #[test]
    fn test_is_square() {
        let grid = Grid::from_vector(vec![2, 2], vec![0; 4]).unwrap();
        assert_eq!(true, grid.is_square());
    }

    #[test]
    fn test_is_square_1_2() {
        let grid = Grid::from_vector(vec![1, 2], vec![0; 2]).unwrap();
        assert_eq!(false, grid.is_square());
    }

    #[test]
    fn test_is_square_2_1() {
        let grid = Grid::from_vector(vec![2, 1], vec![0; 2]).unwrap();
        assert_eq!(false, grid.is_square());
    }

    #[test]
    fn test_is_index_valid_ok() {
        let grid = Grid::from_vector(vec![1, 2], vec![0; 2]).unwrap();
        assert_eq!(true, grid.is_index_valid(&vec![1, 1]));
    }

    #[test]
    fn test_is_index_valid_diff_dimentions() {
        let grid = Grid::from_vector(vec![1, 2], vec![0; 2]).unwrap();
        assert_eq!(false, grid.is_index_valid(&vec![1, 1, 1]));
    }

    #[test]
    fn test_is_index_valid_unbounded() {
        let grid = Grid::from_vector(vec![1, 2], vec![0; 2]).unwrap();
        assert_eq!(false, grid.is_index_valid(&vec![2, 1]));
    }

    #[test]
    fn test_is_index_range_valid_row() {
        let grid = Grid::from_vector(vec![2, 3], vec![0; 6]).unwrap();
        assert_eq!(true, grid.is_index_range_valid(0, &IndexRange::All));
        assert_eq!(true, grid.is_index_range_valid(0, &IndexRange::range(
            Index::abs(1).unwrap(),
            Index::abs(2).unwrap()).unwrap()));
        assert_eq!(false, grid.is_index_range_valid(0, &IndexRange::range(
            Index::abs(1).unwrap(),
            Index::abs(4).unwrap()).unwrap()));
    }

    #[test]
    fn test_is_index_range_valid_column() {
        let grid = Grid::from_vector(vec![2, 3], vec![0; 6]).unwrap();
        assert_eq!(true, grid.is_index_range_valid(1, &IndexRange::All));
        assert_eq!(true, grid.is_index_range_valid(1, &IndexRange::range(
            Index::abs(1).unwrap(),
            Index::abs(2).unwrap()).unwrap()));
        assert_eq!(false, grid.is_index_range_valid(1, &IndexRange::range(
            Index::abs(1).unwrap(),
            Index::end_minus(5)).unwrap()));
    }

    #[test]
    fn test_is_index_range_valid_unbounded() {
        let grid = Grid::from_vector(vec![2, 3], vec![0; 6]).unwrap();
        assert_eq!(false, grid.is_index_range_valid(2, &IndexRange::All));
    }

    #[test]
    fn test_get_at_bounded() {
        let grid = Grid::from_vector(vec![1, 2], vec![0; 2]).unwrap();
        assert_eq!(0, *grid.get_at(&vec![1, 1], IndexND::format_index).unwrap());
        assert_eq!(0, *grid.get_at(&vec![1, 2], IndexND::format_index).unwrap());
    }

    #[test]
    fn test_get_at_unbounded_row_too_small() {
        let grid = Grid::from_vector(vec![1, 2], vec![0; 2]).unwrap();
        assert_result_error_that(grid.get_at(&vec![0, 1], IndexND::format_index))
            .has_message("Invalid element index (0, 1).");
    }

    #[test]
    fn test_get_at_unbounded_row_too_big() {
        let grid = Grid::from_vector(vec![1, 2], vec![0; 2]).unwrap();
        assert_result_error_that(grid.get_at(&vec![2, 1], IndexND::format_index))
            .has_message("Invalid element index (2, 1).");
    }

    #[test]
    fn test_get_at_unbounded_column_too_small() {
        let grid = Grid::from_vector(vec![1, 2], vec![0; 2]).unwrap();
        assert_result_error_that(grid.get_at(&vec![0, 1], IndexND::format_index))
            .has_message("Invalid element index (0, 1).");
    }

    #[test]
    fn test_get_at_unbounded_column_too_big() {
        let grid = Grid::from_vector(vec![1, 2], vec![0; 2]).unwrap();
        assert_result_error_that(grid.get_at(&vec![1, 3], IndexND::format_index))
            .has_message("Invalid element index (1, 3).");
    }

    #[test]
    fn test_get_bounded() {
        let grid = Grid::from_vector(vec![1, 2], vec![0; 2]).unwrap();
        assert_eq!(0, *grid.get(&IndexND::new(vec![1, 1]).unwrap(), IndexND::format_index).unwrap());
        assert_eq!(0, *grid.get(&IndexND::new(vec![1, 2]).unwrap(), IndexND::format_index).unwrap());
    }

    #[test]
    fn test_set_at_bounded() {
        let mut grid = Grid::from_vector(vec![1, 2], vec![0; 2]).unwrap();
        grid.set_at(&vec![1, 1], 1, IndexND::format_index).unwrap();
        assert_eq!(1, *grid.get_at(&vec![1, 1], IndexND::format_index).unwrap());
        assert_eq!(0, *grid.get_at(&vec![1, 2], IndexND::format_index).unwrap());
    }

    #[test]
    fn test_set_at_unbounded_row_too_small() {
        let mut grid = Grid::from_vector(vec![1, 2], vec![0; 2]).unwrap();
        assert_result_error_that(grid.set_at(&vec![0, 1], 1, IndexND::format_index))
            .has_message("Invalid element index (0, 1).");
    }

    #[test]
    fn test_set_at_unbounded_row_too_big() {
        let mut grid = Grid::from_vector(vec![1, 2], vec![0; 2]).unwrap();
        assert_result_error_that(grid.set_at(&vec![2, 1], 1, IndexND::format_index))
            .has_message("Invalid element index (2, 1).");
    }

    #[test]
    fn test_set_bounded() {
        let mut grid = Grid::from_vector(vec![1, 2], vec![0; 2]).unwrap();
        grid.set(&IndexND::new(vec![1, 1]).unwrap(), 1, IndexND::format_index).unwrap();
        assert_eq!(1, *grid.get_at(&vec![1, 1], IndexND::format_index).unwrap());
        assert_eq!(0, *grid.get_at(&vec![1, 2], IndexND::format_index).unwrap());
    }

    #[test]
    fn test_get_mut_at() {
        let mut grid = Grid::from_vector(vec![1, 2], vec![13, 17]).unwrap();
        match grid.get_mut_at(&vec![1, 1], IndexND::format_index) {
            Ok(item) => {
                assert_eq!(13, *item);
                *item = 2;
            }
            Err(_) => {
                assert_eq!(true, false);
            }
        }
    }

    #[test]
    fn test_get_mut_at_unbounded() {
        let mut grid = Grid::from_vector(vec![1, 2], vec![13, 17]).unwrap();
        assert_result_error_that(grid.get_mut_at(&vec![1, 3], IndexND::format_index))
            .has_message("Invalid element index (1, 3).");
    }

    #[test]
    fn test_get_mut_from_index() {
        let mut grid = Grid::from_vector(vec![1, 2], vec![13, 17]).unwrap();
        let idx = IndexND::new(vec![1, 1]).unwrap();
        match grid.get_mut(&idx) {
            Ok(item) => {
                assert_eq!(13, *item);
                *item = 2;
            }
            Err(_) => {
                assert_eq!(true, false);
            }
        }
    }

    #[test]
    fn test_get_mut_from_index_unbounded() {
        let mut grid = Grid::from_vector(vec![1, 2], vec![13, 17]).unwrap();
        let idx = IndexND::new(vec![1, 3]).unwrap();
        assert_result_error_that(grid.get_mut(&idx))
            .has_message("Invalid element index (1, 3).");
    }

    #[test]
    fn test_set_from_index() {
        let mut grid = Grid::from_vector(vec![1, 2], vec![13, 17]).unwrap();
        let idx = IndexND::new(vec![1, 1]).unwrap();
        match grid.set(&idx, 2, IndexND::format_index) {
            Ok(_) => {}
            Err(_) => {
                assert_eq!(true, false);
            }
        }

        match grid.get(&idx, IndexND::format_index) {
            Ok(item) => {
                assert_eq!(2, *item);
            }
            Err(_) => {
                assert_eq!(true, false);
            }
        }
    }

    #[test]
    fn test_set_from_index_unbounded() {
        let mut grid = Grid::from_vector(vec![1, 2], vec![13, 17]).unwrap();
        let idx = IndexND::new(vec![1, 3]).unwrap();
        assert_result_error_that(grid.set(&idx, 2, IndexND::format_index))
            .has_message("Invalid element index (1, 3).");
    }

    #[test]
    fn test_align_index_row_1_1() {
        let grid = Grid::from_vector(vec![1, 1], vec![0; 1]).unwrap();
        assert_eq!(1, grid.align_index(1, 1).unwrap());
    }

    #[test]
    fn test_align_index_row_1_2() {
        let grid = Grid::from_vector(vec![1, 1], vec![0; 1]).unwrap();
        assert_eq!(1, grid.align_index(1, 1).unwrap());
    }

    #[test]
    fn test_align_index_row_1_3() {
        let grid = Grid::from_vector(vec![1, 1], vec![0; 1]).unwrap();
        assert_eq!(1, grid.align_index(1, 3).unwrap());
    }

    #[test]
    fn test_align_index_row_3_3() {
        let grid = Grid::from_vector(vec![1, 3], vec![0; 3]).unwrap();
        assert_eq!(3, grid.align_index(1, 3).unwrap());
    }

    #[test]
    fn test_align_index_row_3_5() {
        let grid = Grid::from_vector(vec![1, 3], vec![0; 3]).unwrap();
        assert_eq!(2, grid.align_index(1, 5).unwrap());
    }

    #[test]
    fn test_align_index_row_zero() {
        let grid = Grid::from_vector(vec![1, 1], vec![0; 1]).unwrap();
        assert_result_error_that(grid.align_index(1, 0))
            .has_message("Invalid index 0.");
    }

    #[test]
    fn test_align_index_column_1_1() {
        let grid = Grid::from_vector(vec![1, 1], vec![0; 1]).unwrap();
        assert_eq!(1, grid.align_index(0, 1).unwrap());
    }

    #[test]
    fn test_align_index_column_1_2() {
        let grid = Grid::from_vector(vec![1, 1], vec![0; 1]).unwrap();
        assert_eq!(1, grid.align_index(0, 2).unwrap());
    }

    #[test]
    fn test_align_index_column_1_3() {
        let grid = Grid::from_vector(vec![1, 1], vec![0; 1]).unwrap();
        assert_eq!(1, grid.align_index(0, 3).unwrap());
    }

    #[test]
    fn test_align_index_column_3_3() {
        let grid = Grid::from_vector(vec![3, 1], vec![0; 3]).unwrap();
        assert_eq!(3, grid.align_index(0, 3).unwrap());
    }

    #[test]
    fn test_align_index_column_3_5() {
        let grid = Grid::from_vector(vec![3, 1], vec![0; 3]).unwrap();
        assert_eq!(2, grid.align_index(0, 5).unwrap());
    }

    #[test]
    fn test_align_index_column_zero() {
        let grid = Grid::from_vector(vec![3, 1], vec![0; 3]).unwrap();
        assert_result_error_that(grid.align_index(0, 0))
            .has_message("Invalid index 0.");
    }

    #[rstest]
    #[case(0, 1, 3)]
    #[case(1, 1, 4)]
    fn test_dimension_range(
        #[case] dim: usize,
        #[case] expected_start: usize,
        #[case] expected_end: usize,
    ) {
        let grid = Grid::from_vector(vec![2, 3], vec![0; 6]).unwrap();
        let result = grid.dimension_range(dim).unwrap();
        assert_eq!(expected_start, result.start);
        assert_eq!(expected_end, result.end);
    }

    #[test]
    fn test_dimension_range_unbound() {
        let grid = Grid::from_vector(vec![2, 3], vec![0; 6]).unwrap();
        assert_result_error_that(grid.dimension_range(2))
            .has_message("Invalid dimension index 2.");
    }

    #[test]
    fn test_right_down_element_iterator_23() {
        let grid = Grid::from_vector(vec![2, 3], vec![0; 6]).unwrap();
        let mut it = grid.right_down_iter();
        assert_eq!(true, it.next() == Some(IndexND::new(vec![1, 1]).unwrap()));
        assert_eq!(true, it.next() == Some(IndexND::new(vec![1, 2]).unwrap()));
        assert_eq!(true, it.next() == Some(IndexND::new(vec![1, 3]).unwrap()));
        assert_eq!(true, it.next() == Some(IndexND::new(vec![2, 1]).unwrap()));
        assert_eq!(true, it.next() == Some(IndexND::new(vec![2, 2]).unwrap()));
        assert_eq!(true, it.next() == Some(IndexND::new(vec![2, 3]).unwrap()));
        assert_eq!(true, it.next() == None);
    }

    #[test]
    fn test_right_down_element_iterator_222() {
        let grid = Grid::from_vector(vec![2, 2, 2], vec![0; 8]).unwrap();
        let mut it = grid.right_down_iter();
        assert_eq!(true, it.next() == Some(IndexND::new(vec![1, 1, 1]).unwrap()));
        assert_eq!(true, it.next() == Some(IndexND::new(vec![1, 1, 2]).unwrap()));
        assert_eq!(true, it.next() == Some(IndexND::new(vec![1, 2, 1]).unwrap()));
        assert_eq!(true, it.next() == Some(IndexND::new(vec![1, 2, 2]).unwrap()));
        assert_eq!(true, it.next() == Some(IndexND::new(vec![2, 1, 1]).unwrap()));
        assert_eq!(true, it.next() == Some(IndexND::new(vec![2, 1, 2]).unwrap()));
        assert_eq!(true, it.next() == Some(IndexND::new(vec![2, 2, 1]).unwrap()));
        assert_eq!(true, it.next() == Some(IndexND::new(vec![2, 2, 2]).unwrap()));
        assert_eq!(true, it.next() == None);
    }

    #[test]
    fn test_down_right_element_iterator_23() {
        let grid = Grid::from_vector(vec![2, 3], vec![0; 6]).unwrap();
        let mut it = grid.down_right_iter();
        assert_eq!(true, it.next() == Some(IndexND::new(vec![1, 1]).unwrap()));
        assert_eq!(true, it.next() == Some(IndexND::new(vec![2, 1]).unwrap()));
        assert_eq!(true, it.next() == Some(IndexND::new(vec![1, 2]).unwrap()));
        assert_eq!(true, it.next() == Some(IndexND::new(vec![2, 2]).unwrap()));
        assert_eq!(true, it.next() == Some(IndexND::new(vec![1, 3]).unwrap()));
        assert_eq!(true, it.next() == Some(IndexND::new(vec![2, 3]).unwrap()));
        assert_eq!(true, it.next() == None);
    }

    #[test]
    fn test_down_right_element_iterator_222() {
        let grid = Grid::from_vector(vec![2, 2, 2], vec![0; 8]).unwrap();
        let mut it = grid.down_right_iter();
        assert_eq!(true, it.next() == Some(IndexND::new(vec![1, 1, 1]).unwrap()));
        assert_eq!(true, it.next() == Some(IndexND::new(vec![2, 1, 1]).unwrap()));
        assert_eq!(true, it.next() == Some(IndexND::new(vec![1, 2, 1]).unwrap()));
        assert_eq!(true, it.next() == Some(IndexND::new(vec![2, 2, 1]).unwrap()));
        assert_eq!(true, it.next() == Some(IndexND::new(vec![1, 1, 2]).unwrap()));
        assert_eq!(true, it.next() == Some(IndexND::new(vec![2, 1, 2]).unwrap()));
        assert_eq!(true, it.next() == Some(IndexND::new(vec![1, 2, 2]).unwrap()));
        assert_eq!(true, it.next() == Some(IndexND::new(vec![2, 2, 2]).unwrap()));
        assert_eq!(true, it.next() == None);
    }

}
