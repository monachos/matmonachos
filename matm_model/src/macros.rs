use crate::factories::ObjFactory;


pub enum ExprLiteralWrapper {
    Int(i32),
    Symbol(String),
}

impl From<i32> for ExprLiteralWrapper {
    fn from(value: i32) -> ExprLiteralWrapper {
        ExprLiteralWrapper::Int(value)
    }
}

impl<'a> From<&'a str> for ExprLiteralWrapper {
    fn from(value: &'a str) -> ExprLiteralWrapper {
        ExprLiteralWrapper::Symbol(String::from(value))
    }
}

pub fn get_ezi(fa: &ObjFactory, v: i32) -> crate::expressions::Expression {
    fa.ezi_u(v)
}


#[macro_export]
macro_rules! scalar {
    ($fa: expr, $v:literal) => {
        match crate::macros::ExprLiteralWrapper::from($v) {
            crate::macros::ExprLiteralWrapper::Int(i) => $fa.rzi_u(i),
            crate::macros::ExprLiteralWrapper::Symbol(s) => $fa.rsn(&s),
        }
    };
    ($fa: expr, (- $v:literal)) => {
        $fa.rzi_u(-$v)
    };
    ($fa: expr, ($num:literal / $den:literal)) => {
        $fa.rfi_u($num, $den)
    };
}

#[macro_export]
macro_rules! matrix {
    // matrix filled with zeros
    ($fa: expr, $rows: literal, $cols: literal) => {
        crate::expressions::matrices::MatrixData::zeros(
            $fa.radix(),
            $rows,
            $cols)
    };
    ($fa: expr, $rows: literal, $cols: literal; $($el:tt),+) => {
        crate::expressions::matrices::MatrixData::from_vector(
            $rows,
            $cols,
            std::vec![$( crate::scalar!($fa, $el) ),*])
    };
}
