#[cfg(test)]
mod tests {
    use crate::testing::asserts::assert_expression_that;
    use crate::testing::asserts::assert_matrix_that;
    use crate::factories::ObjFactory;
    use crate::matrix;


    #[test]
    fn test_matrix2d_zeros() {
        let fa = ObjFactory::new10();

        let m = matrix!(fa, 2, 3).unwrap();

        assert_matrix_that(&m)
            .has_rows(2)
            .has_columns(3);
    }

    #[test]
    fn test_matrix2d_ints() {
        let fa = ObjFactory::new10();

        let m = matrix!(fa, 2, 3;
            1, 2, 3,
            (-4), (-5), (-6)).unwrap();

        assert_matrix_that(&m)
            .has_rows(2)
            .has_columns(3)
            .is_i_i_integer_that(1, 1, |e| {e.is_int(1);})
            .is_i_i_integer_that(1, 2, |e| {e.is_int(2);})
            .is_i_i_integer_that(1, 3, |e| {e.is_int(3);})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(-4);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(-5);})
            .is_i_i_integer_that(2, 3, |e| {e.is_int(-6);})
        ;
    }

    #[test]
    fn test_matrix2d_fractions() {
        let fa = ObjFactory::new10();

        let m = matrix!(fa, 2, 3;
            (-1/2), (2/-3), (-3/-4),
            (4/5), (5/6), (6/7)
        ).unwrap();

        assert_matrix_that(&m)
            .has_rows(2)
            .has_columns(3)
            .is_i_i_fraction_that(1, 1, |e| {e.is_ints(-1, 2);})
            .is_i_i_fraction_that(1, 2, |e| {e.is_ints(2, -3);})
            .is_i_i_fraction_that(1, 3, |e| {e.is_ints(-3, -4);})
            .is_i_i_fraction_that(2, 1, |e| {e.is_ints(4, 5);})
            .is_i_i_fraction_that(2, 2, |e| {e.is_ints(5, 6);})
            .is_i_i_fraction_that(2, 3, |e| {e.is_ints(6, 7);})
        ;
    }

    #[test]
    fn test_matrix2d_mixed_types() {
        let fa = ObjFactory::new10();

        let m = matrix!(fa, 2, 3;
            (1/2), (2/3), "x",
            4, 5, "y"
        ).unwrap();

        assert_matrix_that(&m)
            .has_rows(2)
            .has_columns(3)
            .is_i_i_fraction_that(1, 1, |e| {e.is_ints(1, 2);})
            .is_i_i_fraction_that(1, 2, |e| {e.is_ints(2, 3);})
            .is_i_i_symbol_that(1, 3, |e| {e.has_name("x");})
            .is_i_i_integer_that(2, 1, |e| {e.is_int(4);})
            .is_i_i_integer_that(2, 2, |e| {e.is_int(5);})
            .is_i_i_symbol_that(2, 3, |e| {e.has_name("y");})
        ;
    }

    #[test]
    fn test_epi() {
        let fa = ObjFactory::new10();
        let result = fa.epi();

        assert_expression_that(&result)
            .is_pi();
    }

    #[test]
    fn test_ee() {
        let fa = ObjFactory::new10();
        let result = fa.ee();

        assert_expression_that(&result)
            .is_e();
    }

    #[test]
    fn test_rootz() {
        let fa = ObjFactory::new10();
        let result = fa.rootz(3).unwrap();

        assert_expression_that(&result.radicand)
            .as_integer().is_int(3);
        assert_expression_that(&result.degree)
            .as_integer().is_int(2);
    }

    #[test]
    fn test_erootz() {
        let fa = ObjFactory::new10();
        let result = fa.erootz(3).unwrap();

        assert_expression_that(&result)
            .as_root()
            .has_radicand_that(|e| {e.as_integer().is_int(3);})
            .has_degree_that(|e| {e.as_integer().is_int(2);});
    }

}